<?php
set_time_limit(0);
@ob_clean();
$ImageObj = new DataTable(TABLE_IMAGES);
?>
<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Insert Object</title>
<script>
function insertObject(sHTML)
  {
  var obj=(opener?opener:openerWin).oUtil.obj;

  //Use insertHTML() function to insert your custom html
  obj.insertHTML(sHTML);
  windowClose();
  }
  function windowClose() {
  parent.oUtil.onSelectionChanged=null;
  self.closeWin();
};
</script>
<style>
BODY {font-family:Verdana, Arial, Helvetica;font-size: x-small;margin:3 3 3 3;background-color:#ffffff;}
</style>
</head>
<body>
<h2>My Custom Images</h2>
<ul>
<?php
$SNo=1;
$Count=1;
$ImageObj->Where ="1";
$ImageObj->TableSelectAll(array("ImageID,ImageName"),"ImageName ASC");
while($CurrentImage = $ImageObj->GetObjectFromRecord())
{
?>
<li onclick="insertObject('{%CustomObjectImage_<?php echo $CurrentImage->ImageID?>%}')" style="cursor:pointer"><u><?php echo isset($CurrentImage->ImageName)?MyStripSlashes($CurrentImage->ImageName):""?></u></li>
<?php
}
?>
</ul>

<div id="divSignature" style="display:none">
  Best Regards<br>
  Support Team<br>
  <a href="www.InsiteCreation.com">www.InsiteCreation.com</a>
</div>

<div id="divCompanyName" style="display:none">
  <img src="images/insite.gif" margin="0" width="126" height="31"><br>
  <a href="www.InsiteCreation.com">www.InsiteCreation.com</a><br>
</div>

</body>
</html>
<?php exit;?>