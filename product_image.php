<?php
require_once("includes/configure.php");
$ImageDisplay = false;

$p = isset($_GET['p'])?$_GET['p']:"";
$p_array = explode("/",$p);

$ProductID = "";
$ImageID = "";
$Size = "";

$FileName = basename($p);
$FileNameArr = explode(".",$FileName);

if(isset($FileNameArr[0]) && $FileNameArr[0] != "")
{
	$SeoObj = new DataTable(TABLE_SEO);
	$SeoObj->Where ="RefrenceType= 'product' AND URLName='".$SeoObj->MysqlEscapeString($FileNameArr[0])."'";
	$CurrentProduct = $SeoObj->TableSelectOne(array("ReferenceID"));
	$ProductID = isset($CurrentProduct->ReferenceID)?$CurrentProduct->ReferenceID:"";
}
for($i=0;$i<count($p_array)-1;$i++)
{
	switch (substr($p_array[$i],0,2))
	{
		case "i_":
			$ImageID = substr($p_array[$i],2);
			
			$ProductImageObj = new DataTable(TABLE_PRODUCT_IMAGES);
			$ProductImageObj->Where = "ImageID='".$ProductImageObj->MysqlEscapeString((int)$ImageID)."'";
			$CurrentImage = $ProductImageObj->TableSelectOne(array("ProductID"));
			if(isset($CurrentImage->ProductID) && $CurrentImage->ProductID != "" )
			$ProductID = $CurrentImage->ProductID;
		break;
		case "s_":
			$Size = substr($p_array[$i],2);
			$SizeArray = explode("_",$Size);
		break;
	}
	
}


if($ProductID > 0)
{
	
	$ProductObj= new DataTable(TABLE_PRODUCT);
	$ProductObj->Where ="ProductID='".$ProductObj->MysqlEscapeString((int)$ProductID)."'";
	$CurrentProduct = $ProductObj->TableSelectOne(array("Image"));
	if(isset($CurrentProduct->Image) && $CurrentProduct->Image != "" && file_exists(DIR_FS_SITE_UPLOADS_PRODUCT_ORIGINAL.$CurrentProduct->Image))
	{
		$OriginalFile = DIR_FS_SITE_UPLOADS_PRODUCT_ORIGINAL.$CurrentProduct->Image;
		$ImageDisplay = true;
	}
	
	if($ImageID > 0)
	{
		$ProductImageObj = new DataTable(TABLE_PRODUCT_IMAGES);
		$ProductImageObj->Where = "ImageType ='Image' AND ImageID='".$ProductObj->MysqlEscapeString((int)$ImageID)."' AND ProductID='".$ProductObj->MysqlEscapeString((int)$ProductID)."'";
		$CurrentImage = $ProductImageObj->TableSelectOne(array("Image"));
		if(isset($CurrentImage->Image) && $CurrentImage->Image != "" && file_exists(DIR_FS_SITE_UPLOADS_PRODUCT_ORIGINAL.$CurrentImage->Image))
		{
			$OriginalFile = DIR_FS_SITE_UPLOADS_PRODUCT_ORIGINAL.$CurrentImage->Image;
			$ImageDisplay = true;
		}
								
	}
}




if($ImageDisplay==true)
{
	if(isset($SizeArray) && is_array($SizeArray) && count($SizeArray) > 0)
	{
		@ob_clean();
		$ThumbnailObj = new Thumbnail();
		$ThumbnailObj->HeightMode = true;
		$ThumbnailObj->OriginalFileName = $OriginalFile;
		$ThumbnailObj->ThumbnailWidth = $SizeArray[0];
		if(isset($SizeArray[1]) && $SizeArray[1] > 0)
			$ThumbnailObj->ThumbnailHeight = $SizeArray[1];
		
		if(isset($SizeArray[2]) && $SizeArray[2]==="0")
		$ThumbnailObj->HeightMode = false;
		
		$ThumbnailObj->Display =true;				
		$ThumbnailObj->CreateThumbnail();
			
	}
	else 
	{

		$ThumbnailObj = new Thumbnail();
		$ThumbnailObj->ThumbnailRead($OriginalFile);
		exit;
	}
	
}
else 
{
	header("HTTP/1.1 404 Not Found");
	header("Status: 404 Not Found");
	echo "404 Not Found";
	exit;
}
?>

