<?php 
if(__FILE__==$_SERVER['SCRIPT_FILENAME'])
	die('some error occurred.');
$creditcard_save ="creditcard_save";

if(isset($PaymentProcessStart) && $PaymentProcessStart == "Payment")
{
	@ob_clean();
	global $CurrentOrder, $PaymentStatusArray, $OrderStatusArray, $ShippingStatusArray;
	
	$OrderID = isset($CurrentOrder->OrderID)?$CurrentOrder->OrderID:"";
	
	$CreditCard = new DataTable(TABLE_CREDIT_CARD);
	$CardArray = array();
	$CardArray['OrderID'] = $OrderID;
	$CardArray['CardName'] =(isset($_POST['CardName'])?$_POST['CardName']:(isset($CurrentOrder->BillingFirstName)?$CurrentOrder->BillingFirstName." ".$CurrentOrder->BillingLastName:""));
	$CardArray['CardNo'] = (isset($_POST['CCNo'])?PublicCrypted($_POST['CCNo']):"");
	$CardArray['CVV'] = (isset($_POST['CVV'])?PublicCrypted($_POST['CVV']):"");
	$CardArray['CardType'] = (isset($_POST['CardType'])?$_POST['CardType']:"");
	$CardArray['ExpiryMonth'] = (isset($_POST['ExpiryMonth'])?$_POST['ExpiryMonth']:"");
	$CardArray['ExpiryYear'] = (isset($_POST['ExpiryYear'])?$_POST['ExpiryYear']:"");
	$CardArray['StartMonth'] = (isset($_POST['StartMonth'])?$_POST['StartMonth']:"");
	$CardArray['StartYear'] = (isset($_POST['StartYear'])?$_POST['StartYear']:"");
	$CardArray['CardDisplay'] = "1";
	$CreditCard->TableInsert($CardArray);
	
	/* ORDER change status insert start*/
	$OrderChangeStatusObj = new DataTable(TABLE_ORDER_CHANGE_STATUS);
	$DataArray = array();
	$DataArray['OrderID'] = $OrderID;
	$DataArray['OrderStatus'] = $OrderStatusArray["PendingPayment"];
	$DataArray['ShippingStatus'] = $ShippingStatusArray['Pending'];
	$DataArray['Comments'] = "";
	$DataArray['Notify'] ="0";
	$DataArray['CreatedBy'] ="SCRIPT";
	$DataArray['CreatedDate'] = date("Y-m-d H:i:s");
	$OrderChangeStatusObj->TableInsert($DataArray);
	/* ORDER change status insert end*/
		
	$PaymentStatus ="Completed";
	if($PaymentStatus =="Completed" && @$OrderID !="")
	{
		
		set_time_limit(60);
		@ob_clean();
		unset($_SESSION['OrderID']);
		$CartObj = new DataTable(TABLE_TMPCART);
		$SessionID = session_id();
		$CartObj->Where =" SessionID='".$CartObj->MysqlEscapeString($SessionID)."'";
		$CartObj->TableDelete();	
		
		session_unset();
		session_destroy();
		session_regenerate_id();
		MyRedirect(MakePageURL("index.php","Page=thank_you&Status=Completed&PaymentEnd=1&OrderID=".$OrderID.""));
		exit;
		////
	}
	
}

$ResponseArray[$creditcard_save] = array("code"=>$creditcard_save,
													  "title"=>"CC Save",
													  );

	$ResponseArray[$creditcard_save]['note'] = str_shuffle("Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.");
	$ResponseArray[$creditcard_save]['hidenote'] = '<div class="col-sm-12">
			  <table class="table table-striped table-bordered">
				<tr>
					<td>Credit Card No.</td>
					<td ><input type="text" name="CCNo" id="CCNo_R" placeholder="Credit card" title="Please enter your credit card number" style="width:100%;max-width:150px;" maxlength="16" required></td>
				</tr>
				<tr>
					<td>Expiry Date</td>
					<td><input type="text" name="Month" id="Month_R" title="Please enter month" placeholder="MM" size="1" style="width:40px" maxlength="2" required>&nbsp;&nbsp;/&nbsp;&nbsp;
					<input type="text" name="Year" id="Year_R" title="Please enter year" size="1" placeholder="YY" style="width:40px" maxlength="2" required>&nbsp;
					</td>
				</tr>
				<tr>
					<td>Security Code</td>
					<td><input type="text" name="SCode" id="SCode_R" title="Please enter security code" size="2" style="width:70px" maxlength="4" required></td>
				<tr >
			</table>
	</div>
   ';

													  
if(isset($ResponseArray) && is_array($ResponseArray) && count($ResponseArray) > 0 && !isset($ResponseArray['Error'])){
	$PaymentArray[$creditcard_save] = $ResponseArray;
}
else{
	
}

?>