<?php 
if(__FILE__==$_SERVER['SCRIPT_FILENAME'])
	die('some error occurred.');

require_once (dirname(__FILE__).'/config.php');

if(isset($PaymentProcessStart) && $PaymentProcessStart == "Payment")
{
	
	@ob_clean();
		global $CurrentOrder, $PaymentStatusArray;
		$OrderID = isset($CurrentOrder->OrderID)?$CurrentOrder->OrderID:"";
		$BillingFirstName = isset($CurrentOrder->BillingFirstName)?$CurrentOrder->BillingFirstName:"";
		$BillingLastName = isset($CurrentOrder->BillingLastName)?$CurrentOrder->BillingLastName:"";
		$BillingAddress1 = isset($CurrentOrder->BillingAddress1)?$CurrentOrder->BillingAddress1:"";
		$BillingAddress2 = isset($CurrentOrder->BillingAddress2)?$CurrentOrder->BillingAddress2:"";
		$BillingCity = isset($CurrentOrder->BillingCity)?$CurrentOrder->BillingCity:"";
		$BillingState = isset($CurrentOrder->BillingState)?$CurrentOrder->BillingState:"";
		$BillingCountry = isset($CurrentOrder->BillingCountry)?$CurrentOrder->BillingCountry:"";
		$BillingZip = isset($CurrentOrder->BillingZip)?$CurrentOrder->BillingZip:"";
		$BillingPhone = isset($CurrentOrder->BillingPhone)?$CurrentOrder->BillingPhone:"";
		$BillingFax = isset($CurrentOrder->BillingFax)?$CurrentOrder->BillingFax:"";
	
		$ShippingFirstName = isset($CurrentOrder->ShippingFirstName)?$CurrentOrder->ShippingFirstName:"";
		$ShippingLastName = isset($CurrentOrder->ShippingLastName)?$CurrentOrder->ShippingLastName:"";
		$ShippingAddress1 = isset($CurrentOrder->ShippingAddress1)?$CurrentOrder->ShippingAddress1:"";
		$ShippingAddress2 = isset($CurrentOrder->ShippingAddress2)?$CurrentOrder->ShippingAddress2:"";
		$ShippingCity = isset($CurrentOrder->ShippingCity)?$CurrentOrder->ShippingCity:"";
		$ShippingState = isset($CurrentOrder->ShippingState)?$CurrentOrder->ShippingState:"";
		$ShippingCountry = isset($CurrentOrder->ShippingCountry)?$CurrentOrder->ShippingCountry:"";
		$ShippingZip = isset($CurrentOrder->ShippingZip)?$CurrentOrder->ShippingZip:"";
		$ShippingPhone = isset($CurrentOrder->ShippingPhone)?$CurrentOrder->ShippingPhone:"";
		$ShippingFax = isset($CurrentOrder->ShippingFax)?$CurrentOrder->ShippingFax:"";
		
		$TaxAmount = isset($CurrentOrder->TaxAmount)?$CurrentOrder->TaxAmount:"";
		$ShippingAmount = isset($CurrentOrder->ShippingAmount)?$CurrentOrder->ShippingAmount:"";
		$SubTotal = isset($CurrentOrder->SubTotal)?$CurrentOrder->SubTotal:"";
		$VoucherAmount = isset($CurrentOrder->VoucherAmount)?$CurrentOrder->VoucherAmount:"";
		$Total = $SubTotal - $VoucherAmount;
		$GrandTotal = isset($CurrentOrder->GrandTotal)?$CurrentOrder->GrandTotal:"";;
		$Email = isset($CurrentOrder->Email)?$CurrentOrder->Email:"";
		
		$CartObj = new DataTable(TABLE_ORDER_DETAILS);
		$CartObj->Where =" OrderID='".$CartObj->MysqlEscapeString($OrderID)."' AND (ParentItemID='' OR ISNULL(ParentItemID))";
		$CartObj->TableSelectAll(array("ItemName,ItemNo"),"OrderDetailID ASC");
		$ItemName = ":";
		while ($CurrentCartItem = $CartObj->GetObjectFromRecord()) 
		{
		$ItemName .= strip_tags($CurrentCartItem->ItemName)."\n".", "; 
		}
		$ItemName = substr($ItemName, 0, -2); 
		/*Currency Array start*/
		$OrderCurrencyObj = new DataTable(TABLE_ORDER_CURRENCIES);
		$OrderCurrencyObj->Where ="OrderID='".$CurrentOrder->OrderID."'";
		$CurrentObj = $OrderCurrencyObj->TableSelectOne("");
		$Code = isset($CurrentObj->Code)?$CurrentObj->Code:"GBP";
		if(isset($CurrentObj->Value) && $CurrentObj->Value !="")
		{
			$GrandTotal = $CurrentObj->Value * $GrandTotal;
			$ShippingAmount = $CurrentObj->Value * $ShippingAmount;
			$TaxAmount = $CurrentObj->Value * $TaxAmount;
			$Total = $CurrentObj->Value * $Total;
		}
		/*Currency Array end*/
						      
		$Custom = base64_encode((int)$OrderID.'@@@'.number_format($GrandTotal,2));
		
		$PaypalFields  = array('currency_code'=>$Code,
		 						'cmd'=>'_xclick',
		 						//'redirect_cmd'=>'_cart', 
		 						'business'=>MODULE_PAYMENT_PAYPAL_STANDARD_ACCOUNT,
		 						'rm'=>'2',
		 						'return'=>MakePageURL("index.php","Page=thank_you&Status=Completed&PaymentEnd=1&OrderID=".$OrderID.""),
		 						'cancel_return'=>MakePageURL("index.php","Page=shop/confirm_order"),
		 						'notify_url'=>DIR_WS_SITE_PAYMENT.$paypal_standard_code."/paypal_ipn.php",
		 						'no_notes'=>'1',
		 						'custom'=>$Custom,
		 						'email'=>$Email,              ///billing info
		 						'first_name'=>$BillingFirstName, 
		 						'last_name'=>$BillingLastName, 
		 						'address1'=>$BillingAddress1, 
		 						'address2'=>$BillingAddress2, 
		 						'city'=>$BillingCity, 
		 						'state'=>$BillingState, 
		 						'country'=>$BillingCountry, 
		 						'zip'=>$BillingZip, 
		 						'sh_first_name'=>$ShippingFirstName,     ///shipping info
		 						'sh_last_name'=>$ShippingLastName, 
		 						'sh_address1'=>$ShippingAddress1, 
		 						'sh_address2'=>$ShippingAddress2, 
		 						'sh_city'=>$ShippingCity, 
		 						'sh_state'=>$ShippingState, 
		 						'sh_country'=>$ShippingCountry, 
		 						'sh_zip'=>$ShippingZip, 
		 						'no_shipping'=>'0',
		 						'item_name'=>$ItemName,
		 						//'shipping'=>number_format($ShippingAmount,2),
		 						'tax'=>number_format(0,2),
		 						'amount'=>number_format($GrandTotal,2),
		 					  );
     					      //echo $QueryString; exit;
     					      
	/* -------------------- Website Paypal  Standard start-------------------------*/

				?>
<html>
<head>
<title></title>
<script type="text/javascript">
function FormSubmission()
{
	FormObj = document.getElementById('FormRt');
	FormObj.submit();
	return true;
}
</script>
</head>
<body onload="return FormSubmission();">
<form name="FormRt"  id="FormRt" action="<?php echo MODULE_PAYMENT_PAYPAL_STANDARD_URL?>" method="POST">
<?php
foreach ($PaypalFields as $k=>$v)
{
    ?>
    <input type="hidden" name="<?php echo $k?>" value="<?php echo $v?>">
    <?php
}
?>
<br>
<br>
<input type="submit" value="If this page appears for more than five seconds click here to reload.">
</form>
<br>
</body>
</html>
		<?php
exit;
	/* -------------------- Website Paypal  Standard end-------------------------*/

	
}

$ResponseArray[$paypal_standard_code] = array("code"=>$paypal_standard_code,
													  "title"=>"PayPal Standard",
													  );

if(rand(1,10)%2==0)
{
	$ResponseArray[$paypal_standard_code]['note'] = str_shuffle("");
	$ResponseArray[$paypal_standard_code]['hidenote'] = str_shuffle("");
}
													  
if(isset($ResponseArray) && is_array($ResponseArray) && count($ResponseArray) > 0 && !isset($ResponseArray['Error'])){
	$PaymentArray[$paypal_standard_code] = $ResponseArray;
}
else{
	
}

?>