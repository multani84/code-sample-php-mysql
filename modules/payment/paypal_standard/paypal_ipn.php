<?php 
require_once(dirname(__FILE__)."/../../../includes/configure.php");
require_once (dirname(__FILE__).'/config.php');

$dir = CheckPrivateLogDirectory($paypal_standard_code);
$filename=$dir."PayPalIPN-".date("Md-Y-his")."-".(str_replace(array(".","@@@"),array("-","_"),base64_decode(@$_POST['custom']))).".txt";
$fp2 = fopen($filename, "w");
fputs($fp2,"----------------GET Variable Start---------\r\n");	
foreach ($_GET as $Key=>$Value)
 fputs($fp2,$Key."=".print_r($Value,true)."\r\n");	

fputs($fp2,"----------------GET Variable End---------\r\n");	

fputs($fp2,"----------------POST Variable Start---------\r\n");	
foreach ($_POST as $Key=>$Value)
  fputs($fp2,$Key."=".print_r($Value,true)."\r\n");	

fputs($fp2,"----------------POST Variable end---------\r\n");	

fputs($fp2,"----------------SESSION Variable start---------\r\n");	
foreach ($_SESSION as $Key=>$Value)
 fputs($fp2,$Key."=".print_r($Value,true)."\r\n");	

fputs($fp2,"----------------SESSION Variable end---------\r\n");	

//https://github.com/paypal/ipn-code-samples/blob/master/paypal_ipn.php
error_reporting(0);
$PaymentStatus ="";
$OrderID = "";
if(isset($_POST['custom']))
{ 
	fputs($fp2,"SK= 1\r\n");	
	if(function_exists('get_magic_quotes_gpc'))
	{
	$get_magic_quotes_exists = true;
	}
	
	$req = 'cmd=_notify-validate';
	foreach ($_POST as $key => $value) 
	{
		if($get_magic_quotes_exists == true && get_magic_quotes_gpc() == 1)
			$value = urlencode(stripslashes($value));
		 else 
			$value = urlencode($value);
		
		$req .= "&$key=$value";
	}
	
	/* New code start*/
	
	$ch = curl_init(MODULE_PAYMENT_PAYPAL_STANDARD_URL);
	if ($ch == FALSE) {
		fputs($fp2,"----------------CURL not working--------\r\n");	
	}

	curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
	curl_setopt($ch, CURLOPT_SSLVERSION,6);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $req);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
	curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);

	
	curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
	curl_setopt($ch, CURLOPT_HTTPHEADER, array('Connection: Close'));
	
	$res = curl_exec($ch);
	curl_close($ch);
	
	fputs($fp2,"----------------PAYPAL Request START--------\r\n");	
	fputs($fp2,$req);	
	fputs($fp2,"----------------PAYPAL Request END---------\r\n");	
	
	fputs($fp2,"----------------PAYPAL Response START--------\r\n");	
	fputs($fp2,$res);	
	fputs($fp2,"----------------PAYPAL Response END---------\r\n");	
	
	if (strcmp ($res, "VERIFIED") == 0) 
	{
		
		fputs($fp2,"Business= ".$_POST['business']."---MODULE_PAYMENT_PAYPAL_STANDARD_ACCOUNT= ".MODULE_PAYMENT_PAYPAL_STANDARD_ACCOUNT."\r\n");	
		if(strtolower($_POST['business']) ==strtolower(MODULE_PAYMENT_PAYPAL_STANDARD_ACCOUNT))
		{
			/* custom fetch with amount*/
			$custom_array = explode("@@@",base64_decode($_POST['custom']));
			fputs($fp2,"mc_gross= ".$_POST['mc_gross']."---Custom Amount= ".$custom_array[1]."\r\n");	
			/* custom fetch with amount end*/
	     	
			if($_POST['mc_gross'] == $custom_array[1])
			{
				fputs($fp2,"Custom orderid= ".$custom_array[0]."\r\n");	
				$OrderID= $custom_array[0];
				$PaymentStatus = $_POST['payment_status'];
			}
		}
		
	}
	else if (strcmp ($res, "INVALID") == 0) 
	{
		fputs($fp2,"----------------PAYPAL ERROR START---------\r\n");	
		fputs($fp2,$res);	
		fputs($fp2,"----------------PAYPAL ERROR END---------\r\n");	
	}
	elseif (strcmp ($res, "Failure") == 0)
	{
		fputs($fp2,"----------------PAYPAL ERROR (Failure)---------\r\n");
		fclose($fp2);
		header('HTTP/1.1 500 Internal Server Error');
		exit;
	}	
	
	
	
	/* New code end*/

}
//$PaymentStatus ="Completed";$OrderID ="4";
if($PaymentStatus =="Completed" && @$OrderID !="")
{
	fputs($fp2,"orderid= ".@$OrderID."\r\n");	
	////
	try {
	set_time_limit(60);
	//@GiftVoucherQueue($OrderID);
	@UpdatePaidOrderSetting($OrderID);
	
	@ob_clean();
	unset($_SESSION['OrderID']);
	session_unset();
	session_destroy();
	session_regenerate_id();
	}
	catch(Exception $e)
	{
		
		fputs($fp2,"----------------Exception START---------\r\n");
		fputs($fp2,print_r($e,true)."\r\n");
		fputs($fp2,"----------------Exception END---------\r\n");
		
		$LogObj = new DataTable(TABLE_ORDER_LOG);
		$DataArray = array();
		$DataArray['MSGOBJ'] =serialize($_POST);
		$DataArray['MSG'] =$e->getMessage();
		$DataArray['OrderID'] =$OrderID;
		$DataArray['LogInfo'] =serialize($e);
		$DataArray['CreatedDate'] = date('Y-m-d H:i:s');	
		$LogObj->TableInsert($DataArray);
		
		
		
	}
	
	
	MyRedirect(MakePageURL("index.php","Page=thank_you&Status=Completed&PaymentEnd=1&OrderID=".$OrderID."")); 	
	exit;	
	////
	
}
fclose($fp2);
