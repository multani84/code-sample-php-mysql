<?php 
define("MODULE_PAYMENT_PAYPAL_MODE","LIVE",true);
if(constant("MODULE_PAYMENT_PAYPAL_MODE")=="TEST")
{
	define("MODULE_PAYMENT_PAYPAL_STANDARD_URL","https://www.sandbox.paypal.com/cgi-bin/webscr",true);
	define("MODULE_PAYMENT_PAYPAL_STANDARD_ACCOUNT","",true);

}
else
{
	define("MODULE_PAYMENT_PAYPAL_STANDARD_URL","https://www.paypal.com/cgi-bin/webscr",true);
	define("MODULE_PAYMENT_PAYPAL_STANDARD_ACCOUNT","",true);
}

$paypal_standard_code = "paypal_standard";


?>