<?php 
if(__FILE__==$_SERVER['SCRIPT_FILENAME'])
	die('some error occurred.');
$authorize_aim ="authorize_aim";
require_once (dirname(__FILE__).'/config.php');

if(isset($PaymentProcessStart) && $PaymentProcessStart == "Payment")
{
		@ob_clean();
		global $CurrentOrder, $PaymentStatusArray;
		$OrderID = isset($CurrentOrder->OrderID)?$CurrentOrder->OrderID:"";
		$BillingFirstName = isset($CurrentOrder->BillingFirstName)?$CurrentOrder->BillingFirstName:"";
		$BillingLastName = isset($CurrentOrder->BillingLastName)?$CurrentOrder->BillingLastName:"";
		$BillingAddress1 = isset($CurrentOrder->BillingAddress1)?$CurrentOrder->BillingAddress1:"";
		$BillingAddress2 = isset($CurrentOrder->BillingAddress2)?$CurrentOrder->BillingAddress2:"";
		$BillingCity = isset($CurrentOrder->BillingCity)?$CurrentOrder->BillingCity:"";
		$BillingState = isset($CurrentOrder->BillingState)?$CurrentOrder->BillingState:"";
		$BillingCountry = isset($CurrentOrder->BillingCountry)?$CurrentOrder->BillingCountry:"";
		$BillingZip = isset($CurrentOrder->BillingZip)?$CurrentOrder->BillingZip:"";
		$BillingPhone = isset($CurrentOrder->BillingPhone)?$CurrentOrder->BillingPhone:"";
		$BillingFax = isset($CurrentOrder->BillingFax)?$CurrentOrder->BillingFax:"";
	
		$ShippingFirstName = isset($CurrentOrder->ShippingFirstName)?$CurrentOrder->ShippingFirstName:"";
		$ShippingLastName = isset($CurrentOrder->ShippingLastName)?$CurrentOrder->ShippingLastName:"";
		$ShippingAddress1 = isset($CurrentOrder->ShippingAddress1)?$CurrentOrder->ShippingAddress1:"";
		$ShippingAddress2 = isset($CurrentOrder->ShippingAddress2)?$CurrentOrder->ShippingAddress2:"";
		$ShippingCity = isset($CurrentOrder->ShippingCity)?$CurrentOrder->ShippingCity:"";
		$ShippingState = isset($CurrentOrder->ShippingState)?$CurrentOrder->ShippingState:"";
		$ShippingCountry = isset($CurrentOrder->ShippingCountry)?$CurrentOrder->ShippingCountry:"";
		$ShippingZip = isset($CurrentOrder->ShippingZip)?$CurrentOrder->ShippingZip:"";
		$ShippingPhone = isset($CurrentOrder->ShippingPhone)?$CurrentOrder->ShippingPhone:"";
		$ShippingFax = isset($CurrentOrder->ShippingFax)?$CurrentOrder->ShippingFax:"";
		
		$TaxAmount = isset($CurrentOrder->TaxAmount)?$CurrentOrder->TaxAmount:"";
		$ShippingAmount = isset($CurrentOrder->ShippingAmount)?$CurrentOrder->ShippingAmount:"";
		$SubTotal = isset($CurrentOrder->SubTotal)?$CurrentOrder->SubTotal:"";
		$VoucherAmount = isset($CurrentOrder->VoucherAmount)?$CurrentOrder->VoucherAmount:"";
		$Total = $SubTotal - $VoucherAmount;
		$GrandTotal = isset($CurrentOrder->GrandTotal)?$CurrentOrder->GrandTotal:"";;
		$Email = isset($CurrentOrder->Email)?$CurrentOrder->Email:"";
		
		$CartObj = new DataTable(TABLE_ORDER_DETAILS);
		$CartObj->Where =" OrderID='".$CartObj->MysqlEscapeString($OrderID)."' AND (ParentItemID='' OR ISNULL(ParentItemID))";
		$CartObj->TableSelectAll("","OrderDetailID ASC");
		$ItemName = "";
		while ($CurrentCartItem = $CartObj->GetObjectFromRecord()) 
		{
		$ItemName .= "<lineItem>
						        <itemId>$CurrentCartItem->ItemID</itemId>
						        <name>".substr(strip_tags(str_replace(array("&nbsp;","\""),array("","inch"),$CurrentCartItem->ItemName)),0,20)."</name>
						        <description>".strip_tags(str_replace(array("&nbsp;","\""),array("","inch"),$CurrentCartItem->ItemName))."</description>
						        <quantity>$CurrentCartItem->Qty</quantity>
						        <unitPrice>$CurrentCartItem->Price</unitPrice>
						      </lineItem>"; 
		}
		/*Currency Array start*/
		$OrderCurrencyObj = new DataTable(TABLE_ORDER_CURRENCIES);
		$OrderCurrencyObj->Where ="OrderID='".$CurrentOrder->OrderID."'";
		$CurrentObj = $OrderCurrencyObj->TableSelectOne("");
		$Code = isset($CurrentObj->Code)?$CurrentObj->Code:"GBP";
		if(isset($CurrentObj->Value) && $CurrentObj->Value !="")
		{
			$GrandTotal = $CurrentObj->Value * $GrandTotal;
			$ShippingAmount = $CurrentObj->Value * $ShippingAmount;
			$TaxAmount = $CurrentObj->Value * $TaxAmount;
			$Total = $CurrentObj->Value * $Total;
		}
		/*Currency Array end*/
						      
		//$Custom = base64_encode((int)$OrderID.'@@@'.number_format($GrandTotal,2));
		
		$AuthorizeString="<createTransactionRequest xmlns='AnetApi/xml/v1/schema/AnetApiSchema.xsd'>
						  <merchantAuthentication>
						    <name>".AUTHORIZE_API_LOGIN."</name>
						    <transactionKey>".AUTHORIZE_API_TRANS_KEY."</transactionKey>
						  </merchantAuthentication>
						  <refId>".$CurrentOrder->OrderID."</refId>
						  <transactionRequest>
						    <transactionType>authCaptureTransaction</transactionType>
						    <amount>$CurrentOrder->GrandTotal</amount>
						    <payment>
						      <creditCard>
						        <cardNumber>".$_POST['CCNo']."</cardNumber>
						        <expirationDate>".$_POST['Month'].$_POST['Year']."</expirationDate>
						        <cardCode>".$_POST['SCode']."</cardCode>
						      </creditCard>
						    </payment>
						    <order>  
						     <invoiceNumber>".$CurrentOrder->OrderNo."</invoiceNumber>
						     <description>".DEFINE_SITE_NAME."</description>
						    </order>
							<lineItems>".$ItemName."
							</lineItems>
						    <tax>
						      <amount>".(isset($CurrentOrder->TaxAmount)?$CurrentOrder->TaxAmount:0)."</amount>
						      <name>Tax</name>
						      <description>Tax</description>
						    </tax>						    
						    <shipping>
						      <amount>".(isset($CurrentOrder->ShippingAmount)?$CurrentOrder->ShippingAmount:0)."</amount>
						      <name>Shipping</name>
						      <description>Shipping</description>
						    </shipping>
						    <customer>
						    	<type>individual</type>
						    	<email>".$CurrentOrder->Email."</email>
						    </customer>
						    <billTo>
						      <firstName>".$CurrentOrder->BillingFirstName."</firstName>
						      <lastName>".$CurrentOrder->BillingLastName."</lastName>
						      <company>".(isset($CurrentOrder->Organization)?$CurrentOrder->Organization:'')."</company>
						      <address>".$CurrentOrder->BillingAddress1." ".$CurrentOrder->BillingAddress2."</address>
						      <city>".$CurrentOrder->BillingCity."</city>
						      <state>".$CurrentOrder->BillingState."</state>
						      <zip>".$CurrentOrder->BillingZip."</zip>
						      <country>".$CurrentOrder->BillingCountry."</country>
						      <phoneNumber>".$CurrentOrder->BillingPhone."</phoneNumber>						      
						    </billTo>
						    <shipTo>
						    <firstName>".$CurrentOrder->ShippingFirstName."</firstName>
						      <lastName>".$CurrentOrder->ShippingLastName."</lastName>
						      <company></company>
						      <address>".$CurrentOrder->ShippingAddress1." ".$CurrentOrder->ShippingAddress2."</address>
						      <city>".$CurrentOrder->ShippingCity."</city>
						      <state>".$CurrentOrder->ShippingState."</state>
						      <zip>".$CurrentOrder->ShippingZip."</zip>
						      <country>".$CurrentOrder->ShippingCountry."</country>
				
						    </shipTo>
						    <customerIP>".$_SERVER['REMOTE_ADDR']."</customerIP>
						    <!-- Uncomment this section for Card Present Sandbox Accounts -->
						    <!-- <retail><marketType>2</marketType><deviceType>1</deviceType></retail> -->
						    <transactionSettings>
						      <setting>
						        <settingName>testRequest</settingName>
						        <settingValue>false</settingValue>
						      </setting>
						    </transactionSettings>
						    </transactionRequest>
						</createTransactionRequest>";
		
		//echo $AuthorizeString;exit;
		
		$ch = curl_init(AUTHORIZE_API_URL_SSL); // URL of gateway for cURL to post to
		curl_setopt($ch, CURLOPT_HEADER, 0); // set to 0 to eliminate header info from response
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); // Returns response data instead of TRUE(1)
		curl_setopt($ch, CURLOPT_POSTFIELDS, $AuthorizeString); // use HTTP POST to send form data
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE); // uncomment this line if you get no gateway response. ###
		curl_setopt ($ch, CURLOPT_HTTPHEADER, array("Content-Type: text/xml"));
		$resp = curl_exec($ch); //execute post and get results
		
		$filename=DIR_FS_SITE_UPLOADS_DOWNLOAD."tmp/AuthorizeIPN-".date("Md-Y-his")."-".(str_replace(array(".","@@@"),array("-","_"),base64_decode(@$_POST['custom']))).".txt";
		$fp2 = fopen($filename, "w");
		fputs($fp2,"----------------Auth String---------\r\n");	
		$str = str_replace("<cardNumber>".$_POST['CCNo']."</cardNumber>","",$AuthorizeString);		
		$str = str_replace("<expirationDate>".$_POST['Month'].$_POST['Year']."</expirationDate>","",$str);		
		$str = str_replace("<cardCode>".$_POST['SCode']."</cardCode>","",$str);		
		fputs($fp2, $str."\r\n");
		
		if(curl_errno($ch))
		{
			echo curl_errno($ch)."<br>".curl_error($ch);
			fputs($fp2,"----------------Curl Error---------\r\n");	
			fputs($fp2,curl_errno($ch).'\r\n'.curl_error($ch)."\r\n");
			fclose($fp2);
			curl_close ($ch);
			exit;
			
		}
		curl_close($ch);
			
		fputs($fp2,"\r\n\r\n\r\n----------------Response---------\r\n");	
		fputs($fp2,$resp."\r\n");
		fclose($fp2);
	
		$error="";
		error_reporting(0);
		$xml = simplexml_load_string($resp);
			
		if($xml->transactionResponse->responseCode == 1)
		{	
			$DataArray = array();
			$DataArray['TransactionID'] = $xml->transactionResponse->transId;
			$OrderObj = new DataTable(TABLE_ORDERS);
			$OrderObj->Where="OrderID='$CurrentOrder->OrderID'";
			$OrderObj->TableUpdate($DataArray);
			@UpdatePaidOrderSetting($CurrentOrder->OrderID);
			
			@ob_clean();
			unset($_SESSION['OrderID']);
			session_unset();
			session_destroy();
			session_regenerate_id();
			MyRedirect(MakePageURL("index.php","Page=thank_you&Status=Completed&PaymentEnd=1&OrderID=".$OrderID."")); 	
			exit;		
		}
		else {
				
			if($xml->transactionResponse->errors->error->errorText)
				$_SESSION['ErrorMessage'] = (string)$xml->transactionResponse->errors->error->errorText;
			else if($xml->messages->message)
			{
				foreach ($xml->messages->message as $key => $value) {
					$_SESSION['ErrorMessage'] .= (string)$value->text."<br/>";	
				}				
			}
			//var_dump($_SESSION['ErrorMessage']);exit;
		
			reset($_POST);
		}
		
		
		
		
		
}

$ResponseArray[$authorize_aim] = array("code"=>$authorize_aim,
													  "title"=>"Authorize",
													  );

	//$ResponseArray[$authorize_aim]['note'] = str_shuffle("Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.");
	$ResponseArray[$authorize_aim]['hidenote'] = '<div class="col-sm-12">
			  <table class="table table-striped table-bordered">
				<tr>
					<td>Credit Card No.</td>
					<td ><input type="text" name="CCNo" id="CCNo_R" placeholder="Credit card" title="Please enter your credit card number" style="width:100%;max-width:150px;" maxlength="16" required></td>
				</tr>
				<tr>
					<td>Expiry Date</td>
					<td><input type="text" name="Month" id="Month_R" title="Please enter month" placeholder="MM" size="1" style="width:40px" maxlength="2" required>&nbsp;&nbsp;/&nbsp;&nbsp;
					<input type="text" name="Year" id="Year_R" title="Please enter year" size="1" placeholder="YY" style="width:40px" maxlength="2" required>&nbsp;
					</td>
				</tr>
				<tr>
					<td>Security Code</td>
					<td><input type="text" name="SCode" id="SCode_R" title="Please enter security code" size="2" style="width:70px" maxlength="4" required></td>
				<tr >
			</table>
	</div>
   ';

													  
if(isset($ResponseArray) && is_array($ResponseArray) && count($ResponseArray) > 0 && !isset($ResponseArray['Error'])){
	$PaymentArray[$authorize_aim] = $ResponseArray;
}
else{
	
}

?>