<?php 
define('AUTHORIZE_API_MODE',"TEST",true);

if(constant("AUTHORIZE_API_MODE")=="TEST")
{
	define('AUTHORIZE_API_URL_SSL',"https://api.authorize.net/xml/v1/request.api",true);
	define('AUTHORIZE_API_URL',"https://secure.authorize.net/gateway/transact.dll",true);
	define('AUTHORIZE_API_LOGIN',"api_login",true);
	define('AUTHORIZE_API_TRANS_KEY',"trans_key",true);
	define('AUTHORIZE_API_MD5_HASH',"XXXXXXXXXXXX506040",true);

	
}
else
{
	define('AUTHORIZE_API_URL_SSL',"https://api.authorize.net/xml/v1/request.api",true);
	define('AUTHORIZE_API_URL',"https://secure.authorize.net/gateway/transact.dll",true);
	define('AUTHORIZE_API_LOGIN',"api_login",true);
	define('AUTHORIZE_API_TRANS_KEY',"trans_key",true);
	define('AUTHORIZE_API_MD5_HASH',"XXXXXXXXXXXX506040",true);
}




?>