<?php 
require_once(dirname(__FILE__)."/../../includes/configure.php");

$PaymentArray = array();


$PaymentModuleObj = new DataTable(TABLE_MODULES);
$PaymentModuleObj->Where ="ModuleType='Payment' AND Active='1'";
$PaymentModuleObj->TableSelectAll("","ModuleID ASC");
if($PaymentModuleObj->GetNumRows() >0)
{
	while($CurrentModule = $PaymentModuleObj->GetObjectFromRecord())
	{
		$file = pathinfo($CurrentModule->ModuleFile);
		//var_dump($file);exit
		if(file_exists(DIR_FS_SITE_PAYMENT.$file['filename']."/".$file['basename']))
		{
				require_once(DIR_FS_SITE_PAYMENT.$file['filename']."/".$file['basename']);
		}
	}
}
						
					
if( (isset($_GET['type']) && strtolower($_GET['type'])=="html")
	 || (isset($DirectPaymentCall) && strtolower($DirectPaymentCall)=="html")
  )
{
	

	if(isset($PaymentArray) && is_array($PaymentArray) && count($PaymentArray) > 0)
	{
		$_SESSION['PaymentArray'] = $PaymentArray;
		if(count($PaymentArray)==1)
		{
			foreach($PaymentArray as $k=>$arr)
			{
				$p_arr = $arr[$k];
				?>
				<div class="payment" id="div_payment_<?php echo $k?>">
					<ul>
						<li class="payment_method_li" style="padding:0px;margin:0px;list-style:none">
						   <label>
								<input type="hidden" style="display:inline-block!important;min-height:0px!important;" class="radio" id="<?php echo $p_arr['code']?>" value="<?php echo $p_arr['code']?>" title="Please select payment method" name="payment_method" required>
								<?php echo isset($p_arr['title'])?$p_arr['title']:""?>
							</label>
							<?php if(isset($p_arr['note']) && $p_arr['note'] != ""):?>
								<div class="note"><?php echo $p_arr['note']?></div>
							<?php endif;?>
							<?php if(isset($p_arr['hidenote']) && $p_arr['hidenote'] != ""):?>
								<div><?php echo $p_arr['hidenote']?></div>
							<?php endif;?>
						</li>
						
					</ul>
				</div>	
				<?php
						
			}
		}
		else
		{	
		
			foreach($PaymentArray as $k=>$arr)
			{
				$p_arr = $arr[$k];
				?>
				<div class="payment" id="div_payment_<?php echo $k?>">
					<ul>
						<li class="payment_method_li" style="padding:0px;margin:0px;list-style:none">
						   <label>
								<input type="radio" style="display:inline-block!important;min-height:0px!important;" class="radio" id="<?php echo $p_arr['code']?>" value="<?php echo $p_arr['code']?>" title="Please select payment method" name="payment_method" required>
								<?php echo isset($p_arr['title'])?$p_arr['title']:""?>
							</label>
							<?php if(isset($p_arr['note']) && $p_arr['note'] != ""):?>
								<div class="note"><?php echo $p_arr['note']?></div>
							<?php endif;?>
							<?php if(isset($p_arr['hidenote']) && $p_arr['hidenote'] != ""):?>
								<div class="hidenote"><?php echo $p_arr['hidenote']?></div>
							<?php endif;?>
						</li>
						
					</ul>
				</div>	
				<?php
						
			}
		}
	}
}
if(isset($_GET['type']) && strtolower($_GET['type'])=="json")
{
		$_SESSION['PaymentArray'] = $PaymentArray;
		@ob_clean();
		header('Content-Type: application/json');
		echo json_encode($PaymentArray);
		exit;
}

?>