<?php 
require_once(dirname(__FILE__)."/../../../includes/configure.php");
require_once (dirname(__FILE__).'/config.php');

$dir = CheckPrivateLogDirectory($payu_standard_code);
$filename=$dir."PayuIPN-".date("Md-Y-his").".txt";
$fp2 = fopen($filename, "w");
fputs($fp2,"----------------GET Variable Start---------\r\n");	
foreach ($_GET as $Key=>$Value)
 fputs($fp2,$Key."=".print_r($Value,true)."\r\n");	

fputs($fp2,"----------------GET Variable End---------\r\n");	

fputs($fp2,"----------------POST Variable Start---------\r\n");	
foreach ($_POST as $Key=>$Value)
  fputs($fp2,$Key."=".print_r($Value,true)."\r\n");	

fputs($fp2,"----------------POST Variable end---------\r\n");	

fputs($fp2,"----------------SESSION Variable start---------\r\n");	
foreach ($_SESSION as $Key=>$Value)
 fputs($fp2,$Key."=".print_r($Value,true)."\r\n");	

fputs($fp2,"----------------SESSION Variable end---------\r\n");	

//https://github.com/paypal/ipn-code-samples/blob/master/paypal_ipn.php
error_reporting(0);
//$PaymentStatus ="";
$OrderID = "";

$status=$_POST["status"];
$firstname=$_POST["firstname"];
$amount=$_POST["amount"];
$txnid=$_POST["txnid"];

$posted_hash=$_POST["hash"];
$key=$_POST["key"];
$productinfo=$_POST["productinfo"];
$email=$_POST["email"];
$salt=MODULE_PAYMENT_PAYU_STANDARD_SALT;

if (isset($_POST["additionalCharges"])) {
       $additionalCharges=$_POST["additionalCharges"];
        $retHashSeq = $additionalCharges.'|'.$salt.'|'.$status.'|||||||||||'.$email.'|'.$firstname.'|'.$productinfo.'|'.$amount.'|'.$txnid.'|'.$key;
        
}
else {	  

    $retHashSeq = $salt.'|'.$status.'|||||||||||'.$email.'|'.$firstname.'|'.$productinfo.'|'.$amount.'|'.$txnid.'|'.$key;

}
$hash = hash("sha512", $retHashSeq);

if ($hash != $posted_hash) {
	echo "Invalid Transaction. Please try again";
}
else {

	try {

		echo "<h3>Thank You. Your order status is ". $status .".</h3>";
		echo "<h4>Your Transaction ID for this transaction is ".$txnid.".</h4>";
		echo "<h4>We have received a payment of Rs. " . $amount . ". Your order will soon be shipped.</h4>";

		$Orderno = $txnid

		$OrderObj = new DataTable(TABLE_ORDERS);
		$OrderObj->Where ="OrderNo='".$Orderno."'";
		$CurrentOrder = $OrderObj->TableSelectOne();

		$OrderID = $CurrentOrder->OrderID;
		//set_time_limit(60);
		//@GiftVoucherQueue($OrderID);
		@UpdatePaidOrderSetting($OrderID);

		@ob_clean();
		unset($_SESSION['OrderID']);
		session_unset();
		session_destroy();
		session_regenerate_id();

	}
	catch(Exception $e)
	{

		fputs($fp2,"----------------Exception START---------\r\n");
		fputs($fp2,print_r($e,true)."\r\n");
		fputs($fp2,"----------------Exception END---------\r\n");

		$LogObj = new DataTable(TABLE_ORDER_LOG);
		$DataArray = array();
		$DataArray['MSGOBJ'] =serialize($_POST);
		$DataArray['MSG'] =$e->getMessage();
		$DataArray['OrderID'] =$OrderID;
		$DataArray['LogInfo'] =serialize($e);
		$DataArray['CreatedDate'] = date('Y-m-d H:i:s');	
		$LogObj->TableInsert($DataArray);



	}


	MyRedirect(MakePageURL("index.php","Page=thank_you&Status=Completed&PaymentEnd=1&OrderID=".$OrderID."")); 	
	exit;	

}

         
    
fclose($fp2);
