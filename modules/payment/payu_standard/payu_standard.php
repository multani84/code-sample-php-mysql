<?php 
if(__FILE__==$_SERVER['SCRIPT_FILENAME'])
	die('some error occurred.');

require_once (dirname(__FILE__).'/config.php');

if(isset($PaymentProcessStart) && $PaymentProcessStart == "Payment")
{
	
	@ob_clean();
		global $CurrentOrder, $PaymentStatusArray;
		$OrderID = isset($CurrentOrder->OrderID)?$CurrentOrder->OrderID:"";
		$BillingFirstName = isset($CurrentOrder->BillingFirstName)?$CurrentOrder->BillingFirstName:"";
		$BillingLastName = isset($CurrentOrder->BillingLastName)?$CurrentOrder->BillingLastName:"";
		$BillingAddress1 = isset($CurrentOrder->BillingAddress1)?$CurrentOrder->BillingAddress1:"";
		$BillingAddress2 = isset($CurrentOrder->BillingAddress2)?$CurrentOrder->BillingAddress2:"";
		$BillingCity = isset($CurrentOrder->BillingCity)?$CurrentOrder->BillingCity:"";
		$BillingState = isset($CurrentOrder->BillingState)?$CurrentOrder->BillingState:"";
		$BillingCountry = isset($CurrentOrder->BillingCountry)?$CurrentOrder->BillingCountry:"";
		$BillingZip = isset($CurrentOrder->BillingZip)?$CurrentOrder->BillingZip:"";
		$BillingPhone = isset($CurrentOrder->BillingPhone)?$CurrentOrder->BillingPhone:"";
		$BillingFax = isset($CurrentOrder->BillingFax)?$CurrentOrder->BillingFax:"";
	
		$ShippingFirstName = isset($CurrentOrder->ShippingFirstName)?$CurrentOrder->ShippingFirstName:"";
		$ShippingLastName = isset($CurrentOrder->ShippingLastName)?$CurrentOrder->ShippingLastName:"";
		$ShippingAddress1 = isset($CurrentOrder->ShippingAddress1)?$CurrentOrder->ShippingAddress1:"";
		$ShippingAddress2 = isset($CurrentOrder->ShippingAddress2)?$CurrentOrder->ShippingAddress2:"";
		$ShippingCity = isset($CurrentOrder->ShippingCity)?$CurrentOrder->ShippingCity:"";
		$ShippingState = isset($CurrentOrder->ShippingState)?$CurrentOrder->ShippingState:"";
		$ShippingCountry = isset($CurrentOrder->ShippingCountry)?$CurrentOrder->ShippingCountry:"";
		$ShippingZip = isset($CurrentOrder->ShippingZip)?$CurrentOrder->ShippingZip:"";
		$ShippingPhone = isset($CurrentOrder->ShippingPhone)?$CurrentOrder->ShippingPhone:"";
		$ShippingFax = isset($CurrentOrder->ShippingFax)?$CurrentOrder->ShippingFax:"";
		
		$TaxAmount = isset($CurrentOrder->TaxAmount)?$CurrentOrder->TaxAmount:"";
		$ShippingAmount = isset($CurrentOrder->ShippingAmount)?$CurrentOrder->ShippingAmount:"";
		$SubTotal = isset($CurrentOrder->SubTotal)?$CurrentOrder->SubTotal:"";
		$VoucherAmount = isset($CurrentOrder->VoucherAmount)?$CurrentOrder->VoucherAmount:"";
		$Total = $SubTotal - $VoucherAmount;
		$GrandTotal = isset($CurrentOrder->GrandTotal)?$CurrentOrder->GrandTotal:"";;
		$Email = isset($CurrentOrder->Email)?$CurrentOrder->Email:"";
		
		$CartObj = new DataTable(TABLE_ORDER_DETAILS);
		$CartObj->Where =" OrderID='".$CartObj->MysqlEscapeString($OrderID)."' AND (ParentItemID='' OR ISNULL(ParentItemID))";
		$CartObj->TableSelectAll(array("ItemName,ItemNo"),"OrderDetailID ASC");
		$ItemName = "";
		while ($CurrentCartItem = $CartObj->GetObjectFromRecord()) 
		{
		$ItemName .= strip_tags($CurrentCartItem->ItemName).", "; 
		}
		$ItemName = substr($ItemName, 0, -2); 
		/*Currency Array start*/
		$OrderCurrencyObj = new DataTable(TABLE_ORDER_CURRENCIES);
		$OrderCurrencyObj->Where ="OrderID='".$CurrentOrder->OrderID."'";
		$CurrentObj = $OrderCurrencyObj->TableSelectOne("");
		$Code = isset($CurrentObj->Code)?$CurrentObj->Code:"GBP";
		if(isset($CurrentObj->Value) && $CurrentObj->Value !="")
		{
			$GrandTotal = $CurrentObj->Value * $GrandTotal;
			$ShippingAmount = $CurrentObj->Value * $ShippingAmount;
			$TaxAmount = $CurrentObj->Value * $TaxAmount;
			$Total = $CurrentObj->Value * $Total;
		}
		/*Currency Array end*/
						      
		$Custom = base64_encode((int)$OrderID.'@@@'.number_format($GrandTotal,2));
		//$txnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
		$txnid = $CurrentOrder->OrderNo;
		
		
		$PayUFields  = array( 
		 						'key'=>MODULE_PAYMENT_PAYU_STANDARD_ACCOUNT,
		 						//'hash'=>MODULE_PAYMENT_PAYU_STANDARD_ACCOUNT,
		 						
		 						'txnid'=>$txnid,
		 						'amount'=>$GrandTotal,
		 					// 	'productinfo'=>addslashes($ItemName),
		 						'productinfo'=>'Order Payment',
		 						'firstname'=>$BillingFirstName,
		 						'email'=>$Email,
		 						'surl'=>DIR_WS_SITE_PAYMENT.$payu_standard_code."/success.php",
		 						'furl'=>DIR_WS_SITE_PAYMENT.$payu_standard_code."/failure.php",
		 						'curl'=>MakePageURL("index.php","Page=shop/confirm_order"),
		 						'service_provider'=>MODULE_PAYMENT_PAYU_STANDARD_SERVICE_PROVIDER,
		 						              ///billing info
		 						 
		 						'lastname'=>$BillingLastName, 
		 						'address1'=>$BillingAddress1, 
		 						'address2'=>$BillingAddress2, 
		 						'city'=>$BillingCity, 
		 						'state'=>$BillingState, 
		 						'country'=>$BillingCountry, 
		 						'zipcode'=>$BillingZip, 
		 						
		 						
						
		 					  );


		$hash = '';
		// Hash Sequence
		$hashSequence = "key|txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|udf4|udf5|udf6|udf7|udf8|udf9|udf10";
		$hashVarsSeq = explode('|', $hashSequence);
	    $hash_string = '';	
		foreach($hashVarsSeq as $hash_var) {
	      $hash_string .= isset($PayUFields[$hash_var]) ? $PayUFields[$hash_var] : '';
	      $hash_string .= '|';
	    }

	    $hash_string .= MODULE_PAYMENT_PAYU_STANDARD_SALT;


	    
	    $hash = strtolower(hash('sha512', $hash_string));

	    $PayUFields['hash'] = $hash;
	    

	    $action = MODULE_PAYMENT_PAYU_STANDARD_URL . '/_payment';


	
	 

     				

				?>
<html>
<head>
<title></title>
<script type="text/javascript">
var hash = '<?php echo $hash ?>';
// alert(hash);
function FormSubmission()
{
	FormObj = document.getElementById('FormRt');
	FormObj.submit();
	return true;
}
</script>
</head>
<body onload="return FormSubmission();">
<form name="FormRt"  id="FormRt" action="<?php echo $action?>" method="POST">

<?php
foreach ($PayUFields as $k=>$v)
{
   /* if($k == 'productinfo'){ ?>
    <textarea name="<?php echo $k?>"><?php echo (empty($v)) ? '' : $v ?></textarea>
    <?php } else{*/ ?>
    <input type="hidden" name="<?php echo $k?>" value="<?php echo $v?>">
    <?php //}
}
?>
<br>
<br>
<input type="submit" value="If this page appears for more than five seconds click here to reload.">
</form>
<br>
</body>
</html>
		<?php
exit;
	/* -------------------- Website Paypal  Standard end-------------------------*/

	
}

$ResponseArray[$payu_standard_code] = array("code"=>$payu_standard_code,
													  "title"=>"",
													  );

if(rand(1,10)%2==0)
{
	$ResponseArray[$payu_standard_code]['note'] = str_shuffle("");
	$ResponseArray[$payu_standard_code]['hidenote'] = str_shuffle("");
}
													  
if(isset($ResponseArray) && is_array($ResponseArray) && count($ResponseArray) > 0 && !isset($ResponseArray['Error'])){
	$PaymentArray[$payu_standard_code] = $ResponseArray;
}
else{
	
}

?>