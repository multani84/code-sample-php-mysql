<?php
	  require_once (dirname(__FILE__)."/conf.php");
	  
	  $ups_data['PostcodePrimaryLow'] = isset($_REQUEST['zip'])?$_REQUEST['zip']:"";
	  $ups_data['CountryCode'] = isset($_REQUEST['country'])?$_REQUEST['country']:"GB";
	  $ups_data['CustomerContext'] = $ups_data['PostcodePrimaryLow']." ".$ups_data['CountryCode'];
	
	  $www_path = str_replace("data.php","",$_SERVER['SCRIPT_NAME']);	
	  
	  try
      {
        $filename = dirname(__FILE__)."/request.xml";
		$handle = fopen($filename, "r");
		$request = fread($handle, filesize($filename));
		fclose($handle);

		foreach($ups_data as $k=>$val)
			$request = str_replace("{{#$k#}}",$val,$request);
		
		 //create Post request
         $form = array
         (
             'http' => array
             (
                 'method' => 'POST',
                 'header' => 'Content-type: application/x-www-form-urlencoded',
                 'content' => "$request"
             )
         );

         //print form request
         $request = stream_context_create($form);
         $browser = fopen($ups_endpointurl , 'rb' , false , $request);
        
		if(!$browser)
         {
             throw new Exception("Connection failed.");
         }

         //get response
         $response = stream_get_contents($browser);
         fclose($browser);

         if($response == false)
         {
            throw new Exception("Bad data.");
         }
         else
         {
			 //echo $response;exit;
            //get response status
            $resp = new SimpleXMLElement($response);
			$LocationArray  =array ();
			
			/* Default Home start*/
			$LocationID = "Home";
			$LocationArray[$LocationID]['LocationID'] = $LocationID;
			$LocationArray[$LocationID]['Home'] = "1";
			$LocationArray[$LocationID]['Icon'] = $www_path."home.png";
			$LocationArray[$LocationID]['Title'] = $ups_data['PostcodePrimaryLow']." ". $ups_data['CountryCode'];
			$LocationArray[$LocationID]['Address'] = "";
			$LocationArray[$LocationID]['PhoneNumber'] = "";
			$LocationArray[$LocationID]['Latitude'] = (string)$resp->Geocode->Latitude;
			$LocationArray[$LocationID]['Longitude'] = (string)$resp->Geocode->Longitude;
			$LocationArray[$LocationID]['Distance'] = "";
			$LocationArray[$LocationID]['StandardHoursOfOperation'] = "";
			$LocationArray[$LocationID]['Content'] = "<b>You are here: ".$LocationArray[$LocationID]['Title']."</b>";
															 
			
			/* Default Home end*/
			
			foreach($resp->SearchResults->DropLocation as $obj)
			{
					$LocationID = (string)$obj->LocationID;
					$LocationArray[$LocationID]['LocationID'] = $LocationID;
					$LocationArray[$LocationID]['Icon'] = $www_path."ups.png";
					
					$LocationArray[$LocationID]['Title'] = (string)$obj->AddressKeyFormat->ConsigneeName;
					$LocationArray[$LocationID]['Address'] = (string)$obj->AddressKeyFormat->AddressLine;
					
					if(isset($obj->AddressKeyFormat->PoliticalDivision1) && $obj->AddressKeyFormat->PoliticalDivision1 != "")
						$LocationArray[$LocationID]['Address'] .= " ".(string)$obj->AddressKeyFormat->PoliticalDivision1;
					
					if(isset($obj->AddressKeyFormat->PoliticalDivision2) && $obj->AddressKeyFormat->PoliticalDivision2 != "")
						$LocationArray[$LocationID]['Address'] .= " ".(string)$obj->AddressKeyFormat->PoliticalDivision2;
					
					if(isset($obj->AddressKeyFormat->PostcodePrimaryLow) && $obj->AddressKeyFormat->PostcodePrimaryLow != "")
						$LocationArray[$LocationID]['Address'] .= " ".(string)$obj->AddressKeyFormat->PostcodePrimaryLow;
					if(isset($obj->AddressKeyFormat->CountryCode) && $obj->AddressKeyFormat->CountryCode != "")
						$LocationArray[$LocationID]['Address'] .= " ".(string)$obj->AddressKeyFormat->CountryCode;
										
					$LocationArray[$LocationID]['PhoneNumber'] = (string)$obj->PhoneNumber;
					$LocationArray[$LocationID]['Latitude'] = (string)$obj->Geocode->Latitude;
					$LocationArray[$LocationID]['Longitude'] = (string)$obj->Geocode->Longitude;
					$LocationArray[$LocationID]['Distance'] = (string)$obj->Distance->Value ." - ". (string)$obj->Distance->UnitOfMeasurement->Description;
					$LocationArray[$LocationID]['StandardHoursOfOperation'] = (string)$obj->StandardHoursOfOperation;
					$LocationArray[$LocationID]['Content'] = "<b>".$LocationArray[$LocationID]['Title']."</b>".
															 "<br>Address: ".$LocationArray[$LocationID]['Address'].
															 "<hr>Phone: ".$LocationArray[$LocationID]['PhoneNumber'].
															 "<br>Distance: ".$LocationArray[$LocationID]['Distance'].
															 "<br>Hours: ".$LocationArray[$LocationID]['StandardHoursOfOperation'];
															 
			}
			
			@ob_clean;
			echo json_encode($LocationArray);
			exit;
            
         }

      }
		catch(Exception $ex)
      {
      	echo $ex;
      }

?>

