<?php if(!isset($gm_init)):?>
<script type="text/javascript" src="https://maps.google.com/maps/api/js?sensor=false"></script>
<script type="text/javascript">
function geocodeResult(results, status) {
  	if (status == 'OK' && results.length > 0) {
  	   map.zoom = 17;
       map.fitBounds(results[0].geometry.viewport);
     addMarkerAtCenter();
    } else {
      //alert("Geocode was not successful for the following reason: " + status);
    }
  }
</script>
<?php $gm_init =1; endif;?>
<?php 
if(!isset($gm_w))
	$gm_w=isset($_REQUEST['gm_w'])?$_REQUEST['gm_w']:"100%";

if(!isset($gm_h))
	$gm_h=isset($_REQUEST['gm_h'])?$_REQUEST['gm_h']:"400px";

if(!isset($gm_id))
	$gm_id=isset($_REQUEST['gm_id'])?$_REQUEST['gm_id']:uniqid("gm_");

if(!isset($gm_lat))
	$gm_lat=isset($_REQUEST['gm_lat'])?$_REQUEST['gm_lat']:"55.3780510137493";

if(!isset($gm_lng))
	$gm_lng=isset($_REQUEST['gm_lng'])?$_REQUEST['gm_lng']:"-5.486207";

?>
<div id="<?php echo $gm_id?>"></div>
 <style>
  #<?php echo $gm_id?> {
    width: <?php echo $gm_w?>;
    height: <?php echo $gm_h?>;
  }
  </style>
<script type="text/javascript">
	var map_<?php echo $gm_id?>;
  var geocoder_<?php echo $gm_id?>;
  var marker_<?php echo $gm_id?>;
   function initialize_<?php echo $gm_id?>() {
  	var myLatlng = new google.maps.LatLng(<?php echo $gm_lat?>, <?php echo $gm_lng?>);
  	var myOptions = {
      zoom: 11,
      center: myLatlng,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    }
    map_<?php echo $gm_id?> = new google.maps.Map(document.getElementById("<?php echo $gm_id?>"), myOptions);
    geocoder_<?php echo $gm_id?> = new google.maps.Geocoder();
    marker_<?php echo $gm_id?> = new google.maps.Marker({
        draggable: true
    });

    
   	
    
    
  }
  initialize_<?php echo $gm_id?>();
  </script>
 