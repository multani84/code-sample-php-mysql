<!-- Heading  -->
<div class="row-fluid">
	<div class="span12">
		<h1>Shipping</h1>
		<hr />
		<?php  echo isset($CurrentShipping->ShippingGroup)?"<b>".$CurrentShipping->ShippingGroup."</b>":""?>
	</div>
	<div class="pull-left">
		<a href="<?php  echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php  echo $Page?>&Shipping=<?php  echo $Shipping?>" class="adm-link">SHIPPING GROUPS</a>
	</div>
	<div class="pull-right">
		<a href="<?php  echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php  echo $Page?>&Shipping=<?php  echo $Shipping?>&Section=AddShipping" class="btn btn-primary">Add New Shipping Group  <icon class="icon-plus-sign icon-white-t"></icon></a>
	</div>
<div class="clearfix separator bottom"></div>
<!--  End Heading-->	

<?php 
switch ($Section)
{
	case "AddShipping":
	case "EditShipping":
		$CountryNameArray = array();
		$TableName = TABLE_COUNTRIES." c LEFT JOIN ".TABLE_SHIPPING_COUNTRIES." sc on c.CountryID=sc.CountryID";
		$ShippingCountryNameObj = new DataTable($TableName);
		//$ShippingCountryNameObj->DisplayQuery = true;
		$ShippingCountryNameObj->Where ="1";
		$ShippingCountryNameObj->TableSelectAll(array("c.CountryID","c.CountryName"),"c.CountryName ASC");
		while($CountryName = $ShippingCountryNameObj->GetObjectFromRecord())
		{
			$CountryNameArray[$CountryName->CountryID] = $CountryName->CountryName;
		}
		
		$CountryNameGoArray = array();
		$CountryNameInArray = array();
		if(isset($_POST['CountryNameGo']) && is_array($_POST['CountryNameGo']))
		{	
			foreach ($_POST['CountryNameGo'] as $k=>$v)
				$CountryNameGoArray[$v] = $CountryNameArray[$v];
			
		}
		else 
		{
			$ShippingCountryNameObj->Where ="ShippingID='$ShippingID'";
			$ShippingCountryNameObj->TableSelectAll(array("c.CountryID","c.CountryName"),"c.CountryName ASC");
			while($CountryNameGo = $ShippingCountryNameObj->GetObjectFromRecord())
				$CountryNameGoArray[$CountryNameGo->CountryID] = $CountryNameArray[$CountryNameGo->CountryID];
		}
		$CountryNameInArray = array_diff($CountryNameArray,$CountryNameGoArray);
	
	?>
		<form class="form-horizontal" method="POST" action="<?php  echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php  echo $Page?>&Shipping=<?php  echo $Shipping?>&Section=<?php  echo $Section?>&Target=AddShipping&ShippingID=<?php  echo $ShippingID?>" enctype="multipart/form-data">
		
		<div class="well">
		
			<div class="form-group">
				<label for="field" class="col-sm-4 col-md-5 col-lg-4 control-label">Shipping Group Name</label>
				<div class="col-sm-20 col-md-19 col-lg-20 text-left">
					<input type="text" name="ShippingGroup" value="<?php  echo isset($_POST['ShippingGroup'])?MyStripSlashes($_POST['ShippingGroup']):(isset($CurrentShipping->ShippingGroup)?MyStripSlashes($CurrentShipping->ShippingGroup):"")?>" size="70">
				</div>
			</div>
			<div class="form-group">
				<label for="field" class="col-sm-4 col-md-5 col-lg-4 control-label">Shipping Price</label>
				<div class="col-sm-20 col-md-19 col-lg-20 text-left">
					<input type="text" name="ShippingPrice" value="<?php  echo isset($_POST['ShippingPrice'])?MyStripSlashes($_POST['ShippingPrice']):(isset($CurrentShipping->ShippingPrice)?MyStripSlashes($CurrentShipping->ShippingPrice):"")?>" size="70">
				</div>
			</div>
			
			<div class="form-group">
				<label for="field" class="col-sm-4 col-md-5 col-lg-4 control-label">&nbsp;</label>
				<div class="col-sm-20 col-md-19 col-lg-20 text-left">
					<table border="0" cellpadding="3" cellspacing="1" width="100%" class="InsideTable">
							<tr>
							<td height="40" align="center">&nbsp;</td>
							<td width="30%"><b>Countries IN</b></td>
							<td width="10%"></td>
							<td  width="30%"><b>Countries OUT</b></td>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td height="40" width="50" align="center"></td>
						<td>
						<select name="CountryNameGo[]" id="CountryNameGo" size="25" multiple ondblclick="fun_singlego();">
						<?php 
						foreach ($CountryNameGoArray as $k=>$v)
						{?>
						<option value="<?php  echo $k?>"><?php  echo $v?></option>
						<?php 
						}
						?>
						</select>
						</td>
						<td align="center">
						<button type="button" name="go" onclick="fun_singlego();">&gt;&gt;</button>
						<br>
						<br>
						<button type="button" name="in" onclick="fun_singlein();">&lt;&lt;</button>
						</td>
						<td><select name="CountryNameIn[]" id="CountryNameIn"  size="25" multiple ondblclick="fun_singlein();">
						<?php 
						foreach ($CountryNameInArray as $k=>$v)
						{?>
						<option value="<?php  echo $k?>"><?php  echo $v?></option>
						<?php 
						}
						?>
						
						</select>
						</td>
						</tr>
					</table>
					<script type="text/javascript">
		sel_country = document.getElementById("CountryNameIn");
		country = document.getElementById("CountryNameGo");	
		sel_country_len = sel_country.length;
		country_len = country.length;
		
		if(sel_country_len > 0)
			sel_country.options[0].selected = true;
		if(country_len > 0)
			country.options[0].selected = true;

		function FunctionSelectIn()
			{
				for(i=0;i<country_len;i++)
				country.options[i].selected = true;
				return true;
			}
		function fun_sel_all()
		{
			for(i=0;i<sel_country_len;i++)
				sel_country.options[i].selected = true;
		}

		function fun_singlego()
		{
			for(i=0;i<sel_country_len;i++)
				sel_country.options[i].selected = false;
			i=0;		
			while(i<country_len)
			{
				if(country.options[i].selected == true)
				{
					sel_country[sel_country_len] = new Option(country.options[i].text,country.options[i].value);
					country.removeChild(country.options[i]);
					sel_country.options[sel_country_len].selected = true;
					i=0;
					sel_country_len = sel_country.length;
					country_len = country.length;
				}
				else
					i++;
			}
			if(country_len>0)
				country.options[0].selected = true;
		}

		function fun_singlein()
		{
			for(i=0;i<country_len;i++)
				country.options[i].selected = false;
			i=0;			
			while(i<sel_country_len)
			{
				if(sel_country.options[i].selected == true)
				{
					country[country_len] = new Option(sel_country.options[i].text,sel_country.options[i].value);
					sel_country.removeChild(sel_country.options[i]);
					country.options[country_len].selected = true;
					i=0;
					sel_country_len = sel_country.length;
					country_len = country.length;
				}
				else
					i++;
			}
			if(sel_country_len>0)
				sel_country.options[0].selected = true;
		}

		</script>
				</div>
			</div>
		
		
			<div class="form-group">
				<div class="col-sm-6 pull-right">
					<button class="btn btn-warning  btn-block" type="submit" name="AddShipping" onclick="return FunctionSelectIn();">Submit</button>
				</div>
			</div>
		
          	
			</div>
		</form>
		<?php 
	break;
	default:
		$ShippingGroupObj->Where = "1";
		$ShippingGroupObj->TableSelectAll("","CreatedDate ASC");
		if($ShippingGroupObj->GetNumRows() > 0)
		{
		?>
		<table class="table table-striped table-bordered table-responsive block">
			<thead>
   			 <tr>
				<td align="center" width="5%"><b>S.No.</b></td>
				<td align="left"><b>Group Name</b></td>
				<td align="left"><b>Shipping Price</b></td>
				<td align="center"><b>Edit</b></td>
				<td align="center"><b>Delete</b></td>
			</tr>
			</thead>
			<tbody>
			<script type="text/javascript">
				function FunctionDelete()
				{
					result = confirm("Are you sure want to delete the Shipping Zone?")
					if(result == true)
					{
						return true;
					}
					return false;
				}				
							
			</script>
			<?php 
			$SNo=1;
			$Count=1;
			
			
			while($CurrentShippingGroup = $ShippingGroupObj->GetObjectFromRecord())
			{
				
			?>
			<tr class="InsideRightTd">
				<td align="center"><b><?php  echo $SNo?></b></td>
				<td align="left"><b><?php  echo stripslashes($CurrentShippingGroup->ShippingGroup);?></b></td>
				<td align="left"><b><?php  echo stripslashes($CurrentShippingGroup->ShippingPrice);?></b></td>
				<td align="center"><a href="<?php  echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php  echo $Page?>&Shipping=<?php  echo $Shipping?>&Section=EditShipping&ShippingID=<?php  echo $CurrentShippingGroup->ShippingID?>" class="btn btn-success btn-phone-block"><icon class="icon-pencil icon-white"></icon> Edit</a></td>
				<td align="center">
				<?php 
				if($CurrentShippingGroup->ShippingID !="1")
				{
				?>
					<a href="<?php  echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php  echo $Page?>&Shipping=<?php  echo $Shipping?>&Target=DeleteShipping&ShippingID=<?php  echo $CurrentShippingGroup->ShippingID?>" onclick="return FunctionDelete();" class="btn btn-danger btn-phone-block"><icon class="icon-remove icon-white"></icon> Delete</a>
				<?php 
				}?>	
				</td>
			</tr>
			<?php 
			$SNo++;
			$Count++;
			}
		?>
		 </tbody>
		</table>
		<?php 
		}
		else 
		{
			?>
			<b>No Result Found.</b>
			<?php 
		}
	break;
}
?>
				