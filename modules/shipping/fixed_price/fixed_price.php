<?php 
if(__FILE__==$_SERVER['SCRIPT_FILENAME'])
	die('some error occurred.');


$fixed_price = "fixed_price";

function GetFixedPriceResponse()
{
	$ResponseArray  = array();
	$ShippingGroupObj = new DataTable(TABLE_SHIPPING_RZS_GROUPS);
	$ShippingGroupObj->where ="ShippingID = '1'";
	$Obj = $ShippingGroupObj->TableSelectOne();
	
	$Price = isset($Obj->ShippingPrice)?$Obj->ShippingPrice:"";
	if( isset($_POST['ShippingCountry']) && $_POST['ShippingCountry'] != "" ){
		
		//SELECT * FROM zsk_countries c LEFT JOIN zsk_shipping_countries sc on c.CountryID=sc.CountryID LEFT JOIN zsk_shipping_rzs_groups sg on sg.ShippingID=sc.ShippingID WHERE c.CountryISOCode1 = 'US' ORDER BY sg.ShippingPrice DESC 
		$TableName = TABLE_COUNTRIES." c 
					  LEFT JOIN ".TABLE_SHIPPING_COUNTRIES." sc on c.CountryID=sc.CountryID
					  LEFT JOIN ".TABLE_SHIPPING_RZS_GROUPS." sg on sg.ShippingID=sc.ShippingID
					";
		
		$ShippingGroupObj = new DataTable($TableName);
		$ShippingGroupObj->Where ="c.CountryISOCode1 = '".$ShippingGroupObj->MysqlEscapeString(substr($_POST['ShippingCountry'],0,2))."'";
		$Obj = $ShippingGroupObj->TableSelectOne(array("sg.ShippingPrice"),"sg.ShippingPrice DESC");
		if(isset($Obj->ShippingPrice))
			$Price = isset($Obj->ShippingPrice)?$Obj->ShippingPrice:"";
			
		
		
	}
	else if(isset($_SESSION['OrderID']) && $_SESSION['OrderID'] !=""){
	
		$OrderObj1 = new DataTable(TABLE_ORDERS);
		$OrderObj1->Where ="OrderID='".(int)$_SESSION['OrderID']."'";	
		$Obj = $OrderObj1->TableSelectOne(array("OrderID,ShippingCountry"));
		
		$TableName = TABLE_COUNTRIES." c 
					  LEFT JOIN ".TABLE_SHIPPING_COUNTRIES." sc on c.CountryID=sc.CountryID
					  LEFT JOIN ".TABLE_SHIPPING_RZS_GROUPS." sg on sg.ShippingID=sc.ShippingID
					";
		
		$ShippingGroupObj = new DataTable($TableName);
		$ShippingGroupObj->Where ="c.CountryISOCode1 = '".$ShippingGroupObj->MysqlEscapeString(substr($Obj->ShippingCountry,0,2))."'";
		$Obj = $ShippingGroupObj->TableSelectOne(array("sg.ShippingPrice"),"sg.ShippingPrice DESC");
		
		if(isset($Obj->ShippingPrice))
			$Price = isset($Obj->ShippingPrice)?$Obj->ShippingPrice:"";
	
	}
	$ResponseArray['title'] = "Standard";
	$ResponseArray['rate']["flat"] = array("code"=>"flat",
										  "title"=>"Flat",
										  "price"=>$Price,
										  "display_price"=>Change2CurrentCurrency($Price),
										  );
	return $ResponseArray; 
									  
}

									  
$ResponseArray  = GetFixedPriceResponse();

if(isset($ResponseArray) && is_array($ResponseArray) && count($ResponseArray) > 0 && !isset($ResponseArray['Error'])){
	$ShippingArray[$fixed_price] = $ResponseArray;
}
else{
	
}



?>