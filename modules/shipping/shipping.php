<?php 
require_once(dirname(__FILE__)."/../../includes/configure.php");

$ShippingArray = array();

if( isset($_POST['ShippingCountry']) && $_POST['ShippingCountry'] != "" )
	$_SESSION['TaxCountry'] = $_POST['ShippingCountry'];

	
$ShippingModuleObj = new DataTable(TABLE_MODULES);
$ShippingModuleObj->Where ="ModuleType='Shipping' AND Active='1'";
$ShippingModuleObj->TableSelectAll("","ModuleID ASC");
if($ShippingModuleObj->GetNumRows() >0)
{
	while($CurrentModule = $ShippingModuleObj->GetObjectFromRecord())
	{
		$file = pathinfo($CurrentModule->ModuleFile);
		if(file_exists(DIR_FS_SITE_SHIPPING.$file['filename']."/".$file['basename']))
		{
				require_once(DIR_FS_SITE_SHIPPING.$file['filename']."/".$file['basename']);
		}
	}
}
	
	
					
if(isset($_GET['type']) && strtolower($_GET['type'])=="html")
{
	

	if(isset($ShippingArray) && is_array($ShippingArray) && count($ShippingArray) > 0)
	{
		$_SESSION['ShippingArray'] = $ShippingArray;
		$shipping_method = isset($_SESSION['shipping_method'])?$_SESSION['shipping_method']:"";
		
		if(count($ShippingArray)==1)
		{
			
			foreach($ShippingArray as $k=>$arr)
			{
				?>
				<div class="shipping" id="div_shipping_<?php echo $k?>">
					<div style="min-height:30px"><?php echo $arr['title']?></div>
					<ul>
						<?php foreach($arr['rate'] as $code=>$r_arr):?>
						<li class="shipping_method_li" style="padding:0px;margin:0px;list-style:none">
						
								<input type="hidden" style="display:inline-block!important;min-height:0px!important;" class="radio" id="<?php echo $k?>__<?php echo $r_arr['code']?>" value="<?php echo $k?>__<?php echo $r_arr['code']?>" name="shipping_method" title="Choose your preferred shipping method">
								<?php echo isset($r_arr['title'])?$r_arr['title']:""?>(<span class="price"><?php echo isset($r_arr['display_price'])?$r_arr['display_price']:""?></span>)
							
							<?php if(isset($r_arr['note']) && $r_arr['note'] != ""):?>
								<div class="note"><?php echo $r_arr['note']?></div>
							<?php endif;?>
							<?php if(isset($r_arr['hidenote']) && $r_arr['hidenote'] != ""):?>
								<div><?php echo $r_arr['hidenote']?></div>
							<?php endif;?>
						</li>
						<?php endforeach;?>
						
					</ul>
				</div>	
				<?php
						
			}
			
		}
		else
		{	
			foreach($ShippingArray as $k=>$arr)
			{
				?>
				<div class="shipping" id="div_shipping_<?php echo $k?>">
					<div style="min-height:30px"><?php echo $arr['title']?></div>
					<ul>
						<?php foreach($arr['rate'] as $code=>$r_arr):?>
						<li class="shipping_method_li" style="padding:0px;margin:0px;list-style:none">
						
								<input <?php echo $shipping_method==$k.'__'.$r_arr['code']?'checked=true':''?> type="radio" style="display:inline-block!important;min-height:0px!important;" class="radio" id="<?php echo $k?>__<?php echo $r_arr['code']?>" value="<?php echo $k?>__<?php echo $r_arr['code']?>" name="shipping_method" title="Choose your preferred shipping method">
								<?php echo isset($r_arr['title'])?$r_arr['title']:""?>(<span class="price"><?php echo isset($r_arr['display_price'])?$r_arr['display_price']:""?></span>)
							
							<?php if(isset($r_arr['note']) && $r_arr['note'] != ""):?>
								<div class="note"><?php echo $r_arr['note']?></div>
							<?php endif;?>
							<?php if(isset($r_arr['hidenote']) && $r_arr['hidenote'] != ""):?>
								<div class="hidenote"><?php echo $r_arr['hidenote']?></div>
							<?php endif;?>
						</li>
						<?php endforeach;?>
						
					</ul>
				</div>	
				<?php
						
			}
		}
	}
}
if(isset($_GET['type']) && strtolower($_GET['type'])=="json")
{
		$_SESSION['ShippingArray'] = $ShippingArray;
		@ob_clean();
		header('Content-Type: application/json');
		echo json_encode($ShippingArray);
		exit;
}

?>