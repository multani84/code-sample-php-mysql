<?php 
if(__FILE__==$_SERVER['SCRIPT_FILENAME'])
	die('some error occurred.');

require_once (dirname(__FILE__).'/config.php');

function GetUSPSClassArray()
{
	return array('0_FCLE' => 'First-Class Mail Large Envelope',
                 '0_FCL'  => 'First-Class Mail Letter',
                 '0_FCP'  => 'First-Class Mail Parcel',
                 '0_FCPC' => 'First-Class Mail Postcards',
                 '1'      => 'Priority Mail',
                 '2'      => 'Priority Mail Express Hold For Pickup',
                 '3'      => 'Priority Mail Express',
                 '4'      => 'Standard Post',
                 '6'      => 'Media Mail Parcel',
                 '7'      => 'Library Mail Parcel',
                 '13'     => 'Priority Mail Express Flat Rate Envelope',
                 '15'     => 'First-Class Mail Large Postcards',
                 '16'     => 'Priority Mail Flat Rate Envelope',
                 '17'     => 'Priority Mail Medium Flat Rate Box',
                 '22'     => 'Priority Mail Large Flat Rate Box',
                 '23'     => 'Priority Mail Express Sunday/Holiday Delivery',
                 '25'     => 'Priority Mail Express Sunday/Holiday Delivery Flat Rate Envelope',
                 '27'     => 'Priority Mail Express Flat Rate Envelope Hold For Pickup',
                 '28'     => 'Priority Mail Small Flat Rate Box',
                 '29'     => 'Priority Mail Padded Flat Rate Envelope',
                 '30'     => 'Priority Mail Express Legal Flat Rate Envelope',
                 '31'     => 'Priority Mail Express Legal Flat Rate Envelope Hold For Pickup',
                 '32'     => 'Priority Mail Express Sunday/Holiday Delivery Legal Flat Rate Envelope',
                 '33'     => 'Priority Mail Hold For Pickup',
                 '34'     => 'Priority Mail Large Flat Rate Box Hold For Pickup',
                 '35'     => 'Priority Mail Medium Flat Rate Box Hold For Pickup',
                 '36'     => 'Priority Mail Small Flat Rate Box Hold For Pickup',
                 '37'     => 'Priority Mail Flat Rate Envelope Hold For Pickup',
                 '38'     => 'Priority Mail Gift Card Flat Rate Envelope',
                 '39'     => 'Priority Mail Gift Card Flat Rate Envelope Hold For Pickup',
                 '40'     => 'Priority Mail Window Flat Rate Envelope',
                 '41'     => 'Priority Mail Window Flat Rate Envelope Hold For Pickup',
                 '42'     => 'Priority Mail Small Flat Rate Envelope',
                 '43'     => 'Priority Mail Small Flat Rate Envelope Hold For Pickup',
                 '44'     => 'Priority Mail Legal Flat Rate Envelope',
                 '45'     => 'Priority Mail Legal Flat Rate Envelope Hold For Pickup',
                 '46'     => 'Priority Mail Padded Flat Rate Envelope Hold For Pickup',
                 '47'     => 'Priority Mail Regional Rate Box A',
                 '48'     => 'Priority Mail Regional Rate Box A Hold For Pickup',
                 '49'     => 'Priority Mail Regional Rate Box B',
                 '50'     => 'Priority Mail Regional Rate Box B Hold For Pickup',
                 '53'     => 'First-Class Package Service Hold For Pickup',
                 '55'     => 'Priority Mail Express Flat Rate Boxes',
                 '56'     => 'Priority Mail Express Flat Rate Boxes Hold For Pickup',
                 '57'     => 'Priority Mail Express Sunday/Holiday Delivery Flat Rate Boxes',
                 '58'     => 'Priority Mail Regional Rate Box C',
                 '59'     => 'Priority Mail Regional Rate Box C Hold For Pickup',
                 '61'     => 'First-Class Package Service',
                 '62'     => 'Priority Mail Express Padded Flat Rate Envelope',
                 '63'     => 'Priority Mail Express Padded Flat Rate Envelope Hold For Pickup',
                 '64'     => 'Priority Mail Express Sunday/Holiday Delivery Padded Flat Rate Envelope',
                 'INT_1'  => 'Priority Mail Express International',
                 'INT_2'  => 'Priority Mail International',
                 'INT_4'  => 'Global Express Guaranteed (GXG)',
                 'INT_5'  => 'Global Express Guaranteed Document',
                 'INT_6'  => 'Global Express Guaranteed Non-Document Rectangular',
                 'INT_7'  => 'Global Express Guaranteed Non-Document Non-Rectangular',
                 'INT_8'  => 'Priority Mail International Flat Rate Envelope',
                 'INT_9'  => 'Priority Mail International Medium Flat Rate Box',
                 'INT_10' => 'Priority Mail Express International Flat Rate Envelope',
                 'INT_11' => 'Priority Mail International Large Flat Rate Box',
                 'INT_12' => 'USPS GXG Envelopes',
                 'INT_13' => 'First-Class Mail International Letter',
                 'INT_14' => 'First-Class Mail International Large Envelope',
                 'INT_15' => 'First-Class Package International Service',
                 'INT_16' => 'Priority Mail International Small Flat Rate Box',
                 'INT_17' => 'Priority Mail Express International Legal Flat Rate Envelope',
                 'INT_18' => 'Priority Mail International Gift Card Flat Rate Envelope',
                 'INT_19' => 'Priority Mail International Window Flat Rate Envelope',
                 'INT_20' => 'Priority Mail International Small Flat Rate Envelope',
                 'INT_21' => 'First-Class Mail International Postcard',
                 'INT_22' => 'Priority Mail International Legal Flat Rate Envelope',
                 'INT_23' => 'Priority Mail International Padded Flat Rate Envelope',
                 'INT_24' => 'Priority Mail International DVD Flat Rate priced box',
                 'INT_25' => 'Priority Mail International Large Video Flat Rate priced box',
                 'INT_26' => 'Priority Mail Express International Flat Rate Boxes',
                 'INT_27' => 'Priority Mail Express International Padded Flat Rate Envelope',
			);
}

function GetUSPSQuoteDetails()
{
	
	$ResponseArray = array();
	$ResponseArray['date'] = date('Y-m-d');
	
	if( isset($_POST['ShippingCountry']) && $_POST['ShippingCountry'] != "" ){
		$ResponseArray['destination'] = array("PersonName"=>(isset($_POST['ShippingFirstName'])?$_POST['ShippingFirstName']:"").(isset($_POST['ShippingLastName'])?" ".$_POST['ShippingLastName']:""),
											  "CompanyName"=>(isset($_POST['CompanyName'])?$_POST['CompanyName']:""),
											  "PhoneNumber"=>(isset($_POST['ShippingPhone'])?$_POST['ShippingPhone']:""),
											  "StreetLines"=>(isset($_POST['ShippingAddress1'])?$_POST['ShippingAddress1']:"").(isset($_POST['ShippingAddress2'])?" ".$_POST['ShippingAddress2']:""),
											  "City"=>(isset($_POST['ShippingCity'])?$_POST['ShippingCity']:""),
											  "StateOrProvinceCode"=>(isset($_POST['ShippingState'])?$_POST['ShippingState']:""),
											  "PostalCode"=>(isset($_POST['ShippingZip'])?$_POST['ShippingZip']:""),
											  "CountryCode"=>(isset($_POST['ShippingCountry'])?$_POST['ShippingCountry']:""),
											  "Residential"=>true,
											  );
											 
											 
		
	}
	else if(isset($_SESSION['OrderID']) && $_SESSION['OrderID'] !=""){
		
	$OrderObj1 = new DataTable(TABLE_ORDERS);
	$OrderObj1->Where ="OrderID='".(int)$_SESSION['OrderID']."'";	
	$Obj = $OrderObj1->TableSelectOne(array("*"));
		if(isset($Obj->OrderID)){
		$ResponseArray['destination'] = array("PersonName"=>$Obj->ShippingFirstName." ".$Obj->ShippingLastName,
												  "CompanyName"=>(isset($Obj->CompanyName)?$Obj->CompanyName:""),
												  "PhoneNumber"=>(isset($Obj->ShippingPhone)?$Obj->ShippingPhone:""),
												  "StreetLines"=>$Obj->ShippingAddress1." ".$Obj->ShippingAddress2,
												  "City"=>(isset($Obj->ShippingCity)?$Obj->ShippingCity:""),
												  "StateOrProvinceCode"=>(isset($Obj->ShippingState)?$Obj->ShippingState:""),
												  "PostalCode"=>(isset($Obj->ShippingZip)?$Obj->ShippingZip:""),
												  "CountryCode"=>(isset($Obj->ShippingCountry)?$Obj->ShippingCountry:""),
												  "Residential"=>true,
												  );
		}
	
	}
	else{
		/* $ResponseArray['destination'] empty*/
		
	}
	
		
		
	$ResponseArray['origin'] = array("PersonName"=>"Jenet Krimpelbein",
										  "CompanyName"=>"Innerscent Beauty",
										  "PhoneNumber"=>"262-676-3107",
										  "StreetLines"=>"1614 76th St. Lower",
										  "City"=>"Kenosha",
										  "StateOrProvinceCode"=>"WI",
										  "PostalCode"=>"53143",
										  "CountryCode"=>"US",
										  );

		$TmpCartObj = new DataTable(TABLE_TMPCART);
		$TmpCartObj->Where =" SessionID='".session_id()."'";
		$DataArray = array();
		$DataArray[0]= "SUM(Weight*Qty) as WeightTotal, SUM(Price*Qty) as Total";
		$Obj = $TmpCartObj->TableSelectOne($DataArray);
				
	$ResponseArray['cart'] = array("price"=>$Obj->Total,
								   "pounds"=>UnitConverterFunc($Obj->WeightTotal,DEFINE_PRODUCT_WEIGHT_UNIT,"pound"),
								   "ounces"=>UnitConverterFunc($Obj->WeightTotal,DEFINE_PRODUCT_WEIGHT_UNIT,"ounce"),
								   "dimension"=>array("length"=>"15", /* inches*/
													  "width"=>"15",	/* inches*/
													  "height"=>"15",	/* inches*/
													  "unit"=>"15",	/* inches*/
													  ),
								   );
	return $ResponseArray; 
}

function GenerateUSPSXml()
{
			$Arr = GetUSPSQuoteDetails();
	
			
			if(isset($Arr['origin']['CountryCode']) && strtoupper($Arr['origin']['CountryCode'])== "US" && 
			   isset($Arr['destination']['CountryCode']) && strtoupper($Arr['destination']['CountryCode'])== "US" 
			   )
			{
				$xml  = '<RateV4Request USERID="' . MODULE_SHIPPING_USPS_USERID . '">';
				$xml .= '	<Package ID="1ST">';
				$xml .=	'		<Service>ALL</Service>';
				$xml .=	'		<ZipOrigination>' . substr((isset($Arr['origin']['PostalCode'])?$Arr['origin']['PostalCode']:""), 0, 5) . '</ZipOrigination>';
				$xml .=	'		<ZipDestination>' . substr((isset($Arr['destination']['PostalCode'])?$Arr['destination']['PostalCode']:""), 0, 5) . '</ZipDestination>';
				$xml .=	'		<Pounds>' . (isset($Arr['cart']['pounds'])?$Arr['cart']['pounds']:"") . '</Pounds>';
				$xml .=	'		<Ounces>' . (isset($Arr['cart']['ounces'])?$Arr['cart']['ounces']:"") . '</Ounces>';
				$xml .=	'		<Container>VARIABLE</Container>'; /*VARIABLE, FLAT RATE ENVELOPE, FLAT RATE BOX, RECTANGULAR, NONRECTANGULAR*/
				$xml .=	'		<Size>REGULAR</Size>'; /* REGULAR,LARGE*/
				
				
				//$xml .= '		<Width>' . $width . '</Width>';
				//$xml .= '		<Length>' . $length . '</Length>';
				//$xml .= '		<Height>' . $height . '</Height>';
				//$xml .= '		<Girth>' . (round(((float)$length) + (float)$width * 2 + (float)$height * 2), 1)) . '</Girth>';
				
				$xml .=	'		<Machinable>true</Machinable>';
				$xml .=	'	</Package>';
				$xml .= '</RateV4Request>';
			}
			else
			{
					//$Arr['destination']['CountryCode'];
				$country = array('BO' => 'Bolivia',
								 'BA' => 'Bosnia-Herzegovina',
								 'VG' => 'British Virgin Islands',
								 'MM' => 'Burma',
								 'CX' => 'Christmas Island (Australia)',
								 'CC' => 'Cocos Island (Australia)',
								 'CG' => 'Congo (Brazzaville),Republic of the',
								 'ZR' => 'Congo, Democratic Republic of the',
								 'CK' => 'Cook Islands (New Zealand)',
								 'CI' => 'Cote d\'Ivoire (Ivory Coast)',
								 'TP' => 'East Timor (Indonesia)',
								 'FK' => 'Falkland Islands',
								 'GE' => 'Georgia, Republic of',
								 'YT' => 'Mayotte (France)',
								 'MD' => 'Moldova',
								 'MC' => 'Monaco (France)',
								 'AN' => 'Netherlands Antilles',
								 'KP' => 'North Korea (Korea, Democratic People\'s Republic of)',
								 'SH' => 'Saint Helena',
								 'KN' => 'Saint Kitts (St. Christopher and Nevis)',
								 'PM' => 'Saint Pierre and Miquelon',
								 'VC' => 'Saint Vincent and the Grenadines',
								 'YU' => 'Serbia-Montenegro',
								 'SK' => 'Slovak Republic',
								 'GS' => 'South Georgia (Falkland Islands)',
								 'KR' => 'South Korea (Korea, Republic of)',
								 'TK' => 'Tokelau (Union) Group (Western Samoa)',
								 'WF' => 'Wallis and Futuna Islands',
								 'GB'=>"Great Britain and Northern Ireland",
								 'UK'=>"Great Britain and Northern Ireland",
								 );
				
				if(isset($country[$Arr['destination']['CountryCode']]) && $country[$Arr['destination']['CountryCode']] != "")
					$CountryName = $country[$Arr['destination']['CountryCode']];
				else
					$CountryName = SKGetCountryNameByCode($Arr['destination']['CountryCode'],2);
				
				$xml  = '<IntlRateV2Request USERID="' . MODULE_SHIPPING_USPS_USERID . '">';
				$xml .= '	<Revision>2</Revision>';
				$xml .=	'	<Package ID="1">';
				$xml .=	'		<Pounds>' . (isset($Arr['cart']['pounds'])?$Arr['cart']['pounds']:"") . '</Pounds>';
				$xml .=	'		<Ounces>' . (isset($Arr['cart']['ounces'])?$Arr['cart']['ounces']:"") . '</Ounces>';
				$xml .=	'		<MailType>All</MailType>';
				$xml .=	'		<GXG>';
				$xml .=	'		  <POBoxFlag>N</POBoxFlag>';
				$xml .=	'		  <GiftFlag>N</GiftFlag>';
				$xml .=	'		</GXG>';
				$xml .=	'		<ValueOfContents>' .  (isset($Arr['cart']['price'])?$Arr['cart']['price']:"") . '</ValueOfContents>';
				$xml .=	'		<Country>' . $CountryName . '</Country>';
				$xml .=	'		<Container>RECTANGULAR</Container>'; /* RECTANGULAR, NONRECTANGULAR*/
				$xml .=	'		<Size>REGULAR</Size>'; /* REGULAR,LARGE*/
				
				$width = (isset($Arr['cart']['dimension']['width'])?$Arr['cart']['dimension']['width']:"");
				$length = (isset($Arr['cart']['dimension']['length'])?$Arr['cart']['dimension']['length']:"");
				$height = (isset($Arr['cart']['dimension']['height'])?$Arr['cart']['dimension']['height']:"");
				$xml .= '		<Width>' . $width . '</Width>';
				$xml .= '		<Length>' . $length . '</Length>';
				$xml .= '		<Height>' . $height . '</Height>';
				$xml .= '		<Girth>' . number_format(((float)$length) + (float)$width * 2 + (float)$height * 2) . '</Girth>';
				
				$xml .= '		<OriginZip>' . substr((isset($Arr['origin']['PostalCode'])?$Arr['origin']['PostalCode']:""), 0, 5) . '</OriginZip>';
				$xml .= '		<CommercialFlag>N</CommercialFlag>';
				$xml .=	'	</Package>';
				$xml .=	'</IntlRateV2Request>';
					
			}
			
			
			
	
		return $xml;	
			
}


function GetUSPSResponse()
{
	
	$xml = GenerateUSPSXml();
	$response = GetUSPSLog();
	
	if($response===false)
	{
		if(strstr($xml,"IntlRateV2Request"))
			$query = 'API=IntlRateV2&XML=' . urlencode($xml);
		else
			$query = 'API=RateV4&XML=' . urlencode($xml);
		
		//echo MODULE_SHIPPING_USPS_GATEWAY_URL."?".$query;
		//exit;
		$curl = curl_init(MODULE_SHIPPING_USPS_GATEWAY_URL."?".$query);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_HEADER, false);
		curl_setopt($curl, CURLOPT_TIMEOUT, 30);
		$response = curl_exec($curl);
		curl_close($curl);
	}
	
	return ParseUSPSResponse($response);
	
	
}

function ParseUSPSResponse($content)
{
	$ResponseArray = array();
	
	try
	{
		//echo $content;exit;
		if(strstr($content,"IntlRateV2Response"))
		{
			$dom = new DOMDocument('1.0', 'UTF-8');
			@$dom->loadXml($content);
			if($dom->getElementsByTagName('Service')->length > 0){
				
				$Service = $dom->getElementsByTagName('Service');
				
				
				SetUSPSLog($content); /* cache set */
				foreach ($Service as $rate) {
					$code = $rate->getAttribute('ID');
					$allow_array = GetUSPSAllowMethods();
					$title = $rate->getElementsByTagName('SvcDescription')->item(0)->nodeValue;
					$price = $rate->getElementsByTagName('Postage')->item(0)->nodeValue;
					$currency = "USD";
					
					//if(in_array($code,$allow_array))
					{
						$ResponseArray['rate'][$code] = array("code"=>$code,
															  "title"=>strip_tags(html_entity_decode($title)),
															  "price"=>$price,
															  "currency"=>$currency,
															  "display_price"=>$price
															  );
					}
				}
				
				$price = array();
					foreach ($ResponseArray['rate'] as $key => $row)
						$price[$key] = $row['price'];
					array_multisort($price, SORT_ASC, $ResponseArray['rate']);
					$ResponseArray['title'] = "USPS";
					
			}
			else
			{
				$ResponseArray['Error'] = "ERROR: ";
			}
			
			
		}
		else
		{
			$dom = new DOMDocument('1.0', 'UTF-8');
			@$dom->loadXml($content);
			
			
			if($dom->getElementsByTagName('Postage')->length > 0){
				$Postage = $dom->getElementsByTagName('Postage');
				
				SetUSPSLog($content); /* cache set */
				
				foreach ($Postage as $rate) {
					$code = $rate->getAttribute('CLASSID');
					$allow_array = GetUSPSAllowMethods();
					$title = $rate->getElementsByTagName('MailService')->item(0)->nodeValue;
					$price = $rate->getElementsByTagName('Rate')->item(0)->nodeValue;
					$currency = "USD";
					
					if(in_array($code,$allow_array))
					{
						$ResponseArray['rate'][$code] = array("code"=>$code,
															  "title"=>strip_tags(html_entity_decode($title)),
															  "price"=>$price,
															  "currency"=>$currency,
															  "display_price"=>$price
															  );

															  
					}
					
					
					
				}
				
				//var_dump($ResponseArray['rate']);exit;
				$price = array();
				foreach ($ResponseArray['rate'] as $key => $row)
					$price[$key] = $row['price'];
				array_multisort($price, SORT_ASC, $ResponseArray['rate']);

				$ResponseArray['title'] = "USPS";
				
			}
			else{
				$ResponseArray['Error'] = "ERROR: ";
			}
		}
	}
	catch(Exception $e)
	{
		//var_dump($e);exit;
	}
	
	
	return $ResponseArray;
}

function GetUSPSAllowMethods()
{
	$Arr = GetUSPSQuoteDetails();
	if(isset($Arr['destination']['CountryCode']) && strtoupper($Arr['destination']['CountryCode'])=="US")
		return array('0_FCLE','0_FCL','0_FCP','0_FCP','0_FCPC','0','1','2','3','4','5','6','7','12','13','16','17','18','19','22','23','25','27','28');
	else
		return array('1','2','3','4','5','6','7','8','9','10','11','12','13','16','21');
}

function GetUSPSLog()
{
	$Arr = GetUSPSQuoteDetails();
	
	$Return = false;
	global $usps_code;
	$filename = md5(serialize($Arr));
	$File = DIR_FS_SITE_PRIVATE.$usps_code."/".$filename;
	if(file_exists ($File))
	{
		return file_get_contents($File);
	}
	
	return $Return;
}
function SetUSPSLog($content)
{
	$Arr = GetUSPSQuoteDetails();
	
	global $usps_code;
	$filename = md5(serialize($Arr));
	$Dir = DIR_FS_SITE_PRIVATE.$usps_code."/";
	$File = DIR_FS_SITE_PRIVATE.$usps_code."/".$filename;
	if(file_exists ($Dir ))
	{
		if(file_exists ($File)== false)
		{
			$fp = fopen($File,"w");
			fputs($fp,$content);
			fclose($fp);	
		}
		
	}
	else
	{
		if (mkdir($Dir, 0777, true)) 
		{
			if(file_exists ($File)== false)
			{
				$fp = fopen($File,"w");
				fputs($fp,$content);
				fclose($fp);	
			}
		}
	}
	
}

function ClearUSPSLog()
{
	global $usps_code;
	$Dir = DIR_FS_SITE_PRIVATE.$usps_code."/";
	if(file_exists ($Dir ))
	{
		if(function_exists("DeleteFullFolder"))
			DeleteFullFolder($Dir);
	}
}



$ResponseArray  = GetUSPSResponse();
if(isset($ResponseArray) && is_array($ResponseArray) && count($ResponseArray) > 0 && !isset($ResponseArray['Error'])){
	$ShippingArray[$usps_code] = $ResponseArray;
}
else{
	
}



?>