<?php 
if(__FILE__==$_SERVER['SCRIPT_FILENAME'])
	die('some error occurred.');
require_once (dirname(__FILE__).'/config.php');


function GetQuoteDetails()
{
	$Array = array();
}

function GenerateFedexXml()
{
	
	$fedex_owner_name = "Jenet Krimpelbein";
	$fedex_owner_company = "Innerscent Beauty";
	$fedex_owner_phone = "262-676-3107";
	$fedex_owner_address1 = "1614 76th St. Lower";
	$fedex_owner_address2 = "";
	$fedex_owner_city = "Harrison";
	$fedex_owner_state = "AR";
	$fedex_owner_country = "US";
	$fedex_owner_postcode = "72601";

			$xml  = '<?xml version="1.0"?>';
			$xml .= '<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns1="http://fedex.com/ws/rate/v10">';
			$xml .= '	<SOAP-ENV:Body>';
			$xml .= '		<ns1:RateRequest>';
			$xml .= '			<ns1:WebAuthenticationDetail>';
			$xml .= '				<ns1:UserCredential>';
			$xml .= '					<ns1:Key>' . MODULE_SHIPPING_FEDEX_KEY . '</ns1:Key>';
			$xml .= '					<ns1:Password>' . MODULE_SHIPPING_FEDEX_PASSWORD . '</ns1:Password>';
			$xml .= '				</ns1:UserCredential>';
			$xml .= '			</ns1:WebAuthenticationDetail>';
			$xml .= '			<ns1:ClientDetail>';
			$xml .= '				<ns1:AccountNumber>' . MODULE_SHIPPING_FEDEX_ACCOUNT_NUMBER . '</ns1:AccountNumber>';
			$xml .= '				<ns1:MeterNumber>' . MODULE_SHIPPING_FEDEX_METER_NUMBER. '</ns1:MeterNumber>';
			$xml .= '			</ns1:ClientDetail>';
			$xml .= '			<ns1:Version>';
			$xml .= '				<ns1:ServiceId>crs</ns1:ServiceId>';
			$xml .= '				<ns1:Major>10</ns1:Major>';
			$xml .= '				<ns1:Intermediate>0</ns1:Intermediate>';
			$xml .= '				<ns1:Minor>0</ns1:Minor>';
			$xml .= '			</ns1:Version>';
			$xml .= '			<ns1:ReturnTransitAndCommit>true</ns1:ReturnTransitAndCommit>';
			$xml .= '			<ns1:RequestedShipment>';
			$xml .= '				<ns1:ShipTimestamp>' . date('c', time()) . '</ns1:ShipTimestamp>';
			$xml .= '				<ns1:DropoffType>REGULAR_PICKUP</ns1:DropoffType>'; /*REGULAR_PICKUP, REQUEST_COURIER, DROP_BOX, BUSINESS_SERVICE_CENTER, STATION*/
			$xml .= '				<ns1:PackagingType>YOUR_PACKAGING</ns1:PackagingType>';/*FEDEX_ENVELOPE, FEDEX_PAK, FEDEX_BOX, FEDEX_TUBE, FEDEX_10KG_BOX, FEDEX_25KG_BOX, YOUR_PACKAGING*/
			$xml .= '				<ns1:Shipper>';
			$xml .= '					<ns1:Contact>';
			$xml .= '						<ns1:PersonName>' .$fedex_owner_name. '</ns1:PersonName>';
			$xml .= '						<ns1:CompanyName>' .$fedex_owner_company. '</ns1:CompanyName>';
			$xml .= '						<ns1:PhoneNumber>' .$fedex_owner_phone. '</ns1:PhoneNumber>';
			$xml .= '					</ns1:Contact>';
			$xml .= '					<ns1:Address>';
			$xml .= '						<ns1:StateOrProvinceCode>' .$fedex_owner_state. '</ns1:StateOrProvinceCode>';
			$xml .= '						<ns1:PostalCode>' .$fedex_owner_postcode. '</ns1:PostalCode>';
			$xml .= '						<ns1:CountryCode>' .$fedex_owner_country. '</ns1:CountryCode>';
			$xml .= '					</ns1:Address>';
			$xml .= '				</ns1:Shipper>';

			$xml .= '				<ns1:Recipient>';
			$xml .= '					<ns1:Contact>';
			$xml .= '						<ns1:PersonName>John Max</ns1:PersonName>';
			$xml .= '						<ns1:CompanyName>Test</ns1:CompanyName>';
			$xml .= '						<ns1:PhoneNumber>262-412-4484</ns1:PhoneNumber>';
			$xml .= '					</ns1:Contact>';
			$xml .= '					<ns1:Address>';
			$xml .= '						<ns1:StreetLines>Lake Forest</ns1:StreetLines>';
			$xml .= '						<ns1:City>Lake Forest</ns1:City>';
			$xml .= '						<ns1:StateOrProvinceCode>CA</ns1:StateOrProvinceCode>';
			$xml .= '						<ns1:PostalCode>92630</ns1:PostalCode>';
			$xml .= '						<ns1:CountryCode>US</ns1:CountryCode>';
			$xml .= '						<ns1:Residential>true</ns1:Residential>';
			$xml .= '					</ns1:Address>';
			$xml .= '				</ns1:Recipient>';
			$xml .= '				<ns1:ShippingChargesPayment>';
			$xml .= '					<ns1:PaymentType>SENDER</ns1:PaymentType>';
			$xml .= '					<ns1:Payor>';
			$xml .= '						<ns1:AccountNumber>' . MODULE_SHIPPING_FEDEX_ACCOUNT_NUMBER. '</ns1:AccountNumber>';
			$xml .= '						<ns1:CountryCode>US</ns1:CountryCode>';
			$xml .= '					</ns1:Payor>';
			$xml .= '				</ns1:ShippingChargesPayment>';
			$xml .= '				<ns1:RateRequestTypes>LIST</ns1:RateRequestTypes>';
			$xml .= '				<ns1:PackageCount>1</ns1:PackageCount>';
			$xml .= '				<ns1:RequestedPackageLineItems>';
			$xml .= '					<ns1:SequenceNumber>1</ns1:SequenceNumber>';
			$xml .= '					<ns1:GroupPackageCount>1</ns1:GroupPackageCount>';
			$xml .= '					<ns1:Weight>';
			$xml .= '						<ns1:Units>KG</ns1:Units>'; /* KG, LB */
			$xml .= '						<ns1:Value>2</ns1:Value>';
			$xml .= '					</ns1:Weight>';
			//$xml .= '					<ns1:Dimensions>';
			//$xml .= '						<ns1:Length>' . $this->config->get('fedex_length') . '</ns1:Length>';
			//$xml .= '						<ns1:Width>' . $this->config->get('fedex_width') . '</ns1:Width>';
			//$xml .= '						<ns1:Height>' . $this->config->get('fedex_height') . '</ns1:Height>';
			//$xml .= '						<ns1:Units>' . strtoupper($this->length->getUnit($this->config->get('fedex_length_class_id'))) . '</ns1:Units>';
			//$xml .= '					</ns1:Dimensions>';
			$xml .= '				</ns1:RequestedPackageLineItems>';
			$xml .= '			</ns1:RequestedShipment>';
			$xml .= '		</ns1:RateRequest>';
			$xml .= '	</SOAP-ENV:Body>';
			$xml .= '</SOAP-ENV:Envelope>';
			
		return $xml;	
			
}

function GetResponseFromFedex()
{
	
	//return ParseResponseFromFedex("");
	
	$xml = GenerateFedexXml();
	echo($xml);
	exit;
	
	$curl = curl_init(MODULE_SHIPPING_FEDEX_URL);
	
	curl_setopt($curl, CURLOPT_POST, true);
	curl_setopt($curl, CURLOPT_POSTFIELDS, $xml);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($curl, CURLOPT_HEADER, false);
	curl_setopt($curl, CURLOPT_TIMEOUT, 30);
	curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
	$response = curl_exec($curl);
	curl_close($curl);
	
	return ParseResponseFromFedex($response);
	
}

function ParseResponseFromFedex($content)
{
	//$content = file_get_contents(dirname(__FILE__).'/response.xml');
	
	
	$ResponseArray = array();
	$dom = new DOMDocument('1.0', 'UTF-8');
	$dom->loadXml($content);
	
	if($dom->getElementsByTagName('RateReplyDetails')->length > 0){
		$RateReplyDetails = $dom->getElementsByTagName('RateReplyDetails');
		
		foreach ($RateReplyDetails as $rate) {
			$code = $rate->getElementsByTagName('ServiceType')->item(0)->nodeValue;
			$title = $rate->getElementsByTagName('ServiceType')->item(0)->nodeValue;
			$price = $rate->getElementsByTagName('ShipmentRateDetail')->item(0)
						 ->getElementsByTagName('TotalNetChargeWithDutiesAndTaxes')->item(0)
						 ->getElementsByTagName('Amount')->item(0)
						 ->nodeValue;
			$currency = $rate->getElementsByTagName('ShipmentRateDetail')->item(0)
						 ->getElementsByTagName('TotalNetChargeWithDutiesAndTaxes')->item(0)
						 ->getElementsByTagName('Currency')->item(0)
						 ->nodeValue;
			
			
			$ResponseArray['rate'][$code] = array("code"=>$code,
												  "title"=>$title,
												  "price"=>$price,
												  "currency"=>$currency,
												  "display_price"=>$price
												  );
			
			
			
		}
		$ResponseArray['title'] = "FedEx";
		
	}
	else{
		$ResponseArray['Error'] = "ERROR: ";
	}
	
	return $ResponseArray;
}

$ResponseArray  = GetResponseFromFedex();
if(isset($ResponseArray) && is_array($ResponseArray) && count($ResponseArray) > 0 && !isset($ResponseArray['Error'])){
	$ShippingArray[$fedex_code] = $ResponseArray;
}
else{
	
}



?>