<?
if(file_exists(DIR_FS_SITE_MODULES."shipping/".basename(__FILE__)))
@include_once(DIR_FS_SITE_MODULES."shipping/".basename(__FILE__));


$Shipping = isset($_GET['Shipping'])?$_GET['Shipping']:"";
$Section = isset($_GET['Section'])?$_GET['Section']:"";
$Target = isset($_GET['Target'])?$_GET['Target']:"";

$ShippingID = isset($_GET['ShippingID'])?$_GET['ShippingID']:0;

$ShippingZoneObj = new DataTable(TABLE_SHIPPING_ZONES);
$ShippingCountryNameObj = new DataTable(TABLE_COUNTRIES);
$ShippingRangeObj = new DataTable(TABLE_SHIPPING_RANGES);
$ShippingRangeObj2 = new DataTable(TABLE_SHIPPING_RANGES);
$DataArray = array();			

if($ShippingID !=0)
{
	$ShippingZoneObj->Where ="ShippingID = '".$ShippingZoneObj->MysqlEscapeString($ShippingID)."'";
	$CurrentShipping = $ShippingZoneObj->TableSelectOne();

			$ShippingRangeObj->Where = "ShippingID='".$ShippingRangeObj->MysqlEscapeString($ShippingID)."'";
			$ShippingRangeObj->TableSelectAll("","ShippingRangeID ASC");
			$ShippingRangeRows = $ShippingRangeObj->GetNumRows();
}

switch ($Target)
{
	
	case "DeleteShipping":
		if($ShippingID !="1")
		{
			$ShippingCountryNameObj->Where = $ShippingZoneObj->Where = "ShippingID='".$ShippingZoneObj->MysqlEscapeString($ShippingID)."'";
			$ShippingZoneObj->TableDelete();	
			$ShippingCountryNameObj->TableUpdate(array("ShippingID"=>"1"));	
			
		
			
			@ob_clean();
			
			$_SESSION['InfoMessage'] ="Shipping Zone deleted successfully.";
			MyRedirect(DIR_WS_SITE_CONTROL."index.php?Page=$Page&Shipping=$Shipping&Section=$Section&QuotationID=$QuotationID");
			exit;
		}	
		break;
	case "AddShipping":
		if(isset($_POST['ChangeOption']))
				break;
		$DataArray = array();			
		$DataArray['ShippingZone'] = isset($_POST['ShippingZone'])?$_POST['ShippingZone']:"";			
		$DataArray['NormalPrice'] = (isset($_POST['NormalPrice']) && $_POST['NormalPrice'] !="")?$_POST['NormalPrice']:"0";
		$DataArray['FirstClassPrice'] = (isset($_POST['FirstClassPrice']) && $_POST['FirstClassPrice'] !="")?$_POST['FirstClassPrice']:"0";
		$DataArray['FirstClassActive'] = (isset($_POST['FirstClassActive']) && $_POST['FirstClassActive'] !="")?$_POST['FirstClassActive']:"0";
		
		$DataArray['FreeShippingActive'] = (isset($_POST['FreeShippingActive']) && $_POST['FreeShippingActive'] !="")?$_POST['FreeShippingActive']:"0";
		$DataArray['MinFreeShipping'] = (isset($_POST['MinFreeShipping']) && $_POST['MinFreeShipping'] !="")?$_POST['MinFreeShipping']:"0";
		
		$DataArray['ExtraShipping'] = (isset($_POST['ExtraShipping']) && $_POST['ExtraShipping'] !="")?$_POST['ExtraShipping']:"0";
		if($ShippingID=="0")
		{
			$ShippingID = $ShippingZoneObj->GetMax("ShippingID") + 1;
			$DataArray['ShippingID'] = $ShippingID;
			$DataArray['CreatedDate'] = date('Ymd');
			$ShippingZoneObj->TableInsert($DataArray);
			for($i=1;$i<=$_POST['NoOfOption'];$i++)
		{
			$DataSubArray =array();
			$DataSubArray['RangeName'] = (isset($_POST['RangeName'.$i]) && $_POST['RangeName'.$i] !="")?$_POST['RangeName'.$i]:"0";
			$DataSubArray['NormalPrice'] = (isset($_POST['NormalPrice'.$i]) && $_POST['NormalPrice'.$i] !="")?$_POST['NormalPrice'.$i]:"0";
			$DataSubArray['OSPrice'] = (isset($_POST['OSPrice'.$i]) && $_POST['OSPrice'.$i] !="")?$_POST['OSPrice'.$i]:"0";
			$DataSubArray['OWPrice'] = (isset($_POST['OWPrice'.$i]) && $_POST['OWPrice'.$i] !="")?$_POST['OWPrice'.$i]:"0";
			$DataSubArray['ExpressPrice'] = (isset($_POST['ExpressPrice'.$i]) && $_POST['ExpressPrice'.$i] !="")?$_POST['ExpressPrice'.$i]:"0";
			
			$DataSubArray['ShippingID'] =  $ShippingID;			
			
			$ShippingRangeObj->TableInsert($DataSubArray);
		}
			@ob_clean();
			
			$_SESSION['InfoMessage'] ="Shipping zone added successfully.";
		}
		else 
		{
				$ShippingZoneObj->Where ="ShippingID = '".$ShippingZoneObj->MysqlEscapeString($ShippingID)."'";
				$ShippingZoneObj->TableUpdate($DataArray);
				########### ShippingRange Value End ############# 
			if($_POST['NoOfOption'] >= $ShippingRangeRows)
			{
				/////////////// Update ShippingRange Value Start
				
				for($i=1;$i<=$ShippingRangeRows;$i++)
				{
					$DataSubArray =array();
					$ShippingRangeID = isset($_POST['ShippingRangeID'.$i])?$_POST['ShippingRangeID'.$i]:"";
				
					$DataSubArray['RangeName'] = (isset($_POST['RangeName'.$i]) && $_POST['RangeName'.$i] !="")?$_POST['RangeName'.$i]:"0";
					$DataSubArray['RangeMin'] = (isset($_POST['RangeMin'.$i]) && $_POST['RangeMin'.$i] !="")?$_POST['RangeMin'.$i]:"0";
					$DataSubArray['RangeMax'] = (isset($_POST['RangeMax'.$i]) && $_POST['RangeMax'.$i] !="")?$_POST['RangeMax'.$i]:"0";
					$DataSubArray['NormalPrice'] = (isset($_POST['NormalPrice'.$i]) && $_POST['NormalPrice'.$i] !="")?$_POST['NormalPrice'.$i]:"0";
					$DataSubArray['OSPrice'] = (isset($_POST['OSPrice'.$i]) && $_POST['OSPrice'.$i] !="")?$_POST['OSPrice'.$i]:"0";
					$DataSubArray['OWPrice'] = (isset($_POST['OWPrice'.$i]) && $_POST['OWPrice'.$i] !="")?$_POST['OWPrice'.$i]:"0";
					$DataSubArray['ExpressPrice'] = (isset($_POST['ExpressPrice'.$i]) && $_POST['ExpressPrice'.$i] !="")?$_POST['ExpressPrice'.$i]:"0";
					$ShippingRangeObj2->Where = "ShippingRangeID='".$ShippingRangeID."'";
					
					$ShippingRangeObj2->TableUpdate($DataSubArray);		
			
				} 
				/////////////// Update ShippingRange Value End
				
				/////////////// Add New ShippingRange Value Start
				for($i=$ShippingRangeRows+1;$i<=$_POST['NoOfOption'];$i++)
				{
					$DataSubArray =array();
					$DataSubArray['RangeMin'] = (isset($_POST['RangeMin'.$i]) && $_POST['RangeMin'.$i] !="")?$_POST['RangeMin'.$i]:"0";
					$DataSubArray['RangeMax'] = (isset($_POST['RangeMax'.$i]) && $_POST['RangeMax'.$i] !="")?$_POST['RangeMax'.$i]:"0";
					$DataSubArray['NormalPrice'] = (isset($_POST['NormalPrice'.$i]) && $_POST['NormalPrice'.$i] !="")?$_POST['NormalPrice'.$i]:"0";
					$DataSubArray['OSPrice'] = (isset($_POST['OSPrice'.$i]) && $_POST['OSPrice'.$i] !="")?$_POST['OSPrice'.$i]:"0";
					$DataSubArray['OWPrice'] = (isset($_POST['OWPrice'.$i]) && $_POST['OWPrice'.$i] !="")?$_POST['OWPrice'.$i]:"0";
					$DataSubArray['ExpressPrice'] = (isset($_POST['ExpressPrice'.$i]) && $_POST['ExpressPrice'.$i] !="")?$_POST['ExpressPrice'.$i]:"0";
					$DataSubArray['ShippingID'] =  $ShippingID;			
					$DataSubArray['RangeName'] = (isset($_POST['RangeName'.$i]) && $_POST['RangeName'.$i] !="")?$_POST['RangeName'.$i]:"0";
					$ShippingRangeObj2->TableInsert($DataSubArray);		
				}
				/////////////// Add New ShippingRange Value End
				 
			}
			else 
			{
				for($i=1;$i<=$_POST['NoOfOption'];$i++)
				{
					$CurrentShippingRange = $ShippingRangeObj->GetObjectFromRecord();
					
					$DataSubArray =array();
					$ShippingRangeID = isset($_POST['ShippingRangeID'.$i])?$_POST['ShippingRangeID'.$i]:"";
					$DataSubArray['RangeMin'] = (isset($_POST['RangeMin'.$i]) && $_POST['RangeMin'.$i] !="")?$_POST['RangeMin'.$i]:"0";
					$DataSubArray['RangeMax'] = (isset($_POST['RangeMax'.$i]) && $_POST['RangeMax'.$i] !="")?$_POST['RangeMax'.$i]:"0";
					$DataSubArray['NormalPrice'] = (isset($_POST['NormalPrice'.$i]) && $_POST['NormalPrice'.$i] !="")?$_POST['NormalPrice'.$i]:"0";
					$DataSubArray['OSPrice'] = (isset($_POST['OSPrice'.$i]) && $_POST['OSPrice'.$i] !="")?$_POST['OSPrice'.$i]:"0";
					$DataSubArray['OWPrice'] = (isset($_POST['OWPrice'.$i]) && $_POST['OWPrice'.$i] !="")?$_POST['OWPrice'.$i]:"0";
					$DataSubArray['RangeName'] = (isset($_POST['RangeName'.$i]) && $_POST['RangeName'.$i] !="")?$_POST['RangeName'.$i]:"0";
					$DataSubArray['ExpressPrice'] = (isset($_POST['ExpressPrice'.$i]) && $_POST['ExpressPrice'.$i] !="")?$_POST['ExpressPrice'.$i]:"0";
					$ShippingRangeObj2->Where = "ShippingRangeID='".$ShippingRangeID."'";
					$ShippingRangeObj2->TableUpdate($DataSubArray);		
				}
				for($i = $_POST['NoOfOption']+1;$i<=$ShippingRangeRows;$i++)
				{
					$CurrentShippingRange = $ShippingRangeObj->GetObjectFromRecord();
					
					$ShippingRangeObj2->Where = "ShippingRangeID='".$CurrentShippingRange->ShippingRangeID."'";
					$ShippingRangeObj2->TableDelete();		
				}	
			}
			########### ShippingRange Value End ############# 	
		
			
			$ShippingZoneObj->Where ="ShippingID = '".$ShippingZoneObj->MysqlEscapeString($ShippingID)."'";
			
			$ShippingZoneObj->TableUpdate($DataArray);
		
		}
		
		foreach ($_POST['CountryNameGo'] as $k=>$v)
		{
				$ShippingCountryNameObj->Where = "CountryID='".$ShippingCountryNameObj->MysqlEscapeString($v)."'";
				$ShippingCountryNameObj->TableUpdate(array("ShippingID"=>$ShippingID));
		}
					
		MyRedirect(DIR_WS_SITE_CONTROL."index.php?Page=$Page&Shipping=$Shipping&ShippingID=$ShippingID");
		exit;	
				
	break;
}

?>