<table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr>
		<td height="20" align="center">
			<h4>Shipping</h4>
		</td>
	</tr>
	<tr>
		<td height="20" align="right">&nbsp;&nbsp;
		<a href="<?=DIR_WS_SITE_CONTROL?>index.php?Page=<?=$Page?>&Shipping=<?=$Shipping?>&Section=AddShipping" class="adm-link">Add New Shipping Zone</a>
		</td>		
	</tr>	
</table><?
switch ($Section)
{
	case "AddShipping":
	case "EditShipping":
		$NoOfOption = isset($_POST['NoOfOption'])?$_POST['NoOfOption']:(isset($ShippingRangeRows)?$ShippingRangeRows:0);
		$CountryNameArray = array();
		$ShippingCountryNameObj->Where ="1";
		$ShippingCountryNameObj->TableSelectAll(array("CountryID","CountryName"),"CountryName ASC");
		while($CountryName = $ShippingCountryNameObj->GetObjectFromRecord())
		{
			$CountryNameArray[$CountryName->CountryID] = $CountryName->CountryName;
		}
		
		$CountryNameGoArray = array();
		$CountryNameInArray = array();
		if(isset($_POST['CountryNameGo']) && is_array($_POST['CountryNameGo']))
		{	
			foreach ($_POST['CountryNameGo'] as $k=>$v)
				$CountryNameGoArray[$v] = $CountryNameArray[$v];
			
		}
		else 
		{
			$ShippingCountryNameObj->Where ="ShippingID='$ShippingID'";
			$ShippingCountryNameObj->TableSelectAll(array("CountryID","CountryName"),"CountryName ASC");
			while($CountryNameGo = $ShippingCountryNameObj->GetObjectFromRecord())
				$CountryNameGoArray[$CountryNameGo->CountryID] = $CountryNameArray[$CountryNameGo->CountryID];
		}
		$CountryNameInArray = array_diff($CountryNameArray,$CountryNameGoArray);
	
	?>
		<form method="POST" action="<?=DIR_WS_SITE_CONTROL?>index.php?Page=<?=$Page?>&Shipping=<?=$Shipping?>&Section=<?=$Section?>&Target=AddShipping&ShippingID=<?=$ShippingID?>" enctype="multipart/form-data">
		<table cellpadding="3" cellspacing="2" width="100%" class="InsideTable">
			<tr>
				<td align="center" class="InsideLeftTd"><b>Shipping Zone Name</b></td>
				<td align="left" class="InsideRightTd"><input type="text" name="ShippingZone" value="<?=isset($_POST['ShippingZone'])?MyStripSlashes($_POST['ShippingZone']):(isset($CurrentShipping->ShippingZone)?MyStripSlashes($CurrentShipping->ShippingZone):"")?>" size="70"></td>
			</tr>
		
			<tr>
				<td align="center" class="InsideLeftTd"><b>Free Delivery</b></td>
				<td align="left" class="InsideRightTd">
				<input type="checkbox" name="FreeShippingActive" value="1" class="chk" <?=(isset($_POST['FreeShippingActive']) && $_POST['FreeShippingActive']==1)?"checked":((isset($CurrentShipping->FreeShippingActive) && $CurrentShipping->FreeShippingActive==1)?"checked":"")?>>Enable&nbsp;&nbsp;
				&nbsp;&nbsp;&nbsp;&nbsp;
				<b>Minimum Order</b>:<input type="text" name="MinFreeShipping" value="<?=isset($_POST['MinFreeShipping'])?MyStripSlashes($_POST['MinFreeShipping']):(isset($CurrentShipping->MinFreeShipping)?MyStripSlashes($CurrentShipping->MinFreeShipping):"")?>" size="10">
				</td>
			</tr>
			<tr valign=top>
          		<td height="10" colspan="2" align="center"><hr class="InsideTable"></td>
          	</tr>
          	<tr>
				<td align="left" colspan="2"  class="InsideRightTd">
					<table border="0" cellpadding="3" cellspacing="1" width="100%" class="InsideTable">
							<tr>
							<td height="40" align="center">&nbsp;</td>
							<td width="30%"><b>Countries IN</b></td>
							<td width="10%"></td>
							<td  width="30%"><b>Countries OUT</b></td>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td height="40" width="50" align="center"></td>
						<td>
						<select name="CountryNameGo[]" id="CountryNameGo" size="12" multiple ondblclick="fun_singlego();">
						<?
						foreach ($CountryNameGoArray as $k=>$v)
						{?>
						<option value="<?=$k?>"><?=$v?></option>
						<?
						}
						?>
						</select>
						</td>
						<td align="center">
						<button type="button" name="go" onclick="fun_singlego();">&gt;&gt;</button>
						<br>
						<br>
						<button type="button" name="in" onclick="fun_singlein();">&lt;&lt;</button>
						</td>
						<td><select name="CountryNameIn[]" id="CountryNameIn"  size="12" multiple ondblclick="fun_singlein();">
						<?
						foreach ($CountryNameInArray as $k=>$v)
						{?>
						<option value="<?=$k?>"><?=$v?></option>
						<?
						}
						?>
						
						</select>
						</td>
						</tr>
					</table>
					<script type="text/javascript">
		sel_country = document.getElementById("CountryNameIn");
		country = document.getElementById("CountryNameGo");	
		sel_country_len = sel_country.length;
		country_len = country.length;
		
		if(sel_country_len > 0)
			sel_country.options[0].selected = true;
		if(country_len > 0)
			country.options[0].selected = true;

		function FunctionSelectIn()
			{
				for(i=0;i<country_len;i++)
				country.options[i].selected = true;
				return true;
			}
		function fun_sel_all()
		{
			for(i=0;i<sel_country_len;i++)
				sel_country.options[i].selected = true;
		}

		function fun_singlego()
		{
			for(i=0;i<sel_country_len;i++)
				sel_country.options[i].selected = false;
			i=0;		
			while(i<country_len)
			{
				if(country.options[i].selected == true)
				{
					sel_country[sel_country_len] = new Option(country.options[i].text,country.options[i].value);
					country.removeChild(country.options[i]);
					sel_country.options[sel_country_len].selected = true;
					i=0;
					sel_country_len = sel_country.length;
					country_len = country.length;
				}
				else
					i++;
			}
			if(country_len>0)
				country.options[0].selected = true;
		}

		function fun_singlein()
		{
			for(i=0;i<country_len;i++)
				country.options[i].selected = false;
			i=0;			
			while(i<sel_country_len)
			{
				if(sel_country.options[i].selected == true)
				{
					country[country_len] = new Option(sel_country.options[i].text,sel_country.options[i].value);
					sel_country.removeChild(sel_country.options[i]);
					country.options[country_len].selected = true;
					i=0;
					sel_country_len = sel_country.length;
					country_len = country.length;
				}
				else
					i++;
			}
			if(sel_country_len>0)
				sel_country.options[0].selected = true;
		}

		</script>
				</td>				
			</tr>	
			<tr valign=top>
          		<td height="10" colspan="2" align="center"><hr class="InsideTable"></td>
          	</tr>
          		<tr valign=top>
          		<td height="10" colspan="2" align="center"><hr class="InsideTable"></td>
          	</tr>	
			<tr>
				<td align="center" class="InsideLeftTd"><b>Nos of Range(s)</b></td>
				<td align="left" class="InsideRightTd">
				<select name="NoOfOption">
					<?
					for($i=0;$i<=30;$i++)
					{?>
						<option value="<?=$i?>" <?=$i==$NoOfOption?"selected":""?>><?=$i?></option>
					<?
					}?>
				</select>
				<input type="submit" name="ChangeOption" value="Select Option" onclick="return FunctionSelectIn();">
				</td>
			</tr>
			<?
			if($NoOfOption >0)
			{?>
			<tr valign=top>
	          <td colspan="2" align="center" class="InsideRightTd" >
	           <table border="0" cellspacing="2" cellpadding="2" width="100%" class="InsideTable">
		            <tr>
		            <td valign="top" class="InsideLeftTd"><b>Service Type</b></td>  
		    
		              <td valign="top" class="InsideLeftTd"><b>Price</b></td>
		              
		            </tr>
		           <?
		           for($j=1;$j<=$NoOfOption;$j++)
		           {
		              	$CurrentShippingRange = (isset($ShippingRangeRows) && $ShippingRangeRows >0)?$ShippingRangeObj->GetObjectFromRecord():"";
		           ?>
		            <input type="hidden" name="ShippingRangeID<?=$j?>" value="<?=isset($CurrentShippingRange->ShippingRangeID)?$CurrentShippingRange->ShippingRangeID:""?>" size="20">
		            <tr>
		             <td valign="top"><input type="text" name="RangeName<?=$j?>" value="<?=isset($_POST['RangeName'.$j])?$_POST['RangeName'.$j]:(isset($CurrentShippingRange->RangeName)?MyStripSlashes($CurrentShippingRange->RangeName):"")?>" size="50"></td>
		             <td valign="top"><input type="text" name="NormalPrice<?=$j?>" value="<?=isset($_POST['NormalPrice'.$j])?$_POST['NormalPrice'.$j]:(isset($CurrentShippingRange->NormalPrice)?MyStripSlashes($CurrentShippingRange->NormalPrice):"")?>" size="10"></td>
		              <!-- <td valign="top"><input type="text" name="OSPrice<? //=$j?>" value="<? //=isset($_POST['OSPrice'.$j])?$_POST['OSPrice'.$j]:(isset($CurrentShippingRange->OSPrice)?MyStripSlashes($CurrentShippingRange->OSPrice):"")?>" size="10"></td>
		              <td valign="top"><input type="text" name="OWPrice<? //=$j?>" value="<? //=isset($_POST['OWPrice'.$j])?$_POST['OWPrice'.$j]:(isset($CurrentShippingRange->OWPrice)?MyStripSlashes($CurrentShippingRange->OWPrice):"")?>" size="10"></td> 
		              <td valign="top"><input type="text" name="ExpressPrice<?=$j?>" value="<?=isset($_POST['ExpressPrice'.$j])?$_POST['ExpressPrice'.$j]:(isset($CurrentShippingRange->ExpressPrice)?MyStripSlashes($CurrentShippingRange->ExpressPrice):"")?>" size="10"></td>-->
		             </tr>
		            <?
		           }?>
		          </table>
		          </td>
	          </tr>
	          <tr valign=top>
	          	<td height="10" colspan="2" align="center"><hr class="InsideTable">
	           	</td>
	          </tr>		           
	         <?
			}?>	
			<tr>
				<td align="center" class="InsideLeftTd"></td>
				<td align="left" class="InsideRightTd">
				<input type="submit" name="AddShipping" value="Submit" onclick="return FunctionSelectIn();">
				</td>
			</tr> 
			
		</table>
		</form>
		<?
	break;
	default:
		$ShippingZoneObj->Where = "1";
		$ShippingZoneObj->TableSelectAll("","CreatedDate ASC");
		if($ShippingZoneObj->GetNumRows() > 0)
		{
		?>
		<table border="0" cellpadding="3" cellspacing="1" width="100%" class="InsideTable">
			<tr class="InsideLeftTd">
				<td align="center" width="5%"><b>S.No.</b></td>
				<td align="left"><b>Zone Name</b></td>
				<td align="left"><b>Shipping</b></td>
				<td align="center"><b>Edit</b></td>
				<td align="center"><b>Delete</b></td>
			</tr>
			<script type="text/javascript">
				function FunctionDelete()
				{
					result = confirm("Are you sure want to delete the Shipping Zone?")
					if(result == true)
					{
						return true;
					}
					return false;
				}				
							
			</script>
			<?
			$SNo=1;
			$Count=1;
			
			
			while($CurrentShippingZone = $ShippingZoneObj->GetObjectFromRecord())
			{
			?>
			<tr class="InsideRightTd">
				<td align="center"><b><?=$SNo?></b></td>
				<td align="left"><b><?=stripslashes($CurrentShippingZone->ShippingZone);?></b></td>
				<td align="left"><b><?=stripslashes($CurrentShippingZone->NormalPrice);?></b></td>
				<td align="center"><a href="<?=DIR_WS_SITE_CONTROL?>index.php?Page=<?=$Page?>&Shipping=<?=$Shipping?>&Section=EditShipping&ShippingID=<?=$CurrentShippingZone->ShippingID?>"><b>Edit</b></a></td>
				<td align="center">
				<?
				if($CurrentShippingZone->ShippingID !="1")
				{
				?>
					<a href="<?=DIR_WS_SITE_CONTROL?>index.php?Page=<?=$Page?>&Shipping=<?=$Shipping?>&Target=DeleteShipping&ShippingID=<?=$CurrentShippingZone->ShippingID?>" onclick="return FunctionDelete();"><b>Delete</b></a>
				<?
				}?>	
				</td>
			</tr>
			<?
			$SNo++;
			$Count++;
			}
		?>
		</table>
		<?
		}
		else 
		{
			?>
			<b>No Result Found.</b>
			<?
		}
	break;
}
?>
				