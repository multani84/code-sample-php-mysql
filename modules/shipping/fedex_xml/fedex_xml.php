<?php 
if(__FILE__==$_SERVER['SCRIPT_FILENAME'])
	die('some error occurred.');

require_once (dirname(__FILE__).'/config.php');

function GetFedexQuoteDetails()
{
	
	$ResponseArray = array();
	$ResponseArray['date'] = date('Y-m-d');
	
	if( isset($_POST['ShippingCountry']) && $_POST['ShippingCountry'] != "" ){
		$ResponseArray['destination'] = array("PersonName"=>(isset($_POST['ShippingFirstName'])?$_POST['ShippingFirstName']:"").(isset($_POST['ShippingLastName'])?" ".$_POST['ShippingLastName']:""),
											  "CompanyName"=>(isset($_POST['CompanyName'])?$_POST['CompanyName']:""),
											  "PhoneNumber"=>(isset($_POST['ShippingPhone'])?$_POST['ShippingPhone']:""),
											  "StreetLines"=>(isset($_POST['ShippingAddress1'])?$_POST['ShippingAddress1']:"").(isset($_POST['ShippingAddress2'])?" ".$_POST['ShippingAddress2']:""),
											  "City"=>(isset($_POST['ShippingCity'])?$_POST['ShippingCity']:""),
											  "StateOrProvinceCode"=>(isset($_POST['ShippingState'])?$_POST['ShippingState']:""),
											  "PostalCode"=>(isset($_POST['ShippingZip'])?$_POST['ShippingZip']:""),
											  "CountryCode"=>(isset($_POST['ShippingCountry'])?$_POST['ShippingCountry']:""),
											  "Residential"=>true,
											  );
											 
											 
		
	}
	else if(isset($_SESSION['OrderID']) && $_SESSION['OrderID'] !=""){
		
	$OrderObj1 = new DataTable(TABLE_ORDERS);
	$OrderObj1->Where ="OrderID='".(int)$_SESSION['OrderID']."'";	
	$Obj = $OrderObj1->TableSelectOne(array("*"));
		if(isset($Obj->OrderID)){
		$ResponseArray['destination'] = array("PersonName"=>$Obj->ShippingFirstName." ".$Obj->ShippingLastName,
											  "CompanyName"=>(isset($Obj->CompanyName)?$Obj->CompanyName:""),
											  "PhoneNumber"=>(isset($Obj->ShippingPhone)?$Obj->ShippingPhone:""),
											  "StreetLines"=>$Obj->ShippingAddress1." ".$Obj->ShippingAddress2,
											  "City"=>(isset($Obj->ShippingCity)?$Obj->ShippingCity:""),
											  "StateOrProvinceCode"=>(isset($Obj->ShippingState)?$Obj->ShippingState:""),
											  "PostalCode"=>(isset($Obj->ShippingZip)?$Obj->ShippingZip:""),
											  "CountryCode"=>(isset($Obj->ShippingCountry)?$Obj->ShippingCountry:""),
											  "Residential"=>true,
											  );
		}
	
	}
	else{
		/* $ResponseArray['destination'] empty*/
	}
	
		
		
	$ResponseArray['origin'] = array("PersonName"=>"Jenet Krimpelbein",
										  "CompanyName"=>"Innerscent Beauty",
										  "PhoneNumber"=>"262-676-3107",
										  "StreetLines"=>"1614 76th St. Lower",
										  "City"=>"Kenosha",
										  "StateOrProvinceCode"=>"WI",
										  "PostalCode"=>"53143",
										  "CountryCode"=>"US",
										  );
	
	/////////// Cart Values Start ///////////////
				$TmpCartObj = new DataTable(TABLE_TMPCART);
				$TmpCartObj->Where =" SessionID='".session_id()."'";
				$DataArray = array();
				$DataArray[0]= "SUM(Weight*Qty) as WeightTotal, SUM(Price*Qty) as Total";
				$Obj = $TmpCartObj->TableSelectOne($DataArray);
				
		$ResponseArray['cart'] = array("price"=>$Obj->Total,
								   "weight"=>UnitConverterFunc($Obj->WeightTotal,DEFINE_PRODUCT_WEIGHT_UNIT,"kg"),
								   "weight_unit"=>DEFINE_PRODUCT_WEIGHT_UNIT,
								   "dimension"=>array("length"=>"",
													  "width"=>"",
													  "height"=>"",
													  "unit"=>"",
													  ),
																  
	/////////// Cart Values END ///////////////
											   );
				return $ResponseArray; 
}

function GenerateFedexXml()
{
			$Arr = GetFedexQuoteDetails();
	
			$xml  = '<?xml version="1.0"?>';
			$xml .= '<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns1="http://fedex.com/ws/rate/v10">';
			$xml .= '	<SOAP-ENV:Body>';
			$xml .= '		<ns1:RateRequest>';
			$xml .= '			<ns1:WebAuthenticationDetail>';
			$xml .= '				<ns1:UserCredential>';
			$xml .= '					<ns1:Key>' . MODULE_SHIPPING_FEDEX_KEY . '</ns1:Key>';
			$xml .= '					<ns1:Password>' . MODULE_SHIPPING_FEDEX_PASSWORD . '</ns1:Password>';
			$xml .= '				</ns1:UserCredential>';
			$xml .= '			</ns1:WebAuthenticationDetail>';
			$xml .= '			<ns1:ClientDetail>';
			$xml .= '				<ns1:AccountNumber>' . MODULE_SHIPPING_FEDEX_ACCOUNT_NUMBER . '</ns1:AccountNumber>';
			$xml .= '				<ns1:MeterNumber>' . MODULE_SHIPPING_FEDEX_METER_NUMBER. '</ns1:MeterNumber>';
			$xml .= '			</ns1:ClientDetail>';
			$xml .= '			<ns1:Version>';
			$xml .= '				<ns1:ServiceId>crs</ns1:ServiceId>';
			$xml .= '				<ns1:Major>10</ns1:Major>';
			$xml .= '				<ns1:Intermediate>0</ns1:Intermediate>';
			$xml .= '				<ns1:Minor>0</ns1:Minor>';
			$xml .= '			</ns1:Version>';
			$xml .= '			<ns1:ReturnTransitAndCommit>true</ns1:ReturnTransitAndCommit>';
			$xml .= '			<ns1:RequestedShipment>';
			$xml .= '				<ns1:ShipTimestamp>' . date('c', time()) . '</ns1:ShipTimestamp>';
			$xml .= '				<ns1:DropoffType>REGULAR_PICKUP</ns1:DropoffType>'; /*REGULAR_PICKUP, REQUEST_COURIER, DROP_BOX, BUSINESS_SERVICE_CENTER, STATION*/
			$xml .= '				<ns1:PackagingType>YOUR_PACKAGING</ns1:PackagingType>';/*FEDEX_ENVELOPE, FEDEX_PAK, FEDEX_BOX, FEDEX_TUBE, FEDEX_10KG_BOX, FEDEX_25KG_BOX, YOUR_PACKAGING*/
			
				if(isset($Arr['origin']) && is_array($Arr['origin']) && count($Arr['origin']) > 0){
					$xml .= '				<ns1:Shipper>';
					$xml .= '					<ns1:Contact>';
					$xml .= '						<ns1:PersonName>' .(isset($Arr['origin']['PersonName'])?$Arr['origin']['PersonName']:""). '</ns1:PersonName>';
					$xml .= '						<ns1:CompanyName>' .(isset($Arr['origin']['CompanyName'])?$Arr['origin']['CompanyName']:""). '</ns1:CompanyName>';
					$xml .= '						<ns1:PhoneNumber>' .(isset($Arr['origin']['PhoneNumber'])?$Arr['origin']['PhoneNumber']:""). '</ns1:PhoneNumber>';
					$xml .= '					</ns1:Contact>';
					$xml .= '					<ns1:Address>';
					$xml .= '						<ns1:StreetLines>' .(isset($Arr['origin']['StreetLines'])?$Arr['origin']['StreetLines']:""). '</ns1:StreetLines>';
					$xml .= '						<ns1:City>' .(isset($Arr['origin']['City'])?$Arr['origin']['City']:""). '</ns1:City>';
					$xml .= '						<ns1:StateOrProvinceCode>' .(isset($Arr['origin']['StateOrProvinceCode'])?$Arr['origin']['StateOrProvinceCode']:""). '</ns1:StateOrProvinceCode>';
					$xml .= '						<ns1:PostalCode>' .(isset($Arr['origin']['PostalCode'])?$Arr['origin']['PostalCode']:""). '</ns1:PostalCode>';
					$xml .= '						<ns1:CountryCode>' .(isset($Arr['origin']['CountryCode'])?$Arr['origin']['CountryCode']:""). '</ns1:CountryCode>';
					$xml .= '					</ns1:Address>';
					$xml .= '				</ns1:Shipper>';
				}
			
				if(isset($Arr['destination']) && is_array($Arr['destination']) && count($Arr['destination']) > 0){
					$xml .= '				<ns1:Recipient>';
					$xml .= '					<ns1:Contact>';
					$xml .= '						<ns1:PersonName>' .(isset($Arr['destination']['PersonName'])?$Arr['destination']['PersonName']:""). '</ns1:PersonName>';
					if(isset($Arr['destination']['CompanyName']) && $Arr['destination']['CompanyName'] != "")
						$xml .= '						<ns1:CompanyName>' .(isset($Arr['destination']['CompanyName'])?$Arr['destination']['CompanyName']:""). '</ns1:CompanyName>';
					else
						$xml .= '						<ns1:CompanyName>' .(isset($Arr['destination']['PersonName'])?$Arr['destination']['PersonName']:""). '</ns1:CompanyName>';
					
					$xml .= '						<ns1:PhoneNumber>' .(isset($Arr['destination']['PhoneNumber'])?$Arr['destination']['PhoneNumber']:""). '</ns1:PhoneNumber>';
					$xml .= '					</ns1:Contact>';
					$xml .= '					<ns1:Address>';
					$xml .= '						<ns1:StreetLines>' .(isset($Arr['destination']['StreetLines'])?$Arr['destination']['StreetLines']:""). '</ns1:StreetLines>';
					$xml .= '						<ns1:City>' .(isset($Arr['destination']['City'])?$Arr['destination']['City']:""). '</ns1:City>';
					if (in_array($Arr['destination']['CountryCode'], array('US', 'CA'))) 
					{
						$xml .= '						<ns1:StateOrProvinceCode>' .(isset($Arr['destination']['StateOrProvinceCode'])?$Arr['destination']['StateOrProvinceCode']:""). '</ns1:StateOrProvinceCode>';
						
					}
					else
					{
						$xml .= '						<ns1:StateOrProvinceCode></ns1:StateOrProvinceCode>';
					}
					
					$xml .= '						<ns1:PostalCode>' .(isset($Arr['destination']['PostalCode'])?$Arr['destination']['PostalCode']:""). '</ns1:PostalCode>';
					$xml .= '						<ns1:CountryCode>' .(isset($Arr['destination']['CountryCode'])?$Arr['destination']['CountryCode']:""). '</ns1:CountryCode>';
					$xml .= '					</ns1:Address>';
					$xml .= '				</ns1:Recipient>';
				}
				
			
			$xml .= '				<ns1:ShippingChargesPayment>';
			$xml .= '					<ns1:PaymentType>SENDER</ns1:PaymentType>';
			$xml .= '					<ns1:Payor>';
			$xml .= '						<ns1:AccountNumber>' . MODULE_SHIPPING_FEDEX_ACCOUNT_NUMBER. '</ns1:AccountNumber>';
			$xml .= '						<ns1:CountryCode>US</ns1:CountryCode>';
			$xml .= '					</ns1:Payor>';
			$xml .= '				</ns1:ShippingChargesPayment>';
			$xml .= '				<ns1:RateRequestTypes>LIST</ns1:RateRequestTypes>';
			$xml .= '				<ns1:PackageCount>1</ns1:PackageCount>';
			$xml .= '				<ns1:RequestedPackageLineItems>';
			$xml .= '					<ns1:SequenceNumber>1</ns1:SequenceNumber>';
			$xml .= '					<ns1:GroupPackageCount>1</ns1:GroupPackageCount>';
			if(isset($Arr['cart']) && is_array($Arr['cart']) && count($Arr['cart']) > 0){
				$xml .= '					<ns1:Weight>';
				$xml .= '						<ns1:Units>'.(isset($Arr['cart']['weight_unit'])?$Arr['cart']['weight_unit']:"").'</ns1:Units>'; /* KG, LB */
				$xml .= '						<ns1:Value>'.(isset($Arr['cart']['weight'])?$Arr['cart']['weight']:"").'</ns1:Value>';
				$xml .= '					</ns1:Weight>';
			}
			
			if(isset($Arr['cart']['dimension']) && is_array($Arr['cart']['dimension']) && count($Arr['cart']['dimension']) > 0){
				/*
				$xml .= '					<ns1:Dimensions>';
				$xml .= '						<ns1:Length>' .(isset($Arr['cart']['dimension']['length'])?$Arr['cart']['dimension']['length']:"")  . '</ns1:Length>';
				$xml .= '						<ns1:Width>' . (isset($Arr['cart']['dimension']['width'])?$Arr['cart']['dimension']['width']:"") . '</ns1:Width>';
				$xml .= '						<ns1:Height>' . (isset($Arr['cart']['dimension']['height'])?$Arr['cart']['dimension']['height']:"") . '</ns1:Height>';
				$xml .= '						<ns1:Units>' . (isset($Arr['cart']['dimension']['unit'])?$Arr['cart']['dimension']['unit']:"") . '</ns1:Units>';
				$xml .= '					</ns1:Dimensions>';
				*/
			}
			$xml .= '				</ns1:RequestedPackageLineItems>';
			$xml .= '			</ns1:RequestedShipment>';
			$xml .= '		</ns1:RateRequest>';
			$xml .= '	</SOAP-ENV:Body>';
			$xml .= '</SOAP-ENV:Envelope>';
			
		return $xml;	
			
}

function GetFedexResponse()
{
	
	$xml = GenerateFedexXml();
	$response = GetFedexLog();
	if($response===false)
	{
		$curl = curl_init(MODULE_SHIPPING_FEDEX_URL);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $xml);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_HEADER, false);
		curl_setopt($curl, CURLOPT_TIMEOUT, 30);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		$response = curl_exec($curl);
		curl_close($curl);
	}
	
	return ParseFedexResponse($response);
	
	
}

function ParseFedexResponse($content)
{
	$ResponseArray = array();
	try
	{
		$dom = new DOMDocument('1.0', 'UTF-8');
		@$dom->loadXml($content);
		if($dom->getElementsByTagName('RateReplyDetails')->length > 0){
			$RateReplyDetails = $dom->getElementsByTagName('RateReplyDetails');
			
			SetFedexLog($content);
			
			foreach ($RateReplyDetails as $rate) {
				$code = $rate->getElementsByTagName('ServiceType')->item(0)->nodeValue;
				$title = $rate->getElementsByTagName('ServiceType')->item(0)->nodeValue;
				$price = $rate->getElementsByTagName('ShipmentRateDetail')->item(0)
							 ->getElementsByTagName('TotalNetChargeWithDutiesAndTaxes')->item(0)
							 ->getElementsByTagName('Amount')->item(0)
							 ->nodeValue;
				$currency = $rate->getElementsByTagName('ShipmentRateDetail')->item(0)
							 ->getElementsByTagName('TotalNetChargeWithDutiesAndTaxes')->item(0)
							 ->getElementsByTagName('Currency')->item(0)
							 ->nodeValue;
				
				
				$ResponseArray['rate'][$code] = array("code"=>$code,
													  "title"=>$title,
													  "price"=>$price,
													  "currency"=>$currency,
													  "display_price"=>$price
													  );
													  
				
				
			}
			$price = array();
			foreach ($ResponseArray['rate'] as $key => $row)
				$price[$key] = $row['price'];
			array_multisort($price, SORT_ASC, $ResponseArray['rate']);
			$ResponseArray['title'] = "FedEx";
			
		}
		else{
			$ResponseArray['Error'] = "ERROR: ";
			SetFedexLog($content,true);
		}
	}
	catch(Exception $e){
		//var_dump($e);exit;
	}
	
	
	return $ResponseArray;
}

function GetFedexLog()
{
	$Arr = GetFedexQuoteDetails();
	
	$Return = false;
	global $fedex_code;
	$filename = md5(serialize($Arr));
	$File = DIR_FS_SITE_PRIVATE.$fedex_code."/".$filename;
	if(file_exists ($File))
	{
		return file_get_contents($File);
	}
	
	return $Return;
}
function SetFedexLog($content,$Error=false)
{
	$Arr = GetFedexQuoteDetails();
	
	global $fedex_code;
	$filename = md5(serialize($Arr));
	
	if($Error==true)
	{
		$filename = "error_".date('Y-m-d_H-i-s');
		$content .= chr(13)."=========Serialize Array ================".chr(13);
		$content .= serialize($Arr);
		$content .= chr(13)."=========GenerateFedexXml Input ================".chr(13);
		$content .= GenerateFedexXml();
		
	}
	$Dir = DIR_FS_SITE_PRIVATE.$fedex_code."/";
	$File = DIR_FS_SITE_PRIVATE.$fedex_code."/".$filename;
	if(file_exists ($Dir ))
	{
		if(file_exists ($File)== false)
		{
			$fp = fopen($File,"w");
			fputs($fp,$content);
			fclose($fp);	
		}
		
	}
	else
	{
		if (mkdir($Dir, 0777, true)) 
		{
			if(file_exists ($File)== false)
			{
				$fp = fopen($File,"w");
				fputs($fp,$content);
				fclose($fp);	
			}
		}
	}
	
}

function ClearFedexLog()
{
	global $fedex_code;
	$Dir = DIR_FS_SITE_PRIVATE.$fedex_code."/";
	if(file_exists ($Dir ))
	{
		if(function_exists("DeleteFullFolder"))
			DeleteFullFolder($Dir);
	}
}

$ResponseArray  = GetFedexResponse();
if(isset($ResponseArray) && is_array($ResponseArray) && count($ResponseArray) > 0 && !isset($ResponseArray['Error'])){
	$ShippingArray[$fedex_code] = $ResponseArray;
}
else{
	
}




?>