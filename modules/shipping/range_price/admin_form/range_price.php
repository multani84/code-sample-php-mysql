<!-- Heading  -->
<div class="row-fluid">
	<div class="span12">
		<h1>Shipping</h1>
		<hr />
		<?php  echo isset($CurrentShipping->ShippingGroup)?"<b>".$CurrentShipping->ShippingGroup."</b>":""?>
	</div>
	<div class="pull-left">
		<a href="<?php  echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php  echo $Page?>&Shipping=<?php  echo $Shipping?>" class="adm-link">SHIPPING GROUPS</a>
		&nbsp;&nbsp;||&nbsp;&nbsp;
		<a href="<?php  echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php  echo $Page?>&Shipping=<?php  echo $Shipping?>&Section=ShippingType" class="adm-link">SHIPPING TYPES</a>
		
	</div>
	<div class="pull-right">
		<a href="<?php  echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php  echo $Page?>&Shipping=<?php  echo $Shipping?>&Section=AddShipping" class="btn btn-primary">Add New Shipping Group  <icon class="icon-plus-sign icon-white-t"></icon></a>
	</div>
<div class="clearfix separator bottom"></div>
<!--  End Heading-->	

<?php 
switch ($Section)
{
	case "AddShipping":
	case "EditShipping":
		$CountryNameArray = array();
		$TableName = TABLE_COUNTRIES." c LEFT JOIN ".TABLE_SHIPPING_COUNTRIES." sc on c.CountryID=sc.CountryID";
		$ShippingCountryNameObj = new DataTable($TableName);
		//$ShippingCountryNameObj->DisplayQuery = true;
		$ShippingCountryNameObj->Where ="1";
		$ShippingCountryNameObj->TableSelectAll(array("c.CountryID","c.CountryName"),"c.CountryName ASC");
		while($CountryName = $ShippingCountryNameObj->GetObjectFromRecord())
		{
			$CountryNameArray[$CountryName->CountryID] = $CountryName->CountryName;
		}
		
		$CountryNameGoArray = array();
		$CountryNameInArray = array();
		if(isset($_POST['CountryNameGo']) && is_array($_POST['CountryNameGo']))
		{	
			foreach ($_POST['CountryNameGo'] as $k=>$v)
				$CountryNameGoArray[$v] = $CountryNameArray[$v];
			
		}
		else 
		{
			$ShippingCountryNameObj->Where ="ShippingID='$ShippingID'";
			$ShippingCountryNameObj->TableSelectAll(array("c.CountryID","c.CountryName"),"c.CountryName ASC");
			while($CountryNameGo = $ShippingCountryNameObj->GetObjectFromRecord())
				$CountryNameGoArray[$CountryNameGo->CountryID] = $CountryNameArray[$CountryNameGo->CountryID];
		}
		$CountryNameInArray = array_diff($CountryNameArray,$CountryNameGoArray);
	
	?>
		<form class="form-horizontal" method="POST" action="<?php  echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php  echo $Page?>&Shipping=<?php  echo $Shipping?>&Section=<?php  echo $Section?>&Target=AddShipping&ShippingID=<?php  echo $ShippingID?>" enctype="multipart/form-data">
		
		<div class="well">
		
			<div class="form-group">
				<label for="field" class="col-sm-4 col-md-5 col-lg-4 control-label">Shipping Zone Name</label>
				<div class="col-sm-20 col-md-19 col-lg-20 text-left">
					<input type="text" name="ShippingGroup" value="<?php  echo isset($_POST['ShippingGroup'])?MyStripSlashes($_POST['ShippingGroup']):(isset($CurrentShipping->ShippingGroup)?MyStripSlashes($CurrentShipping->ShippingGroup):"")?>" size="70">
				</div>
			</div>
			
			<div class="form-group">
				<label for="field" class="col-sm-4 col-md-5 col-lg-4 control-label">&nbsp;</label>
				<div class="col-sm-20 col-md-19 col-lg-20 text-left">
					<table border="0" cellpadding="3" cellspacing="1" width="100%" class="InsideTable">
							<tr>
							<td height="40" align="center">&nbsp;</td>
							<td width="30%"><b>Countries IN</b></td>
							<td width="10%"></td>
							<td  width="30%"><b>Countries OUT</b></td>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td height="40" width="50" align="center"></td>
						<td>
						<select name="CountryNameGo[]" id="CountryNameGo" size="25" multiple ondblclick="fun_singlego();">
						<?php 
						foreach ($CountryNameGoArray as $k=>$v)
						{?>
						<option value="<?php  echo $k?>"><?php  echo $v?></option>
						<?php 
						}
						?>
						</select>
						</td>
						<td align="center">
						<button type="button" name="go" onclick="fun_singlego();">&gt;&gt;</button>
						<br>
						<br>
						<button type="button" name="in" onclick="fun_singlein();">&lt;&lt;</button>
						</td>
						<td><select name="CountryNameIn[]" id="CountryNameIn"  size="25" multiple ondblclick="fun_singlein();">
						<?php 
						foreach ($CountryNameInArray as $k=>$v)
						{?>
						<option value="<?php  echo $k?>"><?php  echo $v?></option>
						<?php 
						}
						?>
						
						</select>
						</td>
						</tr>
					</table>
					<script type="text/javascript">
		sel_country = document.getElementById("CountryNameIn");
		country = document.getElementById("CountryNameGo");	
		sel_country_len = sel_country.length;
		country_len = country.length;
		
		if(sel_country_len > 0)
			sel_country.options[0].selected = true;
		if(country_len > 0)
			country.options[0].selected = true;

		function FunctionSelectIn()
			{
				for(i=0;i<country_len;i++)
				country.options[i].selected = true;
				return true;
			}
		function fun_sel_all()
		{
			for(i=0;i<sel_country_len;i++)
				sel_country.options[i].selected = true;
		}

		function fun_singlego()
		{
			for(i=0;i<sel_country_len;i++)
				sel_country.options[i].selected = false;
			i=0;		
			while(i<country_len)
			{
				if(country.options[i].selected == true)
				{
					sel_country[sel_country_len] = new Option(country.options[i].text,country.options[i].value);
					country.removeChild(country.options[i]);
					sel_country.options[sel_country_len].selected = true;
					i=0;
					sel_country_len = sel_country.length;
					country_len = country.length;
				}
				else
					i++;
			}
			if(country_len>0)
				country.options[0].selected = true;
		}

		function fun_singlein()
		{
			for(i=0;i<country_len;i++)
				country.options[i].selected = false;
			i=0;			
			while(i<sel_country_len)
			{
				if(sel_country.options[i].selected == true)
				{
					country[country_len] = new Option(sel_country.options[i].text,sel_country.options[i].value);
					sel_country.removeChild(sel_country.options[i]);
					country.options[country_len].selected = true;
					i=0;
					sel_country_len = sel_country.length;
					country_len = country.length;
				}
				else
					i++;
			}
			if(sel_country_len>0)
				sel_country.options[0].selected = true;
		}

		</script>
				</div>
			</div>
		
		
			<div class="form-group">
				<div class="col-sm-6 pull-right">
					<button class="btn btn-warning  btn-block" type="submit" name="AddShipping" onclick="return FunctionSelectIn();">Submit</button>
				</div>
			</div>
		
          	
			</div>
		</form>
		<?php 
	break;
	case "ShippingType":
	case "EditShippingType":
		$ShippingTypeObj = new DataTable(TABLE_SHIPPING_RZS_TYPES);
		$ShippingTypeObj->Where = "1";
		$ShippingTypeObj->TableSelectAll("","Position ASC");
		$SNo = 1;
		?>
		<script type="text/javascript">
		function FunctionDeleteShippingType(TypeID,Status)
		{
			if(Status==0)
			{
				alert("You can not delete this shipping type.");
				return false;
			}
			else
			{
				result = confirm("Are you sure want to delete the shipping type?")
				if(result == true)
				{
					URL="<?php  echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php  echo $Page?>&Target=DeleteShippingType&Section=<?php  echo $Section?>&Shipping=<?php  echo $Shipping?>&TypeID=" + TypeID;
					location.href=URL;
					return true;
				}
			}
			return false;
		}				
							
		</script>
		<table class="table table-striped table-bordered table-responsive block">
			<thead>
			<tr>
				<td align="center"><b>S.No.</b></td>
				<td align="left"><b>Shipping Types</b></td>
				<td align="center"><b>Position</b></td>
				<td align="center">&nbsp;</td>
				<td align="center">&nbsp;</td>
			</tr>
			</thead>
			<tbody>
			<?php
			while($CurrentShippingType = $ShippingTypeObj->GetObjectFromRecord())
			{
				
				$Status = 1;
				
				if($Section=="EditShippingType" && $TypeID==$CurrentShippingType->TypeID)
				{
					?>
					<form method="POST" action="<?php  echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php  echo $Page?>&Section=<?php  echo $Section?>&Shipping=<?php  echo $Shipping?>&TypeID=<?php  echo $TypeID?>&Target=EditShippingType" enctype="multipart/form-data">
						<tr class="InsideRightTd">
							<td align="center"><b><?php  echo $SNo?></b></td>
							<td align="left"><input type="text" name="TypeName" value="<?php  echo MyStripSlashes($CurrentShippingType->TypeName);?>" size="20"></td>
							<td align="center"><input type="text" name="Position" value="<?php  echo MyStripSlashes($CurrentShippingType->Position);?>" size="5" class="SortBox" style="text-align:center;"></td>
							<td align="center"><input type="submit" name="Submit" value="Update ShippingType" class="btn"></td>
							<td align="center"><input type="button" name="button" class="btn" value="Cancel" onclick="javascript:history.back();"></td>
							
						</tr>
					</form>
					<?php 		
				}
				else 
				{
					?>
						<tr class="InsideRightTd">
							<td align="center"><b><?php  echo $SNo?></b></td>
							<td align="left"><?php  echo MyStripSlashes($CurrentShippingType->TypeName);?></td>
							<td align="center"><?php  echo MyStripSlashes($CurrentShippingType->Position);?>
							</td>
							<?php if($CurrentShippingType->DefaultType=="1"):?>
							<td align="center"></td>
							<td align="center"></td>
							<?php else:?>
							<td align="center"><a href="<?php  echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php  echo $Page?>&Section=EditShippingType&Shipping=<?php  echo $Shipping?>&TypeID=<?php  echo $CurrentShippingType->TypeID?>" class="btn btn-success btn-phone-block"><icon class="icon-pencil icon-white"></icon> Edit</a></td>
							<td align="center"><a href="#" onclick="return FunctionDeleteShippingType('<?php  echo $CurrentShippingType->TypeID?>','<?php  echo $Status?>');" class="btn btn-danger btn-phone-block"><icon class="icon-remove icon-white"></icon> Delete</a></td>
							<?php endif;?>
						
							
						</tr>
					<?php 
				}
			   $SNo++;
			}
			?>
			<form method="POST" action="<?php  echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php  echo $Page?>&Section=<?php  echo $Section?>&Shipping=<?php  echo $Shipping?>&Target=AddShippingType" enctype="multipart/form-data">
			<tr class="InsideRightTd">
				<td align="center"><b><?php  echo $SNo?></b></td>
				<td align="left"><input type="text" name="TypeName" value="" size="20"></td>
				<td align="center"><input type="text" name="Position" size="5" class="SortBox"></td>
				<td align="center"><input type="submit" name="Submit" class="btn" value="Add Shipping Type"></td>
				<td align="center">&nbsp;</td>
			</tr>
		</form>
		</tbody>
		</table>	
		<?php 
		break;
	case "PostCode":
		$ShippingPostCodeObj->Where = "ZoneID = '$ZoneID'";
		$ShippingPostCodeObj->TableSelectAll("","PostCode ASC");

		$ShippingZoneObj->Where ="ZoneID = '".$ShippingZoneObj->MysqlEscapeString($ZoneID)."'";
		$CurrentZone = $ShippingZoneObj->TableSelectOne();	
	
		if($ShippingPostCodeObj->GetNumRows() >0)
		{	
		?>
		<h3><a href="<?php  echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php  echo $Page?>&Section=ShippingZone&Shipping=<?php  echo $Shipping?>&ShippingID=<?php  echo $ShippingID?>&ZoneID=<?php  echo $ZoneID?>"><?php  echo isset($CurrentZone->ZoneName)?$CurrentZone->ZoneName:""?></a></h3>
		<form method="POST" action="<?php  echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php  echo $Page?>&Section=<?php  echo $Section?>&Shipping=<?php  echo $Shipping?>&ShippingID=<?php  echo $ShippingID?>&ZoneID=<?php  echo $ZoneID?>&Target=DeletePostCode" enctype="multipart/form-data">
		<table border="0" cellpadding="3" cellspacing="1" width="100%" class="InsideTable">
			<tr class="InsideLeftTd">
					<td class="InsideLeftTd" align="left" colspan="2">
						<ul class="PostCodeList">
							<?php  while ($CurrentPostCode = $ShippingPostCodeObj->GetObjectFromRecord())
							{?>
							<li>
							<input type="checkbox" class="CHKPostCode" id="Chk<?php  echo $CurrentPostCode->PostCodeID?>" name="PostCodeID[]" value="<?php  echo $CurrentPostCode->PostCodeID?>">
							<label for="Chk<?php  echo $CurrentPostCode->PostCodeID?>"><?php  echo $CurrentPostCode->PostCode?></label>
							</li>
							<?php 
							}?>
						</ul>
						
					</td>
				</tr>
				<tr class="InsideLeftTd">
					<td class="InsideLeftTd" align="left" colspan="2">
					<a href="javascript:;" onclick="return jQuery('.CHKPostCode').attr('checked','checked')" class="adm-link">Select All</a>
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<a href="javascript:;" onclick="return jQuery('.CHKPostCode').attr('checked',false)" class="adm-link">UnSelect All</a>
					
					</td>
				</tr>
				
				<tr class="InsideLeftTd">
					<td class="InsideLeftTd" align="center"  colspan="2">
						<input type="submit" name="Submit" value="Delete" style="width:150px;border:1px solid #000;" onclick="return confirm('Are you sure want to delete these Postcodes?')">
					</td>
				</tr>
		</table>
		</form>
		<style type="text/css">
		.PostCodeList{list-style:none;padding:0px;margin:0px;width:100%;}
		.PostCodeList li{background:#fff;float:left;list-style:none;padding:5px;margin:1px;width:80px;}
		</style>	
		<hr>
		<?php 
		}?>
		<?php if($CurrentZone->DefaultZone != "1"):?>
		<hr>	
		<h3><a href="<?php  echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php  echo $Page?>&Section=ShippingZone&Shipping=<?php  echo $Shipping?>&ShippingID=<?php  echo $ShippingID?>&ZoneID=<?php  echo $ZoneID?>"><?php  echo isset($CurrentZone->ZoneName)?$CurrentZone->ZoneName:""?></a></h3>
		<fieldset title="Bulk Postcode Entries">
		<legend><b>Bulk Postcode Entries:</b></legend>
		<form method="POST" action="<?php  echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php  echo $Page?>&Section=<?php  echo $Section?>&Shipping=<?php  echo $Shipping?>&ShippingID=<?php  echo $ShippingID?>&ZoneID=<?php  echo $ZoneID?>&Target=BulkPostCode" enctype="multipart/form-data">
			<table border="0" cellpadding="3" cellspacing="1" width="100%" class="InsideTable">
				<tr class="InsideLeftTd">
					<td class="InsideLeftTd" align="left" colspan="2">
						<textarea name="BulkPostCode" style="width:100%;height:150px;"></textarea>
					</td>
				</tr>
				<tr class="InsideLeftTd">
					<td class="InsideLeftTd" align="left" width="150"><b>Separated with:</b></td>
					<td class="InsideLeftTd" align="left">
					<select name="SeparateSelect">
						<option value="nl" selected>--newline-</option>
						<option value=",">commas(,)</option>
						<option value="">--other--</option>
					</select>
					--or--
					<input type="text" name="Separate" id="Separate" value="," size="3" style="width:30px;">
					</td>
				</tr>
				<tr class="InsideLeftTd">
					<td class="InsideLeftTd" align="center"  colspan="2">
						<input type="submit" name="Submit" value="Submit" style="width:150px;border:1px solid #000;">
					</td>
				</tr>
			</table>
		</form>	
		</fieldset>
		<?php endif;?>
		
		
		<?php /* shipping zone range start*/?>
		<?php 
		$ShippingTypeObj = new DataTable(TABLE_SHIPPING_RZS_TYPES);
		$ShippingPriceObj = new DataTable(TABLE_SHIPPING_RZS_PRICES);
			
		$ShippingTypeObj->Where = "1";
		$ShippingTypeObj->TableSelectAll("","DefaultType DESC, Position ASC, CreatedDate ASC");
		$TypeArray = array();
		$PriceArray = array();
		$ActiveId= "";
		while($CurrentType = $ShippingTypeObj->GetObjectFromRecord())
		{
			if($ActiveId=="")
				$ActiveId = $CurrentType->TypeID;
			
			$TypeArray[$CurrentType->TypeID] = $CurrentType->TypeName;
			
			$ShippingPriceObj->Where = "ZoneID = '$ZoneID' AND TypeID = '$CurrentType->TypeID'";
			$ShippingPriceObj->TableSelectAll("","RangeMin ASC");
			while($CurrentPrice = $ShippingPriceObj->GetObjectFromRecord())
			{
				$PriceArray[$CurrentType->TypeID][] =  $CurrentPrice;
			}
			
			
		}
		?>
				<!--  Tabs -->
	<div class="span3">
		<ul class="tabs-arrow">
			<?php foreach($TypeArray as $k=>$v):?>
			<li class="<?php echo ($ActiveId==$k?'active':'')?>"><a class="glyphicons list" href="#type_<?php echo $k?>" data-toggle="tab"><i></i><?php echo $v?></a></li>
			<?php endforeach;?>
		</ul>
		<div class="clearfix"></div><br/>
	</div>
	<!-- End Tabs -->
	
	<!--  Tab Content -->
	<div class="span8" id="Ranges">
		<form class="form-horizontal" method="POST" action="<?php  echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php  echo $Page?>&Section=<?php  echo $Section?>&Shipping=<?php  echo $Shipping?>&ShippingID=<?php  echo $ShippingID?>&ZoneID=<?php  echo $ZoneID?>&Target=AddRanges" enctype="multipart/form-data">
			<div class="well">
			<div class="tab-content">	
		<?php foreach($TypeArray as $k=>$v):?>
			<!--  Tab Content Block -->
			<div class="tab-pane<?php echo ($ActiveId==$k?' active':'')?>" id="type_<?php echo $k?>">
				<h3><?php echo $v?>'s Range(s)</h3>
				<div class="well" id="range_<?php echo $k?>">	
					<div class="well">	
					<div class="pull-right">
						<a href="javascript:;" onclick="return ChangeRanges('range_<?php echo $k?>');">Add New Row</a>
					</div>
						<table class="table table-striped table-bordered table-responsive block">
							<thead>
							<tr>
								<th align="left" class="InsideLeftTd">Range Minimum</th>
								<th align="left" class="InsideLeftTd">Range Maximum</th>
								<th align="left" class="InsideLeftTd">Price</th>
								<th align="left" class="InsideLeftTd">Enabled</th>
								<th align="left" class="InsideLeftTd">Remove</th>
							</tr>
							</thead>
							<tbody>
								<?php if(isset($PriceArray[$k]) && count($PriceArray[$k]) > 0):?>
								<?php foreach($PriceArray[$k] as $kk=>$obj):?>
								<tr>
									<td align="left" class="InsideLeftTd"><input type="text" name="RangeMin[<?php echo $k?>][]" value="<?php echo isset($obj->RangeMin)?$obj->RangeMin:""?>" style="text-align:center;width:90px"></td>
									<td align="left" class="InsideLeftTd"><input type="text" name="RangeMax[<?php echo $k?>][]" value="<?php echo isset($obj->RangeMax)?$obj->RangeMax:""?>" style="text-align:center;width:90px"></td>
									<td align="left" class="InsideLeftTd"><input type="text" name="Price[<?php echo $k?>][]" value="<?php echo isset($obj->Price)?$obj->Price:""?>" style="text-align:center;width:90px"></td>
									<td align="left" class="InsideLeftTd">
										<select name="Active[<?php echo $k?>][]" style="width:90px;">
											<option value="0" >Disable</option>
											<option value="1" <?php echo (isset($obj->Active) && $obj->Active=="1")?"selected":""?>>Enable</option>
										</select>
									</td>
									<td align="left" class="InsideLeftTd"><a href="javascript:;" class="DeleteRange btn btn-danger btn-phone-block"><icon class="icon-remove icon-white"></icon> Delete</a></td>
								</tr>
								<?php endforeach;?>
								
								<?php else:?>
								<tr>
									<td align="left" class="InsideLeftTd"><input type="text" name="RangeMin[<?php echo $k?>][]" value="" style="text-align:center;width:90px"></td>
									<td align="left" class="InsideLeftTd"><input type="text" name="RangeMax[<?php echo $k?>][]" value="" style="text-align:center;width:90px"></td>
									<td align="left" class="InsideLeftTd"><input type="text" name="Price[<?php echo $k?>][]" value="" style="text-align:center;width:90px"></td>
									<td align="left" class="InsideLeftTd">
										<select name="Active[<?php echo $k?>][]" style="width:90px;">
											<option value="0">Disable</option>
											<option value="1">Enable</option>
										</select>
									</td>
									<td align="left" class="InsideLeftTd"><a href="javascript:;" class="DeleteRange btn btn-danger btn-phone-block"><icon class="icon-remove icon-white"></icon> Delete</a></td>
								</tr>
								<?php endif;?>
								
							</tbody>
						</table>
						<div class="pull-right">
							<a href="javascript:;" onclick="return ChangeRanges('range_<?php echo $k?>');">Add New Row</a>
						</div>
					
					</div>
					
						
					
				</div>
					
					
					
				
				
				</div>	
			<!--  End Tab Content Block -->
			<?php endforeach;?>
		</div>
		<div class="form-group">
					<div class="col-sm-6 pull-right">
						<button class="btn btn-warning  btn-block" type="submit" name="AddProduct">Submit</button>
					</div>
				</div>
			</div>	
			</form>
	</div>
		
	<!--  End Tab Content -->
	<script type="text/javascript">
		
			function ChangeRanges(id)
			{
				var tr = $("#"+id).find("table").find("tbody").find("tr").eq(0).clone()
				$("#"+id).find("table").find("tbody").append(tr);
			}
			
			jQuery(document).ready(function($){
				$( "body" ).delegate( ".DeleteRange", "click", function(){
					if($(this).closest('table').find("tbody").find("tr").length > 1)
						$(this).closest('tr').remove();
					else
						alert('Can not delete first record.');
				})
			})
	
	</script>
	
	
		<?php /* shipping zone range end*/?>
		
		<?php 
		break;	
	case "PostCodeView":
		@ob_clean();
		$ShippingPostCodeObj->Where = "ZoneID = '$ZoneID'";
		$ShippingPostCodeObj->TableSelectAll("","PostCode ASC");

		$ShippingZoneObj->Where ="ZoneID = '".$ShippingZoneObj->MysqlEscapeString($ZoneID)."'";
		$CurrentZone = $ShippingZoneObj->TableSelectOne();	
	
		if($ShippingPostCodeObj->GetNumRows() >0)
		{	
		?>
		<h3><?php  echo isset($CurrentZone->ZoneName)?$CurrentZone->ZoneName:""?></h3>
		<table border="0" cellpadding="3" cellspacing="1" width="100%" class="InsideTable">
			<tr class="InsideLeftTd">
					<td class="InsideLeftTd" align="left" colspan="2">
						<ul class="PostCodeList">
							<?php  while ($CurrentPostCode = $ShippingPostCodeObj->GetObjectFromRecord())
							{?>
							<li>
							<label for="Chk<?php  echo $CurrentPostCode->PostCodeID?>"><?php  echo $CurrentPostCode->PostCode?>,</label>
							</li>
							<?php 
							}?>
						</ul>
						
					</td>
				</tr>
		</table>
		<style type="text/css">
		.PostCodeList{list-style:none;padding:0px;margin:0px;width:100%;}
		.PostCodeList li{background:#fff;float:left;list-style:none;padding:5px;margin:1px;width:40px;}
		</style>	
		<hr>
		<?php 
		}
		else 
		{
		?>
		<h3><?php  echo isset($CurrentZone->ZoneName)?$CurrentZone->ZoneName:""?></h3>
		<h3>No record found.</h3>
		
		<?php 	
		}
		?>
		<?php 
		break;	
	case "ShippingZone":
	case "EditShippingZone":
		$ShippingZoneObj = new DataTable(TABLE_SHIPPING_RZS_ZONES);
		$ShippingZoneObj->Where = "ShippingId='$ShippingID'";
		$ShippingZoneObj->TableSelectAll("","DefaultZone='1' DESC, ZoneName ASC");
		$SNo = 1;
		?>
		<script type="text/javascript">
		function FunctionDeleteShippingZone(ZoneID,Status)
		{
			if(Status==0)
			{
				alert("You can not delete this shipping zone.");
				return false;
			}
			else
			{
				result = confirm("Are you sure want to delete the shipping zone?")
				if(result == true)
				{
					URL="<?php  echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php  echo $Page?>&Target=DeleteShippingZone&Section=<?php  echo $Section?>&Shipping=<?php  echo $Shipping?>&ShippingID=<?php  echo $ShippingID?>&ZoneID=" + ZoneID;
					location.href=URL;
					return true;
				}
			}
			return false;
		}				
							
		</script>
		<table class="table table-striped table-bordered table-responsive block">
			<thead>
			<tr>
				<td align="center"><b>S.No.</b></td>
				<td align="left"><b>Zone Names</b></td>
				<td align="center"><b>Post Code(s)</b></td>
				<td align="center">&nbsp;</td>
				<td align="center">&nbsp;</td>
			</tr>
			</thead>
			<tbody>
			<?php
			while($CurrentShippingZone = $ShippingZoneObj->GetObjectFromRecord())
			{
				$ShippingPostCodeObj->Where = "ZoneID = '$CurrentShippingZone->ZoneID'";
				$ShippingPostCodeObj->TableSelectAll(array("PostCode"));
				$TotalPostCode = $ShippingPostCodeObj->GetNumRows();
				
				$ShippingPriceObj->Where = "ZoneID = '$CurrentShippingZone->ZoneID'";
				$ShippingPriceObj->TableSelectAll(array("ZoneID"));
				$TotalRange = $ShippingPriceObj->GetNumRows();
				
				
				$Status = 1;
				
				if($Section=="EditShippingZone" && $ZoneID==$CurrentShippingZone->ZoneID)
				{
					?>
					<form method="POST" action="<?php  echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php  echo $Page?>&Section=<?php  echo $Section?>&Shipping=<?php  echo $Shipping?>&ShippingID=<?php  echo $ShippingID?>&ZoneID=<?php  echo $ZoneID?>&Target=EditShippingZone" enctype="multipart/form-data">
						<tr class="InsideRightTd">
							<td align="center"><b><?php  echo $SNo?></b></td>
							<td align="left"><input type="text" name="ZoneName" value="<?php  echo MyStripSlashes($CurrentShippingZone->ZoneName);?>" size="20"></td>
							<td align="center"></td>
							<td align="center"><input type="submit" name="Submit" value="Update Shipping Zone"></td>
							<td align="center"><input type="button" name="button" value="Cancel" onclick="javascript:history.back();"></td>
							
						</tr>
					</form>
					<?php 		
				}
				else 
				{
					?>
						<tr class="InsideRightTd">
							<td align="center"><b><?php  echo $SNo?></b></td>
							<td align="left"><?php  echo MyStripSlashes($CurrentShippingZone->ZoneName);?>
							<?php  if($TotalPostCode > 0):?>
							<a rel="colorbox_ajax" href="<?php  echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php  echo $Page?>&Meta=false&Popup=true&Section=PostCodeView&Shipping=<?php  echo $Shipping?>&ShippingID=<?php  echo $ShippingID?>&ZoneID=<?php  echo $CurrentShippingZone->ZoneID?>" target="_blank">Postcodes list</a>
							<?php  endif;?>
							</td>
							<td align="center">
							<a href="<?php  echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php  echo $Page?>&Section=PostCode&Shipping=<?php  echo $Shipping?>&ShippingID=<?php  echo $ShippingID?>&ZoneID=<?php  echo $CurrentShippingZone->ZoneID?>">Post Code(<?php  echo $TotalPostCode?>)/Range(<?php  echo $TotalRange?>)</a>
							</td>
							<?php if($CurrentShippingZone->DefaultZone=="1"):?>
							<td align="center"></td>
							<td align="center"></td>
							<?php else:?>
							<td align="center"><a href="<?php  echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php  echo $Page?>&Section=EditShippingZone&Shipping=<?php  echo $Shipping?>&ShippingID=<?php  echo $ShippingID?>&ZoneID=<?php  echo $CurrentShippingZone->ZoneID?>" class="btn btn-success btn-phone-block"><icon class="icon-pencil icon-white"></icon> Edit</a></td>
							<td align="center"><a href="#" onclick="return FunctionDeleteShippingZone('<?php  echo $CurrentShippingZone->ZoneID?>','<?php  echo $Status?>');" class="btn btn-danger btn-phone-block"><icon class="icon-remove icon-white"></icon><b>Delete</b></a></td>
							<?php endif;?>
						
							
						</tr>
					<?php 
				}
			   $SNo++;
			}
			?>
			<form method="POST" action="<?php  echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php  echo $Page?>&Section=<?php  echo $Section?>&Shipping=<?php  echo $Shipping?>&ShippingID=<?php  echo $ShippingID?>&Target=AddShippingZone" enctype="multipart/form-data">
			<tr class="InsideRightTd">
				<td align="center"><b><?php  echo $SNo?></b></td>
				<td align="left"><input type="text" name="ZoneName" value="" size="20"></td>
				<td align="center"></td>
				<td align="center"><input type="submit" name="Submit" class="btn" value="Add Shipping Zone"></td>
				<td align="center">&nbsp;</td>
			</tr>
		</form>
		</tbody>
		</table>	
		
		<?php 
		break;
	default:
		$ShippingGroupObj->Where = "1";
		$ShippingGroupObj->TableSelectAll("","CreatedDate ASC");
		if($ShippingGroupObj->GetNumRows() > 0)
		{
		?>
		<table class="table table-striped table-bordered table-responsive block">
			<thead>
   			 <tr>
				<td align="center" width="5%"><b>S.No.</b></td>
				<td align="left"><b>Group Name</b></td>
				<td align="left"><b>Zones</b></td>
				<td align="center"><b>Edit</b></td>
				<td align="center"><b>Delete</b></td>
			</tr>
			</thead>
			<tbody>
			<script type="text/javascript">
				function FunctionDelete()
				{
					result = confirm("Are you sure want to delete the Shipping Zone?")
					if(result == true)
					{
						return true;
					}
					return false;
				}				
							
			</script>
			<?php 
			$SNo=1;
			$Count=1;
			
			
			while($CurrentShippingGroup = $ShippingGroupObj->GetObjectFromRecord())
			{
				
				$ShippingZoneObj->Where = "ShippingID = '$CurrentShippingGroup->ShippingID'";
				$ShippingZoneObj->TableSelectAll(array("ZoneID"));
				$TotalZone = $ShippingZoneObj->GetNumRows();
			?>
			<tr class="InsideRightTd">
				<td align="center"><b><?php  echo $SNo?></b></td>
				<td align="left"><b><?php  echo stripslashes($CurrentShippingGroup->ShippingGroup);?></b></td>
				<td align="left"><a href="<?php  echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php  echo $Page?>&Shipping=<?php  echo $Shipping?>&Section=ShippingZone&ShippingID=<?php  echo $CurrentShippingGroup->ShippingID?>">Zones (<?php  echo $TotalZone?>)</a></td>
				<td align="center"><a href="<?php  echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php  echo $Page?>&Shipping=<?php  echo $Shipping?>&Section=EditShipping&ShippingID=<?php  echo $CurrentShippingGroup->ShippingID?>" class="btn btn-success btn-phone-block"><icon class="icon-pencil icon-white"></icon> Edit</a></td>
				<td align="center">
				<?php 
				if($CurrentShippingGroup->ShippingID !="1")
				{
				?>
					<a href="<?php  echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php  echo $Page?>&Shipping=<?php  echo $Shipping?>&Target=DeleteShipping&ShippingID=<?php  echo $CurrentShippingGroup->ShippingID?>" onclick="return FunctionDelete();" class="btn btn-danger btn-phone-block"><icon class="icon-remove icon-white"></icon> Delete</a>
				<?php 
				}?>	
				</td>
			</tr>
			<?php 
			$SNo++;
			$Count++;
			}
		?>
		 </tbody>
		</table>
		<?php 
		}
		else 
		{
			?>
			<b>No Result Found.</b>
			<?php 
		}
	break;
}
?>
				