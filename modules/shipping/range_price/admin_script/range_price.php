<?php

$Shipping = isset($_GET['Shipping'])?$_GET['Shipping']:"";
$Section = isset($_GET['Section'])?$_GET['Section']:"";
$Target = isset($_GET['Target'])?$_GET['Target']:"";

$ShippingID = isset($_GET['ShippingID'])?$_GET['ShippingID']:0;
$TypeID = isset($_GET['TypeID'])?$_GET['TypeID']:"";
$ZoneID = isset($_GET['ZoneID'])?$_GET['ZoneID']:"";

$ShippingGroupObj = new DataTable(TABLE_SHIPPING_RZS_GROUPS);
$ShippingTypeObj = new DataTable(TABLE_SHIPPING_RZS_TYPES);
$ShippingZoneObj = new DataTable(TABLE_SHIPPING_RZS_ZONES);
$ShippingPostCodeObj = new DataTable(TABLE_SHIPPING_RZS_POSTCODES);
$ShippingPriceObj = new DataTable(TABLE_SHIPPING_RZS_PRICES);

$ShippingCountryNameObj = new DataTable(TABLE_SHIPPING_COUNTRIES);

$DataArray = array();			

if($ShippingID !=0)
{
	$ShippingGroupObj->Where ="ShippingID = '".$ShippingGroupObj->MysqlEscapeString($ShippingID)."'";
	$CurrentShipping = $ShippingGroupObj->TableSelectOne();	
}

switch ($Target)
{
	
	case "DeleteShipping":
		if($ShippingID !="1")
		{
			$ShippingCountryNameObj->Where = $ShippingGroupObj->Where = "ShippingID='".$ShippingGroupObj->MysqlEscapeString($ShippingID)."'";
			$ShippingGroupObj->TableDelete();	
			$ShippingCountryNameObj->TableUpdate(array("ShippingID"=>"1"));	
			
			@ob_clean();
			$_SESSION['InfoMessage'] ="Shipping Zone deleted successfully.";
			MyRedirect(DIR_WS_SITE_CONTROL."index.php?Page=$Page&Shipping=$Shipping&Section=$Section&QuotationID=$QuotationID");
			exit;
		}	
		break;
	case "AddShipping":
		if(isset($_POST['ChangeOption']))
				break;
		$DataArray = array();			
		$DataArray['ShippingGroup'] = isset($_POST['ShippingGroup'])?$_POST['ShippingGroup']:"";			
		if($ShippingID=="0")
		{
			$ShippingID = $ShippingGroupObj->GetMax("ShippingID") + 1;
			$DataArray['ShippingID'] = $ShippingID;
			$DataArray['CreatedDate'] = date('Ymd');
			$ShippingGroupObj->TableInsert($DataArray);
			
			@ob_clean();
			$_SESSION['InfoMessage'] ="Shipping zone added successfully.";
		}
		else 
		{
				$ShippingGroupObj->Where ="ShippingID = '".$ShippingGroupObj->MysqlEscapeString($ShippingID)."'";
				$ShippingGroupObj->TableUpdate($DataArray);
		
		}
		
		foreach ($_POST['CountryNameGo'] as $k=>$v)
		{
				$ShippingCountryNameObj->Where = "CountryID='".$ShippingCountryNameObj->MysqlEscapeString($v)."'";
				$ShippingCountryNameObj->TableUpdate(array("ShippingID"=>$ShippingID));
		}
					
		MyRedirect(DIR_WS_SITE_CONTROL."index.php?Page=$Page&Shipping=$Shipping&ShippingID=$ShippingID");
		exit;	
				
	break;
	
	case "AddShippingType":
	
		$DataArray = array();
		$DataArray['TypeName'] = isset($_POST['TypeName'])?$_POST['TypeName']:"";
		$DataArray['Position'] = (isset($_POST['Position']) && $_POST['Position'] !="")?$_POST['Position']:"0";
		$DataArray['CreatedDate'] = date('Y-m-d');
		$ShippingTypeObj->TableInsert($DataArray);
		@ob_clean();
		$_SESSION['InfoMessage'] ="ShippingType added successfully.";
		header("Location:".DIR_WS_SITE_CONTROL."index.php?Page=$Page&Section=$Section&Shipping=$Shipping");
		exit;
	
	break;
	case "EditShippingType":
		$DataArray = array();
		$DataArray['TypeName'] = isset($_POST['TypeName'])?$_POST['TypeName']:"";
		$DataArray['Position'] = (isset($_POST['Position']) && $_POST['Position'] !="")?$_POST['Position']:"0";
		
		$ShippingTypeObj->Where = "TypeID='".$ShippingTypeObj->MysqlEscapeString($TypeID)."'";
		$ShippingTypeObj->TableUpdate($DataArray);
			
		@ob_clean();
		$_SESSION['InfoMessage'] ="Shipping Type updated successfully.";
		header("Location:".DIR_WS_SITE_CONTROL."index.php?Page=$Page&Section=$Section&Shipping=$Shipping");
		exit;
	
	break;
	case "DeleteShippingType":
		$ShippingTypeObj->Where = "TypeID='".$ShippingTypeObj->MysqlEscapeString($TypeID)."'";
		$ShippingTypeObj->TableDelete();
					
		@ob_clean();
		$_SESSION['InfoMessage'] ="ShippingType deleted successfully.";
		header("Location:".DIR_WS_SITE_CONTROL."index.php?Page=$Page&Section=$Section&Shipping=$Shipping");
		exit;
	
	break;
	case "AddShippingZone":
	
		$ShippingZoneObj->Where = "ShippingID = '$ShippingID'";
		$ShippingZoneObj->TableSelectAll(array("ZoneID"));
		$TotalZone = $ShippingZoneObj->GetNumRows();
				
		$DataArray = array();
		$DataArray['ZoneName'] = isset($_POST['ZoneName'])?$_POST['ZoneName']:"";
		$DataArray['ShippingID'] = $ShippingID;
		$DataArray['CreatedDate'] = date('Y-m-d');
		
		if($TotalZone==0)
			$DataArray['DefaultZone'] = "1";
		
		$ShippingZoneObj->TableInsert($DataArray);
		
		@ob_clean();
		$_SESSION['InfoMessage'] ="ShippingZone added successfully.";
		header("Location:".DIR_WS_SITE_CONTROL."index.php?Page=$Page&Section=$Section&Shipping=$Shipping&ShippingID=$ShippingID");
		exit;
	
	break;
	case "EditShippingZone":
		$DataArray = array();
		$DataArray['ZoneName'] = isset($_POST['ZoneName'])?$_POST['ZoneName']:"";
		
		$ShippingZoneObj->Where = "ZoneID='".$ShippingZoneObj->MysqlEscapeString($ZoneID)."'";
		$ShippingZoneObj->TableUpdate($DataArray);
			
		@ob_clean();
		$_SESSION['InfoMessage'] ="Shipping Type updated successfully.";
		header("Location:".DIR_WS_SITE_CONTROL."index.php?Page=$Page&Section=$Section&Shipping=$Shipping&ShippingID=$ShippingID");
		exit;
	
	break;
	case "DeleteShippingZone":
		$ShippingZoneObj->Where = "ZoneID='".$ShippingZoneObj->MysqlEscapeString($ZoneID)."'";
		$ShippingZoneObj->TableDelete();
					
		@ob_clean();
		$_SESSION['InfoMessage'] ="ShippingZone deleted successfully.";
		header("Location:".DIR_WS_SITE_CONTROL."index.php?Page=$Page&Section=$Section&Shipping=$Shipping&ShippingID=$ShippingID");
		exit;
	
	break;
	case "AddRanges":
		if(count($_POST) > 0)
		{
			$ShippingPriceObj = new DataTable(TABLE_SHIPPING_RZS_PRICES);
			$ShippingPriceObj->Where = "ZoneID = '$ZoneID'";
			$ShippingPriceObj->TableDelete();
			
			$RangeMinArray = isset($_POST['RangeMin'])?$_POST['RangeMin']:array();
			if(count($RangeMinArray) > 0)
			{
				foreach($RangeMinArray as $TypeID=>$Arr)
				{
					foreach($Arr as $k=>$v)
					{
							$DataArray = array();
							$DataArray['ZoneID'] = $ZoneID;
							$DataArray['TypeID'] = $TypeID;
							$DataArray['RangeMin'] = isset($_POST['RangeMin'][$TypeID][$k])?$_POST['RangeMin'][$TypeID][$k]:"0";
							$DataArray['RangeMax'] = isset($_POST['RangeMax'][$TypeID][$k])?$_POST['RangeMax'][$TypeID][$k]:"0";
							$DataArray['Price'] = isset($_POST['Price'][$TypeID][$k])?$_POST['Price'][$TypeID][$k]:"0";
							$DataArray['Active'] = isset($_POST['Active'][$TypeID][$k])?$_POST['Active'][$TypeID][$k]:"0";
							$ShippingPriceObj->TableInsert($DataArray);							
					}
				}
			}				
			
			
			
			
			
		}
		@ob_clean();
		$_SESSION['InfoMessage'] ="Ranges added successfully.";
		header("Location:".DIR_WS_SITE_CONTROL."index.php?Page=$Page&Section=$Section&Shipping=$Shipping&ShippingID=$ShippingID&ZoneID=$ZoneID");
		exit;
	break;
	case "BulkPostCode":
		$BulkPostCode = isset($_POST['BulkPostCode'])?$_POST['BulkPostCode']:"";
		$SeparateSelect = isset($_POST['SeparateSelect'])?$_POST['SeparateSelect']:"";
		switch($SeparateSelect)
		{
			case "nl":
			$Separate = chr(13);
			break;
			case ",":
				$Separate = ",";
			break;
			case "":
			default:
				$Separate = isset($_POST['Separate'])?$_POST['Separate']:"";
			break;
		}
		
		foreach (explode($Separate,$BulkPostCode) as $Value)
		{
			if(trim($Value) != "")
			{
				$DataArray = array();
				$DataArray['ZoneID'] = $ZoneID;
				$DataArray['PostCode'] = trim($Value);
				$ShippingPostCodeObj->Where = "ZoneID = '$ZoneID' AND PostCode='".$ShippingPostCodeObj->MysqlEscapeString($DataArray['PostCode'])."'";
				$CurrentPostCode = $ShippingPostCodeObj->TableSelectOne();
				if(!isset($CurrentPostCode->PostCode))
				{
					$ShippingPostCodeObj->TableInsert($DataArray);
				}
			}
			
		}
		@ob_clean();
					
		$_SESSION['InfoMessage'] ="Postcodes added successfully.";
		header("Location:".DIR_WS_SITE_CONTROL."index.php?Page=$Page&Section=$Section&Shipping=$Shipping&ShippingID=$ShippingID&ZoneID=$ZoneID");
		exit;
	break;
	case "DeletePostCode":
		$PostCodeID = isset($_POST['PostCodeID'])?$_POST['PostCodeID']:"";
		
		if(isset($PostCodeID) && $PostCodeID !="" && is_array($PostCodeID) && count($PostCodeID) >0)
		{
			$ShippingPostCodeObj->Where = "ZoneID = '$ZoneID' AND PostCodeID in (".implode(",",$PostCodeID).")";
			$ShippingPostCodeObj->TableDelete();
		}
		
		@ob_clean();
		$_SESSION['InfoMessage'] ="Postcodes deleted successfully.";
		
		header("Location:".DIR_WS_SITE_CONTROL."index.php?Page=$Page&Section=$Section&Shipping=$Shipping&ShippingID=$ShippingID&ZoneID=$ZoneID");
		exit;
	break;
}
?>