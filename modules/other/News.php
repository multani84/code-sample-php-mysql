<?php
/*
 Table Used
*/
//define("TABLE_OTHER_NEWS", "zsk_other_news",true);

function ModuleOtherURLNews($URL,$Parameters="")
{
	parse_str($Parameters,$q_array);
	if(@$q_array['NewsID'] !="")
	{
		parse_str($Parameters,$q_array);
		$q_string = "";
		foreach ($q_array as $Key=>$Value)
		{
			if($Key !="Page" and $Key !="Other" and $Key !="NewsID")
				$q_string .=$Key."=".$Value."&";
		}
		if($q_string !="")
			$q_string = substr($q_string,0,-1);
			
		if(DEFINE_SEO_URL_ACTIVE=="1")
		{
			if(CACHE_IMPLEMENT=="1" && defined("DEFINE_URLNAME_OTHER_NEWS_".@$q_array['NewsID']))
				$CurrentValue = DEFINE_URLNAME_OTHER_NEWS_.@$q_array['NewsID'];
			else 
			{
				$NewsObj = new DataTable(TABLE_OTHER_NEWS);
				$NewsObj->Where ="NewsID='".$NewsObj->MysqlEscapeString(@$q_array['NewsID'])."'";
				$CurrentNews = $NewsObj->TableSelectOne(array("URLName"));
				$CurrentValue = "news-".$CurrentNews->URLName;

				$SeoObj = new DataTable(TABLE_SEO);
				$SeoObj->Where ="RefrenceType = '".$SeoObj->MysqlEscapeString($Type)."' AND ReferenceID='".$SeoObj->MysqlEscapeString($ID)."'";
				$CurrentPage = $SeoObj->TableSelectOne(array("URLName","RedirectEnabled","RedirectURL"));
				$CurrentValue = isset($CurrentPage->URLName)?$CurrentPage->URLName:"";				
			}
			$URL = $CurrentValue.".html";
			$URL = $q_string !=""?$URL."?".$q_string:$URL;
		}
		else 
			$URL = $Parameters !=""?$URL."?".$Parameters:$URL;
	}
	else 
		$URL = $Parameters !=""?$URL."?".$Parameters:$URL;
	
	return DIR_WS_SITE.$URL;
}


function GetNewsLeftPanel($Y,$M)
{
	return "";
	$String = '<div class="NewsLeftPanel">
					<h3>News Headlines -'.$Y.'</h3>
                    <div class="MonthLinks">';
	for($i=1;$i<=12;$i++)
	{
		$String .= '<a href="'.MakeModuleURL("index.php","Page=other&Other=News&Y=$Y&M=$i").'" '.(($M==$i)?"class='Active'":"").' >'.date("M", mktime(0, 0, 0, $i, 1, $Y)).'</a>';

		if($i%6 !=0)
		$String .='| ';
	}
	
	$NewsObj = new DataTable(TABLE_OTHER_NEWS);
	$NewsObj->Where = "Active='1'";
	if($M !="")
	{
		$Title = date("F Y", mktime(0, 0, 0, $M, 1, $Y));
		$NewsObj->Where .= " AND DATE_FORMAT(NewsDate,'%c-%Y') ='".$NewsObj->MysqlEscapeString($M."-".$Y)."'";
	}
	else
	{ 
		$Title =$Y;
		$NewsObj->Where .= " AND DATE_FORMAT(NewsDate,'%Y') ='".$NewsObj->MysqlEscapeString($Y)."'";
	}
				
	$NewsObj->TableSelectAll(array("*","DATE_FORMAT(NewsDate,'%d.%m.%Y') as MyNewsDate"),"NewsDate DESC",5);
					
	
	$String .= '</div>';
	
					/*
					$String .= '<div class="ItemWrapper"><strong>'.$Title.'</strong><br />
                    ';
			       while ($NewsTmp =$NewsObj->GetObjectFromRecord() ) 
					{ 
						$String .= '<span class="Date">'.$NewsTmp->MyNewsDate.'</span><br />
		                    <a href="'.MakeModuleURL("index.php","Page=other&Other=News&Section=Detail&NewsID=".$NewsTmp->NewsID).'">'.MyStripSlashes($NewsTmp->NewsTitle).'&raquo;</a>
		                    <div class="NewsItemSpacer"></div>';
					}            
	
                   $String .= '</div>
                   
                    
                    <div class="Content">
                      <strong>Archives</strong><br />';
	
					$NewsObj = new DataTable(TABLE_OTHER_NEWS);
					$NewsObj->Where = "Active='1'";
					$NewsObj->TableSelectAll(array("distinct(DATE_FORMAT(NewsDate,'%Y')) as MyNewsDate"),"NewsDate DESC");
					while ($YearObj =$NewsObj->GetObjectFromRecord() ) 
					{
						$String .='<a href="'.MakeModuleURL("index.php","Page=other&Other=News&Y=".$YearObj->MyNewsDate).'" class="RedLink">'.$YearObj->MyNewsDate.'</a><br />';
			       }
			       	
			       $String .='</div>';
			       */
					
			       $String .='</div><br><br>';
	return $String;
}

?>