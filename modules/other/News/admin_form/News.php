<table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr>
		<td height="20" colspan="2" align="center">
		<h4>News</h4>
	</td>
	</tr>
	<tr>
		<td height="20" align="left">
		<a href="<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php echo $Page?>&Other=<?php echo $Other?>" class="adm-link">News</a>
		<?php
			if(isset($CurrentNews->NewsID) && $CurrentNews->NewsID !="")
			{
			?>
			&nbsp;&raquo;<a href="<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php echo $Page?>&Other=<?php echo $Other?>" class="adm-link"><?php echo isset($CurrentNews->NewsTitle)?MyStripSlashes($CurrentNews->NewsTitle):""?></a>
			<?php
			}?>
		</td>
		<td height="20" align="right">
			<a href="<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php echo $Page?>&Other=<?php echo $Other?>&Section=AddNews" class="adm-link">Add News</a>&nbsp;&nbsp;
		</td>
	</tr>
</table>				
<?php
//// Section start
switch($Section)
{
	case "Files":
	case "EditFile":
		$FileObj->Where ="ReferenceID='$NewsID' AND ReferenceType='$Other'";
		$FileObj->TableSelectAll("","Position ASC");
		?>
			<script type="text/javascript">
				function FunctionDeleteFile()
				{
					result = confirm("Are you sure want to delete the File?")
					if(result == true)
					{
						return true;
					}
					return false;
				}				
							
			</script>
		<table border="0" cellpadding="3" cellspacing="1" width="100%" class="InsideTable">
			<tr class="InsideLeftTd">
				<td align="center" width="5%"><b>S.No.</b></td>
				<td align="left"><b>File</b></td>
				<td align="left"><b>Type</b></td>
				<td align="center"><b>Active</b></td>
				<td align="center"><b>Position</b></td>
				<td align="center"></td>
				<td align="center"></td>
			</tr>
	<?php
			$SNo=1;
			$Count=1;
			while($CurrentFile = $FileObj->GetObjectFromRecord())
			{
				if($Section=="EditFile" && $FileID==$CurrentFile->FileID)
				{
					?>
					<form method="POST" action="<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php echo $Page?>&Section=<?php echo $Section?>&Other=<?php echo $Other?>&NewsID=<?php echo $NewsID?>&FileID=<?php echo $FileID?>&Target=EditFile" enctype="multipart/form-data">
						<tr class="InsideRightTd">
							<td align="center"><b><?php echo $SNo?></b></td>
							<td align="left">
							<?phpSKDisplayFileInfo($CurrentFile)?>
							<br><input type="file" name="Upload">
							</td>
							<td>
							<select name="UploadType">
								<?php
								foreach ($ProductFileTypeArray as $k=>$v)
								{
								?>
									<option value="<?php echo $k?>" <?php echo $CurrentFile->UploadType==$k?"selected":""?>><?php echo $v?></option>
									<?php
								}?>
								</select>
							</td>
							<td align="center"><input type="checkbox" name="Active" value="1" <?php echo $CurrentFile->Active=="1"?"checked":""?> class="chk"></td>
							<td align="center"><input type="text" name="Position" value="<?php echo MyStripSlashes($CurrentFile->Position);?>" size="5" style="text-align:center;"></td>
							<td align="center" ><input type="submit" name="Submit" value="Update"></td>
							<td align="center" ><input type="button" name="button" value="Cancel" onclick="javascript:history.back();"></td>
						</tr>
						<tr><td colspan="7"><hr></td></tr>
					</form>
					<?php		
				}
				else 
				{
					?>
					<tr class="InsideRightTd">
						<td align="center"><b><?php echo $SNo?></b></td>
						<td align="left">
						<?phpSKDisplayFileInfo($CurrentFile)?>
						</TD>
						<td><?php echo isset($ProductFileTypeArray[$CurrentFile->UploadType])?$ProductFileTypeArray[$CurrentFile->UploadType]:""?></td>
						<td align="center"><img src="<?php echo DIR_WS_SITE_CONTROL_IMAGES.($CurrentFile->Active=="1"?"info.gif":"error.gif")?>">
						</td>
						<td align="center"><?php echo MyStripSlashes($CurrentFile->Position);?>
						</td>
						<td align="center"><a href="<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php echo $Page?>&Other=<?php echo $Other?>&Section=EditFile&NewsID=<?php echo $NewsID?>&FileID=<?php echo $CurrentFile->FileID?>"><b>Edit</b></a></td>
						<td align="center"><a href="<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php echo $Page?>&Other=<?php echo $Other?>&Section=<?php echo $Section?>&NewsID=<?php echo $NewsID?>&Target=DeleteFile&FileID=<?php echo $CurrentFile->FileID?>" onclick="return FunctionDeleteFile();"><b>Delete</b></a></td>
					</tr>
					<tr><td colspan="7"><hr></td></tr>
					<?php
				}
			$SNo++;
			$Count++;
			}
		?>
		<form method="POST" action="<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php echo $Page?>&Other=<?php echo $Other?>&Section=<?php echo $Section?>&NewsID=<?php echo $NewsID?>&Target=AddFile" enctype="multipart/form-data">
			<tr class="InsideRightTd">
				<td align="center"><b><?php echo $SNo?></b></td>
				<td align="left"><input type="file" name="Upload"></td>
				<td>
					<select name="UploadType">
					<?php
					foreach ($ProductFileTypeArray as $k=>$v)
					{
					?>
						<option value="<?php echo $k?>"><?php echo $v?></option>
						<?php
					}?>
					</select>
				</td>
				<td align="center"><input type="checkbox" name="Active" value="1" class="chk"></td>
				<td align="center"><input type="text" name="Position" size="5"></td>
				<td align="center" colspan="2"><input type="submit" name="Submit" value="Add"></td>
			</tr>
		</form>
		</table>	
		<?php
	break;
	case "AddNews" :
	case "EditNews" :
	/////// Add News  Start
	?>
	<form method="POST" action="<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php echo $Page?>&Other=<?php echo $Other?>&Section=<?php echo $Section?>&Target=AddNews&NewsID=<?php echo $NewsID?>" enctype="multipart/form-data">
    <div class="well">
		<table border="0" cellpadding="3" cellspacing="1" width="100%" class="InsideTable">
			<tr>
				<td align="right" class="InsideLeftTd"><b>News Title</b></td>
				<td align="left" class="InsideRightTd"><input type="text" name="NewsTitle" value="<?php echo isset($_POST['NewsTitle'])?$_POST['NewsTitle']:(isset($CurrentNews->NewsTitle)?MyStripSlashes($CurrentNews->NewsTitle):"")?>" size="60"></td>
			</tr>
			<tr>
				<td align="right" class="InsideLeftTd"><b>News Date</b></td>
				<td align="left" class="InsideRightTd">
					<table cellpadding="0" cellspacing="0" border="0">
						<tr>
							<td valign="middle"><input type="text" name="NewsDate"  ID="NewsDate" value="<?php echo (isset($_POST['NewsDate'])?$_POST['NewsDate']:(isset($CurrentNews->NewsDate)?$CurrentNews->NewsDate:""))?>" size="25" ReadOnly></td>
							<td valign="middle">
								<a href="#" id="SelectDate2"><img style="BORDER-RIGHT:0px; BORDER-TOP:0px; BORDER-LEFT:0px; BORDER-BOTTOM:0px" src="<?php echo DIR_WS_SITE_INCLUDES_JAVASCRIPT?>calendar/img.gif"></a>
								<script type="text/javascript">
									Calendar.setup({
											inputField     :    "NewsDate",     // id of the input field
											ifFormat       :    "%Y-%m-%d",     // format of the input field
											button         :    "SelectDate2",  // trigger for the calendar (button ID)
											align          :    "Bl",           // alignment (defaults to "Bl")
											singleClick    :    true,
											timeFormat	   :	24,
											showsTime	   :	false
										});
								</script>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<?php
			if(@constant("SEO_URL_ACTIVE") =="1")
			{
			?>
			<tr>
				<td align="center" class="InsideLeftTd"><b>News URL</b>
				</td>
				<td align="left" class="InsideRightTd">
				news-<input type="text" name="URLName" value="<?php echo isset($_POST['URLName'])?MyStripSlashes($_POST['URLName']):(isset($CurrentNews->URLName)?MyStripSlashes($CurrentNews->URLName):"")?>" size="70" onblur="return CheckSpecialSymbol(this);" title="Please do not use special symbol. Only use alfa numeric characters,(_) underscores or (-) dashes.">
				<br><small>Please do not use special symbol. 
				<br>Only use alfa numeric characters,(_) underscores or (-) dashes.</small>
				</td>
			</tr>
			<?php
			}?>
			<?php
			if(@constant("SEO_META_ACTIVE") =="1")
			{
			?>									
			<tr>
				<td align="center" colspan="2"><hr></td>
			</tr>
			<tr>
				<td align="center" class="InsideLeftTd"><b>Meta Title</b></td>
				<td align="left" class="InsideRightTd"><input type="text" name="MetaTitle" value="<?php echo isset($_POST['MetaTitle'])?MyStripSlashes($_POST['MetaTitle']):(isset($CurrentNews->MetaTitle)?MyStripSlashes($CurrentNews->MetaTitle):"")?>" size="100"></td>
			</tr>
			<tr>
				<td align="center" class="InsideLeftTd"><b>Meta Keywords</b></td>
				<td align="left" class="InsideRightTd"><textarea name="MetaKeywords" cols="60" rows="3"><?php echo isset($_POST['MetaKeywords'])?MyStripSlashes($_POST['MetaKeywords']):(isset($CurrentNews->MetaKeywords)?MyStripSlashes($CurrentNews->MetaKeywords):"")?></textarea></td>
			</tr>
			<tr>
				<td align="center" class="InsideLeftTd"><b>Meta Description</b></td>
				<td align="left" class="InsideRightTd"><textarea name="MetaDescription" cols="60" rows="3"><?php echo isset($_POST['MetaDescription'])?MyStripSlashes($_POST['MetaDescription']):(isset($CurrentNews->MetaDescription)?MyStripSlashes($CurrentNews->MetaDescription):"")?></textarea></td>
			</tr>
			<?php
			}?>
			<tr>
				<td align="center" colspan="2"><hr></td>
			</tr>									
			<tr>
				<td align="right" class="InsideLeftTd"><b>Description</b></td>
				<td align="left" class="InsideRightTd">
				<textarea name="Description" cols="40" rows="4"><?php echo isset($_POST['Description'])?$_POST['Description']:(isset($CurrentNews->Description)?MyStripSlashes($CurrentNews->Description):"")?></textarea>
				</td>
			</tr>
			<tr>
				<td align="right" class="InsideLeftTd"><b>Long Description</b></td>
				<td align="left" class="InsideRightTd">
				<!---Start content editor--->
						<?php
				$sContent = isset($_POST['LongDescription'])?MyStripSlashes($_POST['LongDescription']):(isset($CurrentNews->LongDescription)?MyStripSlashes($CurrentNews->LongDescription):"");
				$SKEditorObj->CArray = array("Width"=>"700",
											 "Height"=>"400",
											 "DMode"=>"Large2",
											 "Help"=>true,
											 "EditorArray"=>array("css"=>DIR_WS_SITE_CONTROL."style.css",
											 					  ),
											 ); 
				$SKEditorObj->CreateEditor("LongDescription",$sContent);?>
						<!---end content editor--->
				</td>
			</tr>
			<tr>
				<td align="center" class="InsideLeftTd"><b>Image</b><br>
				</td>
				<td align="left" class="InsideRightTd" valign="middle">
					<input type="file" name="Upload" size="30">
					&nbsp;&nbsp;
					<?php
					if(isset($CurrentNews->Image) && $CurrentNews->Image !="" && file_exists(DIR_FS_SITE_UPLOADS_OTHER_ORIGINAL.$CurrentNews->Image))
					{?>
						<div style="float:left;">
							<?php SKImgDisplay(DIR_FS_SITE_UPLOADS_OTHER_ORIGINAL.$CurrentNews->Image,100,MyStripSlashes($CurrentNews->NewsTitle));?>
							<br>
							<input type="checkbox" name="RemoveUpload" id="RemoveUpload" value="1"> <label for="RemoveUpload">Remove this?</label>
						</div>	
				  <?php }?>
							
				</td>
			</tr>
			<tr>
				<td align="right" class="InsideLeftTd"><b>Active</b></td>
				<td align="left" class="InsideRightTd">
					<input type="checkbox" name="Active" value="1" class="chk" <?php echo (isset($_POST['Active']) && $_POST['Active']==1)?"checked":((isset($CurrentNews->Active) && $CurrentNews->Active==1)?"checked":"")?>>
				</td>
			</tr>
			<tr>
				<td align="right" class="InsideLeftTd"><b>HomePage</b></td>
				<td align="left" class="InsideRightTd">
					<input type="checkbox" name="HomePage" value="1" class="chk" <?php echo (isset($_POST['HomePage']) && $_POST['HomePage']==1)?"checked":((isset($CurrentNews->HomePage) && $CurrentNews->HomePage==1)?"checked":"")?>>
				</td>
			</tr>
			<?php /*?><tr>
				<td align="center" class="InsideLeftTd"><b>Upper Banner</b></td>
				<td align="left" class="InsideRightTd">
					<select name="UpperBanner1" id="UpperBanner1">
					<option value="">--Select--</option>
					<?php
						$BannerObj = new DataTable(TABLE_BANNERS);
						$BannerObj->Where = "Alignment='T'";
						$BannerObj->TableSelectAll("","Position ASC");
		
					while($CurrentBanner = $BannerObj->GetObjectFromRecord())
					{?>
						<option value="<?php echo $CurrentBanner->BannerID?>" <?php echo (isset($_POST['UpperBanner1']) && $_POST['UpperBanner1']==$CurrentBanner->BannerID)?"selected":((isset($CurrentNews->UpperBanner1) && $CurrentNews->UpperBanner1==$CurrentBanner->BannerID)?"selected":"")?>><?php echo $CurrentBanner->BannerName?></option>
						<?php
					}?>
					</select>
				</td>
			</tr>
			<?php*/?>
			<tr>
				<td align="right" class="InsideLeftTd"></td>
				<td align="left" class="InsideRightTd">
					<input type="submit" name="AddNews" value="Submit">
				</td>
			</tr>
		</table>
		</div>				
	</form>
	<?php
	/////// Add News End
	break;

	/////////Display News 
	case "DisplayNews" :
	default:
		$NewsObj->Where = "1";
		$NewsObj->TableSelectAll(array("*","DATE_FORMAT(NewsDate,'%b %d, %Y') as MyNewsDate"),"NewsDate DESC");
		if($NewsObj->GetNumRows() > 0)
		{
		?>
		<script>
				function FunctionDeleteNews(NewsID)
				{
					
						result = confirm("Are you sure want to delete the news?")
						if(result == true)
						{
							URL="<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php echo $Page?>&Other=<?php echo $Other?>&Target=DeleteNews&&NewsID=" + NewsID;
							location.href=URL;
							return true;
						}
					return false;
				}				
			</script>							
		<form method="POST" action="<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php echo $Page?>&Other=<?php echo $Other?>&Section=<?php echo $Section?>&Target=UpdateNews">
		<table class="table table-striped table-bordered table-responsive block">
			<tr>
				<td align="center" width="5%" class="InsideLeftTd"><b>S.No.</b></td>
				<td align="left"  class="InsideLeftTd"><b>News Title</b></td>
				<td align="center"  class="InsideLeftTd"><b>News Date</b></td>
				<td align="center"  class="InsideLeftTd"><b>Image</b></td>
				<td align="center"  class="InsideLeftTd"><b>Active</b></td>
				<td align="center"  class="InsideLeftTd"><b>HomePage</b></td>
				<td align="center"  class="InsideLeftTd"><b>Edit</b></td>
				<td align="center"  class="InsideLeftTd"><b>Delete</b></td>
			</tr>
			<?php
			$SNo=1;
			$Count=1;
			while($CurrentNews = $NewsObj->GetObjectFromRecord())
			{
					
				?>
			<tr >
				<td align="center" class="InsideRightTd"><b><?php echo $SNo?></b></td>
				<td align="left" class="InsideRightTd">
				<?php echo MyStripSlashes($CurrentNews->NewsTitle);?>			
				<input type="hidden" name="NewsID_<?php echo $Count?>" value="<?php echo $CurrentNews->NewsID?>"></td>
				<td align="center" class="InsideRightTd"><?php echo isset($CurrentNews->MyNewsDate)?$CurrentNews->MyNewsDate:"";?></td>
				<td align="center" class="InsideRightTd">
				<?php SKImgDisplay(DIR_FS_SITE_UPLOADS_OTHER_ORIGINAL.$CurrentNews->Image,100,MyStripSlashes($CurrentNews->NewsTitle));?>
				</td><td align="center" class="InsideRightTd">
					<input type="checkbox" name="Active_<?php echo $Count?>" value="1" class="chk" <?php echo (isset($CurrentNews->Active) && $CurrentNews->Active==1)?"checked":""?>>
				</td>
				<td align="center" class="InsideRightTd">
					<input type="checkbox" name="HomePage_<?php echo $Count?>" value="1" class="chk" <?php echo (isset($CurrentNews->HomePage) && $CurrentNews->HomePage==1)?"checked":""?>>
				</td>
				<td align="center" class="InsideRightTd"><a href="<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php echo $Page?>&Other=<?php echo $Other?>&Section=EditNews&NewsID=<?php echo $CurrentNews->NewsID?>"><b>Edit</b></a></td>
				<td align="center" class="InsideRightTd">
					<a href="#" onclick="return FunctionDeleteNews('<?php echo $CurrentNews->NewsID?>');"><b>Delete</b></a>
				</td>
			</tr>
			<?php
			$SNo++;
			$Count++;
			}
		?>
		<tr class="InsideLeftTd">
				<td align="center" width="5%"></td>
				<td align="center"></td>
				<td align="center"></td>
				<td align="center"><input type="hidden" name="Count" value="<?php echo $Count?>"></td>
				<td align="center"><input type="submit" name="UpdateActive" value="Update"></td>
				<td align="center"><input type="submit" name="UpdateHomePage" value="Update"></td>
				<td align="center"></td>
				<td align="center"></td>
			</tr>
	
		</table>
		</form>					
		<?php
		}
		else 
		{
			?>
			<b>No Result Found.</b>
			<?php
		}
	break;
}
///// Section end?>