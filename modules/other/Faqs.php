<?php
/*
 Table Used
*/
function ModuleOtherURLFaqs($URL,$Parameters="")
{
	parse_str($Parameters,$q_array);
	if(@$q_array['FaqID'] !="")
	{
		parse_str($Parameters,$q_array);
		$q_string = "";
		foreach ($q_array as $Key=>$Value)
		{
			if($Key !="Page" and $Key !="Other" and $Key !="FaqID")
				$q_string .=$Key."=".$Value."&";
		}
		if($q_string !="")
			$q_string = substr($q_string,0,-1);
			
		if(DEFINE_SEO_URL_ACTIVE=="1")
		{
			if(CACHE_ACTIVE=="1" && defined("DEFINE_URLNAME_OTHER_FAQS_".@$q_array['FaqID']))
				$CurrentValue = DEFINE_URLNAME_OTHER_FAQS_.@$q_array['FaqID'];
			else 
			{
				$FaqObj = new DataTable(TABLE_OTHER_FAQS);
				$FaqObj->Where ="FaqID='".$FaqObj->MysqlEscapeString(@$q_array['FaqID'])."'";
				$CurrentFaq = $FaqObj->TableSelectOne(array("URLName"));
				$CurrentValue = "faq-".$CurrentFaq->URLName;
			}
			$URL = $CurrentValue.".html";
			$URL = $q_string !=""?$URL."?".$q_string:$URL;
		}
		else 
			$URL = $Parameters !=""?$URL."?".$Parameters:$URL;
	}
	else 
		$URL = $Parameters !=""?$URL."?".$Parameters:$URL;
	
	return DIR_WS_SITE.$URL;
}

Function GetFaqsForLeftSide($SelectedID=0)
{
	$String ="";
	$FaqObj = new DataTable(TABLE_OTHER_FAQS. " at, ".TABLE_OTHER_CATEGORY_REF. " atc");
	$FaqObj->Where = "at.FaqID=atc.ReferenceID and atc.CategoryID='9'";
	$FaqObj->Where .= " AND at.Active='1'";
	$FaqObj->TableSelectAll(array("at.FaqID","at.FaqTitle","at.HomePage"),"at.Position ASC,at.FaqDate DESC");
	$TopFaqs ="";
	$SelectFaqs ="";
	while ($TmpFaq= $FaqObj->GetObjectFromRecord())
	{
		if($TmpFaq->HomePage =="1")
			$TopFaqs .= '<a href="'.MakeModuleURL("index.php","Other=Faqs&FaqID=$TmpFaq->FaqID").'" '.(($SelectedID==$TmpFaq->FaqID)?"class='Active'":"").' >'.MyStripSlashes($TmpFaq->FaqTitle).'</a><br>';	
		
		$SelectFaqs .='<option value="'.MakeModuleURL("index.php","Other=Faqs&FaqID=$TmpFaq->FaqID").'" '.(($SelectedID==$TmpFaq->FaqID)?"selected":"").'>'.MyStripSlashes($TmpFaq->FaqTitle).'</option>';
	}
	
	$String='<div class="LightGreyPanel">
					<h3 class="Bg"><span class="Red">The</span>Team</h3>
					<img src="'.DIR_WS_SITE_TEMPLATES.DEFINE_TEMPLATE_STYLE.'/images/reflex-red.jpg" vspace="0" border="0" alt="" />
					<div class="ItemWrapper"><img src="'.DIR_WS_SITE_TEMPLATES.DEFINE_TEMPLATE_STYLE.'/images/down-arrow.png" vspace="5" border="0" alt="" /> <br />
	                    <div class="TeamMembers">'.$TopFaqs.'
	                    <select name="select" onchange="document.location.href=this.value;">
	                      <option value="#" >View full team list</option>
	                      '.$SelectFaqs.'
	                    </select><br /><br /></div>
        		</div>
     		  </div>';

	return $String;
}
?>