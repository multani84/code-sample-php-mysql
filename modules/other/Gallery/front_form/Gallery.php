<div class="border"></div>
<div id="other_pages">
	<div id="left_other_page">
			<?php echo isset($SkLeftBannerString)?$SkLeftBannerString:""?>
	   		<?php
    	  $GalleryObj = new DataTable(TABLE_OTHER_GALLERY);
          $GalleryObj->Where = "HomePage='1' AND Active ='1'";
          $GalleryObj->TableSelectAll(array("*,DATE_FORMAT(PostDate,'%b %d, %Y') as MyPostDate"),"PostDate DESC,ItemID DESC",2);
          if($GalleryObj->GetNumRows() >0)
          {
          ?>
           <br />
           <div class="nav_head">
				<div class="nav_head_text">Latest News</div>
			</div>
			<div class="news_latest">
     		<?php
			while($CurrentGallery = $GalleryObj->GetObjectFromRecord())
			{
			?>	
			 <a href="<?php echo SKSEOURL($CurrentGallery->ItemID,"other_gallery")?>"><?php echo isset($CurrentGallery->ItemTitle)?MyStripSlashes($CurrentGallery->ItemTitle):""?></a><br />
		      <i><?php echo isset($CurrentGallery->MyPostDate)?MyStripSlashes($CurrentGallery->MyPostDate):""?></i><br />
		      <?php echo isset($CurrentGallery->Description)?MyStripSlashes(nl2br($CurrentGallery->Description)):""?>
		      <br />
		      <br />
    
			  <?php
			}?>
			</div>
		  <?php
          }?>
	</div>	

	<div id="right_other_page2"><?php // right panel start?>  
		<?php
switch ($Section)
{
	case "Detail":
		if($ItemID !=0)
		{
			$GalleryObj->Where = "ItemID='".$GalleryObj->MysqlEscapeString($ItemID)."'";
			$CurrentGallery = $GalleryObj->TableSelectOne(array("*,DATE_FORMAT(PostDate,'%b %d, %Y') as MyPostDate"));
		}	
		?>
		<div class='heading_text_products'><?php echo isset($CurrentGallery->ItemTitle)?MyStripSlashes($CurrentGallery->ItemTitle):""?></div>
      	<?php
		   if(file_exists(DIR_FS_SITE_UPLOADS_OTHER_ORIGINAL.$CurrentGallery->Image) && $CurrentGallery->Image !="")
		   {
		   ?>
		   		<div style="float:left;padding:0px 10px 10px 0px">
		   		<a href="<?php echo SKSEOURL($CurrentGallery->ItemID,"other_gallery")?>"><?phpSKImgDisplay(DIR_FS_SITE_UPLOADS_OTHER_ORIGINAL.$CurrentGallery->Image,240,MyStripSlashes($CurrentGallery->ItemTitle));?></a>
		   		</div>
		   		<?php
		   }?>
		<?php echo isset($CurrentGallery->LongDescription)?MyStripSlashes(nl2br($CurrentGallery->LongDescription)):""?> 
    	<?php
			$FileObj = NEW DataTable(TABLE_OTHER_FILES);
        	$FileObj->Where ="ReferenceID='".$FileObj->MysqlEscapeString($ItemID)."' AND 	UploadType='Image' AND ReferenceType='".$FileObj->MysqlEscapeString($Other)."'";
			$FileObj->TableSelectAll("","Position ASC");
			if($FileObj->GetNumRows() >0)
			{
				?>
				<div class="NewsGallery">
				<ul>
				<?php
				while($CurrentFile = $FileObj->GetObjectFromRecord())
				{
					?>
					<li><a class="group1" href="<?php echo SKImgDisplay(DIR_FS_SITE_UPLOADS_OTHER_ORIGINAL.$CurrentFile->Upload,500,"","",true,true);?>" title="<?php echo MyStripSlashes($CurrentGallery->ItemTitle)?>" ><?phpSKImgDisplay(DIR_FS_SITE_UPLOADS_OTHER_ORIGINAL.$CurrentFile->Upload,"100,100");?></a></li>
					<?php
				}
				?>
				</ul>
				</div>
				<?php
			}
	break;
	case "Archive":
?>
<?php
break;	
default:
?>
<div class='heading_text_products'>Latest News</div>
<?php
	$GalleryObj = new DataTable(TABLE_OTHER_GALLERY);
	//$GalleryObj->DisplayQuery = true;
	$GalleryObj->Where = "Active='1'";
	//print_r($D);
	//exit;
	if($D !="")
	{
		
		$GalleryObj->Where .= " AND DATE_FORMAT(PostDate,'%d-%c-%Y') ='".$GalleryObj->MysqlEscapeString($D."-".$M."-".$Y)."'";
	}
	else 
	{
	if($M !="")
	
		$GalleryObj->Where .= " AND DATE_FORMAT(PostDate,'%c-%Y') ='".$GalleryObj->MysqlEscapeString($M."-".$Y)."'";
	else
		$GalleryObj->Where .= " AND DATE_FORMAT(PostDate,'%Y') ='".$GalleryObj->MysqlEscapeString($Y)."'";
	}
				
	$GalleryObj->AllowPaging =true;
	$GalleryObj->PageSize=10;
	$GalleryObj->PageNo =isset($_GET['PageNo'])?$_GET['PageNo']:1;	
	$GalleryObj->TableSelectAll(array("*","DATE_FORMAT(PostDate,'%b %d, %Y') as MyPostDate"),"PostDate DESC");
	$TotalRecords = $GalleryObj->TotalRecords ;
	$TotalPages =  $GalleryObj->TotalPages;
	
	$SNo=0;
	while($CurrentGallery = $GalleryObj->GetObjectFromRecord())
	{
	$SNo++;?>
	<table width="98%" border="0" cellspacing="0" cellpadding="0" class="news_grid" style="margin-top:15px;">
        <tr>
          <td width="32%"><a href="<?php echo SKSEOURL($CurrentGallery->ItemID,"other_gallery")?>">
		  <?phpSKImgDisplay(DIR_FS_SITE_UPLOADS_OTHER_ORIGINAL.$CurrentGallery->Image,200,MyStripSlashes($CurrentGallery->ItemTitle));?></a></td>
          <td width="68%" valign="top"><a href="<?php echo SKSEOURL($CurrentGallery->ItemID,"other_gallery")?>"><?php echo isset($CurrentGallery->ItemTitle)?MyStripSlashes($CurrentGallery->ItemTitle):""?></a><br />
            <i><?php echo isset($CurrentGallery->MyPostDate)?MyStripSlashes($CurrentGallery->MyPostDate):""?></i><br />
            <?php echo isset($CurrentGallery->Description)?MyStripSlashes(nl2br($CurrentGallery->Description)):""?></td>
          </tr>
        </table>
      <hr />
    <?php
	}?>

<?php
if($TotalRecords > $GalleryObj->PageSize)
	{
	?>
	<br>
	<table cellpadding="1" width="100%" cellspacing="1" border="0">
		<tr>
		<td width="100%" align="left">
		<div class="paging"><b>Total: <?php echo $TotalRecords?> records</b>&nbsp;&nbsp;<?php echo $GalleryObj->GetPagingLinks(MakeModuleURL("index.php","Page=other&Other=News$Y=$Y&M=$M&PageNo="),PAGING_FORMAT_NUMBERED,"","");?></div>
		</td>	
		</tr>
	</table>
	<br>
	<?php
	}
	if($TotalRecords==0)
		echo "<b>No record found.</b>";
break;
}?>	

	</div><?php // right panel end?> 
</div>
 <script>
	jQuery(document).ready(function($){
		$(".group1").colorbox({rel:'group1'});
	});
	</script>
    