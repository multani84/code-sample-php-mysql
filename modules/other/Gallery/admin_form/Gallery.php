<!-- Heading  -->
<div class="row-fluid">
	<div class="span12">
		<h1>Gallery	- <?php  echo $Label;?></h1>
		<hr />
	</div>
	<div class="pull-left">
		<a href="<?php  echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php  echo $Page?>&Other=<?php  echo $Other?>&Alignment=PH" class="adm-link">Photos</a>&nbsp;&nbsp;
	|&nbsp;&nbsp;<a href="<?php  echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php  echo $Page?>&Other=<?php  echo $Other?>&Alignment=VD" class="adm-link">Video</a> &nbsp;&nbsp;
	<br>
	</div>
	<div class="pull-right">
	<?php  if($Label!=""){?>
			<a href="<?php  echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php  echo $Page?>&Other=<?php  echo $Other?>&Section=AddGallery&Alignment=<?php  echo $Alignment?>" class="adm-link">Add <?php  echo $Label;?></a>&nbsp;&nbsp;
		<?php  }?>
	</div>
<div class="clearfix separator bottom"></div>
<!--  End Heading-->	
			
<?php 
//// Section start
switch($Section)
{
	case "AddGallery" :
	case "EditGallery" :
	/////// Add News  Start
	?>
	<form class="form-horizontal" method="POST" action="<?php  echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php  echo $Page?>&Other=<?php  echo $Other?>&Section=<?php  echo $Section?>&Target=AddGallery&ItemID=<?php  echo $ItemID?>&Alignment=<?php  echo $Alignment?>" enctype="multipart/form-data">
		
		<?php  if($Alignment=="VD")
				{?>
				<div class="well">
				
				<div class="form-group">
					<label for="email" class="col-sm-4 col-md-5 col-lg-4 control-label">Item Title</label>
					<div class="col-sm-20 col-md-19 col-lg-20 text-left">
						<input type="text" name="ItemTitle" value="<?php  echo isset($_POST['ItemTitle'])?$_POST['ItemTitle']:(isset($CurrentGallery->ItemTitle)?MyStripSlashes($CurrentGallery->ItemTitle):"")?>" size="60">
					</div>
				</div>	
				
				<div class="form-group">
					<label for="email" class="col-sm-4 col-md-5 col-lg-4 control-label">Description</label>
					<div class="col-sm-20 col-md-19 col-lg-20 text-left">
						<textarea name="Description" cols="40" rows="4"><?php  echo isset($_POST['Description'])?$_POST['Description']:(isset($CurrentGallery->Description)?MyStripSlashes($CurrentGallery->Description):"")?></textarea>
					</div>
				</div>	
				
				<div class="form-group">
					<label for="email" class="col-sm-4 col-md-5 col-lg-4 control-label">Embed Source for video</label>
					<div class="col-sm-20 col-md-19 col-lg-20 text-left">
						<textarea name="EmbedSource" cols="40" rows="4"><?php  echo isset($_POST['EmbedSource'])?$_POST['EmbedSource']:(isset($CurrentGallery->EmbedSource)?MyStripSlashes($CurrentGallery->EmbedSource):"")?></textarea>
					</div>
				</div>	
				
				<div class="form-group">
					<label for="email" class="col-sm-4 col-md-5 col-lg-4 control-label">Active</label>
					<div class="col-sm-20 col-md-19 col-lg-20 text-left">
						<input type="checkbox" name="Active" value="1" class="chk" <?php  echo (isset($_POST['Active']) && $_POST['Active']==1)?"checked":((isset($CurrentGallery->Active) && $CurrentGallery->Active==1)?"checked":"")?>>
					</div>
				</div>	
				
				<div class="form-group">
					<div class="col-sm-6 pull-right">
						<button class="btn btn-warning  btn-block" name="AddGallery" type="submit" onclick="return SubmitForm(this.form);">Submit</button>
					</div>
				</div>
		 </div>
				
				<?php  }
				elseif($Alignment=="PH"){?>
				
				<div class="well">
				
				<div class="form-group">
					<label for="email" class="col-sm-4 col-md-5 col-lg-4 control-label">Item Title</label>
					<div class="col-sm-20 col-md-19 col-lg-20 text-left">
						<input type="text" name="ItemTitle" value="<?php  echo isset($_POST['ItemTitle'])?$_POST['ItemTitle']:(isset($CurrentGallery->ItemTitle)?MyStripSlashes($CurrentGallery->ItemTitle):"")?>" size="60">
					</div>
				</div>	
				
				<div class="form-group">
					<label for="email" class="col-sm-4 col-md-5 col-lg-4 control-label">Description</label>
					<div class="col-sm-20 col-md-19 col-lg-20 text-left">
						<textarea name="Description" cols="40" rows="4"><?php  echo isset($_POST['Description'])?$_POST['Description']:(isset($CurrentGallery->Description)?MyStripSlashes($CurrentGallery->Description):"")?></textarea>
					</div>
				</div>
											
			<div class="form-group">
					<label for="email" class="col-sm-4 col-md-5 col-lg-4 control-label">Image</label>
					<div class="col-sm-20 col-md-19 col-lg-20 text-left">
						<input type="file" name="Image" size="30">
					&nbsp;&nbsp;
					<?php 
					if(isset($CurrentGallery->Image) && $CurrentGallery->Image !="" && file_exists(DIR_FS_SITE_UPLOADS_OTHER_ORIGINAL.$CurrentGallery->Image))
					{?>
						<?php  SKImgDisplay(DIR_FS_SITE_UPLOADS_OTHER_ORIGINAL.$CurrentGallery->Image,100,MyStripSlashes($CurrentGallery->ItemTitle));?>
				  <?php }?>
					</div>
				</div>
			
					
			
						
			<div class="form-group">
					<label for="email" class="col-sm-4 col-md-5 col-lg-4 control-label">Active</label>
					<div class="col-sm-20 col-md-19 col-lg-20 text-left">
						<input type="checkbox" name="Active" value="1" class="chk" <?php  echo (isset($_POST['Active']) && $_POST['Active']==1)?"checked":((isset($CurrentGallery->Active) && $CurrentGallery->Active==1)?"checked":"")?>>
					</div>
				</div>
		
				<div class="form-group">
					<div class="col-sm-6 pull-right">
						<button class="btn btn-warning  btn-block" name="AddGallery" type="submit" onclick="return SubmitForm(this.form);">Submit</button>
					</div>
				</div>
		 </div>
				
				
				<?php  }?>
						
	</form>
	<?php 
	/////// Add News End
	break;

	/////////Display News 
	case "DisplayGallery" :
	default:
		$GalleryObj->Where = "Alignment='".$Alignment."'";
		$GalleryObj->TableSelectAll(array("*","DATE_FORMAT(PostDate,'%b %d, %Y') as MyPostDate"),"Position ASC");
		if($GalleryObj->GetNumRows() > 0)
		{
		?>
		<script>
				function FunctionDeleteNews(ItemID)
				{
					
						result = confirm("Are you sure want to delete the gallery?")
						if(result == true)
						{
							URL="<?php  echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php  echo $Page?>&Other=<?php  echo $Other?>&Target=DeleteGallery&ItemID=" + ItemID;
							location.href=URL;
							return true;
						}
					return false;
				}				
			</script>							
		<form method="POST" action="<?php  echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php  echo $Page?>&Other=<?php  echo $Other?>&Section=<?php  echo $Section?>&Target=UpdateGallery&Alignment=<?php  echo $Alignment?>">
		<table class="table table-striped table-bordered table-responsive block">
			<thead>
			<tr>
				<th align="center" width="5%" class="InsideLeftTd"><b>S.No.</b></th>
				<th align="left"  class="InsideLeftTd"><b>Item Title</b><br></th>
				<th style="text-align:center"  class="InsideLeftTd"><b>Details</b></th>
				<th style="text-align:center"  class="InsideLeftTd"><b>Active</b></th>
				<th align="center"  class="InsideLeftTd"><b>Position</b></th>
				<th style="text-align:center" class="InsideLeftTd"><b>Edit</b></th>
				<th style="text-align:center"  class="InsideLeftTd"><b>Delete</b></th>
			</tr>
			</thead>
			<?php 
			$SNo=1;
			$Count=1;
			while($CurrentGallery = $GalleryObj->GetObjectFromRecord())
			{
					
				?>
			<tr >
				<td align="center" class="InsideRightTd"><b><?php  echo $SNo?></b></td>
				<td align="left" class="InsideRightTd">
				<?php  echo MyStripSlashes($CurrentGallery->ItemTitle);?><br />
				<!-- <a href="<?php  echo SKSEOURL($CurrentGallery->ItemID,"other_gallery")?>" target="_blank">Preview</a> -->
				<input type="hidden" name="ItemID_<?php  echo $Count?>" value="<?php  echo $CurrentGallery->ItemID?>"></td>
				  <td align="center" class="InsideRightTd">
				  <?php  if($Alignment=="PH"):?>
					<?php SKImgDisplay(DIR_FS_SITE_UPLOADS_OTHER_ORIGINAL.$CurrentGallery->Image,80,MyStripSlashes($CurrentGallery->ItemTitle));?>
				<?php  endif;?>
				<?php  if($Alignment=="VD"):?>
						<?php  echo MyStripSlashes($CurrentGallery->EmbedSource);?><br />
				<?php  endif;?>
					
				<br>
				</td>
				 <td align="center" class="InsideRightTd">
					<input type="checkbox" name="Active_<?php  echo $Count?>" value="1" class="chk" <?php  echo (isset($CurrentGallery->Active) && $CurrentGallery->Active==1)?"checked":""?>>
				</td>
				<td align="center" class="InsideRightTd"><input type="text" name="Position_<?php  echo $Count?>" value="<?php  echo MyStripSlashes($CurrentGallery->Position);?>" size="5" style="text-align:center;"></td>
				<td align="center" class="InsideRightTd"><a href="<?php  echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php  echo $Page?>&Other=<?php  echo $Other?>&Section=EditGallery&ItemID=<?php  echo $CurrentGallery->ItemID?>&Alignment=<?php  echo $CurrentGallery->Alignment?>"><b>Edit</b></a></td>
				<td align="center" class="InsideRightTd">
					<a href="#" onclick="return FunctionDeleteNews('<?php  echo $CurrentGallery->ItemID?>');"><b>Delete</b></a>
				</td>
			</tr>
			<?php 
			$SNo++;
			$Count++;
			}
		?>
		<tr class="InsideLeftTd">
				<td align="center" width="5%"></td>
				<td align="center"><input type="hidden" name="Count" value="<?php  echo $Count?>"></td>
				<!-- <td align="center"></td> -->
				<td align="center"><input type="hidden" name="Count" value="<?php  echo $Count?>"></td>
				
				<td align="center" colspan="2"><input type="submit" name="UpdateActive" value="Update"></td>
				<td align="center"></td>
				<td align="center"></td>
			</tr>
	
		</table>
		</form>					
		<?php 
		}
		else 
		{
			?>
			<b>No Result Found.</b>
			<?php 
		}
	break;
}
///// Section end?>