<table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr>
		<td height="20" colspan="2" align="center">
		<h4>Gallery	- <?php echo $Label;?></h4>
	</td>
	</tr>
	<tr><td colspan="2"><a href="<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php echo $Page?>&Other=<?php echo $Other?>&Alignment=AD" class="adm-link">Audio</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php echo $Page?>&Other=<?php echo $Other?>&Alignment=VD" class="adm-link">Video &nbsp;&nbsp;|&nbsp;&nbsp;<a href="<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php echo $Page?>&Other=<?php echo $Other?>&Alignment=PH" class="adm-link">Photos<br>
</a></td></tr>
	<tr>
		<td height="20" align="left">
		<?php
			if(isset($CurrentGallery->ItemID) && $CurrentGallery->ItemID !="")
			{
			?>
			&raquo;<a href="<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php echo $Page?>&Other=<?php echo $Other?>" class="adm-link"><?php echo isset($CurrentGallery->ItemTitle)?MyStripSlashes($CurrentGallery->ItemTitle):""?></a>
			<?php
			}?>
		</td>
		<td height="20" align="right">
		<?php if($Label!=""){?>
			<a href="<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php echo $Page?>&Other=<?php echo $Other?>&Section=AddGallery&Alignment=<?php echo $Alignment?>" class="adm-link">Add <?php echo $Label;?></a>&nbsp;&nbsp;
		<?php }?>
		</td>
	</tr>
</table>				
<?php
//// Section start
switch($Section)
{
	case "Files":
	case "EditFile":
		$FileObj->Where ="ReferenceID='$ItemID' AND ReferenceType='$Other'";
		$FileObj->TableSelectAll("","Position ASC");
		?>
			<script type="text/javascript">
				function FunctionDeleteFile()
				{
					result = confirm("Are you sure want to delete the File?")
					if(result == true)
					{
						return true;
					}
					return false;
				}				
							
			</script>
		<table border="0" cellpadding="3" cellspacing="1" width="100%" class="InsideTable">
			<tr class="InsideLeftTd">
				<td align="center" width="5%"><b>S.No.</b></td>
				<td align="left"><b>File</b></td>
				<td align="left"><b>Type</b></td>
				<td align="center"><b>Active</b></td>
				<td align="center"><b>Position</b></td>
				<td align="center"></td>
				<td align="center"></td>
			</tr>
	<?php
			$SNo=1;
			$Count=1;
			while($CurrentFile = $FileObj->GetObjectFromRecord())
			{
				if($Section=="EditFile" && $FileID==$CurrentFile->FileID)
				{
					?>
					<form method="POST" action="<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php echo $Page?>&Section=<?php echo $Section?>&Other=<?php echo $Other?>&ItemID=<?php echo $ItemID?>&FileID=<?php echo $FileID?>&Target=EditFile" enctype="multipart/form-data">
						<tr class="InsideRightTd">
							<td align="center"><b><?php echo $SNo?></b></td>
							<td align="left">
							<?phpSKDisplayFileInfo($CurrentFile)?>
							<br><input type="file" name="Upload">
							</td>
							<td>
							<select name="UploadType">
								<?php
								foreach ($ProductFileTypeArray as $k=>$v)
								{
								?>
									<option value="<?php echo $k?>" <?php echo $CurrentFile->UploadType==$k?"selected":""?>><?php echo $v?></option>
									<?php
								}?>
								</select>
							</td>
							<td align="center"><input type="checkbox" name="Active" value="1" <?php echo $CurrentFile->Active=="1"?"checked":""?> class="chk"></td>
							<td align="center"><input type="text" name="Position" value="<?php echo MyStripSlashes($CurrentFile->Position);?>" size="5" style="text-align:center;"></td>
							<td align="center" ><input type="submit" name="Submit" value="Update"></td>
							<td align="center" ><input type="button" name="button" value="Cancel" onclick="javascript:history.back();"></td>
						</tr>
						<tr><td colspan="7"><hr></td></tr>
					</form>
					<?php		
				}
				else 
				{
					?>
					<tr class="InsideRightTd">
						<td align="center"><b><?php echo $SNo?></b></td>
						<td align="left">
						<?phpSKDisplayFileInfo($CurrentFile)?>
						</TD>
						<td><?php echo isset($ProductFileTypeArray[$CurrentFile->UploadType])?$ProductFileTypeArray[$CurrentFile->UploadType]:""?></td>
						<td align="center"><img src="<?php echo DIR_WS_SITE_CONTROL_IMAGES.($CurrentFile->Active=="1"?"info.gif":"error.gif")?>">
						</td>
						<td align="center"><?php echo MyStripSlashes($CurrentFile->Position);?>
						</td>
						<td align="center"><a href="<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php echo $Page?>&Other=<?php echo $Other?>&Section=EditFile&ItemID=<?php echo $ItemID?>&FileID=<?php echo $CurrentFile->FileID?>"><b>Edit</b></a></td>
						<td align="center"><a href="<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php echo $Page?>&Other=<?php echo $Other?>&Section=<?php echo $Section?>&ItemID=<?php echo $ItemID?>&Target=DeleteFile&FileID=<?php echo $CurrentFile->FileID?>" onclick="return FunctionDeleteFile();"><b>Delete</b></a></td>
					</tr>
					<tr><td colspan="7"><hr></td></tr>
					<?php
				}
			$SNo++;
			$Count++;
			}
		?>
		<form method="POST" action="<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php echo $Page?>&Other=<?php echo $Other?>&Section=<?php echo $Section?>&ItemID=<?php echo $ItemID?>&Target=AddFile" enctype="multipart/form-data">
			<tr class="InsideRightTd">
				<td align="center"><b><?php echo $SNo?></b></td>
				<td align="left"><input type="file" name="Upload"></td>
				<td>
					<select name="UploadType">
					<?php
					foreach ($ProductFileTypeArray as $k=>$v)
					{
					?>
						<option value="<?php echo $k?>"><?php echo $v?></option>
						<?php
					}?>
					</select>
				</td>
				<td align="center"><input type="checkbox" name="Active" value="1" class="chk"></td>
				<td align="center"><input type="text" name="Position" size="5"></td>
				<td align="center" colspan="2"><input type="submit" name="Submit" value="Add"></td>
			</tr>
		</form>
		</table>	
		<?php
	break;
	case "AddGallery" :
	case "EditNews" :
	/////// Add News  Start
	?>
	<form method="POST" action="<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php echo $Page?>&Other=<?php echo $Other?>&Section=<?php echo $Section?>&Target=AddGallery&ItemID=<?php echo $ItemID?>&Alignment=<?php echo $Alignment?>" enctype="multipart/form-data">
		
		<?php if($Alignment=="AD"){?>
		<table border="0" cellpadding="3" cellspacing="1" width="100%"%" class="InsideTable">
			<tr>
				<td align="right" class="InsideLeftTd"><b>Item Title</b></td>
				<td align="left" class="InsideRightTd"><input type="text" name="ItemTitle" value="<?php echo isset($_POST['ItemTitle'])?$_POST['ItemTitle']:(isset($CurrentGallery->ItemTitle)?MyStripSlashes($CurrentGallery->ItemTitle):"")?>" size="60"></td>
			</tr>
			<tr>
				<td align="right" class="InsideLeftTd"><b>Post Date</b></td>
				<td align="left" class="InsideRightTd">
					<table cellpadding="0" cellspacing="0" border="0">
						<tr>
							<td valign="middle"><input type="text" name="PostDate"  ID="PostDate" value="<?php echo (isset($_POST['PostDate'])?$_POST['PostDate']:(isset($CurrentGallery->PostDate)?$CurrentGallery->PostDate:""))?>" size="25" ReadOnly></td>
							<td valign="middle">
								<a href="#" id="SelectDate2"><img style="BORDER-RIGHT:0px; BORDER-TOP:0px; BORDER-LEFT:0px; BORDER-BOTTOM:0px" src="<?php echo DIR_WS_SITE_INCLUDES_JAVASCRIPT?>calendar/img.gif"></a>
								<script type="text/javascript">
									Calendar.setup({
											inputField     :    "PostDate",     // id of the input field
											ifFormat       :    "%Y-%m-%d",     // format of the input field
											button         :    "SelectDate2",  // trigger for the calendar (button ID)
											align          :    "Bl",           // alignment (defaults to "Bl")
											singleClick    :    true,
											timeFormat	   :	24,
											showsTime	   :	false
										});
								</script>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<?php
			if(@constant("DEFINE_SEO_URL_ACTIVE") =="1")
			{
			?>
			<tr>
				<td align="center" class="InsideLeftTd"><b>Item URL</b>
				</td>
				<td align="left" class="InsideRightTd">
				<input type="text" name="URLName" value="<?php echo isset($_POST['URLName'])?MyStripSlashes($_POST['URLName']):(isset($CurrentGallery->URLName)?MyStripSlashes($CurrentGallery->URLName):"")?>" size="70" onblur="return CheckSpecialSymbol(this);" title="Please do not use special symbol. Only use alfa numeric characters,(_) underscores or (-) dashes.">
				<br><small>Please do not use special symbol. 
				<br>Only use alfa numeric characters,(_) underscores or (-) dashes.</small>
				</td>
			</tr>
			<?php
			}?>
			<?php
			if(@constant("SEO_META_ACTIVE") =="1")
			{
			?>									
			<tr>
				<td align="center" colspan="2"><hr></td>
			</tr>
			<tr>
				<td align="center" class="InsideLeftTd"><b>Meta Title</b></td>
				<td align="left" class="InsideRightTd"><input type="text" name="MetaTitle" value="<?php echo isset($_POST['MetaTitle'])?MyStripSlashes($_POST['MetaTitle']):(isset($CurrentGallery->MetaTitle)?MyStripSlashes($CurrentGallery->MetaTitle):"")?>" size="100"></td>
			</tr>
			<tr>
				<td align="center" class="InsideLeftTd"><b>Meta Keyword</b></td>
				<td align="left" class="InsideRightTd"><textarea name="MetaKeyword" cols="60" rows="3"><?php echo isset($_POST['MetaKeyword'])?MyStripSlashes($_POST['MetaKeyword']):(isset($CurrentGallery->MetaKeyword)?MyStripSlashes($CurrentGallery->MetaKeyword):"")?></textarea></td>
			</tr>
			<tr>
				<td align="center" class="InsideLeftTd"><b>Meta Description</b></td>
				<td align="left" class="InsideRightTd"><textarea name="MetaDescription" cols="60" rows="3"><?php echo isset($_POST['MetaDescription'])?MyStripSlashes($_POST['MetaDescription']):(isset($CurrentGallery->MetaDescription)?MyStripSlashes($CurrentGallery->MetaDescription):"")?></textarea></td>
			</tr>
			<?php
			}?>
			<tr>
				<td align="center" colspan="2"><hr></td>
			</tr>									
			<tr>
				<td align="right" class="InsideLeftTd"><b>Description</b></td>
				<td align="left" class="InsideRightTd">
				<textarea name="Description" cols="40" rows="4"><?php echo isset($_POST['Description'])?$_POST['Description']:(isset($CurrentGallery->Description)?MyStripSlashes($CurrentGallery->Description):"")?></textarea>
				</td>
			</tr>
			
			<tr>
			
			</tr>
			<td align="right" class="InsideLeftTd"><b>Upload mp3</b></td>
				<td align="left" class="InsideRightTd">
				<input type="file" name="Image" size="30">
				<?php
					if(isset($CurrentGallery->Image) && $CurrentGallery->Image !="" && file_exists(DIR_FS_SITE_UPLOADS_OTHER_ORIGINAL.$CurrentGallery->Image))
					{?>
						<a href="<?php echo DIR_WS_SITE_UPLOADS_OTHER_ORIGINAL.$CurrentGallery->Image?>" target="_blank"/>View File</a>
				  <?php}?>
				</td>
			<tr>
				<td align="center" class="InsideLeftTd"><b>Upload Image</b><br>
				</td>
				<td align="left" class="InsideRightTd" valign="middle">
					<input type="file" name="Upload" size="30">
					&nbsp;&nbsp;
					<?php
					if(isset($CurrentGallery->Upload) && $CurrentGallery->Upload !="" && file_exists(DIR_FS_SITE_UPLOADS_OTHER_ORIGINAL.$CurrentGallery->Upload))
					{?>
						<a href="<?php echo DIR_WS_SITE_UPLOADS_OTHER_ORIGINAL.$CurrentGallery->Upload?>" target="_blank">View File</a>
				  <?php}?>
							
				</td>
			</tr> 
			
			<tr>
				<td align="right" class="InsideLeftTd"><b>Display Image</b></td>
				<td align="left" class="InsideRightTd">
					<input type="checkbox" name="UploadActive" value="1" class="chk" <?php echo (isset($_POST['UploadActive']) && $_POST['UploadActive']==1)?"checked":((isset($CurrentGallery->UploadActive) && $CurrentGallery->UploadActive==1)?"checked":"")?>>
				</td>
			</tr>
			<tr>
				<td align="right" class="InsideLeftTd"><b>Active</b></td>
				<td align="left" class="InsideRightTd">
					<input type="checkbox" name="Active" value="1" class="chk" <?php echo (isset($_POST['Active']) && $_POST['Active']==1)?"checked":((isset($CurrentGallery->Active) && $CurrentGallery->Active==1)?"checked":"")?>>
				</td>
			</tr>
			<!--  <tr>
				<td align="right" class="InsideLeftTd"><b>HomePage</b></td>
				<td align="left" class="InsideRightTd">
					<input type="checkbox" name="HomePage" value="1" class="chk" <?php echo (isset($_POST['HomePage']) && $_POST['HomePage']==1)?"checked":((isset($CurrentGallery->HomePage) && $CurrentGallery->HomePage==1)?"checked":"")?>>
				</td>
			</tr>  -->
			
			<tr>
				<td align="right" class="InsideLeftTd"><b>Disply on Archives Page</b></td>
				<td align="left" class="InsideRightTd">
					<input type="checkbox" name="ArchivePage" value="1" class="chk" <?php echo (isset($_POST['ArchivePage']) && $_POST['ArchivePage']==1)?"checked":((isset($CurrentGallery->ArchivePage) && $CurrentGallery->ArchivePage==1)?"checked":"")?>>
				</td>
			</tr> 
			
			<tr>
				<td align="right" class="InsideLeftTd"></td>
				<td align="left" class="InsideRightTd">
					<input type="submit" name="AddGallery" value="Submit">
				</td>
			</tr>
		</table>
		
		
		
				<?php }
				elseif($Alignment=="VD")
				{?>
				
				
				
				<table border="0" cellpadding="3" cellspacing="1" width="100%"%" class="InsideTable">
			<tr>
				<td align="right" class="InsideLeftTd"><b>Item Title</b></td>
				<td align="left" class="InsideRightTd"><input type="text" name="ItemTitle" value="<?php echo isset($_POST['ItemTitle'])?$_POST['ItemTitle']:(isset($CurrentGallery->ItemTitle)?MyStripSlashes($CurrentGallery->ItemTitle):"")?>" size="60"></td>
			</tr>
			<tr>
				<td align="right" class="InsideLeftTd"><b>Post Date</b></td>
				<td align="left" class="InsideRightTd">
					<table cellpadding="0" cellspacing="0" border="0">
						<tr>
							<td valign="middle"><input type="text" name="PostDate"  ID="PostDate" value="<?php echo (isset($_POST['PostDate'])?$_POST['PostDate']:(isset($CurrentGallery->PostDate)?$CurrentGallery->PostDate:""))?>" size="25" ReadOnly></td>
							<td valign="middle">
								<a href="#" id="SelectDate2"><img style="BORDER-RIGHT:0px; BORDER-TOP:0px; BORDER-LEFT:0px; BORDER-BOTTOM:0px" src="<?php echo DIR_WS_SITE_INCLUDES_JAVASCRIPT?>calendar/img.gif"></a>
								<script type="text/javascript">
									Calendar.setup({
											inputField     :    "PostDate",     // id of the input field
											ifFormat       :    "%Y-%m-%d",     // format of the input field
											button         :    "SelectDate2",  // trigger for the calendar (button ID)
											align          :    "Bl",           // alignment (defaults to "Bl")
											singleClick    :    true,
											timeFormat	   :	24,
											showsTime	   :	false
										});
								</script>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<?php
			if(@constant("DEFINE_SEO_URL_ACTIVE") =="1")
			{
			?>
			<tr>
				<td align="center" class="InsideLeftTd"><b>Item URL</b>
				</td>
				<td align="left" class="InsideRightTd">
				<input type="text" name="URLName" value="<?php echo isset($_POST['URLName'])?MyStripSlashes($_POST['URLName']):(isset($CurrentGallery->URLName)?MyStripSlashes($CurrentGallery->URLName):"")?>" size="70" onblur="return CheckSpecialSymbol(this);" title="Please do not use special symbol. Only use alfa numeric characters,(_) underscores or (-) dashes.">
				<br><small>Please do not use special symbol. 
				<br>Only use alfa numeric characters,(_) underscores or (-) dashes.</small>
				</td>
			</tr>
			<?php
			}?>
			<?php
			if(@constant("SEO_META_ACTIVE") =="1")
			{
			?>									
			<tr>
				<td align="center" colspan="2"><hr></td>
			</tr>
			<tr>
				<td align="center" class="InsideLeftTd"><b>Meta Title</b></td>
				<td align="left" class="InsideRightTd"><input type="text" name="MetaTitle" value="<?php echo isset($_POST['MetaTitle'])?MyStripSlashes($_POST['MetaTitle']):(isset($CurrentGallery->MetaTitle)?MyStripSlashes($CurrentGallery->MetaTitle):"")?>" size="100"></td>
			</tr>
			<tr>
				<td align="center" class="InsideLeftTd"><b>Meta Keyword</b></td>
				<td align="left" class="InsideRightTd"><textarea name="MetaKeyword" cols="60" rows="3"><?php echo isset($_POST['MetaKeyword'])?MyStripSlashes($_POST['MetaKeyword']):(isset($CurrentGallery->MetaKeyword)?MyStripSlashes($CurrentGallery->MetaKeyword):"")?></textarea></td>
			</tr>
			<tr>
				<td align="center" class="InsideLeftTd"><b>Meta Description</b></td>
				<td align="left" class="InsideRightTd"><textarea name="MetaDescription" cols="60" rows="3"><?php echo isset($_POST['MetaDescription'])?MyStripSlashes($_POST['MetaDescription']):(isset($CurrentGallery->MetaDescription)?MyStripSlashes($CurrentGallery->MetaDescription):"")?></textarea></td>
			</tr>
			<?php
			}?>
			<tr>
				<td align="center" colspan="2"><hr></td>
			</tr>									
			<tr>
				<td align="right" class="InsideLeftTd"><b>Description</b></td>
				<td align="left" class="InsideRightTd">
				<textarea name="Description" cols="40" rows="4"><?php echo isset($_POST['Description'])?$_POST['Description']:(isset($CurrentGallery->Description)?MyStripSlashes($CurrentGallery->Description):"")?></textarea>
				</td>
			</tr>
			
			<tr>
				<td align="right" class="InsideLeftTd"><b>Embed Source for video</b></td>
				<td align="left" class="InsideRightTd">
				<textarea name="EmbedSource" cols="40" rows="4"><?php echo isset($_POST['EmbedSource'])?$_POST['EmbedSource']:(isset($CurrentGallery->EmbedSource)?MyStripSlashes($CurrentGallery->EmbedSource):"")?></textarea>
				</td>
			</tr>
			
			<tr>
				<td align="center" class="InsideLeftTd"><b>Image</b><br>
				</td>
				<td align="left" class="InsideRightTd" valign="middle">
					<input type="file" name="Upload" size="30">
					&nbsp;&nbsp;
					<?php
					if(isset($CurrentGallery->Upload) && $CurrentGallery->Upload !="" && file_exists(DIR_FS_SITE_UPLOADS_OTHER_ORIGINAL.$CurrentGallery->Upload))
					{?>
						<?phpSKImgDisplay(DIR_FS_SITE_UPLOADS_OTHER_ORIGINAL.$CurrentGallery->Upload,100,MyStripSlashes($CurrentGallery->ItemTitle));?>
				  <?php}?>
							
				</td>
			</tr>
			
			<tr>
				<td align="right" class="InsideLeftTd"><b>Disply Image </b></td>
				<td align="left" class="InsideRightTd">
					<input type="checkbox" name="UploadActive" value="1" class="chk" <?php echo (isset($_POST['UploadActive']) && $_POST['UploadActive']==1)?"checked":((isset($CurrentGallery->UploadActive) && $CurrentGallery->UploadActive==1)?"checked":"")?>>
				</td>
			</tr>
			
			<tr>
				<td align="right" class="InsideLeftTd"><b>Active</b></td>
				<td align="left" class="InsideRightTd">
					<input type="checkbox" name="Active" value="1" class="chk" <?php echo (isset($_POST['Active']) && $_POST['Active']==1)?"checked":((isset($CurrentGallery->Active) && $CurrentGallery->Active==1)?"checked":"")?>>
				</td>
			</tr>
			<!--  <tr>
				<td align="right" class="InsideLeftTd"><b>HomePage</b></td>
				<td align="left" class="InsideRightTd">
					<input type="checkbox" name="HomePage" value="1" class="chk" <?php echo (isset($_POST['HomePage']) && $_POST['HomePage']==1)?"checked":((isset($CurrentGallery->HomePage) && $CurrentGallery->HomePage==1)?"checked":"")?>>
				</td>
			</tr>  -->
			<tr>
				<td align="right" class="InsideLeftTd"><b>Disply on Archives Page</b></td>
				<td align="left" class="InsideRightTd">
					<input type="checkbox" name="ArchivePage" value="1" class="chk" <?php echo (isset($_POST['ArchivePage']) && $_POST['ArchivePage']==1)?"checked":((isset($CurrentGallery->ArchivePage) && $CurrentGallery->ArchivePage==1)?"checked":"")?>>
				</td>
			</tr> 
			
			<tr>
				<td align="right" class="InsideLeftTd"></td>
				<td align="left" class="InsideRightTd">
					<input type="submit" name="AddGallery" value="Submit">
				</td>
			</tr>
		</table>
				
				<?php }elseif($Alignment=="PH"){?>
				<table border="0" cellpadding="3" cellspacing="1" width="100%"%" class="InsideTable">
			<tr>
				<td align="right" class="InsideLeftTd"><b>Item Title</b></td>
				<td align="left" class="InsideRightTd"><input type="text" name="ItemTitle" value="<?php echo isset($_POST['ItemTitle'])?$_POST['ItemTitle']:(isset($CurrentGallery->ItemTitle)?MyStripSlashes($CurrentGallery->ItemTitle):"")?>" size="60"></td>
			</tr>
			<tr>
				<td align="right" class="InsideLeftTd"><b>Post Date</b></td>
				<td align="left" class="InsideRightTd">
					<table cellpadding="0" cellspacing="0" border="0">
						<tr>
							<td valign="middle"><input type="text" name="PostDate"  ID="PostDate" value="<?php echo (isset($_POST['PostDate'])?$_POST['PostDate']:(isset($CurrentGallery->PostDate)?$CurrentGallery->PostDate:""))?>" size="25" ReadOnly></td>
							<td valign="middle">
								<a href="#" id="SelectDate2"><img style="BORDER-RIGHT:0px; BORDER-TOP:0px; BORDER-LEFT:0px; BORDER-BOTTOM:0px" src="<?php echo DIR_WS_SITE_INCLUDES_JAVASCRIPT?>calendar/img.gif"></a>
								<script type="text/javascript">
									Calendar.setup({
											inputField     :    "PostDate",     // id of the input field
											ifFormat       :    "%Y-%m-%d",     // format of the input field
											button         :    "SelectDate2",  // trigger for the calendar (button ID)
											align          :    "Bl",           // alignment (defaults to "Bl")
											singleClick    :    true,
											timeFormat	   :	24,
											showsTime	   :	false
										});
								</script>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<?php
			if(@constant("DEFINE_SEO_URL_ACTIVE") =="1")
			{
			?>
			<tr>
				<td align="center" class="InsideLeftTd"><b>Item URL</b>
				</td>
				<td align="left" class="InsideRightTd">
				<input type="text" name="URLName" value="<?php echo isset($_POST['URLName'])?MyStripSlashes($_POST['URLName']):(isset($CurrentGallery->URLName)?MyStripSlashes($CurrentGallery->URLName):"")?>" size="70" onblur="return CheckSpecialSymbol(this);" title="Please do not use special symbol. Only use alfa numeric characters,(_) underscores or (-) dashes.">
				<br><small>Please do not use special symbol. 
				<br>Only use alfa numeric characters,(_) underscores or (-) dashes.</small>
				</td>
			</tr>
			<?php
			}?>
			<?php
			if(@constant("SEO_META_ACTIVE") =="1")
			{
			?>									
			<tr>
				<td align="center" colspan="2"><hr></td>
			</tr>
			<tr>
				<td align="center" class="InsideLeftTd"><b>Meta Title</b></td>
				<td align="left" class="InsideRightTd"><input type="text" name="MetaTitle" value="<?php echo isset($_POST['MetaTitle'])?MyStripSlashes($_POST['MetaTitle']):(isset($CurrentGallery->MetaTitle)?MyStripSlashes($CurrentGallery->MetaTitle):"")?>" size="100"></td>
			</tr>
			<tr>
				<td align="center" class="InsideLeftTd"><b>Meta Keyword</b></td>
				<td align="left" class="InsideRightTd"><textarea name="MetaKeyword" cols="60" rows="3"><?php echo isset($_POST['MetaKeyword'])?MyStripSlashes($_POST['MetaKeyword']):(isset($CurrentGallery->MetaKeyword)?MyStripSlashes($CurrentGallery->MetaKeyword):"")?></textarea></td>
			</tr>
			<tr>
				<td align="center" class="InsideLeftTd"><b>Meta Description</b></td>
				<td align="left" class="InsideRightTd"><textarea name="MetaDescription" cols="60" rows="3"><?php echo isset($_POST['MetaDescription'])?MyStripSlashes($_POST['MetaDescription']):(isset($CurrentGallery->MetaDescription)?MyStripSlashes($CurrentGallery->MetaDescription):"")?></textarea></td>
			</tr>
			<?php
			}?>
			<tr>
				<td align="center" colspan="2"><hr></td>
			</tr>									
			<tr>
				<td align="right" class="InsideLeftTd"><b>Description</b></td>
				<td align="left" class="InsideRightTd">
				<textarea name="Description" cols="40" rows="4"><?php echo isset($_POST['Description'])?$_POST['Description']:(isset($CurrentGallery->Description)?MyStripSlashes($CurrentGallery->Description):"")?></textarea>
				</td>
			</tr>
			
					
			<tr>
				<td align="center" class="InsideLeftTd"><b>Image</b><br>
				</td>
				<td align="left" class="InsideRightTd" valign="middle">
					<input type="file" name="Image" size="30">
					&nbsp;&nbsp;
					<?php
					if(isset($CurrentGallery->Image) && $CurrentGallery->Image !="" && file_exists(DIR_FS_SITE_UPLOADS_OTHER_ORIGINAL.$CurrentGallery->Image))
					{?>
						<?phpSKImgDisplay(DIR_FS_SITE_UPLOADS_OTHER_ORIGINAL.$CurrentGallery->Image,100,MyStripSlashes($CurrentGallery->ItemTitle));?>
				  <?php}?>
							
				</td>
			</tr>
			
						
			<tr>
				<td align="right" class="InsideLeftTd"><b>Active</b></td>
				<td align="left" class="InsideRightTd">
					<input type="checkbox" name="Active" value="1" class="chk" <?php echo (isset($_POST['Active']) && $_POST['Active']==1)?"checked":((isset($CurrentGallery->Active) && $CurrentGallery->Active==1)?"checked":"")?>>
				</td>
			</tr>
			<!--  <tr>
				<td align="right" class="InsideLeftTd"><b>HomePage</b></td>
				<td align="left" class="InsideRightTd">
					<input type="checkbox" name="HomePage" value="1" class="chk" <?php echo (isset($_POST['HomePage']) && $_POST['HomePage']==1)?"checked":((isset($CurrentGallery->HomePage) && $CurrentGallery->HomePage==1)?"checked":"")?>>
				</td>
			</tr> 
			 -->
			<tr>
				<td align="right" class="InsideLeftTd"><b>Disply on Archives Page</b></td>
				<td align="left" class="InsideRightTd">
					<input type="checkbox" name="ArchivePage" value="1" class="chk" <?php echo (isset($_POST['ArchivePage']) && $_POST['ArchivePage']==1)?"checked":((isset($CurrentGallery->ArchivePage) && $CurrentGallery->ArchivePage==1)?"checked":"")?>>
				</td>
			</tr> 
			
			<tr>
				<td align="right" class="InsideLeftTd"></td>
				<td align="left" class="InsideRightTd">
					<input type="submit" name="AddGallery" value="Submit">
				</td>
			</tr>
		</table>
				
				
				<?php }?>
						
	</form>
	<?php
	/////// Add News End
	break;

	/////////Display News 
	case "DisplayGallery" :
	default:
		$GalleryObj->Where = "Alignment='".$Alignment."'";
		$GalleryObj->TableSelectAll(array("*","DATE_FORMAT(PostDate,'%b %d, %Y') as MyPostDate"),"PostDate DESC");
		if($GalleryObj->GetNumRows() > 0)
		{
		?>
		<script>
				function FunctionDeleteNews(ItemID)
				{
					
						result = confirm("Are you sure want to delete the news?")
						if(result == true)
						{
							URL="<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php echo $Page?>&Other=<?php echo $Other?>&Target=DeleteNews&&ItemID=" + ItemID;
							location.href=URL;
							return true;
						}
					return false;
				}				
			</script>							
		<form method="POST" action="<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php echo $Page?>&Other=<?php echo $Other?>&Section=<?php echo $Section?>&Target=UpdateGallery&Alignment=<?php echo $Alignment?>">
		<table border="0" cellpadding="3" cellspacing="1" width="100%" class="InsideTable">
			<tr>
				<td align="center" width="5%" class="InsideLeftTd"><b>S.No.</b></td>
				<td align="left"  class="InsideLeftTd"><b>Item Title</b><br>
				</td>
				<td style="text-align:center"  class="InsideLeftTd"><b>Post Date</b></td>
				<!-- <td style="text-align:center"  class="InsideLeftTd"><b>Images</b></td> -->
				<td style="text-align:center"  class="InsideLeftTd"><b>Active</b></td>
				
				<?php if($Alignment =="AD" OR $Alignment =="VD"){?>
				<td style="text-align:center"  class="InsideLeftTd"><b>Archive Page</b></td>
				 <td style="text-align:center"  class="InsideLeftTd"><b>HomePage</b></td> 
				 <?php }?>
				<td style="text-align:center" class="InsideLeftTd"><b>Edit</b></td>
				<td style="text-align:center"  class="InsideLeftTd"><b>Delete</b></td>
			</tr>
			<?php
			$SNo=1;
			$Count=1;
			while($CurrentGallery = $GalleryObj->GetObjectFromRecord())
			{
					
				?>
			<tr >
				<td align="center" class="InsideRightTd"><b><?php echo $SNo?></b></td>
				<td align="left" class="InsideRightTd">
				<?php echo MyStripSlashes($CurrentGallery->ItemTitle);?><br />
				<!-- <a href="<?php echo SKSEOURL($CurrentGallery->ItemID,"other_gallery")?>" target="_blank">Preview</a> -->
				<input type="hidden" name="ItemID_<?php echo $Count?>" value="<?php echo $CurrentGallery->ItemID?>"></td>
				<td align="center" class="InsideRightTd"><?php echo isset($CurrentGallery->MyPostDate)?$CurrentGallery->MyPostDate:"";?></td>
				<?php /*  <td align="center" class="InsideRightTd">
				<?php /*?><?phpSKImgDisplay(DIR_FS_SITE_UPLOADS_OTHER_ORIGINAL.$CurrentGallery->Image,100,MyStripSlashes($CurrentGallery->ItemTitle));?>
				<br><?php?>
				[<a href="<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php echo $Page?>&Other=<?php echo $Other?>&Section=Files&ItemID=<?php echo $CurrentGallery->ItemID?>" class="adm-link">Add Thumbnail Images</a>]
				</td>*/ ?>
				 <td align="center" class="InsideRightTd">
					<input type="checkbox" name="Active_<?php echo $Count?>" value="1" class="chk" <?php echo (isset($CurrentGallery->Active) && $CurrentGallery->Active==1)?"checked":""?>>
				</td>
				
				<?php if($Alignment =="AD" OR $Alignment =="VD"){?>
				</td><td align="center" class="InsideRightTd">
					<input type="checkbox" name="ArchivePage_<?php echo $Count?>" value="1" class="chk" <?php echo (isset($CurrentGallery->ArchivePage) && $CurrentGallery->ArchivePage==1)?"checked":""?>>
				</td>
				 <td align="center" class="InsideRightTd">
					<input type="checkbox" name="HomePage_<?php echo $Count?>" value="1" class="chk" <?php echo (isset($CurrentGallery->HomePage) && $CurrentGallery->HomePage==1)?"checked":""?>>
				</td>
				<?php }?>
				<td align="center" class="InsideRightTd"><a href="<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php echo $Page?>&Other=<?php echo $Other?>&Section=EditNews&ItemID=<?php echo $CurrentGallery->ItemID?>&Alignment=<?php echo $CurrentGallery->Alignment?>"><b>Edit</b></a></td>
				<td align="center" class="InsideRightTd">
					<a href="#" onclick="return FunctionDeleteNews('<?php echo $CurrentGallery->ItemID?>');"><b>Delete</b></a>
				</td>
			</tr>
			<?php
			$SNo++;
			$Count++;
			}
		?>
		<tr class="InsideLeftTd">
				<td align="center" width="5%"></td>
				<td align="center"><input type="hidden" name="Count" value="<?php echo $Count?>"></td>
				<!-- <td align="center"></td> -->
				<td align="center"><input type="hidden" name="Count" value="<?php echo $Count?>"></td>
				
				<td align="center"><input type="submit" name="UpdateActive" value="Update"></td>
				<?php if($Alignment =="AD" OR $Alignment =="VD"){?>
				<td align="center"><input type="hidden" name="Count" value="<?php echo $Count?>"></td>
				<td align="center"><input type="hidden" name="Count" value="<?php echo $Count?>"></td>
				<?php }?>
				<td align="center"></td>
				<td align="center"></td>
			</tr>
	
		</table>
		</form>					
		<?php
		}
		else 
		{
			?>
			<b>No Result Found.</b>
			<?php
		}
	break;
}
///// Section end?>