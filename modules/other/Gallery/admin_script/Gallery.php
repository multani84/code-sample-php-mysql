<?php
$GalleryObj = new DataTable(TABLE_OTHER_GALLERY);
$SeoObj = new DataTable(TABLE_SEO);

$DataArray = array();
$Other = isset($_GET['Other'])?$_GET['Other']:"";
$Alignment = isset($_GET['Alignment'])?$_GET['Alignment']:"PH";
$Section = isset($_GET['Section'])?$_GET['Section']:"";
$Target = isset($_GET['Target'])?$_GET['Target']:"";
$ItemID = isset($_GET['ItemID'])?$_GET['ItemID']:0;
$FileID = isset($_GET['FileID'])?$_GET['FileID']:0;
$FileObj = new DataTable(TABLE_OTHER_FILES);

if($Alignment=="AD")
{
$Label = "Audio";
}
elseif($Alignment=="VD")
{
$Label = "Video";
}
elseif($Alignment=="PH")
{
$Label = "Photos";
}
else
{
$Label = "";
}
	if($ItemID !=0)
	{
		$GalleryObj = new DataTable(TABLE_OTHER_GALLERY." ne LEFT JOIN ".TABLE_SEO." s on (s.ReferenceID = ne.ItemID AND s.RefrenceType ='other_gallery')");
		$GalleryObj->Where ="ne.ItemID='".$GalleryObj->MysqlEscapeString($ItemID)."'";
		$CurrentGallery = $GalleryObj->TableSelectOne();
	}
	
	if($FileID !=0)
	{
			$FileObj->Where = "FileID='".$FileObj->MysqlEscapeString($FileID)."'";
			$CurrentFile = $FileObj->TableSelectOne();
	}
/// Target  start 
switch ($Target)
{
	case "DeleteGallery":
			$GalleryObj = new DataTable(TABLE_OTHER_GALLERY);
			$GalleryObj->Where = "ItemID='".$GalleryObj->MysqlEscapeString($ItemID)."'";
			$GalleryObj->TableDelete();								
			
			$SeoObj->Where = "ReferenceID = ".$ItemID." AND RefrenceType ='other_gallery'";
			$SeoObj->TableDelete();	
			
			$FileObj = new DataTable(TABLE_OTHER_FILES);
			$FileObj->Where = "ReferenceType ='$Other' AND ReferenceID='".$FileObj->MysqlEscapeString($ItemID)."'";
			$FileObj->TableDelete();
			
			@unlink(DIR_FS_SITE_UPLOADS_OTHER_ORIGINAL.$CurrentGallery->Image);
			@unlink(DIR_FS_SITE_UPLOADS_OTHER_ORIGINAL.$CurrentGallery->File);
										
			@ob_clean();
			
			$_SESSION['InfoMessage'] ="Gallery deleted successfully.";
			header("Location:".DIR_WS_SITE_CONTROL."index.php?Page=$Page&Other=$Other&Section=$Section&Alignment=$Alignment");
			exit;
		
	break;	
	case "UpdateGallery":
		for ($i=1;$i <$_POST['Count'];$i++)
		{
			$GalleryObj->Where = "ItemID='".$GalleryObj->MysqlEscapeString($_POST['ItemID_'.$i])."'";
			$DataArray['UploadActive'] = isset($_POST['UploadActive_'.$i])?$_POST['UploadActive_'.$i]:0;			
			$DataArray['Active'] = isset($_POST['Active_'.$i])?$_POST['Active_'.$i]:0;		
			$DataArray['Position'] = (isset($_POST['Position_'.$i]) && $_POST['Position_'.$i] > 0)?$_POST['Position_'.$i]:0;	
			//$GalleryObj->DisplayQuery =true;
			$GalleryObj->TableUpdate($DataArray);		
		}
		ob_clean();
		
		$_SESSION['InfoMessage'] ="Gallery updated successfully.";
		header("Location:".DIR_WS_SITE_CONTROL."index.php?Page=$Page&Other=$Other&Section=$Section&Alignment=$Alignment");
		exit;
		
	break;
	
		case "AddGallery":
			
			$DataArray['ItemTitle'] = isset($_POST['ItemTitle'])?$_POST['ItemTitle']:"";
			$DataArray['Description'] = isset($_POST['Description'])?$_POST['Description']:"";
			$DataArray['EmbedSource'] = isset($_POST['EmbedSource'])?$_POST['EmbedSource']:"";
			$DataArray['PostDate'] = (isset($_POST['PostDate']) && $_POST['PostDate'] !="")?$_POST['PostDate']:"0000-00-00";
			$DataArray['Active'] = isset($_POST['Active'])?$_POST['Active']:"0";
			$DataArray['HomePage'] = isset($_POST['HomePage'])?$_POST['HomePage']:"0";
			$DataArray['UploadActive'] = isset($_POST['UploadActive'])?$_POST['UploadActive']:"0";
			$DataArray['ArchivePage'] = isset($_POST['ArchivePage'])?$_POST['ArchivePage']:"0";
			$DataArray['Alignment'] = $Alignment;
			
			if(isset($_FILES['Upload']) && $_FILES['Upload']['name'] !="")
			{
				@unlink(DIR_FS_SITE_UPLOADS_OTHER_ORIGINAL.$CurrentGallery->Image);
				$ArrayType = explode(".",$_FILES['Upload']['name']);
				$Type=$ArrayType[count($ArrayType)-1];
				
				$ImageName = uniqid("GalleryUpload_").".".$Type;				
				$OriginalImage =DIR_FS_SITE_UPLOADS_OTHER_ORIGINAL.$ImageName;
				copy($_FILES['Upload']['tmp_name'],$OriginalImage);
				chmod($OriginalImage,0777);
				$DataArray['Upload'] = $ImageName;
			}
			
			
			if(isset($_FILES['Image']) && $_FILES['Image']['name'] !="")
			{
				@unlink(DIR_FS_SITE_UPLOADS_OTHER_ORIGINAL.$CurrentGallery->Image);
				$ArrayType = explode(".",$_FILES['Image']['name']);
				$Type=$ArrayType[count($ArrayType)-1];
				
				$ImageName = uniqid("GalleryImage_").".".$Type;				
				$OriginalImage =DIR_FS_SITE_UPLOADS_OTHER_ORIGINAL.$ImageName;
				copy($_FILES['Image']['tmp_name'],$OriginalImage);
				chmod($OriginalImage,0777);
				$DataArray['Image'] = $ImageName;
			}
			
			if(isset($_FILES['File']) && $_FILES['File']['name'] !="")
			{
				@unlink(DIR_FS_SITE_UPLOADS_OTHER_ORIGINAL.$CurrentGallery->File);
				$ArrayType = explode(".",$_FILES['File']['name']);
				$Type=$ArrayType[count($ArrayType)-1];
				
				$FileName = uniqid("NewsFile_").".".$Type;				
				$OriginalFile =DIR_FS_SITE_UPLOADS_OTHER_ORIGINAL.$FileName;
				copy($_FILES['File']['tmp_name'],$OriginalFile);
				chmod($OriginalFile,0777);
				$DataArray['File'] = $FileName;
			}
			
			if($ItemID==0)
			{
				
				$GalleryObj->Where ="Alignment='".$Alignment."'";
				$Position = $GalleryObj->GetMax("Position") + 1;
				$DataArray['Position'] = $Position;
				$DataArray['CreatedDate'] =date('Y-m-d');
				
				$ItemID = $GalleryObj->TableInsert($DataArray);		
				@ob_clean();
				
				$_SESSION['InfoMessage'] ="Gallery added successfully.";
			}
			else 
			{
							
				$GalleryObj->TableUpdate($DataArray);		
				@ob_clean();
				
				$_SESSION['InfoMessage'] ="Gallery updated successfully.";
				
			}
			
			$SeoObj->Where = "ReferenceID = ".$ItemID." AND RefrenceType ='other_gallery'";
			$CurrentSEO = $SeoObj->TableSelectOne(array("ReferenceID"));
			
			$DataArray = array();
			$DataArray['URLName'] = SkURLCreate((isset($_POST['URLName']) && $_POST['URLName'] !="")?$_POST['URLName']:"news-".$_POST['ItemTitle']);
			$DataArray['RedirectEnabled'] = isset($_POST['RedirectEnabled'])?$_POST['RedirectEnabled']:0;
			$DataArray['RedirectURL'] = isset($_POST['RedirectURL'])?$_POST['RedirectURL']:"";
			
			if(@constant("SEO_META_ACTIVE") =="1")
			{
				$DataArray['MetaTitle'] = isset($_POST['MetaTitle'])?$_POST['MetaTitle']:"";
				$DataArray['MetaKeyword'] = isset($_POST['MetaKeyword'])?$_POST['MetaKeyword']:"";
				$DataArray['MetaDescription'] = isset($_POST['MetaDescription'])?$_POST['MetaDescription']:"";
			}
			
			if(isset($CurrentSEO->ReferenceID) && $CurrentSEO->ReferenceID !="")
			{
				$SeoObj->TableUpdate($DataArray);
			}
			else 
			{
				$DataArray['RefrenceType'] = "other_gallery";
				$DataArray['ReferenceID'] = $ItemID;
				$SeoObj->TableInsert($DataArray);
			}
		
			header("Location:".DIR_WS_SITE_CONTROL."index.php?Page=$Page&Other=$Other&ItemID=$ItemID&Alignment=$Alignment");
			exit;

	break;
	
	
	
	

}
