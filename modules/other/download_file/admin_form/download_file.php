<!-- Heading  -->
<div class="row-fluid">
	<div class="span12">
		<h1>Download Files</h1>
		<hr />
	</div>
	<div class="pull-left">
		
	</div>
	<div class="pull-right">
	</div>
<div class="clearfix separator bottom"></div>
<!--  End Heading-->	
			
<?php
//// Section start
switch($Section)
{
	case "Files":
	case "EditFile":
	default;
		$FileObj->Where ="ReferenceType='$Other'";
		$FileObj->TableSelectAll("","Position ASC");
		?>
			<script type="text/javascript">
				function FunctionDeleteFile()
				{
					result = confirm("Are you sure want to delete the File?")
					if(result == true)
					{
						return true;
					}
					return false;
				}				
							
			</script>
			<table class="table table-striped table-bordered table-responsive block">
			<thead>
			<tr class="InsideLeftTd">
				<th align="center" width="5%"><b>S.No.</b></th>
				<th align="left"><b>Name</b></th>
				<th align="left"><b>File</b></th>
				<th align="center"><b>Active</b></th>
				<th align="center"><b>Position</b></th>
				<th align="center"></th>
				<th align="center"></th>
			</tr>
			</thead>
	<?php
			$SNo=1;
			$Count=1;
			while($CurrentFile = $FileObj->GetObjectFromRecord())
			{
				if($Section=="EditFile" && $FileID==$CurrentFile->FileID)
				{
					?>
					<form method="POST" action="<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php echo $Page?>&Section=<?php echo $Section?>&Other=<?php echo $Other?>&ReferenceID=<?php echo $ReferenceID?>&FileID=<?php echo $FileID?>&Target=EditFile" enctype="multipart/form-data">
						<tr class="InsideRightTd">
							<td align="center"><b><?php echo $SNo?></b></td>
							<td align="left"><input type="text" name="FileName" value="<?php echo MyStripSlashes($CurrentFile->FileName);?>"></td>
							<td align="left"><?php SKDisplayFileInfo($CurrentFile)?>
							<br><input type="file" name="Upload">
							</td>
							<td align="center"><input type="checkbox" name="Active" value="1" <?php echo $CurrentFile->Active=="1"?"checked":""?> class="chk"></td>
							<td align="center"><input type="text" name="Position" value="<?php echo MyStripSlashes($CurrentFile->Position);?>" size="5" style="text-align:center;"></td>
							<td align="center" ><input type="submit" name="Submit" value="Update"></td>
							<td align="center" ><input type="button" name="button" value="Cancel" onclick="javascript:history.back();"></td>
						</tr>
						<tr><td colspan="7"><hr></td></tr>
					</form>
					<?php		
				}
				else 
				{
					?>
					<tr class="InsideRightTd">
						<td align="center"><b><?php echo $SNo?></b></td>
						<td align="left"><?php echo MyStripSlashes($CurrentFile->FileName);?></td>
						<td align="left">
						<?php SKDisplayFileInfo($CurrentFile)?>
						</TD>
						<td align="center"><img src="<?php echo DIR_WS_SITE_CONTROL_IMAGES.($CurrentFile->Active=="1"?"info.gif":"error.gif")?>">
						</td>
						<td align="center"><?php echo MyStripSlashes($CurrentFile->Position);?>
						</td>
						<td align="center"><a href="<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php echo $Page?>&Other=<?php echo $Other?>&Section=EditFile&ReferenceID=<?php echo $ReferenceID?>&FileID=<?php echo $CurrentFile->FileID?>"><b>Edit</b></a></td>
						<td align="center"><a href="<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php echo $Page?>&Other=<?php echo $Other?>&Section=<?php echo $Section?>&ReferenceID=<?php echo $ReferenceID?>&Target=DeleteFile&FileID=<?php echo $CurrentFile->FileID?>" onclick="return FunctionDeleteFile();"><b>Delete</b></a></td>
					</tr>
					<tr><td colspan="7"><hr></td></tr>
					<?php
				}
			$SNo++;
			$Count++;
			}
		?>
		<form method="POST" action="<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php echo $Page?>&Other=<?php echo $Other?>&Section=<?php echo $Section?>&ReferenceID=<?php echo $ReferenceID?>&Target=AddFile" enctype="multipart/form-data">
			<tr class="InsideRightTd">
				<td align="center"><b><?php echo $SNo?></b></td>
				<td align="left"><input type="text" name="FileName" value=""></td>
				<td align="left"><input type="file" name="Upload"></td>
				<td align="center"><input type="checkbox" name="Active" value="1" class="chk"></td>
				<td align="center"><input type="text" name="Position" size="5"></td>
				<td align="center" colspan="2"><input type="submit" name="Submit" value="Add"></td>
			</tr>
		</form>
		</table>	
		<?php
	break;
	
}
///// Section end?>