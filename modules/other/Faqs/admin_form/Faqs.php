<!-- Heading  -->
<div class="row-fluid">
	<div class="span12">
		<h1>Faqs</h1>
		<hr />
		<b><?php echo isset($CurrentCategory->CategoryName)?MyStripSlashes($CurrentCategory->CategoryName):""?></b>
	</div>
	<div>
		<div class="pull-left">
		<a href="<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php echo $Page?>&Other=<?php echo $Other?>" class="adm-link">Categories</a>
		<?php
		if($CategoryID !=0)
		{
		?>
			&nbsp;&raquo;<a href="<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php echo $Page?>&Other=<?php echo $Other?>&Section=DisplayFaqs&CategoryID=<?php echo $CategoryID?>" class="adm-link"><?php echo isset($CurrentCategory->CategoryName)?MyStripSlashes($CurrentCategory->CategoryName):""?></a>
			<?php
			if(isset($CurrentFaq->FaqID) && $CurrentFaq->FaqID !="")
			{
			?>
			&nbsp;&raquo;<a href="<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php echo $Page?>&Other=<?php echo $Other?>&Section=DisplayFaqs&CategoryID=<?php echo $CategoryID?>" class="adm-link"><?php echo isset($CurrentFaq->FaqTitle)?MyStripSlashes($CurrentFaq->FaqTitle):""?></a>
			<?php
			}?>
			<?php
		}?>
		</div>
		<div class="pull-right">
			<?php
			if($CategoryID !=0)
			{
			?>
				<a href="<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php echo $Page?>&Other=<?php echo $Other?>&Section=AddFaq&CategoryID=<?php echo $CategoryID?>" class="btn btn-primary">Add Faq <icon class="icon-plus-sign icon-white-t"></icon></a>
				<?php
			}?>
		
		</div>
	</div>
<div class="clearfix separator bottom"></div>
<!--  End Heading-->		
			
<?php
//// Section start
switch($Section)
{
	case "Files":
	case "EditFile":
		$FileObj->Where ="ReferenceID='$FaqID' AND ReferenceType='$Other'";
		$FileObj->TableSelectAll("","Position ASC");
		?>
			<script type="text/javascript">
				function FunctionDeleteFile()
				{
					result = confirm("Are you sure want to delete the File?")
					if(result == true)
					{
						return true;
					}
					return false;
				}				
							
			</script>
		<table border="0" cellpadding="3" cellspacing="1" width="100%" class="InsideTable">
			<tr class="InsideLeftTd">
				<td align="center" width="5%"><b>S.No.</b></td>
				<td align="left"><b>File</b></td>
				<td align="left"><b>Type</b></td>
				<td align="center"><b>Active</b></td>
				<td align="center"><b>Position</b></td>
				<td align="center"></td>
				<td align="center"></td>
			</tr>
	<?php
			$SNo=1;
			$Count=1;
			while($CurrentFile = $FileObj->GetObjectFromRecord())
			{
				if($Section=="EditFile" && $FileID==$CurrentFile->FileID)
				{
					?>
					<form method="POST" action="<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php echo $Page?>&Section=<?php echo $Section?>&Other=<?php echo $Other?>&CategoryID=<?php echo $CategoryID?>&FaqID=<?php echo $FaqID?>&FileID=<?php echo $FileID?>&Target=EditFile" enctype="multipart/form-data">
						<tr class="InsideRightTd">
							<td align="center"><b><?php echo $SNo?></b></td>
							<td align="left">
							<?phpSKDisplayFileInfo($CurrentFile)?>
							<br><input type="file" name="Upload">
							</td>
							<td>
							<select name="UploadType">
								<?php
								foreach ($ProductFileTypeArray as $k=>$v)
								{
								?>
									<option value="<?php echo $k?>" <?php echo $CurrentFile->UploadType==$k?"selected":""?>><?php echo $v?></option>
									<?php
								}?>
								</select>
							</td>
							<td align="center"><input type="checkbox" name="Active" value="1" <?php echo $CurrentFile->Active=="1"?"checked":""?> class="chk"></td>
							<td align="center"><input type="text" name="Position" value="<?php echo MyStripSlashes($CurrentFile->Position);?>" size="5" style="text-align:center;"></td>
							<td align="center" ><input type="submit" name="Submit" value="Update"></td>
							<td align="center" ><input type="button" name="button" value="Cancel" onclick="javascript:history.back();"></td>
						</tr>
						<tr><td colspan="7"><hr></td></tr>
					</form>
					<?php		
				}
				else 
				{
					?>
					<tr class="InsideRightTd">
						<td align="center"><b><?php echo $SNo?></b></td>
						<td align="left">
						<?phpSKDisplayFileInfo($CurrentFile)?>
						</TD>
						<td><?php echo isset($ProductFileTypeArray[$CurrentFile->UploadType])?$ProductFileTypeArray[$CurrentFile->UploadType]:""?></td>
						<td align="center"><img src="<?php echo DIR_WS_SITE_CONTROL_IMAGES.($CurrentFile->Active=="1"?"info.gif":"error.gif")?>">
						</td>
						<td align="center"><?php echo MyStripSlashes($CurrentFile->Position);?>
						</td>
						<td align="center"><a href="<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php echo $Page?>&Other=<?php echo $Other?>&Section=EditFile&CategoryID=<?php echo $CategoryID?>&FaqID=<?php echo $FaqID?>&FileID=<?php echo $CurrentFile->FileID?>"><b>Edit</b></a></td>
						<td align="center"><a href="<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php echo $Page?>&Other=<?php echo $Other?>&Section=<?php echo $Section?>&CategoryID=<?php echo $CategoryID?>&FaqID=<?php echo $FaqID?>&Target=DeleteFile&FileID=<?php echo $CurrentFile->FileID?>" onclick="return FunctionDeleteFile();"><b>Delete</b></a></td>
					</tr>
					<tr><td colspan="7"><hr></td></tr>
					<?php
				}
			$SNo++;
			$Count++;
			}
		?>
		<form method="POST" action="<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php echo $Page?>&Other=<?php echo $Other?>&Section=<?php echo $Section?>&CategoryID=<?php echo $CategoryID?>&FaqID=<?php echo $FaqID?>&Target=AddFile" enctype="multipart/form-data">
			<tr class="InsideRightTd">
				<td align="center"><b><?php echo $SNo?></b></td>
				<td align="left"><input type="file" name="Upload"></td>
				<td>
					<select name="UploadType">
					<?php
					foreach ($ProductFileTypeArray as $k=>$v)
					{
					?>
						<option value="<?php echo $k?>"><?php echo $v?></option>
						<?php
					}?>
					</select>
				</td>
				<td align="center"><input type="checkbox" name="Active" value="1" class="chk"></td>
				<td align="center"><input type="text" name="Position" size="5"></td>
				<td align="center" colspan="2"><input type="submit" name="Submit" value="Add"></td>
			</tr>
		</form>
		</table>	
		<?php
	break;
	case "AddFaq" :
	case "EditFaq" :
	/////// Add Faq  Start
	?>
	<div class="well">
	<form class="form-horizontal" method="POST" action="<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php echo $Page?>&Other=<?php echo $Other?>&Section=<?php echo $Section?>&Target=AddFaq&CategoryID=<?php echo $CategoryID?>&FaqID=<?php echo $FaqID?>" enctype="multipart/form-data">
		
		<div class="form-group">
			<label for="email" class="col-sm-4 col-md-5 col-lg-4 control-label">Faq Title/Question: </label>
			<div class="col-sm-20 col-md-19 col-lg-20 text-left">
				<input type="text" name="FaqTitle" value="<?php echo isset($_POST['FaqTitle'])?$_POST['FaqTitle']:(isset($CurrentFaq->FaqTitle)?MyStripSlashes($CurrentFaq->FaqTitle):"")?>" size="90">
			</div>
		</div>
		
		<div class="form-group">
			<label for="email" class="col-sm-4 col-md-5 col-lg-4 control-label">Answer: </label>
			<div class="col-sm-20 col-md-19 col-lg-20 text-left">
				<!---Start content editor--->
						<?php
				$sContent = isset($_POST['LongDescription'])?MyStripSlashes($_POST['LongDescription']):(isset($CurrentFaq->LongDescription)?MyStripSlashes($CurrentFaq->LongDescription):"");
				$SKEditorObj->CArray = array("Width"=>"600",
											 "Height"=>"300",
											 "DMode"=>"Large2",
											 "Help"=>true,
											 "EditorArray"=>array("css"=>DIR_WS_SITE_CONTROL."style.css",
											 					  ),
											 ); 
				$SKEditorObj->CreateEditor("LongDescription",$sContent);?>
						<!---end content editor--->
			</div>
		</div>
		
		<div class="form-group">
			<label for="email" class="col-sm-4 col-md-5 col-lg-4 control-label">Active: </label>
			<div class="col-sm-20 col-md-19 col-lg-20 text-left">
				<input type="checkbox" name="Active" value="1" class="chk" <?php echo (isset($_POST['Active']) && $_POST['Active']==1)?"checked":((isset($CurrentFaq->Active) && $CurrentFaq->Active==1)?"checked":"")?>>
			</div>
		</div>
		
		<div class="form-group">
			<div class="col-sm-6 pull-right">
				<button class="btn btn-warning  btn-block" type="submit">Submit</button>
			</div>
		</div>
	</form>
	</div>
	<?php
	/////// Add Faq End
	break;

	/////////Display Faq 
	case "DisplayFaqs" :
		$FaqObj = new DataTable(TABLE_OTHER_FAQS. " at, ".TABLE_OTHER_CATEGORY_REF. " atc");
		$FaqObj->Where = "at.FaqID=atc.ReferenceID and atc.CategoryID='".$FaqObj->MysqlEscapeString($CategoryID)."'";
		$FaqObj->TableSelectAll("","Position ASC");
		
		if($FaqObj->GetNumRows() > 0)
		{
		?>
		<script>
				function FunctionDeleteFaq(FaqID)
				{
					
						result = confirm("Are you sure want to delete the faq?")
						if(result == true)
						{
							URL="<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php echo $Page?>&Other=<?php echo $Other?>&Target=DeleteFaq&CategoryID=<?php echo $CategoryID?>&FaqID=" + FaqID;
							location.href=URL;
							return true;
						}
					return false;
				}				
			</script>							
		<form method="POST" action="<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php echo $Page?>&Other=<?php echo $Other?>&Section=<?php echo $Section?>&CategoryID=<?php echo $CategoryID?>&Target=UpdateFaq">
		<table class="table table-striped table-bordered table-responsive block">
			<thead>
			<tr>
				<th align="center" width="5%" class="InsideLeftTd">S.No.</th>
				<th align="left"  class="InsideLeftTd">Faq Title</th>
				<th align="center"  class="InsideLeftTd">Active</th>
				<th align="center"  class="InsideLeftTd">Position</th>
				<th align="center"  class="InsideLeftTd">Edit</th>
				<th align="center"  class="InsideLeftTd">Delete</th>
			</tr>
			</thead>
			<?php
			$SNo=1;
			$Count=1;
			while($CurrentFaq = $FaqObj->GetObjectFromRecord())
			{
					
				?>
			<tr >
				<td align="center" class="InsideRightTd"><?php echo $SNo?></td>
				<td align="left" class="InsideRightTd">
				<?php echo MyStripSlashes($CurrentFaq->FaqTitle);?>			
				<input type="hidden" name="FaqID_<?php echo $Count?>" value="<?php echo $CurrentFaq->FaqID?>"></td>
				<td align="center" class="InsideRightTd">
					<input type="checkbox" name="Active_<?php echo $Count?>" value="1" class="chk" <?php echo (isset($CurrentFaq->Active) && $CurrentFaq->Active==1)?"checked":""?>>
				</td>
				<td align="center" class="InsideRightTd">
					<input type="text" name="Position_<?php echo $Count?>" value="<?php echo $CurrentFaq->Position?>"  size="3" style="text-align:center;width:60px">
				</td>
				<td align="center" class="InsideRightTd"><a href="<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php echo $Page?>&Other=<?php echo $Other?>&Section=EditFaq&CategoryID=<?php echo $CategoryID?>&FaqID=<?php echo $CurrentFaq->FaqID?>" class="btn btn-success btn-phone-block"><icon class="icon-pencil icon-white"></icon> Edit</a></td>
				<td align="center" class="InsideRightTd">
					<a href="#" onclick="return FunctionDeleteFaq('<?php echo $CurrentFaq->FaqID?>');" class="btn btn-danger btn-phone-block"><icon class="icon-remove icon-white"></icon> Delete</a>
				</td>
			</tr>
			<?php
			$SNo++;
			$Count++;
			}
		?>
		<tr class="InsideLeftTd">
				<td align="center" width="5%"></td>
				<td align="center"><input type="hidden" name="Count" value="<?php echo $Count?>"></td>
				<td align="center" colspan="2">
				<div class="text-center"><input type="submit" name="UpdateActive" value="Update" class="btn"><div>
				</td>
				<td align="center"></td>
				<td align="center"></td>
			</tr>
	
		</table>
		</form>					
		<?php
		}
		else 
		{
			?>
			<b>No Result Found.</b>
			<?php
		}
	break;
	case "Category":
	case "EditCategory":
	default:
	$CategoryObj->Where = "CategoryType='".$Other."'";
	$CategoryObj->TableSelectAll("","Position ASC");
	?>
	<script type="text/javascript">
		function FunctionDeleteCategory(CategoryID,Status)
		{
			if(Status==0)
			{
				alert("First delete all records inside this category.");
				return false;
			}
			else
			{
				result = confirm("Are you sure want to delete the category?")
				if(result == true)
				{
					URL="<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php echo $Page?>&Target=DeleteCategory&Other=<?php echo $Other?>&CategoryID=" + CategoryID;
					location.href=URL;
					return true;
				}
			}
			return false;
		}				
							
			</script>
	<table class="table table-striped table-bordered table-responsive block">
		<thead>		
				<tr class="InsideLeftTd">
				<th align="center">S.No.</th>
				<th align="left">Category</th>
				<th align="center">Faqs</th>
				
				<th align="center">Active</th>
				<th align="center">Position</th>
				<th align="center">&nbsp;</th>
				<th align="center">&nbsp;</th>
				
			</tr>
		</thead>
		<tbody>
		<?php
			$SNo=1;
			$Count=1;
			while($CurrentCategory = $CategoryObj->GetObjectFromRecord())
			{
				$CategoryRefObj->Where = "CategoryID='".$CategoryRefObj->MysqlEscapeString($CurrentCategory->CategoryID)."'";
				$CategoryRefObj->TableSelectAll(array("ReferenceID"));
					
					$TotalRows = $CategoryRefObj->GetNumRows();
					if($TotalRows==0)
						$Status = 1;
					else 
						$Status = 0;
						
				if($Section=="EditCategory" && $CategoryID==$CurrentCategory->CategoryID)
				{
					?>
					<form method="POST" action="<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php echo $Page?>&Section=<?php echo $Section?>&Other=<?php echo $Other?>&CategoryID=<?php echo $CategoryID?>&Target=EditCategory" enctype="multipart/form-data">
						<tr class="InsideRightTd">
							<td align="center"><b><?php echo $SNo?></b></td>
							<td align="left"><input type="text" name="CategoryName" value="<?php echo MyStripSlashes($CurrentCategory->CategoryName);?>" size="20"></td>
							<td align="center"><a href="<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php echo $Page?>&Section=DisplayFaqs&Other=<?php echo $Other?>&CategoryID=<?php echo $CurrentCategory->CategoryID?>"><b>View Faqs</b> (<?php echo $TotalRows?>)</a>				
							<td align="center"><input type="checkbox" name="Active" value="1" <?php echo $CurrentCategory->Active=="1"?"checked":""?> class="chk"></td>
							<td align="center"><input type="text" name="Position" value="<?php echo MyStripSlashes($CurrentCategory->Position);?>" size="5" style="text-align:center;width:60px"></td>
							<td align="center"><input type="submit" name="Submit" value="Update Category" class="btn btn-primary"></td>
							<td align="center"><input type="button" name="button" value="Cancel" class="btn btn-danger" onclick="javascript:history.back();"></td>
							
						</tr>
					</form>
					<?php		
				}
				else 
				{
					?>
						<tr class="InsideRightTd">
							<td align="center""><b><?php echo $SNo?></b></td>
							<td align="left"><?php echo MyStripSlashes($CurrentCategory->CategoryName);?></td>
							<td align="center"><a href="<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php echo $Page?>&Section=DisplayFaqs&Other=<?php echo $Other?>&CategoryID=<?php echo $CurrentCategory->CategoryID?>">View Faqs <span class="badge"><?php echo $TotalRows?></span></a>				
							<td align="center"><img src="<?php echo DIR_WS_SITE_CONTROL_IMAGES.($CurrentCategory->Active=="1"?"info.gif":"error.gif")?>">
							</td>
							<td align="center"><?php echo MyStripSlashes($CurrentCategory->Position);?>
							</td>
							<td align="center"><a href="<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php echo $Page?>&Section=EditCategory&Other=<?php echo $Other?>&CategoryID=<?php echo $CurrentCategory->CategoryID?>" class="btn btn-success btn-phone-block"><icon class="icon-pencil icon-white"></icon> Edit</a></td>
							<td align="center"><a href="#" onclick="return FunctionDeleteCategory('<?php echo $CurrentCategory->CategoryID?>','<?php echo $Status?>');" class="btn btn-danger btn-phone-block"><icon class="icon-remove icon-white"></icon> Delete</a></td>
							
						</tr>
					<?php
				}
			$SNo++;
			$Count++;
			}
			
		?>
		<form method="POST" action="<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php echo $Page?>&Section=<?php echo $Section?>&Other=<?php echo $Other?>&Target=AddCategory" enctype="multipart/form-data">
			<tr class="InsideRightTd">
				<td align="center"><b><?php echo $SNo?></b></td>
				<td align="left"><input type="text" name="CategoryName" value="" size="20"></td>
				<td align="center">&nbsp;</td>
				<td align="center"><input type="checkbox" name="Active" value="1" class="chk"></td>
				<td align="center"><input type="text" name="Position" size="5" style="text-align:center;width:60px"></td>
				<td align="center"><input type="submit" name="Submit" value="Add Category" class="btn btn-primary"></td>
				<td align="center">&nbsp;</td>
			</tr>
		</form>
		<?php
		?>
		</tbody>		
		</table>		
	
	<?php
	break;
}
///// Section end
?>
&nbsp;