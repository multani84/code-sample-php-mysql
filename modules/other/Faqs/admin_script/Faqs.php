<?php
$FaqObj = new DataTable(TABLE_OTHER_FAQS);
$CategoryObj = new DataTable(TABLE_OTHER_CATEGORIES);
$CategoryRefObj = new DataTable(TABLE_OTHER_CATEGORY_REF);
$FileObj = new DataTable(TABLE_OTHER_FILES);
$DataArray = array();

$Other = isset($_GET['Other'])?$_GET['Other']:"";
$Section = isset($_GET['Section'])?$_GET['Section']:"";
$Target = isset($_GET['Target'])?$_GET['Target']:"";
$FaqID = isset($_GET['FaqID'])?$_GET['FaqID']:0;
$CategoryID = isset($_GET['CategoryID'])?$_GET['CategoryID']:0;
$FileID = isset($_GET['FileID'])?$_GET['FileID']:0;


	if($CategoryID !=0)
	{
		$CategoryObj->Where = "CategoryID='".$CategoryObj->MysqlEscapeString($CategoryID)."'";
		$CurrentCategory = $CategoryObj->TableSelectOne();
	}	
	
	if($FaqID !=0)
	{
		$FaqObj->Where = "FaqID='".$FaqObj->MysqlEscapeString($FaqID)."'";
		$CurrentFaq = $FaqObj->TableSelectOne();
	}	
	
	if($FileID !=0)
	{
			$FileObj->Where = "FileID='".$FileObj->MysqlEscapeString($FileID)."'";
			$CurrentFile = $FileObj->TableSelectOne();
	}
/// Target  start 
switch ($Target)
{
	case "DeleteFaq":
			$FaqObj->Where = "FaqID='".$FaqObj->MysqlEscapeString($FaqID)."'";
			@unlink(DIR_FS_SITE_UPLOADS_OTHER_ORIGINAL.$CurrentFaq->Image);
			@unlink(DIR_FS_SITE_UPLOADS_OTHER_ORIGINAL.$CurrentFaq->File);
			
			$FaqObj->TableDelete();								
			
			if($CategoryID !="")
			{
				$CategoryRefObj->Where = "CategoryID='".$CategoryID."' AND ReferenceID='".$CategoryRefObj->MysqlEscapeString($FaqID)."'";
				$CategoryRefObj->TableDelete();	
			}
			
			@ob_clean();
			
			$_SESSION['InfoMessage'] ="Faq deleted successfully.";
			header("Location:".DIR_WS_SITE_CONTROL."index.php?Page=$Page&Other=$Other&Section=DisplayFaqs&CategoryID=$CategoryID");
			exit;
		
	break;	
	case "UpdateFaq":
		for ($i=1;$i <$_POST['Count'];$i++)
		{
			$FaqObj->Where = "FaqID='".$FaqObj->MysqlEscapeString($_POST['FaqID_'.$i])."'";
			$DataArray['Active'] = isset($_POST['Active_'.$i])?$_POST['Active_'.$i]:0;			
			$DataArray['Position'] = isset($_POST['Position_'.$i])?$_POST['Position_'.$i]:0;			
			$FaqObj->TableUpdate($DataArray);		
		}
		ob_clean();
		
		$_SESSION['InfoMessage'] ="Faq updated successfully.";
		header("Location:".DIR_WS_SITE_CONTROL."index.php?Page=$Page&Other=$Other&Section=$Section&CategoryID=$CategoryID");
		exit;
		
	break;
	
		case "AddFaq":
			
			
			
			$DataArray['FaqTitle'] = isset($_POST['FaqTitle'])?$_POST['FaqTitle']:"";
			$DataArray['LongDescription'] = isset($_POST['LongDescription'])?$_POST['LongDescription']:"";
			$DataArray['Active'] = isset($_POST['Active'])?$_POST['Active']:"0";
			
			if($FaqID==0)
			{
				$DataArray['CreatedDate'] =date('Y-m-d');
				$FaqID = $FaqObj->TableInsert($DataArray);		
				@ob_clean();
				
				$_SESSION['InfoMessage'] ="Faq added successfully.";
			}
			else 
			{
							
				$FaqObj->TableUpdate($DataArray);		
				@ob_clean();
				
				$_SESSION['InfoMessage'] ="Faq updated successfully.";
				
			}
			
			if($CategoryID !="")
			{
				$CategoryRefObj->Where = "CategoryID='".$CategoryID."' AND ReferenceID='".$CategoryRefObj->MysqlEscapeString($FaqID)."'";
				$CategoryRefObj->TableDelete();	
				
				$DataArray = array();
				$DataArray['ReferenceID'] = $FaqID;
				$DataArray['CategoryID'] = $CategoryID;
				$CategoryRefObj->TableInsert($DataArray);
			}
		
			header("Location:".DIR_WS_SITE_CONTROL."index.php?Page=$Page&Other=$Other&Section=DisplayFaqs&CategoryID=$CategoryID&FaqID=$FaqID");
			exit;

	break;
	case "DeleteCategory":
		$CategoryObj->Where = "CategoryID='".$CategoryObj->MysqlEscapeString($CategoryID)."'";
		$CategoryObj->TableDelete();
					
		@ob_clean();
							
		$_SESSION['InfoMessage'] ="Category deleted successfully.";
		header("Location:".DIR_WS_SITE_CONTROL."index.php?Page=$Page&Section=$Section&Other=$Other");
		exit;
	
	break;
	case "EditCategory":
		$DataArray = array();
		$DataArray['CategoryName'] = isset($_POST['CategoryName'])?$_POST['CategoryName']:"0";
		$DataArray['Active'] = isset($_POST['Active'])?$_POST['Active']:"0";
		$DataArray['Position'] = (isset($_POST['Position']) && $_POST['Position'] !="")?$_POST['Position']:"0";
		
		$CategoryObj->Where = "CategoryID='".$CategoryObj->MysqlEscapeString($CategoryID)."'";
		$CategoryObj->TableUpdate($DataArray);
			
		@ob_clean();
							
		$_SESSION['InfoMessage'] ="Category updated successfully.";
		header("Location:".DIR_WS_SITE_CONTROL."index.php?Page=$Page&Section=$Section&Other=$Other");
		exit;
	
	break;
	case "AddCategory":
	
		$DataArray = array();
		$DataArray['CategoryType'] = $Other;
		$DataArray['CategoryName'] = isset($_POST['CategoryName'])?$_POST['CategoryName']:"0";
		$DataArray['Active'] = isset($_POST['Active'])?$_POST['Active']:"0";
		$DataArray['Position'] = (isset($_POST['Position']) && $_POST['Position'] !="")?$_POST['Position']:"0";
		$DataArray['CreatedDate'] = date('Y-m-d');
		$CategoryObj->TableInsert($DataArray);
		@ob_clean();
							
		$_SESSION['InfoMessage'] ="Category added successfully.";
		header("Location:".DIR_WS_SITE_CONTROL."index.php?Page=$Page&Section=$Section&Other=$Other");
		exit;
	
	break;
	
	case "DeleteFile":
		$FileObj->Where = "FileID='".$FileObj->MysqlEscapeString($FileID)."'";
		$FileObj->TableDelete();
		@unlink(DIR_FS_SITE_UPLOADS_OTHER_ORIGINAL.$CurrentFile->Upload);
					
		@ob_clean();
							
		$_SESSION['InfoMessage'] ="Record updated successfully.";
		header("Location:".DIR_WS_SITE_CONTROL."index.php?Page=$Page&Section=$Section&Other=$Other&CategoryID=$CategoryID&FaqID=$FaqID");
		exit;
	
	break;
	case "EditFile":
		$DataArray = array();
		$DataArray['Active'] = isset($_POST['Active'])?$_POST['Active']:"0";
		$DataArray['Position'] = (isset($_POST['Position']) && $_POST['Position'] !="")?$_POST['Position']:"0";
		$DataArray['UploadType'] = (isset($_POST['UploadType']) && $_POST['UploadType'] !="")?$_POST['UploadType']:"Image";
		if(isset($_FILES['Upload']) && $_FILES['Upload']['name'] !="")
		{
				@unlink(DIR_FS_SITE_UPLOADS_OTHER_ORIGINAL.$CurrentFile->Upload);
				
				$ArrayType = explode(".",$_FILES['Upload']['name']);
				$Type=$ArrayType[count($ArrayType)-1];
				
				$UploadName = uniqid("File_".$FaqID."_").".".$Type;				
				$OriginalImage =DIR_FS_SITE_UPLOADS_OTHER_ORIGINAL.$UploadName;
				copy($_FILES['Upload']['tmp_name'],$OriginalImage);
				chmod($OriginalImage,0777);
				
				$DataArray['Upload'] = $UploadName;
				
							
		 }
			
		$FileObj->Where = "FileID='".$FileObj->MysqlEscapeString($FileID)."'";
		$FileObj->TableUpdate($DataArray);
			
		@ob_clean();
							
		$_SESSION['InfoMessage'] ="Record updated successfully.";
		header("Location:".DIR_WS_SITE_CONTROL."index.php?Page=$Page&Section=$Section&Other=$Other&CategoryID=$CategoryID&FaqID=$FaqID");
		exit;
	
	break;
	case "AddFile":
	
		$DataArray = array();
		$DataArray['ReferenceID'] = $FaqID;
		$DataArray['ReferenceType'] = $Other;
		$DataArray['Active'] = isset($_POST['Active'])?$_POST['Active']:"0";
		$DataArray['Position'] = (isset($_POST['Position']) && $_POST['Position'] !="")?$_POST['Position']:"0";
		$DataArray['UploadType'] = (isset($_POST['UploadType']) && $_POST['UploadType'] !="")?$_POST['UploadType']:"Image";
		$DataArray['CreatedDate'] = date('Y-m-d H:i:s');
		
		if(isset($_FILES['Upload']) && $_FILES['Upload']['name'] !="")
		{
			$ArrayType = explode(".",$_FILES['Upload']['name']);
			$Type=$ArrayType[count($ArrayType)-1];
			
			$UploadName = uniqid("File_".$FaqID."_").".".$Type;				
			$OriginalImage =DIR_FS_SITE_UPLOADS_OTHER_ORIGINAL.$UploadName;
			copy($_FILES['Upload']['tmp_name'],$OriginalImage);
			chmod($OriginalImage,0777);
			$DataArray['Upload'] = $UploadName;
		}
		
		$FileObj->TableInsert($DataArray);
			
		@ob_clean();
							
		$_SESSION['InfoMessage'] ="Record updated successfully.";
		header("Location:".DIR_WS_SITE_CONTROL."index.php?Page=$Page&Section=$Section&Other=$Other&CategoryID=$CategoryID&FaqID=$FaqID");
		exit;
	
	break;
	

}
