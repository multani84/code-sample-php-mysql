<?
/*
 Table Used
*/
define("TABLE_OTHER_NEWS", "zsk_other_news",true);

if(isset($_GET['URLName']) && $_GET['URLName'] !="")
{
	$NewsObj= new DataTable(TABLE_SEO);
	$NewsObj->Where ="URLName='".$NewsObj->MysqlEscapeString($_GET['URLName'])."' AND RefrenceType='other_news'";
	$CurrentMeta = $NewsObj->TableSelectOne();
	if(isset($CurrentMeta->ReferenceID) && $CurrentMeta->ReferenceID !="")
	{
		$_REQUEST['Page'] = "other";
		$Other="News";
		$Section="Detail";
		$ModuleMeta = 1;
		$ModuleMetaDescription = isset($CurrentMeta->MetaDescription)?$CurrentMeta->MetaDescription:"";
		$ModuleMetaKeyword = isset($CurrentMeta->MetaKeyword)?$CurrentMeta->MetaKeyword:"";
		$ModuleMetaTitle = isset($CurrentMeta->MetaTitle)?$CurrentMeta->MetaTitle:"";
		$NewsID = $CurrentMeta->ReferenceID;
	}
}

if(isset($_GET['Page']) && $_GET['Page'] =="other_news")
{
	$_REQUEST['Page'] = "other";
	$Other="News";
	
	if(isset($_GET['ID']) && $_GET['ID'] !="")
	{
		$SEOObj= new DataTable(TABLE_SEO);
		$SEOObj->Where ="ReferenceID='".$SEOObj->MysqlEscapeString((int)$_GET['ID'])."' AND RefrenceType='other_news'";
		$CurrentMeta = $SEOObj->TableSelectOne();	
		$Section="Detail";
		$ModuleMeta = 1;
		$ModuleMetaDescription = isset($CurrentMeta->MetaDescription)?$CurrentMeta->MetaDescription:"";
		$ModuleMetaKeyword = isset($CurrentMeta->MetaKeyword)?$CurrentMeta->MetaKeyword:"";
		$ModuleMetaTitle = isset($CurrentMeta->MetaTitle)?$CurrentMeta->MetaTitle:"";
		$NewsID = $CurrentMeta->ReferenceID;
	}
	
}

if(@$_GET['Page'] =="other" && $_GET['Other'] =="News")
{
	$_REQUEST['Page'] = "other";
	$Other="News";
	if(isset($_GET['NewsID']) && $_GET['NewsID'] !="")
	{
		$SEOObj= new DataTable(TABLE_SEO);
		$SEOObj->Where ="ReferenceID='".$SEOObj->MysqlEscapeString((int)$_GET['NewsID'])."' AND RefrenceType='other_news'";
		$CurrentMeta = $SEOObj->TableSelectOne();	
		$Section="Detail";
		$ModuleMeta = 1;
		$ModuleMetaDescription = isset($CurrentMeta->MetaDescription)?$CurrentMeta->MetaDescription:"";
		$ModuleMetaKeyword = isset($CurrentMeta->MetaKeyword)?$CurrentMeta->MetaKeyword:"";
		$ModuleMetaTitle = isset($CurrentMeta->MetaTitle)?$CurrentMeta->MetaTitle:"";
		$NewsID = $CurrentMeta->ReferenceID;
	}
}


function ModuleOtherURLNews($URL,$Parameters="")
{
	parse_str($Parameters,$q_array);
	$URL = $Parameters !=""?$URL."?".$Parameters:$URL;
	
	return DIR_WS_SITE.$URL;
}


function GetNewsLeftPanel($Y,$M)
{
	$String = '<div class="NewsHeadlines">
					<h2>News Headlines</h2>
                    <div class="MonthLinks">';
	for($i=1;$i<=12;$i++)
	{
		$String .= '<a href="'.MakeModuleURL("index.php","Page=other&Other=News&Y=$Y&M=$i").'" '.(($M==$i)?"class='Active'":"").' >'.date("M", mktime(0, 0, 0, $i, 1, $Y)).'</a>';
		//if($i%6 !=0)
		$String .='&nbsp;|&nbsp;';
	}
	
	$NewsObj = new DataTable(TABLE_OTHER_NEWS);
	$NewsObj->Where = "Active='1'";
	if($M !="")
	{
		$Title = date("F Y", mktime(0, 0, 0, $M, 1, $Y));
		$NewsObj->Where .= " AND DATE_FORMAT(NewsDate,'%c-%Y') ='".$NewsObj->MysqlEscapeString($M."-".$Y)."'";
	}
	else
	{ 
		$Title =$Y;
		$NewsObj->Where .= " AND DATE_FORMAT(NewsDate,'%Y') ='".$NewsObj->MysqlEscapeString($Y)."'";
	}
				
	$NewsObj->TableSelectAll(array("*","DATE_FORMAT(NewsDate,'%d.%m.%Y') as MyNewsDate"),"NewsDate DESC",5);
					
	
	$String .= '</div>
                    <div class="ItemWrapper"><strong>'.$Title.'</strong>
                    <br />';
			       while ($NewsTmp =$NewsObj->GetObjectFromRecord() ) 
					{ 
						$String .= '<span class="Date">'.$NewsTmp->MyNewsDate.'</span><br />
		                    <a href="'.SKSEOURL($NewsTmp->NewsID,"other_news").'">'.MyStripSlashes($NewsTmp->NewsTitle).'&raquo;</a>
		                    <hr />
		                    ';
					}            
	
                   $String .= '</div>
                    <div class="Content">
                      <strong>Archives</strong><br />';
	
					$NewsObj = new DataTable(TABLE_OTHER_NEWS);
					$NewsObj->Where = "Active='1'";
					$NewsObj->TableSelectAll(array("distinct(DATE_FORMAT(NewsDate,'%Y')) as MyNewsDate"),"NewsDate DESC");
					while ($YearObj =$NewsObj->GetObjectFromRecord() ) 
					{
						$String .='<a href="'.MakeModuleURL("index.php","Page=other&Other=News&Y=".$YearObj->MyNewsDate).'" class="RedLink">'.$YearObj->MyNewsDate.'</a><br />';
			       }
			       	
			       $String .='</div>
                  			</div>';
	return $String;
}

?>