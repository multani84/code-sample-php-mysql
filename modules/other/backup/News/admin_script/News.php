<?
$NewsObj = new DataTable(TABLE_OTHER_NEWS);
$SeoObj = new DataTable(TABLE_SEO);


$DataArray = array();
$Other = isset($_GET['Other'])?$_GET['Other']:"";
$Section = isset($_GET['Section'])?$_GET['Section']:"";
$Target = isset($_GET['Target'])?$_GET['Target']:"";
$NewsID = isset($_GET['NewsID'])?$_GET['NewsID']:0;
$FileID = isset($_GET['FileID'])?$_GET['FileID']:0;
$FileObj = new DataTable(TABLE_OTHER_FILES);

	
	if($NewsID !=0)
	{
		$NewsObj = new DataTable(TABLE_OTHER_NEWS." ne LEFT JOIN ".TABLE_SEO." s on (s.ReferenceID = ne.NewsID AND s.RefrenceType ='other_news')");
		$NewsObj->Where ="ne.NewsID='".$NewsObj->MysqlEscapeString($NewsID)."'";
		$CurrentNews = $NewsObj->TableSelectOne();
	}

	if($FileID !=0)
	{
			$FileObj->Where = "FileID='".$FileObj->MysqlEscapeString($FileID)."'";
			$CurrentFile = $FileObj->TableSelectOne();
	}
/// Target  start 
switch ($Target)
{
	case "DeleteNews":
			$NewsObj = new DataTable(TABLE_OTHER_NEWS);
			$NewsObj->Where = "NewsID='".$NewsObj->MysqlEscapeString($NewsID)."'";
			$NewsObj->TableDelete();								
			
			$SeoObj->Where = "ReferenceID = ".$NewsID." AND RefrenceType ='other_news'";
			$SeoObj->TableDelete();	
			
			$FileObj = new DataTable(TABLE_OTHER_FILES);
			$FileObj->Where = "ReferenceType ='$Other' AND ReferenceID='".$FileObj->MysqlEscapeString($NewsID)."'";
			$FileObj->TableDelete();
			
			@unlink(DIR_FS_SITE_UPLOADS_OTHER_ORIGINAL.$CurrentNews->Image);
			@unlink(DIR_FS_SITE_UPLOADS_OTHER_ORIGINAL.$CurrentNews->File);
										
			@ob_clean();
			
			
			$_SESSION['InfoMessage'] ="News deleted successfully.";
			header("Location:".DIR_WS_SITE_CONTROL."index.php?Page=$Page&Other=$Other&Section=$Section");
			exit;
		
	break;	
	case "UpdateNews":
		for ($i=1;$i <$_POST['Count'];$i++)
		{
			$NewsObj->Where = "NewsID='".$NewsObj->MysqlEscapeString($_POST['NewsID_'.$i])."'";
			$DataArray['Active'] = isset($_POST['Active_'.$i])?$_POST['Active_'.$i]:0;			
			$DataArray['HomePage'] = isset($_POST['HomePage_'.$i])?$_POST['HomePage_'.$i]:0;			
			$NewsObj->TableUpdate($DataArray);		
		}
		ob_clean();
		
		
		$_SESSION['InfoMessage'] ="News updated successfully.";
		header("Location:".DIR_WS_SITE_CONTROL."index.php?Page=$Page&Other=$Other&Section=$Section");
		exit;
		
	break;
	
		case "AddNews":
			
			$DataArray['NewsTitle'] = isset($_POST['NewsTitle'])?$_POST['NewsTitle']:"";
			$DataArray['Description'] = isset($_POST['Description'])?$_POST['Description']:"";
			$DataArray['LongDescription'] = isset($_POST['LongDescription'])?$_POST['LongDescription']:"";
			$DataArray['NewsDate'] = (isset($_POST['NewsDate']) && $_POST['NewsDate'] !="")?$_POST['NewsDate']:"0000-00-00";
			$DataArray['Active'] = isset($_POST['Active'])?$_POST['Active']:"0";
			$DataArray['HomePage'] = isset($_POST['HomePage'])?$_POST['HomePage']:"0";
			$DataArray['UpperGallery1'] = (isset($_POST['UpperGallery1']) && $_POST['UpperGallery1'] !="")?implode(",",$_POST['UpperGallery1']):"";
			
			
			if(isset($_FILES['Upload']) && $_FILES['Upload']['name'] !="")
			{
				@unlink(DIR_FS_SITE_UPLOADS_OTHER_ORIGINAL.$CurrentNews->Image);
				$ArrayType = explode(".",$_FILES['Upload']['name']);
				$Type=$ArrayType[count($ArrayType)-1];
				
				$ImageName = uniqid("NewsImage_").".".$Type;				
				$OriginalImage =DIR_FS_SITE_UPLOADS_OTHER_ORIGINAL.$ImageName;
				copy($_FILES['Upload']['tmp_name'],$OriginalImage);
				chmod($OriginalImage,0777);
				$DataArray['Image'] = $ImageName;
			}
			
			if(isset($_FILES['File']) && $_FILES['File']['name'] !="")
			{
				@unlink(DIR_FS_SITE_UPLOADS_OTHER_ORIGINAL.$CurrentNews->File);
				$ArrayType = explode(".",$_FILES['File']['name']);
				$Type=$ArrayType[count($ArrayType)-1];
				
				$FileName = uniqid("NewsFile_").".".$Type;				
				$OriginalFile =DIR_FS_SITE_UPLOADS_OTHER_ORIGINAL.$FileName;
				copy($_FILES['File']['tmp_name'],$OriginalFile);
				chmod($OriginalFile,0777);
				$DataArray['File'] = $FileName;
			}
			
			if($NewsID==0)
			{
				$DataArray['CreatedDate'] =date('Y-m-d');
				$NewsID = $NewsObj->TableInsert($DataArray);		
				@ob_clean();
				
				
				$_SESSION['InfoMessage'] ="News added successfully.";
			}
			else 
			{
							
				$NewsObj->TableUpdate($DataArray);		
				@ob_clean();
				
				
				$_SESSION['InfoMessage'] ="News updated successfully.";
				
			}
			
			$SeoObj->Where = "ReferenceID = ".$NewsID." AND RefrenceType ='other_news'";
			$CurrentSEO = $SeoObj->TableSelectOne(array("ReferenceID"));
			
			$DataArray = array();
			$DataArray['URLName'] = SkURLCreate((isset($_POST['URLName']) && $_POST['URLName'] !="")?$_POST['URLName']:"news-".$_POST['NewsTitle']);
			$DataArray['RedirectEnabled'] = isset($_POST['RedirectEnabled'])?$_POST['RedirectEnabled']:0;
			$DataArray['RedirectURL'] = isset($_POST['RedirectURL'])?$_POST['RedirectURL']:"";
			
			if(@constant("SEO_META_ACTIVE") =="1")
			{
				$DataArray['MetaTitle'] = isset($_POST['MetaTitle'])?$_POST['MetaTitle']:"";
				$DataArray['MetaKeyword'] = isset($_POST['MetaKeyword'])?$_POST['MetaKeyword']:"";
				$DataArray['MetaDescription'] = isset($_POST['MetaDescription'])?$_POST['MetaDescription']:"";
			}
			
			if(isset($CurrentSEO->ReferenceID) && $CurrentSEO->ReferenceID !="")
			{
				$SeoObj->TableUpdate($DataArray);
			}
			else 
			{
				$DataArray['RefrenceType'] = "other_news";
				$DataArray['ReferenceID'] = $NewsID;
				$SeoObj->TableInsert($DataArray);
			}
		
			header("Location:".DIR_WS_SITE_CONTROL."index.php?Page=$Page&Other=$Other&NewsID=$NewsID");
			exit;

	break;
	
	case "DeleteFile":
		$FileObj->Where = "FileID='".$FileObj->MysqlEscapeString($FileID)."'";
		$FileObj->TableDelete();
		@unlink(DIR_FS_SITE_UPLOADS_OTHER_ORIGINAL.$CurrentFile->Upload);
					
		@ob_clean();
						
		$_SESSION['InfoMessage'] ="Record updated successfully.";
		header("Location:".DIR_WS_SITE_CONTROL."index.php?Page=$Page&Section=$Section&Other=$Other&NewsID=$NewsID");
		exit;
	
	break;
	case "EditFile":
		$DataArray = array();
		$DataArray['Active'] = isset($_POST['Active'])?$_POST['Active']:"0";
		$DataArray['Position'] = (isset($_POST['Position']) && $_POST['Position'] !="")?$_POST['Position']:"0";
		$DataArray['UploadType'] = (isset($_POST['UploadType']) && $_POST['UploadType'] !="")?$_POST['UploadType']:"Image";
		if(isset($_FILES['Upload']) && $_FILES['Upload']['name'] !="")
		{
				@unlink(DIR_FS_SITE_UPLOADS_OTHER_ORIGINAL.$CurrentFile->Upload);
				
				$ArrayType = explode(".",$_FILES['Upload']['name']);
				$Type=$ArrayType[count($ArrayType)-1];
				
				$UploadName = uniqid("File_".$NewsID."_").".".$Type;				
				$OriginalImage =DIR_FS_SITE_UPLOADS_OTHER_ORIGINAL.$UploadName;
				copy($_FILES['Upload']['tmp_name'],$OriginalImage);
				chmod($OriginalImage,0777);
				
				$DataArray['Upload'] = $UploadName;
				
							
		 }
			
		$FileObj->Where = "FileID='".$FileObj->MysqlEscapeString($FileID)."'";
		$FileObj->TableUpdate($DataArray);
			
		@ob_clean();
					
		$_SESSION['InfoMessage'] ="Record updated successfully.";
		header("Location:".DIR_WS_SITE_CONTROL."index.php?Page=$Page&Section=$Section&Other=$Other&NewsID=$NewsID");
		exit;
	
	break;
	case "AddFile":
	
		$DataArray = array();
		$DataArray['ReferenceID'] = $NewsID;
		$DataArray['ReferenceType'] = $Other;
		$DataArray['Active'] = isset($_POST['Active'])?$_POST['Active']:"0";
		$DataArray['Position'] = (isset($_POST['Position']) && $_POST['Position'] !="")?$_POST['Position']:"0";
		$DataArray['UploadType'] = (isset($_POST['UploadType']) && $_POST['UploadType'] !="")?$_POST['UploadType']:"Image";
		$DataArray['CreatedDate'] = date('Y-m-d H:i:s');
		
		if(isset($_FILES['Upload']) && $_FILES['Upload']['name'] !="")
		{
			$ArrayType = explode(".",$_FILES['Upload']['name']);
			$Type=$ArrayType[count($ArrayType)-1];
			
			$UploadName = uniqid("File_".$NewsID."_").".".$Type;				
			$OriginalImage =DIR_FS_SITE_UPLOADS_OTHER_ORIGINAL.$UploadName;
			copy($_FILES['Upload']['tmp_name'],$OriginalImage);
			chmod($OriginalImage,0777);
			$DataArray['Upload'] = $UploadName;
		}
		
		$FileObj->TableInsert($DataArray);
			
		@ob_clean();
				
		$_SESSION['InfoMessage'] ="Record updated successfully.";
		header("Location:".DIR_WS_SITE_CONTROL."index.php?Page=$Page&Section=$Section&Other=$Other&NewsID=$NewsID");
		exit;
	
	break;
	
	

}
