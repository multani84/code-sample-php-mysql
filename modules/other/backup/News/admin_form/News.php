<table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr>
		<td height="20" colspan="2" align="center">
		<h4>News</h4>
	</td>
	</tr>
	<tr>
		<td height="20" align="left">
		<?
			if(isset($CurrentNews->NewsID) && $CurrentNews->NewsID !="")
			{
			?>
			&nbsp;&raquo;<a href="<?=DIR_WS_SITE_CONTROL?>index.php?Page=<?=$Page?>&Other=<?=$Other?>" class="adm-link"><?=isset($CurrentNews->NewsTitle)?MyStripSlashes($CurrentNews->NewsTitle):""?></a>
			<?
			}?>
		</td>
		<td height="20" align="right">
			<a href="<?=DIR_WS_SITE_CONTROL?>index.php?Page=<?=$Page?>&Other=<?=$Other?>&Section=AddNews" class="adm-link">Add News</a>&nbsp;&nbsp;
		</td>
	</tr>
</table>				
<?
//// Section start
switch($Section)
{
	case "Files":
	case "EditFile":
		$FileObj->Where ="ReferenceID='$NewsID' AND ReferenceType='$Other'";
		$FileObj->TableSelectAll("","Position ASC");
		?>
			<script type="text/javascript">
				function FunctionDeleteFile()
				{
					result = confirm("Are you sure want to delete the File?")
					if(result == true)
					{
						return true;
					}
					return false;
				}				
							
			</script>
		<table border="0" cellpadding="3" cellspacing="1" width="100%" class="InsideTable">
			<tr class="InsideLeftTd">
				<td align="center" width="5%"><b>S.No.</b></td>
				<td align="left"><b>File</b></td>
				<td align="left"><b>Type</b></td>
				<td align="center"><b>Active</b></td>
				<td align="center"><b>Position</b></td>
				<td align="center"></td>
				<td align="center"></td>
			</tr>
	<?
			$SNo=1;
			$Count=1;
			while($CurrentFile = $FileObj->GetObjectFromRecord())
			{
				if($Section=="EditFile" && $FileID==$CurrentFile->FileID)
				{
					?>
					<form method="POST" action="<?=DIR_WS_SITE_CONTROL?>index.php?Page=<?=$Page?>&Section=<?=$Section?>&Other=<?=$Other?>&NewsID=<?=$NewsID?>&FileID=<?=$FileID?>&Target=EditFile" enctype="multipart/form-data">
						<tr class="InsideRightTd">
							<td align="center"><b><?=$SNo?></b></td>
							<td align="left">
							<?SKDisplayFileInfo($CurrentFile)?>
							<br><input type="file" name="Upload">
							</td>
							<td>
							<select name="UploadType">
								<?
								foreach ($ProductFileTypeArray as $k=>$v)
								{
								?>
									<option value="<?=$k?>" <?=$CurrentFile->UploadType==$k?"selected":""?>><?=$v?></option>
									<?
								}?>
								</select>
							</td>
							<td align="center"><input type="checkbox" name="Active" value="1" <?=$CurrentFile->Active=="1"?"checked":""?> class="chk"></td>
							<td align="center"><input type="text" name="Position" value="<?=MyStripSlashes($CurrentFile->Position);?>" size="5" style="text-align:center;"></td>
							<td align="center" ><input type="submit" name="Submit" value="Update"></td>
							<td align="center" ><input type="button" name="button" value="Cancel" onclick="javascript:history.back();"></td>
						</tr>
						<tr><td colspan="7"><hr></td></tr>
					</form>
					<?		
				}
				else 
				{
					?>
					<tr class="InsideRightTd">
						<td align="center"><b><?=$SNo?></b></td>
						<td align="left">
						<?SKDisplayFileInfo($CurrentFile)?>
						</TD>
						<td><?=isset($ProductFileTypeArray[$CurrentFile->UploadType])?$ProductFileTypeArray[$CurrentFile->UploadType]:""?></td>
						<td align="center"><img src="<?=DIR_WS_SITE_CONTROL_IMAGES.($CurrentFile->Active=="1"?"info.gif":"error.gif")?>">
						</td>
						<td align="center"><?=MyStripSlashes($CurrentFile->Position);?>
						</td>
						<td align="center"><a href="<?=DIR_WS_SITE_CONTROL?>index.php?Page=<?=$Page?>&Other=<?=$Other?>&Section=EditFile&NewsID=<?=$NewsID?>&FileID=<?=$CurrentFile->FileID?>"><b>Edit</b></a></td>
						<td align="center"><a href="<?=DIR_WS_SITE_CONTROL?>index.php?Page=<?=$Page?>&Other=<?=$Other?>&Section=<?=$Section?>&NewsID=<?=$NewsID?>&Target=DeleteFile&FileID=<?=$CurrentFile->FileID?>" onclick="return FunctionDeleteFile();"><b>Delete</b></a></td>
					</tr>
					<tr><td colspan="7"><hr></td></tr>
					<?
				}
			$SNo++;
			$Count++;
			}
		?>
		<form method="POST" action="<?=DIR_WS_SITE_CONTROL?>index.php?Page=<?=$Page?>&Other=<?=$Other?>&Section=<?=$Section?>&NewsID=<?=$NewsID?>&Target=AddFile" enctype="multipart/form-data">
			<tr class="InsideRightTd">
				<td align="center"><b><?=$SNo?></b></td>
				<td align="left"><input type="file" name="Upload"></td>
				<td>
					<select name="UploadType">
					<?
					foreach ($ProductFileTypeArray as $k=>$v)
					{
					?>
						<option value="<?=$k?>"><?=$v?></option>
						<?
					}?>
					</select>
				</td>
				<td align="center"><input type="checkbox" name="Active" value="1" class="chk"></td>
				<td align="center"><input type="text" name="Position" size="5"></td>
				<td align="center" colspan="2"><input type="submit" name="Submit" value="Add"></td>
			</tr>
		</form>
		</table>	
		<?
	break;
	case "AddNews" :
	case "EditNews" :
	/////// Add News  Start
	?>
	<form method="POST" action="<?=DIR_WS_SITE_CONTROL?>index.php?Page=<?=$Page?>&Other=<?=$Other?>&Section=<?=$Section?>&Target=AddNews&NewsID=<?=$NewsID?>" enctype="multipart/form-data">
		<table border="0" cellpadding="3" cellspacing="1" width="100%"%" class="InsideTable">
			<tr>
				<td align="right" class="InsideLeftTd"><b>News Title</b></td>
				<td align="left" class="InsideRightTd"><input type="text" name="NewsTitle" value="<?=isset($_POST['NewsTitle'])?$_POST['NewsTitle']:(isset($CurrentNews->NewsTitle)?MyStripSlashes($CurrentNews->NewsTitle):"")?>" size="60"></td>
			</tr>
			<tr>
				<td align="right" class="InsideLeftTd"><b>News Date</b></td>
				<td align="left" class="InsideRightTd">
					<table cellpadding="0" cellspacing="0" border="0">
						<tr>
							<td valign="middle"><input type="text" name="NewsDate"  ID="NewsDate" value="<?=(isset($_POST['NewsDate'])?$_POST['NewsDate']:(isset($CurrentNews->NewsDate)?$CurrentNews->NewsDate:""))?>" size="25" ReadOnly></td>
							<td valign="middle">
								<a href="#" id="SelectDate2"><img style="BORDER-RIGHT:0px; BORDER-TOP:0px; BORDER-LEFT:0px; BORDER-BOTTOM:0px" src="<?=DIR_WS_SITE_INCLUDES_JAVASCRIPT?>calendar/img.gif"></a>
								<script type="text/javascript">
									Calendar.setup({
											inputField     :    "NewsDate",     // id of the input field
											ifFormat       :    "%Y-%m-%d",     // format of the input field
											button         :    "SelectDate2",  // trigger for the calendar (button ID)
											align          :    "Bl",           // alignment (defaults to "Bl")
											singleClick    :    true,
											timeFormat	   :	24,
											showsTime	   :	false
										});
								</script>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<?
			if(@constant("SEO_URL_ACTIVE") =="1")
			{
			?>
			<tr>
				<td align="center" class="InsideLeftTd"><b>News URL</b>
				</td>
				<td align="left" class="InsideRightTd">
				<input type="text" name="URLName" value="<?=isset($_POST['URLName'])?MyStripSlashes($_POST['URLName']):(isset($CurrentNews->URLName)?MyStripSlashes($CurrentNews->URLName):"")?>" size="70" onblur="return CheckSpecialSymbol(this);" title="Please do not use special symbol. Only use alfa numeric characters,(_) underscores or (-) dashes.">
				<br><small>Please do not use special symbol. 
				<br>Only use alfa numeric characters,(_) underscores or (-) dashes.</small>
				</td>
			</tr>
			<?
			}?>
			<?
			if(@constant("SEO_META_ACTIVE") =="1")
			{
			?>									
			<tr>
				<td align="center" colspan="2"><hr></td>
			</tr>
			<tr>
				<td align="center" class="InsideLeftTd"><b>Meta Title</b></td>
				<td align="left" class="InsideRightTd"><input type="text" name="MetaTitle" value="<?=isset($_POST['MetaTitle'])?MyStripSlashes($_POST['MetaTitle']):(isset($CurrentNews->MetaTitle)?MyStripSlashes($CurrentNews->MetaTitle):"")?>" size="100"></td>
			</tr>
			<tr>
				<td align="center" class="InsideLeftTd"><b>Meta Keyword</b></td>
				<td align="left" class="InsideRightTd"><textarea name="MetaKeyword" cols="60" rows="3"><?=isset($_POST['MetaKeyword'])?MyStripSlashes($_POST['MetaKeyword']):(isset($CurrentNews->MetaKeyword)?MyStripSlashes($CurrentNews->MetaKeyword):"")?></textarea></td>
			</tr>
			<tr>
				<td align="center" class="InsideLeftTd"><b>Meta Description</b></td>
				<td align="left" class="InsideRightTd"><textarea name="MetaDescription" cols="60" rows="3"><?=isset($_POST['MetaDescription'])?MyStripSlashes($_POST['MetaDescription']):(isset($CurrentNews->MetaDescription)?MyStripSlashes($CurrentNews->MetaDescription):"")?></textarea></td>
			</tr>
			<?
			}?>
			<tr>
				<td align="center" colspan="2"><hr></td>
			</tr>									
			<tr>
				<td align="right" class="InsideLeftTd"><b>Description</b></td>
				<td align="left" class="InsideRightTd">
				<textarea name="Description" cols="40" rows="4"><?=isset($_POST['Description'])?$_POST['Description']:(isset($CurrentNews->Description)?MyStripSlashes($CurrentNews->Description):"")?></textarea>
				</td>
			</tr>
			<tr>
				<td align="right" class="InsideLeftTd"><b>Long Description</b></td>
				<td align="left" class="InsideRightTd">
				<!---Start content editor--->
						<?
				$sContent = isset($_POST['LongDescription'])?MyStripSlashes($_POST['LongDescription']):(isset($CurrentNews->LongDescription)?MyStripSlashes($CurrentNews->LongDescription):"");
				$SKEditorObj->CArray = array("Width"=>"700",
											 "Height"=>"400",
											 "DMode"=>"Large2",
											 "Help"=>true,
											 "EditorArray"=>array("css"=>DIR_WS_SITE_CONTROL."style.css",
											 					  ),
											 ); 
				$SKEditorObj->CreateEditor("LongDescription",$sContent);?>
						<!---end content editor--->
				</td>
			</tr>
			 <tr>
				<td align="center" class="InsideLeftTd"><b>Image</b><br>
				</td>
				<td align="left" class="InsideRightTd" valign="middle">
					<input type="file" name="Upload" size="30">
					&nbsp;&nbsp;
					<?
					if(isset($CurrentNews->Image) && $CurrentNews->Image !="" && file_exists(DIR_FS_SITE_UPLOADS_OTHER_ORIGINAL.$CurrentNews->Image))
					{?>
						<?SKImgDisplay(DIR_FS_SITE_UPLOADS_OTHER_ORIGINAL.$CurrentNews->Image,100,MyStripSlashes($CurrentNews->NewsTitle));?>
				  <?}?>
							
				</td>
			</tr> 
			
			<tr>
				<td align="right" class="InsideLeftTd"><b>Active</b></td>
				<td align="left" class="InsideRightTd">
					<input type="checkbox" name="Active" value="1" class="chk" <?=(isset($_POST['Active']) && $_POST['Active']==1)?"checked":((isset($CurrentNews->Active) && $CurrentNews->Active==1)?"checked":"")?>>
				</td>
			</tr>
			<!-- <tr>
				<td align="right" class="InsideLeftTd"><b>HomePage</b></td>
				<td align="left" class="InsideRightTd">
					<input type="checkbox" name="HomePage" value="1" class="chk" <?=(isset($_POST['HomePage']) && $_POST['HomePage']==1)?"checked":((isset($CurrentNews->HomePage) && $CurrentNews->HomePage==1)?"checked":"")?>>
				</td>
			</tr> -->
			<tr>
				<td align="right" class="InsideLeftTd"></td>
				<td align="left" class="InsideRightTd">
					<input type="submit" name="AddNews" value="Submit">
				</td>
			</tr>
		</table>
						
	</form>
	<?
	/////// Add News End
	break;

	/////////Display News 
	case "DisplayNews" :
	default:
		$NewsObj->Where = "1";
		$NewsObj->TableSelectAll(array("*","DATE_FORMAT(NewsDate,'%b %d, %Y') as MyNewsDate"),"NewsDate DESC");
		if($NewsObj->GetNumRows() > 0)
		{
		?>
		<script>
				function FunctionDeleteNews(NewsID)
				{
					
						result = confirm("Are you sure want to delete the news?")
						if(result == true)
						{
							URL="<?=DIR_WS_SITE_CONTROL?>index.php?Page=<?=$Page?>&Other=<?=$Other?>&Target=DeleteNews&&NewsID=" + NewsID;
							location.href=URL;
							return true;
						}
					return false;
				}				
			</script>							
		<form method="POST" action="<?=DIR_WS_SITE_CONTROL?>index.php?Page=<?=$Page?>&Other=<?=$Other?>&Section=<?=$Section?>&Target=UpdateNews">
		<table border="0" cellpadding="3" cellspacing="1" width="100%" class="InsideTable">
			<tr>
				<td align="center" width="5%" class="InsideLeftTd"><b>S.No.</b></td>
				<td align="left"  class="InsideLeftTd"><b>News Title</b></td>
				<td align="center"  class="InsideLeftTd"><b>News Date</b></td>
				<td align="center"  class="InsideLeftTd"><b>Image</b></td>
				<td align="center"  class="InsideLeftTd"><b>Active</b></td>
				<!-- <td align="center"  class="InsideLeftTd"><b>HomePage</b></td> -->
				<td align="center"  class="InsideLeftTd"><b>Edit</b></td>
				<td align="center"  class="InsideLeftTd"><b>Delete</b></td>
			</tr>
			<?
			$SNo=1;
			$Count=1;
			while($CurrentNews = $NewsObj->GetObjectFromRecord())
			{
					
				?>
			<tr >
				<td align="center" class="InsideRightTd"><b><?=$SNo?></b></td>
				<td align="left" class="InsideRightTd">
				<?echo MyStripSlashes($CurrentNews->NewsTitle);?><br />
				<a href="<?=SKSEOURL($CurrentNews->NewsID,"other_news")?>" target="_blank">Preview</a>		
				<input type="hidden" name="NewsID_<?=$Count?>" value="<?=$CurrentNews->NewsID?>"></td>
				<td align="center" class="InsideRightTd"><?=isset($CurrentNews->MyNewsDate)?$CurrentNews->MyNewsDate:"";?></td>
				<td align="center" class="InsideRightTd">
				<?SKImgDisplay(DIR_FS_SITE_UPLOADS_OTHER_ORIGINAL.$CurrentNews->Image,100,MyStripSlashes($CurrentNews->NewsTitle));?>
				<br>
<!-- 				[<a href="<?=DIR_WS_SITE_CONTROL?>index.php?Page=<?=$Page?>&Other=<?=$Other?>&Section=Files&NewsID=<?=$CurrentNews->NewsID?>" class="adm-link">Add More Images/Files</a>] -->
				</td>
				<td align="center" class="InsideRightTd">
					<input type="checkbox" name="Active_<?=$Count?>" value="1" class="chk" <?=(isset($CurrentNews->Active) && $CurrentNews->Active==1)?"checked":""?>>
				</td>
				<!-- <td align="center" class="InsideRightTd">
					<input type="checkbox" name="HomePage_<?=$Count?>" value="1" class="chk" <?=(isset($CurrentNews->HomePage) && $CurrentNews->HomePage==1)?"checked":""?>>
				</td> -->
				<td align="center" class="InsideRightTd"><a href="<?=DIR_WS_SITE_CONTROL?>index.php?Page=<?=$Page?>&Other=<?=$Other?>&Section=EditNews&NewsID=<?=$CurrentNews->NewsID?>"><b>Edit</b></a></td>
				<td align="center" class="InsideRightTd">
					<a href="#" onclick="return FunctionDeleteNews('<?=$CurrentNews->NewsID?>');"><b>Delete</b></a>
				</td>
			</tr>
			<?
			$SNo++;
			$Count++;
			}
		?>
		<tr class="InsideLeftTd">
				<td align="center" width="5%"></td>
				<td align="center"></td>
				<!-- <td align="center"></td> -->
				<td align="center"><input type="hidden" name="Count" value="<?=$Count?>"></td>
				<td align="center"></td>
				<td align="center"><input type="submit" name="UpdateHomePage" value="Update"></td>
				<td align="center"></td>
				<td align="center"></td>
			</tr>
	
		</table>
		</form>					
		<?
		}
		else 
		{
			?>
			<b>No Result Found.</b>
			<?
		}
	break;
}
///// Section end?>