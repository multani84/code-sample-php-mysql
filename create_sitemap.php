<?php
require_once("includes/configure.php");
function MySiteMapCreator($ParentID,$CurrentPageID=0,$SelectedElementArr=null,$ActiveOnly=true,$Level=0) 
{
	$Text = "";
	$PageObj = new DataTable(TABLE_PAGES);
	//$PageObj->Where ="PageID >1000 AND ParentID='".$PageObj->MysqlEscapeString($ParentID)."'";
	$PageObj->Where ="PageID >1000 ";
	if($ActiveOnly)
		$PageObj->Where .=" And Active='1'";
		
	$PageObj->TableSelectAll(array("PageID",0,"PageName"),"Position ASC ");
	while ($CurrentPage = $PageObj->GetObjectFromRecord())  
	{
			$Text .=  '<url>
						<loc>'.SKSEOURL($CurrentPage->PageID,"cms").'</loc>
						<lastmod>'.date('Y-m-d').'</lastmod>
						<changefreq>monthly</changefreq>
						<priority>'.($ParentID==0?'1.0':'0.5').'</priority>
					</url>';
			//$Text .= MySiteMapCreator($CurrentPage->PageID,$CurrentPageID,$SelectedElementArr,$ActiveOnly,$Level+1);     
		
	}
	return $Text;
}

/* Category Sitemap start*/


function MySiteMapCategory($ParentID,$CurrentCategoryID=0,$SelectedElementArr=null,$ActiveOnly=false,$Level=0) 
{
	$Text = "";
	$CategoryObj = new DataTable(TABLE_CATEGORY);
	$CategoryObj->Where ="ParentID='".$CategoryObj->MysqlEscapeString($ParentID)."'";
	if($ActiveOnly)
		$CategoryObj->Where .=" And Active='1'";
		
	$CategoryObj->TableSelectAll(array("CategoryID",0,"CategoryName"),"Position ASC ");
	while ($CurrentCategory = $CategoryObj->GetObjectFromRecord())  
	{
			$Text .=  '<url>
						<loc>'.SKSEOURL($CurrentCategory->CategoryID,"shop/category").'</loc>
						<lastmod>'.date('Y-m-d').'</lastmod>
						<changefreq>monthly</changefreq>
						<priority>'.($ParentID==0?'1.0':'0.5').'</priority>
					</url>';
			$Text .= MySiteMapCategory($CurrentCategory->CategoryID,$CurrentCategoryID,$SelectedElementArr,$ActiveOnly,$Level+1);     
		
	}
	return $Text;
}

function MySiteMapProduct($ActiveOnly=false) 
{
	$Text = "";
	$ProductObj = new DataTable(TABLE_PRODUCT);
	$ProductObj->Where ="ProductType != 'Child'";
	if($ActiveOnly)
		$ProductObj->Where .=" And Active='1'";
		
	$ProductObj->TableSelectAll(array("ProductID",0,"ProductName"),"Position ASC ");
	while ($CurrentProduct = $ProductObj->GetObjectFromRecord())  
	{
			$Text .=  '<url>
						<loc>'.SKSEOURL($CurrentProduct->ProductID,"shop/product").'</loc>
						<lastmod>'.date('Y-m-d').'</lastmod>
						<changefreq>monthly</changefreq>
						<priority>1.0</priority>
					</url>';
		
	}
	return $Text;
}

/* Category Sitemap end*/

$Text ='<?xml version="1.0" encoding="iso-8859-1"?>
	<urlset xmlns="http://www.google.com/schemas/sitemap/0.84"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:schemaLocation="http://www.google.com/schemas/sitemap/0.84
                      http://www.google.com/schemas/sitemap/0.84/sitemap.xsd">';

$Text .= MySiteMapCreator(0,0,array());

$Text .= MySiteMapCategory(0,0,array());
$Text .= MySiteMapProduct(true);

$Text .= '</urlset>';

$fp = fopen(DIR_FS_SITE."sitemap.xml", "w");
	  fputs($fp,$Text);	
	  fclose($fp);
?>
<a href='<?php echo DIR_WS_SITE."sitemap.xml"?>' target="_blank">[SITEMAP]</a>