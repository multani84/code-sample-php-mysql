<div class="container-fluid">
    	<div class="row no-gutter">
        	<div class="col-lg-2 col-sm-3">
            	<div id="LeftSection" class="bg-danger">
                	<header id="Header">
                    	<a href="<?php echo SKSEOURL(d("DEFAULT_PAGEID"),"cms")?>"><img src="<?php echo DIR_WS_SITE_TEMPLATES.DEFINE_TEMPLATE_STYLE?>/images/logo_crystal.png" alt="Crystal Sanitary" class="img-responsive center-block logo"></a>
                        <div id="sidebar-nav">
                            <div class="navbar" role="navigation">
                                <div class="navbar-header">
                                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-navbar-collapse">
                                        <span class="sr-only">Toggle navigation</span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                    </button>
                                </div>
                                <div class="navbar-collapse collapse sidebar-navbar-collapse">
                                    <ul id="accordion" class="nav navbar-nav" <?php echo (isset($CategoryID) && $CategoryID !="")?' style="display:none"':''?>>
										<?php 	$PageObj = new DataTable(TABLE_PAGES);
												$PageObj->Where ="ParentID='0' AND Active='1'";
												$PageObj->TableSelectAll('','Position ASC');
												while($CurrentPageNav = $PageObj->GetObjectFromRecord()):
											?>
									    	<?php 
											if(1121==$CurrentPageNav->PageID):?>
											<li class="panel"><a href="#ProductNavCon" class="dropdown-toggle collapsed" data-toggle="collapse" id="ProductNavLink">Products <b class="caret"></b></a>
											
											</li>
											<?php else:?>
											<li class="panel"><a href="<?php echo SKSEOURL($CurrentPageNav->PageID,"cms")?>"><?php echo GetPageLinkTitle($CurrentPageNav->PageID);?><b class="caret"></b></a></li>
											<?php endif;
										 endwhile;?>
                                   </ul>
								   <div class="CategoryNav" id="ProductNavCon" <?php echo (isset($CategoryID) && $CategoryID !="")?' style="display:block"':''?>>
										<?php if(file_exists(DIR_FS_SITE."source/shop/category_nav.php"))
													require_once(DIR_FS_SITE."source/shop/category_nav.php");
										?>
										<a href="javascript:;" id="nav-back" class="nav-back"><i class="fa fa-arrow-left"></i> BACK</a>	
									</div>
                                </div>
                            </div>
                        </div>
                    </header>
                    <footer id="Footer">
                    	<div class="social">
                        	<a href="#"><i class="fa fa-twitter"></i></a>
                        	<a href="#"><i class="fa fa-facebook"></i></a>
                        	<a href="#"><i class="fa fa-google-plus"></i></a>
                        </div>
                    	<div class="copy">&copy; 2016. All rights reserved<br /> Crystal Sanitary Fitting Pvt. Ltd.</div>
                    </footer>
                </div>
            </div>
        	<div class="col-lg-10 col-sm-9">
            	<div id="MainContentWrap">
                    <div Id="TopNav"><a href="<?php echo SKSEOURL(d("DEFAULT_PAGEID"),"cms")?>">HOME</a> <a href="mailto:<?php echo d("ADMIN_EMAIL")?>">MAIL</a> <a href="<?php echo SKSEOURL(1117,"cms")?>">CONTACTS</a> <?php if(isset($_SESSION['EnquiryProducts']) && is_array($_SESSION['EnquiryProducts']) && count($_SESSION['EnquiryProducts']) > 0){?><a href="<?php  echo MakePageURL("index.php","Page=shop/product_enquiry")?>">VIEW ENQUIRY</a><?php }?></div>
                    <div id="SearchPanel">
                        <form class="form-inline" role="form" action="<?php echo MakePageURL("index.php","Page=shop/search_results")?>" method="GET" name="search_f" id="search_f">
                         <input type="hidden" name="Page" value="shop/search_results">
						 <div class="form-group">
                                <div class="input-group">
                                    <input type="text" id="Search" class="form-control" value="<?=(isset($_REQUEST['k']) && $_REQUEST['k'] !="")?$_REQUEST['k']:""?>" name="k" placeholder="Search" />
                                    <span class="input-group-btn"><button class="btn" type="submit"><i class="fa fa-search"></i></button></span>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="clearfix"></div>
