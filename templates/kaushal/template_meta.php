<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600|Oswald:400,500" rel="stylesheet">
<link href="<?php echo DIR_WS_SITE_TEMPLATES.DEFINE_TEMPLATE_STYLE?>/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo DIR_WS_SITE_TEMPLATES.DEFINE_TEMPLATE_STYLE?>/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo DIR_WS_SITE_TEMPLATES.DEFINE_TEMPLATE_STYLE?>/css/style.css" rel="stylesheet" type="text/css" />
<link href="<?php echo DIR_WS_SITE_TEMPLATES.DEFINE_TEMPLATE_STYLE?>/extra.css" rel="stylesheet" type="text/css" />
<!--[if lt IE 9]>
  <script src="//oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="//oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
<script type="text/javascript">
var sk_overlay_bg = "<?php echo DIR_WS_SITE_GRAPHICS?>/blank.png";
var sk_loading_img = "<?php echo DIR_WS_SITE_GRAPHICS?>/ajax-loader.gif";
</script>