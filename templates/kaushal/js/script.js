// JavaScript Document
$(function () {
    var ww = $(window).width();
    if (ww >= 768) {
        var wh = $(window).height();
        $(".btmLogo").css("top", (wh - 135) + "px");
    }
	
	jQuery("#ProductNavLink").bind("click",function(){
		jQuery("#accordion").hide("300",function(){
			jQuery("#ProductNavCon").show("300");
		});
		return false;
	})
	
	jQuery("#nav-back").bind("click",function(){
		jQuery("#ProductNavCon").hide("300",function(){
			jQuery("#accordion").show("300");
		});
		return false;
	})
});