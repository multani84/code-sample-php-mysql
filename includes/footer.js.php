<?php 
require_once (dirname(__FILE__)."/configure.php");

$CountryArray = $RegionArray = array();
$CountryObj = new DataTable(TABLE_COUNTRIES);	
$RegionObj = new DataTable(TABLE_COUNTRY_REGION);	
$CountryObj->Where = "Active=1";
$CountryObj->TableSelectAll(array("CountryID,CountryName,CountryISOCode1,CountryISOCode2"),"CountryName='".DEFINE_DEFAULT_COUNTRY."' DESC,CountryName ASC");
while ($CurrentCountry = $CountryObj->GetArrayFromRecord()) 
{
	array_walk($CurrentCountry,"sk_json_string");
	
	$CountryArray[] = $CurrentCountry;
	$RegionObj->Where = "country_id = '".$CurrentCountry['CountryID']."'";
	$RegionObj->TableSelectAll(array("*"),"region_name ASC");
	while ($CurrentRegion = $RegionObj->GetArrayFromRecord()) 
	{
		array_walk($CurrentRegion,"sk_json_string");
		$RegionArray[] = $CurrentRegion;
	}

	
}



function sk_json_string(&$item, $key)
{
	 $item = utf8_encode($item);
}
?>
var countries = <?php echo json_encode($CountryArray)?>;	
var regions = <?php echo json_encode($RegionArray)?>;	

for (k = 0; k < regions.length; k++) 
{
	//console.log(regions[k].region_name);
}
jQuery(document).ready(function($){
	 //$('[data-toggle="tooltip"]').tooltip();   
	if($("select[rel^='sk_country']").length > 0)
	{
		$("select[rel^='sk_country']").each(function( index ) {
			
			 country_string = "";
			for (k = 0; k < countries.length; k++) 
			 {
				if($(this).attr("data-value") != null &&  $(this).attr("data-value") != undefined && $(this).attr("data-value") != "" && 
				(String($(this).attr("data-value")).toLowerCase()==String(countries[k].CountryISOCode1).toLowerCase() || String($(this).attr("data-value")).toLowerCase()==String(countries[k].CountryName).toLowerCase())) 
					country_string = country_string + "<option value='"+countries[k].CountryISOCode1+"' selected>"+countries[k].CountryName+"</option>";
				else
					country_string = country_string + "<option value='"+countries[k].CountryISOCode1+"'>"+countries[k].CountryName+"</option>";
			 }
			  $(this).empty();
			 $(this).append(country_string);
			
			 $(this).on("change",function(){
					ChangeRegionBox($(this));		
			 });
			 ChangeRegionBox($(this));
			 
		});
	 
	 
	}

function ChangeRegionBox(obj)
{
	reg_obj = jQuery(obj.attr("data-regionid"));
	parent_obj = reg_obj.parent().eq(0);
	region_string = "";
	 for (k = 0; k < countries.length; k++) 
	 {
		  for (i = 0; i < regions.length; i++) 
		  {
			  if(countries[k].CountryISOCode1==obj.val() && regions[i].country_id==countries[k].CountryID)
			  {
				region_string = region_string + "<option value='"+regions[i].code+"'>"+regions[i].region_name+"</option>";
			  }
		  }
		
	 }
	 
	 if(reg_obj.length > 0)
	 {
		 
		if(reg_obj.is('input'))
		{
			if(region_string != "") 
			{
				if(reg_obj.parent("[data-select!='']").length > 0)
				{
					parent_obj.append(reg_obj.parent("[data-select!='']").attr("data-select"));
					reg_obj.remove();
				}
				jQuery(obj.attr("data-regionid")).empty();
				jQuery(obj.attr("data-regionid")).append(region_string);
				
			}
			else
			{
				
			}
		}
		
		if(reg_obj.is('select'))
		{
			reg_obj.empty();
			if(region_string != "") 
			{
				reg_obj.append(region_string);
			}
			else
			{
				if(reg_obj.parent("[data-input!='']").length > 0)
				{
					parent_obj.append(reg_obj.parent("[data-input!='']").attr("data-input"));
					reg_obj.remove();
					
				}
			}
		}
		if(jQuery(obj.attr("data-regionid")).attr("value") != "")
		{
			jQuery(obj.attr("data-regionid")).val(jQuery(obj.attr("data-regionid")).attr("value"));
		}
		else if(jQuery(obj.attr("data-regionid")).attr("data-val") != "")
		{
			jQuery(obj.attr("data-regionid")).val(jQuery(obj.attr("data-regionid")).attr("data-val"));
		}
		
	 }
	 
}	
	
});
