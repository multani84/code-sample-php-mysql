function ValidateForm(form, NotSubmit)
{
	if(!form.elements)
		form = document.getElementById(form);
	
	var x = form.elements;
	for (var i=0;i<x.length;i++)
	{
		EleObj=x[i].id;
		LastIndex = EleObj.substring(EleObj.length-2,EleObj.length)
		switch(LastIndex)
		{
			case "_R":
				if(ValidateForm_Required(x[i],x[i].title)==false)
					return false;
			break;
			case "_U":
				if(ValidateForm_Required(x[i],x[i].title)==false)
					return false;
				if(ValidateForm_AlfaNumeric(x[i],x[i].title)==false)
					return false;
			break;
			case "_P":
				if(ValidateForm_Password(x[i],x[i].title,6)==false)
					return false;
			break;
			case "_F":
				if(ValidateForm_AlfaNumeric(x[i],x[i].title)==false)
					return false;
			break;
			case "_E":
				if(ValidateForm_Email(x[i],x[i].title)==false)
					return false;
			break;
			case "_C":
				if(ValidateForm_Confirm(x[i-1],x[i],x[i].title)==false)
					return false;
			break;
			case "_O":
				if(ValidateForm_Other(x[i],x[i+1],x[i].title)==false)
					return false;
			break;
			case "_I":
				if(ValidateForm_Required(x[i],x[i].title)==false)
					return false;
				if(ValidateForm_Numeric(x[i],'0123456789',x[i].title)==false)
					return false;
			break;
			case "_G":
				if(ValidateForm_Required(x[i],x[i].title)==false)
					return false;
				if(ValidateForm_Numeric(x[i],'0123456789',x[i].title)==false)
					return false;
				if(ValidateForm_Compare(x[i],0,">",x[i].title)==false)
					return false;
			break;
			case "_N":
				if(ValidateForm_Numeric(x[i],'0123456789',x[i].title)==false)
					return false;
			break;
			case "_A":
			FieldArray = form.elements[x[i].name];
			if(ValidateForm_CheckArray(x[i],FieldArray,x[i].title)==false)			
				return false;
			break;
		}		
	}
	if(NotSubmit =="1")
		return true;
	else
		form.submit();
	
	
return false;
}
function ValidateForm_Required(Ctrl,msg)
{
	ElementType = Ctrl.type;
	ElementTypeString = ElementType.toUpperCase();
	switch(ElementTypeString)
	{
		case "CHECKBOX":
			if(Ctrl.checked == false)
			{
				alert(msg);
				Ctrl.focus();
				return false;
			}
		break;
		default :
			if(trimString(Ctrl.value) == "")
			{
				alert(msg);
				Ctrl.focus();
				return false;
			}
		break;		
		
	}
	return true;
}

function ValidateForm_Password(Ctrl,msg,Minlen)
{
	var charpos = Ctrl.value.search("[^A-Za-z0-9]"); 
	if(trimString(Ctrl.value).length < Minlen ||  charpos >= 0) 
      { 
      	alert(msg);
		Ctrl.focus();
		return false;
      } 
		
	return true;
}
function ValidateForm_AlfaNumeric(Ctrl,msg)
{
	var charpos = Ctrl.value.search("[^A-Za-z0-9]"); 
	if(charpos >= 0) 
      { 
      	alert(msg);
		Ctrl.focus();
		return false;
      } 
		
	return true;
}
function ValidateForm_CheckArray(Ctrl,FieldArray,msg)
{
	MyCheck = true;
	for (var j=0;j<FieldArray.length;j++)
	{
		if(FieldArray[j].checked)
			MyCheck = false;
		
	}
	if(MyCheck)
	{
		alert(msg);
		Ctrl.focus();
		return false;
	}	
	return true;
}
function ValidateForm_Checked(Ctrl,msg)
{
	if(Ctrl.checked == false)
	{
		alert(msg);
		Ctrl.focus();
		return false;
	}
	return true;
}
function ValidateForm_Numeric(Ctrl,valid_chars,msg)
{
	if(chkNumericValidate(Ctrl.value,valid_chars) == false)
	{
		alert(msg);
		Ctrl.focus();
		return false;
	}
	return true;
}
function ValidateForm_Compare(Ctrl,val,operator,msg)
{
	switch(operator)
	{
		case ">":
			if(Ctrl.value > val == false)
			{
				alert(msg);
				Ctrl.focus();
				return false;
			}
			break;
		case "<":
			if(Ctrl.value < val == false)
			{
				alert(msg);
				Ctrl.focus();
				return false;
			}
			break;
		case "==":
			if(Ctrl.value == val == false)
			{
				alert(msg);
				Ctrl.focus();
				return false;
			}
			break;
		case "<=":
			if(Ctrl.value <= val == false)
			{
				alert(msg);
				Ctrl.focus();
				return false;
			}
			break;
		case ">=":
			if(Ctrl.value >= val == false)
			{
				alert(msg);
				Ctrl.focus();
				return false;
			}
			break;
		default:
			alert("Wrong parameter for operator.");
	}
	return true;
}
function chkNumericValidate(strString,strValidChars)
{
   var strChar;
   var blnResult = true; 
  	for (i = 0; i < strString.length && blnResult == true; i++)
   {
	  strChar = strString.charAt(i);
      if (strValidChars.indexOf(strChar) == -1)
      {
    	   blnResult = false;
      }
   }
   return blnResult;
}
function ValidateForm_Email(Ctrl,msg)
{
	if(chkEmailValidate(Ctrl.value) == false)
	{
		alert(msg);
		Ctrl.focus();
		return false;
	}
	return true;
}

function ValidateForm_Confirm(Ctrl1,Ctrl2,msg)
{
	if(Ctrl1.value != Ctrl2.value)
	{
		alert(msg);
		Ctrl2.focus();
		return false;
	}
	return true;
}
function ValidateForm_Other(Ctrl1,Ctrl2,msg)
{
	if(trimString(Ctrl1.value) =="" && trimString(Ctrl2.value)=="")
	{
		alert(msg);
		Ctrl1.focus();
		return false;
	}
	return true;
}

function chkEmailValidate(str)
{
	return(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(str));
}
function trimString (str)
{
  str = this != window? this : str;
  return str.replace(/^\s+/g, '').replace(/\s+$/g, '');
}
 function getObject(nameStr) 
 {
    var ie  = (document.all);
    var ns4 = document.layers? true : false;
    var dom = document.getElementById && !document.all ? true : false;

    if (dom) {
        return document.getElementById(nameStr);
    } else if (ie) {
        return document.all[nameStr];
    } else if (ns4) {
        return document.layers[nameStr];
    }
}
function counterUpdate(opt_countedTextBox, opt_countBody, opt_maxSize) 
{
  var countedTextBox = opt_countedTextBox ?
    opt_countedTextBox : "countedTextBox";
  var countBody = opt_countBody ? opt_countBody : "countBody";
  var maxSize = opt_maxSize ? opt_maxSize : 1024;
    
  var field = document.getElementById(countedTextBox);
  if (field && field.value.length >= maxSize) 
  {
    field.value = field.value.substring(0, maxSize);
  }
  var txtField = document.getElementById(countBody);
  if (txtField) 
  {  
    txtField.innerHTML = field.value.length;
  }
}

	function chatExm()
	{
		DivObj = document.createElement("div");
		DivObj.setAttribute("id", "MyDivID");
		DivObj.style.left = "200px";
		DivObj.style.top = "300px";
		DivObj.style.display = "block";
		DivObj.style.zIndex = "1";
		DivObj.style.position ="absolute";
		DivObj.innerHTML ="sdsdfdsfsdfdsfsdff";
		document.body.appendChild(DivObj);
		return false;
	}
	




function CheckInvalidQuantity(Quantity,PreValue,msg)
{
	if(parseInt(Quantity.value) ==0)
	{
		alert(msg);
		Quantity.value =PreValue;
		Quantity.focus();
	}
	if(isNaN(Quantity.value)==true)
	{
		alert(msg);
		Quantity.value =PreValue;
		Quantity.focus();
	}
	if(CheckFloatValue(Quantity.value))
	{
		alert(msg);
		Quantity.value =PreValue;
		Quantity.focus();
	}															
	
}

function CheckFloatValue(f) 
{
	var r = /^[0-9]*$/;
	if (!r.test(f)) 
	{
		// return f = f.replace(/[^0-9]/g,"");
		return true;		
	}
return false;
} 

function CheckSpecialSymbol(Obj)
{
	if(ValidateForm_Required(Obj,Obj.title)==false)
		return false;
	if(ValidateForm_SkipSymbol(Obj,Obj.title)==false)
		return false;
}
function ValidateForm_SkipSymbol(Ctrl,msg)
{
	var charpos = Ctrl.value.search("[^A-Za-z0-9_-]"); 
	if(charpos >= 0) 
      { 
      	alert(msg);
		Ctrl.focus();
		return false;
      } 
		
	return true;
}
function ChangeCaptcha(obj,src)
{
	var TimeStr = new Date().getTime();
	obj.src = src + TimeStr;
	return false;	
}

function SkLeftNavigation(id)
{
	 $("#"+id).next("div.main_prd").slideDown(500).siblings("div.main_prd").slideUp("slow");
     $("#"+id).siblings().find("a").css({"color":"#ffffff"});
     $("#"+id).find("a").css({"color":"#ff0000"});
}
			   



								