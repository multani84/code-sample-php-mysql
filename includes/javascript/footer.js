jQuery(document).ready(function($){
	SKajaxLink($)
	if($("a[rel^='colorbox']").length > 0)
	{
	  $("a[rel^='colorbox']").colorbox({iframe:true, width:600, height:600});
	}
	  
});

function SKajaxLink($)
{
	$(".sk_ajax").bind("click",function(){
		var sk_link = $(this).attr("href").replace('index.php', 'ajax.php'); 
		sk_q = sk_link.indexOf("?");
		sk_link_ajax = sk_link.substring(0,sk_q);
		sk_query_ajax = sk_link.substring(sk_q+1,sk_link.length);
		var sk_rel = $(this).attr("rel");
		sk_a = sk_rel.indexOf("@@@");
		sk_contain = sk_rel.substring(0,sk_a);
		sk_plu = sk_rel.substring(sk_a+3,sk_rel.length);
		sk_query_ajax = "SKPlugin="+sk_plu+"&"+sk_query_ajax;

		SKDisplayResultFromParms(sk_link_ajax,sk_contain,sk_plu,sk_query_ajax);
		return false;
	});
	
}

//++++++++++++++++++++ Ovelay Function start //
function DisableOverlay() {
    jQuery("#Overlay").animate({
        left: jQuery(document).width() / 2 + "px",
        top: jQuery(window).height() / 2 - 20 + "px",
        width: "0px",
        height: "0px"
    }, 100, function () {
        jQuery(this).remove();
    });
    return false;
}

function EnableOverlay(BgImg) {
    if (jQuery("#Overlay").length > 0) return;
    jQuery("body").append(jQuery("<div></div>").attr("id", "Overlay").css({
        position: "absolute",
        left: jQuery(document).width() / 2 + "px",
        top: jQuery(window).height() / 2 - 20 + "px",
        zIndex: 9999999,
        width: "0px",
        height: "0px",
        backgroundImage: "url("+BgImg+")"
    }).animate({
        left: "0px",
        top: "0px",
        width: "100%",
        height: jQuery(document).height() + "px"
    }, 0));

    if (jQuery.browser.version == "6.0" && jQuery.browser.msie)
        jQuery("#Overlay").css({ filter: "progid:DXImageTransform.Microsoft.AlphaImageLoader(src="+BgImg+",sizingmethod=scale)" });
    else
        jQuery("#Overlay").css({ backgroundImage: "url("+BgImg+")" });
}
//++++++++++++++++++++ Ovelay Function end //
//++++++++++++++++++++ Ajax Function start //

function SKDisplayResultFromParms(sk_link,sk_contain,sk_plugin,sk_data)
{
	
	jQuery.ajax({
	  'type' : 'GET',
	  'url' : sk_link,
	  'data': sk_data,
	  'dataType': "html",
	  'beforeSend':function(){
	  	var top = $("#"+sk_contain).offset().top - 170;
    	$('html, body').animate({scrollTop: top}, 600);
	   
	   $("#"+sk_contain).html("<div class='sk_loading'><img src='"+sk_loading_img+"' /><span>Loading.......</span><div>");	
	   EnableOverlay(sk_overlay_bg);
	   
	  },
	  'success': function(res) {
	    $("#"+sk_contain).html(res);
	    SKajaxLink($)
	    DisableOverlay();

	    //if (history && history.pushState) 
	   	  //history.pushState("", document.title, sk_link+"?"+sk_data.replace("SKPlugin="+sk_plugin+"&",""));
	    
	  },
	  'failure':function(){
	  	DisableOverlay();
	  	alert("Please try again.");
	  }	  
	});	
}
//++++++++++++++++++++ Ajax Function end //
