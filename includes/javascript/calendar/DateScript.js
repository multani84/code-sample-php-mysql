//<![CDATA[
<!--
 
 
/*******************************************
Script used to display current date.
********************************************/
function addToArray(ar, str)
{
            var l
            l = ar[0] + 1
            ar[l] = str
            ar[0] = l
}
function makeArray()
{
            array = new Object();
            array[0] = 0;
            return array;
}
 
var dayNames = makeArray()
addToArray(dayNames, " Sun")
addToArray(dayNames, " Mon")
addToArray(dayNames, " Tue")
addToArray(dayNames, " Wed")
addToArray(dayNames, " Thu")
addToArray(dayNames, " Fri")
addToArray(dayNames, " Sat")
 
function ShowDay() {
	if(document.r)
	{ 
		var ryear = document.r.ResYear.selectedIndex;
		if(ryear < 0)
					ryear= 0;
		ryear= document.r.ResYear.options[ryear].value - 0;
		var rmonth = document.r.ResMonth.selectedIndex;
		var rdate = document.r.ResDate.selectedIndex + 1;
		var rnow = new Date(ryear,rmonth,rdate);
		var now = new Date();

		var rday = rnow.getDay() + 1;
		document.r.dayweek.value = dayNames[rday]
	}
}
 
 
 
function setDate() {
    var now = new Date()
    var closestTime = (now.getTime() + (30 * 60000)) //1/2 hour in advance...
    var nearest = new Date(closestTime)
    var nearyear = nearest.getYear()
    var nearmonth = nearest.getMonth()
    var neardate = nearest.getDate()

	if(document.r)
	{ 
		document.r.ResMonth.selectedIndex = nearmonth
		document.r.ResDate.selectedIndex = neardate - 1
		document.r.ResYear.selectedIndex = nearyear - 2000
	}
}
 
setDate();
ShowDay();
 
//-->
//]]>
