<?php
#########PagingFormat Defination Start ##########
	define("PAGING_FORMAT_NUMBERED","Numbered",true);
	define("PAGING_FORMAT_PREVNEXT","PrevNext",true);
	define("PAGING_FORMAT_BOTH","Both",true);
	define("PAGING_FORMAT_MULTIPLE","Multiple",true);	define("PAGING_FORMAT_LIST","List",true);
#########PagingFormat Defination End ##########

class Connection
{
	var $Recordset, $Query;
	var $PageSize=0, $AllowPaging=false, $PageNo, $TotalRecords=0,$TotalPages=0,$PageTotalDisplay=10;
	var $StrPrev = "Previous Page", $StrNext ="Next Page"; 
	var $DisplayQuery =false;
		
	function Connection()
	{
		global $sk_link_conn;	
	 $sk_link_conn = mysql_connect(DB_HOSTNAME,DB_USERNAME,DB_PASSWORD);
	if($sk_link_conn)
		{
			if(!mysql_select_db(DB_DATABASE))
			{
				$ParameterArray = array("Host" => DB_HOSTNAME, 
										"User" => DB_USERNAME, 
										"Password" => DB_PASSWORD,
										"DataBase" =>DB_DATABASE);
			
				$this->SendErrorMail(DEFINE_ERROR_EMAIL,mysql_error(),$ParameterArray);
				die(ERROR_MESSAGE);
			}
		}
		else 
		{
			$ParameterArray = array("Host" => DB_HOSTNAME, 
									"User" => DB_USERNAME, 
									"Password" => DB_PASSWORD,
									"DataBase" =>DB_DATABASE);
			
			$this->SendErrorMail(DEFINE_ERROR_EMAIL,mysql_error(),$ParameterArray);
			die(ERROR_MESSAGE);
		}
	}
	
	function ExecuteQuery($Query)
	{
		if($this->DisplayQuery ==true)
		{
			echo "<pre>";
			echo $Query;
			exit;
		}
		$this->Query = $Query;
		if ($this->Recordset = mysql_query($Query)) 
		{
			if($this->AllowPaging and $this->PageSize > 0 and $this->GetNumRows() > 0)
			{
				$this->TotalRecords = $this->GetNumRows();
				$this->TotalPages = intval($this->TotalRecords/$this->PageSize);
				
				$this->TotalPages = ($this->TotalRecords%$this->PageSize) > 0 ? $this->TotalPages+1:$this->TotalPages;
				$this->PageNo = (empty($this->PageNo) or $this->PageNo==0) ? 1:$this->PageNo;
				$this->Query .= " LIMIT ".($this->PageNo-1)*$this->PageSize.",".$this->PageSize;
				if ($this->Recordset = mysql_query($this->Query)) 
					return true;		
				else 
				{
					$ParameterArray = array("Query" => $Query,
											"FunctionName"=>"ExecuteQuery");
					$this->SendErrorMail(DEFINE_ERROR_EMAIL,mysql_error(),$ParameterArray);
					die(ERROR_MESSAGE);
				}
			}
			else
				return true;
		}
		else 
		{
			$ParameterArray = array("Query" => $Query,
									"FunctionName"=>"ExecuteQuery");
			$this->SendErrorMail(DEFINE_ERROR_EMAIL,mysql_error(),$ParameterArray);
			die("Please try again.");
		}
	}
	
	
	function GetPagingLinks($URL,$PagingFormat,$LinkClass="click",$PrefixText="")
	{
		$Text = "";
		$CurrentPageNo = $this->PageNo;
		
		$CurrentPageNo = (empty($CurrentPageNo) or $CurrentPageNo == 0) ? 1:$CurrentPageNo;
		if($this->AllowPaging and $this->PageSize > 0 and $this->GetNumRows() > 0 and $this->TotalPages > 1)
		{
			$Text .= $PrefixText;
			if($PagingFormat==PAGING_FORMAT_PREVNEXT)
			{
				for ($i=1; $i<=$this->TotalPages;$i++)
				{
					if($i == 1)
					{
						if($CurrentPageNo == 1)
							$Text .= "&laquo;&nbsp;Prev&nbsp;";
						else 
							$Text .= " <a href='$URL".($CurrentPageNo-1)."' class='$LinkClass'>&laquo;&nbsp;Prev</a>&nbsp;";
						$Text .= "<select name='ddlPager' onChange='document.location.href = value'>";
					}
					
					$Text .= "<option value='$URL$i'".($i==$CurrentPageNo ? " selected":"").">$i</option>";
					if($i == $this->TotalPages)
					{
						$Text .= "</select>";
						if($CurrentPageNo == $this->TotalPages)
							$Text .= "&nbsp;Next&nbsp;&raquo;";
						else 
							$Text .= "&nbsp;<a href='$URL".($CurrentPageNo+1)."' class='$LinkClass'>Next&nbsp;&raquo;</a>";
					}
					
				}
			}
			elseif($PagingFormat==PAGING_FORMAT_BOTH)
			{
				$PTotalDisplay = $this->PageTotalDisplay;
				$InitialLoop= ceil($CurrentPageNo/$PTotalDisplay)*$PTotalDisplay -($PTotalDisplay-1);
				$EndLoop = $this->TotalPages >=$InitialLoop+($PTotalDisplay-1)?$InitialLoop+($PTotalDisplay-1):$this->TotalPages;
				
				$PreviousPage = $InitialLoop - 1;
				$NextPage = $EndLoop + 1;
				$Text ="<table border='0' width='100%'><tr><td width='23%' valign='bottom' align='left' style='padding-top:10px;'>";
				if($PreviousPage ==0)
					$Text .="&laquo;".$this->StrPrev;
				else
					$Text .="<a href='$URL$PreviousPage' class='$LinkClass'>&laquo;".$this->StrPrev."</a>";
					
					$Text .="</td><td align='center'>";
				
				$Text .= "".$PrefixText;
				for ($i=$InitialLoop; $i<= $EndLoop;$i++)
				{
					if($i == $CurrentPageNo)
						$Text .= "<font color='gray'><b>$i</b></font>, ";
					else 
						$Text .= "<a href='$URL$i' class='$LinkClass'>$i</a>, ";
				}
				
				$Text = substr($Text, 0, strlen($Text)-2);
				
				$Text .="</td><td width='23%' align='right'>";
				if($this->TotalPages == $NextPage-1)
					$Text .=$this->StrNext."&raquo;";
				else 
					$Text .="<a href='$URL$NextPage' class='$LinkClass'>".$this->StrNext."&raquo;</a>";
					
					$Text .="</td></tr></table>";
				
				
			}			elseif($PagingFormat==PAGING_FORMAT_LIST)			{				$PTotalDisplay = $this->PageTotalDisplay;				$InitialLoop= ceil($CurrentPageNo/$PTotalDisplay)*$PTotalDisplay -($PTotalDisplay-1);				$EndLoop = $this->TotalPages >=$InitialLoop+($PTotalDisplay-1)?$InitialLoop+($PTotalDisplay-1):$this->TotalPages;				$PreviousPage = $InitialLoop - 1;				$NextPage = $EndLoop + 1;				$PreviousPageNo = $CurrentPageNo - 1;				$NextPageNo = $CurrentPageNo +1;				$Text ="<div class='pagination'><ul>";				$Text .= "<li class='active'><a href='#'>".$PrefixText."</a></li>";								if($PreviousPage ==0)						$Text .="<li class='disabled'><a href='#'>&laquo;&laquo;</a></li>";				else						$Text .="<li class='hidden-phone'><a href='$URL$PreviousPage' class='$LinkClass'><span>&laquo;&laquo;</span></a></li>";								if($CurrentPageNo > 1)						$Text .="<li class='hidden-phone'><a href='$URL$PreviousPageNo' class='$LinkClass'><span>&laquo;</span></a></li>";																							for ($i=$InitialLoop; $i<= $EndLoop;$i++)				{					if($i == $CurrentPageNo)						$Text .= "<li class='active hidden-phone'><a href='#'>$i</a></li>";					else 						$Text .= "<li class='hidden-phone'><a href='$URL$i' class='$LinkClass'>$i</a></li>";				}								$Text .="";								if($CurrentPageNo < $this->TotalPages) 					$Text .="<li class='hidden-phone'><a href='$URL$NextPageNo' class='$LinkClass'><span>&raquo;</span</a></li>";								if($this->TotalPages == $NextPage-1)					$Text .="<li class='disabled'><a href='#'><span>&raquo;&raquo;</span></a></li>";				else 					$Text .="<li class='hidden-phone'><a href='$URL$NextPage' class='$LinkClass'><span>&raquo;&raquo;</span</a></li>";																	$Text .="</ul></div>";											}
			elseif($PagingFormat==PAGING_FORMAT_MULTIPLE)			{				$PTotalDisplay = $this->PageTotalDisplay;				$InitialLoop= ceil($CurrentPageNo/$PTotalDisplay)*$PTotalDisplay -($PTotalDisplay-1);				$EndLoop = $this->TotalPages >=$InitialLoop+($PTotalDisplay-1)?$InitialLoop+($PTotalDisplay-1):$this->TotalPages;				$PreviousPage = $InitialLoop - 1;				$NextPage = $EndLoop + 1;				$PreviousPageNo = $CurrentPageNo - 1;				$NextPageNo = $CurrentPageNo +1;				$Text ="<table border='0' width='100%'><tr><td width='23%' valign='bottom' align='left' style='padding-top:10px;'>";				if($PreviousPage ==0)						$Text .="<span style='cursor:hand;border:solid 1px gray;padding-left:3px;padding-right:3px;padding-top:3px;padding-bottom:3px;'>&laquo;&laquo;</span>";				else						$Text .="<a href='$URL$PreviousPage' class='$LinkClass'><span style='cursor:hand;border:solid 1px gray;padding-left:3px;padding-right:3px;padding-top:3px;padding-bottom:3px;'>&laquo;&laquo;</span></a>";								if($CurrentPageNo > 1)						$Text .="&nbsp;&nbsp;&nbsp;&nbsp;<a href='$URL$PreviousPageNo' class='$LinkClass'><span style='cursor:hand;border:solid 1px gray;padding-left:3px;padding-right:3px;padding-top:3px;padding-bottom:3px;'>&laquo;</span></a>";																$Text .="</td><td align='center'>";								$Text .= "".$PrefixText;				for ($i=$InitialLoop; $i<= $EndLoop;$i++)				{					if($i == $CurrentPageNo)						$Text .= "<font color='gray'><b>$i</b></font>, ";					else 						$Text .= "<a href='$URL$i' class='$LinkClass'>$i</a>, ";				}								$Text = substr($Text, 0, strlen($Text)-2);								$Text .="</td><td width='23%' align='right'>";								if($CurrentPageNo < $this->TotalPages) 					$Text .="<a href='$URL$NextPageNo' class='$LinkClass'><span style='cursor:hand;border:solid 1px gray;padding-left:3px;padding-right:3px;padding-top:3px;padding-bottom:3px;'>&raquo;</span</a>&nbsp;&nbsp;&nbsp;&nbsp;";								if($this->TotalPages == $NextPage-1)					$Text .="<span style='cursor:hand;border:solid 1px gray;padding-left:3px;padding-right:3px;padding-top:3px;padding-bottom:3px;'>&raquo;&raquo;</span";				else 					$Text .="<a href='$URL$NextPage' class='$LinkClass'><span style='cursor:hand;border:solid 1px gray;padding-left:3px;padding-right:3px;padding-top:3px;padding-bottom:3px;'>&raquo;&raquo;</span</a>";										$Text .="</td></tr></table>";											}						else 			{				for ($i=1; $i<= $this->TotalPages;$i++)				{					if($i == $CurrentPageNo)						$Text .= "<span><b>$i</b></span> ";					else 						$Text .= "<a href='$URL$i' class='$LinkClass'>$i</a> ";				}				$Text = substr($Text, 0, strlen($Text)-2);			}		}		return $Text;	}			function GetObjectFromRecord()	{		if($this->Recordset)
		{
			if(mysql_num_rows($this->Recordset)>0)
			{
				return mysql_fetch_object($this->Recordset);
			}
			return false;
		}
		else 
		{
			$ParameterArray = array("Query" => $this->Query, 
									"FunctionName"=>"GetObjectFromRecord");
			$this->SendErrorMail(DEFINE_ERROR_EMAIL,mysql_error(),$ParameterArray);
			die(ERROR_MESSAGE);
		}
	}
	
	function GetArrayFromRecord()
	{
		if ($this->Recordset) 
		{
			if (mysql_num_rows($this->Recordset)>0) 
			{
				return mysql_fetch_array($this->Recordset);	
			}
			return false;
			
		}
		else 
		{
			$ParameterArray = array("Query" => $this->Query, 
									"FunctionName"=>"GetObjectFromRecord");
			$this->SendErrorMail(DEFINE_ERROR_EMAIL,mysql_error(),$ParameterArray);
			die(ERROR_MESSAGE);
		}
	}
	
	function GetNumRows()
	{
		if($this->Recordset)
		{
			return mysql_num_rows($this->Recordset);
		}
		else 
		{
			$ParameterArray = array("Query" => $this->Query, 
									"FunctionName"=>"GetNumRows");
			$this->SendErrorMail(DEFINE_ERROR_EMAIL,mysql_error(),$ParameterArray);
			die(ERROR_MESSAGE);
		}
	}
	
	function GetInsertID()
	{
		return mysql_insert_id();
	}
	
	
	function MoveTo($RowNo=0)
	{
		if($this->Recordset)
		{
			return mysql_data_seek($this->Recordset,$RowNo);
		}
		else 
		{
			$this->SendErrorMail(DEFINE_ERROR_EMAIL,mysql_error(),"Move row pointer to : ".$RowNo);
			die(ERROR_MESSAGE);
		}
	}
	
	function GetNumFields()
	{
		if($this->Recordset)
		{
			return mysql_num_fields($this->Recordset);
		}
		else 
		{
			$ParameterArray = array("Query" => $this->Query, 
									"FunctionName"=>"GetNumFields");
			$this->SendErrorMail(DEFINE_ERROR_EMAIL,mysql_error(),$ParameterArray);
			die(ERROR_MESSAGE);
		}
	}
	function GetFieldName($i)
	{
		if($this->Recordset)
		{
			return mysql_field_name($this->Recordset,$i);
		}
		else 
		{
			$ParameterArray = array("Query" => $this->Query, 
									"FunctionName"=>"GetFieldName");
			$this->SendErrorMail(DEFINE_ERROR_EMAIL,mysql_error(),$ParameterArray);
			die(ERROR_MESSAGE);
		}
	}
	
	function MysqlEscapeString($Str)
	{
		
		$Str  = str_replace ( array("'"), array('&apos;'), $Str);
		if (!get_magic_quotes_gpc()) 
			$Str = mysql_escape_string($Str);
		
		$Str = str_replace(DIR_WS_SITE,"<?php echo DIR_WS_SITE?>",$Str);
		return trim($Str);
	}
	
	
	function SendErrorMail($EmailAddress,$MySQLError,$SupportParms)
	{
		echo "<font color='Maroon'>$MySQLError</font><br>";
		$Body = $MySQLError."<br>";
		foreach ($SupportParms as $key => $value) 
			$Body .= $key."===========".$value."<br>"; 	
		
		$Body .= "<br>Server Informatin<br>";
		foreach ($_SERVER as $key => $value) 
			$Body .= $key."===========".$value."<br>"; 	
			
		if(!SendEmail($MySQLError,DEFINE_ERROR_EMAIL,DEFINE_ADMIN_EMAIL,DEFINE_SITE_NAME,$Body))
			echo $Body."<br><br>".$MySQLError;
	}
};
