<?php echo '<?xml version="1.0" encoding="iso-8859-1"?>';
if(isset($ModuleMeta) && $ModuleMeta=="1")
{
	$MetaDescription = isset($ModuleMetaDescription)?$ModuleMetaDescription:$MetaDescription;
	$MetaKeyword = isset($ModuleMetaKeyword)?$ModuleMetaKeyword:$MetaKeyword;
	$MetaTitle = isset($ModuleMetaTitle)?$ModuleMetaTitle:$MetaTitle;
}
?>
<!doctype html>
<html lang="en">
<head>
    <title><?php echo $MetaTitle?></title>
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta name="viewport" content="width=device-width">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="Description" content="<?php echo $MetaDescription;?>" />
	<meta name="Keywords" content="<?php echo $MetaKeyword?>" />
    <meta name="format-detection" content="telephone=no">
    <script type="text/javascript" src="<?php echo DIR_WS_SITE_INCLUDES_JAVASCRIPT?>jquery/jquery-1.11.0.min.js"></script>
    <script src="<?php echo DIR_WS_SITE_INCLUDES_JAVASCRIPT?>jquery/validation/jquery.validate.min.js" type="text/javascript"></script>	
    <script src="<?php echo DIR_WS_SITE_INCLUDES_JAVASCRIPT?>header.js" type="text/javascript"></script>	
	<script type="text/javascript" src="<?php echo DIR_WS_SITE_INCLUDES_JAVASCRIPT?>NumberFormat.js"></script>
	
	<link href="<?php echo DIR_WS_SITE_INCLUDES_JAVASCRIPT?>/jquery/lity/lity.css" rel="stylesheet" />
	<script src="<?php echo DIR_WS_SITE_INCLUDES_JAVASCRIPT?>/jquery/lity/lity.min.js" type="text/javascript"></script>
	
<?php 
	if(file_exists(DIR_FS_SITE_TEMPLATES.DEFINE_TEMPLATE_STYLE."/template_meta.php"))
		require_once(DIR_FS_SITE_TEMPLATES.DEFINE_TEMPLATE_STYLE."/template_meta.php");

if(isset($MetaStringContent) && $MetaStringContent != "")
	echo $MetaStringContent;
?>
</head>