<?php
if(DEFINE_EMAIL_FORMAT=="html")
{?>
Dear <?php echo $arr['FirstName']?> <?php echo $arr['LastName']?>,<br>
<br>
To reset your account password , please go to: <a href='<?php echo MakePageURL("index.php","Page=reset_password&code=".$arr['ForgetCode'])?>'>here.</a>
<br><br>
If link is not working then copy and paste below code in the browser.<br>
<?php echo MakePageURL("index.php","Page=reset_password&code=".$arr['ForgetCode'])?>
<br><br>

<br>
For help with any of our online services, please email <a href='mailto:<?php echo DEFINE_ADMIN_EMAIL?>'><?php echo DEFINE_ADMIN_EMAIL?></a>.<br>
<br>
Thank you !<br>
<a href='<?php echo DIR_WS_SITE?>'><?php echo DEFINE_SITE_NAME?></a>
<?php
}
else 
{
	?>
Dear <?php echo $arr['FirstName']?> <?php echo $arr['LastName']?>,<?php echo chr(13)?>
<?php echo chr(13)?>
To reset your account password , please go to: <a href='<?php echo MakePageURL("index.php","Page=reset_password&code=".$arr['ForgetCode'])?>'>here.</a>
<?php echo chr(13)?><?php echo chr(13)?>
If link is not working then copy and paste below code in the browser.<?php echo chr(13)?>
<?php echo chr(13)?>
For help with any of our online services, please email <?php echo DEFINE_ADMIN_EMAIL?>.<?php echo chr(13)?>
<?php echo chr(13)?>
Thank you !<?php echo chr(13)?>
<?php echo DEFINE_SITE_NAME?>
	<?php
}?>