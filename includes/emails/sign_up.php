<?php
if(DEFINE_EMAIL_FORMAT=="html")
{?>
Dear <?php echo $arr['FirstName']?> <?php echo $arr['LastName']?>,<br>
<br>
<br>
Thank you for creating your account with us. Please login with the following information.
<br><br>
Username: <?php echo $arr['UserName']?>
<br>
Password: <?php echo $arr['Password']?>
<br>
<br>
To access your login page, please goto: <a href='<?php echo MakePageURL("index.php","Page=login")?>'><?php echo DEFINE_SITE_NAME?></a>
<br>
<br>
Thank you
<br>
<?php echo DEFINE_SITE_NAME?>
<?php
}
else 
{
?>
Dear <?php echo $arr['FirstName']?> <?php echo $arr['LastName']?>,<?php echo chr(13)?>
<?php echo chr(13)?>
<?php echo chr(13)?>
Thank you for creating your account with us. Please login with the following information.
<?php echo chr(13)?><?php echo chr(13)?>
Username: <?php echo $arr['UserName']?>
<?php echo chr(13)?>
Password: <?php echo $arr['Password']?>
<?php echo chr(13)?>
<?php echo chr(13)?>
To access your login page, please goto: <a href='<?php echo MakePageURL("index.php","Page=login")?>'><?php echo DEFINE_SITE_NAME?></a>
<?php echo chr(13)?>
<?php echo chr(13)?>
Thank you
<?php echo chr(13)?>
<?php echo SITE_NAME?>
<?php
}?>