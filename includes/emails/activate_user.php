<?php
if(DEFINE_EMAIL_FORMAT=="html")
{?>
Dear <?php echo $arr['FirstName']?> <?php echo $arr['LastName']?><br>
<br>
<br>
Thank you for creating your account with us. 
<br>
To activate your account, please go to: <a href='<?php echo MakePageURL("index.php","Page=activate_user&code=".$arr['ActivationCode']."&neo_sid=".$arr['neo_sid'])?>'>here.</a>
<br><br>
If link is not working then copy and paste below code in the browser.<br>
<?php echo MakePageURL("index.php","Page=activate_user&code=".$arr['ActivationCode']."&neo_sid=".$arr['neo_sid'])?>
<br><br>
Thank you
<br>
<?php echo DEFINE_SITE_NAME?>
<br><br>
If you received this email by mistake, simply delete it. You won't be registered if you don't click the confirmation link above.<br>
For questions about this email, please contact: <a href="mailto:<?php echo DEFINE_ADMIN_EMAIL?>"><?php echo DEFINE_ADMIN_EMAIL?></a>
<?php
}
else 
{
?>
Dear <?php echo $arr['FirstName']?> <?php echo $arr['LastName']?><?php echo chr(13)?>
<?php echo chr(13)?>
<?php echo chr(13)?>
Thank you for creating your account with us. 
<?php echo chr(13)?>
To activate your account, please go to: <?php echo MakePageURL("index.php","Page=activate_user&code=".$arr['ActivationCode']."&neo_sid=".$arr['neo_sid'])?>
<?php echo chr(13)?><?php echo chr(13)?>
If link is not working then copy and paste below code in the browser.<?php echo chr(13)?>
<?php echo MakePageURL("index.php","Page=activate_user&code=".$arr['ActivationCode']."&neo_sid=".$arr['neo_sid'])?>
<?php echo chr(13)?><?php echo chr(13)?>
Thank you
<?php echo chr(13)?>
<?php echo DEFINE_SITE_NAME?>
<?php echo chr(13)?><?php echo chr(13)?>
If you received this email by mistake, simply delete it. You won't be registered if you don't click the confirmation link above.<?php echo chr(13)?>
For questions about this email, please contact: <?php echo DEFINE_ADMIN_EMAIL?>
<?php
}?>
