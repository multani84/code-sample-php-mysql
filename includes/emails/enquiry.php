<?php

if(defined("DEFINE_EMAIL_FORMAT") && DEFINE_EMAIL_FORMAT=="html")

{?>

	<BODY bgColor=#ffffff>

		<div align="center">

		<STYLE type=text/css>

			.MessageBody

			{

				FONT-SIZE: 11px; COLOR: #000000; LINE-HEIGHT: 120%; FONT-STYLE: normal; FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif

			}

			.MessageBodyTitle

			{

				FONT-SIZE: 13px; COLOR: #000000; LINE-HEIGHT: 120%; FONT-STYLE: bold; FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif

			}

		</STYLE>

			<table cellpadding="6" cellspacing="1" width="70%" border="0" style="BORDER-RIGHT: #acacac 1px solid; BORDER-TOP: #acacac 1px solid; BORDER-LEFT: #acacac 1px solid; BORDER-BOTTOM: #acacac 1px solid">

				<tr>

					<td class="MessageBodyTitle" align="left" colspan="2"><b><?php echo (isset($arr['Mail_Header'])?$arr['Mail_Header']:"");?></b></td>

				</tr>

					<?php

				foreach ($arr as $Key=>$Value)

				{

					if (preg_match("/MailTitle/i", $Key))

					{?>

					<tr bgcolor="#cccccc" height="25">

						<td class="MessageBodyTitle" align="left" colspan="2"><b><?php echo nl2br($Value)?></b></td>

					</tr>

					<?php

					}

					else 

					{

						if(preg_match("/@@@/i", $Key))

							$Key = substr($Key,strpos($Key,"@@@")+3);

						if(!preg_match("/Mail_/i", $Key) && !preg_match("/neo_/i", $Key))

						{								

						?>

						<tr bgcolor="#ececec" height="25">

							<td class="MessageBody" align="right" width="30%"><b><?php echo $Key?></b></td>

							<td class="MessageBody" align="left"><?php echo nl2br($Value)?></td>

						</tr>

						<?php

						}

					}

				}?>

				<tr>

					<td class="MessageBodyTitle" align="left" colspan="2"><b><?php echo (isset($arr['Mail_Footer'])?$arr['Mail_Footer']:"");?></b></td>

				</tr>

			</table>

			<br>

		</div>

	<?php

}

else 

{

		$BrkPoint = chr(13);

				

		echo (isset($arr['Mail_Header'])?str_replace("<br>",$BrkPoint,$arr['Mail_Header']).$BrkPoint.$BrkPoint:"");

		foreach ($arr as $Key=>$Value)

		{

			if (preg_match("/MailTitle/i", $Key))

				echo $Value."".$BrkPoint;

			else 

			{

				if(preg_match("/@@@/i", $Key))

					$Key = substr($Key,strpos($Key,"@@@")+3);

				if(!preg_match("/Mail_/i", $Key) && !preg_match("/neo_/i", $Key))

				{								

					echo $Key." : ".$Value.$BrkPoint;

				}

			}

		}

		echo (isset($arr['Mail_Footer'])?$BrkPoint.$BrkPoint.str_replace("<br>",$BrkPoint,$arr['Mail_Footer']).$BrkPoint.$BrkPoint:"");

				

}?>