<?php
if(DEFINE_EMAIL_FORMAT=="html")
{?>
<style type="text/css">
</style>
<table width="600"  border="0" align="center" cellpadding="3" cellspacing="2" bgcolor="#efefef">
  <tr>
    <td width="600" align="center" valign="top"><br />
     <table width="85%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td align="center"><h1>Gift Certificate </h1>
        	<hr size="1" color="#CCCCCC" />
        </td>
      </tr>
      <tr>
        <td align="center">
        <table width="100%" border="0" cellspacing="3" cellpadding="2" >
          <tr bgcolor="#dcdcdc">
            <td width="47%" align="center" valign="middle" ><strong>For</strong></td>
            <td width="53%" align="left" valign="middle" ><strong>:<?php echo $arr['ToName']?></strong></td>
          </tr>
          <tr bgcolor="#dcdcdc">
            <td align="center" valign="middle" ><strong>Value</strong></td>
            <td align="left" valign="middle" ><strong>: <?php echo $arr['Amount']?></strong></td>
          </tr>
          <tr bgcolor="#dcdcdc">
            <td align="center" valign="middle" ><strong>From</strong></td>
            <td align="left" valign="middle"><strong> : <?php echo $arr['From']?></strong></td>
          </tr>
          <tr bgcolor="#dcdcdc">
            <td align="center" valign="middle" ><strong> Gift Code/Coupon code</strong></td>
            <td align="left" valign="middle" ><strong>: <?php echo $arr['CouponCode']?></strong></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td align="center"><h2>Congratulations, you have recieved a gift certificate for</h2>
          <u><h1><strong><?php echo DEFINE_SITE_NAME?></strong></h1>
          </u></td>
      </tr>
      <tr>
        <td align="left"><p>
        <b>You can use the above coupon code on your next purchase (note: one coupon code per transaction).</b>
          <br />
             <br />
            <?php echo nl2br($arr['Message'])?>
			<br>
         </p></td>
      </tr>
    </table></td>
  </tr>
</table>
<?php
}
else 
{
	?>
	Dear <?php echo $arr['ToName']?>,<?php echo chr(13)?>
	<?php echo chr(13)?>
	<?php echo chr(13)?>
	For		: <?php echo $arr['ToName']?><?php echo chr(13)?>
	Value 	: <?php echo $arr['Amount']?><?php echo chr(13)?>
	From 	: <?php echo $arr['From']?><?php echo chr(13)?>
	Gift No.: <?php echo $arr['CouponCode']?><?php echo chr(13)?>
	<?php echo chr(13)?>
	Congratulations, you have recieved a gift certificate for <?php echo DEFINE_SITE_NAME?><?php echo chr(13)?>
	<?php echo chr(13)?><?php echo chr(13)?>
	<?php echo chr(13)?>
	<?php echo chr(13)?>
<?php echo chr(13)?>
<?php echo $arr['Message']?>
<?php echo chr(13)?>
Happy Shopping and enjoy your purchases.
	<?php echo chr(13)?>
	<?php echo chr(13)?>
	Thank you
	<?php echo chr(13)?>
	<?php echo DEFINE_SITE_NAME?>
	<?php
}
?>