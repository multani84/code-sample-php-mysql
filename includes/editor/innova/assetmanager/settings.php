<?php
require_once("../../../../includes/configure.php");
if(!isset($_SESSION['AdminObj']))
{
	@ob_clean();
	
	$_SESSION['ErrorMessage'] ="Please login to access the website control panel.";
	header("Location:".DIR_WS_SITE_CONTROL."index.php?Page=login");
	exit;
}


define("CMS_THUMBNAIL_SIZE","100",true);
define("CMS_MEDIUM_SIZE","300",true);

global $CMSImageTypeArray;
$CMSImageTypeArray = array("Jpg","Jpeg","Gif","Bmp","Png");


$bReturnAbsolute=false;

$sBaseVirtual0=DIR_WS_SITE_UPLOADS_CMS."_images";  //Assuming that the path is http://yourserver/Editor/assets/ ("Relative to Root" Path is required)
//$sBase0="c:/inetpub/wwwroot/Editor/assets"; //The real path
$sBase0=DIR_FS_SITE_UPLOADS_CMS."_images"; //The real path
//$sBase0="/home/yourserver/web/Editor/assets"; //example for Unix server
$sName0="Images";

$sBaseVirtual1=DIR_WS_SITE_UPLOADS_CMS."_others";  //Assuming that the path is http://yourserver/Editor/assets/ ("Relative to Root" Path is required)
//$sBase0="c:/inetpub/wwwroot/Editor/assets"; //The real path
$sBase1=DIR_FS_SITE_UPLOADS_CMS."_others"; //The real path
//$sBase0="/home/yourserver/web/Editor/assets"; //example for Unix server
$sName1="Others";

$sBaseVirtual2="";  //Assuming that the path is http://yourserver/Editor/assets/ ("Relative to Root" Path is required)
//$sBase0="c:/inetpub/wwwroot/Editor/assets"; //The real path
$sBase2=""; //The real path
//$sBase0="/home/yourserver/web/Editor/assets"; //example for Unix server
$sName2="";

$sBaseVirtual3="";  //Assuming that the path is http://yourserver/Editor/assets/ ("Relative to Root" Path is required)
//$sBase0="c:/inetpub/wwwroot/Editor/assets"; //The real path
$sBase3=""; //The real path
//$sBase0="/home/yourserver/web/Editor/assets"; //example for Unix server
$sName3="";

?>
