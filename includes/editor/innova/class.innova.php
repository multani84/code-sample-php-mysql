<?php
class innova extends SKEditor 
{
	var $Directory = "innova";
	var $CArray = array();
	
	function innova()
	{
		
	}
	
	function HeadScript()
	{
		$RT ="";
		$RT .='<script type="text/javascript" src="'.DIR_WS_SITE_INCLUDES_EDITOR.$this->Directory.'/scripts/language/en-US/editor_lang.js"></script>';
		$RT .='<script type="text/javascript" src="'.DIR_WS_SITE_INCLUDES_EDITOR.$this->Directory.'/scripts/innovaeditor.js"></script>';
		//var_dump($RT);exit;
		return $RT;
			
	}
	function FooterScript()
	{
		
	}
	function CustomScript()
	{
		
	}
	function CreateEditor($Id,$Content="")
	{
		$Help = isset($this->CArray['Help'])?$this->CArray['Help']:false;
		if($Help == true)
		{
			?>
			<div align="right"><a href="javascript:;" onclick='window.open("<?php echo DIR_WS_SITE_INCLUDES_EDITOR.$this->Directory?>/help/index.htm","my_new_window","toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=no, copyhistory=no, width=750, height=600, top=20, left=20");return false;' title="CLICK HERE for editor help">[HELP]</a></div>
			<?php
		}
		?>
		<textarea id="<?php echo $Id?>" name="<?php echo $Id?>" rows=4 cols=30><?php echo $this->encodeHTML($Content)?></textarea>											
		<script>
			var oEdit<?php echo $Id?> = new InnovaEditor("oEdit<?php echo $Id?>");
			oEdit<?php echo $Id?>.useTab = false;
		<?php
		$Mode = isset($this->CArray['DMode'])?$this->CArray['DMode']:"";
		switch ($Mode)
		{
			case "Complete":
				 ?>
				 oEdit<?php echo $Id?>.features=["FullScreen","Preview","Search","Table","Guidelines","Absolute","Bold","Italic","Underline","Strikethrough", "Superscript","Subscript",
				 					"Form","Characters","ClearAll",
				 					"BRK",
								    "Cut","Copy","Paste","PasteWord","PasteText",
								    "Undo","Redo","Hyperlink","InternalHyperlink","Bookmark","Image",
								    "JustifyLeft","JustifyCenter","JustifyRight","JustifyFull",
								    "Numbering","Bullets","Indent","Outdent", 
								    "BRK",
								    "StyleAndFormatting","TextFormatting","ListFormatting",
								    "BoxFormatting","ParagraphFormatting","CssText","Styles",
								   	"Paragraph","FontName","FontSize",
								    "ForeColor","BackColor","LTR","RTL",
								    "BRK", "Flash","Media","InternalLink","CustomObject","Line","RemoveFormat","XHTMLFullSource","XHTMLSource",								    
								    ];// => Custom Button Placement
			<?php
			break;	
			case "Large4":
				 ?>
				 oEdit<?php echo $Id?>.features=["FullScreen","Preview","Cut","Copy","Paste","PasteWord","PasteText","|","Undo","Redo","|",
										"ForeColor","BackColor","BRK","Bookmark","Hyperlink","InternalHyperlink",
										"Image","|","Table","Guidelines","Absolute","|","Characters","Line",
										"Form","RemoveFormat","XHTMLSource","ClearAll","BRK", 
										"StyleAndFormatting","Styles","|","Paragraph","FontName","FontSize","|",
										"Bold","Italic","Underline","BRK",
										"JustifyLeft","JustifyCenter","JustifyRight","JustifyFull","|",
										"Numbering","Bullets","|","Indent","Outdent","LTR","RTL"];// => Custom Button Placement
			<?php
			break;	
			case "Large2":
			case "Large":
				default:
				 ?>
				 oEdit<?php echo $Id?>.features=["FullScreen","Preview","Cut","Copy","Paste","PasteWord","PasteText","|","Undo","Redo","|",
										"ForeColor","BackColor","Bookmark","Hyperlink","InternalHyperlink",
										"Image","|","Table","Guidelines","Absolute","|","Characters","Line",
										"Form","RemoveFormat","XHTMLSource","ClearAll","BRK", 
										"StyleAndFormatting","Styles","|","Paragraph","FontName","FontSize","|",
										"Bold","Italic","Underline",
										"JustifyLeft","JustifyCenter","JustifyRight","JustifyFull","|",
										"Numbering","Bullets","|","Indent","Outdent","LTR","RTL"];// => Custom Button Placement
			<?php
			break;	
			case "Small":
				 ?>
				 oEdit<?php echo $Id?>.features=["FullScreen","Preview","Cut","Copy","Paste","PasteWord","PasteText","|","Undo","Redo","|",
										"ForeColor","BackColor","BRK","Bookmark","Hyperlink","InternalHyperlink",
										"Image","|","Table","Guidelines","Absolute","|","Characters","Line",
										"Form","RemoveFormat","XHTMLSource","ClearAll","BRK", 
										"StyleAndFormatting","Styles","|","Paragraph","FontName","FontSize","|",
										"Bold","Italic","Underline","BRK",
										"JustifyLeft","JustifyCenter","JustifyRight","JustifyFull","|",
										"Numbering","Bullets","|","Indent","Outdent","LTR","RTL"];// => Custom Button Placement
			<?php
			break;	
			case "tiny":
				 ?>
				 oEdit<?php echo $Id?>.features=["ForeColor","BackColor","Image","Hyperlink","|","Paragraph","FontName","FontSize","|","Bold","Italic","Underline","BRK",
				                         "Undo","Redo","|","Cut","Copy","Paste","PasteWord","PasteText","|","Table","Guidelines","|","Characters","Line","|","JustifyLeft","JustifyCenter","JustifyRight","JustifyFull","|","Numbering","Bullets","XHTMLSource"
				        				 ];// => Custom Button Placement
			<?php
			break;	
			case "tiny2":
				 ?>
				 oEdit<?php echo $Id?>.features=["ForeColor","BackColor","|","Paragraph","FontName","FontSize","BRK","Bold","Italic","Underline","Hyperlink","Image"];// => Custom Button Placement
			<?php
			break;	
		}
		$EditorArray = isset($this->CArray['EditorArray'])?$this->CArray['EditorArray']:array();
		if(isset($EditorArray) && is_array($EditorArray) && count($EditorArray) >0)
		{
			foreach ($EditorArray as $k=>$v)
			{
				echo 'oEdit'.$Id.'.'.$k.'= "'.$v.'";';
			}
		}
		?>
			oEdit<?php echo $Id?>.width = <?php echo isset($this->CArray['Width'])?$this->CArray['Width']:""?>;
			oEdit<?php echo $Id?>.height = <?php echo isset($this->CArray['Height'])?$this->CArray['Height']:""?>;
			oEdit<?php echo $Id?>.cmdAssetManager="modalDialogShow('<?php echo DIR_WS_SITE_INCLUDES_EDITOR.$this->Directory?>/assetmanager/assetmanager.php',740,565)";//Use "relative to root" path
			oEdit<?php echo $Id?>.btnStyles=true;
			oEdit<?php echo $Id?>.REPLACE("<?php echo $Id?>");
		</script>
		<?php
	}
	
	function encodeHTML($sHTML)
    {
	    $sHTML=ereg_replace("&","&amp;",$sHTML);
	    $sHTML=ereg_replace("<","&lt;",$sHTML);
	    $sHTML=ereg_replace(">","&gt;",$sHTML);
	    return $sHTML;
    }
};
?>