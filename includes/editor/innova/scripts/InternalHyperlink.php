<?php
require_once("../../../../includes/configure.php");
set_time_limit(120);
$LanguageID = isset($_GET['Language'])?$_GET['Language']:"1";?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="style/editor.css" rel="stylesheet" type="text/css">
<script>
	var sLangDir=dialogArguments.oUtil.langDir;
	document.write("<scr"+"ipt src='language/"+sLangDir+"/InternalHyperlink.js'></scr"+"ipt>");
</script>
<script>writeTitle()</script>
<script>
function GetElement(oElement,sMatchTag)
	{
	while (oElement!=null&&oElement.tagName!=sMatchTag)
		{
		if(oElement.tagName=="BODY")return null;
		oElement=oElement.parentElement;
		}
	return oElement;
	}
	
function doWindowFocus()
	{
	dialogArguments.oUtil.onSelectionChanged=new Function("realTime()");
	}
function bodyOnLoad()
	{
	window.onfocus=doWindowFocus;	
	dialogArguments.oUtil.onSelectionChanged=new Function("realTime()");

	realTime()
	}
function openAsset()
	{
	if(dialogArguments.oUtil.obj.cmdAssetManager!="")
		inpURL.value=eval(dialogArguments.oUtil.obj.cmdAssetManager);
	if(dialogArguments.oUtil.obj.cmdFileManager!="")
		inpURL.value=eval(dialogArguments.oUtil.obj.cmdFileManager);	
	}
function modalDialogShow(url,width,height)
	{
	return window.showModalDialog(url,window,
		"dialogWidth:"+width+"px;dialogHeight:"+height+"px;edge:Raised;center:Yes;help:No;Resizable:Yes;Maximize:Yes");
	}
function updateList()
	{
	
	}
function realTime()
	{
	if(!dialogArguments.oUtil.obj.checkFocus()){return;}//Focus stuff
	var oEditor=dialogArguments.oUtil.oEditor;
	var oSel=oEditor.document.selection.createRange();
	var sType=oEditor.document.selection.type;
	
	updateList();

	//If text or control is selected, Get A element if any
	if (oSel.parentElement)	oEl=GetElement(oSel.parentElement(),"A");
	else oEl=GetElement(oSel.item(0),"A");

	//Is there an A element ?
	if (oEl)
		{
		btnInsert.style.display="none";
		btnApply.style.display="block";
		btnOk.style.display="block";
		

		//~~~~~~~~~~~~~~~~~~~~~~~~
		sTmp=oEl.outerHTML;
		if(sTmp.indexOf("href")!=-1) //1.5.1
			{
			sTmp=sTmp.substring(sTmp.indexOf("href")+6);
			sTmp=sTmp.substring(0,sTmp.indexOf('"'));
			var arrTmp = sTmp.split("&amp;");
			if (arrTmp.length > 1) sTmp = arrTmp.join("&");		
			sURL=sTmp
			//sURL=oEl.href;
			}
		else
			{
			sURL=""
			}

		if(sType!="Control")
			{
			try
				{			
				var oSelRange = oEditor.document.body.createTextRange()
				oSelRange.moveToElementText(oEl)
				oSel.setEndPoint("StartToStart",oSelRange);
				oSel.setEndPoint("EndToEnd",oSelRange);
				oSel.select();
				}
			catch(e){return;}
			}
		
		inpTarget.value="";
		inpTargetCustom.value="";
		if(oEl.target=="_self" || oEl.target=="_blank" || oEl.target=="_parent")
			inpTarget.value=oEl.target;//inpTarget
		else
			inpTargetCustom.value=oEl.target;
		
		inpTitle.value="";
		if(oEl.title!=null) inpTitle.value=oEl.title;//inpTitle //1.5.1
		inpURL.value=sURL;
		inpURL.disabled=false;
		
		}
	else
		{
		btnInsert.style.display="block";
		btnApply.style.display="none";
		btnOk.style.display="none";

		inpTarget.value="";
		inpTargetCustom.value="";
		inpTitle.value="";
		
		inpURL.value="";
		
		inpURL.disabled=false;
		}			
	}

function applyHyperlink()
	{
	if(!dialogArguments.oUtil.obj.checkFocus()){return;}//Focus stuff
	var oEditor=dialogArguments.oUtil.oEditor;
	var oSel=oEditor.document.selection.createRange();
	
	dialogArguments.oUtil.obj.saveForUndo();
	
	var sURL;
		sURL= inpURL.value;
	
	if((inpURL.value!=""))
		{
		if (oSel.parentElement)
			{
			if(btnInsert.style.display=="block")
				{
				if(oSel.text=="")//If no (text) selection, then build selection using the typed URL
					{
					var oSelTmp=oSel.duplicate();
					oSel.text=sURL;
					oSel.setEndPoint("StartToStart",oSelTmp);
					oSel.select();
					}
				}
			}
		
		oSel.execCommand("CreateLink",false,sURL);

		//get A element
		if (oSel.parentElement)	oEl=GetElement(oSel.parentElement(),"A");
		else oEl=GetElement(oSel.item(0),"A");
		if(oEl)
			{
			if(inpTarget.value=="" && inpTargetCustom.value=="") oEl.removeAttribute("target",0);//target
			else 
				{
				if(inpTargetCustom.value!="")
					oEl.target=inpTargetCustom.value;
				else
					oEl.target=inpTarget.value;
				}
			
			if(inpTitle.value=="") oEl.removeAttribute("title",0);//1.5.1
			else oEl.title=inpTitle.value;
			}
			
		dialogArguments.realTime(dialogArguments.oUtil.oName);
		dialogArguments.oUtil.obj.selectElement(0);
		}
	else
		{
		oSel.execCommand("unlink");//unlink
		
		dialogArguments.realTime(dialogArguments.oUtil.oName);
		dialogArguments.oUtil.activeElement=null;
		}	
	realTime();
	}
		
</script>
</head>
<body onload="loadTxt();bodyOnLoad()" style="overflow:hidden;">

<table width=100% height=100% align=center cellpadding=0 cellspacing=0>
<tr>
<td valign=top style="padding:5;height:100%">
	<table width=100%>
	<tr>
		<td nowrap>
			<input type="radio" value="url" name="rdoLinkTo" checked class="inpRdo">
			<span id="txtLang" name="txtLang">Source</span>:
		</td>
		<td width="100%">
			<table cellpadding="0" cellspacing="0" width="100%">
			<tr>
			<td width="100%">
			<select ID="inpURL" NAME="inpURL" style="width:100%" class="inpTxt">
			<?php include_once("../../../../internal_hyperlink.php");
			  //echo file_get_contents(DIR_WS_SITE."/internal_hyperlink.php?Language=".$LanguageID);?>								
			</select>
			</td>
			</tr>
			</table>		
		</td>
	</tr>
	<tr>
		<td nowrap>&nbsp;<span id="txtLang" name="txtLang">Target</span>:</td>
		<td><INPUT type="text" ID="inpTargetCustom" NAME="inpTargetCustom" size=15 class="inpTxt">
		<select ID="inpTarget" NAME="inpTarget" class="inpSel">
			<option value=""></option>
			<option value="_self" id="optLang" name="optLang">Self</option>
			<option value="_blank" id="optLang" name="optLang">Blank</option>
			<option value="_parent" id="optLang" name="optLang">Parent</option>
		</select></td>
	</tr>
	<tr>
		<td nowrap>&nbsp;<span id="txtLang" name="txtLang">Title</span>:</td>
		<td><INPUT type="text" ID="inpTitle" NAME="inpTitle" style="width:160px" class="inpTxt"></td>
	</tr>
	</table>
</td>
</tr>
<tr>
<td class="dialogFooter" style="padding:6;" align="right">
	<table cellpadding=1 cellspacing=0>
	<td>
	<input type=button name=btnCancel id=btnCancel value="cancel" onclick="self.close()" class="inpBtn" onmouseover="this.className='inpBtnOver';" onmouseout="this.className='inpBtnOut'">
	</td>
	<td>
	<input type=button name=btnInsert id=btnInsert value="insert" onclick="applyHyperlink();" class="inpBtn" onmouseover="this.className='inpBtnOver';" onmouseout="this.className='inpBtnOut'">
	</td>
	<td>
	<input type=button name=btnApply id=btnApply value="apply" style="display:none" onclick="applyHyperlink()" class="inpBtn" onmouseover="this.className='inpBtnOver';" onmouseout="this.className='inpBtnOut'">
	</td>
	<td>
	<input type=button name=btnOk id=btnOk value=" ok " style="display:none;" onclick="applyHyperlink();self.close()" class="inpBtn" onmouseover="this.className='inpBtnOver';" onmouseout="this.className='inpBtnOut'">
	</td>
	</table>
</td>
</tr>
</table>

</body>
</html>