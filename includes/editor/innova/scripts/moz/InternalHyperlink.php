<?php
require_once("../../../../../includes/configure.php");
set_time_limit(120);
$LanguageID = isset($_GET['Language'])?$_GET['Language']:"1";?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="../style/editor.css" rel="stylesheet" type="text/css">
<script>
	var sLangDir=window.opener.oUtil.langDir;
	document.write("<scr"+"ipt src='../language/"+sLangDir+"/InternalHyperlink.js'></scr"+"ipt>");
</script>
<script>writeTitle()</script>
<script>

var activeModalWin;

function GetElement(oElement,sMatchTag)
    {
    while (oElement!=null&&oElement.tagName!=sMatchTag)
        {
        if(oElement.tagName=="BODY")return null;
        oElement=oElement.parentNode;
        }
    return oElement;
    }
    
function doWindowFocus()
    {
    window.opener.oUtil.onSelectionChanged=new Function("realTime()");
    }

function bodyOnLoad()
    {
    window.onfocus=doWindowFocus;   
    window.opener.oUtil.onSelectionChanged=new Function("realTime()");

    realTime()
    }

function openAsset()
    {
    if(window.opener.oUtil.obj.cmdAssetManager!="")
		eval(window.opener.oUtil.obj.cmdAssetManager);
	if(window.opener.oUtil.obj.cmdFileManager!="")
		eval(window.opener.oUtil.obj.cmdFileManager);
    }

function setAssetValue(v) 
    {
    document.getElementById("inpURL").value = v;
    }
    
function modalDialogShow(url,width,height)
    {
    var left = screen.availWidth/2 - width/2;
    var top = screen.availHeight/2 - height/2;
    activeModalWin = window.open(url, "", "width="+width+"px,height="+height+",left="+left+",top="+top);
    window.onfocus = function(){if (activeModalWin.closed == false){activeModalWin.focus();};};
        
    }
    
function updateList()
    {
   
    }
    
function realTime()
    {
    
    var inpURL = document.getElementById("inpURL");
    var inpTargetCustom = document.getElementById("inpTargetCustom");
    var inpTarget = document.getElementById("inpTarget");
    var inpTitle = document.getElementById("inpTitle");
    
    var btnInsert = document.getElementById("btnInsert");
    var btnApply = document.getElementById("btnApply");
    var btnOk = document.getElementById("btnOk");
    
    var oEditor=window.opener.oUtil.oEditor;
    
    var oSel = oEditor.getSelection();
    //var oEl = window.opener.getSelectedElement(oSel);
    var oEl = GetElement(window.opener.getSelectedElement(oSel),"A");//new
    
    updateList();

    if(!oEl)//yus
		{
        btnInsert.style.display="block";
        btnApply.style.display="none";
        btnOk.style.display="none";

        inpTarget.value="";
        inpTargetCustom.value="";
        inpTitle.value="";
        
        inpURL.value="";
        
        inpURL.disabled=false;
        return;
		}
		
    //Is there an A element ?
    if (oEl.nodeName == "A")
        {
        
        var range =oEditor.document.createRange();
        range.selectNode(oEl);
        oSel.removeAllRanges();
        oSel.addRange(range);
        
        btnInsert.style.display="none";
        btnApply.style.display="block";
        btnOk.style.display="block";
        

        var sURL = oEl.getAttribute("HREF");
        
        inpTarget.value="";
        inpTargetCustom.value="";
        var trg = oEl.getAttribute("TARGET");
        if(trg=="_self" || trg=="_blank" || trg=="_parent")
            inpTarget.value=trg;//inpTarget
        else
            inpTargetCustom.value=trg;
        
        inpTitle.value="";
        if(oEl.getAttribute("TITLE")!=null) inpTitle.value=oEl.getAttribute("TITLE");//inpTitle //1.5.1

		if(sURL==null)sURL="";
	    inpURL.value=sURL;
            
        }
    else
        {
        btnInsert.style.display="block";
        btnApply.style.display="none";
        btnOk.style.display="none";

        inpTarget.value="";
        inpTargetCustom.value="";
        inpTitle.value="";
        
        inpURL.value="";
        
        }           
    }

function applyHyperlink()
    {
    //if(!window.opener.oUtil.obj.checkFocus()){return;}//Focus stuff
    
    var oEditor=window.opener.oUtil.oEditor;

    //var oSel=oEditor.document.selection.createRange();
    var oSel=oEditor.getSelection();
    var range = oSel.getRangeAt(0);
    window.opener.oUtil.obj.saveForUndo();
    
    var rdoLinkTo = document.getElementsByName("rdoLinkTo");
    var inpURL = document.getElementById("inpURL");
    var inpTargetCustom = document.getElementById("inpTargetCustom");
    var inpTarget = document.getElementById("inpTarget");
    var inpTitle = document.getElementById("inpTitle");
    
    var sURL;
      sURL=inpURL.value;
    
    if(inpURL.value!="")
        {
        var emptySel = false;
        if(document.getElementById("btnInsert").style.display=="block" ||
            document.getElementById("btnInsert").style.display=="")
            {
            
            if(range.toString()=="") 
                { //If no (text) selection, then build selection using the typed URL
                if (range.startContainer.nodeType==Node.ELEMENT_NODE) 
                    {
                    if (range.startContainer.childNodes[range.startOffset].nodeType != Node.TEXT_NODE) 
                        { 
                        if (range.startContainer.childNodes[range.startOffset].nodeName=="BR") emptySel = true; else emptySel=false;  
                        } 
                        else 
                        { 
                        emptySel = true; 
                        }
                    } else {
                        emptySel = true;
                    }
                }
                
            if (emptySel) 
                {
                var node = oEditor.document.createTextNode(sURL);
                range.insertNode(node);
                oEditor.document.designMode = "on";
                
                range = oEditor.document.createRange();
                range.setStart(node, 0);
                range.setEnd(node, sURL.length);
                
                oSel = oEditor.getSelection();
                oSel.removeAllRanges();
                oSel.addRange(range);            
                }
                
            }
        
        var isSelInMidText = (range.startContainer.nodeType==Node.TEXT_NODE) && (range.startOffset>0)
        
        oEditor.document.execCommand("CreateLink", false, sURL);
        
        oSel = oEditor.getSelection();
        range = oSel.getRangeAt(0);
        
        //get A element
        if (range.startContainer.nodeType == Node.TEXT_NODE) {
            var node = (emptySel || !isSelInMidText ? range.startContainer.parentNode : range.startContainer.nextSibling); //A node
            range = oEditor.document.createRange();
            range.selectNode(node);
            
            oSel = oEditor.getSelection();
            oSel.removeAllRanges();
            oSel.addRange(range);
            
        }
        
        var oEl = range.startContainer.childNodes[range.startOffset];
        if(oEl)
            {
            if(inpTarget.value=="" && inpTargetCustom.value=="") oEl.removeAttribute("target",0);//target
            else 
                {
                if(inpTargetCustom.value!="")
                    oEl.target=inpTargetCustom.value;
                else
                    oEl.target=inpTarget.value;
                }
            
            if(inpTitle.value=="") oEl.removeAttribute("title",0);//1.5.1
            else oEl.title=inpTitle.value;
            }

            
        window.opener.realTime(window.opener.oUtil.obj);
        window.opener.oUtil.obj.selectElement(0);
        }
    else
        {
        oEditor.document.execCommand("unlink", false, null);//unlink
        window.opener.realTime(window.opener.oUtil.obj);
        window.opener.oUtil.activeElement=null;
        }   
    realTime();
    window.focus();
    }

function changeLinkTo()
    {
    var rdoLinkTo = document.getElementsByName("rdoLinkTo");
    var inpURL = document.getElementById("inpURL");

        inpURL.disabled=false;
    }           
</script>
</head>
<body onload="loadTxt();bodyOnLoad()" style="overflow:hidden;">

<table width=100% height=100% align=center cellpadding=0 cellspacing=0>
<tr>
<td valign=top style="padding:5;">
    <table width=100%>
    <tr>
        <td nowrap>
            <input type="radio" value="url" name="rdoLinkTo" class="inpRdo" checked onclick="changeLinkTo()">
            <span id="txtLang" name="txtLang">Source</span>:
        </td>
        <td width="100%">
            <table cellpadding="0" cellspacing="0" width="100%">
            <tr>
            <td width="100%">
            <select ID="inpURL" NAME="inpURL" style="width:100%" class="inpTxt">
			<?php include_once("../../../../../internal_hyperlink.php");
			  //echo file_get_contents(DIR_WS_SITE."/internal_hyperlink.php?Language=".$LanguageID);?>								
			</select>
			</td>
            </tr>
            </table>        
        </td>
    </tr>
    <tr>
        <td nowrap>&nbsp;<span id="txtLang" name="txtLang">Target</span>:</td>
        <td><INPUT type="text" ID="inpTargetCustom" NAME="inpTargetCustom" size=15 class="inpTxt">
        <select ID="inpTarget" NAME="inpTarget" class="inpSel">
            <option value=""></option>
            <option value="_self" id="optLang" name="optLang">Self</option>
            <option value="_blank" id="optLang" name="optLang">Blank</option>
            <option value="_parent" id="optLang" name="optLang">Parent</option>
        </select></td>
    </tr>
    <tr>
        <td nowrap>&nbsp;<span id="txtLang" name="txtLang">Title</span>:</td>
        <td><INPUT type="text" ID="inpTitle" NAME="inpTitle" style="width:160px" class="inpTxt"></td>
    </tr>
    </table>
</td>
</tr>
<tr>
<td class="dialogFooter" style="padding:6;" align="right">
    <table cellpadding=1 cellspacing=0>
    <td>
    <input type=button name=btnCancel id=btnCancel value="cancel" onclick="self.close()" class="inpBtn" onmouseover="this.className='inpBtnOver';" onmouseout="this.className='inpBtnOut'">
    </td>
    <td>
    <input type=button name=btnInsert id=btnInsert value="insert" onclick="applyHyperlink()" class="inpBtn" onmouseover="this.className='inpBtnOver';" onmouseout="this.className='inpBtnOut'">
    </td>
    <td>
    <input type=button name=btnApply id=btnApply value="apply" style="display:none" onclick="applyHyperlink()" class="inpBtn" onmouseover="this.className='inpBtnOver';" onmouseout="this.className='inpBtnOut'">
    </td>
    <td>
    <input type=button name=btnOk id=btnOk value=" ok " style="display:none;" onclick="applyHyperlink();self.close()" class="inpBtn" onmouseover="this.className='inpBtnOver';" onmouseout="this.className='inpBtnOut'">
    </td>
    </table>
</td>
</tr>
</table>

</body>
</html>