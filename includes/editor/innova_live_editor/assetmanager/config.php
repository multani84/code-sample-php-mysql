<?php
require_once(dirname(__FILE__)."/../../../../includes/configure.php");
if(!isset($_SESSION['AdminObj']))
{
	@ob_clean();
	$_SESSION['ErrorMessage'] ="Please login to access the website control panel.";
	header("Location:".DIR_WS_SITE_CONTROL."index.php?Page=login");	
	exit;
}

error_reporting(0);
define("CMS_THUMBNAIL_SIZE","100",true);
define("CMS_MEDIUM_SIZE","300",true);
global $CMSImageTypeArray;
$CMSImageTypeArray = array("Jpg","Jpeg","Gif","Bmp","Png","Pdf","Doc","Docx","Txt","Rtf","Xls","Xlsx");

define("CMS_WEBSITE_IMAGES", DIR_WS_SITE_UPLOADS_CMS."_images/");
define("WEBSITEROOT_LOCALPATH", DIR_FS);
define("ASSET_MANAGER_BASE_PATH", str_replace(DIR_FS,"/",DIR_FS_SITE)."uploads/cms/");
define("UPLOAD_FILE_TYPES", strtolower(implode ("|",$CMSImageTypeArray)));
?>