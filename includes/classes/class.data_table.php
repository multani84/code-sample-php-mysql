<?php
class DataTable extends Connection 
{
	var $TableName, $Where;
	
	/**
	* @return boolean
	* @param String $TableName
	* @param  Array $Data
	* @param enum $Action
	* @param string $Parameters
	* @desc This class operate the select, insert, update, delete command
	*/
	
	function __construct($TableName="")	{
		$this->TableName = empty($TableName) ? $this->TableName:$TableName;		$this->Where ='1';		return true;
	}
	function TableInsert($Data="",$Quote=true) 
	{
	    $Query ="";
	
    	$Query = "INSERT INTO ".$this->TableName." set ";
			while (list($Columns,$Value) = each($Data)) {
				if($Quote==true)
		        	$Query .= $Columns."='".$this->MysqlEscapeString($Value)."', ";
		        else 
		        	$Query .= $Columns."=".$this->MysqlEscapeString($Value).", ";
		        	
		      }
		      $Query = substr($Query, 0, -2); 
	    			
    	 $this->ExecuteQuery($Query);
    	 return $this->GetInsertID();
  	}
	
  	function TableUpdate($Data="",$Quote=true) 
	{
	   $Query = "UPDATE ".$this->TableName." set ";
			while (list($Columns,$Value) = each($Data)) {
		      	if($Quote==true)
		        	$Query .= $Columns."='".$this->MysqlEscapeString($Value)."', ";
		        else 
		        	$Query .= $Columns."=".$this->MysqlEscapeString($Value).", ";
		      }
		      $Query = substr($Query, 0, -2);
		      
		      $Query .=" WHERE ".$this->Where;
	    
    	return $this->ExecuteQuery($Query);
  	}
	
  	function TableDelete() 
	{
	   $Query = "DELETE FROM ".$this->TableName." WHERE ".$this->Where;	    
    	return $this->ExecuteQuery($Query);
  	}
  	
  	function TableSelectAll($Data="",$OrderBy="",$Limit="") 
	{
	   $Query = "Select ";
			if($Data =="")
			{
				$Query .= "*";
			}
			else 
			{
				while (list(,$Value) = each($Data)) {
			        $Query .= $Value .", ";
			      }
			      $Query = substr($Query, 0, -2); 
			}
			$Query .=" FROM ".$this->TableName."";
			$Query .=" WHERE ".$this->Where;
			
			if($OrderBy !="")
			$Query .=" ORDER BY ".$OrderBy;
			
			if($Limit !="")
			$Query .=" LIMIT 0,".$Limit;
			
	    
    	return $this->ExecuteQuery($Query);
  	}
  	
  	function TableSelectOne($Data="",$OrderBy="") 
	{
	   $Query = "Select ";
			if($Data =="")
			{
				$Query .= "*";
			}
			else 
			{
				while (list(,$Value) = each($Data)) {
			        $Query .= $Value .", ";
			      }
			      $Query = substr($Query, 0, -2); 
			}
			$Query .=" FROM ".$this->TableName."";
			
			$Query .=" WHERE ".$this->Where;
			
			if($OrderBy !="")
			$Query .=" ORDER BY ".$OrderBy;
			
			$Query .=" LIMIT 0,1 ";
			
	    $this->ExecuteQuery($Query);
    	return $this->GetObjectFromRecord();
  	}

	function GetMax($ColumnName)
	{
		$Query = "SELECT MAX($ColumnName) as $ColumnName FROM ".$this->TableName."";
		
		$Query .=" WHERE ".$this->Where;

		$this->ExecuteQuery($Query);
		$Obj = $this->GetObjectFromRecord();
		return $Obj->$ColumnName ;
	}
  	
	function TableAction($TableName, $Data="", $Action = '0', $Parameters = '1') 
	{
	    $Query ="";
		if($Action=='0' or strtolower($Action) =="select")
	    {
			$Query = "Select ";
			if($Data =="")
			{
				$Query .= "*";
			}
			else 
			{
				while (list(,$Value) = each($Data)) {
			        $Query .= $Value .", ";
			      }
			      $Query = substr($Query, 0, -2); 
			}
			$Query .=" FROM ".$TableName."";
			 $Query .=" WHERE ".$Parameters;
	    }
      elseif($Action=='1' or strtolower($Action) =="insert")
	    {
			$Query = "INSERT INTO ".$TableName." set ";
			while (list($Columns,$Value) = each($Data)) {
		        $Query .= $Columns."='".$this->MysqlEscapeString($Value)."', ";
		      }
		      $Query = substr($Query, 0, -2); 
	    }
     elseif($Action=='2' or strtolower($Action) =="update")
	    {
			$Query = "UPDATE ".$TableName." set ";
			while (list($Columns,$Value) = each($Data)) {
		        $Query .= $Columns."='".$this->MysqlEscapeString($Value)."', ";
		      }
		      $Query = substr($Query, 0, -2);
		      
		      $Query .=" WHERE ".$Parameters;
		      
	    }
    elseif($Action=='3' or strtolower($Action) =="delete")
	    {
			$Query = "Delete FROM ".$TableName."";		      
		    $Query .=" WHERE ".$Parameters;
		      
	    }
    return $this->ExecuteQuery($Query);
  }
	
};
$DataTableObj = new DataTable();
?>