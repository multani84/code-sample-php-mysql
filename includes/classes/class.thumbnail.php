<?php
class Thumbnail 
{
	var $OriginalFileName, $ThumbnailFileName, $ThumbnailWidth, $ThumbnailHeight,$Quality =100;
	var $WaterMarkApply=0,$WaterMarkLogo ="", $LogoX=10,$LogoY=10;
	var $WaterMarkText="Watermark", $WaterMarkColor ="#BBBBBB",$WaterMarkBgColor="#FF0000", $WaterMarkFont, $WaterMarkSize ="30",$WaterMarkAngle="0",$WaterMarkY="20",$WaterMarkX="20",$WartMarkHeight ="20";
	var $GDSupport,$Display=false, $HeightMode =false,$WidthBound = true;
	var $CacheFolder, $GrayScale=false;
	
	function __construct()
	{
		$this->ThumbnailWidth ="100";
		$this->ThumbnailHeight ="";
		$this->WaterMarkFont = dirname(__FILE__)."/FRAMDCN.TTF";
		if(defined("DIR_FS_SITE_UPLOADS"))
			$this->CacheFolder = DIR_FS_SITE_UPLOADS."thumbnail_cache/";	
		else 
			$this->CacheFolder = dirname(__FILE__)."/../../uploads/thumbnail_cache/";	
			
		
		if ($gdv = $this->GDVersion()) 
			$this->GDSupport =true;
		else 
			$this->GDSupport =false;
				
	}
	
	function GDVersion() 	{		return true;		/*
   	   if (! extension_loaded('gd')) { return; }
		   ob_start();
		   phpinfo(8);
		   $info=ob_get_contents();
		   ob_end_clean();
		   $info=stristr($info, 'gd version');
		   preg_match('/\d/', $info, $gd);
	   		return $gd[0];			*/
	} 

	function CreateThumbnail()
	{
		
		if($this->Display===true)
			@ob_start();
		
		$ArrayType = explode(".",basename($this->OriginalFileName));
		$Type=$ArrayType[count($ArrayType)-1];
		
		$ThumbnailSize = getimagesize($this->OriginalFileName);
		
		if($this->ThumbnailWidth >$ThumbnailSize[0] && $this->WidthBound ===true)
					$this->ThumbnailWidth = $ThumbnailSize[0];
				
		$ThumbnailRatio = $ThumbnailSize[0]/$this->ThumbnailWidth;
		$ThumbnailHeight = $this->ThumbnailHeight != ""?$this->ThumbnailHeight:($ThumbnailSize[1]/$ThumbnailRatio);
		
		
		
		if($this->HeightMode===true)
		{
			
			if($ThumbnailSize[1] > $ThumbnailSize[0])
			{
				if($this->ThumbnailHeight =="")
					$ThumbnailHeight = $this->ThumbnailWidth;
				else 
					$ThumbnailHeight = $this->ThumbnailHeight;
					
				$ThumbnailWidth = $ThumbnailSize[1]/$ThumbnailRatio;	
				
				
				$ThumbnailRatio = $ThumbnailSize[1]/$ThumbnailHeight;
				$ThumbnailWidth = $ThumbnailSize[0]/$ThumbnailRatio;
				
				if($ThumbnailWidth > $this->ThumbnailWidth)
				{
					if($this->ThumbnailHeight > $ThumbnailHeight)
					{
					
						$ThumbnailRatio = $ThumbnailSize[1]/$this->ThumbnailWidth;
						$this->ThumbnailWidth = $ThumbnailSize[0]/$ThumbnailRatio;
						$ThumbnailHeight = $ThumbnailSize[1]/$ThumbnailRatio;
					}
					else 
					{
						$ThumbnailRatio = $ThumbnailSize[0]/$this->ThumbnailWidth;
						$ThumbnailHeight = $ThumbnailSize[1]/$ThumbnailRatio;
					}
				
					
				}
				else 
				{
					$this->ThumbnailWidth = $ThumbnailWidth;					
				}
			}
			else 
			{
				if($ThumbnailSize[1]/$ThumbnailRatio > $ThumbnailHeight)
				{
					
					$ThumbnailRatio = $ThumbnailSize[1]/$ThumbnailHeight;
					$this->ThumbnailWidth = $ThumbnailSize[0]/$ThumbnailRatio;
					 
				}
				else 
				{
					$ThumbnailHeight = $ThumbnailSize[1]/$ThumbnailRatio;
				}
				
			}
		
		}
		
		$this->ThumbnailWidth = round($this->ThumbnailWidth);
		$ThumbnailHeight = round($ThumbnailHeight);
		
		if($this->ThumbnailFileName !="")
			$CacheFileName = basename($this->ThumbnailFileName);
		else
			$CacheFileName = basename($this->OriginalFileName);
			
		if(isset($this->CacheFolder) && $this->CacheFolder !="")
		{
			$CacheFileFolder = $this->CacheFolder.$this->ThumbnailWidth."x".$ThumbnailHeight;
			$CacheFileName = $CacheFileFolder."/".$CacheFileName;
		}

		if(isset($CacheFileName) && $CacheFileName !="" && file_exists($CacheFileName) && $this->Display==true)
		{
			$this->ThumbnailRead($CacheFileName);
			
		}
		
		### Check GD Support Start ###
		if($this->GDSupport===false)
		{
			@copy($this->OriginalFileName,$this->ThumbnailFileName);
			return;
		}
		### Check GD Support End ###
		
		switch(trim(ucwords(strtolower($Type))))
			{
				case "Jpg":
				case "Jpeg":
				$Thumbnail = imagecreatetruecolor($this->ThumbnailWidth, $ThumbnailHeight);				
	    		$new = imagecreatefromjpeg($this->OriginalFileName);
	       		 for($i=0; $i<256; $i++) {
	       				 imagecolorallocate($Thumbnail, $i, $i, $i);
	        		}				$size = getimagesize($this->OriginalFileName);
			    imagecopyresampled($Thumbnail, $new, 0, 0, 0, 0, $this->ThumbnailWidth, $ThumbnailHeight, $size[0], $size[1]);				if($this->GrayScale===true)						imagefilter($Thumbnail, IMG_FILTER_GRAYSCALE); //first, convert to grayscale				
			    
			    if($this->WaterMarkApply=="1")			    {
			    	if($this->WaterMarkLogo !="" and file_exists($this->WaterMarkLogo))			    	{
			    		$LogoSize = getimagesize($this->WaterMarkLogo);
			    		if(substr($this->WaterMarkLogo,-3,3) =="png")
						$BgImage = imagecreatefrompng($this->WaterMarkLogo);						
						else 
						$BgImage = imagecreatefromjpeg($this->WaterMarkLogo);						
						
						imageAlphaBlending($BgImage, false);
					    imageSaveAlpha($BgImage, true);						
					    imagecopy($Thumbnail, $BgImage, $this->LogoX, $this->LogoY, 0, 0, $LogoSize[0], $LogoSize[1]);
			    		
			    	}
			    	else 
			    	{
				    		$x = $this->WaterMarkX;
							$y = $this->WaterMarkY;				
							$r = intval(substr($this->WaterMarkColor,1,2),16);
							$g = intval(substr($this->WaterMarkColor,3,2),16);
							$b = intval(substr($this->WaterMarkColor,5,2),16);
							
			    		if($this->WaterMarkBgColor==1)
						{
							$text_color = imagecolorallocate($Thumbnail,$r,$g,$b);			
							ImageTTFText ($Thumbnail, $this->WaterMarkSize, $this->WaterMarkAngle,$x,$y, -$text_color, $this->WaterMarkFont, $this->WaterMarkText); 
			    		}
						else 
						{
						
							$BgImage = imagecreatetruecolor($this->ThumbnailWidth,$this->WartMarkHeight);		
							$br = intval(substr($this->WaterMarkBgColor,1,2),16);
							$bg = intval(substr($this->WaterMarkBgColor,3,2),16);
							$bb = intval(substr($this->WaterMarkBgColor,5,2),16);
							$BgColor =imagecolorallocate($BgImage, $br, $bg, $bb);
							imagefilledrectangle($BgImage, 0, 0, $this->ThumbnailWidth,$this->WartMarkHeight, $BgColor);
							
							$text_color = imagecolorallocate($BgImage,$r,$g,$b);			
							ImageTTFText ($BgImage, $this->WaterMarkSize, $this->WaterMarkAngle,0,($this->WartMarkHeight-5), -$text_color, $this->WaterMarkFont, $this->WaterMarkText); 
							imagecopy($Thumbnail, $BgImage, $x, $y, 0, 0, $this->ThumbnailWidth,$this->WartMarkHeight);
						}
			    	}
					
			    }
			    
			    if(isset($CacheFileName) && $CacheFileName !="" && !file_exists($CacheFileName))
				{
					@mkdir($CacheFileFolder,0777);					imagejpeg($Thumbnail, $CacheFileName,$this->Quality);
				}
		
			    if($this->Display==true)
			    {
			    	@ob_clean();
					header("Content-Type: image/jpeg");
					header("Pragma: no-cache");
					header("Expires: 0");
					imagejpeg($Thumbnail,null,$this->Quality);
			    }
			    else 
			    {
					imagejpeg($Thumbnail, $this->ThumbnailFileName,$this->Quality);
				}
				
				
				
				ImageDestroy($new);
			    ImageDestroy($Thumbnail);
			    break;
			
			    case "Png":
				$Thumbnail = imagecreatetruecolor($this->ThumbnailWidth, $ThumbnailHeight);
				imagealphablending($Thumbnail, false);
				imagesavealpha($Thumbnail, true); 
	    		$new = imagecreatefrompng($this->OriginalFileName);
	    		imagealphablending($new, true);
	       		 for($i=0; $i<256; $i++) {
	       				 imagecolorallocate($Thumbnail, $i, $i, $i);
	        		}
			    $size = getimagesize($this->OriginalFileName);
			    imagecopyresampled($Thumbnail, $new, 0, 0, 0, 0, $this->ThumbnailWidth, $ThumbnailHeight, $size[0], $size[1]);
			    
			    if($this->WaterMarkApply=="1")
			    {
			    	if($this->WaterMarkLogo !="" and file_exists($this->WaterMarkLogo))
			    	{
			    		$LogoSize = getimagesize($this->WaterMarkLogo);
			    		if(substr($this->WaterMarkLogo,-3,3) =="png")
						$BgImage = imagecreatefrompng($this->WaterMarkLogo);						
						else 
						$BgImage = imagecreatefromjpeg($this->WaterMarkLogo);						
						
						imageAlphaBlending($BgImage, false);
					    imageSaveAlpha($BgImage, true);
					    imagecopy($Thumbnail, $BgImage, $this->LogoX, $this->LogoY, 0, 0, $LogoSize[0], $LogoSize[1]);
			    		
			    	}
			    	else 
			    	{
				    		$x = $this->WaterMarkX;
							$y = $this->WaterMarkY;				
							$r = intval(substr($this->WaterMarkColor,1,2),16);
							$g = intval(substr($this->WaterMarkColor,3,2),16);
							$b = intval(substr($this->WaterMarkColor,5,2),16);
							
			    		if($this->WaterMarkBgColor==1)
						{
							$text_color = imagecolorallocate($Thumbnail,$r,$g,$b);			
							ImageTTFText ($Thumbnail, $this->WaterMarkSize, $this->WaterMarkAngle,$x,$y, -$text_color, $this->WaterMarkFont, $this->WaterMarkText); 
			    		}
						else 
						{
						
							$BgImage = imagecreatetruecolor($this->ThumbnailWidth,$this->WartMarkHeight);		
							$br = intval(substr($this->WaterMarkBgColor,1,2),16);
							$bg = intval(substr($this->WaterMarkBgColor,3,2),16);
							$bb = intval(substr($this->WaterMarkBgColor,5,2),16);
							$BgColor =imagecolorallocate($BgImage, $br, $bg, $bb);
							imagefilledrectangle($BgImage, 0, 0, $this->ThumbnailWidth,$this->WartMarkHeight, $BgColor);
							
							$text_color = imagecolorallocate($BgImage,$r,$g,$b);			
							ImageTTFText ($BgImage, $this->WaterMarkSize, $this->WaterMarkAngle,0,($this->WartMarkHeight-5), -$text_color, $this->WaterMarkFont, $this->WaterMarkText); 
							imagecopy($Thumbnail, $BgImage, $x, $y, 0, 0, $this->ThumbnailWidth,$this->WartMarkHeight);
						}
			    	}
					
			    }
			    
			    if(isset($CacheFileName) && $CacheFileName !="" && !file_exists($CacheFileName))
				{
					@mkdir($CacheFileFolder,0777);
					imagepng($Thumbnail, $CacheFileName);
				}
			    
			    if($this->Display==true)
			    {
			    	@ob_clean();
					header("Content-Type: image/x-png");
					header("Pragma: no-cache");
					header("Expires: 0");
					imagepng($Thumbnail);
			    }
			    else 
			    {
				imagepng($Thumbnail, $this->ThumbnailFileName);
			    }		    
			    ImageDestroy($new);
			    ImageDestroy($Thumbnail);
			    break;
			  
			    case "Bmp":
				$Thumbnail = imagecreatetruecolor($this->ThumbnailWidth, $ThumbnailHeight);
	    		$new = $this->imagecreatefrombmp($this->OriginalFileName);
	       		 for($i=0; $i<256; $i++) {
	       				 imagecolorallocate($Thumbnail, $i, $i, $i);
	        		}
			    $size = getimagesize($this->OriginalFileName);
			    imagecopyresampled($Thumbnail, $new, 0, 0, 0, 0, $this->ThumbnailWidth, $ThumbnailHeight, $size[0], $size[1]);
			    
			    if($this->WaterMarkApply=="1")
			    {
			    	if($this->WaterMarkLogo !="" and file_exists($this->WaterMarkLogo))
			    	{
			    		$LogoSize = getimagesize($this->WaterMarkLogo);
			    		if(substr($this->WaterMarkLogo,-3,3) =="png")
						$BgImage = imagecreatefrompng($this->WaterMarkLogo);						
						else 
						$BgImage = imagecreatefromjpeg($this->WaterMarkLogo);						
						
						imageAlphaBlending($BgImage, false);
					    imageSaveAlpha($BgImage, true);
					    imagecopy($Thumbnail, $BgImage, $this->LogoX, $this->LogoY, 0, 0, $LogoSize[0], $LogoSize[1]);
			    		
			    	}
			    	else 
			    	{
				    		$x = $this->WaterMarkX;
							$y = $this->WaterMarkY;				
							$r = intval(substr($this->WaterMarkColor,1,2),16);
							$g = intval(substr($this->WaterMarkColor,3,2),16);
							$b = intval(substr($this->WaterMarkColor,5,2),16);
							
			    		if($this->WaterMarkBgColor==1)
						{
							$text_color = imagecolorallocate($Thumbnail,$r,$g,$b);			
							ImageTTFText ($Thumbnail, $this->WaterMarkSize, $this->WaterMarkAngle,$x,$y, -$text_color, $this->WaterMarkFont, $this->WaterMarkText); 
			    		}
						else 
						{
						
							$BgImage = imagecreatetruecolor($this->ThumbnailWidth,$this->WartMarkHeight);		
							$br = intval(substr($this->WaterMarkBgColor,1,2),16);
							$bg = intval(substr($this->WaterMarkBgColor,3,2),16);
							$bb = intval(substr($this->WaterMarkBgColor,5,2),16);
							$BgColor =imagecolorallocate($BgImage, $br, $bg, $bb);
							imagefilledrectangle($BgImage, 0, 0, $this->ThumbnailWidth,$this->WartMarkHeight, $BgColor);
							
							$text_color = imagecolorallocate($BgImage,$r,$g,$b);			
							ImageTTFText ($BgImage, $this->WaterMarkSize, $this->WaterMarkAngle,0,($this->WartMarkHeight-5), -$text_color, $this->WaterMarkFont, $this->WaterMarkText); 
							imagecopy($Thumbnail, $BgImage, $x, $y, 0, 0, $this->ThumbnailWidth,$this->WartMarkHeight);
						}
			    	}
					
			    }
			    
			    if(isset($CacheFileName) && $CacheFileName !="" && !file_exists($CacheFileName))
				{
					@mkdir($CacheFileFolder,0777);
					imagejpeg($Thumbnail, $CacheFileName,$this->Quality);
				}
			    
			    if($this->Display==true)
			    {
			    	@ob_clean();
					header("Content-Type: image/jpeg");
					header("Pragma: no-cache");
					header("Expires: 0");
					imagejpeg($Thumbnail,null,$this->Quality);
			    }
			    else 
			    {
					imagejpeg($Thumbnail, $this->ThumbnailFileName,$this->Quality);
				}
				ImageDestroy($new);
			    ImageDestroy($Thumbnail);
			    break;
			 
			    case "Gif":
			    
		    		$new = imagecreatefromgif($this->OriginalFileName);
	       		   	$size = getimagesize($this->OriginalFileName);
			   		$tpcolor = imagecolorat($new, 0, 0);
		            $Thumbnail = imagecreate($this->ThumbnailWidth, $ThumbnailHeight);
		           imagepalettecopy($Thumbnail, $new);
		           imagecopyresized($Thumbnail, $new, 0, 0, 0, 0, $this->ThumbnailWidth, $ThumbnailHeight,$size[0], $size[1]);
		           $pixel_over_black = imagecolorat($Thumbnail, 0, 0);
		           
		           $bg = imagecolorallocate($Thumbnail, 255, 255, 255);
		           imagefilledrectangle($Thumbnail, 0, 0, $this->ThumbnailWidth, $ThumbnailHeight,$bg);
		           imagecopyresized($Thumbnail, $new, 0, 0, 0, 0, $this->ThumbnailWidth, $ThumbnailHeight, $size[0], $size[1]);
		           $pixel_over_white = imagecolorat($Thumbnail, 0, 0);
	        	 
		           if($pixel_over_black != $pixel_over_white)
	        	  {
	        	 	 imagefilledrectangle($Thumbnail, 0, 0, $this->ThumbnailWidth, $ThumbnailHeight,$tpcolor);
		             imagecopyresized($Thumbnail, $new, 0, 0, 0, 0, $this->ThumbnailWidth,$ThumbnailHeight, $size[0], $size[1]);
		             imagecolortransparent($Thumbnail, $tpcolor);
		          	
	        	  }
				
	        	//imagecopyresampled($Thumbnail, $new, 0, 0, 0, 0, $this->ThumbnailWidth, $ThumbnailHeight, $size[0], $size[1]);
			    ob_clean();
			    /* watermark*/
		        if($this->Display==true)
			    {
			    	@ob_clean();
					header("Content-Type: image/gif");
					header("Pragma: no-cache");
					header("Expires: 0");
				imagegif($Thumbnail);
			    }
			    else 
			    {
				imagegif($Thumbnail, $this->ThumbnailFileName);
			    }
			    ImageDestroy($new);
			    ImageDestroy($Thumbnail);
			    break;
			    
			    case "GifOld":
				$Thumbnail = imagecreatetruecolor($this->ThumbnailWidth, $ThumbnailHeight);
	    		$new = imagecreatefromgif($this->OriginalFileName);
	       		 for($i=0; $i<256; $i++) {
	       				imagecolorallocate($Thumbnail, $i, $i, $i);
	        		}
	        	
	        	$size = getimagesize($this->OriginalFileName);
			    imagecopyresampled($Thumbnail, $new, 0, 0, 0, 0, $this->ThumbnailWidth, $ThumbnailHeight, $size[0], $size[1]);
			    ob_clean();
			    
			    if($this->WaterMarkApply=="1")
			     {
			    	if($this->WaterMarkLogo !="" and file_exists($this->WaterMarkLogo))
			    	{
			    		$LogoSize = getimagesize($this->WaterMarkLogo);
						$BgImage = imagecreatefrompng($this->WaterMarkLogo);						
						imageAlphaBlending($BgImage, false);
					    imageSaveAlpha($BgImage, true);
					    imagecopy($Thumbnail, $BgImage, $this->LogoX, $this->LogoY, 0, 0, $LogoSize[0], $LogoSize[1]);
			    		
			    	}
			    	else 
			    	{
			    		$x = $this->WaterMarkX;
						$y = $this->WaterMarkY;				
						$r = intval(substr($this->WaterMarkColor,1,2),16);
						$g = intval(substr($this->WaterMarkColor,3,2),16);
						$b = intval(substr($this->WaterMarkColor,5,2),16);
						$BgImage = imagecreatetruecolor($this->ThumbnailWidth,$this->WartMarkHeight);		
						
						$br = intval(substr($this->WaterMarkBgColor,1,2),16);
						$bg = intval(substr($this->WaterMarkBgColor,3,2),16);
						$bb = intval(substr($this->WaterMarkBgColor,5,2),16);
						$BgColor =imagecolorallocate($BgImage, $br, $bg, $bb);
						imagefilledrectangle($BgImage, 0, 0, $this->ThumbnailWidth,$this->WartMarkHeight, $BgColor);
						
						$text_color = imagecolorallocate($BgImage,$r,$g,$b);			
						ImageTTFText ($BgImage, $this->WaterMarkSize, $this->WaterMarkAngle,0,($this->WartMarkHeight-5), -$text_color, $this->WaterMarkFont, $this->WaterMarkText); 
					    imagecopy($Thumbnail, $BgImage, $x, $y, 0, 0, $this->ThumbnailWidth,$this->WartMarkHeight);
			    	}
				
					$text_color = imagecolorallocate($Thumbnail,$r,$g,$b);			
					ImageTTFText ($Thumbnail, $this->WaterMarkSize, $this->WaterMarkAngle,$x,$y, -$text_color, $this->WaterMarkFont, $this->WaterMarkText); 
			    }
			    
			    if(isset($CacheFileName) && $CacheFileName !="" && !file_exists($CacheFileName))
				{
					@mkdir($CacheFileFolder,0777);
					imagegif($Thumbnail, $CacheFileNames);
				}
			    
			    if($this->Display==true)
			    {
			    	@ob_clean();
					header("Content-Type: image/gif");
					header("Pragma: no-cache");
					header("Expires: 0");
					imagegif($Thumbnail);
			    }
			    else 
			    {
				imagegif($Thumbnail, $this->ThumbnailFileName);
			    }
			    ImageDestroy($new);
			    ImageDestroy($Thumbnail);
			    break;
			}
			
	}/// Create Function end
	
	function ResizeThumbnail()
		{
			$ArrayType = explode(".",basename($this->OriginalFileName));
			$Type=$ArrayType[count($ArrayType)-1];
		
			$ThumbnailSize = getimagesize($this->OriginalFileName);
			$ThumbnailRatio = $ThumbnailSize[0]/$this->ThumbnailWidth;
			$ThumbnailHeight = $this->ThumbnailHeight != ""?$this->ThumbnailHeight:($ThumbnailSize[1]/$ThumbnailRatio);
			
			
		
		switch(trim(ucwords(strtolower($Type))))
			{
			case "Jpg":
			case "Jpeg":
				$Thumbnail = imagecreatetruecolor($this->ThumbnailWidth, $ThumbnailHeight);
		    	$new = imagecreatefromjpeg($this->OriginalFileName);
		        for($i=0; $i<256; $i++) 
		       	{
		       		 imagecolorallocate($Thumbnail, $i, $i, $i);
		        }
				    $size = getimagesize($this->OriginalFileName);
				    imagecopyresampled($Thumbnail, $new, 0, 0, 0, 0, $this->ThumbnailWidth, $ThumbnailHeight, $size[0], $size[1]);
				    imagejpeg($Thumbnail, $this->ThumbnailFileName);
				    ImageDestroy($new);
				    ImageDestroy($Thumbnail);
			 break;
		
			case "Png":
				$Thumbnail = imagecreatetruecolor($this->ThumbnailWidth, $ThumbnailHeight);
		    	$new = imagecreatefrompng($this->OriginalFileName);
		        for($i=0; $i<256; $i++) 
		       	{
		       		 imagecolorallocate($Thumbnail, $i, $i, $i);
		        }
				    $size = getimagesize($this->OriginalFileName);
				    imagecopyresampled($Thumbnail, $new, 0, 0, 0, 0, $this->ThumbnailWidth, $ThumbnailHeight, $size[0], $size[1]);
				    imagepng($Thumbnail, $this->ThumbnailFileName);
				    ImageDestroy($new);
				    ImageDestroy($Thumbnail);
		 break;
		 case "Bmp":
				$Thumbnail = imagecreatetruecolor($this->ThumbnailWidth, $ThumbnailHeight);
		    	$new = imagecreatefrombmp($this->OriginalFileName);
		        for($i=0; $i<256; $i++) 
		       	{
		       		 imagecolorallocate($Thumbnail, $i, $i, $i);
		        }
				    $size = getimagesize($this->OriginalFileName);
				    imagecopyresampled($Thumbnail, $new, 0, 0, 0, 0, $this->ThumbnailWidth, $ThumbnailHeight, $size[0], $size[1]);
				    imagejpeg($Thumbnail, $this->ThumbnailFileName);
				    ImageDestroy($new);
				    ImageDestroy($Thumbnail);
			break;
		case "Gif":
				$Thumbnail = imagecreatetruecolor($this->ThumbnailWidth, $ThumbnailHeight);
		    	$new = imagecreatefromgif($this->OriginalFileName);
		        for($i=0; $i<256; $i++) 
		       	{
		       		 imagecolorallocate($Thumbnail, $i, $i, $i);
		        }
				    $size = getimagesize($this->OriginalFileName);
				    imagecopyresampled($Thumbnail, $new, 0, 0, 0, 0, $this->ThumbnailWidth, $ThumbnailHeight, $size[0], $size[1]);
				    imagejpeg($Thumbnail, $this->ThumbnailFileName);
				    ImageDestroy($new);
				    ImageDestroy($Thumbnail);
			break;
			
			}
		
		}// Resize Function end
		
	function RotateImage($RotateAngle)
		{

		$temp_image_filename = $this->OriginalFileName;
		$ArrayType = explode(".",basename($this->OriginalFileName));
		$Type=$ArrayType[count($ArrayType)-1];
		
		switch(trim(ucwords(strtolower($Type))))
		{
			case"Jpg":
			case"Jpeg":
			case"Bmp":
			case"Gif":
			$temp_image_size = getimagesize($temp_image_filename);
			  if($temp_image_size[0] > $temp_image_size[1]) {
			  $new_dimensions = $temp_image_size[0];
			  } else {
			  $new_dimensions = $temp_image_size[1];
			  }
			$original = imagecreatefromjpeg($temp_image_filename);
			$intermediate = imagecreatetruecolor($new_dimensions, $new_dimensions);
			$final = imagecreatetruecolor($temp_image_size[1], $temp_image_size[0]);
			imagecopyresampled($intermediate, $original, 0, 0, 0, 0, $temp_image_size[0], $temp_image_size[1], $temp_image_size[0], $temp_image_size[1]);
			  if($RotateAngle == 90) {
			  $rotated = imagerotate($intermediate, 90, 0);
			    if($temp_image_size[1] > $temp_image_size[0]) {
			    imagecopyresampled($final, $rotated, 0, 0, 0, $temp_image_size[1]-$temp_image_size[0], $temp_image_size[1], $temp_image_size[0], $temp_image_size[1], $temp_image_size[0]);
			    } else {
			    imagecopyresampled($final, $rotated, 0, 0, 0, 0, $temp_image_size[1], $temp_image_size[0], $temp_image_size[1], $temp_image_size[0]);
			    }
			  } 
			  elseif($RotateAngle == 270)
			   {
			  $rotated = imagerotate($intermediate, -90, 0);
			    if($temp_image_size[0] > $temp_image_size[1]) {
			    imagecopyresampled($final, $rotated, 0, 0, $temp_image_size[0]-$temp_image_size[1], 0, $temp_image_size[1], $temp_image_size[0], $temp_image_size[1], $temp_image_size[0]);
			    } else {
			    imagecopyresampled($final, $rotated, 0, 0, 0, 0, $temp_image_size[1], $temp_image_size[0], $temp_image_size[1], $temp_image_size[0]);
			    }
			  }
			
			imagejpeg($final, $temp_image_filename);
			ImageDestroy($original);
			ImageDestroy($intermediate);
			ImageDestroy($rotated);
			ImageDestroy($final);
	
					break;		
				
			case"Png":
				$temp_image_size = getimagesize($temp_image_filename);
				if($temp_image_size[0] > $temp_image_size[1]) {
						  $new_dimensions = $temp_image_size[0];
						  } else {
						  $new_dimensions = $temp_image_size[1];
						  }
						$original = imagecreatefrompng($temp_image_filename);
						$intermediate = imagecreatetruecolor($new_dimensions, $new_dimensions);
						$final = imagecreatetruecolor($temp_image_size[1], $temp_image_size[0]);
						imagecopyresampled($intermediate, $original, 0, 0, 0, 0, $temp_image_size[0], $temp_image_size[1], $temp_image_size[0], $temp_image_size[1]);
						
						if($RotateAngle == 90) {
						  $rotated = imagerotate($intermediate, 90, 0);
						    if($temp_image_size[1] > $temp_image_size[0]) {
						    imagecopyresampled($final, $rotated, 0, 0, 0, $temp_image_size[1]-$temp_image_size[0], $temp_image_size[1], $temp_image_size[0], $temp_image_size[1], $temp_image_size[0]);
						    } else {
						    imagecopyresampled($final, $rotated, 0, 0, 0, 0, $temp_image_size[1], $temp_image_size[0], $temp_image_size[1], $temp_image_size[0]);
						    }
						  } elseif($RotateAngle==270)
						   {
						  $rotated = imagerotate($intermediate, -90, 0);
						    if($temp_image_size[0] > $temp_image_size[1]) {
						    imagecopyresampled($final, $rotated, 0, 0, $temp_image_size[0]-$temp_image_size[1], 0, $temp_image_size[1], $temp_image_size[0], $temp_image_size[1], $temp_image_size[0]);
						    } else {
						    imagecopyresampled($final, $rotated, 0, 0, 0, 0, $temp_image_size[1], $temp_image_size[0], $temp_image_size[1], $temp_image_size[0]);
						    }
						  }
						
						imagepng($final, $temp_image_filename);
						ImageDestroy($original);
						ImageDestroy($intermediate);
						ImageDestroy($rotated);
						ImageDestroy($final);
				break;			
		}
				
		return true;			
	} /// Rotate function end
	
	  function generateCode($characters) {
      /* list all possible characters, similar looking characters and vowels have been removed */
      $possible = '23456789bcdfghjkmnpqrstvwxyz';
      $code = '';
      $i = 0;
      while ($i < $characters) { 
         $code .= substr($possible, mt_rand(0, strlen($possible)-1), 1);
         $i++;
      }
      return $code;
   }
   
	 function CaptchaSecurityImages($width='120',$height='40',$characters='6',$session_name ='SecurityCode') {
	  $code = $this->generateCode($characters);
      /* font size will be 75% of the image height */
      $font_size = $height * 0.75;
      $image = imagecreate($width, $height) or die('Cannot initialize new GD image stream');
      /* set the colours */
      $background_color = imagecolorallocate($image, 255, 255, 255);
      $text_color = imagecolorallocate($image, 20, 40, 100);
      $noise_color = imagecolorallocate($image, 100, 120, 180);
      /* generate random dots in background */
      for( $i=0; $i<($width*$height)/3; $i++ ) {
         imagefilledellipse($image, mt_rand(0,$width), mt_rand(0,$height), 1, 1, $noise_color);
      }
      /* generate random lines in background */
      for( $i=0; $i<($width*$height)/150; $i++ ) {
         imageline($image, mt_rand(0,$width), mt_rand(0,$height), mt_rand(0,$width), mt_rand(0,$height), $noise_color);
      }
      /* create textbox and add text */
      $textbox = imagettfbbox($font_size, 0, $this->WaterMarkFont, $code) or die('Error in imagettfbbox function');
      $x = ($width - $textbox[4])/2;
      $y = ($height - $textbox[5])/2;
      imagettftext($image, $font_size, 0, $x, $y, $text_color, $this->WaterMarkFont , $code) or die('Error in imagettftext function');
      /* output captcha image to browser */
      @ob_clean();
      if(session_id() == '') {
	    session_start();
	  }
      $_SESSION[$session_name] = $code;
      header('Content-Type: image/jpeg');
      imagejpeg($image);
      imagedestroy($image);
      exit;
   }
   
     function imagecreatefrombmp($filename) 
	{
	 $tmp_name = tempnam("/tmp", "GD");
	 if($this->ConvertBMP2GD($filename, $tmp_name)) 
	 {
	  $img = imagecreatefromgd($tmp_name);
	  unlink($tmp_name);
	  return $img;
	 } return false;
	}
	
	/* ConvertBMP2GD Start*/

function ConvertBMP2GD($src, $dest = false) {
 if(!($src_f = fopen($src, "rb"))) {
  return false;
 }
 if(!($dest_f = fopen($dest, "wb"))) {
  return false;
 }
 $header = unpack("vtype/Vsize/v2reserved/Voffset", fread($src_f,
14));
 $info = unpack("Vsize/Vwidth/Vheight/vplanes/vbits/Vcompression/Vimagesize/Vxres/Vyres/Vncolor/Vimportant",
fread($src_f, 40));
 
 extract($info);
 extract($header);

 if($type != 0x4D42) {  // signature "BM"
  return false;
 }

 $palette_size = $offset - 54;
 $ncolor = $palette_size / 4;
 $gd_header = "";
 // true-color vs. palette
 $gd_header .= ($palette_size == 0) ? "\xFF\xFE" : "\xFF\xFF"; 
 $gd_header .= pack("n2", $width, $height);
 $gd_header .= ($palette_size == 0) ? "\x01" : "\x00";
 if($palette_size) {
  $gd_header .= pack("n", $ncolor);
 }
 // no transparency
 $gd_header .= "\xFF\xFF\xFF\xFF";     

 fwrite($dest_f, $gd_header);

 if($palette_size) {
  $palette = fread($src_f, $palette_size);
  $gd_palette = "";
  $j = 0;
  while($j < $palette_size) {
   $b = $palette{$j++};
   $g = $palette{$j++};
   $r = $palette{$j++};
   $a = $palette{$j++};
   $gd_palette .= "$r$g$b$a";
  }
  $gd_palette .= str_repeat("\x00\x00\x00\x00", 256 - $ncolor);
  fwrite($dest_f, $gd_palette);
 }

 $scan_line_size = (($bits * $width) + 7) >> 3;
 $scan_line_align = ($scan_line_size & 0x03) ? 4 - ($scan_line_size &
0x03) : 0;

 for($i = 0, $l = $height - 1; $i < $height; $i++, $l--) {
  // BMP stores scan lines starting from bottom
  fseek($src_f, $offset + (($scan_line_size + $scan_line_align) *
$l));
  $scan_line = fread($src_f, $scan_line_size);
  if($bits == 24) {
   $gd_scan_line = "";
   $j = 0;
   while($j < $scan_line_size) {
    $b = $scan_line{$j++};
    $g = $scan_line{$j++};
    $r = $scan_line{$j++};
    $gd_scan_line .= "\x00$r$g$b";
   }
  }
  else if($bits == 8) {
   $gd_scan_line = $scan_line;
  }
  else if($bits == 4) {
   $gd_scan_line = "";
   $j = 0;
   while($j < $scan_line_size) {
    $byte = ord($scan_line{$j++});
    $p1 = chr($byte >> 4);
    $p2 = chr($byte & 0x0F);
    $gd_scan_line .= "$p1$p2";
   } $gd_scan_line = substr($gd_scan_line, 0, $width);
  }
  else if($bits == 1) {
   $gd_scan_line = "";
   $j = 0;
   while($j < $scan_line_size) {
    $byte = ord($scan_line{$j++});
    $p1 = chr((int) (($byte & 0x80) != 0));
    $p2 = chr((int) (($byte & 0x40) != 0));
    $p3 = chr((int) (($byte & 0x20) != 0));
    $p4 = chr((int) (($byte & 0x10) != 0));
    $p5 = chr((int) (($byte & 0x08) != 0));
    $p6 = chr((int) (($byte & 0x04) != 0));
    $p7 = chr((int) (($byte & 0x02) != 0));
    $p8 = chr((int) (($byte & 0x01) != 0));
    $gd_scan_line .= "$p1$p2$p3$p4$p5$p6$p7$p8";
   } $gd_scan_line = substr($gd_scan_line, 0, $width);
  }
    
  fwrite($dest_f, $gd_scan_line);
 }
 fclose($src_f);
 fclose($dest_f);
 return true;
}
	/* ConvertBMP2GD end*/
	
	/* download function start*/
function ThumbnailRead($file)
{
	$len = filesize($file);
	$filename = basename($file);
	$file_extension = strtolower(substr(strrchr($filename,"."),1));
				
	switch( $file_extension )
	{
		case "gif": $ctype="image/gif"; break;
		case "png": $ctype="image/png"; break;
		case "jpeg":
		case "jpg": $ctype="image/jpg"; break;
		default: $ctype="application/force-download";
	}
	
	@ob_clean();
	header("Content-Type: $ctype");
	header("Pragma: public");
	header("Expires: 0");
	@readfile($file);
	exit;
}
/* download function end*/
	
	
};

$ThumbnailObj = new Thumbnail();

if(isset($_GET['OriginalPath']) && $_GET['OriginalPath'] != "")
{
	$ThumbnailObj->OriginalFileName = $_GET['OriginalPath'];	
	
	if(@$_GET['enc']!="")
		$ThumbnailObj->OriginalFileName = base64_decode($ThumbnailObj->OriginalFileName);	
	
	$ThumbnailObj->ThumbnailWidth = $_GET['w'];
	if(@$_GET['h']!="")
		$ThumbnailObj->ThumbnailHeight = $_GET['h'];
	
	if(@$_GET['HeightMode'] =="true")
		$ThumbnailObj->HeightMode= true;
	if(@$_GET['HeightMode'] =="false")
		$ThumbnailObj->HeightMode=false;
	
	if(@$_GET['WidthBound'] =="true")
		$ThumbnailObj->WidthBound= true;
	if(@$_GET['WidthBound'] =="false")
		$ThumbnailObj->WidthBound= false;
	
	if(@$_GET['wm'] =="1")
	{
		$ThumbnailObj->WaterMarkApply ="1";				
		if(@$_GET['wms'] !="")
			$ThumbnailObj->WaterMarkSize =$_GET['wms'];
		if(@$_GET['wmt'] !="")
			$ThumbnailObj->WaterMarkText =$_GET['wmt'];				
		if(@$_GET['wmbc'] !="")
			$ThumbnailObj->WaterMarkBgColor ="#".$_GET['wmbc'];				
		if(@$_GET['wmfc'] !="")
			$ThumbnailObj->WaterMarkColor ="#".$_GET['wmfc'];				
		if(@$_GET['wmh'] !="")
			$ThumbnailObj->WartMarkHeight =$_GET['wmh'];				
		if(@$_GET['wmx'] !="")
			$ThumbnailObj->WaterMarkX =$_GET['wmx'];				
		if(@$_GET['wmx'] !="")
			$ThumbnailObj->WaterMarkY =$_GET['wmy'];				
		if(@$_GET['wmlo'] !="")
			$ThumbnailObj->WaterMarkLogo =$_GET['wmlo'];
		if(@$_GET['wmlox'] !="")
			$ThumbnailObj->LogoX =$_GET['wmlox'];
		if(@$_GET['wmloy'] !="")
			$ThumbnailObj->LogoY =$_GET['wmloy'];
			
	
	}	
		
	$ThumbnailObj->Display =true;				
	$ThumbnailObj->CreateThumbnail();
					
}
if(isset($_GET['sk_id']) && $_GET['sk_id'] != "")
{
	parse_str(base64_decode($_GET['sk_id']),$_SKArray);
	if(isset($_SKArray['OriginalPath']) && $_SKArray['OriginalPath'] != "")
	{
		$ThumbnailObj->OriginalFileName = $_SKArray['OriginalPath'];	
		
		if(@$_SKArray['enc']!="")
			$ThumbnailObj->OriginalFileName = base64_decode($ThumbnailObj->OriginalFileName);	
		
		$ThumbnailObj->ThumbnailWidth = $_SKArray['w'];
		if(@$_SKArray['h']!="")
			$ThumbnailObj->ThumbnailHeight = $_SKArray['h'];
		
		if(@$_SKArray['HeightMode'] =="true" OR @$_SKArray['HeightMode'] =="1")
			$ThumbnailObj->HeightMode= true;
		if(@$_SKArray['HeightMode'] =="false")
			$ThumbnailObj->HeightMode=false;
	
		if(@$_SKArray['WidthBound'] =="true")
			$ThumbnailObj->WidthBound= true;
		if(@$_SKArray['WidthBound'] =="false")
			$ThumbnailObj->WidthBound= false;
	
		
		if(@$_SKArray['wm'] =="1")
		{
			$ThumbnailObj->WaterMarkApply ="1";				
			if(@$_SKArray['wms'] !="")
				$ThumbnailObj->WaterMarkSize =$_SKArray['wms'];
			if(@$_SKArray['wmt'] !="")
				$ThumbnailObj->WaterMarkText =$_SKArray['wmt'];				
			if(@$_SKArray['wmbc'] !="")
				$ThumbnailObj->WaterMarkBgColor ="#".$_SKArray['wmbc'];				
			if(@$_SKArray['wmfc'] !="")
				$ThumbnailObj->WaterMarkColor ="#".$_SKArray['wmfc'];				
			if(@$_SKArray['wmh'] !="")
				$ThumbnailObj->WartMarkHeight =$_SKArray['wmh'];				
			if(@$_SKArray['wmx'] !="")
				$ThumbnailObj->WaterMarkX =$_SKArray['wmx'];				
			if(@$_SKArray['wmx'] !="")
				$ThumbnailObj->WaterMarkY =$_SKArray['wmy'];				
			if(@$_SKArray['wmlo'] !="")
				$ThumbnailObj->WaterMarkLogo =$_SKArray['wmlo'];
			if(@$_SKArray['wmlox'] !="")
				$ThumbnailObj->LogoX =$_SKArray['wmlox'];
			if(@$_SKArray['wmloy'] !="")
				$ThumbnailObj->LogoY =$_SKArray['wmloy'];
				
		
		}	
		$ThumbnailObj->Display =true;				
		$ThumbnailObj->CreateThumbnail();
						
	}			
	
	
					
}

if(isset($_GET['Captcha']) && $_GET['Captcha'] == "1")
{
	$width = isset($_GET['w']) && $_GET['w'] < 600 ? $_GET['w'] : '120';
	$height = isset($_GET['h']) && $_GET['h'] < 200 ? $_GET['h'] : '40';
	$characters = isset($_GET['c']) && $_GET['c'] > 2 ? $_GET['c'] : '6';
	$session_name = isset($_GET['s']) && $_GET['s'] != '' ? $_GET['s'] : 'SecurityCode';
	$ThumbnailObj->CaptchaSecurityImages($width,$height,$characters,$session_name);
	
					
}