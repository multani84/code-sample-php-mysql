<?php
class Defination 
{
	var $FileName, $FileinArray = array();
	
		
	function __construct()
	{
		return true;
	}
	function InsertArrayInFile($FileName,$ContentArray) 
	{
	    	
		if(file_exists($FileName) && is_writable($FileName))
		{
				$NewLine =chr(13);
				$fp = fopen($FileName, "w");
				 fputs($fp,"<?php".$NewLine);	
				foreach ($ContentArray as $Key=>$Value)
				{
				    $String = 'define("'.$ContentArray[$Key]['Name'].'","'.$this->FileStringReplace($ContentArray[$Key]['Value']).'"); ### '.$ContentArray[$Key]['Label'].' /### '.$NewLine;
					fputs($fp,$String);	
				}
				 fputs($fp,"?>".$NewLine);	
				fclose($fp);
		}
			return true;
  	}
	function InsertAdvanceArrayInFile($FileName,$ContentArray) 
	{
	    	
		if(file_exists($FileName) && is_writable($FileName))
		{
				$NewLine =chr(13);
				$fp = fopen($FileName, "w");
				 fputs($fp,"<?php".$NewLine);	
				foreach ($ContentArray as $Key=>$Value)
				{
				    $String = 'define("'.$ContentArray[$Key]['Name'].'","'.$this->FileStringReplace($ContentArray[$Key]['Value']).'"); ### '.$ContentArray[$Key]['Label'].' ### ';
				    
				    if(isset($ContentArray[$Key]['Type']) && $ContentArray[$Key]['Type'] !="")
				    	$String .= ' '.$ContentArray[$Key]['Type'].' ### ';
				    
				    if(isset($ContentArray[$Key]['Attributes']) && $ContentArray[$Key]['Attributes'] !="")
				    	$String .= ' '.$ContentArray[$Key]['Attributes'].' ### ';
				    
				    
					$String .= $NewLine;
					fputs($fp,$String);	
				}
				 fputs($fp,"?>".$NewLine);	
				fclose($fp);
		}
			return true;
  	}
	
  	function GetArrayFromDefineFile($FileName) 
	{
		$ReturnArray = array();
		if(file_exists($FileName) && is_readable($FileName))
		{
			
			$Content =  file_get_contents($FileName);
			$Content = str_replace(array('<?php','?>',"\n"),array('','',chr(13)),$Content);
			
			$LineArray = explode(chr(13),$Content);
			
			foreach ($LineArray as $Key=>$Value)
			{
				if(trim($Value) !="")
				{
					$FormArray  = explode('"',$Value);
					
					$Label = $this->SubStringBetween($Value,'###','/###');
					$ColumnName = isset($FormArray['1'])?$FormArray['1']:"";
					$ColumnValue =  isset($FormArray['3'])?$FormArray['3']:"";
					
					$ReturnArray[$Key]['Label'] = $Label;
					$ReturnArray[$Key]['Name'] = $ColumnName;
					$ReturnArray[$Key]['Value'] = $ColumnValue;
				}
			}
		}
			return $ReturnArray;
	}

	function GetAdvanceArrayFromDefineFile($FileName) 
	{
		$ReturnArray = array();
		if(file_exists($FileName) && is_readable($FileName))
		{
			
			$Content =  file_get_contents($FileName);
			$Content = str_replace(array('<?php','?>',"\n"),array('','',chr(13)),$Content);
			
			$LineArray = explode(chr(13),$Content);
			
			foreach ($LineArray as $Key=>$Value)
			{
				if(trim($Value) !="")
				{
					$FormArray  = explode('"',$Value);
					$TypeArray = explode('###',$Value);
					$Type ="";
					$Attributes ="";
					foreach ($TypeArray as $k=>$v)
					{
						if(stristr($v,"type:"))
							$Type = $v;
						
						if(stristr($v,"attributes:"))
							$Attributes = $v;
					}
					
					//$Label = $this->SubStringBetween($Value,'###','###');
					$ColumnName = isset($FormArray['1'])?$FormArray['1']:"";
					$ColumnValue =  isset($FormArray['3'])?$FormArray['3']:"";
					
					$ReturnArray[$Key]['Label'] = isset($TypeArray['1'])?trim($TypeArray['1']):"";
					$ReturnArray[$Key]['Type'] = isset($Type)?trim($Type):"";
					$ReturnArray[$Key]['Attributes'] = isset($Attributes)?trim($Attributes):"";
					$ReturnArray[$Key]['Name'] = trim($ColumnName);
					$ReturnArray[$Key]['Value'] = $ColumnValue;
				}
			}
		}
			return $ReturnArray;
	} 
	
	function DisplayDefinationType($name,$val,$type="",$Attributes="")
	{
		$Attributes = @substr($Attributes,11);
		switch ($type)
		{
			case (stristr($type,"type:select")>=0):
				$OptionArray = array();
				if(strtolower(substr(trim($type),12,7))=="options")
				{
				$OptionSubArray = explode("|",$this->SubStringBetween(substr(trim($type),19),'{','}'));
					foreach ($OptionSubArray as $k=>$v)
					{
						
						$SA = explode("=",$v);
						if(isset($SA[1]) && $SA[1] !="")
							$OptionArray[$SA[0]] = $SA[1];
						else 
							$OptionArray[$SA[0]] = $SA[0];
						
					}
				}
				?>
				<select name="<?php echo $name?>" <?php echo $Attributes?>>
				<?php
				foreach ($OptionArray as $k=>$v)
				{
					?>
					<option value="<?php echo $k?>" <?php echo $k==$val?"selected":""?> ><?php echo $v?></option>
					<?php
				}
				?>
				
				</select>
				<?php
		
			break;
			default:
				?>
				<input type="text" name="<?php echo $name?>" value="<?php echo $val?>" <?php echo $Attributes?>>
				<?php
			break;
		}
		
	}
	
	function FileStringReplace($Str)
	{
		$Str = str_replace(array('"',"###"),array('&quot;',""),$Str);
		return $Str; 
	}
  	
  	//Helper function for parsing response
function SubStringBetween($haystack,$start,$end) 
{
   if (strpos($haystack,$start) === false || strpos($haystack,$end) === false) 
   {
      return false;
   } 
   else 
   {
      $start_position = strpos($haystack,$start)+strlen($start);
      $end_position = strpos($haystack,$end);
      return substr($haystack,$start_position,$end_position-$start_position);
   }
}

	
};
$DefinationObj = new Defination();
?>