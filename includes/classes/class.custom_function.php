<?php
class Custom extends Connection 
{
	function __construct()
	{
		if(defined("DEFINE_BLOCKIPADDRESS") && trim(@constant("DEFINE_BLOCKIPADDRESS")) !="" && strstr(trim(@constant("DEFINE_BLOCKIPADDRESS")),@$_SERVER['REMOTE_ADDR']) ==true)
		{
			var_dump("You can not access this site.");exit;	
		}
		if(!isset($_SESSION['SKCOunterInit']))
		{
		  $CounterObj = new DataTable(TABLE_COUNTER);
		  $CounterObj->Where ="DATE_FORMAT(CounterDate,'%Y-%m-%d')='".date('Y-m-d')."'";	
		  $Obj = $CounterObj->TableSelectOne();
		  if(isset($Obj->CounterID) && $Obj->CounterID !="")
		  {
		  	$CounterObj->TableUpdate(array("Visits"=>$Obj->Visits+1));
		  }
		  else 
		  {
		  	$CounterArray = array();
		  	$CounterArray['CounterDate'] = date('Y-m-d');
		  	$CounterArray['Visits'] = "1";
		  	$CounterObj->TableInsert($CounterArray);
		  }
		  @ob_clean();
		  $_SESSION['SKCOunterInit'] = "1";
		}
		return;
	}
	function GetCounterStat($Day='',$Month='',$Year='')
	{
		$CounterObj = new DataTable(TABLE_COUNTER);
		$CounterObj->Where = "1";
		if($Day !="")
			$CounterObj->Where = $CounterObj->Where." AND DATE_FORMAT(CounterDate,'%d') = '".$Day."'";
		if($Month !="")
			$CounterObj->Where = $CounterObj->Where." AND DATE_FORMAT(CounterDate,'%m') = '".$Month."'";
		if($Year !="")
			$CounterObj->Where = $CounterObj->Where." AND DATE_FORMAT(CounterDate,'%Y') = '".$Year."'";
		$Obj = $CounterObj->TableSelectOne(array("Sum(Visits) AS Count"));
		return $Obj->Count;		
	}
	function CheckUserLogin($ReturnURL="",$LoginMessage="")
	{
		if(isset($_SESSION['UserID']) and !empty($_SESSION['UserID']))
		{
			$TableName = TABLE_USERS." u 
						 LEFT JOIN ".TABLE_USER_TYPE_RELATION." r on (u.UserID = r.UserID)
						 LEFT JOIN ".TABLE_USER_TYPE." ut on (ut.UserTypeID = r.UserTypeID)";
						 
			$UserObj = new DataTable($TableName);
			//$UserObj->DisplayQuery = true;
			$UserObj->Where = "u.UserID = '".$UserObj->MysqlEscapeString($_SESSION['UserID'])."' AND Active = 1";
			$CurrentUserObj = $UserObj->TableSelectOne(array("*"),"r.Discount DESC,ut.SortOrder DESC");
			if($UserObj->GetNumRows() == 1)
			{
				$UserObj->TableUpdate(array("LastLogin"=>date("YmdHis")));
				return $CurrentUserObj;
			}
			else 
			{
				if(!empty($ReturnURL))
				{
					@ob_clean();
					$_SESSION['ReturnURL'] = $ReturnURL;
				}
				MyRedirect(MakePageURL("index.php","Page=account/login")); 	
		  		exit;
			}
		}
		else 
		{
			if(!empty($ReturnURL))
			{
				@ob_clean();
				$_SESSION['ReturnURL'] = $ReturnURL;
				//
				//$_SESSION['ErrorMessage'] = $LoginMessage==""?NOT_LOGGED_IN_MSG:$LoginMessage;
			}
			MyRedirect(MakePageURL("index.php","Page=account/login"));			
			exit;
		}
	}
	function AuthenticateUser($UserName, $Password,$Encrypted=false)
	{
		$UserFirstObj = new DataTable(TABLE_USERS);
		$UserObj = new DataTable(TABLE_USERS);
		$UserFirstObj->Where = "UserName = '".$UserFirstObj->MysqlEscapeString($UserName)."' AND Active = 1";
		$FirstObj = $UserFirstObj->TableSelectOne(array("Password"));
		if(isset($FirstObj->Password) && $FirstObj->Password !="")
		{
			$TmpPwd = explode(":",$FirstObj->Password);
			$EncyptPassword = NeoCryptedPassword($Password,$TmpPwd[1]).":".$TmpPwd[1];
			if($EncyptPassword == $FirstObj->Password)
			{
				$UserObj->Where = "UserName = '".$UserObj->MysqlEscapeString($UserName)."' AND Password = '".$EncyptPassword."' AND Active = 1";
				$CurrentUserObj = $UserObj->TableSelectOne(array("UserID"));
				$NumRows = $UserObj->GetNumRows();
			}
		}
		if($Encrypted===true)
		{
			$UserObj->Where = "UserName = '".$UserObj->MysqlEscapeString($UserName)."' AND Password = '".$Password."' AND Active = 1";
			$CurrentUserObj = $UserObj->TableSelectOne(array("UserID"));
			$NumRows = $UserObj->GetNumRows();
		}
		if(isset($NumRows) && $NumRows == 1)
		{
			$UserObj->TableUpdate(array("PrevLastLogin"=>"LastLogin"),false);
			$UserObj->TableUpdate(array("LastLogin"=>date("YmdHis")));
			@ob_clean();
			$_SESSION['UserID'] = $CurrentUserObj->UserID;
			
			if(isset($_SESSION['ReturnURL']) && $_SESSION['ReturnURL'] != "")
			{
				$ReturnURL = $_SESSION['ReturnURL'];
				$_SESSION['ReturnURL'] = "";
				unset($_SESSION['ReturnURL']);
				MyRedirect($ReturnURL);
				exit;
			}
			else 
			{
				MyRedirect(MakePageURL("index.php","Page=account/my_account"));			
				exit;
			}
		}
		return false;
	}
	function AuthenticateController($UserName, $Password,$Encrypted=false)
	{
		$ControllerFirstObj = new DataTable(TABLE_CONTROLLERS);
		$ControllerObj = new DataTable(TABLE_CONTROLLERS);
		$ControllerFirstObj->Where = "UserName = '".$ControllerFirstObj->MysqlEscapeString($UserName)."' AND Active = 1";
		$FirstObj = $ControllerFirstObj->TableSelectOne(array("Password"));
		if(isset($FirstObj->Password) && $FirstObj->Password !="")
		{
			$TmpPwd = explode(":",$FirstObj->Password);
			$EncyptPassword = NeoCryptedPassword($Password,$TmpPwd[1]).":".$TmpPwd[1];
			if($EncyptPassword == $FirstObj->Password)
			{
				$ControllerObj->Where = "UserName = '".$ControllerObj->MysqlEscapeString($UserName)."' AND Password = '".$EncyptPassword."' AND Active = 1";
				$CurrentControllerObj = $ControllerObj->TableSelectOne();
				$NumRows = $ControllerObj->GetNumRows();
			}
		}
		if($Encrypted===true)
		{
			$ControllerObj->Where = "UserName = '".$ControllerObj->MysqlEscapeString($UserName)."' AND Password = '".$Password."' AND Active = 1";
			$CurrentControllerObj = $ControllerObj->TableSelectOne();
			$NumRows = $ControllerObj->GetNumRows();
		}
		if(isset($NumRows) && $NumRows == 1)
		{
			$ControllerObj->TableUpdate(array("PrevLastLogin"=>"LastLogin"),false);
			$ControllerObj->TableUpdate(array("LastLogin"=>date("YmdHis")));
			return $CurrentControllerObj;
		}
		return false;
	}	
	function RedirectIfLogin($ReturnURL="")
	{
		if(isset($_SESSION['UserID']) and !empty($_SESSION['UserID']))
		{
			$UserObj = new DataTable(TABLE_USERS);
			$UserObj->Where = "UserID = '".$UserObj->MysqlEscapeString($_SESSION['UserID'])."' AND Active = 1";
			$UserObj->TableSelectOne(array("UserName"));
			if($UserObj->GetNumRows() == 1)
			{
				MyRedirect(MakePageURL("index.php","Page=account/my_account"));			
				exit;
			}
		}
	}
	function IsUserLogin()
	{
		if(isset($_SESSION['UserID']) and !empty($_SESSION['UserID']))
		{
			$TableName = TABLE_USERS." u 
						 LEFT JOIN ".TABLE_USER_TYPE_RELATION." r on (u.UserID = r.UserID)
						 LEFT JOIN ".TABLE_USER_TYPE." ut on (ut.UserTypeID = r.UserTypeID)";
						 
			$UserObj = new DataTable($TableName);
			//$UserObj->DisplayQuery = true;
			$UserObj->Where = "u.UserID = '".$UserObj->MysqlEscapeString($_SESSION['UserID'])."' AND Active = 1";
			$CurrentUserObj = $UserObj->TableSelectOne(array("*"),"r.Discount DESC,ut.SortOrder DESC");
			if($UserObj->GetNumRows() == 1)
				return $CurrentUserObj;
			else 
				return false;
		}
	}
	function Logout()
	{
		@ob_clean();
		if(isset($_SESSION['UserID']) and !empty($_SESSION['UserID']))
		{
			$_SESSION['UserID'] = "";
			unset($_SESSION['UserID']);
			$_SESSION['InfoMessage'] = constant("DEFINE_LOGOUT_MSG");
			$CartObj = new DataTable(TABLE_TMPCART);
			$SessionID = session_id();
			$CartObj->Where =" SessionID='".$CartObj->MysqlEscapeString(session_id())."'";
			$CartObj->TableDelete();	
			unset($_SESSION['OrderID']);
			session_unset();
			session_destroy();
			session_regenerate_id();
			@ob_clean();
		}
		else 
		{
			$_SESSION['ErrorMessage'] = constant("DEFINE_NOT_LOGGED_IN_MSG");
		}
		MyRedirect(MakePageURL("index.php","Page=account/login"));			
		exit;
	}
	function CartQuantity()
	{
		$TmpCartObj = new DataTable(TABLE_TMPCART);
		$TmpCartObj->Where =" SessionID='".session_id()."'";
		$DataArray = array();
		$DataArray[0]= "SUM(Qty) as Qty";
		$Obj = $TmpCartObj->TableSelectOne($DataArray);
		if(isset($Obj->Qty) && $Obj->Qty >0)
			return $Obj->Qty;
		else 
			return 0;
	}
	function CartTotal()
	{
		$TmpCartObj = new DataTable(TABLE_TMPCART);
		$TmpCartObj->Where =" SessionID='".session_id()."'";
		$DataArray = array();
		$DataArray[0]= "SUM(Total) as Total";
		$Obj = $TmpCartObj->TableSelectOne($DataArray);
		if(isset($Obj->Total) && $Obj->Total >0)
			return $Obj->Total;
		else 
			return 0;
	}
	function SendMailByUsingTemplate($TemplateName,$arr=array())
	{
		$arr['neo_sid'] = session_id();
		@ob_clean();
		if(file_exists(DIR_FS_SITE_INCLUDES_EMAILS.$TemplateName))
			require_once(DIR_FS_SITE_INCLUDES_EMAILS.$TemplateName);
		$MessageBody = ob_get_contents();
		@ob_clean();
		$Mail_FileAttachment = isset($arr['Mail_FileAttachment'])?$arr['Mail_FileAttachment']:false;
		$Mail_AttachmentFileName = isset($arr['Mail_AttachmentFileName'])?$arr['Mail_FAttachmentFileName']:"";
		$Mail_Subject = isset($arr['Mail_Subject'])?$arr['Mail_Subject']:constant("DEFINE_SITE_NAME");
		$Mail_ToEmail = isset($arr['Mail_ToEmail'])?$arr['Mail_ToEmail']:"";
		$Mail_FromEmail = isset($arr['Mail_FromEmail'])?$arr['Mail_FromEmail']:constant("DEFINE_ADMIN_EMAIL");
		$Mail_FromName = isset($arr['Mail_FromName'])?$arr['Mail_FromName']:constant("DEFINE_SITE_NAME");
		$Mail_BCC = isset($arr['Mail_BCC'])?$arr['Mail_BCC']:"";
		$Mail_Subject = str_replace("sitename.com",constant("DEFINE_SITE_NAME"),$Mail_Subject);
		SendEmail($Mail_Subject,$Mail_ToEmail,$Mail_FromEmail,$Mail_FromName,$MessageBody,$Mail_BCC,constant("DEFINE_EMAIL_FORMAT"),$Mail_FileAttachment,$Mail_AttachmentFileName);
	}
};
$CustomObj = new Custom();
?>