<?php

class Controllers extends Connection 

{

	var $ControllerID, $UserName, $Password, $Name, $Address1, $Address2, $City , $State, $Country, $ZipCode, $EmailAddress, $Phone, $AllowPages, $Active;

	

	function AddController()

	{

		$Query = "INSERT into ".TABLE_CONTROLLERS." set ControllerID='".$this->MysqlEscapeString($this->ControllerID)."', 

												  UserName='".$this->MysqlEscapeString($this->UserName)."',

												  Password='".$this->MysqlEscapeString($this->Password)."',

												  Name='".$this->MysqlEscapeString($this->Name)."',

												  Address1='".$this->MysqlEscapeString($this->Address1)."',

											      Address2='".$this->MysqlEscapeString($this->Address2)."', 

												  City='".$this->MysqlEscapeString($this->City)."',

												  State='".$this->MysqlEscapeString($this->State)."',

												  ZipCode='".$this->MysqlEscapeString($this->State)."',

												  Country='".$this->MysqlEscapeString($this->Country)."',

												  EmailAddress='".$this->MysqlEscapeString($this->EmailAddress)."',

		 										  Phone='".$this->MysqlEscapeString($this->Phone)."',

												  AllowPages='".$this->MysqlEscapeString($this->AllowPages)."',

											      Active='".$this->MysqlEscapeString($this->Active)."'";

		

		return $this->ExecuteQuery($Query);

	}

	

	function UpdateController($ControllerID)

	{

		if($ControllerID =="1")

		{

			$this->AllowPages = "*";

			$this->Active = "1";

		}

		$Query = "UPDATE ".TABLE_CONTROLLERS." set UserName='$this->UserName',

													  Password='$this->Password',

													  Name='$this->Name',

													  Address1='$this->Address1',

												      Address2='$this->Address2', 

													  City='$this->City',

												      State='$this->State',

													  ZipCode='$this->ZipCode',

													  Country='$this->Country',

													  EmailAddress='$this->EmailAddress',

			 										  Phone='$this->Phone',

												      AllowPages='$this->AllowPages',

												      Active='$this->Active'

											   WHERE ControllerID='$ControllerID'";

		return $this->ExecuteQuery($Query);

	}

	

	function ChangePassword($ControllerID)

	{

		$Query = "UPDATE ".TABLE_CONTROLLERS." set UserName='".$this->MysqlEscapeString($this->UserName)."',

											 Password='".$this->MysqlEscapeString($this->Password)."' WHERE ControllerID='".$this->MysqlEscapeString($ControllerID)."'";

		return $this->ExecuteQuery($Query);

	}

	

	function UpdateControllerActive($ControllerID,$Active=0)

	{

		$Query = "UPDATE ".TABLE_CONTROLLERS." set Active='$Active' WHERE ControllerID='".$this->MysqlEscapeString($ControllerID)."'";

		return $this->ExecuteQuery($Query);

	}

	

	

	function GetMaxControllerIDByControllerTable()

	{

		$Query = "SELECT MAX(ControllerID) as ControllerID FROM ".TABLE_CONTROLLERS."";

		

		$this->ExecuteQuery($Query);

		$Obj = $this->GetObjectFromRecord();

		return $Obj->ControllerID;

	}

	

	

	function GetControllerDetailByControllerIDInControllerTable($ControllerID)

	{

		$Query ="SELECT * FROM ".TABLE_CONTROLLERS." WHERE ControllerID='".$this->MysqlEscapeString($ControllerID)."'";

		$this->ExecuteQuery($Query);

		return $this->GetObjectFromRecord(); 

	}

	

	function GetControllerByUserNameAndPassword($UserName,$Password)

	{

		$Query ="SELECT * FROM ".TABLE_CONTROLLERS." WHERE UserName='$UserName' AND Password='$Password' AND Active='1'";

		$this->ExecuteQuery($Query);

		$Obj = $this->GetObjectFromRecord(); 

		$Query1 ="UPDATE ".TABLE_CONTROLLERS." SET PrevLastLogin=LastLogin WHERE UserName='$UserName' AND Password='$Password' AND Active='1'";

		$this->ExecuteQuery($Query1);

		

		$Query2 ="UPDATE ".TABLE_CONTROLLERS." SET LastLogin='".date('YmdHis')."' WHERE UserName='$UserName' AND Password='$Password' AND Active='1'";

		$this->ExecuteQuery($Query2);

		

		return $Obj;

	}

	

	function GetControllerByUserName($UserName)

	{

		$Query ="SELECT * FROM ".TABLE_CONTROLLERS." WHERE UserName='".$this->MysqlEscapeString($UserName)."'";

		$this->ExecuteQuery($Query);

		return $this->GetObjectFromRecord(); 

	}

	

	function GetAllControllersByControllerTable($AllowPages = 0)

	{

		$Query ="SELECT * FROM ".TABLE_CONTROLLERS." WHERE AllowPages='".$this->MysqlEscapeString($AllowPages)."' ORDER BY ControllerID";

		return $this->ExecuteQuery($Query);

	}

	

	function GetAllControllers($OrderBy ="UserName ASC")

	{

		$Query ="SELECT *,DATE_FORMAT(PrevLastLogin,'%b %d, %Y %r')as MyLastLogin FROM ".TABLE_CONTROLLERS." ORDER BY $OrderBy";

		return $this->ExecuteQuery($Query);

	}



	function DeleteControllerByControllerID($ControllerID)

	{

		$Query1 ="DELETE FROM ".TABLE_CONTROLLERS." WHERE ControllerID='".$this->MysqlEscapeString($ControllerID)."'";

		$this->ExecuteQuery($Query1);

		return true;

	}	

	

	/////////All Countries 

	function GetAllCountries()

	{

		$Query ="SELECT * FROM ".TABLE_COUNTRIES." ORDER BY CountryID";

		return $this->ExecuteQuery($Query);

	}



	

};

$ControllerObj = new Controllers();

?>