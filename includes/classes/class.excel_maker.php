<?php
class ExcelMaker
{
	var $SheetArray = array();
	var $Version, $WindowHeight, $WindowWidth, $WindowTopX, $WindowTopY, $BrkPoint ;	
	var $DBHostName,$DBUserName,$DBPassword,$DBDataBase;
	var $QueryArray = array();
	var $TxtFormat = true;
	var $CsvQuoteFormat = true;
	var $CsvFileFormat = true, $CsvFileHander = "";
	
	
	function __construct()
	{
		$this->Version = "11.8132";
		$this->WindowHeight = "9345";
		$this->WindowWidth = "11340";
		$this->WindowTopX = "120";
		$this->WindowTopY = "60";
		$this->BrkPoint = chr(13);
		return;
	}
	
	function CreateHeader()
	{
		$HeaderContent ="";
		$HeaderContent ='<DocumentProperties xmlns="urn:schemas-microsoft-com:office:office">'.$this->BrkPoint;
			$HeaderContent .='<Version>'.$this->Version.'</Version>'.$this->BrkPoint;
		$HeaderContent .='</DocumentProperties>'.$this->BrkPoint;
		$HeaderContent .='<ExcelWorkbook xmlns="urn:schemas-microsoft-com:office:excel">'.$this->BrkPoint;
			$HeaderContent .='<WindowHeight>'.$this->WindowHeight.'</WindowHeight>'.$this->BrkPoint;
			$HeaderContent .='<WindowWidth>'.$this->WindowWidth.'</WindowWidth>'.$this->BrkPoint;
			$HeaderContent .='<WindowTopX>'.$this->WindowTopX.'</WindowTopX>'.$this->BrkPoint;
			$HeaderContent .='<WindowTopY>'.$this->WindowTopY.'</WindowTopY>'.$this->BrkPoint;
			$HeaderContent .='<ActiveSheet>1</ActiveSheet>'.$this->BrkPoint;
			$HeaderContent .='<ProtectStructure>False</ProtectStructure>'.$this->BrkPoint;
			$HeaderContent .='<ProtectWindows>False</ProtectWindows>'.$this->BrkPoint;
		$HeaderContent .='</ExcelWorkbook>'.$this->BrkPoint;
			
		
		return $HeaderContent;
	}
	
	function CreateSheet()
	{
		$SheetContent ="";
		$StyleContent ="";
		$StyleContent .='<Styles>'.$this->BrkPoint;
			$StyleContent .='<Style ss:ID="Default" ss:Name="Normal">'.$this->BrkPoint;
			$StyleContent .='<Alignment ss:Vertical="Bottom"/>'.$this->BrkPoint;
			$StyleContent .='<Borders/>'.$this->BrkPoint;
			$StyleContent .='<Font/>'.$this->BrkPoint;
			$StyleContent .='<Interior/>'.$this->BrkPoint;
			$StyleContent .='<NumberFormat/>'.$this->BrkPoint;
			$StyleContent .='<Protection/>'.$this->BrkPoint;
			$StyleContent .='</Style>'.$this->BrkPoint;
		
		$SheetCount = isset($this->SheetArray)?count($this->SheetArray):"";
		foreach ($this->SheetArray as $key=>$Value)
		{
			$TxtContent = "";
			
			$RowCount = isset($this->SheetArray[$key]['Data'])?count($this->SheetArray[$key]['Data']):"0";
			$ColCount = isset($this->SheetArray[$key]['Data'][1])?count($this->SheetArray[$key]['Data'][1]):"0";
			$SheetName = isset($this->SheetArray[$key]['Name'])?$this->SheetArray[$key]['Name']:"Sheet".$key;
			
			$SheetContent .='<Worksheet ss:Name="'.$SheetName.'">'.$this->BrkPoint;
			$SheetContent .='<Table ss:ExpandedColumnCount="'.$ColCount.'" ss:ExpandedRowCount="'.$RowCount.'" x:FullColumns="1" x:FullRows="1">'.$this->BrkPoint;
			$SheetContent .='<Column ss:AutoFitWidth="0" ss:Width="80" ss:Span="1"/>'.$this->BrkPoint;
			//$SheetContent .='<Column ss:Index="'.$SheetCount.'" ss:AutoFitWidth="0" ss:Width="80.75"/>'.$this->BrkPoint;
				   
					foreach ($this->SheetArray[$key]['Data'] as $DKey=>$Value)    ////// Multiple Rows loop
					{
					
						if($this->CsvFileFormat===true)
						{
							 fputcsv($this->CsvFileHander, $Value);
						}
						
						$RW_ID = "s_".$key."_".$DKey;
						$FontName = isset($this->SheetArray[$key]['Style'][$DKey]['FontName'])?$this->SheetArray[$key]['Style'][$DKey]['FontName']:"Tahoma";
						$Color = isset($this->SheetArray[$key]['Style'][$DKey]['Color'])?$this->SheetArray[$key]['Style'][$DKey]['Color']:"#000000";
						$Bold = (isset($this->SheetArray[$key]['Style'][$DKey]['Bold']) && $this->SheetArray[$key]['Style'][$DKey]['Bold']==true)?"1":"0";
						$Size = isset($this->SheetArray[$key]['Style'][$DKey]['Size'])?$this->SheetArray[$key]['Style'][$DKey]['Size']:"14";
						$BgColor = isset($this->SheetArray[$key]['Style'][$DKey]['BgColor'])?$this->SheetArray[$key]['Style'][$DKey]['BgColor']:"#FFFFFF";
						$Height = isset($this->SheetArray[$key]['Style'][$DKey]['Height'])?$this->SheetArray[$key]['Style'][$DKey]['Height']:"20";
						$Align = isset($this->SheetArray[$key]['Style'][$DKey]['Align'])?$this->SheetArray[$key]['Style'][$DKey]['Align']:"Center";
						$Valign = isset($this->SheetArray[$key]['Style'][$DKey]['Valign'])?$this->SheetArray[$key]['Style'][$DKey]['Valign']:"Bottom";
						
							$StyleContent .='<Style ss:ID="'.$RW_ID.'">'.$this->BrkPoint;
								$StyleContent .='<Alignment ss:Horizontal="'.$Align.'" ss:Vertical="'.$Valign.'"/>'.$this->BrkPoint;
								$StyleContent .='<Font ss:FontName="'.$FontName.'" ss:Color="'.$Color.'" ss:Bold="'.$Bold.'"/>'.$this->BrkPoint;
								//if($BgColor !="#FFFFFF")
									//$StyleContent .='<Interior ss:Color="'.$BgColor.'" ss:Pattern="Solid"/>'.$this->BrkPoint;
							$StyleContent .='</Style>'.$this->BrkPoint;
 												
						$SheetContent .='<Row ss:StyleID="'.$RW_ID.'">'.$this->BrkPoint;				
						for($i=1;$i<=$ColCount;$i++)         ////// Multiple Rows Column
						{
							$Val = isset($Value[$i])?$Value[$i]:"";
							$Val = $this->StripTags($Val);
							
							if($this->CsvQuoteFormat)
							{
								$TxtContent .= '"'.$Val.'",';		
							}
							else
							{ 
								$TxtContent .= $Val.Chr(9);		
							}
											
							$SheetContent .='<Cell><Data ss:Type="String">'.$Val.'</Data></Cell>'.$this->BrkPoint;
						}
						
						$TxtContent .= Chr(13);						
						$SheetContent .='</Row>'.$this->BrkPoint;			
					}
					
					if($this->TxtFormat)
						return $TxtContent;
						
					$SheetContent .='</Table>'.$this->BrkPoint;
				    $SheetContent .='<WorksheetOptions xmlns="urn:schemas-microsoft-com:office:excel">'.$this->BrkPoint;
					$SheetContent .='<Print>'.$this->BrkPoint;
					$SheetContent .='<ValidPrinterInfo/>'.$this->BrkPoint;
					$SheetContent .='<HorizontalResolution>600</HorizontalResolution>'.$this->BrkPoint;
					$SheetContent .='<VerticalResolution>0</VerticalResolution>'.$this->BrkPoint;
					$SheetContent .='</Print>'.$this->BrkPoint;
					$SheetContent .='<Panes>'.$this->BrkPoint;
					$SheetContent .='<Pane>'.$this->BrkPoint;
					$SheetContent .='<Number>3</Number>'.$this->BrkPoint;
					$SheetContent .='<ActiveRow>1</ActiveRow>'.$this->BrkPoint;
					$SheetContent .='<ActiveCol>1</ActiveCol>'.$this->BrkPoint;
					$SheetContent .='</Pane>'.$this->BrkPoint;
					$SheetContent .='</Panes>'.$this->BrkPoint;
					$SheetContent .='<ProtectObjects>False</ProtectObjects>'.$this->BrkPoint;
					$SheetContent .='<ProtectScenarios>False</ProtectScenarios>'.$this->BrkPoint;
				$SheetContent .='</WorksheetOptions>'.$this->BrkPoint;
			$SheetContent .='</Worksheet>'.$this->BrkPoint;
			
									
		}
		
		$StyleContent .='</Styles>'.$this->BrkPoint;
		return $StyleContent .$SheetContent;	
	}
	
	function GenerateExcel($filename,$Download=false)
	{
		if($this->DBHostName !="" && $this->DBUserName !="" && $this->DBDataBase !="")
			$this->DatabaseBackup();
		$this->CsvFileHander = fopen($filename, "w");
		
		$Content = '';
		$Content .= '<?phpxml version="1.0"?>'.$this->BrkPoint;
		$Content .= '<?phpmso-application progid="Excel.Sheet"?>'.$this->BrkPoint;
		$Content .= '<Workbook xmlns="urn:schemas-microsoft-com:office:spreadsheet" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet" xmlns:html="http://www.w3.org/TR/REC-html40">'.$this->BrkPoint;
		$Content .= $this->CreateHeader();
		$Content .=$this->CreateSheet();
		$Content .="</Workbook>";
		
		if($this->TxtFormat)
		$Content = $this->CreateSheet();
		
		@ob_clean();
		
		if($this->CsvFileFormat===true)
		{
			
		}
		else 
		{
			fputs($this->CsvFileHander,$Content);	
		}
		
		fclose($this->CsvFileHander);
		
		if($Download)
			$this->DownloadFile($filename);
		
	}
	
	function DatabaseBackup()
	{
		if(mysql_connect($this->DBHostName,$this->DBUserName,$this->DBPassword) or die("Connection close"))
		{
			if(!mysql_select_db($this->DBDataBase))
			{
					echo "Enable to connect with database";
					exit;
			}
			else 
			{
				if(count($this->QueryArray)==0)
				{
					$SNo =1;
					$TableQuery = @mysql_query('show tables');
		        	while ($tables = @mysql_fetch_array($TableQuery)) 
		        	{
		        		$this->QueryArray[$SNo] ="Select * from ".$tables[0]."";
		        		$SNo++;
				   	}
				}
				$this->QueryBuilder();           
			}
		
		}
		else 
		{
			echo "Enable to connect with mysql";
			exit;
		}	
		
	}
		
	function QueryBuilder()
	{
		foreach ($this->QueryArray as $Key=>$Value)
		{
			$RecordSet = @mysql_query($Value);
			$ColFields = @mysql_num_fields($RecordSet);
			$TableName = @mysql_field_table($RecordSet,0);
			$Int =1;
			$this->SheetArray[$Key]['Name'] =$TableName;
			$this->SheetArray[$Key]['Style'][$Int]['FontName'] ="Tahoma";
			$this->SheetArray[$Key]['Style'][$Int]['Color'] ="#000000";
			$this->SheetArray[$Key]['Style'][$Int]['Bold'] =true;
			$this->SheetArray[$Key]['Style'][$Int]['Size'] ="20";
			$this->SheetArray[$Key]['Style'][$Int]['Height'] ="30";
			for ($i = 0; $i < $ColFields; $i++) 
			{
    			$ColName = @mysql_field_name($RecordSet, $i);
    			$this->SheetArray[$Key]['Data'][1][$i+1] =$ColName;
    			
			}
			$Int++;			
			while ($Result = @mysql_fetch_array($RecordSet))
			{
				$this->SheetArray[$Key]['Style'][$Int]['FontName'] ="Tahoma";
				$this->SheetArray[$Key]['Style'][$Int]['Color'] ="#0000FF";
				$this->SheetArray[$Key]['Style'][$Int]['Bold'] =false;
				$this->SheetArray[$Key]['Style'][$Int]['Size'] ="12";
				$this->SheetArray[$Key]['Style'][$Int]['BgColor'] ="#efefef";
				for ($i = 0; $i < $ColFields; $i++) 
				{
					$this->SheetArray[$Key]['Data'][$Int][$i+1] =$Result[$i];
				}
				$Int++;
			}			
		}
	
	}
	
	function StripTags($Str)
	{
		$MyStr  = str_replace ( array('"', "'", '?','�','�','�','�',chr(13),chr(9)), array('&quot;','&apos;','&#63;','&quot;','&quot;','&apos;','-',' ',' '), $Str);
		$MyStr = strip_tags(htmlentities($MyStr));
		return $MyStr;
	}
	
	function DownloadFile($file)
	{
		if (!is_file($file)) 
   			{
   				 die("<b>404 File not found!</b>"); 
   			}

		
			  //Gather relevent info about file
			$len = filesize($file);
			$filename = basename($file);
			$file_extension = strtolower(substr(strrchr($filename,"."),1));

			//This will set the Content-Type to the appropriate setting for the file
			switch( $file_extension )
				 {
 				case "pdf": $ctype="application/pdf"; break;
					case "exe": $ctype="application/octet-stream"; break;
					case "zip": $ctype="application/zip"; break;
					case "doc": $ctype="application/msword"; break;
					case "xls": $ctype="application/vnd.ms-excel"; break;
					case "ppt": $ctype="application/vnd.ms-powerpoint"; break;
					case "gif": $ctype="image/gif"; break;
					case "png": $ctype="image/png"; break;
					case "jpeg":
					case "jpg": $ctype="image/jpg"; break;
					case "mp3": $ctype="audio/mpeg"; break;
					case "wav": $ctype="audio/x-wav"; break;
					case "mpeg":
					case "mpg":
					case "mpe": $ctype="video/mpeg"; break;
					case "mov": $ctype="video/quicktime"; break;	
					case "avi": $ctype="video/x-msvideo"; break;

					//The following are for extensions that shouldn't be downloaded (sensitive stuff, like php files)
					case "php":
					case "htm":
					case "html":
					case "txt":die("<b>Cannot be used for ". $file_extension ." files!</b>"); break; 

					default: $ctype="application/force-download";
				}
				@ob_clean();
				//Begin writing headers
				header("Pragma: public");
				header("Expires: 0");
				header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
				header("Cache-Control: public"); 
				header("Content-Description: File Transfer");

				//Use the switch-generated Content-Type
				header("Content-Type: $ctype");

				//Force the download
				$header="Content-Disposition: attachment; filename=".$filename.";";
				header($header );
				header("Content-Transfer-Encoding: binary");
				header("Content-Length: ".$len);
				@readfile($file);
				exit;
		}
	
};
?>
