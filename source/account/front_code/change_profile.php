<?php
	$CurrentUserObj = $CustomObj->CheckUserLogin(MakePageURL("index.php","Page=$Page"));
	$DataArray = array();
			
	$DataArray['FirstName']	= $CurrentUserObj->FirstName;
	$DataArray['LastName'] 	= $CurrentUserObj->LastName;
	$DataArray['Address1']	= $CurrentUserObj->Address1;
	$DataArray['Address2']	= $CurrentUserObj->Address2;
	$DataArray['City']		= $CurrentUserObj->City;
	$DataArray['State']		= $CurrentUserObj->State;
	$DataArray['Country']	= $CurrentUserObj->Country;
	$DataArray['Area']	= $CurrentUserObj->Area;
	$DataArray['ZipCode']	= $CurrentUserObj->ZipCode;
	$DataArray['Phone'] 	= $CurrentUserObj->Phone;
	
	
	if(isset($_POST['FirstName']))
	{
		$DataArray['FirstName']	= $_POST['FirstName'];
		$DataArray['LastName'] 	= $_POST['LastName'];
		$DataArray['Address1']	= $_POST['Address1'];
		$DataArray['Address2']	= $_POST['Address2'];
		$DataArray['City']		= $_POST['City'];
		$DataArray['State']		= $_POST['State'];
		$DataArray['Country']	= $_POST['Country'];
		$DataArray['Area']	= @$_POST['Area'];
		$DataArray['ZipCode']		= $_POST['ZipCode'];
		$DataArray['Phone'] 	= $_POST['Phone'];
		$ErrorArr = array();
		
		if(empty($DataArray['FirstName']))
			array_push($ErrorArr,"Please enter first name");
		
		if(empty($DataArray['Address1']))
			array_push($ErrorArr,"Please select Address1");
		
		if(empty($DataArray['City']))
			array_push($ErrorArr,"Please select City");
		
		if(empty($DataArray['State']))
			array_push($ErrorArr,"Please select State/County");
		if(empty($DataArray['ZipCode']))
			array_push($ErrorArr,"Please enter Post Code/Zip Code");
		
		
		
		if(count($ErrorArr) == 0)
		{
		
			$UserObj = new DataTable(TABLE_USERS);
			$UserObj->Where = "UserID = '".$UserObj->MysqlEscapeString($_SESSION['UserID'])."' AND Active = 1";
			$UserObj->TableUpdate($DataArray);
			@ob_clean();
			
			$_SESSION['InfoMessage'] = constant("DEFINE_ACCOUNT_INFO_UPDATED_MSG");
			MyRedirect(MakePageURL("index.php","Page=account/my_account"));			
			exit;
		}
		else 
		{
			@ob_clean();
			
			$_SESSION['ErrorMessage'] = implode($ErrorArr,"<br>");
			
		}
		
		
			
		
	}
	
?>