<?php
	$Target = isset($_REQUEST['Target'])?$_REQUEST['Target']:"";

	$ReferenceID = "102";
	extract(GetPageDetail($ReferenceID));

	$CustomObj->RedirectIfLogin(false);
	if(isset($_POST['UserName']) and $_POST['UserName']!='')
	{

		$_SESSION['UserName'] = $_POST['UserName'];
		$CustomObj->AuthenticateUser($_POST['UserName'],$_POST['Password']);
		$_SESSION['ErrorMessage'] = DEFINE_USER_LOGIN_INVALID_MSG;
		
		if($Target=="FromCheckout")
		 {
			MyRedirect(MakePageURL("index.php","Page=shop/shop_checkout"));
			exit;
		 }
		MyRedirect(MakePageURL("index.php","Page=account/login"));
		exit;
	}
?>