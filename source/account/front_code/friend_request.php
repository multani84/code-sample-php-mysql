<?php
	$DataArray = array();
	$FriendRequestObj = new DataTable(TABLE_FRIEND_REQUESTS);
	
$Section = isset($_REQUEST['Section'])?$_REQUEST['Section']:"";
$Target = isset($_REQUEST['Target'])?$_REQUEST['Target']:"";
$ProductID = isset($_GET['ProductID'])?$_GET['ProductID']:0;

$ProductObj= new DataTable(TABLE_PRODUCT);
$ProductID = (isset($CurrentProduct->ProductID) && $CurrentProduct->ProductID !="")?$CurrentProduct->ProductID:$ProductID;
if($ProductID !="")
{
	$ProductObj->Where ="ProductID='".$ProductObj->MysqlEscapeString($ProductID)."'";
	$CurrentProduct = $ProductObj->TableSelectOne();
	
	$ProdCatObj = new DataTable(TABLE_CATEGORY. " c, ".TABLE_PRODUCT_RELATION. " ptc");
	$ProdCatObj->Where = "c.Active='1' and ptc.Relationtype='category' and ptc.RelationID=c.CategoryID and ptc.ProductID='".$ProdCatObj->MysqlEscapeString($ProductID)."'";
	$CurrentCategory = $ProdCatObj->TableSelectOne(array("c.CategoryID","CategoryName"),"Position ASC");
	
	$MessageBody ="Dear ".chr(13).chr(13)."I've found a product at  ".SITE_NAME.chr(13)." that I think you'll like. Please take a look:".chr(13).SKSEOURL($CurrentProduct->ProductID,"product").chr(13).chr(13)."Thanks!";

}
else 
{
	$MessageBody ="Dear ".chr(13).chr(13)."I 'd like to recommend the website ".chr(13).SITE_NAME.chr(13)." It's an online menswear retailers \"for men with better things to do at the weekend.....\""." and I think you 'll like it!".chr(13).chr(13)."Thanks!";
}

if(isset($_GET['Target']) && $_GET['Target'] =="SendRequest")
{
	//$Mail_Subject = "Please check this product at ".SITE_NAME.":".$CurrentProduct->ProductName;
	$Mail_Subject = "I 'd like to recommend the website ".SITE_NAME;
	$Mail_ToEmail = isset($_POST['FEmail'])?$_POST['FEmail']:"";
	$Mail_FromEmail = isset($_POST['Email'])?$_POST['Email']:"";
	$Mail_FromName = isset($_POST['Name'])?$_POST['Name']:SITE_NAME;
	$MessageBody =isset($_POST['MessageBody'])?stripslashes($_POST['MessageBody']):"";
	SendEmail($Mail_Subject,$Mail_ToEmail,$Mail_FromEmail,$Mail_FromName,$MessageBody);
	@ob_clean();
	$ThankMsg = "You have successfully sent email to your friend.";
	
	$DataArray = array();
	$DataArray['FromName'] = isset($_POST['Name'])?$_POST['Name']:"";
	$DataArray['FromEmail'] = isset($_POST['Email'])?$_POST['Email']:"";
	$DataArray['FriendName'] = isset($_POST['FName'])?$_POST['FName']:"";
	$DataArray['FriendEmail'] = isset($_POST['FEmail'])?$_POST['FEmail']:"";
	$DataArray['CreatedDate'] = date('Y-m-d H:i:s');
	if($DataArray['FriendEmail'] != $DataArray['FromEmail'])
	{
		$DataArray['ProductID'] = isset($CurrentProduct->ProductID)?$CurrentProduct->ProductID:0;
	}
	$FriendRequestObj->TableInsert($DataArray);
}

?>