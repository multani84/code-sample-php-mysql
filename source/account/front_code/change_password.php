<?php
$CurrentUserObj = $CustomObj->CheckUserLogin(MakePageURL("index.php","Page=$Page"));
if(!empty($_POST['Password']))
	{
		$UserFirstObj = new DataTable(TABLE_USERS);
		$UserObj = new DataTable(TABLE_USERS);
		$UserFirstObj->Where = "UserName = '".$UserFirstObj->MysqlEscapeString($CurrentUserObj->UserName)."' AND Active = 1";
		$FirstObj = $UserFirstObj->TableSelectOne(array("Password"));
		if(isset($FirstObj->Password) && $FirstObj->Password !="")
		{
			$TmpPwd = explode(":",$FirstObj->Password);
			$EncyptPassword = NeoCryptedPassword($_POST['OldPassword'],$TmpPwd[1]).":".$TmpPwd[1];
			if($EncyptPassword == $FirstObj->Password)
			{
				$UserObj->Where = "UserName = '".$CurrentUserObj->UserName."' AND Password = '".$EncyptPassword."' AND Active = 1";
				$UserObj->TableSelectOne(array("UserID"));
				$NumRows = $UserObj->GetNumRows();
			}
			
		}
		if(isset($NumRows) && $NumRows == 1)
		{
			if(!empty($_POST['Password']))
			{
				if($_POST['Password'] == $_POST['RPassword'])
				{
					$DataArray = array();
					$DataArray['Password'] = $_POST['Password'];
					$PassSalt = md5(uniqid(rand(),true));
					$DataArray['Password'] = NeoCryptedPassword($DataArray['Password'],$PassSalt).":".$PassSalt;
		
					$UserObj->TableUpdate($DataArray);
					$_SESSION['InfoMessage'] = "You have successfully changed your password.";
					MyRedirect(MakePageURL("index.php","Page=account/my_account"));			
					exit;
				}
				else 
				{
					$_SESSION['ErrorMessage'] = "Password does not match.";
					MyRedirect(MakePageURL("index.php","Page=$Page"));			
					exit;
				}
			}
			else 
			{
				$_SESSION['ErrorMessage'] = "Password cannot be left empty.";
				MyRedirect(MakePageURL("index.php","Page=$Page"));			
				exit;
			}
		}
		else 
		{
				$_SESSION['ErrorMessage'] = "Please enter correct old password.";
				MyRedirect(MakePageURL("index.php","Page=$Page"));			
				exit;
			
		}
	}
	
?>