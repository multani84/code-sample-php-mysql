<?php
	$ReferenceID = "104";
	extract(GetPageDetail($ReferenceID));
	
	$CustomObj->RedirectIfLogin(false);
	if(isset($_POST['Email']) and $_POST['Email']!='')
	{

		$_SESSION['Email'] = $_POST['Email'];
		$UserObj = new DataTable(TABLE_USERS);
		$UserObj->Where = "Email ='".$UserObj->MysqlEscapeString($_POST['Email'])."' and Active ='1'";
		$CurrentUserObj = $UserObj->TableSelectOne(array("UserID","Email","UserName","Password","FirstName","LastName"));
		if($UserObj->GetNumRows() == 1)
		{
			$DataArray = array();
			$DataArray['ForgetCode'] 	= md5(uniqid(rand(),true));	
			$UserObj->TableUpdate($DataArray);
			
			$DataArray['FirstName'] = $CurrentUserObj->FirstName;
			$DataArray['LastName'] = $CurrentUserObj->LastName;
			$DataArray['Email']    = $CurrentUserObj->Email;
			$DataArray['UserName'] = $CurrentUserObj->UserName;
			
			$DataArray['Mail_Subject'] =@constant('EMAIL_SUBJECT_USER_FORGET_PASSWORD');
			$DataArray['Mail_ToEmail'] = $DataArray['Email'];
			$CustomObj->SendMailByUsingTemplate("forgot_password.php",$DataArray);
			$_SESSION['InfoMessage'] = "We have sent your login details in your email address.";
			MyRedirect(MakePageURL("index.php","Page=account/login"));
			exit;
		}
		else 
		{
			$_SESSION['ErrorMessage'] = USERNAME_NOT_FOUND_MSG;
			MyRedirect(MakePageURL("index.php","Page=$Page"));
			exit;
		}
	}
?>