<?php
$ReferenceID = "103";
extract(GetPageDetail($ReferenceID));
$CustomObj->RedirectIfLogin(false);
											
	if(isset($_POST['UserName']))
	{

		$DataArray = array();
		$DataArray['UserName']	= isset($_POST['UserName'])?$_POST['UserName']:"";
		$DataArray['Email']		= isset($_POST['Email'])?$_POST['Email']:$DataArray['UserName'];
		$DataArray['Password']	= isset($_POST['Password'])?$_POST['Password']:"";
		$DataArray['FirstName']	= isset($_POST['FirstName'])?$_POST['FirstName']:"";
		$DataArray['LastName'] 	= isset($_POST['LastName'])?$_POST['LastName']:"";
		$DataArray['DOB'] 	= isset($_POST['DOB'])?$_POST['DOB']:"0000-00-00";
		$DataArray['Gender'] 	= isset($_POST['Gender'])?$_POST['Gender']:"";
		$DataArray['Goal'] 	= isset($_POST['Goal'])?$_POST['Goal']:"";
		$DataArray['Address1']	= isset($_POST['Address1'])?$_POST['Address1']:"";
		$DataArray['Address2']	= isset($_POST['Address2'])?$_POST['Address2']:"";
		$DataArray['City']		= isset($_POST['City'])?$_POST['City']:"";
		$DataArray['State']		= isset($_POST['State'])?$_POST['State']:"";
		$DataArray['Area']	= isset($_POST['Area'])?$_POST['Area']:"";
		$DataArray['Country']	= isset($_POST['Country'])?$_POST['Country']:"";
		$DataArray['ZipCode']		= isset($_POST['ZipCode'])?$_POST['ZipCode']:"";
		$DataArray['Phone'] 	= isset($_POST['Phone'])?$_POST['Phone']:"";
		$DataArray['Qualification'] 	= isset($_POST['Qualification'])?$_POST['Qualification']:"";
		$NewsLetter 	= isset($_POST['NewsLetter'])?$_POST['NewsLetter']:"";
		if(isset($_FILES['CertificateCopy']) && $_FILES['CertificateCopy'] !="")
		{
			$ArrayType = explode(".",$_FILES['CertificateCopy']['name']);
			$Type=$ArrayType[count($ArrayType)-1];
			$ImageName = uniqid($DataArray['FirstName']."_").".".$Type;				
			$OriginalImage =DIR_FS_SITE_UPLOADS_CERTIFACTECOPY.$ImageName;
			copy($_FILES['CertificateCopy']['tmp_name'],$OriginalImage);
			chmod($OriginalImage,0777);
			$DataArray['QualificationProof'] = $ImageName;
		}
		if(isset($_FILES['IdProof']) && $_FILES['IdProof'] !="")
		{
			$ArrayType = explode(".",$_FILES['IdProof']['name']);
			$Type=$ArrayType[count($ArrayType)-1];
			$ImageName = uniqid($DataArray['FirstName']."_").".".$Type;				
			$OriginalImage =DIR_FS_SITE_UPLOADS_IDPROOF.$ImageName;
			copy($_FILES['IdProof']['tmp_name'],$OriginalImage);
			chmod($OriginalImage,0777);
			$DataArray['IdProof'] = $ImageName;
		}

		$DataArray['AboutYou'] = isset($_POST['AboutYou'])?$_POST['AboutYou']:"";
		$DataArray['AccountCode'] 	= SkGetUniqueUserNo();
		$PassSalt = md5(uniqid(rand(),true));
		$DataArray['Password'] = NeoCryptedPassword($DataArray['Password'],$PassSalt).":".$PassSalt;
		
		$DataArray['ActivationCode'] 	= md5(uniqid(rand(),true));
		$DataArray['CreatedDate'] 	= date("YmdHis");
		$DataArray['DoctorRequest'] = 0;
		$DataArray['PartnerRequest'] = 1;
		
		if(constant("DEFINE_ACTIVATE_BY_EMAIL")=="1")
		{
			$DataArray['Active'] =0;
		}
		else 
		{
			$DataArray['Active'] =1;

			$DataArray['ActiveOnce'] ="1";
		}			
			
		$ErrorArr = array();
		if(($_SESSION['SecurityCode'] != $_POST['SecurityCode']) OR @$_POST['SecurityCode']== "")
		{
			array_push($ErrorArr,"Invalid Security Code ! Please try again");
		}
		
		if(empty($DataArray['UserName']))
			array_push($ErrorArr,"Please enter username");
		
		
		if(empty($DataArray['Password']))
			array_push($ErrorArr,"Please enter password");
		
		if(!ValidateEmail($DataArray['Email']))
			array_push($ErrorArr,"Invalid Email Address ! Please try user@domain.com");
		
		if(empty($DataArray['FirstName']))
			array_push($ErrorArr,"Please enter first name");
		
		//if(empty($DataArray['LastName']))
			//array_push($ErrorArr,"Please enter last name");
		
		if(empty($DataArray['Address1']))
			array_push($ErrorArr,"Please select Address1");
		if(empty($DataArray['City']))
			array_push($ErrorArr,"Please select City");
		if(empty($DataArray['State']))
			array_push($ErrorArr,"Please select State/County");
		if(empty($DataArray['ZipCode']))
			array_push($ErrorArr,"Please enter Post Code/Zip Code");
		
		
		if(count($ErrorArr) == 0)
		{
			$UserObj = new DataTable(TABLE_USERS);
			$UserObj->Where = "UserName = '".$UserObj->MysqlEscapeString($DataArray['UserName'])."'";
			$Obj = $UserObj->TableSelectOne(array("COUNT(*) As Status"));
			
			if($Obj->Status == 0)
			{
				$DataArray['UserID']  = SKRemoveSpecialChars(substr($DataArray['UserName'],0,5))."_".md5(uniqid(rand(), true));
				$UserObj->TableInsert($DataArray);

				/* News letter addition start */
				if($NewsLetter=="1")
				{
					$DataSubArray = array();
					$DataSubArray['UserID'] = $DataArray['UserID'];
					$DataSubArray['DisplayName'] = $DataArray['FirstName']." ".$DataArray['LastName'];
					$DataSubArray['Email'] = $DataArray['Email'];
					$DataSubArray['CreatedDate'] = $DataArray['CreatedDate'];
					$NewsletterUserObj = new DataTable(TABLE_USERS_NEWSLETTER);
					$NewsletterUserObj->Where ="Email='".$NewsletterUserObj->MysqlEscapeString($DataSubArray['Email'])."'";
					$CurrentNewsLetterObj = $NewsletterUserObj->TableSelectOne();
					if(isset($CurrentNewsLetterObj->Email) && $CurrentNewsLetterObj->Email !="")
						$NewsletterUserObj->TableUpdate($DataSubArray);
					else 
						$NewsletterUserObj->TableInsert($DataSubArray);
				}
					/* News letter addition end */
					
					/* UserTypeRelObj addition start */
					
					$DataSubArray = array();
					$DataSubArray['UserID'] = $DataArray['UserID'];
					$DataSubArray['UserTypeID'] = (int)(isset($DefaultUserType->UserTypeID)?$DefaultUserType->UserTypeID:"1");
					$DataSubArray['Discount'] = "0";
					$DataSubArray['CreatedDate'] = $DataArray['CreatedDate'];
					
					$UserTypeRelObj = new DataTable(TABLE_USER_TYPE_RELATION);
					$UserTypeRelObj->TableInsert($DataSubArray);
				
					/* UserTypeRelObj addition end */
				
				if($DataArray['Active'] =="0")
				{
					
					$CustomObj->SendMailByUsingTemplate("activate_user.php",$DataArray);
					MyRedirect(MakePageURL("index.php","Page=thank_you&Status=Activate"));			
					exit;
				}
				else 
				{

					$DataArray['Mail_Subject'] =@constant('DEFINE_EMAIL_SUBJECT_USER_SIGNUP');
					$DataArray['Mail_ToEmail'] =$DataArray['Email'];
					
					$DataArray['Password']	= isset($_POST['Password'])?$_POST['Password']:"";
					$CustomObj->SendMailByUsingTemplate("sign_up.php",$DataArray);
					@ob_clean();

					if($DataArray['PartnerRequest']=='1')
					{
						$DataArray['Mail_Subject'] ="Partner Request";
						$DataArray['Mail_ToEmail'] =DEFINE_ADMIN_EMAIL;
						
						$DataArray['Email']	= isset($_POST['Email'])?$_POST['Email']:"";
						$DataArray['UserTypeName']	= "Partner";
						$CustomObj->SendMailByUsingTemplate("request_notification.php",$DataArray);
						@ob_clean();
					}

					$_SESSION['UserID'] = $DataArray['UserID'];			
					
					if(isset($_SESSION['ReturnURL']) && $_SESSION['ReturnURL'] !="")
					{
						$ReturnURL = $_SESSION['ReturnURL'];
						$_SESSION['ReturnURL'] ="";
						unset($_SESSION['ReturnURL']);
						MyRedirect($ReturnURL);
						exit;
					}
					else 
					{
						MyRedirect(MakePageURL("index.php","Page=thank_you&Status=Registered"));			
						exit;
					}
					
				
				}
				
			}
			else 
			{
				@ob_clean();
				
				$_SESSION['ErrorMessage'] = DEFINE_USERNAME_ALREADY_EXIST_MSG;
			}
			
		}
		else 
		{
			@ob_clean();
			
			$_SESSION['ErrorMessage'] = implode($ErrorArr,"<br>");
			
		}
	}
?>