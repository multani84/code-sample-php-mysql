<!-- Heading  -->
<div class="row-fluid">
	<div class="span12">
		<h1>User Section</h1>
		<hr />
		<?php
		if(isset($CurrentUser->UserID) && $CurrentUser->UserID !="")
		{
			echo "<b>".$CurrentUser->FirstName." ".$CurrentUser->LastName."(".$CurrentUser->UserName.")</b>";
	
		}
		?>
	</div>
	<div>
	</div>
<div class="clearfix separator bottom"></div>
<!--  End Heading-->	

<?php
if(isset($CurrentUser->UserID) && $CurrentUser->UserID !="")
{
?>
<table border="0" cellpadding="3" cellspacing="1" width="100%" class="InsideTable">
	<tr>
		<td align="center">
		<div>
		  	<ul class="nav nav-tabs">
		  			<li <?php echo ('EditUser'==$Section)?'class="active"':''?>><a href="<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php echo $Page?>&Section=EditUser&UserID=<?php echo $UserID?>&UserTypeID=<?php echo $UserTypeID?>">Profile</a></li>
		  			<li <?php echo ('ChangePassword'==$Section)?'class="active"':''?>><a href="<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php echo $Page?>&Section=ChangePassword&UserID=<?php echo $UserID?>&UserTypeID=<?php echo $UserTypeID?>">Change Password</a></li>
		  			<li <?php echo ('UserAccount'==$Section)?'class="active"':''?>><a href="<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php echo $Page?>&Section=UserAccount&UserID=<?php echo $UserID?>&UserTypeID=<?php echo $UserTypeID?>">User Account/Discount</a></li>
		  			<li <?php echo ('OrderHistory'==$Section)?'class="active"':''?>><a href="<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php echo $Page?>&Section=OrderHistory&UserID=<?php echo $UserID?>&UserTypeID=<?php echo $UserTypeID?>">Order History</a></li>
		  	</ul>
		</div>
		</td>
	</tr>
</table>
<?php
}?>
<?php
switch($Section)
{

	case "UserAccount" :
	
		$TableName = TABLE_USER_TYPE_RELATION." r 
					 LEFT JOIN ".TABLE_USER_TYPE." ut on (ut.UserTypeID = r.UserTypeID)";
					 //										 WHERE r.UserID = '".$UserID."' ORDER BY SortOrder DESC";
					 
		$UserObj = new DataTable($TableName);
		//$UserObj->DisplayQuery = true;
		$UserObj->Where = "r.UserID = '".$UserObj->MysqlEscapeString($UserID)."' AND ut.IsDefault != '1'";
		$UserObj->TableSelectAll(array("*"),"r.Discount DESC,ut.SortOrder DESC");
		$Total = $UserObj->GetNumRows();
		$UserTypeArray = array();
		?>
		<h3><?php echo $CurrentUser->UserName?></h3>						
		<form class="form-horizontal" method="POST" id="FrmUser" name="FrmUser" action="<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php echo $Page?>&Section=<?php echo $Section?>&UserTypeID=<?php echo $UserTypeID?>&Target=AccountUpdate&UserID=<?php echo $UserID?>" enctype="multipart/form-data" onsubmit="return ValidateForm('FrmUser');">
		<?php
		if($Total > 0)
		{
		?>
		<?php 
		while($UerType = $UserObj->GetObjectFromRecord()):
		 $UserTypeArray[] = $UerType->UserTypeID;
		?>
		<div class="well">
				<div class="form-group">
					<label for="email" class="col-sm-4 col-md-5 col-lg-4 control-label">Username: </label>
					<div class="col-sm-20 col-md-19 col-lg-20 text-left">
						<?php echo $CurrentUser->UserName?>
					</div>
				</div>
				<div class="form-group">
					<label for="email" class="col-sm-4 col-md-5 col-lg-4 control-label">User Type: </label>
					<div class="col-sm-20 col-md-19 col-lg-20 text-left">
						<?php echo $UerType->UserType?>
					</div>
				</div>
				<div class="form-group">
					<label for="email" class="col-sm-4 col-md-5 col-lg-4 control-label">Discount:</label>
					<div class="col-sm-20 col-md-19 col-lg-20 text-left">
						<input class="form-control"  name="Discount[<?php echo $UerType->RelationID?>]" id="Discount_<?php echo $UerType->RelationID?>" value="<?php echo $UerType->Discount?>" title="" placeholder="Discount" type="text" />
					</div>
					<a href="<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php echo $Page?>&Section=<?php echo $Section?>&UserTypeID=<?php echo $UserTypeID?>&Target=AccountDelete&UserID=<?php echo $UserID?>&RelationID=<?php echo $UerType->RelationID?>" class="btn btn-danger btn-phone-block" onclick="return confirm('Are you sure you want to remove this user type')"><icon class="icon-trash icon-white"></icon>DELETE</a>
				</div>
			</div>
		<?php endwhile;?>	
		<?php }?>
			<?php 
			 $UserTypeObj = new DataTable(TABLE_USER_TYPE);
			 $UserTypeObj->Where ="IsDefault != '1'";
			 if(is_array( $UserTypeArray) && count( $UserTypeArray) > 0)
			 {
				 $UserTypeArray = array_filter($UserTypeArray, 'ctype_digit');
				$UserTypeObj->Where .=" AND UserTypeID NOT IN(".implode(",",$UserTypeArray).")";
			 }
			 $UserTypeObj->TableSelectAll(array("*"),"SortOrder DESC");
			 if($UserTypeObj->GetNumRows() > 0):
			 ?>
			 <div class="well">
				<h3>Add User Type for <?php echo $CurrentUser->UserName?></h3>
			<?php
			 while($CurrentUerType =  $UserTypeObj->GetObjectFromRecord()): 
			 ?>
				<div class="form-group">
					<label for="email" class="col-sm-4 col-md-5 col-lg-4 control-label"><b><?php echo $CurrentUerType->UserType?></b>
					<input class="form-control"  name="CheckDiscount[<?php echo $CurrentUerType->UserTypeID?>]" id="CheckDiscount_<?php echo $CurrentUerType->UserTypeID?>" value="1" type="checkbox" />
					</label>
					<div class="col-sm-20 col-md-19 col-lg-20 text-left">
						Discount: <input class="form-control"  name="AddDiscount[<?php echo $CurrentUerType->UserTypeID?>]" id="AddDiscount_<?php echo $CurrentUerType->UserTypeID?>" value="" title="" placeholder="Discount" type="text" />
					</div>
				</div>
			<?php endwhile;?>	
			<?php endif?>	
		 	</div>
		 	<div class="form-group">
					<div class="col-sm-6 pull-right">
					<button class="btn btn-warning  btn-block" type="submit">Submit</button>
				</div>
			</div>
		</form>
			
	<?php
		
	break;
	case "LoyaltyPoints" :
		$LoyalityPointArr = GetLoyalityPoint($CurrentUser->UserID);
		?>
		<div style="padding-top:20px;display:inline-block;width:100%;">
		<table width="100%"  border="0" align="left" cellpadding="3" cellspacing="1" class="InsideTable">
			<tr>
				<th colspan="2" align="left" class="InsideLeftTd"><b>Product loyalty Points</b></font></th>				
			</tr>
			<tr>
				<td width="40%" class="InsideRightTd"><b>Total Points</b></td>
				<td align="left" class="InsideRightTd"><b><?php echo $LoyalityPointArr['Total']?></b></td>
			</tr>
			<tr>
				<td class="InsideRightTd"><b>Accessed Points</b></td>
				<td align="left" class="InsideRightTd"><b><?php echo isset($LoyalityPointArr['AccessPoint'])?$LoyalityPointArr['AccessPoint']:0?></b></td>
			</tr>
			<tr>
				<td class="InsideRightTd"><b>Left Points</b></td>
			 	<td align="left" class="InsideRightTd"><b><?php echo isset($LoyalityPointArr['LeftPoint'])?$LoyalityPointArr['LeftPoint']:0?></b></td>
			</tr>
		</table>
		</div>
		<div style="margin-top:10px;">
			<br>
			<form method="POST" id="FrmUser" name="FrmUser" action="<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php echo $Page?>&Section=<?php echo $Section?>&UserTypeID=<?php echo $UserTypeID?>&Target=PointsUpdate&UserID=<?php echo $UserID?>" enctype="multipart/form-data" onsubmit="return ValidateForm('FrmUser');">
			<table width="100%"  border="0" align="left" cellpadding="3" cellspacing="1" class="InsideTable">
				<tr>
					<td class="InsideLeftTd" width="35%" align="left"><b>Add/Subtract User Points</b></td>
					<td align="left" width="20%">
					<input type="text" name="Points" value="">
					</td>
					<td align="left">
						<select name="PointStatus">
							<option value="<?php echo $PointStatusArray['Get']?>">Add</option>
							<option value="<?php echo $PointStatusArray['Access']?>">Subtract</option>
						</select>
					</td>
					<td><input type="submit" value="Submit"></td>
				</tr>
			</table>
			<br>
			</form>
		</div>
		<?php
		//$SNo=($UserPointObj->PageSize*($UserPointObj->PageNo-1)) +1;
		//$UserPointObj->DisplayQuery = true;
		$SNo=1;
		$OrderBy = isset($_GET['OrderBy'])?$_GET['OrderBy']:"CreatedDate DESC";
		$UserPointObj->TableName =TABLE_USERS_POINT." up";
		$UserPointObj->Where ="up.UserID ='".$CurrentUser->UserID."' AND up.OrderStatus='".$PaymentStatusArray['Paid']."'";
		$UserPointObj->TableSelectAll(array("*,DATE_FORMAT(CreatedDate,'%M %d, %Y')as MyCreatedDate,(Select OrderNo from ".TABLE_ORDERS." Where OrderID = up.OrderID) as OrderNo"),$OrderBy);
		if($UserPointObj->GetNumRows())
		{
			?>
			<div align="left" style="margin-top:10px;">
				<br>
				<table width="100%"  border="0" align="left" cellpadding="3" cellspacing="1" class="InsideTable">
					<tr class="InsideLeftTd">
						<td width="10%" height="25" align="center"><b>S.No</b></td>
						<td width="20%" align="left"><b>Order No</b></td>
						<td width="20%" align="left"><b>Points</b>
							<br><a href="index.php?Page=<?php echo $Page?>&Section=<?php echo $Section?>&UserTypeID=<?php echo $UserTypeID?>&UserID=<?php echo $UserID?>&OrderBy=Points ASC" class="adm-link">ASC</a>&nbsp;|&nbsp;
							<a href="index.php?Page=<?php echo $Page?>&Section=<?php echo $Section?>&UserTypeID=<?php echo $UserTypeID?>&UserID=<?php echo $UserID?>&OrderBy=Points DESC" class="adm-link">DESC</a>
						</td>
						<td width="20%" align="left"><b>Point Status</b>
							<br><a href="index.php?Page=<?php echo $Page?>&Section=<?php echo $Section?>&UserTypeID=<?php echo $UserTypeID?>&UserID=<?php echo $UserID?>&OrderBy=PointStatus ASC" class="adm-link">ASC</a>&nbsp;|&nbsp;
							<a href="index.php?Page=<?php echo $Page?>&Section=<?php echo $Section?>&UserTypeID=<?php echo $UserTypeID?>&UserID=<?php echo $UserID?>&OrderBy=PointStatus DESC" class="adm-link">DESC</a>
						</td>
						<td align="left"><b>Date</b>
							<br><a href="index.php?Page=<?php echo $Page?>&Section=<?php echo $Section?>&UserTypeID=<?php echo $UserTypeID?>&UserID=<?php echo $UserID?>&OrderBy=CreatedDate ASC" class="adm-link">ASC</a>&nbsp;|&nbsp;
							<a href="index.php?Page=<?php echo $Page?>&Section=<?php echo $Section?>&UserTypeID=<?php echo $UserTypeID?>&UserID=<?php echo $UserID?>&OrderBy=CreatedDate DESC" class="adm-link">DESC</a>
						</td>
					</tr>
					<?php
					while ($CurrentPoint = $UserPointObj->GetObjectFromRecord())
					{
						?>
						<tr  class="InsideRightTd">
							<td align="center"><?php echo $SNo?>.</td>
							<td align="left"><?php
							if($CurrentPoint->OrderNo =="")
								echo "By Admin";
							else 
								echo '<a href ="index.php?Page=shop/display_order&Section=FullDetail&OrderID='.$CurrentPoint->OrderID.'">'.$CurrentPoint->OrderNo.'</a>';	
							
							?></td>
							<td align="left"><?php echo $CurrentPoint->Points?></td>
							<td align="left"><?php echo $CurrentPoint->PointStatus?></td>
							<td align="left"><?php echo $CurrentPoint->MyCreatedDate?></td>
						</tr>
						<?php
						$SNo++;
					}
					?>
				</table>
			</div>
			<?php
		}
		?>
		<?php
	break;
	case "WishList" :
		?>
		<?php
		$ProductObj = new DataTable(TABLE_PRODUCT. " p, ".TABLE_USERS_WISHLIST. " uw");
		$ProductObj->Where = "uw.ProductID=p.ProductID and uw.UserID='".$ProductObj->MysqlEscapeString($CurrentUser->UserID)."'";
		$ProductObj->TableSelectAll("","Position ASC");
		if($ProductObj->GetNumRows() > 0)
		{
		?>			
		<table cellpadding="0" cellspacing="0" width="100%" border="0">
		<tr>
		<td align="center">
			 <table border="0" cellpadding="3" cellspacing="1" width="100%" class="InsideTable">
				<tr class="InsideLeftTd">
					<td align="center" width="5%"><b>S.No.</b></td>
					<td align="left"><b>Product Name</b>
						</td>
						<td align="center"><b>Stock</b>
						</td>
						<td align="center"><b>Product Loyality Points</b>
						</td>
				
				</tr>
				<?php
				$SNo=1;
				$Count=1;
				
				while($CurrentProduct = $ProductObj->GetObjectFromRecord())
				{
					$StArr = GetProductPriceNStock($CurrentProduct->ProductID,1);
				?>
				<tr>
					<td align="center"><b><?php echo $SNo?></b></td>
					<td align="left"><b><?php echo MyStripSlashes($CurrentProduct->ProductName);?></b>
					<br><?php echo MyStripSlashes($CurrentProduct->ModelNo);?>
					<br><?php echo isset($StArr['Price'])?number_format($StArr['Price'],2):"";?>
					<br>
					<a href="<?php echo SKSEOURL($CurrentProduct->ProductID,"product")?>" target="_blank">Preview</a>&nbsp;|&nbsp;
					<a href="<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=shop/product&Section=EditProduct&ProductID=<?php echo $CurrentProduct->ProductID?>">Edit</a>
					<input type="hidden" name="ProductID_<?php echo $Count?>" value="<?php echo $CurrentProduct->ProductID?>"></td>
					<td align="center"><?php echo $CurrentProduct->Stock?></td>
					<td align="center"><?php echo $CurrentProduct->ProductPoints?></td>
				</tr>
				<tr>
					<td align="center" colspan="4"><hr style="height:2px;color:#000;background-color:#000;"></td>
				</tr>
				<?php
				$SNo++;
				$Count++;
				}
			?>
			</table>
		 </td>
		 </tr>
		 </table>
		<?php
		}
		else
		{
			echo "There are no products in wishlist.";
		}
		?>		
		<?php
	break;
	case "OrderHistory" :
	$PaymentStatus =isset($_GET['PaymentStatus'])?$_GET['PaymentStatus']:"";
	$ShippingStatus = isset($_GET['ShippingStatus'])?$_GET['ShippingStatus']:"";
	$PaymentMethod=isset($_GET['PaymentMethod'])?$_GET['PaymentMethod']:"";
	$OrderBy = isset($_GET['OrderBy'])?$_GET['OrderBy']:"CreatedDate DESC";
	
	$OrderObj = new DataTable(TABLE_ORDERS);	
	$OrderObj->Where ="UserID ='$UserID'";
	
	$OrderObj->TableSelectAll(array("*, DATE_FORMAT(CreatedDate,'%b %d, %Y')as MyCreatedDate, DATE_FORMAT(CreatedDate,'%M, %Y')as MyCreatedMonth"),$OrderBy);
	
	?>
		<?php
		if($OrderObj->GetNumRows() >0)
		{
		?>
		<table class="table table-striped table-bordered table-responsive block">
			<tr>
				<td width="96%" align="left" height="30">
				<div style="float:left;"><b>Status:&nbsp;</b></div>
				<?php
				foreach ($OrderStatusArray as $key=>$val)
				{?>
				<div style="float:left;width:16px;height:16px;background-color:<?php echo $OrderStatusColorArray[$key]?>">&nbsp;</div>
				<div style="float:left;height:16px;">&nbsp;<b><?php echo $val?></b>&nbsp;</div>
				<?php
				}?>
				</td>
			</tr>
		</table>					
		<br>
		<table class="table table-bordered table-responsive block">
			<thead>
			<tr class="InsideLeftTd" >
				<td width="2%" height="25"><b>S.No</b></td>
				<td width="10%" align="center"><b>Order No</b></td>
				<td width="12%" align="center"><b>Date</b>
				</td>
				<td width="18%"><b>Customer Name</b>
				</td>
				<td ><b>Member Details</b></td>
				<td width="10%"><b>Payment Status</b>
				</td>
				<td width="15%"><b>Order Status</b>
				</td>
				<td  class="header"  width="12%" align="center"><b>Amount</b>
				</td>
				<td class="header"  width="10%" align="right"><b>Email</b></td>
			</tr>
			</thead>
			<tbody>
		<?php
		
			$SNo=1;
			$Count=1;
			$CurrentMonth="";
			$MonthAmount ="";
			$OrderObj1 = new DataTable(TABLE_ORDERS);	
		while ($CurrentOrder = $OrderObj->GetObjectFromRecord())
		{
			$CurrencyOrderID = $CurrentOrder->OrderID;
			?>
		<tr  class="InsideRightTd" style="background-color:<?php echo @$OrderStatusColorArray[$CurrentOrder->OrderStatus]?>">
			<td height="25" align="center"><?php echo $SNo?>.</td>
			<td align="center"><a href ="index.php?Page=shop/display_order&Section=FullDetail&OrderID=<?php echo $CurrentOrder->OrderID?>"><label class="label label-info"><icon class="icon-eye-open icon-white"></icon> <?php echo $CurrentOrder->OrderNo?></label></a></td>
			<td><?php echo $CurrentOrder->MyCreatedDate?></td>
			<td><?php echo MyStripSlashes($CurrentOrder->BillingFirstName)?>&nbsp;<?php echo MyStripSlashes($CurrentOrder->BillingLastName)?></td>
			<td>
				<?php echo MyStripSlashes($CurrentOrder->BillingAddress1)."&nbsp;&nbsp;".MyStripSlashes($CurrentOrder->BillingAddress2)?><br>
				<?php echo MyStripSlashes($CurrentOrder->BillingCity)."&nbsp;&nbsp;".MyStripSlashes($CurrentOrder->BillingState)?><br>
				<?php echo MyStripSlashes($CurrentOrder->BillingArea)."&nbsp;&nbsp;".MyStripSlashes($CurrentOrder->BillingCountry)."&nbsp;&nbsp;".MyStripSlashes($CurrentOrder->BillingZip)?><br>
				T: <?php echo MyStripSlashes($CurrentOrder->BillingPhone)?>
				</td>
			<td><?php echo MyStripSlashes($CurrentOrder->PaymentStatus)?></td>	
			<td><?php echo MyStripSlashes($CurrentOrder->OrderStatus)?></td>	
			<td align="right"><?php echo Change2CurrentCurrency($CurrentOrder->GrandTotal)?></td>
			<td align="right"><a href="mailto:<?php echo MyStripSlashes($CurrentOrder->Email)?>" class="btn btn-success btn-phone-block"><icon class="icon-envelope icon-white"></icon> Send Email</a></td>
		</tr>
	<?php
			$SNo++;
			$Count++;
			}			
			?>	
		</tbody>			
		</table>
		</form>
	<?php
		}
		else 
		{
			?>
			<br>
			<table cellpadding="1" width="100%" cellspacing="1" border="0" class="InsideTable">
				<tr class="InsideLeftTd">
				<td width="100%" align="center">
				<b>No Record Found.</b></td>	
				</tr>
			</table>
			<br>	
			<?php
		}
	break;
	case "ChangePassword" :
	?>
	<form class="form-horizontal" method="POST" id="FrmUser" name="FrmUser" action="<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php echo $Page?>&Section=<?php echo $Section?>&UserTypeID=<?php echo $UserTypeID?>&Target=PasswordUpdate&UserID=<?php echo $UserID?>" enctype="multipart/form-data" onsubmit="return ValidateForm('FrmUser');">
						<div class="well">
								<h2>Change Password</h2>
								<div class="form-group">
									<label for="email" class="col-sm-4 col-md-5 col-lg-4 control-label">Username: </label>
									<div class="col-sm-20 col-md-19 col-lg-20 text-left">
										<?php echo $CurrentUser->UserName?>
									</div>
								</div>
								<div class="form-group">
									<label for="email" class="col-sm-4 col-md-5 col-lg-4 control-label">New Password: *</label>
									<div class="col-sm-20 col-md-19 col-lg-20 text-left">
										<input class="form-control"  name="Password" id="Password_R" value="" title="" placeholder="New Password" type="password" value="" required />
									</div>
								</div>
								
								<div class="form-group">
									<label for="email" class="col-sm-4 col-md-5 col-lg-4 control-label">Retype Password: *</label>
									<div class="col-sm-20 col-md-19 col-lg-20 text-left">
										<input class="form-control"  name="RPassword" id="RPassword_R" value="" title="" placeholder="Retype Password" type="password" value="" required />
									</div>
								</div>
								
								<div class="form-group">
									<div class="col-sm-6 pull-right">
										<button class="btn btn-warning  btn-block" type="submit">Submit</button>
									</div>
								</div>
								
							</div>
							</form>
							
	<?php
	break;
	case "AddUser":
	case "EditUser" :
	?>
	
	<form class="form-horizontal" role="form" method="POST" id="FrmUser" name="FrmUser" action="<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php echo $Page?>&Section=<?php echo $Section?>&UserTypeID=<?php echo $UserTypeID?>&Target=AddUser&UserID=<?php echo $UserID?>" enctype="multipart/form-data" onsubmit="return ValidateForm('FrmUser');">
			<div class="well">
			<h2>User Details</h2>
			
			<div class="form-group">
				<label for="email" class="col-sm-4 col-md-5 col-lg-4 control-label">Email:</label>
				<div class="col-sm-20 col-md-19 col-lg-20 text-left">
				<?php
				if($UserID=="")
				{
				?>
					<input name="UserName" id="UserName_E" title="Invalid Email Address ! Please try user@domain.com" type="text" size="40" value="<?php echo isset($_POST['UserName'])?MyStripSlashes($_POST['UserName']):(isset($CurrentUser->UserName)?MyStripSlashes($CurrentUser->UserName):"")?>"  />
				<?php
				}
				else 
				{
					echo (isset($CurrentUser->UserName)?MyStripSlashes($CurrentUser->UserName):"");
					//echo (isset($CurrentUser->AccountCode)?"<br><br>AccountCode:".MyStripSlashes($CurrentUser->AccountCode):"");
				}?>
				</div>
			</div>
			
			<div class="form-group">
				<label for="email" class="col-sm-4 col-md-5 col-lg-4 control-label">Name: *</label>
				<div class="col-sm-20 col-md-19 col-lg-20 text-left">
					<input class="form-control"  name="FirstName" id="FirstName_R" value="<?php echo isset($_POST['FirstName'])?MyStripSlashes($_POST['FirstName']):(isset($CurrentUser->FirstName)?MyStripSlashes($CurrentUser->FirstName):"")?>" title="" placeholder="Name" type="text" value="" required />
				</div>
			</div>
			
			<div class="form-group">
				<label for="email" class="col-sm-4 col-md-5 col-lg-4 control-label">Contact Phone: *</label>
				<div class="col-sm-20 col-md-19 col-lg-20 text-left">
					<input class="form-control"  name="Phone" id="Phone_R" value="<?php echo isset($_POST['Phone'])?MyStripSlashes($_POST['Phone']):(isset($CurrentUser->Phone)?MyStripSlashes($CurrentUser->Phone):"")?>" title="" placeholder="Phone" type="text" value="" required />
				</div>
			</div>
			
			<div class="form-group">
				<label for="email" class="col-sm-4 col-md-5 col-lg-4 control-label">Qualification: *</label>
				<div class="col-sm-20 col-md-19 col-lg-20 text-left">
					<input class="form-control"  name="Qualification" id="Qualification" value="<?php echo isset($_POST['Qualification'])?MyStripSlashes($_POST['Qualification']):(isset($CurrentUser->Qualification)?MyStripSlashes($CurrentUser->Qualification):"")?>" title="" placeholder="Qualification" type="text" value=""  />
				</div>
			</div>

			<div class="form-group">
				<label for="email" class="col-sm-4 col-md-5 col-lg-4 control-label">Qualification Proof: *</label>
				<div class="col-sm-20 col-md-19 col-lg-20 text-left">
					<?php  
						if(isset($CurrentUser->QualificationProof) && $CurrentUser->QualificationProof !="" && file_exists(DIR_FS_SITE_UPLOADS_CERTIFACTECOPY.$CurrentUser->QualificationProof))
						{?>
							<div>
								<a href="<?php  echo DIR_WS_SITE_UPLOADS_CERTIFACTECOPY.$CurrentUser->QualificationProof?>" target="_blank">
									<?php  SKImgDisplay(DIR_FS_SITE_UPLOADS_CERTIFACTECOPY.$CurrentUser->QualificationProof,100,'&nbsp;');?>
									<br>
									View Enlarge
								</a>
							</div>
					<?php  
					}?>

				</div>
			</div>

			<div class="form-group">
				<label for="email" class="col-sm-4 col-md-5 col-lg-4 control-label">Id Proof: *</label>
				<div class="col-sm-20 col-md-19 col-lg-20 text-left">
					<?php  
						if(isset($CurrentUser->IdProof) && $CurrentUser->IdProof !="" && file_exists(DIR_FS_SITE_UPLOADS_IDPROOF.$CurrentUser->IdProof))
						{?>
							<div>
								<a href="<?php  echo DIR_WS_SITE_UPLOADS_IDPROOF.$CurrentUser->IdProof?>" target="_blank">
									<?php  SKImgDisplay(DIR_FS_SITE_UPLOADS_IDPROOF.$CurrentUser->IdProof,100,'&nbsp;');?>
									<br>
									View Enlarge
								</a>
							</div>
					<?php  
					}?>
					
				</div>
			</div>

			<div class="form-group">
				<label for="email" class="col-sm-4 col-md-5 col-lg-4 control-label">AboutYou : *</label>
				<div class="col-sm-20 col-md-19 col-lg-20 text-left">
					<textarea class="form-control"  name="AboutYou" id="AboutYou" title="" placeholder="AboutYou " type="text"  /><?php echo isset($_POST['AboutYou'])?MyStripSlashes($_POST['AboutYou']):(isset($CurrentUser->AboutYou )?MyStripSlashes($CurrentUser->AboutYou):"")?></textarea>
				</div>
			</div>

			<div class="form-group">
				<label for="email" class="col-sm-4 col-md-5 col-lg-4 control-label">Address1: *</label>
				<div class="col-sm-20 col-md-19 col-lg-20 text-left">
					<input class="form-control"  name="Address1" id="Address1_R" value="<?php echo isset($_POST['Address1'])?MyStripSlashes($_POST['Address1']):(isset($CurrentUser->Address1)?MyStripSlashes($CurrentUser->Address1):"")?>" title="" placeholder="Address1" type="text" value="" required />
				</div>
			</div>
			
			<div class="form-group">
				<label for="email" class="col-sm-4 col-md-5 col-lg-4 control-label">Address2: </label>
				<div class="col-sm-20 col-md-19 col-lg-20 text-left">
					<input class="form-control"  name="Address2" id="Address2" value="<?php echo isset($_POST['Address2'])?MyStripSlashes($_POST['Address2']):(isset($CurrentUser->Address2)?MyStripSlashes($CurrentUser->Address2):"")?>" title="" placeholder="Address2" type="text" value="" />
				</div>
			</div>
			
			<div class="form-group">
				<label for="email" class="col-sm-4 col-md-5 col-lg-4 control-label">Town/City: *</label>
				<div class="col-sm-20 col-md-19 col-lg-20 text-left">
					<input class="form-control"  name="City" id="City_R" value="<?php echo isset($_POST['City'])?MyStripSlashes($_POST['City']):(isset($CurrentUser->City)?MyStripSlashes($CurrentUser->City):"")?>" title="" placeholder="City" type="text" value="" required />
				</div>
			</div>
			
			<div class="form-group">
				<label for="email" class="col-sm-4 col-md-5 col-lg-4 control-label">County/State: *</label>
				<div class="col-sm-20 col-md-19 col-lg-20 text-left"
				data-input='<input type="text" name="State" value="<?php echo isset($_POST['State'])?MyStripSlashes($_POST['State']):(isset($CurrentUser->State)?MyStripSlashes($CurrentUser->State):"")?>"" id="State_R" title="Please enter County." class="form-control">'
				data-select='<select data-val="<?php echo isset($_POST['State'])?MyStripSlashes($_POST['State']):(isset($CurrentUser->State)?MyStripSlashes($CurrentUser->State):"")?>"" name="State" id="State_R" title="Please select State" required></select>'
				>
						<input  class="form-control"  type="text" name="State" value="<?php echo isset($_POST['State'])?MyStripSlashes($_POST['State']):(isset($CurrentUser->State)?MyStripSlashes($CurrentUser->State):"")?>"" id="State_R" title="Please enter State." placeholder="State" required />
				</div>
			</div>
			
			<div class="form-group">
				<label for="email" class="col-sm-4 col-md-5 col-lg-4 control-label">Postal/Zip Code: *</label>
				<div class="col-sm-20 col-md-19 col-lg-20 text-left">
					<input class="form-control"  name="ZipCode" id="ZipCode_R" value="<?php echo isset($_POST['ZipCode'])?MyStripSlashes($_POST['ZipCode']):(isset($CurrentUser->ZipCode)?MyStripSlashes($CurrentUser->ZipCode):"")?>" title="" placeholder="Zip Code" type="text" value="" required />
				</div>
			</div>
			
			<div class="form-group">
				<label for="Country_R" class="col-sm-4 col-md-5 col-lg-4 control-label">Country: *</label>
				<div class="col-sm-20 col-md-19 col-lg-20 text-left">
						<select rel="sk_country" data-regionid="#State_R" data-value="<?php echo isset($_POST['Country'])?MyStripSlashes($_POST['Country']):(isset($CurrentUser->Country)?MyStripSlashes($CurrentUser->Country):DEFINE_DEFAULT_COUNTRY)?>" name="Country" id="Country_R" title="Please select Country."></select>
				</div>
			</div>
			
			<div class="form-group">
				<label for="email" class="col-sm-4 col-md-5 col-lg-4 control-label">Status: </label>
				<div class="col-sm-20 col-md-19 col-lg-20 text-left">
					<input type="checkbox" name="Active" value="1" class="chk" <?php echo (isset($_POST['Active']) && $_POST['Active']==1)?"checked":((isset($CurrentUser->Active) && $CurrentUser->Active==1)?"checked":"")?> >&nbsp;<b>Active</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				</div>
			</div>
			
			<div class="form-group">
				<label for="email" class="col-sm-4 col-md-5 col-lg-4 control-label">Partner Code: *</label>
				<div class="col-sm-20 col-md-19 col-lg-20 text-left">
					<input class="form-control"  name="PartnerCode" id="PartnerCode" value="<?php echo isset($_POST['PartnerCode'])?MyStripSlashes($_POST['PartnerCode']):(isset($CurrentUser->PartnerCode)?MyStripSlashes($CurrentUser->PartnerCode):"")?>" title="" placeholder="Partner Code" type="text" value="" />
				</div>
			</div>

			<div class="form-group">
				<label for="email" class="col-sm-4 col-md-5 col-lg-4 control-label">Reference Code: *</label>
				<div class="col-sm-20 col-md-19 col-lg-20 text-left">
					<input class="form-control"  name="ReferenceCode" id="ReferenceCode" value="<?php echo isset($_POST['ReferenceCode'])?MyStripSlashes($_POST['ReferenceCode']):(isset($CurrentUser->ReferenceCode)?MyStripSlashes($CurrentUser->ReferenceCode):"")?>" title="" placeholder="Partner Code" type="text" value="" />
				</div>
			</div>
			
		</div>
		
		<div class="col-sm-24">
		<div class="row">
			<div class="col-sm-18"></div>
			<div class="col-sm-6 pull-right"><button type="submit" class="btn btn-warning  btn-block">Submit</button></div>
		</div>
	</div>
		</form>	
	
	
	
		<?php
	break;

case "DisplayDoctorRequest":
	$TableName = TABLE_USERS." u LEFT JOIN ".TABLE_USER_TYPE_RELATION." r on (u.UserID = r.UserID)
								   
				";
	$UserObj = new DataTable($TableName);
	$OrderBy = isset($_GET['OrderBy'])?$_GET['OrderBy']:"u.UserID DESC";
	$UserObj->AllowPaging =true;
	//$UserObj->DisplayQuery=true;
	
	$UserObj->PageSize=25;
	$UserObj->PageNo =isset($_GET['PageNo'])?$_GET['PageNo']:1;	
	
	$UserObj->Where ="u.DoctorRequest='1' ";

	if($UserTypeID > 0)
	$UserObj->Where .=" AND r.UserTypeID='".(int)$UserTypeID."'";
	

	
	$Keyword = @$_REQUEST['k'];
	$WherePart ="";
	if(!empty($Keyword))
	{
		$WherePart .=" AND (";
		if(preg_match("/\+\b/i",$Keyword))
		{
			$Prcident= "AND";
			$SecGate = "1";
			$KeywordArr =  explode("+",$Keyword);
		}
		else 
		{
			$Prcident= "OR";
			$SecGate = "0";
			$KeywordArr =  explode(" ",$Keyword);
		}
		foreach ($KeywordArr as $Key=>$Value)
		{
			if(trim($Value) !="")
				$WherePart .="(
						  	u.UserName LIKE '%$Value%' 
						  	OR u.AccountCode LIKE '%$Value%' 
						  	OR u.UserID LIKE '%$Value%' 
						  	OR u.Email LIKE '%$Value%' 
						  	OR u.FirstName LIKE '%$Value%' 
						  	OR u.LastName LIKE '%$Value%' 
						  	OR u.Address1 LIKE '%$Value%' 
						  	OR u.Address2 LIKE '%$Value%' 
						  	OR u.City LIKE '%$Value%' 
						  	OR u.State LIKE '%$Value%' 
						  	OR u.Country LIKE '%$Value%' 
						  	OR u.Area LIKE '%$Value%' 
						  	OR u.ZipCode LIKE '%$Value%' 
						  	OR u.Phone LIKE '%$Value%' 
						  ) $Prcident ";
    
		}
		
		$WherePart .=" $SecGate)";
		$UserObj->Where .= $WherePart;
	}		
	
	
	$UserObj->Where .= " GROUP BY u.UserID";
	$UserObj->TableSelectAll(array("*","DATE_FORMAT(LastLogin,'%b %d, %Y') as MyLastLogin"),$OrderBy);			
	?>
	<!-- Search -->
		<form class="form-horizontal form-search well" id="FrmFilter" method="GET" action="index.php">
			<legend>Search Users</legend>
			<input type="hidden" name="Page" value="<?php echo $Page?>" >
			<input type="hidden" name="Section" value="<?php echo $Section?>" >
			<input type="hidden" name="UserTypeID" value="<?php echo $UserTypeID?>" >
			Keyword&nbsp;<input type="text" name="k" value="<?php echo @$_GET['k']?>">
					&nbsp;&nbsp;&nbsp;&nbsp;
					<button type="submit" class="btn"><icon class="icon-search icon-brown"></icon> Search</button>
		</form>
	<!-- End Search -->
	<?php
	$TotalRecords = $UserObj->TotalRecords ;
	if($TotalRecords >0)
	{
		$TotalPages =  $UserObj->TotalPages;
		if($TotalRecords > $UserObj->PageSize)
		{
		?>
		
		<br>
		<table cellpadding="1" width="100%" cellspacing="1" border="0" class="InsideTable">
			<tr>
			<td width="100%" align="center">
				<?php echo $UserObj->GetPagingLinks("index.php?Page=".$Page."&Section=".$Section."&k=".$Keyword."&UserTypeID=".$UserTypeID."&Target=".$Target."&OrderBy=".$OrderBy."&PageNo=",PAGING_FORMAT_LIST,"adm-link","<b>Page No : </b>");?>
			</td>	
			</tr>
		</table>
		<br>
		<?php
		}?>
		<form method="POST" action="<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php echo $Page?>&k=<?php echo $Keyword?>&Section=<?php echo $Section?>&UserTypeID=<?php echo $UserTypeID?>&Target=UpdateUser&PageNo=<?php echo $UserObj->PageNo?>">
		<table class="table table-bordered table-responsive block">
			<thead>
			<tr class="InsideLeftTd">
				<th width="2%" height="25">S.No</th>
				<th width="10%" align="left">Email
				<br><a href="index.php?Page=<?php echo $Page?>&Section=<?php echo $Section?>&UserTypeID=<?php echo $UserTypeID?>&k=<?php echo $Keyword?>&OrderBy=UserName ASC" class="adm-link"><icon class="icon-arrow-up icon-gray"></icon></a>
				<a href="index.php?Page=<?php echo $Page?>&Section=<?php echo $Section?>&UserTypeID=<?php echo $UserTypeID?>&k=<?php echo $Keyword?>&OrderBy=UserName DESC" class="adm-link"><icon class="icon-arrow-down icon-gray"></icon></a></th>
				<th width="10%" align="left">Name
				<br><a href="index.php?Page=<?php echo $Page?>&Section=<?php echo $Section?>&UserTypeID=<?php echo $UserTypeID?>&k=<?php echo $Keyword?>&OrderBy=FirstName ASC" class="adm-link"><icon class="icon-arrow-up icon-gray"></icon></a>
				<a href="index.php?Page=<?php echo $Page?>&Section=<?php echo $Section?>&UserTypeID=<?php echo $UserTypeID?>&k=<?php echo $Keyword?>&OrderBy=LastName DESC" class="adm-link"><icon class="icon-arrow-down icon-gray"></icon></a></th>
				<th width="10%" align="left">Phone
				<br><a href="index.php?Page=<?php echo $Page?>&Section=<?php echo $Section?>&UserTypeID=<?php echo $UserTypeID?>&k=<?php echo $Keyword?>&OrderBy=Phone ASC" class="adm-link"><icon class="icon-arrow-up icon-gray"></icon></a>
				<a href="index.php?Page=<?php echo $Page?>&Section=<?php echo $Section?>&UserTypeID=<?php echo $UserTypeID?>&k=<?php echo $Keyword?>&OrderBy=Phone DESC" class="adm-link"><icon class="icon-arrow-down icon-gray"></icon></a></th>
				
				<th width="15%">Last Visit
				<br><a href="index.php?Page=<?php echo $Page?>&Section=<?php echo $Section?>&k=<?php echo $Keyword?>&UserTypeID=<?php echo $UserTypeID?>&OrderBy=LastLogin ASC" class="adm-link"><icon class="icon-arrow-up icon-gray"></icon></a>
				<a href="index.php?Page=<?php echo $Page?>&Section=<?php echo $Section?>&k=<?php echo $Keyword?>&UserTypeID=<?php echo $UserTypeID?>&OrderBy=LastLogin DESC" class="adm-link"><icon class="icon-arrow-down icon-gray"></icon></a></th>
				
			
				<th width="5%">Active</th>
				<th width="8%" align="right"></th>
				<th width="8%" align="right"></th>
				<th width="8%" align="right"></th>
			</tr>
			</thead>
			<tbody>
		<?php
		
			$SNo=($UserObj->PageSize * ($UserObj->PageNo-1)) +1;
			$Count=1;
			while ($CurrentUser = $UserObj->GetObjectFromRecord())
			{
				
			?>
		<tr class="InsideRightTd">
			<td height="25" align="center"><?php echo $SNo?>.
				<input type="hidden" name="UserID_<?php echo $Count?>" value="<?php echo $CurrentUser->UserID?>">
			</td>
			<td align="left"><a href ="<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php echo $Page?>&Section=EditUser&UserTypeID=<?php echo $UserTypeID?>&UserID=<?php echo $CurrentUser->UserID?>" class="adm-link"><?php echo MyStripSlashes($CurrentUser->UserName)?></a>
			<?php echo (isset($CurrentUser->AccountCode)?"<br>AccountCode:".MyStripSlashes($CurrentUser->AccountCode):"");?>
			</td>
			<td><?php echo MyStripSlashes($CurrentUser->FirstName)." ".MyStripSlashes($CurrentUser->LastName)?></td>
			<td><?php echo MyStripSlashes($CurrentUser->Phone)?></td>
			<td><?php echo $CurrentUser->MyLastLogin?></td>
			<td align="center">
				<input type="checkbox" name="Active_<?php echo $Count?>" value="1" class="chk" <?php echo $CurrentUser->Active==1?"checked":""?>>
			</td>
			<td align="right"><a href ="<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php echo $Page?>&Section=EditUser&UserTypeID=<?php echo $UserTypeID?>&UserID=<?php echo $CurrentUser->UserID?>" class="btn btn-success btn-phone-block"><icon class="icon-pencil icon-white"></icon> Edit</a></td>
			<td align="right"><a href ="<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php echo $Page?>&Section=<?php echo $Section?>&UserTypeID=<?php echo $UserTypeID?>&Target=DeleteUser&UserID=<?php echo $CurrentUser->UserID?>" class="btn btn-danger btn-phone-block" onclick="return confirm('Wooooo - hold on please - the action you want to do is highly hazardous!!\nIf you press ok, You will NOT be able to recover this user.')"><icon class="icon-remove icon-white"></icon> Delete</a></td>

			<td align="right"><a href ="<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php echo $Page?>&Section=<?php echo $Section?>&UserTypeID=<?php echo $UserTypeID?>&UserID=<?php echo $CurrentUser->UserID?>&Target=ApproveDoctor" class="btn btn-success btn-phone-block"><icon class="icon-ok icon-white"></icon> Approve</a></td>

		</tr>
	<?php
			$SNo++;
			$Count++;
			}
			?>
			<tr class="InsideLeftTd">
				<td align="center" width="5%"></td>
				<td align="left"></td>
				<td align="left"></td>
				<td align="left"></td>
				<td align="center"><input type="hidden" name="Count" value="<?php echo $Count?>"></td>
				<td align="center"><input type="submit" name="UpdateActive" value="Update" class="btn btn-warning"></td>
				<td align="center"></td>
				<td align="center"></td>
				<td align="center"></td>
				
			</tr>
			</tbody>					
		</table>
		</form>						
		
	<?php
	}
	else 
	{
		echo "No record found.";
	}
	break;

	case "DisplayPartnerRequest":
	$TableName = TABLE_USERS." u LEFT JOIN ".TABLE_USER_TYPE_RELATION." r on (u.UserID = r.UserID)
								   
				";
	$UserObj = new DataTable($TableName);
	$OrderBy = isset($_GET['OrderBy'])?$_GET['OrderBy']:"u.UserID DESC";
	$UserObj->AllowPaging =true;
	//$UserObj->DisplayQuery=true;
	
	$UserObj->PageSize=25;
	$UserObj->PageNo =isset($_GET['PageNo'])?$_GET['PageNo']:1;	
	
	$UserObj->Where ="u.PartnerRequest='1' ";

	if($UserTypeID > 0)
	$UserObj->Where .=" AND r.UserTypeID='".(int)$UserTypeID."'";
	

	
	$Keyword = @$_REQUEST['k'];
	$WherePart ="";
	if(!empty($Keyword))
	{
		$WherePart .=" AND (";
		if(preg_match("/\+\b/i",$Keyword))
		{
			$Prcident= "AND";
			$SecGate = "1";
			$KeywordArr =  explode("+",$Keyword);
		}
		else 
		{
			$Prcident= "OR";
			$SecGate = "0";
			$KeywordArr =  explode(" ",$Keyword);
		}
		foreach ($KeywordArr as $Key=>$Value)
		{
			if(trim($Value) !="")
				$WherePart .="(
						  	u.UserName LIKE '%$Value%' 
						  	OR u.AccountCode LIKE '%$Value%' 
						  	OR u.UserID LIKE '%$Value%' 
						  	OR u.Email LIKE '%$Value%' 
						  	OR u.FirstName LIKE '%$Value%' 
						  	OR u.LastName LIKE '%$Value%' 
						  	OR u.Address1 LIKE '%$Value%' 
						  	OR u.Address2 LIKE '%$Value%' 
						  	OR u.City LIKE '%$Value%' 
						  	OR u.State LIKE '%$Value%' 
						  	OR u.Country LIKE '%$Value%' 
						  	OR u.Area LIKE '%$Value%' 
						  	OR u.ZipCode LIKE '%$Value%' 
						  	OR u.Phone LIKE '%$Value%' 
						  ) $Prcident ";
    
		}
		
		$WherePart .=" $SecGate)";
		$UserObj->Where .= $WherePart;
	}		
	
	
	$UserObj->Where .= " GROUP BY u.UserID";
	$UserObj->TableSelectAll(array("*","DATE_FORMAT(LastLogin,'%b %d, %Y') as MyLastLogin"),$OrderBy);			
	?>
	
	<?php
	$TotalRecords = $UserObj->TotalRecords ;
	if($TotalRecords >0)
	{
		$TotalPages =  $UserObj->TotalPages;
		if($TotalRecords > $UserObj->PageSize)
		{
		?>
		
		<br>
		<table cellpadding="1" width="100%" cellspacing="1" border="0" class="InsideTable">
			<tr>
			<td width="100%" align="center">
				<?php echo $UserObj->GetPagingLinks("index.php?Page=".$Page."&Section=".$Section."&k=".$Keyword."&UserTypeID=".$UserTypeID."&Target=".$Target."&OrderBy=".$OrderBy."&PageNo=",PAGING_FORMAT_LIST,"adm-link","<b>Page No : </b>");?>
			</td>	
			</tr>
		</table>
		<br>
		<?php
		}?>
		
		<table class="table table-bordered table-responsive block">
			<thead>
			<tr class="InsideLeftTd">
				<th width="2%" height="25">S.No</th>
				<th width="10%" align="left">Email
				<br><a href="index.php?Page=<?php echo $Page?>&Section=<?php echo $Section?>&UserTypeID=<?php echo $UserTypeID?>&k=<?php echo $Keyword?>&OrderBy=UserName ASC" class="adm-link"><icon class="icon-arrow-up icon-gray"></icon></a>
				<a href="index.php?Page=<?php echo $Page?>&Section=<?php echo $Section?>&UserTypeID=<?php echo $UserTypeID?>&k=<?php echo $Keyword?>&OrderBy=UserName DESC" class="adm-link"><icon class="icon-arrow-down icon-gray"></icon></a></th>
				<th width="10%" align="left">Name
				<br><a href="index.php?Page=<?php echo $Page?>&Section=<?php echo $Section?>&UserTypeID=<?php echo $UserTypeID?>&k=<?php echo $Keyword?>&OrderBy=FirstName ASC" class="adm-link"><icon class="icon-arrow-up icon-gray"></icon></a>
				<a href="index.php?Page=<?php echo $Page?>&Section=<?php echo $Section?>&UserTypeID=<?php echo $UserTypeID?>&k=<?php echo $Keyword?>&OrderBy=LastName DESC" class="adm-link"><icon class="icon-arrow-down icon-gray"></icon></a></th>
				<th width="10%" align="left">Phone
				<br><a href="index.php?Page=<?php echo $Page?>&Section=<?php echo $Section?>&UserTypeID=<?php echo $UserTypeID?>&k=<?php echo $Keyword?>&OrderBy=Phone ASC" class="adm-link"><icon class="icon-arrow-up icon-gray"></icon></a>
				<a href="index.php?Page=<?php echo $Page?>&Section=<?php echo $Section?>&UserTypeID=<?php echo $UserTypeID?>&k=<?php echo $Keyword?>&OrderBy=Phone DESC" class="adm-link"><icon class="icon-arrow-down icon-gray"></icon></a></th>
				
				<th width="15%">Last Visit
				<br><a href="index.php?Page=<?php echo $Page?>&Section=<?php echo $Section?>&k=<?php echo $Keyword?>&UserTypeID=<?php echo $UserTypeID?>&OrderBy=LastLogin ASC" class="adm-link"><icon class="icon-arrow-up icon-gray"></icon></a>
				<a href="index.php?Page=<?php echo $Page?>&Section=<?php echo $Section?>&k=<?php echo $Keyword?>&UserTypeID=<?php echo $UserTypeID?>&OrderBy=LastLogin DESC" class="adm-link"><icon class="icon-arrow-down icon-gray"></icon></a></th>
				
			
				
				<th width="8%" align="right"></th>
				<th width="8%" align="right"></th>
				<th width="8%" align="right"></th>
			</tr>
			</thead>
			<tbody>
		<?php
		
			$SNo=($UserObj->PageSize * ($UserObj->PageNo-1)) +1;
			$Count=1;
			while ($CurrentUser = $UserObj->GetObjectFromRecord())
			{
				
			?>
		<tr class="InsideRightTd">
			<td height="25" align="center"><?php echo $SNo?>.
				<input type="hidden" name="UserID_<?php echo $Count?>" value="<?php echo $CurrentUser->UserID?>">
			</td>
			<td align="left"><a href ="<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php echo $Page?>&Section=EditUser&UserTypeID=<?php echo $UserTypeID?>&UserID=<?php echo $CurrentUser->UserID?>" class="adm-link"><?php echo MyStripSlashes($CurrentUser->UserName)?></a>
			<?php echo (isset($CurrentUser->AccountCode)?"<br>AccountCode:".MyStripSlashes($CurrentUser->AccountCode):"");?>
			</td>
			<td><?php echo MyStripSlashes($CurrentUser->FirstName)." ".MyStripSlashes($CurrentUser->LastName)?></td>
			<td><?php echo MyStripSlashes($CurrentUser->Phone)?></td>
			<td><?php echo $CurrentUser->MyLastLogin?></td>
			<?php /* <td align="center">
				<input type="checkbox" name="Active_<?php echo $Count?>" value="1" class="chk" <?php echo $CurrentUser->Active==1?"checked":""?>>
			</td> */ ?>
			<td align="right"><a href ="<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php echo $Page?>&Section=EditUser&UserTypeID=<?php echo $UserTypeID?>&UserID=<?php echo $CurrentUser->UserID?>" class="btn btn-success btn-phone-block"><icon class="icon-pencil icon-white"></icon> Edit</a></td>
			<td align="right"><a href ="<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php echo $Page?>&Section=<?php echo $Section?>&UserTypeID=<?php echo $UserTypeID?>&Target=DeleteUser&UserID=<?php echo $CurrentUser->UserID?>" class="btn btn-danger btn-phone-block" onclick="return confirm('Wooooo - hold on please - the action you want to do is highly hazardous!!\nIf you press ok, You will NOT be able to recover this user.')"><icon class="icon-remove icon-white"></icon> Delete</a></td>
			<td align="right">

			<form method="POST" action="<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php echo $Page?>&k=<?php echo $Keyword?>&Section=<?php echo $Section?>&UserID=<?php echo $CurrentUser->UserID?>&UserTypeID=<?php echo $UserTypeID?>&Target=ApprovePartner&PageNo=<?php echo $UserObj->PageNo?>">
			Partner Code : <input type="text" name="PartnerCode">
			<button class="btn btn-success btn-phone-block"><icon class="icon-ok icon-white"></icon> Approve</button>
			</form>
			</td>

		</tr>
	<?php
			$SNo++;
			$Count++;
			}
			?>
			<tr class="InsideLeftTd">
				<td align="center" width="5%"></td>
				<td align="left"></td>
				<td align="left"></td>
				<td align="left"></td>
				<td align="center"><input type="hidden" name="Count" value="<?php echo $Count?>"></td>
				<?php /* <td align="center"><input type="submit" name="UpdateActive" value="Update" class="btn btn-warning"></td> */?>
				<td align="center"></td>
				<td align="center"></td>
				<td align="center"></td>
				
			</tr>
			</tbody>					
		</table>
								
		
	<?php
	}
	else 
	{
		echo "No record found.";
	}
	break;

	case "DisplayUser":
	default:
	$TableName = TABLE_USERS." u LEFT JOIN ".TABLE_USER_TYPE_RELATION." r on (u.UserID = r.UserID)
								   
				";
	$UserObj = new DataTable($TableName);
	$OrderBy = isset($_GET['OrderBy'])?$_GET['OrderBy']:"u.UserID DESC";
	$UserObj->AllowPaging =true;
	//$UserObj->DisplayQuery=true;
	
	$UserObj->PageSize=25;
	$UserObj->PageNo =isset($_GET['PageNo'])?$_GET['PageNo']:1;	
	
	if($UserTypeID > 0)
	$UserObj->Where ="r.UserTypeID='".(int)$UserTypeID."'";
	
	
	$Keyword = @$_REQUEST['k'];
	$WherePart ="";
	if(!empty($Keyword))
	{
		$WherePart .=" AND (";
		if(preg_match("/\+\b/i",$Keyword))
		{
			$Prcident= "AND";
			$SecGate = "1";
			$KeywordArr =  explode("+",$Keyword);
		}
		else 
		{
			$Prcident= "OR";
			$SecGate = "0";
			$KeywordArr =  explode(" ",$Keyword);
		}
		foreach ($KeywordArr as $Key=>$Value)
		{
			if(trim($Value) !="")
				$WherePart .="(
						  	u.UserName LIKE '%$Value%' 
						  	OR u.AccountCode LIKE '%$Value%' 
						  	OR u.UserID LIKE '%$Value%' 
						  	OR u.Email LIKE '%$Value%' 
						  	OR u.FirstName LIKE '%$Value%' 
						  	OR u.LastName LIKE '%$Value%' 
						  	OR u.Address1 LIKE '%$Value%' 
						  	OR u.Address2 LIKE '%$Value%' 
						  	OR u.City LIKE '%$Value%' 
						  	OR u.State LIKE '%$Value%' 
						  	OR u.Country LIKE '%$Value%' 
						  	OR u.Area LIKE '%$Value%' 
						  	OR u.ZipCode LIKE '%$Value%' 
						  	OR u.Phone LIKE '%$Value%' 
						  ) $Prcident ";
    
		}
		
		$WherePart .=" $SecGate)";
		$UserObj->Where .= $WherePart;
	}		
	
	
	$UserObj->Where .= " GROUP BY u.UserID";
	$UserObj->TableSelectAll(array("*","DATE_FORMAT(LastLogin,'%b %d, %Y') as MyLastLogin"),$OrderBy);			
	?>
	<!-- Search -->
		<form class="form-horizontal form-search well" id="FrmFilter" method="GET" action="index.php">
			<legend>Search Users</legend>
			<input type="hidden" name="Page" value="<?php echo $Page?>" >
			<input type="hidden" name="Section" value="<?php echo $Section?>" >
			<input type="hidden" name="UserTypeID" value="<?php echo $UserTypeID?>" >
			Keyword&nbsp;<input type="text" name="k" value="<?php echo @$_GET['k']?>">
					&nbsp;&nbsp;&nbsp;&nbsp;
					<button type="submit" class="btn"><icon class="icon-search icon-brown"></icon> Search</button>
		</form>
	<!-- End Search -->
	<?php
	$TotalRecords = $UserObj->TotalRecords ;
	if($TotalRecords >0)
	{
		$TotalPages =  $UserObj->TotalPages;
		if($TotalRecords > $UserObj->PageSize)
		{
		?>
		
		<br>
		<table cellpadding="1" width="100%" cellspacing="1" border="0" class="InsideTable">
			<tr>
			<td width="100%" align="center">
				<?php echo $UserObj->GetPagingLinks("index.php?Page=".$Page."&Section=".$Section."&k=".$Keyword."&UserTypeID=".$UserTypeID."&Target=".$Target."&OrderBy=".$OrderBy."&PageNo=",PAGING_FORMAT_LIST,"adm-link","<b>Page No : </b>");?>
			</td>	
			</tr>
		</table>
		<br>
		<?php
		}?>
		<form method="POST" action="<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php echo $Page?>&k=<?php echo $Keyword?>&Section=<?php echo $Section?>&UserTypeID=<?php echo $UserTypeID?>&Target=UpdateUser&PageNo=<?php echo $UserObj->PageNo?>">
		<table class="table table-bordered table-responsive block">
			<thead>
			<tr class="InsideLeftTd">
				<th width="2%" height="25">S.No</th>
				<th width="10%" align="left">Email
				<br><a href="index.php?Page=<?php echo $Page?>&Section=<?php echo $Section?>&UserTypeID=<?php echo $UserTypeID?>&k=<?php echo $Keyword?>&OrderBy=UserName ASC" class="adm-link"><icon class="icon-arrow-up icon-gray"></icon></a>
				<a href="index.php?Page=<?php echo $Page?>&Section=<?php echo $Section?>&UserTypeID=<?php echo $UserTypeID?>&k=<?php echo $Keyword?>&OrderBy=UserName DESC" class="adm-link"><icon class="icon-arrow-down icon-gray"></icon></a></th>
				<th width="10%" align="left">Name
				<br><a href="index.php?Page=<?php echo $Page?>&Section=<?php echo $Section?>&UserTypeID=<?php echo $UserTypeID?>&k=<?php echo $Keyword?>&OrderBy=FirstName ASC" class="adm-link"><icon class="icon-arrow-up icon-gray"></icon></a>
				<a href="index.php?Page=<?php echo $Page?>&Section=<?php echo $Section?>&UserTypeID=<?php echo $UserTypeID?>&k=<?php echo $Keyword?>&OrderBy=LastName DESC" class="adm-link"><icon class="icon-arrow-down icon-gray"></icon></a></th>
				<th width="10%" align="left">Phone
				<br><a href="index.php?Page=<?php echo $Page?>&Section=<?php echo $Section?>&UserTypeID=<?php echo $UserTypeID?>&k=<?php echo $Keyword?>&OrderBy=Phone ASC" class="adm-link"><icon class="icon-arrow-up icon-gray"></icon></a>
				<a href="index.php?Page=<?php echo $Page?>&Section=<?php echo $Section?>&UserTypeID=<?php echo $UserTypeID?>&k=<?php echo $Keyword?>&OrderBy=Phone DESC" class="adm-link"><icon class="icon-arrow-down icon-gray"></icon></a></th>
				
				<th width="15%">Last Visit
				<br><a href="index.php?Page=<?php echo $Page?>&Section=<?php echo $Section?>&k=<?php echo $Keyword?>&UserTypeID=<?php echo $UserTypeID?>&OrderBy=LastLogin ASC" class="adm-link"><icon class="icon-arrow-up icon-gray"></icon></a>
				<a href="index.php?Page=<?php echo $Page?>&Section=<?php echo $Section?>&k=<?php echo $Keyword?>&UserTypeID=<?php echo $UserTypeID?>&OrderBy=LastLogin DESC" class="adm-link"><icon class="icon-arrow-down icon-gray"></icon></a></th>
				
			
				<th width="5%">Active</th>
				<th width="8%" align="right"></th>
				<th width="8%" align="right"></th>
			</tr>
			</thead>
			<tbody>
		<?php
		
			$SNo=($UserObj->PageSize * ($UserObj->PageNo-1)) +1;
			$Count=1;
			while ($CurrentUser = $UserObj->GetObjectFromRecord())
			{
				
			?>
		<tr class="InsideRightTd">
			<td height="25" align="center"><?php echo $SNo?>.
				<input type="hidden" name="UserID_<?php echo $Count?>" value="<?php echo $CurrentUser->UserID?>">
			</td>
			<td align="left"><a href ="<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php echo $Page?>&Section=EditUser&UserTypeID=<?php echo $UserTypeID?>&UserID=<?php echo $CurrentUser->UserID?>" class="adm-link"><?php echo MyStripSlashes($CurrentUser->UserName)?></a>
			<?php echo (isset($CurrentUser->AccountCode)?"<br>AccountCode:".MyStripSlashes($CurrentUser->AccountCode):"");?>
			</td>
			<td><?php echo MyStripSlashes($CurrentUser->FirstName)." ".MyStripSlashes($CurrentUser->LastName)?></td>
			<td><?php echo MyStripSlashes($CurrentUser->Phone)?></td>
			<td><?php echo $CurrentUser->MyLastLogin?></td>
			<td align="center">
				<input type="checkbox" name="Active_<?php echo $Count?>" value="1" class="chk" <?php echo $CurrentUser->Active==1?"checked":""?>>
			</td>
			<td align="right"><a href ="<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php echo $Page?>&Section=EditUser&UserTypeID=<?php echo $UserTypeID?>&UserID=<?php echo $CurrentUser->UserID?>" class="btn btn-success btn-phone-block"><icon class="icon-pencil icon-white"></icon> Edit</a></td>
			<td align="right"><a href ="<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php echo $Page?>&Section=<?php echo $Section?>&UserTypeID=<?php echo $UserTypeID?>&Target=DeleteUser&UserID=<?php echo $CurrentUser->UserID?>" class="btn btn-danger btn-phone-block" onclick="return confirm('Wooooo - hold on please - the action you want to do is highly hazardous!!\nIf you press ok, You will NOT be able to recover this user.')"><icon class="icon-remove icon-white"></icon> Delete</a></td>
		</tr>
	<?php
			$SNo++;
			$Count++;
			}
			?>
			<tr class="InsideLeftTd">
				<td align="center" width="5%"></td>
				<td align="left"></td>
				<td align="left"></td>
				<td align="left"></td>
				<td align="center"><input type="hidden" name="Count" value="<?php echo $Count?>"></td>
				<td align="center"><input type="submit" name="UpdateActive" value="Update" class="btn btn-warning"></td>
				<td align="center"></td>
				<td align="center"></td>
				
			</tr>
			</tbody>					
		</table>
		</form>						
		
	<?php
	}
	else 
	{
		echo "No record found.";
	}
	break;
}
///// Section end?>