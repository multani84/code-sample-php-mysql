<table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr>
		<td height="20" align="center">
		<h4>Customers Reports Section</h4>
	</td>
	</tr>
</table>
<form id="FrmFilter" method="GET" action="index.php">
	<input type="hidden" name="Page" value="<?php echo $Page?>" >
	<input type="hidden" name="Section" value="Display" >
<table border="0" cellpadding="0" cellspacing="0" width="100%" class="InsideTable">
	<tr>
		<td align="left" colspan="5"><b>Select Field(s) to display</b></td>
	</tr>
	<tr>
		<td align="left"><input type="checkbox" name="c1" value="1" <?php echo @$_REQUEST['c1']=="1"?"checked":"";?>>&nbsp;Email</td>
		<td align="left"><input type="checkbox" name="c2" value="1" <?php echo @$_REQUEST['c2']=="1"?"checked":"";?>>&nbsp;First Name</td>
		<td align="left"><input type="checkbox" name="c3" value="1" <?php echo @$_REQUEST['c3']=="1"?"checked":"";?>>&nbsp;Last Name</td>
		<td align="left"><input type="checkbox" name="c4" value="1" <?php echo @$_REQUEST['c4']=="1"?"checked":"";?>>&nbsp;Address</td>
		<td align="left"><input type="checkbox" name="c5" value="1" <?php echo @$_REQUEST['c5']=="1"?"checked":"";?>>&nbsp;Last Login</td>
	</tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="100%" class="InsideTable">
	<tr>
		<td align="left" colspan="4"><b>Filter Your results</b></td>
	</tr>
	<tr>
	<td align="left" valign="top" colspan="2">Any <b>keyword/ name or<br> Name/Address</b> <input size="25" type="text" name="k" value="<?php echo @$_REQUEST['k']?>"></td>
		<td align="right">
		<select name="o" id="o">
			<option value="">--Order By--</option>
			<?php
			$OrderByArray = array(
			"CreatedDate DESC"=>"Created Date DESC",
			"CreatedDate ASC"=>"Created Date ASC",
			"Email DESC"=>"Email DESC",
			"Email ASC"=>"Email ASC",
			"FirstName DESC"=>"FirstName DESC",
			"FirstName ASC"=>"FirstName ASC",
			);
			foreach ($OrderByArray as $k=>$v)
			{
			?>
			<option value="<?php echo $k?>" <?php echo @$_REQUEST['o'] ==$k?"selected":""?>><?php echo $v?></option>
			<?php
			}?>
		</select>
		</td>
		<td align="left"><input type="submit" name="Submit" value="Generate Report"></td>
	</tr>
</table>
</form>
<?php
switch($Section)
{
	case "Display":
		$SelectArray = array("Email");
		
		
		
		if(@$_REQUEST['c2']=="1")
			array_push($SelectArray,"FirstName as 'First Name'");
		
		if(@$_REQUEST['c3']=="1")
			array_push($SelectArray,"LastName as 'Last Name'");
		
		if(@$_REQUEST['c4']=="1")
		{
			array_push($SelectArray,"concat(Address1,
										    ', ', Address2,
										    ' ', City,
										    ', ', State,
										    ' ', Country,
										    ' ', ZipCode,
										    ' ', Phone
										    ) as ' Address'");
			
			
		}	

		if(@$_REQUEST['c5']=="1")
			array_push($SelectArray,"DATE_FORMAT(LastLogin,'%b %d, %Y')as 'Last Login'");
			
		
		
		//$UserObj->DisplayQuery = true;
		$UserObj->Where = "1";
		
	$Keyword = @$_REQUEST['k'];
	$WherePart ="";
	if(!empty($Keyword))
	{
		$WherePart .=" AND (";
		if(preg_match("/\+\b/i",$Keyword))
		{
			$Prcident= "AND";
			$SecGate = "1";
			$KeywordArr =  explode("+",$Keyword);
		}
		else 
		{
			$Prcident= "OR";
			$SecGate = "0";
			$KeywordArr =  explode(" ",$Keyword);
		}
		foreach ($KeywordArr as $Key=>$Value)
		{
			if(trim($Value) !="")
				$WherePart .="(
						  	Email LIKE '%$Value%' 
						  	OR FirstName LIKE '%$Value%' 
						  	OR LastName LIKE '%$Value%' 
						  	OR Address1 LIKE '%$Value%' 
						  	OR Address2 LIKE '%$Value%' 
						  	OR City LIKE '%$Value%' 
						  	OR State LIKE '%$Value%' 
						  	OR Country LIKE '%$Value%' 
						  	OR ZipCode LIKE '%$Value%' 
						  	OR Phone LIKE '%$Value%' 
						  	) $Prcident ";
    
		}
		
		$WherePart .=" $SecGate)";
		$UserObj->Where .= $WherePart;
	}
		
		if(@$_REQUEST['o']!="")
			$OrderBy = @$_REQUEST['o'];
		else 
			$OrderBy = "CreatedDate DESC";
				
		//$UserObj->DisplayQuery = true;	
		$UserObj->TableSelectAll($SelectArray,$OrderBy);
		
		if($UserObj->GetNumRows() >0)
		{
			$ColFields = $UserObj->GetNumFields();
			?>
			<div align="left">
			<br>
				<form target="_blank" action="<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php echo $Page?>&Section=<?php echo $Section?>&Target=Download" method="POST">
					<input type="hidden" name="Q" value="<?php echo base64_encode($UserObj->Query)?>">
					&nbsp;&nbsp;<input type="submit" name="Download" value="Download ALL">
					&nbsp;&nbsp;<input type="submit" name="Print" value="Print">
					&nbsp;&nbsp;<input type="button" name="AskEmail" value="Email Report" onclick="return document.getElementById('DivEmailAddress').style.display='block';">
					<div id="DivEmailAddress" style="display:none;padding:15px;border:1px solid #000;">
						Enter email address:<input size="40" type="text" name="EmailAddress" id="EmailAddress" value="" />
						&nbsp;&nbsp;<input type="submit" name="Email" value="Send">
					</div>
				</form>
			<br>
			</div>
			<table border="0" cellpadding="3" cellspacing="2" width="100%" class="InsideTable">
			  <tr class="InsideLeftTd">
			     <?php
			     for ($i = 0; $i < $ColFields; $i++) 
					{
		    			$ColName = $UserObj->GetFieldName($i);
		    			echo "<td align='left' height=25><b>".$ColName."</b></td>";
		    		}
					 	
				?>
	           </tr>
	          <?php
	          	while ($Result = $UserObj->GetArrayFromRecord())
				{
					echo "<tr>";
					for ($i = 0; $i < $ColFields; $i++) 
						echo "<td align='left' style='padding-bottom:10px;'>".MyStripSlashes($Result[$i])."</td>";
		    		
					echo "</tr>";
					
				}
	          ?>
	        </table>
			<?php
		}
		else 
		{
			echo "<br><br><br><br><b>No record found</b>";
		}
	break;
	
}
?>

