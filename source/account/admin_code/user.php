<?php
require_once(DIR_FS_SITE_INCLUDES_CLASSES."class.excel_maker.php");
$UserObj = new DataTable(TABLE_USERS);
$UserObj2 = new DataTable(TABLE_USERS);
$UserAddressObj = new DataTable(TABLE_USERS_ADDRESS);
$UsersNewsLetterObj= new DataTable(TABLE_USERS_NEWSLETTER);
$UserWatchListObj = new DataTable(TABLE_USERS_WATCHLIST);
$UserWCategoryObj = new DataTable(TABLE_USERS_WCATEGORY);
$UserWishListObj = new DataTable(TABLE_USERS_WISHLIST);
$UserPointObj = new DataTable(TABLE_USERS_POINT);

$Section = isset($_GET['Section'])?$_GET['Section']:"";
$Target = isset($_GET['Target'])?$_GET['Target']:"";
$UserID =isset($_GET['UserID'])?$_GET['UserID']:"";
$UserTypeID =isset($_GET['UserTypeID'])?$_GET['UserTypeID']:"";
$DataArray = array();

if($UserID != "")
{
$UserObj->Where = "UserID='".$UserObj->MysqlEscapeString($UserID)."'";
$CurrentUser = $UserObj->TableSelectOne();
		
}

/// Target  start 	
switch ($Target)
{
	case "DeleteUser":
		$UserObj->Where ="UserID='".$UserID."'";
		$UserObj->TableDelete();
		
		$UserAddressObj->Where = $UserObj->Where;
		$UserAddressObj->TableDelete();
		
		$UsersNewsLetterObj->Where = $UserObj->Where;
		$UsersNewsLetterObj->TableDelete();
		
		$UserWatchListObj->Where = $UserObj->Where;
		$UserWatchListObj->TableDelete();
		
		$UserWCategoryObj->Where = $UserObj->Where;
		$UserWCategoryObj->TableDelete();
		
		$UserWishListObj->Where = $UserObj->Where;
		$UserWishListObj->TableDelete();
		
		
		
		ob_clean();
					
		$_SESSION['InfoMessage'] ="Users deleted successfully.";
		MyRedirect(DIR_WS_SITE_CONTROL."index.php?Page=$Page");
		exit;

	break;	
	case "AccountUpdate":
	if(count($_POST) > 0)
	{
		$UserTypeRelObj = new DataTable(TABLE_USER_TYPE_RELATION);
		
		$Discount = isset($_POST['Discount'])?$_POST['Discount']:"";
		if(is_array($Discount) && count($Discount) > 0)
		{
			foreach($Discount as $RelationID=>$val)
			{
				$UserTypeRelObj->Where ="RelationID= '".(int)$RelationID."'";
				$UserTypeRelObj->TableUpdate(array("Discount"=>$val));
			}
		}
		
		$CheckDiscount = isset($_POST['CheckDiscount'])?$_POST['CheckDiscount']:"";
		if(is_array($CheckDiscount) && count($CheckDiscount) > 0)
		{
			foreach($CheckDiscount as $UserTypeID=>$val)
			{
				//$UserTypeRelObj->Where ="RelationID= '".(int)$RelationID."'";
				//$UserTypeRelObj->TableUpdate(array("Discount"=>$val));
				$AddDiscount = isset($_POST['AddDiscount'][$UserTypeID])?$_POST['AddDiscount'][$UserTypeID]:"";
				
				$DataArray = array();
				$DataArray["UserTypeID"] = $UserTypeID;
				$DataArray["UserID"] = $UserID;
				$DataArray["Discount"] = $AddDiscount;
				$UserTypeRelObj->TableInsert($DataArray);
			}
		}
	
		@ob_clean();
		$_SESSION['InfoMessage'] ="You have successfully updated account.";
		MyRedirect(DIR_WS_SITE_CONTROL."index.php?Page=$Page&Section=$Section&UserTypeID=$UserTypeID&UserID=$UserID");
		exit;
	}
	
	break;

	case "ApproveDoctor":
		$UserTypeRelObj = new DataTable(TABLE_USER_TYPE_RELATION);
		$UserID = isset($_GET['UserID'])?$_GET['UserID']:"";
		$UserTypeRelObj->Where ="UserID= '".$UserID."'";
		$UserTypeRelObj->TableUpdate(array("UserTypeID"=>2));
		
		$UserObj->TableUpdate(array("DoctorRequest"=>0));

		$DataArray = array();
		$DataArray['Mail_Subject'] ="Request Approved";
		$DataArray['Mail_ToEmail'] =$CurrentUser->Email;
		$DataArray['Name'] =$CurrentUser->FirstName;
		
		$DataArray['UserTypeName']	= "Partner";
		$CustomObj->SendMailByUsingTemplate("approve_notification.php",$DataArray);
		@ob_clean();

		$_SESSION['InfoMessage'] ="You have successfully approvded Doctor request.";
		MyRedirect(DIR_WS_SITE_CONTROL."index.php?Page=$Page&Section=$Section&UserTypeID=$UserTypeID&UserID=$UserID");
		exit;
	
	break;

	case "ApprovePartner":
		$UserTypeRelObj = new DataTable(TABLE_USER_TYPE_RELATION);
		$UserID = isset($_GET['UserID'])?$_GET['UserID']:"";
		$UserTypeRelObj->Where ="UserID= '".$UserID."'";
		// $UserTypeRelObj->DisplayQuery = true;
		$UserTypeRelObj->TableUpdate(array("UserTypeID"=>3));
		
		$UserObj->TableUpdate(array("PartnerRequest"=>0, "PartnerCode"=>$_POST['PartnerCode']));

		$DataArray = array();
		$DataArray['Mail_Subject'] ="Request Approved";
		$DataArray['Mail_ToEmail'] =$CurrentUser->Email;
		$DataArray['Name'] =$CurrentUser->FirstName;
		
		$DataArray['UserTypeName']	= "Partner";
		$DataArray['PartnerCode']	= $_POST['PartnerCode'];
		$CustomObj->SendMailByUsingTemplate("approve_notification.php",$DataArray);
		@ob_clean();

		$_SESSION['InfoMessage'] ="You have successfully approvded Partner request.";
		MyRedirect(DIR_WS_SITE_CONTROL."index.php?Page=$Page&UserTypeID=3&UserID=$UserID");
		exit;
	
	break;

	case "AccountDelete":
		$UserTypeRelObj = new DataTable(TABLE_USER_TYPE_RELATION);
		$RelationID = isset($_GET['RelationID'])?$_GET['RelationID']:"";
		$UserTypeRelObj->Where ="RelationID= '".(int)$RelationID."'";
		$UserTypeRelObj->TableDelete();
		
		@ob_clean();
		$_SESSION['InfoMessage'] ="You have successfully deleted account type.";
		MyRedirect(DIR_WS_SITE_CONTROL."index.php?Page=$Page&Section=$Section&UserTypeID=$UserTypeID&UserID=$UserID");
		exit;
	
	break;
	case "UpdateUser":
		for ($i=1;$i <$_POST['Count'];$i++)
		{
			$UserObj->Where = "UserID='".$UserObj->MysqlEscapeString($_POST['UserID_'.$i])."'";
			$DataArray['Active'] = isset($_POST['Active_'.$i])?$_POST['Active_'.$i]:0;						
			$UserObj->TableUpdate($DataArray);	
		}
		$PageNo = isset($_GET['PageNo'])?$_GET['PageNo']:"1";
		ob_clean();
					
		$_SESSION['InfoMessage'] ="Users updated successfully.";
		MyRedirect(DIR_WS_SITE_CONTROL."index.php?Page=$Page&Section=$Section&UserTypeID=$UserTypeID&PageNo=$PageNo");
		exit;
		
	break;
	
	case "AddUser":
			$ErrorArr = array();
			if($UserID=="")
			{
				$DataArray['UserName']= isset($_POST['UserName'])?$_POST['UserName']:"";
				$UserObj2->Where = "UserName = '".$UserObj2->MysqlEscapeString($DataArray['UserName'])."'";
				$Obj = $UserObj2->TableSelectOne(array("UserID"));
				if(isset($Obj->UserID) && $Obj->UserID !="")
					array_push($ErrorArr,constant("DEFINE_USERNAME_ALREADY_EXIST_MSG"));
			}
			if(count($ErrorArr) ==0)
			{
				$DataArray['FirstName'] = isset($_POST['FirstName'])?$_POST['FirstName']:"";
				$DataArray['LastName'] = isset($_POST['LastName'])?$_POST['LastName']:"";
				$DataArray['Gender'] = isset($_POST['Gender'])?$_POST['Gender']:"";
				$DataArray['Address1'] = isset($_POST['Address1'])?$_POST['Address1']:"";
				$DataArray['Address2'] = isset($_POST['Address2'])?$_POST['Address2']:"";
				$DataArray['City'] = isset($_POST['City'])?$_POST['City']:"";
				$DataArray['State'] = isset($_POST['State'])?$_POST['State']:"";
				$DataArray['ZipCode'] = isset($_POST['ZipCode'])?$_POST['ZipCode']:"";
				$DataArray['Country'] = isset($_POST['Country'])?$_POST['Country']:"";
				$DataArray['Area'] = isset($_POST['Area'])?$_POST['Area']:"";
				$DataArray['Phone'] = isset($_POST['Phone'])?$_POST['Phone']:"";
				$DataArray['Active'] = isset($_POST['Active'])?$_POST['Active']:0;
				$DataArray['PartnerCode'] = isset($_POST['PartnerCode'])?$_POST['PartnerCode']:"";
				
				if($UserID=="")
				{
					$DataArray['UserID']  = SKRemoveSpecialChars(substr($DataArray['UserName'],0,5))."_".md5(uniqid(rand(), true));
					$DataArray['Email']	= isset($_POST['Email'])?$_POST['Email']:$DataArray['UserName'];
					$DataArray['ActiveOnce'] ="1";
					$DataArray['AccountCode'] 	= SkGetUniqueUserNo();
					$DataArray['CreatedDate']= date('YmdHis');
					$UserObj->TableInsert($DataArray);
				}
				else 
				{
					$UserObj->Where ="UserID='".$UserID."'";
					$UserObj->TableUpdate($DataArray);
				}
	
				ob_clean();
							
				$_SESSION['InfoMessage'] ="Users updated successfully.";
				MyRedirect(DIR_WS_SITE_CONTROL."index.php?Page=$Page&UserTypeID=$UserTypeID&UserID=$UserID");
				exit;
			}
			else 
			{
				@ob_clean();
				$_SESSION['ErrorMessage'] = implode($ErrorArr,"<br>");
			}
			
					
	break;
	case "PasswordUpdate":
		if(!empty($_POST['Password']))
		{
			if($_POST['Password'] == $_POST['RPassword'])
			{
				$DataArray = array();
				$DataArray['Password'] = $_POST['Password'];
				$PassSalt = md5(uniqid(rand(),true));
				$DataArray['Password'] = NeoCryptedPassword($DataArray['Password'],$PassSalt).":".$PassSalt;
	
				$UserObj->Where ="UserID='".$UserID."'";
				$UserObj->TableUpdate($DataArray);
				ob_clean();
							
				$_SESSION['InfoMessage'] ="You have successfully changed password.";
				MyRedirect(DIR_WS_SITE_CONTROL."index.php?Page=$Page&Section=$Section&UserTypeID=$UserTypeID&UserID=$UserID");
				exit;
			}
			
		}
			
			
					
	break;
	case "PointsUpdate":
			if(isset($_POST['Points']) && $_POST['Points'] >0)
			{
				$DataArray = array();
				$DataArray['UserID'] =$UserID;
				$DataArray['OrderID'] =0;
				$DataArray['Points'] =$_POST['Points'];
				$DataArray['OrderStatus'] =$PaymentStatusArray['Paid'];
				$DataArray['PointStatus'] =$_POST['PointStatus'];
				$DataArray['CreatedDate'] =date('Y-m-d H:i:s');
				
				$UserPointObj->TableInsert($DataArray);
				ob_clean();
							
				$_SESSION['InfoMessage'] ="You have successfully updated the user points.";
				MyRedirect(DIR_WS_SITE_CONTROL."index.php?Page=$Page&Section=$Section&UserTypeID=$UserTypeID&UserID=$UserID");
				exit;
			}					
	break;
	
}

//// target end
?>