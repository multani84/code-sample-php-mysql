<?php
require_once(DIR_FS_SITE_INCLUDES_CLASSES."class.excel_maker.php");

$UserObj = new DataTable(TABLE_USERS);
$UserObj2 = new DataTable(TABLE_USERS);
$OrderObj = new DataTable(TABLE_ORDERS);
$Section = isset($_GET['Section'])?$_GET['Section']:"";
$Target = isset($_GET['Target'])?$_GET['Target']:"";

	/// Target  start 
switch ($Target)
{
	case "Download":
	
		if(isset($_POST['Download']) && $_POST['Download'] !="")
		{
			@ob_clean();
			$FileName = DIR_FS_SITE_UPLOADS_DOWNLOAD."tmp/Maker_".date("Md-Y-his").".csv";
			$Obj = new ExcelMaker();
			$Obj->DBHostName =DB_HOSTNAME;
			$Obj->DBUserName =DB_USERNAME;
			$Obj->DBPassword =DB_PASSWORD;
			$Obj->DBDataBase =DB_DATABASE;
			$Obj->QueryArray[1] =base64_decode(@$_REQUEST['Q']);
			$Obj->GenerateExcel($FileName,true);
			exit;
		}
		
		if(isset($_POST['Print']) && $_POST['Print'] !="")
		{
			ob_clean();
			$DataTableObj->ExecuteQuery(base64_decode(@$_REQUEST['Q']));
			$ColFields = $DataTableObj->GetNumFields();
			?>
				<html>
				<body onload="print();">
					<table border="1" cellpadding="3" cellspacing="2" width="100%" style="border:1px solid #000000;">
					  <tr>
					     <?php
					     for ($i = 0; $i < $ColFields; $i++) 
							{
				    			$ColName = $DataTableObj->GetFieldName($i);
				    			echo "<td align='left' height=25><b>".$ColName."</b></td>";
				    		}
							 	
						?>
			           </tr>
			          <?php
			          	while ($Result = $DataTableObj->GetArrayFromRecord())
						{
							echo "<tr>";
							for ($i = 0; $i < $ColFields; $i++) 
								echo "<td align='left' style='padding-bottom:10px;'>".MyStripSlashes($Result[$i])."</td>";
				    		
							echo "</tr>";
							
						}
			          ?>
			        </table>
				</body></html>
				<?php
			exit;
		}
		
		if(isset($_POST['Email']) && $_POST['Email'] !="" && isset($_POST['EmailAddress']) && $_POST['EmailAddress'] !="")
		{
			@ob_clean();
			$DataTableObj->ExecuteQuery(base64_decode(@$_REQUEST['Q']));
			$ColFields = $DataTableObj->GetNumFields();
			?>
					<table border="1" cellpadding="3" cellspacing="2" width="100%" style="border:1px solid #000000;">
					  <tr>
					     <?php
					     for ($i = 0; $i < $ColFields; $i++) 
							{
				    			$ColName = $DataTableObj->GetFieldName($i);
				    			echo "<td align='left' height=25><b>".$ColName."</b></td>";
				    		}
							 	
						?>
			           </tr>
			          <?php
			          	while ($Result = $DataTableObj->GetArrayFromRecord())
						{
							echo "<tr>";
							for ($i = 0; $i < $ColFields; $i++) 
								echo "<td align='left' style='padding-bottom:10px;'>".MyStripSlashes($Result[$i])."</td>";
				    		
							echo "</tr>";
							
						}
			          ?>
			        </table>
				<?php
				$MessageBody = ob_get_contents();
				
				@ob_clean();
				$FileName = DIR_FS_SITE_UPLOADS_DOWNLOAD."tmp/Maker_".date("Md-Y-his").".csv";
				$Obj = new ExcelMaker();
				$Obj->DBHostName =DB_HOSTNAME;
				$Obj->DBUserName =DB_USERNAME;
				$Obj->DBPassword =DB_PASSWORD;
				$Obj->DBDataBase =DB_DATABASE;
				$Obj->QueryArray[1] =base64_decode(@$_REQUEST['Q']);
				$Obj->GenerateExcel($FileName,false);
				
				SendEmail("Reports from ".DEFINE_SITE_NAME." on ".date("Md-Y-his"),$_POST['EmailAddress'],DEFINE_ADMIN_EMAIL,SITE_NAME,$MessageBody,"",DEFINE_EMAIL_FORMAT,true,$FileName);
				@ob_clean();
				?>
				<html>
					<body onload="alert('You have successfully sent report');window.close();">
					</body>
				</html>
				<?php
				exit;
		}
		
	break;
}


//// target end
?>