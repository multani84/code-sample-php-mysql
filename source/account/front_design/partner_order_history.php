<div id="MainContent">
    	<div class="container"> 
            <ul class="breadcrumb">
                <li><a href="<?php echo SKSEOURL("1001","cms")?>">Home</a></li>
                <li class="active">Reference Order History</li>
            </ul>
                <h1>Order History</h1>
				<?php /* middle content start*/?>
				
					<?php
		switch ($Section)
		{
			
		default:
			$OrderObj = new DataTable(TABLE_ORDERS);
			//$OrderObj->DisplayQuery = true;
			$OrderObj->Where = "PartnerCode='".$OrderObj->MysqlEscapeString($CurrentUserObj->PartnerCode)."'";
			$OrderObj->TableSelectAll(array("*, DATE_FORMAT(CreatedDate,'%b %d, %Y')as MyCreatedDate, DATE_FORMAT(CreatedDate,'%M, %Y')as MyCreatedMonth"),"CreatedDate DESC",10);
			if($OrderObj->GetNumRows() > 0)
			{
			?>
			<div class="table-responsive">  
			<table class="table table-bordered">
			 	<thead>
				<tr>
					<th align="center" height="25"><b>S.No</b></th>
					<th align="left"><b>Order Date</b></th>
					<th align="left"><b>Order No</b></th>
					<th align="left"><b>Name</b></th>
					<th align="center"><b>Order Status</b></th>
					<th class="text-right"><b>Amount&nbsp;</b></th>
					
				</tr>
				</thead>
				<tbody>
				<?php
				$SNo = 1;
			 	while($CurrentOrder = $OrderObj->GetObjectFromRecord())
				{
					$CurrencyOrderID = $CurrentOrder->OrderID;
					?>
					<tr>
						<td align="left"  style="height:50px;"><b><?php echo $SNo?></b></td>
						<td align="left" ><?php echo $CurrentOrder->MyCreatedDate?></td>
						<td align="left" ><?php echo $CurrentOrder->OrderNo?></td>
						<td align="left" ><?php echo $CurrentOrder->BillingFirstName.' '.$CurrentOrder->BillingLastName?></td>
						<td align="left" ><?php echo $CurrentOrder->OrderStatus?>
						
						</td>
						<td align="right" ><?php echo Change2CurrentCurrency($CurrentOrder->GrandTotal)?></td>
						
					</tr>
			
					<?php
					$SNo++;
			    }?>
				</tbody>
			 </table>
			 </div>
			<?php
			}
			else
			{
				echo "There is no any order in your account.";
			}
		 break;
		}
			?>
				
				<?php /* middle content end*/?>
				
            <p>&nbsp;</p>
			<p>&nbsp;</p>
			<p>&nbsp;</p>
			</div>
        </div>