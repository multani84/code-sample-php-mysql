<div id="MainContent">
    	<div class="container"> 
            <ul class="breadcrumb">
                <li><a href="<?php echo SKSEOURL("1001","cms")?>">Home</a></li>
                <li class="active">Your Details</li>
            </ul>
        	<h1>Signup as Partner</h1>

			 <?php /* middle content start*/?>
				<?php require_once(DIR_FS_SITE_INCLUDES."message.php");?>
				
				<?php 
				echo (isset($CurrentPage->LeftDescription1)?MyStripSlashes($CurrentPage->LeftDescription1):"&nbsp;");
				?>
				<form class="form-horizontal" role="form" method="POST" action="<?php echo MakePageURL("index.php","Page=$Page");?>" id="RegistrationForm" name="RegistrationForm" enctype="multipart/form-data" >
							 

	
	
						<div class="row">
                             <div class="panel panel-warning">
								<div class="panel-body">
								
								<div class="form-group">
									<label for="email" class="control-label col-sm-4">Email: *</label>
									<div class="col-sm-8">
										<input class="form-control" name="UserName" id="UserName_E" value="<?php echo @$_POST['UserName']?>" title="Invalid email ! Please try user@domain.com" placeholder="Email Address" type="email" required />
									</div>
								</div>
								
								<div class="form-group">
									<label for="email" class="control-label col-sm-4">Password: *</label>
									<div class="col-sm-8">
										<input class="form-control" name="Password" id="Password_R" value="" title="Please enter Password." placeholder="Password" type="password" required />
									</div>
								</div>
								<div class="form-group">
									<label for="email" class="control-label col-sm-4">Confirm Password: *</label>
									<div class="col-sm-8">
										<input class="form-control" name="CPassword" id="CPassword_C" title="Password should be same." placeholder="Confirm Password" type="password" value="" required />
									</div>
								</div>
								
								<div class="form-group">
									<label for="email" class="control-label col-sm-4">Name: *</label>
									<div class="col-sm-8">
										<input class="form-control" name="FirstName" id="FirstName_R" value="<?php echo @$_POST['FirstName']?>" title="" placeholder="Name" type="text" required />
									</div>
								</div>
								
								<div class="form-group">
									<label for="email" class="control-label col-sm-4">Contact Phone: *</label>
									<div class="col-sm-8">
										<input class="form-control" name="Phone" id="Phone_R" value="<?php echo @$_POST['Phone']?>" title="" placeholder="Phone" type="text" required />
									</div>
								</div>
								<div class="form-group">
									<label for="email" class="control-label col-sm-4">Your Higher Qualification: *</label>
									<div class="col-sm-8">
										<input class="form-control" name="Qualification" id="Qualification_R" value="<?php echo @$_POST['Qualification']?>" title="" placeholder="" type="text" required />
									</div>
								</div>
								<div class="form-group">
									<label for="email" class="control-label col-sm-4">Upload Your Higher Qualification's Certificate Copy: *</label>
									<div class="col-sm-8">
										<input class="" name="CertificateCopy" id="CertificateCopy_R" value="" title="" placeholder="" type="file" required />
									</div>
								</div>
								<div class="form-group">
									<label for="email" class="control-label col-sm-4">Upload Your Adhar Card or Identity Proof: *</label>
									<div class="col-sm-8">
										<input class="" name="IdProof" id="IdProof_R" value="" title="" placeholder="" type="file" required />
									</div>
								</div>
								<div class="form-group">
									<label for="email" class="control-label col-sm-4">Explain More About You: *</label>
									<div class="col-sm-8">
										<input class="form-control" name="AboutYou" id="AboutYou_R" value="<?php echo @$_POST['AboutYou']?>" title="" placeholder="" type="text" required />
									</div>
								</div>
								<div class="form-group">
									<label for="email" class="control-label col-sm-4">Address1: *</label>
									<div class="col-sm-8">
										<input class="form-control" name="Address1" id="Address1_R" value="<?php echo @$_POST['Address1']?>" title="" placeholder="Address1" type="text" required />
									</div>
								</div>
								
								<div class="form-group">
									<label for="email" class="control-label col-sm-4">Address2: </label>
									<div class="col-sm-8">
										<input class="form-control" name="Address2" id="Address2" value="<?php echo @$_POST['Address2']?>" title="" placeholder="Address2" type="text"/>
									</div>
								</div>
								
								<div class="form-group">
									<label for="email" class="control-label col-sm-4">Town/City: *</label>
									<div class="col-sm-8">
										<input class="form-control" name="City" id="City_R" value="<?php echo @$_POST['City']?>" title="" placeholder="City" type="text"  required />
									</div>
								</div>
								
								<div class="form-group">
									<label for="email" class="control-label col-sm-4">County/State: *</label>
									<div class="col-sm-8">
									<input class="form-control"  type="text" name="State" value="<?php echo @$_POST['State']?>" id="State_R" title="Please enter State." placeholder="State" required />
									</div>
								</div>
								
								<div class="form-group">
									<label for="email" class="control-label col-sm-4">Postal/Zip Code: *</label>
									<div class="col-sm-8">
										<input class="form-control" name="ZipCode" id="ZipCode_R" value="<?php echo @$_POST['ZipCode']?>" title="" placeholder="Zip Code" type="text" required />
									</div>
								</div>
								
								<div class="form-group">
									<label for="email" class="control-label col-sm-4">Country: *</label>
									<div class="col-sm-8">
											<select rel="sk_country" data-regionid="#State_R" data-value="<?php echo @$_POST['Country']?>" name="Country" id="Country_R" title="Please select Country." class="form-control"></select>
									</div>
								</div>
								
								
								<div class="form-group">
									<label for="email" class="control-label col-sm-4"><a onclick="return ChangeCaptcha(document.getElementById('imgCapcha'),'<?php echo DIR_WS_SITE_INCLUDES?>classes/class.thumbnail.php?Captcha=1&Time=');" href="#">Try a different image</a></label>
									<div class="col-sm-8">
										<img src="<?php echo DIR_WS_SITE_INCLUDES?>classes/class.thumbnail.php?Captcha=1&Time=<?php echo date('YmdHis')?>" id="imgCapcha">
									</div>
								</div>
								
								<div class="form-group">
									<label for="email" class="control-label col-sm-4">Security Code: *</label>
									<div class="col-sm-8">
										<input type="text" name="SecurityCode" id="SecurityCode_R" placeholder="Security Code" title="Please enter Security Code" class="form-control" required />
									</div>
								</div>
								
							
							
							<div class="form-group">
                           		 <label for="FormData_AcceptTerms" class="control-label col-sm-4">I have read and agree to the <a href="<?php echo SKSEOURL(1087,"cms")?>" target="_blank">terms and condition</a> </label>
								<div class="col-sm-8"><input type="checkbox" name="Terms" value="1" required  /></div>
							</div>
                                
								
							<div class="form-group">
							<div class="col-sm-6 pull-right">
								<button type="submit" class="btn btn-default Button">Submit</button>
							</div>
						</div>
						</div>
					</div>
				</div>
							</form>
				
		<?php /* middle content end*/?>
				
            </div>
        </div>