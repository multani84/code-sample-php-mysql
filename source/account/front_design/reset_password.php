<section id="MainContent">
        <div id="CMSPage" class="container vh100">
            <div class="EditorContent">
                <?php echo ((isset($CurrentPage->LeftTitle1) && $CurrentPage->LeftTitle1 !="")?"<h1 class='Title'>".MyStripSlashes($CurrentPage->LeftTitle1)."</h1>":"");?>
				<?php/* middle content start*/?>
				<?php
				require_once(DIR_FS_SITE_INCLUDES."message.php");?>
		    	<div style="display:inline-block;width:100%;">
					<?php
	    If(is_object($CurrentUserObj) && $CurrentUserObj->UserName !="" && $code !="")
		{
			?>
			<form  class="form-horizontal" name="FrmChangePassword" id="FrmChangePassword" action="<?php echo MakePageURL("index.php","Page=$Page&code=$code&Target=Reset")?>" method="POST">
				<div class="well">
				
				<div class="form-group">
					<label for="email" class="col-sm-4 col-md-5 col-lg-4 control-label">Username: *</label>
					<div class="col-sm-20 col-md-19 col-lg-20">
						<?php echo $CurrentUserObj->UserName?>
					</div>
				</div>
				
				<div class="form-group">
					<label for="email" class="col-sm-4 col-md-5 col-lg-4 control-label">New Password: *</label>
					<div class="col-sm-20 col-md-19 col-lg-20">
						<input class="form-control"  name="Password" id="Password_R" value="" title="" placeholder="Password" type="password" value="" required />
					</div>
				</div>
				
				<div class="form-group">
					<label for="email" class="col-sm-4 col-md-5 col-lg-4 control-label">Retype Password: *</label>
					<div class="col-sm-20 col-md-19 col-lg-20">
						<input class="form-control"  name="RPassword" id="CPassword_C" value="" title="" placeholder="Retype Password" type="password" value="" required />
					</div>
				</div>
				
				<div class="form-group">
					<div class="col-sm-6 pull-right">
						<button class="btn btn-compare  btn-block" type="submit">Submit</button>
					</div>
				</div>
				
				
				</div>
			</form>	
			<?php
		}
		else 
		{
	    ?>
			<b>Sorry, Your code is invalid.</b>
			<?php
		}?>
				</div>
				<?php/* middle content end*/?>
				
            </div>
        </div>
    </section>
	


