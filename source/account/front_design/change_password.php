<div id="MainContent">
    	<div class="container"> 
            <ul class="breadcrumb">
                <li><a href="<?php echo SKSEOURL("1001","cms")?>">Home</a></li>
                <li class="active">Change Password</li>
            </ul>
                <h1>Change Password</h1>
				<?php /* middle content start*/?>
				<?php require_once(DIR_FS_SITE_INCLUDES."message.php");?>
				
				<form  class="form-horizontal" name="FrmChangePassword" id="FrmChangePassword" action="<?php echo MakePageURL("index.php","Page=$Page")?>" method="POST">
					<div class="row">
                             <div class="panel panel-warning">
								<div class="panel-body">
								<h1>Welcome <?php echo isset($CurrentUserObj->FirstName)?$CurrentUserObj->FirstName:""?></h1>
								<div class="form-group">
									<label for="email" class="control-label col-sm-4">Username: </label>
									<div class="col-sm-8">
										<?php echo $CurrentUserObj->UserName?>
									</div>
								</div>
								<div class="form-group">
									<label for="email" class="control-label col-sm-4">Old Password: *</label>
									<div class="col-sm-8">
										<input class="form-control"  name="OldPassword" id="OldPassword_R" value="" title="" placeholder="Old Password" type="password" value="" required />
									</div>
								</div>
								
								<div class="form-group">
									<label for="email" class="control-label col-sm-4">New Password: *</label>
									<div class="col-sm-8">
										<input class="form-control"  name="Password" id="Password_R" value="" title="" placeholder="New Password" type="password" value="" required />
									</div>
								</div>
								
								<div class="form-group">
									<label for="email" class="control-label col-sm-4">Retype Password: *</label>
									<div class="col-sm-8">
										<input class="form-control"  name="RPassword" id="RPassword_R" value="" title="" placeholder="Retype Password" type="password" value="" required />
									</div>
								</div>
								
								<div class="form-group">
									<div class="col-sm-6 pull-right">
										<button class="btn btn-default Button" type="submit">Submit</button>
									</div>
								</div>
								
							</div></div></div>
							</form>
						
            </div>
        </div>
   
	


