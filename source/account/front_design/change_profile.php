<div id="MainContent">
    	<div class="container"> 
            <ul class="breadcrumb">
                <li><a href="<?php echo SKSEOURL("1001","cms")?>">Home</a></li>
                <li class="active">Change Profile</li>
            </ul>
                <h1>Change Profile</h1>
				<?php /* middle content start*/?>
				<?php require_once(DIR_FS_SITE_INCLUDES."message.php");?>
				
				<form class="form-horizontal" role="form" method="POST" action="<?php echo MakePageURL("index.php","Page=$Page");?>" id="RegistrationForm" name="RegistrationForm">
							<div class="row">
                             <div class="panel panel-warning">
								<div class="panel-body">
								<h1>User Details</h1>
								
								<div class="form-group">
									<label for="email" class="control-label col-sm-4">Email:</label>
									<div class="col-sm-8"><?php echo $CurrentUserObj->UserName?></div>
								</div>
								<?php if($CurrentUserObj->UserTypeID=='3'){ ?>
								<div class="form-group">
									<label for="email" class="control-label col-sm-4">Partner Code:</label>
									<div class="col-sm-8"><?php echo $CurrentUserObj->PartnerCode?></div>
								</div>
								<?php } ?>
								<?php if($CurrentUserObj->UserTypeID=='2'){ ?>
								<div class="form-group">
									<label for="email" class="control-label col-sm-4">Reference Code:</label>
									<div class="col-sm-8">
									<?php if($CurrentUserObj->ReferenceCode!=""){
										echo $CurrentUserObj->ReferenceCode;
									}
									else{?>
										<input class="form-control"  name="ReferenceCode" id="ReferenceCode" value="<?php echo @$DataArray['ReferenceCode']?>" title="" placeholder="Reference Code" type="text" value=""  />
									
									<?php } ?>
									</div>
								</div>
								<?php } ?>
								<div class="form-group">
									<label for="email" class="control-label col-sm-4">Name: *</label>
									<div class="col-sm-8">
										<input class="form-control"  name="FirstName" id="FirstName_R" value="<?php echo @$DataArray['FirstName']?>" title="" placeholder="Name" type="text" value="" required />
									</div>
								</div>
								
								<div class="form-group">
									<label for="email" class="control-label col-sm-4">Contact Phone: *</label>
									<div class="col-sm-8">
										<input class="form-control"  name="Phone" id="Phone_R" value="<?php echo @$DataArray['Phone']?>" title="" placeholder="Phone" type="text" value="" required />
									</div>
								</div>
								
								<div class="form-group">
									<label for="email" class="control-label col-sm-4">Address1: *</label>
									<div class="col-sm-8">
										<input class="form-control"  name="Address1" id="Address1_R" value="<?php echo @$DataArray['Address1']?>" title="" placeholder="Address1" type="text" value="" required />
									</div>
								</div>
								
								<div class="form-group">
									<label for="email" class="control-label col-sm-4">Address2: </label>
									<div class="col-sm-8">
										<input class="form-control"  name="Address2" id="Address2" value="<?php echo @$DataArray['Address2']?>" title="" placeholder="Address2" type="text" value="" />
									</div>
								</div>
								
								<div class="form-group">
									<label for="email" class="control-label col-sm-4">Town/City: *</label>
									<div class="col-sm-8">
										<input class="form-control"  name="City" id="City_R" value="<?php echo @$DataArray['City']?>" title="" placeholder="City" type="text" value="" required />
									</div>
								</div>
								
								<div class="form-group">
									<label for="email" class="control-label col-sm-4">County/State: *</label>
									<div class="col-sm-8"
									data-input='<input type="text" name="State" value="<?php echo @$DataArray['State']?>" id="State_R" title="Please enter County." class="form-control">'
									data-select='<select data-val="<?php echo @$DataArray['State']?>" name="State" class="form-control" id="State_R" class="form-control" title="Please select State" required></select>'
									>
									<input  class="form-control"  type="text" name="State" value="<?php echo @$DataArray['State']?>" id="State_R" title="Please enter State." placeholder="State" required />
									</div>
								</div>
								
								<div class="form-group">
									<label for="email" class="control-label col-sm-4">Postal/Zip Code: *</label>
									<div class="col-sm-8">
										<input class="form-control"  name="ZipCode" id="ZipCode_R" value="<?php echo @$DataArray['ZipCode']?>" title="" placeholder="Zip Code" type="text" value="" required />
									</div>
								</div>
								
								<div class="form-group">
									<label for="email" class="control-label col-sm-4">Country: *</label>
									<div class="col-sm-8">
											<select rel="sk_country" data-regionid="#State_R" data-value="<?php echo @$DataArray['Country']?>" name="Country" id="Country_R" title="Please select Country." class="form-control"></select>
									</div>
								</div>
								
								
								
							</div>
							
							<div class="col-sm-24">
							<div class="row">
								<div class="col-sm-18"></div>
								<div class="col-sm-6 pull-right"><button type="submit" class="btn btn-default Button">Submit</button></div>
							</div>
						</div></div></div>
							</form>
				
		<?php /* middle content end*/?>
				
            </div>
        </div>

	
	
