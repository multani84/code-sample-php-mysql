<div id="MainBanners" class="clearfix"><img src="<?php echo DIR_WS_SITE_TEMPLATES.DEFINE_TEMPLATE_STYLE?>/images/bnr.jpg" alt="Banner" /></div>
<div id="MainContent" class="clearfix"> 
	<div class="CMSContent">
	<?php require_once(DIR_FS_SITE_INCLUDES."breadcrumb.php");?>
    <h2 class='Title'><?php echo (isset($CurrentProduct->ProductName)?MyStripSlashes($CurrentProduct->ProductName)."":"Recommend a friend");?></h2>
			<?php/* middle content start*/?>
				<?php
				require_once(DIR_FS_SITE_INCLUDES."message.php");?>
		    	<div style="display:inline-block;width:100%;">
					<?php
	    if(isset($ThankMsg) && $ThankMsg !="")
	    {
	    	echo "<div><b>".$ThankMsg."</b></div>";
	    }
	    else 
	    {
	    ?>
	    <div align="left">
	    Recommend a friend using the form below and we will send you a discount voucher if they sign-up and make a purchase!
	    </div>
			<form name="RegistrationForm" action="<?php echo MakePageURL("index.php","Page=$Page&Popup=true&ProductID=$ProductID&Target=SendRequest")?>" id="RegistrationForm" method="POST" onSubmit="return ValidateForm(this);">
                    <table cellpadding="3" width="100%" align="center" border="0" cellspacing="2" class="TableFormat">
                      <tbody>
                        <tr valign=top>
                            <td align="left">
                          		<table border="0" width="100%" cellspacing="2" cellpadding="2">
			                      <tr valign=top>
			                          <td nowrap width="30%" valign="middle">Your Name<font size="2">*</font></td>
			                          <td><input type="text" size=24 name="Name"  value="<?php echo isset($IsCurrentUser->FirstName)?$IsCurrentUser->FirstName." ".$IsCurrentUser->LastName:""?>" id="Name_R" title="Please enter your Name"></td>
			                      </tr>
    				              <tr valign=top>
			                          <td nowrap width="30%" valign="middle">Your Email<font size="2">*</font></td>
			                          <td><input type="text" size=24 name="Email"  value="<?php echo isset($IsCurrentUser->Email)?$IsCurrentUser->Email:""?>" id="Email_E" title="Invalid Email Address ! Please try user@domain.com"></td>
			                      </tr>
    				              <tr valign=top>
			                          <td nowrap width="30%" valign="middle">Friend's Name<font size="2">*</font></td>
			                          <td><input type="text" size=24 name="FName"  value="" id="FName_R" title="Please enter your Friend's Name"></td>
			                      </tr>
    				              <tr valign=top>
			                          <td nowrap width="30%" valign="middle">Friend's Email<font size="2">*</font></td>
			                          <td><input type="text" size=24 name="FEmail"  value="" id="FEmail_E" title="Invalid Email Address ! Please try user@domain.com"></td>
			                      </tr>
    				              <tr>
			                        <td colspan="2"><textarea title="Please enter your message" name="MessageBody" id="MessageBody_R" wrap="soft" cols="50" rows="7"><?php echo isset($_POST['MessageBody'])?$_POST['MessageBody']:$MessageBody?></textarea></td>
								 </tr>
			                    </table>
                          </td>
                        </tr>
                        <tr valign=top>
                          <td><!--<input type="reset" value="Reset">--><br>
                          <table  border="0" cellspacing="0" cellpadding="0">
				           <tr>
				             <td width="50px"></td>
				             <td width="50px" style="padding-left:10px"><a name="Register" href="javascript:;" onclick="return ValidateForm('RegistrationForm');"  class="frmButton">Submit</a>
				             <input type="image" name="DefaultSubmit" src="<?php echo DIR_WS_SITE_GRAPHICS?>dot.png" class="DefaultSubmit"></td>
				             </tr></table><br>
                            </td>
                        </tr>
                      </tbody>
                    </table>
                </form>	
                <?php
	    }?>
				</div>
				<?php/* middle content end*/?>
	</div>
</div>

