<div id="MainContent">
    	<div class="container"> 
            <ul class="breadcrumb">
                <li><a href="<?php echo SKSEOURL("1001","cms")?>">Home</a></li>
                <li class="active">Forgot Password</li>
            </ul>
            <h1>Forgot Password</h1>
				<?php /* middle content start*/?>
				<?php require_once(DIR_FS_SITE_INCLUDES."message.php");?>
				
				<?php 
				echo (isset($CurrentPage->LeftDescription1)?MyStripSlashes($CurrentPage->LeftDescription1):"&nbsp;");
				?>
				<form class="form-horizontal" role="form" method="POST" action="<?php echo MakePageURL("index.php","Page=$Page");?>" id="FormRecoverPassword" name="FormRecoverPassword">
							<div class="row">
                             <div class="panel panel-warning">
								<div class="panel-body">
								
								<div class="form-group">
									<label for="email" class="control-label col-sm-4">Email: *</label>
									<div class="col-sm-8">
										<input class="form-control"  name="Email" id="Email_E" value="<?php echo isset($_SESSION['Email']) ? $_SESSION['Email']:""?>" title="Invalid email ! Please try user@domain.com" placeholder="Email Address" type="email" value="" required />
									</div>
								</div>
								
								<div class="form-group">
									<div class="col-sm-6 pull-right">
										<button onclick="return ValidateForm('FormRecoverPassword');"  class="btn btn-default Button" type="submit">Submit</button>
									</div>
								</div>
								
							</div></div></div>
							</form>
				
				
            </div>
        </div>
   
	


