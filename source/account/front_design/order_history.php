<div id="MainContent">
    	<div class="container"> 
            <ul class="breadcrumb">
                <li><a href="<?php echo SKSEOURL("1001","cms")?>">Home</a></li>
                <li class="active">Order History</li>
            </ul>
                <h1>Order History</h1>
				<?php /* middle content start*/?>
				
					<?php
		switch ($Section)
		{
			case "FullDetail":
				$OrderID = $OID;
				
				$OrderObj->Where ="UserID='".$OrderObj->MysqlEscapeString($CurrentUserObj->UserID)."' AND OrderID = '".$OrderID."'";
				$CurrentOrder = $OrderObj->TableSelectOne(array("*, DATE_FORMAT(CreatedDate,'%M %d, %Y %r')as MyCreatedDate, DATE_FORMAT(CreatedDate,'%M %d, %Y')as MyShortCreatedDate, DATE_FORMAT(CreatedDate,'%y%m%d')as FormattedCreatedDate"));
				if(is_object($CurrentOrder)):
				
					$CurrencyOrderID = $CurrentOrder->OrderID;		
					$ShoppingCalculationObj->OrderID = $OrderID;
					$ShoppingCalculationObj->Calculate();
						
					$SKCartTable =TABLE_ORDER_DETAILS;
					$CartObj = new DataTable(TABLE_ORDER_DETAILS);
					$CartObj2 = new DataTable(TABLE_ORDER_DETAILS);

					$CartObj->Where =" OrderID='".$CartObj->MysqlEscapeString($OrderID)."'";
					$CartObj->TableSelectAll(array("*"));

								
					$CartObj->Where =" OrderID='".$CartObj->MysqlEscapeString($OrderID)."' AND (ParentItemID='' OR ISNULL(ParentItemID))";
					$CartObj->TableSelectAll("","OrderDetailID ASC");
								
			?>
			
<ul class="nav nav-tabs">
  <li class="active"><a data-toggle="tab" href="#OrderSummary"> 1. Order Details</a></li>
  <li><a data-toggle="tab" href="#OrderStatus">2. Status/notes</a></li>
</ul>

<div class="tab-content">
	  <div id="OrderSummary" class="tab-pane fade in active">
		<h3>Order Summary</h3>
			<?php
				if ($CartObj->GetNumRows()>0)
					require_once(DIR_FS_SITE."source/shop/show_cart.php");
					require_once(DIR_FS_SITE."source/shop/shipping_detail.php");
				
			?>
	  </div>
 			
			<!--  Tab Content Block -->
			<div class="tab-pane fade" id="OrderStatus">
				<h3>Order Status</h3>
				<?php
					if(@constant("DEFINE_SHIPPING_INFO_DISPLAY")=="1")
					{
					?>
					<br/>
						<?php
						$OrderChangeStatusObj = new DataTable(TABLE_ORDER_CHANGE_STATUS);
						$OrderChangeStatusObj->Where = "OrderID='".$CurrentOrder->OrderID."' AND Notify='1'";
						$OrderChangeStatusObj->TableSelectAll(array("*,DATE_FORMAT(CreatedDate,'%M %d, %Y')as MyCreatedDate"),"CreatedDate DESC");
						if($OrderChangeStatusObj->GetNumRows() > 0)
						{
						?>
						 <table class="table table-striped table-bordered table-hover">
								<thead>
									<tr>
										<th>Status</th>
										<th>Comments</th>
										<th><b>Created Date</b></th>								
									</tr>
									</thead>
									<tbody>
									<?php
									while ($CurrentOrderStatus = $OrderChangeStatusObj->GetObjectFromRecord())
									{?>
									<tr class="InsideRightTd">
										<td><?php echo MyStripSlashes($CurrentOrderStatus->OrderStatus)?></td>
										<td><?php echo MyStripSlashes(nl2br($CurrentOrderStatus->Comments))?></td>
										<td><?php echo MyStripSlashes($CurrentOrderStatus->MyCreatedDate)?></td>								
									</tr>
									<?php
									}?>
									</tbody>
								</table>
							
						<?php
						}
						else
						{
							?>
							No record found.
							<?php
						}
						?>
						
				<?php }?>
				
			</div>	
			<!--  End Tab Content Block -->
  
  
</div>
		<?php
		endif;			
			break;
		default:
			$OrderObj = new DataTable(TABLE_ORDERS);
			//$OrderObj->DisplayQuery = true;
			$OrderObj->Where = "UserID='".$OrderObj->MysqlEscapeString($CurrentUserObj->UserID)."'";
			$OrderObj->TableSelectAll(array("*, DATE_FORMAT(CreatedDate,'%b %d, %Y')as MyCreatedDate, DATE_FORMAT(CreatedDate,'%M, %Y')as MyCreatedMonth"),"CreatedDate DESC",10);
			if($OrderObj->GetNumRows() > 0)
			{
			?>
			<div class="table-responsive">  
			<table class="table table-bordered">
			 	<thead>
				<tr>
					<th align="center" height="25"><b>S.No</b></th>
					<th align="left"><b>Order Date</b></th>
					<th align="left"><b>Order No</b></th>
					<th align="left"><b>Method</b></th>
					<th align="center"><b>Order Status</b></th>
					<th class="text-right"><b>Amount&nbsp;</b></th>
					<th align="center"></th>
				</tr>
				</thead>
				<tbody>
				<?php
				$SNo = 1;
			 	while($CurrentOrder = $OrderObj->GetObjectFromRecord())
				{
					$CurrencyOrderID = $CurrentOrder->OrderID;
					?>
					<tr>
						<td align="left"  style="height:50px;"><b><?php echo $SNo?></b></td>
						<td align="left" ><?php echo $CurrentOrder->MyCreatedDate?></td>
						<td align="left" ><?php echo $CurrentOrder->OrderNo?></td>
						<td align="left" ><?php echo $CurrentOrder->PaymentMethod?></td>
						<td align="left" ><?php echo $CurrentOrder->OrderStatus?>
						<?php
						if($CurrentOrder->TrackingCode !="")
						{
							?>
							<br>
							<br>
							Track your Order<br>
							<?php echo $CurrentOrder->TrackingCode?>
							<?php
						}
						?>
						</td>
						<td align="right" ><?php echo Change2CurrentCurrency($CurrentOrder->GrandTotal)?></td>
						<td align="left" ><a href='<?php echo MakePageURL("index.php","Page=$Page&Section=FullDetail&OID=$CurrentOrder->OrderID")?>' class='btn btn-default'>Details</a></td>
					</tr>
			
					<?php
					$SNo++;
			    }?>
				</tbody>
			 </table>
			 </div>
			<?php
			}
			else
			{
				echo "There is no any order in your account.";
			}
		 break;
		}
			?>
				
				<?php /* middle content end*/?>
				
            <p>&nbsp;</p>
			<p>&nbsp;</p>
			<p>&nbsp;</p>
			</div>
        </div>