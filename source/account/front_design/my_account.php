<div id="MainContent">
    	<div class="container"> 
            <ul class="breadcrumb">
                <li><a href="<?php echo SKSEOURL("1001","cms")?>">Home</a></li>
                <li class="active">MY ACCOUNT</li>
            </ul>
        	<h1>MY ACCOUNT</h1>


				<?php /* middle content start*/?>
				<?php require_once(DIR_FS_SITE_INCLUDES."message.php");?>
				
				<?php echo (isset($CurrentPage->LeftDescription1)?MyStripSlashes($CurrentPage->LeftDescription1):"&nbsp;");
				?>
					<div class="row">
                     <div class="panel panel-warning">
                     <div class="panel-body">
						<h1 class="list-group-item" style="color:black;">Welcome <?php echo isset($CurrentUserObj->FirstName)?$CurrentUserObj->FirstName:""?></h1>
						
                            
								
						  <a href="<?php echo MakePageURL("index.php","Page=account/change_profile")?>" class="list-group-item">Change Profile</a>
						  <a href="<?php echo MakePageURL("index.php","Page=account/change_password")?>" class="list-group-item">Change Password</a>
						  <a href="<?php echo MakePageURL("index.php","Page=account/order_history")?>" class="list-group-item">View Order History</a>

						  <?php if($CurrentUserObj->UserTypeID=='3'){ ?>
							<a href="<?php echo MakePageURL("index.php","Page=account/partner_order_history")?>" class="list-group-item">View Reference Order History</a>
						  <?php	}?>

						  <?php /* <a href="<?php echo MakePageURL("index.php","Page=account/address_book")?>" class="list-group-item">My Address Book</a> */ ?>
						  <a href="<?php echo MakePageURL("index.php","Page=account/logout")?>" class="list-group-item">Logout</a>
						</div></div></div>
					
				
		<?php /* middle content end*/?>
				
            </div>
        </div>

	