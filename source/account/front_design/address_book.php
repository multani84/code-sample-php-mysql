<div id="MainContent">
    	<div class="container"> 
            <ul class="breadcrumb">
                <li><a href="<?php echo SKSEOURL("1001","cms")?>">Home</a></li>
                <li class="active">Forgot Password</li>
            </ul>
                <h1>Address Book</h1>
				
				<?php
				require_once(DIR_FS_SITE_INCLUDES."message.php");?>
		    	
					<?php switch ($Section)
{
	case "AddAddress":
	case "EditAddress":
	
	?>
		<form class="form-horizontal" role="form" method="POST" action="<?php echo MakePageURL("index.php","Page=$Page&Section=$Section&Target=AddAddress&AddressID=$AddressID")?>" id="RegistrationForm" name="RegistrationForm">
			<input type="hidden" value="<?php echo $ReturnURL?>" name ="ReturnURL">
			<div class="row">
                             <div class="panel panel-warning">
								<div class="panel-body">
				<div class="form-group">
					<label for="email" class="control-label col-sm-4">Name: *</label>
					<div class="col-sm-8">
						<input class="form-control"  name="FirstName" id="FirstName_R" value="<?php echo isset($CurrentAddress->FirstName)?$CurrentAddress->FirstName:""?>" title="" placeholder="Name" type="text" value="" required />
					</div>
				</div>
				
				<div class="form-group">
					<label for="email" class="control-label col-sm-4">Phone: *</label>
					<div class="col-sm-8">
						<input class="form-control"  name="Phone" id="Phone_R" value="<?php echo isset($CurrentAddress->Phone)?$CurrentAddress->Phone:""?>" title="" placeholder="Phone" type="text" value="" required />
					</div>
				</div>
				
				<div class="form-group">
					<label for="email" class="control-label col-sm-4">Address1: *</label>
					<div class="col-sm-8">
						<input class="form-control"  name="Address1" id="Address1_R" value="<?php echo isset($CurrentAddress->Address1)?$CurrentAddress->Address1:""?>" title="" placeholder="Address1" type="text" value="" required />
					</div>
				</div>
				
				<div class="form-group">
					<label for="email" class="control-label col-sm-4">Address2: </label>
					<div class="col-sm-8">
						<input class="form-control"  name="Address2" id="Address2" value="<?php echo isset($CurrentAddress->Address2)?$CurrentAddress->Address2:""?>" title="" placeholder="Address2" type="text" value="" />
					</div>
				</div>
				
				<div class="form-group">
					<label for="email" class="control-label col-sm-4">Town/City: *</label>
					<div class="col-sm-8">
						<input class="form-control"  name="City" id="City_R" value="<?php echo isset($CurrentAddress->City)?$CurrentAddress->City:""?>" title="" placeholder="City" type="text" value="" required />
					</div>
				</div>
				
				<div class="form-group">
					<label for="email" class="control-label col-sm-4">County/State: *</label>
					<div class="col-sm-8"
					
					data-input='<input type="text" name="State" value="<?php echo isset($CurrentAddress->State)?$CurrentAddress->State:""?>" id="State_R" title="Please enter County." class="form-control">'
					data-select='<select data-val="<?php echo isset($CurrentAddress->State)?$CurrentAddress->State:""?>" name="State" class="form-control" id="State_R" class="form-control" title="Please select State" required></select>'
					>
					<input  class="form-control"  type="text" name="State" value="<?php echo isset($CurrentAddress->State)?$CurrentAddress->State:""?>" id="State_R" title="Please enter State." placeholder="State" required />
					</div>
				</div>
				
				<div class="form-group">
					<label for="email" class="control-label col-sm-4">Postal/Zip Code: *</label>
					<div class="col-sm-8">
						<input class="form-control"  name="ZipCode" id="ZipCode_R" value="<?php echo isset($CurrentAddress->ZipCode)?$CurrentAddress->ZipCode:""?>" title="" placeholder="Zip Code" type="text" value="" required />
					</div>
				</div>
				
				<div class="form-group">
					<label for="email" class="control-label col-sm-4">Country: *</label>
					<div class="col-sm-8">
							<select rel="sk_country" data-regionid="#State_R" data-value="<?php echo (isset($CurrentAddress->Country)?$CurrentAddress->Country:"")?>" name="Country" id="Country_R" title="Please select Country." class="form-control"></select>
					
					</div>
				</div>
			</div>					
			<div class="col-sm-24">
				<div class="row">
					<div class="col-sm-18"></div>
					<div class="col-sm-6 pull-right"><button type="submit" class="btn btn-default Button">Submit</button></div>
				</div>
			</div>
	       </div></div> 
        </form>	
	<?php
	
	break;
	default:
?>
<div class="row">
                             <div class="panel panel-warning">
								<div class="panel-body">


 
 
    <h4 class="list-group-item-heading">This is your Default Address</h4>
    <p class="list-group-item-text">
		<div class="pull-right"><a href="<?php echo MakePageURL("index.php","Page=change_profile")?>" class="btn btn-success">Edit Address</a></div>
		<?php
		  echo $CurrentUserObj->FirstName." ".$CurrentUserObj->LastName."<br>";
		  echo $CurrentUserObj->Address1." ".$CurrentUserObj->Address2."<br>";
		  echo $CurrentUserObj->City." ".$CurrentUserObj->State."<br>";
		  echo $CurrentUserObj->Area." ".$CurrentUserObj->Country." ".$CurrentUserObj->ZipCode."<br>";
		  ?>
		  
	</p>
  </div>
  
	<?php
	
	$AddressObj->Where ="UserID='".$CurrentUserObj->UserID."'";
	$AddressObj->TableSelectAll("","CreatedDate ASC");
	while ($CurrentAddress = $AddressObj->GetObjectFromRecord())
	{
	?>
	<div class="list-group-item">
    <p class="list-group-item-text">
		<div class="pull-right">
			<a href="<?php echo MakePageURL("index.php","Page=$Page&Section=EditAddress&AddressID=$CurrentAddress->AddressID")?>" class="btn btn-default Button">Edit Address</a>
			<a onclick="return confirm('Are you sure want to delete the address?')" href="<?php echo MakePageURL("index.php","Page=$Page&Target=DeleteAddress&AddressID=$CurrentAddress->AddressID")?>" class="btn btn-danger">Delete Address</a>
		</div>
		 <?php
		  echo $CurrentAddress->FirstName." ".$CurrentAddress->LastName."<br>";
		  echo $CurrentAddress->Address1." ".$CurrentAddress->Address2."<br>";
		  echo $CurrentAddress->City." ".$CurrentAddress->State."<br>";
		  echo $CurrentAddress->Area." ".$CurrentAddress->Country." ".$CurrentAddress->ZipCode."<br>";
		  ?>
		  
	</p>
  
	
	<?php
	}?>
  
</div></div></div>

<div class="col-sm-24">
	<div class="row">
		<div class="col-sm-18"></div>
		<div class="col-sm-6 pull-right">
		<a href="<?php echo MakePageURL("index.php","Page=$Page&Section=AddAddress")?>" class="btn btn-default Button">Add New Address</a>
	</div>
</div>
<?php
break;
}?>
				</div>
				<?php /* middle content end*/?>
				
            
        </div>
</div>
	
	
	