<div id="MainContent">
    	<div class="container"> 
            <ul class="breadcrumb">
                <li><a href="./">Home</a></li>
                <li class="active">Login</li>
            </ul>
			<?php require_once(DIR_FS_SITE_INCLUDES."message.php");?>
        	<h1>Login</h1>
            <div class="row">
                <div class="col-md-6">
                    <div class="panel panel-warning">
                        <div class="panel-heading">New Customer</div>
                        <div class="panel-body" style="min-height:185px;">
                            <div class="form-horizontal">
                                <p>Not Registered yet? Enter your details to start benefiting from faster shopping and easy access to your account information and order history.</p><br />
                                 <a href="<?php echo MakePageURL("index.php","Page=account/sign_up");?>" class="btn btn-primary Button pull-right">New User</a><br />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="panel panel-warning">
                        <div class="panel-heading">Existing Customer</div>
                        <div class="panel-body">
                            <form class="form-horizontal" role="form" method="POST" action="<?php echo MakePageURL("index.php","Page=account/login")?>" id="FormLogin" name="FormLogin">
                                <div class="form-group">
                                    <label class="control-label col-sm-3" for="email">Email: <b>*</b></label>
                                    <div class="col-sm-9">
                                        <div class="input-group">
                                            <span class="input-group-addon">@</span>
                                            <input type="email" class="form-control"  name="UserName" id="UserName_E" value="<?php echo  isset($CurrentParcel->FromEmail)?$CurrentParcel->FromEmail:""?>"  placeholder="e.g. user@domain.com" title="Invalid email ! Please try user@domain.com" required/>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-3" for="pwd">Password: <b>*</b></label>
                                    <div class="col-sm-9">
                                        <div class="input-group">
                                            <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
                                            <input type="password" class="form-control" name="Password" id="Password_R" title="<?=@constant("DEFINE_BLANK_PASSWORD_MSG")?>" placeholder="Enter password" required/>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-offset-3 col-sm-2">
									<button type="submit" class="btn btn-primary Button">Login <span class="glyphicon glyphicon-log-in"></span></button>
									</div>
                                    <div class="col-sm-7 text-right">Forgot password? <a href="<?=MakePageURL("index.php","Page=account/forgot_password");?>">Click here</a></div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>


		 <?php if(DEFINE_CHECKOUT_WITHOUT_LOGIN=="1" && @$_SESSION['ReturnURL']==MakePageURL("index.php","Page=shop/shop_checkout")){?>
                <div class="col-md-12">
                    <div class="panel panel-warning">
                        <div class="panel-heading">Skip Login and Checkout as Guest</div>
                        <div class="panel-body">
                            <p>If so please click on the button below to your right. Please note that if you do not create an account, you can not view the status of your order, and some of our services may not be available. Creating an account is free. If you wish to open one now click the new user button above. If you still wish to continue to checkout without creating an account, please click the button to your right.</p>
                                <div class="text-right">
								<a href="<?php echo MakePageURL("index.php","Page=shop/shop_checkout");?>" class="btn btn-default Button">Checkout 
								<span class="glyphicon glyphicon-chevron-right"></span></a></div>
                         </div>
                    </div>                    	
                </div>
				<?php }?>
            </div>
        </div>
    </div>






