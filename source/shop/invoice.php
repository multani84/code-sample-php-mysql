<?php
if($InvoiceSent)
{
	global $CurrencyOrderID;
	$OrderObj = new DataTable(TABLE_ORDERS);
	$OrderObj->Where ="OrderID='".$OrderID."'";
	$CurrentOrder = $OrderObj->TableSelectOne(array("*, DATE_FORMAT(CreatedDate,'%M %d, %Y %r')as MyCreatedDate, DATE_FORMAT(CreatedDate,'%M %d, %Y')as MyShortCreatedDate, DATE_FORMAT(CreatedDate,'%y%m%d')as FormattedCreatedDate"));
	$CurrencyOrderID = $CurrentOrder->OrderID;	

	$ShoppingCalculationObj = new ShoppingCalculations();
	$ShoppingCalculationObj->OrderID = $OrderID;
	$ShoppingCalculationObj->Calculate();
		
	$SKCartTable =TABLE_ORDER_DETAILS;
	$CartObj = new DataTable(TABLE_ORDER_DETAILS);
	$CartObj2 = new DataTable(TABLE_ORDER_DETAILS);

	$CartObj->Where =" OrderID='".$CartObj->MysqlEscapeString($OrderID)."'";
	$CartObj->TableSelectAll(array("*"));

	
	$CartObj->Where =" OrderID='".$CartObj->MysqlEscapeString($OrderID)."' AND (ParentItemID='' OR ISNULL(ParentItemID))";
	$CartObj->TableSelectAll("","OrderDetailID ASC");
				
	?><head>
</head>

<div align="center" class="InvoiceStart">

<table border="0" cellspacing="4" cellpadding="0" width="80%" bgcolor="gainsboro">
    <tr>
        <td>
            <table border=0 cellspacing=2 cellpadding=2 width=100% bgcolor=white>
                <tr>
                    <td>
                        <table border='0' cellpadding='1' cellspacing='1' width='100%' class="tabular">
                            <tr>
                                <td align="center" style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size:35px; font-weight: bold; line-height:50px; background-color:#db291d; color:#FFFFFF;">INVOICE</td>
								</tr>
								<tr>
									<td align="center" style="background-color:#000000;">
										<?php if(file_exists(DIR_FS_SITE_TEMPLATES.DEFINE_TEMPLATE_STYLE."/images/invoice-logo.jpg")):?>
											<img src="<?php echo DIR_WS_SITE_TEMPLATES.DEFINE_TEMPLATE_STYLE."/images/invoice-logo.jpg"?>" border="0">        	
										<?php else:?>
											<?php echo DEFINE_SITE_NAME?>
										<?php endif;?>
									</td>
								</tr>      

								  <tr>
									<td valign="top">
									 <table width="100%" border="0" cellpadding="0" cellspacing="0">
									  <tr style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size:12px; font-weight: normal; line-height:24px;">
										<td valign="top" colspan="2" width="53%" align="center" style="padding:5px;color:#fff;background-color:#000000; ">
										<!--<h1><?php echo @constant("DEFINE_ADDRESS_LINE_1") !=""?@constant("DEFINE_ADDRESS_LINE_1")."<br>":""?></h1>-->
										<?php echo @constant("DEFINE_ADDRESS_LINE_2") !=""?@constant("DEFINE_ADDRESS_LINE_2")." ":""?>
										<?php echo @constant("DEFINE_ADDRESS_LINE_3") !=""?@constant("DEFINE_ADDRESS_LINE_3")." ":""?>
										<?php echo @constant("DEFINE_ADDRESS_LINE_4") !=""?@constant("DEFINE_ADDRESS_LINE_4")." ":""?>
										<?php echo @constant("DEFINE_ADDRESS_LINE_5") !=""?@constant("DEFINE_ADDRESS_LINE_5")." ":""?>
										<?php echo @constant("DEFINE_ADDRESS_LINE_6") !=""?@constant("DEFINE_ADDRESS_LINE_6")." ":""?>
										E-mail: <a href="mailto:<?php echo DEFINE_SALES_EMAIL?>" style="color:#db291d; "><?php echo DEFINE_SALES_EMAIL?></a><br>
										<!--Website: <a href="<?php echo DIR_WS_SITE?>"><?php echo DEFINE_SITE_NAME?></a><br>-->
										</td>
									  </tr>
									</table>
									</td>
								  </tr>
								
								  <tr>
									<td>        
									<br>
									<table width="100%" border="0" cellpadding="0" cellspacing="0">
									   <tr style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size:12px; font-weight: normal; line-height:24px;">
									  	<th align="left" width="53%" bgcolor="#CCCCCC" style="color:#000000; padding:8px; text-transform:uppercase;">Order Details</th>
										<th align="right" width="47%" bgcolor="#CCCCCC" style="color:#000000; padding:8px; text-transform:uppercase; ">Payment Details</th>
									  </tr>
									 <tr style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size:12px; font-weight: normal; line-height:24px;">
									   <td valign="top" width="53%"  style="padding:10px; ">
									   <b>Order No. </b><?php echo $CurrentOrder->OrderNo?><br>
										<b>Order Date</b>: <?php echo $CurrentOrder->MyShortCreatedDate?><br>
										<b>Order Status</b>: <?php echo $CurrentOrder->OrderStatus?><br>
										</td>
										<td valign="top" width="47%" align="right"  style="padding:10px; ">
										<b>Payment Method</b>: <?php echo $CurrentOrder->PaymentMethod?><br>
										<b>Payment Status</b>: <?php echo $CurrentOrder->PaymentStatus?><br>
										<b>Transaction ID</b>: <?php echo $CurrentOrder->TransactionID?><br>
										<?php
										if($CurrentOrder->PaymentStatus=="Paid" && $CurrentOrder->DownloadID !="")
											echo '<br><b>Download ID</b>: '.$CurrentOrder->DownloadID;
										?>	
										</td>
									  </tr>
									  <tr ><td colspan="2">&nbsp;</td></tr>
									  <tr style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size:12px; font-weight: normal; line-height:24px;">
									  	<th align="left" width="53%" bgcolor="#CCCCCC" style="color:#000000; padding:8px; text-transform:uppercase;">Billing Address</th>
										<th align="right" width="47%" bgcolor="#CCCCCC" style="color:#000000; padding:8px; text-transform:uppercase; ">Shipping Address</th>
									  </tr>
									  <tr style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size:12px; font-weight: normal; line-height:24px;">
										  <td valign="top" width="53%" style="padding:10px; ">
										    <?=$CurrentOrder->BillingFirstName."&nbsp;&nbsp;".$CurrentOrder->BillingLastName?><br>
											<?=($CurrentOrder->BillingAddress1 !='')?$CurrentOrder->BillingAddress1."<br>":""?>
											<?=($CurrentOrder->BillingAddress2 !='')?$CurrentOrder->BillingAddress2."<br>":""?>
											<?=$CurrentOrder->BillingCity."&nbsp;&nbsp;".$CurrentOrder->BillingState?><br>
											<?=$CurrentOrder->BillingCountry."<br>".$CurrentOrder->BillingZip?><br>
											<?=($CurrentOrder->BillingFax !="")?"<br>Fax:".$CurrentOrder->BillingFax:"";?>		
											</td>
											<td valign="top" width="47%" align="right"  style="padding:10px; ">
											 <?=$CurrentOrder->ShippingFirstName."&nbsp;&nbsp;".$CurrentOrder->ShippingLastName?><br>
											<?=($CurrentOrder->ShippingAddress1 !='')?$CurrentOrder->ShippingAddress1."<br>":""?>
											<?=($CurrentOrder->ShippingAddress2 !='')?$CurrentOrder->ShippingAddress2."<br>":""?>
											<?=$CurrentOrder->ShippingCity."&nbsp;&nbsp;".$CurrentOrder->ShippingState?><br>
											<?=$CurrentOrder->ShippingCountry."<br>".$CurrentOrder->ShippingZip?><br>
											<?=($CurrentOrder->ShippingFax !="")?"<br>Fax:".$CurrentOrder->ShippingFax:"";?>	
											</td>
									  </tr>
									</table>
									</td>
								</tr>    
							  <tr>
								<td><br> <?php 	include(dirname(__FILE__)."/show_cart_inv.php");?></td>
							  </tr>
							  <tr>
								<td><img src="<?php echo DIR_WS_SITE_GRAPHICS?>black.jpg" style="width:100%;height:1px;" width="100%" height="1" /></td>
							  </tr>
							  <tr>
								<td>&nbsp;</td>
							  </tr>
							   
  					  </table>
    <br>
	Thank you for your order placed on <?php echo DEFINE_SITE_NAME?>.<br>
    If you have any query regarding your order, kindly get in touch with us at <a href="mailto:<?php echo DEFINE_CONTACT_EMAIL?>"><?php echo DEFINE_CONTACT_EMAIL?></a>.<br>
	<br>
	 </td>
 </tr>
</table>	
</div>
<div class="InvoiceEnd"></div>
<?php
$MessageBody = ob_get_contents();
if(!isset($NotMailSent))
	{
		SendEmail("Confirmation of your Order Number ".$CurrentOrder->OrderNo." at ".DEFINE_SITE_NAME,$CurrentOrder->Email,DEFINE_SALES_EMAIL,DEFINE_SITE_NAME,$MessageBody,@constant("DEFINE_BCC_ORDER_EMAIL"),DEFINE_EMAIL_FORMAT);
		@ob_clean();
	}

}

?>



						