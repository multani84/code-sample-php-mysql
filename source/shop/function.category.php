<?php
/*category nav start*/
	function GetCategoryHerarcyArray($CategoryID,$PathArr=null) 
	{
		$CategoryObj = new DataTable(TABLE_CATEGORY);
		$CategoryObj->Where ="CategoryID='".$CategoryObj->MysqlEscapeString($CategoryID)."'";
		$CurrentCategory = $CategoryObj->TableSelectOne(array("ParentID"));
		$ParentID = $CurrentCategory->ParentID;
		if($ParentID =="0")
			return $PathArr;
		else 
		{
			$PathArr[] = $ParentID;
			return GetCategoryHerarcyArray($ParentID,$PathArr);
		}
	}
	function GetCategoryTreeArray($CategoryID,$PathArr=null) 
	{
		$PathArr[] = $CategoryID;
		$CategoryObj = new DataTable(TABLE_CATEGORY);
		$CategoryObj->Where ="ParentID='".$CategoryObj->MysqlEscapeString($CategoryID)."'";
		$CategoryObj->TableSelectAll(array("CategoryID,CategoryName"),"Position");
		while ($CurrentCategory = $CategoryObj->GetObjectFromRecord())
			GetCategoryTreeArray($CurrentCategory->CategoryID,$PathArr);
		return $PathArr;		
	}
	function GetCatLeftNavText($CategoryID,$PathArr,$SubParentID,&$LeftNavText,$Level=0)
	{
		if($Level == 0)
			$LeftNavText = "";
		$CategoryObj = new DataTable(TABLE_CATEGORY);
		$CategoryObj->Where ="ParentID ='".$SubParentID."' and Active='1'";
		$CategoryObj->TableSelectAll(array("CategoryID","CategoryName"),"Position ASC");
		if($CategoryObj->GetNumRows() >0)
		{
			if($LeftNavText == "")
				$LeftNavText .= "<ul class='sf-menu sf-vertical flyout$Level'>";
			else 
				$LeftNavText .= "<ul class='flyout$Level'>";
			while ($TempCategory = $CategoryObj->GetObjectFromRecord()) 
			{
				$Space ="";
				//for ($i=1;$i<$Level;$i++)
				//	$Space .= "&nbsp;&nbsp;&nbsp;";
				if($Level == 0)
					$LeftNavText .= "<li><a href='".SKSEOURL($TempCategory->CategoryID,"shop/category")."' class='LfLink'>".MyStripSlashes($TempCategory->CategoryName)."</a>";
				else
					$LeftNavText .= "<li><a href='".SKSEOURL($TempCategory->CategoryID,"shop/category")."' class='LfLink'>".$Space.MyStripSlashes($TempCategory->CategoryName).(($TempCategory->CategoryID == $CategoryID)?'&nbsp;&raquo;':'')."</a>";
				//if(in_array($TempCategory->CategoryID,$PathArr))
					GetCatLeftNavText($CategoryID,$PathArr,$TempCategory->CategoryID,$LeftNavText,$Level+1);	
				$LeftNavText .= "</li>";				
			}
			$LeftNavText .= "</ul>";
		}
	}
	/*category nav end*/
	/*  Categories Caption start */
	function CategoryChain4Caption($CatID,$LinkPath,$Parm="cID",$LinkClass="",$ActiveOnly=true,$ExtraParm="",$Encoded=true,$Title="",$StartCatID ="")
	{
		$CategoryObj = new DataTable(TABLE_CATEGORY);
		if(!empty($CatID))
		{
			if($StartCatID =="")
			$StartCatID = $CatID;
			$CategoryObj->Where = "CategoryID='".$CategoryObj->MysqlEscapeString($CatID)."'";
			$CurrentCategory = $CategoryObj->TableSelectOne(array("CategoryName","ParentID","CategoryID"));
			if(isset($CurrentCategory->ParentID) && $CurrentCategory->ParentID !="")
			{
				if($StartCatID == $CurrentCategory->CategoryID)
				{
						if($Parm =="SEOURL")
							$Title="<a class='$LinkClass' href='".SKSEOURL($CatID,"shop/category").$ExtraParm."'>".ucwords(strtolower($CurrentCategory->CategoryName))."</a>".$Title;
						else 
							$Title="<a class='$LinkClass' href='".$LinkPath."?".$Parm."=".($Encoded ? EncodeString($CatID):$CatID).$ExtraParm."'>".ucwords(strtolower($CurrentCategory->CategoryName))."</a>".$Title;
						//$Title=ucwords($CurrentCategory->CategoryName)."".$Title;
				}
				else 
				{
					if($Parm =="SEOURL")
						$Title="<a class='$LinkClass' href='".SKSEOURL($CatID,"shop/category").$ExtraParm."'>".ucwords(strtolower($CurrentCategory->CategoryName))."</a>&nbsp;&raquo;&nbsp;".$Title;
					else 
						$Title="<a class='$LinkClass' href='".$LinkPath."?".$Parm."=".($Encoded ? EncodeString($CatID):$CatID).$ExtraParm."'>".ucwords(strtolower($CurrentCategory->CategoryName))."</a>&nbsp;&raquo;&nbsp;".$Title;
				}
				CategoryChain4Caption($CurrentCategory->ParentID,$LinkPath,$Parm,$LinkClass,$ActiveOnly,$ExtraParm,$Encoded,$Title,$StartCatID);	
			}
			return;
		}
		else 
		{
			print($Title);
		}
	}
	/*  Categories Caption start */
	/*  Categories Chain start */
	function CategoryChain($ParentID,$CurrentCategoryID=0,$SelectedElementArr=null,$ActiveOnly=false,$Level=0,$hierarchy=0,$DisplayAll=0) 
	{
		$Text = "";
		$LoopIn =0;
		$CategoryObj = new DataTable(TABLE_CATEGORY);
		$CategoryObj->Where = "ParentID='".$CategoryObj->MysqlEscapeString($ParentID)."' ";
		if($ActiveOnly)
			$CategoryObj->Where .=" And Active='1'";
		$CategoryObj->TableSelectAll(array("CategoryID,CategoryName"),"Position ASC");
		while ($CurrentCategory = $CategoryObj->GetObjectFromRecord())  
		{
			if($CurrentCategoryID != $CurrentCategory->CategoryID)
			{
				$Text .=  "<option";
				if($ParentID == 0)
					$Text .= " style='color:#003c6e;background-color:#d5e6f4;'";
				$Text .= " value=".$CurrentCategory->CategoryID.(in_array($CurrentCategory->CategoryID,$SelectedElementArr) ? " selected":"").">";
				for ($i=0;$i<$Level;$i++)
					$Text .= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
				$Text .= $CurrentCategory->CategoryName."</option>";
				if(($LoopIn >$hierarchy && $LoopIn !=0) OR ($DisplayAll=="1"))
				{
					$Text .= CategoryChain($CurrentCategory->CategoryID,$CurrentCategoryID,$SelectedElementArr,$ActiveOnly,$Level+1,$hierarchy+1,$DisplayAll);     
				}
			}
		}
		return $Text;
	}
/*  Categories Chain start */