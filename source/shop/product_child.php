<?php
$AttributeArray = array();
if(isset($CurrentProduct->ProductAttribute) && $CurrentProduct->ProductAttribute != "")
{
	$AttributeObj = new DataTable(TABLE_ATTRIBUTE);
	$AttributeObj->Where ="AttributeID in (".$CurrentProduct->ProductAttribute.")";
	$AttributeObj->TableSelectAll();
	$AttributeArray = array();
	if($AttributeObj->GetNumRows() > 0)
	{
		while($CurrentAttribute = $AttributeObj->GetObjectFromRecord())
		{
			$AttributeArray[$CurrentAttribute->AttributeID]['id'] = $CurrentAttribute->AttributeName; 
			$AttributeArray[$CurrentAttribute->AttributeID]['option'] = array();
		}
	}
	
	
	$ProductObj2 = new DataTable(TABLE_PRODUCT);
	$ProductObj2->Where = "MasterID='".$ProductObj->MysqlEscapeString($CurrentProduct->ProductID)."' AND Active='1'";
	$CurrentChilds = $ProductObj2->TableSelectOne(array("GROUP_CONCAT(ProductID) as ChildIDs"));
	
	if(isset($CurrentChilds->ChildIDs) && $CurrentChilds->ChildIDs != "")
	{
		
		$ProductRelationObj = new DataTable();
		$ProductRelationObj->TableName = TABLE_PRODUCT_RELATION." pr, ".TABLE_ATTRIBUTE_VALUE." av";
		$ProductRelationObj->Where = "pr.ProductID IN (".$CurrentChilds->ChildIDs.") AND pr.RelationID = av.AttributeValueID  AND pr.RelationType = 'attribute_value' GROUP BY av.AttributeValueID";
		//$ProductRelationObj->DisplayQuery = true;
		$ProductRelationObj->TableSelectAll(array("*"),"Position ASC");
		
		while ($CurrentRelation = $ProductRelationObj->GetObjectFromRecord())
		{
			$AttributeArray[$CurrentRelation->AttributeID]['option'][] = $CurrentRelation;
		}
	}
	
}

$ProductObj2 = new DataTable(TABLE_PRODUCT);
$ProductObj2->Where = "MasterID='".$ProductObj->MysqlEscapeString($CurrentProduct->ProductID)."' AND Active='1'";
$ProductObj2->TableSelectAll("","Position ASC");

$childString = "";
$childStringHTML = "";
while ($CurrentChildProduct = $ProductObj2->GetObjectFromRecord())
{
	
	$ProductRelationObj = new DataTable();
	$ProductRelationObj->TableName = TABLE_PRODUCT_RELATION." pr, ".TABLE_ATTRIBUTE_VALUE." av";
	$ProductRelationObj->Where = "pr.ProductID = ".$CurrentChildProduct->ProductID." AND pr.RelationID = av.AttributeValueID  AND pr.RelationType = 'attribute_value'";
	$CurrentChildAtt = $ProductRelationObj->TableSelectOne(array("GROUP_CONCAT(DISTINCT AttributeValueID ORDER BY AttributeValueID ASC SEPARATOR '_') as AttributeValues"));

	
	$Image = "";
	$AttValue = "";
	$StChildArr = GetProductPriceNStock($CurrentChildProduct->ProductID,0,true);
	
	$childString .= '{id: "'.$CurrentChildProduct->ProductID.'", stock: "'.(isset($StChildArr['Stock'])?$StChildArr['Stock']:"").'", model: "'.$CurrentChildProduct->ModelNo.'", price: "'.(isset($StChildArr['DisplayPrice'])?$StChildArr['DisplayPrice']:"").'", image: "'.$Image.'", attribute: "'.(isset($CurrentChildAtt->AttributeValues)?$CurrentChildAtt->AttributeValues:"").'"},';
	$childStringHTML .= '<div id="child_product_'.$CurrentChildProduct->ProductID.'">
							<div class="id">'.$CurrentChildProduct->ProductID.'</div>
							<div class="stock">'.(isset($StChildArr['Stock'])?$StChildArr['Stock']:"").'</div>
							<div class="price">'.(isset($StChildArr['DisplayPrice'])?$StChildArr['DisplayPrice']:"").'</div>
							<div class="price_details">'.(isset($StChildArr['DisplayPriceDetails'])?$StChildArr['DisplayPriceDetails']:"").'</div>
							<div class="image">'.$Image.'</div>
							<div class="attribute">'.(isset($CurrentChildAtt->AttributeValues)?$CurrentChildAtt->AttributeValues:"").'</div>
							<div class="id">'.$CurrentChildProduct->ProductID.'</div>
							<div class="id">'.$CurrentChildProduct->ProductID.'</div>
						</div>';
	
}

?>

<?php 	$AttributeIndex = 0;
 	$AttributeExist=0;
    $Count =count($AttributeArray);

	if($Count >'0')
	 {
		foreach ($AttributeArray as $k=>$v)
		{$AttributeExist=1;
			/*
			SELECT GROUP_CONCAT(DISTINCT r2.RelationID ORDER BY r2.RelationID ASC SEPARATOR ',') as AttributeDepends FROM `zsk_product_relation` r1,`zsk_product_relation` r2
				WHERE r1.ProductID = r2.ProductID AND
				r1.ProductID IN(47,48,49,50,51,52) AND r1.RelationID = 151 AND r2.RelationID != 151
			*/
			
			?>
				<div class="rowItem ProMgn">
				<label style="width:80px;"><?php echo isset($v['id'])?$v['id']:"&nbsp;"?></label>
					
						<select  rel="<?php echo $AttributeIndex?>" class="AttributeValueCl" name="AttributeValue[]" style="width:100px; ">
							<?php
							foreach ($v['option'] as $kk=>$obj)
							{
								$ProductRelationObj = new DataTable();
								$ProductRelationObj->TableName = TABLE_PRODUCT_RELATION." r1, ".TABLE_PRODUCT_RELATION." r2";
								$ProductRelationObj->Where = "r1.ProductID = r2.ProductID AND 
															  r1.ProductID IN(".$CurrentChilds->ChildIDs.") AND 
															  r1.RelationID = '".$obj->AttributeValueID."' AND r2.RelationID != '".$obj->AttributeValueID."'";
								$AttributeDependsObj = $ProductRelationObj->TableSelectOne(array("GROUP_CONCAT(DISTINCT r2.RelationID ORDER BY r2.RelationID ASC SEPARATOR ',') as AttributeDepends"));
	
								?>
								<option rel="<?php echo isset($AttributeDependsObj->AttributeDepends)?$AttributeDependsObj->AttributeDepends:""?>" value="<?php echo $obj->AttributeValueID?>" <?php echo (isset($_GET['AttributeValue']) && in_array($obj->AttributeValueID,explode(",",$_GET['AttributeValue'])))?"selected":""?>><?php echo $obj->AttributeValue?></option>
								<?php
							}
							$AttributeIndex++;
							?>
						</select>
					</div>
					
			<?php
		}
   }			
?>

<?php if($AttributeExist>0) 
  {?>
<input type="hidden" id="FixedAttributePrice" value="">
<div style="display:none;"><?php echo $childStringHTML?></div>
<script type="text/javascript">
var childs = [<?php echo substr($childString,0,-1)?>];

jQuery(document).ready(function($){

 			AttLen = jQuery(".AttributeValueCl").length;
 			jQuery(".AttributeValueCl").change(function(){	
			 
				OptionList = jQuery(".AttributeValueCl").eq(0).find("option:selected").attr("rel");	
				var OptionListArray = OptionList.split(",");	
				
				for (i = 0; i <= parseInt($(this).attr("rel")); i++) 
				 {
				 	
				 	jQuery(".AttributeValueCl").eq(i+1).attr("disabled",false);
				 	var OptionListArray = jQuery.grep(OptionListArray, function(element) {
					    return jQuery.inArray(element, jQuery(".AttributeValueCl").eq(i).find("option:selected").attr("rel").split(",")) !== -1;
					});
					
					
					
				 }
				
				  
				 for (i = parseInt($(this).attr("rel"))+1; i < AttLen; i++) 
				 {
				 	
				 	jQuery(".AttributeValueCl").eq(i+1).attr("disabled",true);
				 	jQuery(".AttributeValueCl").eq(i).find("option").hide();
				 	var ct = 0;
				 	for (var k = 0; k < OptionListArray.length; k++)
				 	{
						if(jQuery(".AttributeValueCl").eq(i).find("option[value='"+OptionListArray[k]+"']").length==1)
				 		{
				 			jQuery(".AttributeValueCl").eq(i).find("option[value='"+OptionListArray[k]+"']").show().attr('selected', 'selected');
							jQuery(".AttributeValueCl").eq(i).val(OptionListArray[k]);
				 			ct++;
				 		}
				 	}				 	
				 	if(ct==1)
				 	 jQuery(".AttributeValueCl").eq(i).attr("disabled",false);
					
				 }			 
			 
				SwitchAttribute();
		});
		
		jQuery(".AttributeValueCl").eq(0).attr("disabled",false).change(); 			
		
 	});

function SwitchAttribute(ms)
{
	

	var myatt = new Array();
	var found = false;
	
	jQuery(".AttributeValueCl").each(function(i,val){
		myatt.push(jQuery(this).val());
		
	 });
	 
	 myatt.sort(function(a,b){return a - b});
	 myatt = myatt.toString().replace(/,/g,'_');
	 
	 for (i = 0; i < childs.length; i++) 
	 {
		   if(childs[i].attribute==myatt)
		   {
		   		
		   		found=true;
		   		jQuery(".ChildImage_"+childs[i].id).find("a").click();
		   		jQuery("#DisplayPriceDetails").html(jQuery("#child_product_"+childs[i].id).find('.price_details').html());
				//alert(childs[i].attribute);
				var AttributePrice = childs[i].price;
		   		CalPriceNew(AttributePrice);
				
				
				
				jQuery("#ChildInStock,#ChildOutStock").hide();
		   		if(parseFloat(childs[i].stock) > 0)
					jQuery("#ChildInStock").show();
		   		else
					jQuery("#ChildOutStock").show();
		   }
		   
	}
	if(ms==1 && found==false)
	{
		alert("Combination of the attributes does not exist.");
		return false;
	}
	if(ms==1 && found==true)
	{
		jQuery("#FrmMainProduct").submit();
		return false;
	}
	
	return true;
	 
}
function CalPriceNew(AttPrice)
 {
 	
 	var AName=$(".AttributeValueCl option:selected").attr("rel");
 	
    var BasePrice= parseFloat("<?php echo isset($StArr['Price'])?$StArr['Price']:"";?>");	
    var FinalPrice=0;
    var Qty=$("#Qty").val();
    
	var AttributePrice=parseFloat(AttPrice).toFixed(2).toString();
	$("#FixedAttributePrice").val(AttPrice);
	var QtyBasePrice= parseFloat(BasePrice).toFixed(2).toString();	
	BasePrice= parseFloat(QtyBasePrice*Qty)+parseFloat(AttributePrice*Qty); 
	
   $("#GetPrice").html("Total Price :&nbsp;&pound;"+(parseFloat(BasePrice)).toFixed(2).toString())
 }

 function FinalPrice()
 {
 	
    var BasePrice= parseFloat("<?php echo isset($StArr['Price'])?$StArr['Price']:"";?>");	
    var FinalPrice=0;
    var Qty=$("#Qty").val();
    
    /* Attribute Price   */
	var AttributePrice=$("#FixedAttributePrice").val();
	AttributePrice=parseFloat(AttributePrice*Qty).toFixed(2).toString();	
	/* Attribute Price END  */
   
	var QtyBasePrice= parseFloat(BasePrice*Qty).toFixed(2).toString();	
    BasePrice= parseFloat(QtyBasePrice)+parseFloat(AttributePrice);  
   $("#GetPrice").html("Total Price :&nbsp;&pound;"+(parseFloat(BasePrice)).toFixed(2).toString())
 }

<?php
if(isset($AttJavascipt) && $AttJavascipt !="")
			$AttJavascipt .='return SwitchAttribute(1);';
		else 
			$AttJavascipt ='return SwitchAttribute(1);';

?>
</script>
<?php }?>