<div class="well">
	<fieldset>
            <div class="row">
                <div class="col-sm-5"><strong>Order #</strong></div>
				<div class="col-sm-7 text-left"><?php echo $CurrentOrder->OrderNo?></div>
		        <div class="col-sm-5"><strong>Order Date</strong></div>
				<div class="col-sm-7 text-left"><?php echo $CurrentOrder->MyCreatedDate?></div>
		    </div>
            <div class="row">
                <div class="col-sm-5"><strong>Order Status</strong></div>
				<div class="col-sm-7 text-left"><?php echo $CurrentOrder->OrderStatus?></div>
		        <div class="col-sm-5"><strong>Email</strong></div>
				<div class="col-sm-7 text-left"><?php echo $CurrentOrder->Email?></div>
		    </div>
			<div class="row">
                <div class="col-sm-5"><strong>Payment Status</strong></div>
				<div class="col-sm-7 text-left"><?php echo $CurrentOrder->PaymentStatus?></div>
		        <div class="col-sm-5"><strong>Payment Method</strong></div>
				<div class="col-sm-7 text-left"><?php echo $CurrentOrder->PaymentMethod?></div>
		    </div>
        </fieldset>
</div>
<div class="well">
	<fieldset>
	<?php include(dirname(__FILE__)."/show_cart.php");?>
	</fieldset>
</div>
<div class="well">
	<fieldset>
	<?php include(dirname(__FILE__)."/shipping_detail_admin.php");?>
	</fieldset>
</div>