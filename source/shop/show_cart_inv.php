
        <table border='0' cellpadding='5' cellspacing='1' width='100%'>
		<tr style="color:#000000; font-family: Verdana, Arial, Helvetica, sans-serif; font-size:14px; font-weight: normal; line-height:24px;">
			<th align="center" bgcolor="#CCCCCC">S.No</th>
			<th align="left" bgcolor="#CCCCCC">Item</th>
			<?php if(@constant("DEFINE_PRODUCTS_ATTRIBUTE")=="1") {
			?>
			<th align="left" bgcolor="#CCCCCC"><?=@constant("DEFINE_PRODUCTS_ATTRIBUTE_OPTION1");?></th>
			<th align="left" bgcolor="#CCCCCC"><?=@constant("DEFINE_PRODUCTS_ATTRIBUTE_OPTION2");?></th>
			<?php
			}?>
			<th align="center" bgcolor="#CCCCCC">Qty</th>
			<th align="right" bgcolor="#CCCCCC">Price</th>
			<th align="right" bgcolor="#CCCCCC">Total</th>
			
		</tr>

<?php		$SNo = 1;

		while ($CurrentCartItem = $CartObj->GetObjectFromRecord()) 
		{
					?>
					<tr style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size:12px; font-weight: normal; line-height:24px;">
					<td align="center" bgcolor="#f2f2f2" width="5%" valign="top"><input type="hidden" name="ItemID<?php echo $SNo?>" value="<?php echo $CurrentCartItem->ItemID?>"><?php echo $SNo?>.</td>
					<td align="left" bgcolor="#f2f2f2" valign="top"><?php echo $CurrentCartItem->ItemName?>
						<?php echo $CurrentCartItem->Comments !=""?"<br>".nl2br($CurrentCartItem->Comments):""?>
					</td>
					<?php
					if(@constant("DEFINE_PRODUCTS_ATTRIBUTE")=="1")
					{
					?>
					<td align="left" bgcolor="#f2f2f2"><?php echo $CurrentCartItem->Attribute1?></td>
					<td align="left" bgcolor="#f2f2f2"><?php echo $CurrentCartItem->Attribute2?></td>
					<?php
					}?>
					<td align="center" bgcolor="#f2f2f2" width="8%" valign="top">
					<?php
					if($CurrentCartItem->ItemStatus =="Normal")
					{
						echo ((isset($Editable) and $Editable == true)) ? "<input style='text-align:center' type='text' size='2' name='Qty$SNo' value='$CurrentCartItem->Qty' onblur='return CheckInvalidQuantity(this,\"$CurrentCartItem->Qty\",\"".@constant('INVALID_QUANTITY')."\");'>":$CurrentCartItem->Qty;
					}
					else 
					{
						echo ((isset($Editable) and $Editable == true)) ? "<input style='text-align:center' type='hidden' size='2' name='Qty$SNo' value='$CurrentCartItem->Qty' onblur='return CheckInvalidQuantity(this,\"$CurrentCartItem->Qty\",\"".@constant('INVALID_QUANTITY')."\");'>":$CurrentCartItem->Qty;
					}
					?>

					</td>

					<td align="right" bgcolor="#f2f2f2" width="12%" valign="top" style="text-align:right;<?php echo ($CurrentCartItem->ItemType=="Offer")?"color:#ff0000;":"";?>">
					<?php
						if($CurrentCartItem->Price > $CurrentCartItem->UnitPrice)
						{
							echo Change2CurrentCurrency($CurrentCartItem->Price);
							echo "<br><small>(".Change2CurrentCurrency($CurrentCartItem->UnitPrice). " + " . Change2CurrentCurrency($CurrentCartItem->TaxPrice)."-GST)</small>" ;
						}
						else
						{
							echo Change2CurrentCurrency($CurrentCartItem->UnitPrice);
						}
						?></td>

					<td align="right" bgcolor="#f2f2f2" width="15%" valign="top" style="text-align:right;<?php echo ($CurrentCartItem->ItemType=="Offer")?"color:#ff0000;":"";?>"><?php echo $CurrentCartItem->Total != 0?Change2CurrentCurrency($CurrentCartItem->Total):""?></td>
				</tr>

				<?php
				$CartObj2->Where =" ParentItemID='".$CartObj2->MysqlEscapeString($CurrentCartItem->ItemID)."'";
				$CartObj2->TableSelectAll(array("*"));
				while($CurrentCartItem2 = $CartObj2->GetObjectFromRecord())
				{
					?>
					<tr>
					<td align="center" width="5%"></td>
					<td class="title">--<i><?php echo $CurrentCartItem2->ItemName?></i>
						<?php echo $CurrentCartItem2->Comments !=""?"<br>".nl2br($CurrentCartItem2->Comments):""?>
					</td>
					<?php
					if(@constant("DEFINE_PRODUCTS_ATTRIBUTE")=="1")
					{
					?>
					<td align="left"><?php echo $CurrentCartItem2->Attribute1?></td>
					<td align="left"><?php echo $CurrentCartItem2->Attribute2?></td>
					<?php
					}?>
					<td class="qty">
					<?php
					if($CurrentCartItem2->ItemStatus =="Normal")
					{
						echo ((isset($Editable) and $Editable == true)) ? "<input style='text-align:center' type='text' size='2' name='Qty$SNo' value='$CurrentCartItem2->Qty' onblur='return CheckInvalidQuantity(this,\"$CurrentCartItem2->Qty\",\"".@constant('INVALID_QUANTITY')."\");'>":$CurrentCartItem2->Qty;
					}
					else 
					{
						echo ((isset($Editable) and $Editable == true)) ? "<input style='text-align:center' type='hidden' size='2' name='Qty$SNo' value='$CurrentCartItem2->Qty' onblur='return CheckInvalidQuantity(this,\"$CurrentCartItem2->Qty\",\"".@constant('INVALID_QUANTITY')."\");'>":$CurrentCartItem2->Qty;
					}
					?>
					</td>
					<td class="price" style="text-align:right;<?php echo ($CurrentCartItem2->ItemType=="Offer")?"color:#ff0000;":"";?>">
						<?php
						if($CurrentCartItem2->Price > $CurrentCartItem2->UnitPrice)
						{
							echo Change2CurrentCurrency($CurrentCartItem2->Price);
							echo "<br><small>(".Change2CurrentCurrency($CurrentCartItem2->UnitPrice). " + " . Change2CurrentCurrency($CurrentCartItem2->TaxPrice)."-GST)</small>" ;
						}
						else
						{
							echo Change2CurrentCurrency($CurrentCartItem2->UnitPrice);
						}
						?>
					</td>
					<td class="price" style="text-align:right;<?php echo ($CurrentCartItem2->ItemType=="Offer")?"color:#ff0000;":"";?>"><?php echo $CurrentCartItem2->Total != 0?Change2CurrentCurrency($CurrentCartItem2->Total):""?></td>
					
				</tr>
					<?php
				}
				$SNo =$SNo+1;
		}
		?>
		<input type="hidden" name="Count" value="<?=$SNo?>">
	    <tr style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size:12px; font-weight: normal; line-height:24px;">
			<td bgcolor="#e2e2e2" colspan="<?php echo @constant("DEFINE_PRODUCTS_ATTRIBUTE")=="1"?"6":"4"?>" align="right">Sub Total</td>
			<td bgcolor="#e2e2e2" align="right"><?php echo Change2CurrentCurrency($ShoppingCalculationObj->SubTotal);?></td>
		</tr>

		

		<?php
		if($ShoppingCalculationObj->ShippingPrice >0 OR (isset($ShippingOptionApply) && $ShippingOptionApply=="1"))
		{?>
		<tr style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size:12px; font-weight: normal; line-height:24px;">
			<td bgcolor="#e9e9e9" colspan="<?php echo @constant("DEFINE_PRODUCTS_ATTRIBUTE")=="1"?"6":"4"?>" align="right">Shipping 
			<?php echo isset($ShippingOptionTEXT)?$ShippingOptionTEXT:$ShoppingCalculationObj->ShippingText?>
			</td>
			<td bgcolor="#e9e9e9" align="right"><?php echo Change2CurrentCurrency($ShoppingCalculationObj->ShippingPrice);?></td>
		</tr>
		<?php
		}
		?>
		<?php
		if($ShoppingCalculationObj->VATCharges >0)
		{?>
		<tr style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size:12px; font-weight: normal; line-height:24px;">
			<td bgcolor="#f2f2f2" colspan="<?php echo @constant("DEFINE_PRODUCTS_ATTRIBUTE")=="1"?"6":"4"?>" align="right">GST</td>
			<td bgcolor="#f2f2f2" align="right"><?php echo Change2CurrentCurrency($ShoppingCalculationObj->VATCharges);?></td>
		</tr>
		<?php
		}
		?>
		<tr style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size:12px; font-weight: normal; line-height:24px;">
			<td bgcolor="#e2e2e2" colspan="<?php echo @constant("DEFINE_PRODUCTS_ATTRIBUTE")=="1"?"6":"4"?>" align="right"><strong>TOTAL</strong></td>
			<td bgcolor="#e2e2e2" align="right"><strong><?php echo Change2CurrentCurrency($ShoppingCalculationObj->Total);?></strong></td>
		</tr>
		<?php
		if($ShoppingCalculationObj->VoucherAmount >0)
		{?>
		<tr style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size:12px; font-weight: normal; line-height:24px;">
			<td bgcolor="#f2f2f2" colspan="<?php echo @constant("DEFINE_PRODUCTS_ATTRIBUTE")=="1"?"6":"4"?>" align="right">Discount <?php echo $ShoppingCalculationObj->VoucherText !=""?'<br>('.$ShoppingCalculationObj->VoucherText.')':'';?></td>
			<td bgcolor="#f2f2f2" align="right"><?php echo Change2CurrentCurrency($ShoppingCalculationObj->VoucherAmount);?></td>
		</tr>
		<?php
		}
		if($ShoppingCalculationObj->PointAmount >0)
		{?>
		<tr style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size:12px; font-weight: normal; line-height:24px;">
			<td bgcolor="#e2e2e2" colspan="<?php echo @constant("DEFINE_PRODUCTS_ATTRIBUTE")=="1"?"6":"4"?>"  align="right">Discount on points -<?php echo $ShoppingCalculationObj->PointText?></td>
			<td bgcolor="#e2e2e2" align="right"><?php echo Change2CurrentCurrency($ShoppingCalculationObj->PointAmount);?></td>

		</tr>
	<?php
		}
		if($ShoppingCalculationObj->GrandTotal !=$ShoppingCalculationObj->Total)
		{?>
		<tr style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size:12px; font-weight: normal; line-height:24px;">
			<td align="right" bgcolor="#f2f2f2" colspan="<?php echo @constant("DEFINE_PRODUCTS_ATTRIBUTE")=="1"?"6":"4"?>" class="text-right">Grand Total </td>
			<td align="right" bgcolor="#f2f2f2"><?php echo Change2CurrentCurrency($ShoppingCalculationObj->GrandTotal);?></td>
		</tr>
		<?php
		}?>
</table>