  <div class="row">
        <div class="col-md-6">
				    <h2>Billing Address</h2>
                       <table class="table table-striped table-bordered">
						<tr>
							<td class="right"><b>Email</td>
							<td><?=$CurrentOrder->Email?></td>
						</tr>
						<tr>
							<td class="right"><b>Name</td>
							<td><?=$CurrentOrder->BillingFirstName?>&nbsp;&nbsp;<?=$CurrentOrder->BillingLastName?></td>
						</tr>
						<tr>
							<td class="right"><b>Address</td>
							<td><?=$CurrentOrder->BillingAddress1?></td>
						</tr>
						<tr>
							<td class="right"><b>Address</td>
							<td><?=$CurrentOrder->BillingAddress2?></td>
						</tr>
						<tr>
							<td class="right"><b>Town/City</td>
							<td><?=$CurrentOrder->BillingCity?></td>
						</tr>
						<tr>
							<td class="right"><b>County/State</td>
							<td><?=$CurrentOrder->BillingState?></td>
						</tr>
						<tr>
							<td class="right"><b>Country</b></td>
							<td><?=$CurrentOrder->BillingCountry?></td>
						</tr>
						<tr>
							<td class="right"><b>Postal/Zip Code</td>
							<td><?=$CurrentOrder->BillingZip?></td>
						</tr>
						<tr>
							<td class="right"><b>Phone</b></td>
							<td><?=$CurrentOrder->BillingPhone?></td>
						</tr>
						
					
					</table>
				 </div>
        <div class="col-md-6">
				<?php
				if(@constant("SHIPPING_INFO_DISPLAY")!="0")
				{?>
				 <div class="RightPanel floatRight">
                    <h2>Shipping Address</h2>
					    <table class="table table-striped table-bordered">
					
						<tr>
							<td class="right"><b>Name</td>
							<td><?=$CurrentOrder->ShippingFirstName?>&nbsp;&nbsp;<?=$CurrentOrder->ShippingLastName?></td>
						</tr>
						<tr>
							<td class="right"><b>Address1</td>
							<td><?=$CurrentOrder->ShippingAddress1?></td>
						</tr>
						<tr>
							<td class="right"><b>Address2</b></td>
							<td><?=$CurrentOrder->ShippingAddress2?></td>
						</tr>
						<tr>
							<td class="right"><b>Town/City</td>
							<td><?=$CurrentOrder->ShippingCity?></td>
						</tr>
						<tr>
							<td class="right"><b>County/State</td>
							<td><?=$CurrentOrder->ShippingState?></td>
						</tr>
						<tr>
							<td class="right"><b>Country</b></td>
							<td><?=$CurrentOrder->ShippingCountry?></td>
						</tr>
						<tr>
							<td class="right"><b>Postal/Zip Code</td>
							<td><?=$CurrentOrder->ShippingZip?></td>
						</tr>
						<tr>
							<td class="right"><b>Phone</b></td>
							<td><?=$CurrentOrder->ShippingPhone?></td>
						</tr>
						
					
						
					</table>
				</div>
				<?php
				}?>
		</div>	
		<?php
			if(!empty($CurrentOrder->Comments))
			{
		?>
				<div style="clear:both"></div>
				<div class="col-sm-12">
					<table class="table table-striped table-bordered">
					<tr>
						<td><b>COMMENTS / INSTRUCTIONS</b></font></td>
					</tr>
					<tr>
						<td><?=nl2br($CurrentOrder->Comments)?></td>
					</tr>
					</table>
				</div>					
				<?php
				}
				?>	
				</div>

