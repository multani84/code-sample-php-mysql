
<!-- Heading  -->
<div class="row-fluid">
	<div class="span12">
		<h1>Products</h1>
		<hr />
	</div>
	<div class="pull-left">
		<a href="<?php  echo DIR_WS_SITE_CONTROL?>index.php?Page=shop/category" class="adm-link">Categories</a>
		&nbsp;&raquo;&nbsp;
		<?php  echo CategoryChain4Caption($ParentID,"index.php","Page=$Page&ParentID","adm-link",false,"",false,"");?>	
	</div>
	<div class="pull-right">
		<?php  
		if(isset($CurrentCategory->CategoryID) && $CurrentCategory->CategoryID !="")
		{
			?>
			<a href="<?php  echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php  echo $Page?>&ParentID=<?php  echo $ParentID?>" class="adm-link"><?php  echo MyStripSlashes($CurrentCategory->CategoryName)?></a>&nbsp;&nbsp;||&nbsp;&nbsp;
			<?php  
		}
		?>	
		<?php  
		if(isset($CurrentProduct->ProductID) && $CurrentProduct->ProductID !="")
		{
			?>
			<a href="<?php  echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php  echo $Page?>&Section=EditProduct&Pg=<?php  echo $Pg?>&ParentID=<?php  echo $ParentID?>&ProductID=<?php  echo $ProductID?>" class="adm-link"><?php  echo MyStripSlashes($CurrentProduct->ProductName)." (".MyStripSlashes($CurrentProduct->ModelNo).")"?></a>
			<?php  
		}
		?>	
		<a href="<?php  echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php  echo $Page?>&Section=AddProduct&Pg=<?php  echo $Pg?>&ParentID=<?php  echo $ParentID?>" class="btn btn-primary">Add Product <icon class="icon-plus-sign icon-white-t"></icon></a>
		
	</div>
<div class="clearfix separator bottom"></div>
<!--  End Heading-->	
<?php  
if(isset($CurrentProduct->ProductID) && $CurrentProduct->ProductID !="")
{
?>
<div class="well">
<table border="0" cellpadding="3" cellspacing="1" width="100%" class="InsideTable">
	<tr>
		<td align="center">
		<div class="DivTab">
		  	<ul>
		  			<li><a href="<?php  echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php  echo $Page?>&Section=Images&Pg=<?php  echo $Pg?>&ParentID=<?php  echo $ParentID?>&ProductID=<?php  echo $CurrentProduct->ProductID?>"><?php  echo substr(MyStripSlashes($CurrentProduct->ProductName),0,6)?>'s Images/Pdfs</a></li>
		  			<li><a href="<?php  echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php  echo $Page?>&Section=Attributes&Pg=<?php  echo $Pg?>&ParentID=<?php  echo $ParentID?>&ProductID=<?php  echo $CurrentProduct->ProductID?>"><?php  echo substr(MyStripSlashes($CurrentProduct->ProductName),0,6)?>'s Filter(s)</a></li>
		  			<?php  
					if(isset($CurrentProduct->ProductType) && $CurrentProduct->ProductType=="Bundle")
					{
					?><li><a href="<?php  echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php  echo $Page?>&Section=ChildProduct&Pg=<?php  echo $Pg?>&ParentID=<?php  echo $ParentID?>&ProductID=<?php  echo $CurrentProduct->ProductID?>">Child Products/Attributes</a></li><?php  
					}?>
					<?php  
					if(@constant("DEFINE_PRODUCT_REVIEWS") =="1")
					{
					?><li><a href="<?php  echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php  echo $Page?>&Section=Reviews&Pg=<?php  echo $Pg?>&ParentID=<?php  echo $ParentID?>&ProductID=<?php  echo $CurrentProduct->ProductID?>"><?php  echo substr(MyStripSlashes($CurrentProduct->ProductName),0,6)?>'s Reviews</a></li><?php  
					}?>
					<?php  
					if(@constant("DEFINE_PRODUCT_ADDITION") =="1")
					{
					?><li><a href="<?php  echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php  echo $Page?>&Section=Addition&Pg=<?php  echo $Pg?>&ParentID=<?php  echo $ParentID?>&ProductID=<?php  echo $CurrentProduct->ProductID?>"><?php  echo substr(MyStripSlashes($CurrentProduct->ProductName),0,6)?>'s Option(s)</a></li><?php  
					}?>
					<?php  
					if(@constant("DEFINE_PRODUCT_QUESTION") =="1")
					{
					?><li><a href="<?php  echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php  echo $Page?>&Section=Questions&Pg=<?php  echo $Pg?>&ParentID=<?php  echo $ParentID?>&ProductID=<?php  echo $CurrentProduct->ProductID?>"><?php  echo substr(MyStripSlashes($CurrentProduct->ProductName),0,6)?>'s Questions</a></li><?php  
					}?>
					<?php  
					if(@constant("DEFINE_PRODUCT_TAB") =="1")
					{
					?>
					<li><a href="<?php  echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php  echo $Page?>&Section=Infos&Pg=<?php  echo $Pg?>&ParentID=<?php  echo $ParentID?>&ProductID=<?php  echo $CurrentProduct->ProductID?>"><?php  echo substr(MyStripSlashes($CurrentProduct->ProductName),0,6)?>'s Tab Info.</a></li><?php  
					}?>
					<?php  
					if(@constant("DEFINE_PRODUCT_RELATED") =="1")
					{
					?><li><a href="<?php  echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php  echo $Page?>&Section=Related&Pg=<?php  echo $Pg?>&ParentID=<?php  echo $ParentID?>&ProductID=<?php  echo $CurrentProduct->ProductID?>">Related</a></li><?php  
					}?>
					<?php  
					if(@constant("DEFINE_PRODUCT_SIMILAR") =="1" && false)
					{
					?><li><a href="<?php  echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php  echo $Page?>&Section=Similar&Pg=<?php  echo $Pg?>&ParentID=<?php  echo $ParentID?>&ProductID=<?php  echo $CurrentProduct->ProductID?>">Similar</a></li><?php  
					}?>
		  	</ul>
		</div>
		</td>
	</tr>
</table>
</div>
<?php  
}?>
<?php  

//// Section start
switch($Section)
{
	case "AddProduct" :
	case "EditProduct" :
	/////// Add Product  Start
	?>
	<form class="form-horizontal" method="POST" action="<?php  echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php  echo $Page?>&Section=<?php  echo $Section?>&Pg=<?php  echo $Pg?>&Target=AddProduct&ParentID=<?php  echo $ParentID?>&ProductID=<?php  echo $ProductID?>" enctype="multipart/form-data">
		
		<div class="well">
			
			<div class="form-group">
				<label for="email" class="col-sm-4 col-md-5 col-lg-4 control-label">Product Name</label>
				<div class="col-sm-20 col-md-19 col-lg-20 text-left">
					<input type="text" name="ProductName" value="<?php  echo isset($_POST['ProductName'])?$_POST['ProductName']:(isset($CurrentProduct->ProductName)?MyStripSlashes($CurrentProduct->ProductName):"")?>" id="ProductName" size="70">
				</div>
			</div>
			
			<div class="form-group">
				<label for="email" class="col-sm-4 col-md-5 col-lg-4 control-label">SKU/Model No</label>
				<div class="col-sm-20 col-md-19 col-lg-20 text-left">
					<input type="text" name="ModelNo" value="<?php  echo isset($_POST['ModelNo'])?$_POST['ModelNo']:(isset($CurrentProduct->ModelNo)?MyStripSlashes($CurrentProduct->ModelNo):"")?>" size="70">
				</div>
			</div>
			<?php  
			if(@constant("DEFINE_SEO_URL_ACTIVE") =="1")
			{
			?>
			<div class="form-group">
				<label for="email" class="col-sm-4 col-md-5 col-lg-4 control-label">Product URL</label>
				<div class="col-sm-20 col-md-19 col-lg-20 text-left">
				<input type="text" name="URLName" value="<?php  echo isset($_POST['URLName'])?MyStripSlashes($_POST['URLName']):(isset($CurrentProduct->URLName)?MyStripSlashes($CurrentProduct->URLName):"")?>" onblur="return CheckSpecialSymbol---(this);" title="Please do not use special symbol. Only use alfa numeric characters,(_) underscores or (-) dashes." size="70">
				<br><small>Please do not use special symbol. 
				<br>Only use alfa numeric characters,(_) underscores or (-) dashes.</small>
				</div>
			</div>
			<?php  
			}?>
			<?php  
			if(@constant("DEFINE_SEO_META_ACTIVE") =="1")
			{
			?>
			
			<div class="form-group">
				<hr>
			</div>
			
			<div class="form-group">
				<label for="email" class="col-sm-4 col-md-5 col-lg-4 control-label">Meta Title</label>
				<div class="col-sm-20 col-md-19 col-lg-20 text-left"><input type="text" name="MetaTitle" value="<?php  echo isset($_POST['MetaTitle'])?MyStripSlashes($_POST['MetaTitle']):(isset($CurrentProduct->MetaTitle)?MyStripSlashes($CurrentProduct->MetaTitle):"")?>" size="100"></div>
			</div>
			<div class="form-group">
				<label for="email" class="col-sm-4 col-md-5 col-lg-4 control-label">Meta Keywords</label>
				<div class="col-sm-20 col-md-19 col-lg-20 text-left">
				 	<table border="0" width="100%" cellpadding="0" cellspacing="0">
				 	<tr>
					 	<td align="left" valign="top"><textarea name="MetaKeyword" id="MetaKeyword" cols="60" rows="6"><?php  echo isset($_POST['MetaKeyword'])?MyStripSlashes($_POST['MetaKeyword']):(isset($CurrentProduct->MetaKeyword)?MyStripSlashes($CurrentProduct->MetaKeyword):"")?></textarea></td>
					 	<td align="left">
					 		<div style="height:200px;overflow: auto;padding:3px;display:block">
					 		<script type="text/javascript">
					 			function SKKeywordSelection(ckPt,val,ObjID)
								{
									EleObj = document.getElementById(ObjID)
									Str = EleObj.value;
									LStr = EleObj.value;
									if(ckPt)
									{
										LastIndex = LStr.substring(LStr.length-1,LStr.length)
										
										if(Str.indexOf(val) > 0)
											Str = Str
										else
										{
											Str = Str + val + ",";
										}
									}
									else
									{
										Str = Str.replace(val+",",'');
										Str = Str.replace(val,'');
									}
										 
									EleObj.value = Str;
									
								}
					 		</script>
					 			<table width="100%" cellpadding="0" cellspacing="0" class="InsideTable">
					 			<?php  
					 			$KeywordObj = new DataTable(TABLE_KEYWORDS);
								$KeywordObj->TableSelectAll("","Keyword ASC");
								$SNo =1;
								while ($CurrentKeyword = $KeywordObj->GetObjectFromRecord())
								{
					 			?>
					 				<tr>
					 					<td><input type="checkbox" name="ChkAdd[]" id="ChkAdd_<?php  echo $SNo?>" class="chk" value="<?php  echo trim($CurrentKeyword->Keyword)?>" onclick="return SKKeywordSelection(this.checked,this.value,'MetaKeyword');" ></td>
					 					<td><?php  echo $CurrentKeyword->Keyword?></td>
					 				</tr>
					 			<?php  
					 			$SNo++;
								}?>	
					 			</table>
					 		</div>
					 	</td>
					 	</tr>
				 	</table>
				 </div>
			</div>
			<div class="form-group">
				<label for="email" class="col-sm-4 col-md-5 col-lg-4 control-label">Meta Description</label>
				<div class="col-sm-20 col-md-19 col-lg-20 text-left"><textarea name="MetaDescription" cols="60" rows="3"><?php  echo isset($_POST['MetaDescription'])?MyStripSlashes($_POST['MetaDescription']):(isset($CurrentProduct->MetaDescription)?MyStripSlashes($CurrentProduct->MetaDescription):"")?></textarea>
				</div>
			</div>
			<?php  
			}?>
			<div class="form-group">
				<div><hr></div>
			</div>									
			<div class="form-group">
				<label for="email" class="col-sm-4 col-md-5 col-lg-4 control-label">Manufacturer</label>
				<div class="col-sm-20 col-md-19 col-lg-20 text-left">
					<select name="MNID">
					<option value="0">----Select----</option>
					<?php  
					$ManufacturerObj->Where = "1";
					$ManufacturerObj->TableSelectAll("","MNName ASC");
					while ($CurrentManufacturer=$ManufacturerObj->GetObjectFromRecord()) 
					{
						?>
						<option value="<?php  echo $CurrentManufacturer->MNID?>" <?php  echo ((isset($_POST['MNID']) && $_POST['MNID']==$CurrentManufacturer->MNID)?"selected":((isset($CurrentProduct->MNID) && $CurrentProduct->MNID==$CurrentManufacturer->MNID)?"selected":""))?>><?php  echo $CurrentManufacturer->MNName?></option>
						<?php  					
					}?>
					</select>
				</div> 
			</div> 
			<div class="form-group">
				<label for="email" class="col-sm-4 col-md-5 col-lg-4 control-label">Small Description</label>
				<div class="col-sm-20 col-md-19 col-lg-20 text-left">
				<?php  
					$sContent = isset($_POST['SmallDescription'])?MyStripSlashes($_POST['SmallDescription'],true):(isset($CurrentProduct->SmallDescription)?MyStripSlashes($CurrentProduct->SmallDescription,true):"");
					$SKEditorObj->CArray = array("Width"=>"500",
												 "Height"=>"250",
												 "DMode"=>"tiny",
												 "Help"=>true,
												 "EditorArray"=>array("css"=>DIR_WS_SITE_CONTROL."style.css",
												 					  ),
												 ); 
					$SKEditorObj->CreateEditor("SmallDescription",$sContent);?>
			</div>
			</div>
					
			<div class="form-group">
				<label for="email" class="col-sm-4 col-md-5 col-lg-4 control-label">Large Description</label>
				<div class="col-sm-20 col-md-19 col-lg-20 text-left">
				<?php  
					$sContent = isset($_POST['LargeDescription'])?MyStripSlashes($_POST['LargeDescription'],true):(isset($CurrentProduct->LargeDescription)?MyStripSlashes($CurrentProduct->LargeDescription,true):"");
					$SKEditorObj->CArray = array("Width"=>"750",
												 "Height"=>"400",
												 "DMode"=>"Large2",
												 "EditorArray"=>array("css"=>DIR_WS_SITE_CONTROL."style.css",
												 					  ),
												 ); 
					$SKEditorObj->CreateEditor("LargeDescription",$sContent);?>
				</div>
			</div>
			
			
			
			
			
			<?php   if($ProductID ==0 && false):?>
			<div class="form-group">
				<label for="email" class="col-sm-4 col-md-5 col-lg-4 control-label">Product Type</label>
				<div class="col-sm-20 col-md-19 col-lg-20 text-left">
				<select name="ProductType" id="ProductType">
					<?php  
					foreach ($ProductTypeArray as $k=>$v)
					{
						?>
						<option value="<?php  echo $k?>" <?php  echo (isset($_POST['ProductType']) && $_POST['ProductType']==$k)?"selected":((isset($CurrentProduct->ProductType) && $CurrentProduct->ProductType==$k)?"selected":"")?>><?php  echo $v?></option>
						<?php  
					}
					?>
				</select>
				
				</div>
			</div>
			<?php  endif;?>
			
					<div class="ProductTypeDiv">
						<div id="Product_Type_Simple" class="DivProductType" style="<?php  echo (isset($CurrentProduct->ProductType) && $CurrentProduct->ProductType=="Simple")?"display:block;":"display:none;"?>">
							<div class="form-group">
								<label for="email" class="col-sm-4 col-md-5 col-lg-4 control-label">Weight</b></label>
								<div class="col-sm-20 col-md-19 col-lg-20 text-left"><input type="text" name="Weight" value="<?php  echo isset($_POST['Weight'])?$_POST['Weight']:(isset($CurrentProduct->Weight)?MyStripSlashes($CurrentProduct->Weight):"")?>" size="10"></div>
							</div>
							<div class="form-group">
								<label for="email" class="col-sm-4 col-md-5 col-lg-4 control-label">Price</label>
								<div class="col-sm-20 col-md-19 col-lg-20 text-left"><input type="text" id="Price" onblur="UpdateDiscount();" name="Price" value="<?php  echo isset($_POST['Price'])?$_POST['Price']:(isset($CurrentProduct->Price)?MyStripSlashes($CurrentProduct->Price):"")?>" size="10"></div>
							</div>
							<div class="form-group">
								<label for="email" class="col-sm-4 col-md-5 col-lg-4 control-label">Sale Price</label>
								<div class="col-sm-20 col-md-19 col-lg-20 text-left">
									<input type="text" id="SalePrice" name="SalePrice" onblur="UpdateDiscount();" value="<?php  echo isset($_POST['SalePrice'])?$_POST['SalePrice']:(isset($CurrentProduct->SalePrice)?MyStripSlashes($CurrentProduct->SalePrice):"")?>" size="10">
									<?php   
										$disc = "";
										$rPrice = floatval(isset($_POST['Price'])?$_POST['Price']:(isset($CurrentProduct->Price)?MyStripSlashes($CurrentProduct->Price):"0"));
										$sPrice = floatval(isset($_POST['SalePrice'])?$_POST['SalePrice']:(isset($CurrentProduct->SalePrice)?MyStripSlashes($CurrentProduct->SalePrice):"0"));
										$svPrice = $rPrice - $sPrice;
										if($rPrice > 0 && $sPrice > 0)
											$disc = number_format(($svPrice/$rPrice)*100, 2);
									?>
									&nbsp;&nbsp;Discount(%) : <input type="text" id="SaleDiscount" name="SaleDiscount" value="<?php  echo $disc?>" size="10" onblur="UpdateSalePrice();">
									<input type="checkbox" name="SaleActive" value="1" class="chk" <?php  echo (isset($_POST['SaleActive']) && $_POST['SaleActive']==1)?"checked":((isset($CurrentProduct->SaleActive) && $CurrentProduct->SaleActive==1)?"checked":"")?>> Check to display Sale Price
								</div>
							</div>
							<div class="form-group">
								<label for="email" class="col-sm-4 col-md-5 col-lg-4 control-label">Stock</label>
								<div class="col-sm-20 col-md-19 col-lg-20 text-left"><input type="text" name="Stock" value="<?php  echo isset($_POST['Stock'])?$_POST['Stock']:(isset($CurrentProduct->Stock)?MyStripSlashes($CurrentProduct->Stock):"25")?>" size="10">								
								</div>								
							</div>
							
							<div class="form-group">
								<label for="email" class="col-sm-4 col-md-5 col-lg-4 control-label">Tax </label>
								<div class="col-sm-20 col-md-19 col-lg-20 text-left">
									<select id="TaxClass" name="TaxClass">
										<?php  
										$TaxClassObj = new DataTable(TABLE_TAX_CLASS);
										$TaxClassObj->Where="1";
										$TaxClassObj->TableSelectAll("","TaxPercent ASC");
										while($Currentvat = $TaxClassObj->GetObjectFromRecord())
										{
										?>
										<option value="<?php  echo $Currentvat->TaxClassID?>"  <?php  echo isset($CurrentProduct->TaxClass) && $CurrentProduct->TaxClass==$Currentvat->TaxClassID?"selected":""?>><?php  echo isset($Currentvat->TaxClassTitle)?MyStripSlashes($Currentvat->TaxClassTitle):""?></option>
										<?php   
										}?>
									</select>						
								</div>								
							</div>
							
							<hr/>
						</div>
						
						
						
						<div id="Product_Type_Bundle" class="DivProductType" style="display:none;">
							
								<?php    
							$AttributeObj = new DataTable(TABLE_ATTRIBUTE);
							$AttributeObj->Where ="AttributeStatus='product_child'";
							$AttributeObj->TableSelectAll();
							while($CurrentAttribute = $AttributeObj->GetObjectFromRecord())
							{
								?>
									
								<div class="form-group">
										<label for="email" class="col-sm-4 col-md-5 col-lg-4 control-label">&nbsp;</label>
										<div class="col-sm-20 col-md-19 col-lg-20 text-left">
											<label><input type="checkbox" name="ProductAttribute[]" value="<?php  echo $CurrentAttribute->AttributeID?>" id="ProductAttribute">&nbsp;<?php  echo $CurrentAttribute->AttributeName?></label>
									</div>
								</div>
								<?php  
							}

							
							?>
							
						</div>
					</div>
				
			<hr>
			
			<div class="form-group">
				<label for="email" class="col-sm-4 col-md-5 col-lg-4 control-label">Upload Image</label>
				<div class="col-sm-20 col-md-19 col-lg-20 text-left">
				<input type="file" name="Upload">
					<b>Alt/Title:</b> <input type="text" name="ImageTitle" value="<?php  echo isset($_POST['ImageTitle'])?$_POST['ImageTitle']:(isset($CurrentProduct->ImageTitle)?MyStripSlashes($CurrentProduct->ImageTitle):"")?>" size="50">
						<?php  
						if(isset($CurrentProduct->Image) && $CurrentProduct->Image !="" && file_exists(DIR_FS_SITE_UPLOADS_PRODUCT_ORIGINAL.$CurrentProduct->Image))
						{?>
							<div>
								<a href="<?php  echo DIR_WS_SITE_UPLOADS_PRODUCT_ORIGINAL.$CurrentProduct->Image?>" target="_blank">
									<?php  SKImgDisplay(DIR_FS_SITE_UPLOADS_PRODUCT_ORIGINAL.$CurrentProduct->Image,70,'&nbsp;');?>
									<br>
									View Enlarge
								</a>
							</div>
						<?php  
						}?>
					<br>
				</div>
			</div>
			<div class="form-group">
				<label for="email" class="col-sm-4 col-md-5 col-lg-4 control-label">Display</label>
				<div class="col-sm-20 col-md-19 col-lg-20 text-left">
					<input type="checkbox" name="Featured" value="1" class="chk" <?php  echo (isset($_POST['Featured']) && $_POST['Featured']==1)?"checked":((isset($CurrentProduct->Featured) && $CurrentProduct->Featured==1)?"checked":"")?>><b>Featured</b>&nbsp;&nbsp;&nbsp;&nbsp;
					<?php /*<input type="checkbox" name="Homepage" value="1" class="chk" <?php  echo (isset($_POST['Homepage']) && $_POST['Homepage']==1)?"checked":((isset($CurrentProduct->Homepage) && $CurrentProduct->Homepage==1)?"checked":"")?>><b>Special Offer</b>&nbsp;&nbsp;&nbsp;&nbsp;
					<input type="checkbox" name="BestSeller" value="1" class="chk" <?php  echo (isset($_POST['BestSeller']) && $_POST['BestSeller']==1)?"checked":((isset($CurrentProduct->BestSeller) && $CurrentProduct->BestSeller==1)?"checked":"")?>><b>Best Seller</b>&nbsp;&nbsp;&nbsp;&nbsp; */ ?>
				</div>
			</div>
			<?php    
			if(@constant("DEFINE_PRODUCT_TEMPLATE")=="1")
			{
				$Template = ((isset($_POST['Template']) && $_POST['Template'] !="")?$_POST['Template']:((isset($CurrentProduct->Template) && $CurrentProduct->Template !="")?$CurrentProduct->Template:DEFINE_PRODUCT_TEMPLATE_DEFAULT));
				?>
			<div class="form-group">
				<label for="email" class="col-sm-4 col-md-5 col-lg-4 control-label">Template</label>
				<div class="col-sm-20 col-md-19 col-lg-20 text-left">
					<select name="Template">														
					<?php    foreach (SKGetTemplates('product') as $k=>$v)
					{?>
					<option value="<?php  echo $k?>" <?php  echo $Template==$k?"selected":""?>><?php  echo $v?></option>
					<?php  
					}?>
					</select>
				</div>
			</div>
			<?php  
			}?>
			<div class="form-group">
				<label for="email" class="col-sm-4 col-md-5 col-lg-4 control-label">Active</label>
				<div class="col-sm-20 col-md-19 col-lg-20 text-left">
				<input type="checkbox" name="Active" value="1" class="chk" <?php  echo (isset($_POST['Active']) && $_POST['Active']==1)?"checked":((isset($CurrentProduct->Active) && $CurrentProduct->Active==1)?"checked":"")?>>
				</div>
			</div>
			
			
			
			
			<!-- <div class="form-group">
				<label for="email" class="col-sm-4 col-md-5 col-lg-4 control-label">Banner</label>
				<div class="col-sm-20 col-md-19 col-lg-20 text-left">
					<select name="UpperBanner1[]" id="UpperBanner1" size="10" multiple>
					<option value="">--Select--</option>
					<?php  
						$BannerObj->Where = "Alignment='H'";
						$BannerObj->TableSelectAll("","Position ASC");
		
					while($CurrentBanner = $BannerObj->GetObjectFromRecord())
					{?>
						<option value="<?php  echo $CurrentBanner->BannerID?>" <?php  echo (isset($_POST['UpperBanner1']) && in_array($CurrentBanner->BannerID,$_POST['UpperBanner1']))?"selected":((isset($CurrentProduct->UpperBanner1) && in_array($CurrentBanner->BannerID,explode(",",$CurrentProduct->UpperBanner1)))?"selected":"")?>><?php  echo $CurrentBanner->BannerName?></option>
						<?php  
					}?>
					</select>
				</div>
			</div> -->
			
			<!--
			<tr>
				<td align="center" class="InsideLeftTd"><b>Purchase Online</b></td>
				<td align="left" class="InsideRightTd"><input type="checkbox" name="PurchaseOnline" value="1" class="chk" <?php  echo (isset($_POST['PurchaseOnline']) && $_POST['PurchaseOnline']==1)?"checked":((isset($CurrentProduct->PurchaseOnline) && $CurrentProduct->PurchaseOnline==1)?"checked":"")?>></td>
			</tr>
			-->
			<div class="form-group">
				<label for="email" class="col-sm-4 col-md-5 col-lg-4 control-label">Categories</label>
				<div class="col-sm-20 col-md-19 col-lg-20 text-left">
					<select id="CategoryArr" name="CategoryArr[]" size="20" style="width:320px;" multiple>
					<option value="">--Select Categories--</option>
					<?php  
					$CategorySelected = isset($_POST['CategoryArr'])?$_POST['CategoryArr']:(isset($ProductCatgoryArr)?$ProductCatgoryArr:array());
					$CategoryOption =CategoryChain(0,0,$CategorySelected,false,0,0,1);
					echo $CategoryOption;?>
					?>
					</select>
					<br>
					<small>For multiple selection, please press Ctrl Key + select the categories</small>
				</div>
			</div>
			<hr>
			<!--<tr>
				<td align="center" class="InsideLeftTd"><b>Banner</b></td>
				<td align="left" class="InsideRightTd">
					<select name="UpperBanner1[]" id="UpperBanner1" size="10" multiple>
					<option value="">--Select--</option>
					<?php  
						$BannerObj->Where = "Alignment='H'";
						$BannerObj->TableSelectAll("","Position ASC");
		
					while($CurrentBanner = $BannerObj->GetObjectFromRecord())
					{?>
						<option value="<?php  echo $CurrentBanner->BannerID?>" <?php  echo (isset($_POST['UpperBanner1']) && in_array($CurrentBanner->BannerID,$_POST['UpperBanner1']))?"selected":((isset($CurrentProduct->UpperBanner1) && in_array($CurrentBanner->BannerID,explode(",",$CurrentProduct->UpperBanner1)))?"selected":"")?>><?php  echo $CurrentBanner->BannerName?></option>
						<?php  
					}?>
					</select>
				</td>
			</tr>-->
			
				<?php  
				$AttributeObj->Where="0 AND AttributeType in('product','both') AND AttributeStatus='custom'";
				$AttributeObj->TableSelectAll("","Position ASC");
				while ($CurrentAttribute = $AttributeObj->GetObjectFromRecord())
				{
					$AttributeValueObj->Where = "AttributeID='".$CurrentAttribute->AttributeID."'";
					$AttributeValueObj->TableSelectAll("","Position ASC");
					
					$ProductRelationObj->Where ="RelationType='attribute_value' AND ProductID='".$ProductObj->MysqlEscapeString($ProductID)."'";
					$CurrentProductRelation = $ProductRelationObj->TableSelectOne(array("GROUP_CONCAT(DISTINCT RelationID ORDER BY RelationID DESC SEPARATOR ',') as AttributeValues"));	
					$AttributeValueArray = isset($CurrentProductRelation->AttributeValues)?explode(",",$CurrentProductRelation->AttributeValues):array();

				?>
					<hr>
					<div class="form-group">
					<label for="email" class="col-sm-4 col-md-5 col-lg-4 control-label"><?php  echo MyStripSlashes($CurrentAttribute->AttributeName)?></label>
					   <div class="col-sm-20 col-md-19 col-lg-20 text-left">
						<div style="width:100%;height:200px;overflow:scroll;">
				    	<?php  while($CurrentAttributeValue = $AttributeValueObj->GetObjectFromRecord())
						{?>
							<input <?php  echo (is_array($AttributeValueArray) && in_array($CurrentAttributeValue->AttributeValueID,$AttributeValueArray))?"checked":""?> type="checkbox" value="<?php  echo $CurrentAttributeValue->AttributeValueID?>" name="RelAttributeValue[]" id="RelAttributeValue_<?php  echo $CurrentAttributeValue->AttributeValueID?>">
							<label for="RelAttributeValue_<?php  echo $CurrentAttributeValue->AttributeValueID?>"><?php  echo MyStripSlashes($CurrentAttributeValue->AttributeValue);?></label>
							<br />
						<?php  }?>
						</div>
						</div>
					</div>
					<?php  
				}?>			
							 
			<div class="form-group">
				<div class="col-sm-6 pull-right">
					<button class="btn btn-warning  btn-block" type="submit" name="AddProduct">Submit</button>
				</div>
			</div>
	</div>
</form>
	
<script type="text/javascript">
	jQuery(document).ready(function($){
		
		$("#ProductType").change(function(){
			ChangeProductType($("#ProductType").val());
		});
		ChangeProductType($("#ProductType").val());
	});

	function ChangeProductType(val)
	{	
		<?php   if($ProductID ==0):?>
		//$(".DivProductType").hide();
		$("#Product_Type_Simple").show();
		<?php   endif;?>
		
	}
	
	function UpdateSalePrice(){
		var disc = $("#SaleDiscount").val();
		if(!isNaN(parseFloat($("#Price").val())) && !isNaN(parseFloat(disc))){
			var rPrice = parseFloat($("#Price").val());								
			var svPrice = (disc/100)*rPrice;
			var sPrice = rPrice - svPrice;
			$("#SalePrice").val(sPrice.toFixed(2));
		}
	}
	
	function UpdateDiscount(){
		var sPrice = $("#SalePrice").val();
		if(!isNaN(parseFloat($("#Price").val())) && !isNaN(parseFloat(sPrice))){
			var rPrice = parseFloat($("#Price").val());								
			var svPrice = rPrice - parseFloat(sPrice);
			var disc = (svPrice/rPrice)*100;
			$("#SaleDiscount").val(disc.toFixed(2));
		}
	}
</script>
	
	<?php  
	/////// Add Product End
	break;
	
	case "ChildProduct":
	?>
			<table border="0" cellpadding="3" cellspacing="1" width="100%" class="InsideTable">
				<tr>
					<td align="right"><a id="AddChildProduct" href="<?php  echo DIR_WS_SITE_CONTROL?>index.php?Page=shop/product_child&Section=AddChildProduct&MasterID=<?php  echo $ProductID?>&Popup=true" class="adm-link">Add New Child/Attribute Product</a></td>
				</tr>
				<tr>
					<td><div id="DivChildProduct"></div></td>
				</tr>
			</table>
		<script type="text/javascript">
	jQuery(document).ready(function($){
		
		FancyBoxStart("1");
		UpdateChildProduct();
	});

	function FancyBoxStart(start)
	{
		if(start=="1")
		{
			$("#AddChildProduct").fancybox({
				'width'				: '70%',
				'height'			: '80%',
				'autoScale'			: false,
				'transitionIn'		: 'none',
				'transitionOut'		: 'none',
				'type'				: 'iframe',
				onClosed	:	function() {
		           UpdateChildProduct();
				}
			});
		}
		
		if(start=="2")
		{
			$(".child_fancy").fancybox({
				'width'				: '70%',
				'height'			: '80%',
				'autoScale'			: false,
				'transitionIn'		: 'none',
				'transitionOut'		: 'none',
				'type'				: 'iframe',
				 onClosed	:	function() {
		           UpdateChildProduct();
				}
			});
		}
		
	}
	function UpdateChildProduct()
	{	
		$.ajax({
			   type: "GET",
			   url: "index.php",
			   data: "Page=shop/product_child&MasterID=<?php  echo $ProductID?>",
			   success: function(msg){
			   	$("#DivChildProduct").html(msg);
			   	FancyBoxStart("2");
			   }
			 });
		
	}	
	function ChildProductEdit(obj)
	{	
		obj.fancybox({
				'width'				: '70%',
				'height'			: '80%',
				'autoScale'			: false,
				'transitionIn'		: 'none',
				'transitionOut'		: 'none',
				'type'				: 'iframe',
				onClosed	:	function() {
		           UpdateChildProduct();
				}
			}).open();
		
		return false;
	}	
	
</script>	
		<?php  
		break;
	
	case "Questions":
	case "EditQuestion":
	$ProductQuestionObj->Where = "ProductID='".$ProductID."'";
	$ProductQuestionObj->TableSelectAll("","Position ASC");
	?>
	<script type="text/javascript">
				function FunctionDeleteQuestion()
				{
					result = confirm("Are you sure want to delete this Question?")
					if(result == true)
					{
						return true;
					}
					return false;
				}				
							
			</script>
	<table border="0" cellpadding="3" cellspacing="1" width="100%" class="InsideTable">
			<tr class="InsideLeftTd">
				<td align="center"><b>S.No.</b></td>
				<td align="left"><b>Email</b></td>
				<td align="left"><b>Question</b></td>
				<td align="left"><b><b>Answer</b></b></td>
				<td align="center"><b>Active</b></td>
				<td align="center"><b>Position</b></td>
				<td align="center">&nbsp;</td>
				<td align="center">&nbsp;</td>
			</tr>
		<?php  
			$SNo=1;
			$Count=1;
			while($CurrentQuestion = $ProductQuestionObj->GetObjectFromRecord())
			{
				if($Section=="EditQuestion" && $QuestionID==$CurrentQuestion->QuestionID)
				{
					?>
					<form method="POST" action="<?php  echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php  echo $Page?>&Section=<?php  echo $Section?>&Pg=<?php  echo $Pg?>&ParentID=<?php  echo $ParentID?>&ProductID=<?php  echo $ProductID?>&QuestionID=<?php  echo $QuestionID?>&Target=EditQuestion" enctype="multipart/form-data">
						<tr class="InsideRightTd">
							<td align="center"><b><?php  echo $SNo?></b></td>
							<td align="center"><input type="text" name="CEmail" value="<?php  echo MyStripSlashes($CurrentQuestion->CEmail);?>" size="20"></td>
							<td align="left"><textarea name="Question" rows=4 cols=25><?php  echo MyStripSlashes($CurrentQuestion->Question);?></textarea></td>
							<td><textarea id="Answer" name="Answer" rows=4 cols=25><?php  echo MyStripSlashes($CurrentQuestion->Answer); ?></textarea></td>
							<td align="center"><input type="checkbox" name="Active" value="1" <?php  echo $CurrentQuestion->Active=="1"?"checked":""?> class="chk"></td>
							<td align="center"><input type="text" name="Position" value="<?php  echo MyStripSlashes($CurrentQuestion->Position);?>" size="5" style="text-align:center;"></td>
							<td align="center"><input type="submit" name="Submit" value="Update Question"></td>
							<td align="center"><input type="button" name="button" value="Cancel" onclick="javascript:history.back();"></td>
						</tr>
					</form>
					<?php  		
				}
				else 
				{
					?>
						<tr class="InsideRightTd">
							<td align="center""><b><?php  echo $SNo?></b></td>
							<td align="left"><?php  echo MyStripSlashes($CurrentQuestion->CEmail);?></td>
							<td align="left"><?php  echo MyStripSlashes($CurrentQuestion->Question);?></td>
							<td align="left"><?php  echo MyStripSlashes($CurrentQuestion->Answer);?></td>
							<td align="center"><img src="<?php  echo DIR_WS_SITE_CONTROL_IMAGES.($CurrentQuestion->Active=="1"?"info.gif":"error.gif")?>">
							</td>
							<td align="center"><?php  echo MyStripSlashes($CurrentQuestion->Position);?>
							</td>
							<td align="center"><a href="<?php  echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php  echo $Page?>&Section=EditQuestion&Pg=<?php  echo $Pg?>&ParentID=<?php  echo $ParentID?>&ProductID=<?php  echo $ProductID?>&QuestionID=<?php  echo $CurrentQuestion->QuestionID?>"><b>Edit</b></a></td>
							<td align="center"><a href="<?php  echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php  echo $Page?>&Section=<?php  echo $Section?>&Pg=<?php  echo $Pg?>&ParentID=<?php  echo $ParentID?>&ProductID=<?php  echo $ProductID?>&Target=DeleteQuestion&QuestionID=<?php  echo $CurrentQuestion->QuestionID?>" onclick="return FunctionDeleteQuestion();"><b>Delete</b></a></td>
						</tr>
					<?php  
				}
			$SNo++;
			$Count++;
			}
		?>
		<form method="POST" action="<?php  echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php  echo $Page?>&Section=<?php  echo $Section?>&Pg=<?php  echo $Pg?>&ParentID=<?php  echo $ParentID?>&ProductID=<?php  echo $ProductID?>&Target=AddQuestion" enctype="multipart/form-data">
			<tr class="InsideRightTd">
				<td align="center"><b><?php  echo $SNo?></b></td>
				<td align="center"><input type="text" name="CEmail" value="" size="20"></td>
				<td align="left"><textarea name="Question" rows=4 cols=20></textarea></td>
				<td><textarea id="Answer" name="Answer" rows=4 cols=20></textarea></td>
				<td align="center"><input type="checkbox" name="Active" value="1" class="chk"></td>
				<td align="center"><input type="text" name="Position" size="5"></td>
				<td align="center"><input type="submit" name="Submit" value="Add Question"></td>
				<td align="center">&nbsp;</td>
			</tr>
		</form>	
		</table>		
	
	<?php  
	break;
	case "Related":
		$ProductObj = new DataTable(TABLE_PRODUCT. " p, ".TABLE_PRODUCT_RELATION. " ptc");
		$ProductObj->Where = "ptc.RelationID=p.ProductID and ptc.RelationType='product_related' and ptc.ProductID='".$ProductObj->MysqlEscapeString($ProductID)."'";
		$ProductObj->TableSelectAll("","SortOrder ASC");
		$Count =1;			
		?>
		<br/>
		<table border="0" cellpadding="3" cellspacing="1" width="800" class="table table-striped table-bordered table-responsive">
			<tr class="InsideLeftTd">
				<td><b>Related Product Name</b></td>
				<!--<td align="center" width="140"><b>Related Delivery</b></td>-->
				<td align="center"><b>Position</b></td>
				<td></td>
			</tr>
			<?php    if($ProductObj->GetNumRows() > 0):?>
				<form method="POST" action="<?php  echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php  echo $Page?>&Section=<?php  echo $Section?>&Pg=<?php  echo $Pg?>&ParentID=<?php  echo $ParentID?>&ProductID=<?php  echo $ProductID?>&Target=UpdateRelatedProduct" enctype="multipart/form-data">
					<?php    while ($CurrentProduct = $ProductObj->GetObjectFromRecord()):?>
						<tr >
							<td><?php    echo MyStripSlashes($CurrentProduct->ProductName)?></td>
							<!--<td align="center"><input type="checkbox" name="Custom_<?php  echo $Count?>" size="3" value="1" <?php  echo (isset($CurrentProduct->Custom) && $CurrentProduct->Custom=="1")?"checked":""?>></td>-->
							<td align="center"><input type="hidden" name="ID_<?php  echo $Count?>" size="3" value="<?php  echo $CurrentProduct->id?>" /><input type="text" name="SortOrder_<?php  echo $Count?>" size="3" value="<?php  echo $CurrentProduct->SortOrder?>" class="AlignCenter"></td>
							<td align="center"><a href="<?php  echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php  echo $Page?>&Target=DeleteRelatedProduct&ParentID=<?php  echo $ParentID?>&ProductID=<?php  echo $CurrentProduct->ProductID?>&ID=<?php  echo $CurrentProduct->id?>" onclick="return FunctionDeleteRelatedProduct();"><b>Delete</b></a></td>						
						</tr>
						<?php    $Count+=1;?>
					<?php    endwhile; ?>
					<tr>
						<td><input type="hidden" name="Count" value="<?php  echo $Count?>"></td>
						<td align="center"><input type="submit" name="UpdateActive" value="Update"></td>
						<td></td>
					</tr>
				</form>
			<?php    else :?>
				<tr class="InsideRightTd">
					<td colspan="3">No products found.</td>
				</tr>	
			<?php    endif;?>
			<tr>
				<td align="center" colspan="3"><hr style="height:2px;color:#000;background-color:#000;"></td>
			</tr>
			<form method="POST" action="<?php  echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php  echo $Page?>&Section=<?php  echo $Section?>&Pg=<?php  echo $Pg?>&ParentID=<?php  echo $ParentID?>&ProductID=<?php  echo $ProductID?>&Target=AddRelatedProduct" enctype="multipart/form-data">
				<tr class="InsideRightTd">
					<td>
						<input type="hidden" value="" name="_RelationProductID" id="_RelationProductID">
						<select data-order="true" id="RelationProductID" name="RelationProductID[]" multiple size="10" data-placeholder="Choose a Product..." style="width:600px;">
							<?php    
								$ProductObj = new DataTable(TABLE_PRODUCT);
								$ProductObj->Where = "ProductType !='Child' AND ProductID NOT IN (select RelationID FROM ".TABLE_PRODUCT_RELATION." WHERE RelationType='product_related' and ProductID='".$ProductObj->MysqlEscapeString($ProductID)."')";
								$ProductObj->TableSelectAll("","ModelNo, ProductName ASC");
								while ($CurrentProduct = $ProductObj->GetObjectFromRecord()):
							?>		<option value="<?php    echo MyStripSlashes($CurrentProduct->ProductID)?>"><?php    echo MyStripSlashes($CurrentProduct->ModelNo)." -- ".MyStripSlashes($CurrentProduct->ProductName)?></option>
							<?php    endwhile;?>
						</select>
						<script type="text/javascript">
							$(function(){
								$("#RelationProductID").chosen();	
								 $("#RelationProductID").each(function(){
								    if($(this).data("order")==true){
								        $(this).val('');
								        $(this).chosen();
								        var $ordered = $('[name="_'+jQuery(this).attr('id')+'"]');
								        var selected = $ordered.val().split(',');
								        var chosen_object = $(this).data('chosen');
								        $.each(selected, function(index, item){
								            $.each(chosen_object.results_data, function(i, data){
								                if(data.value == item){
								                    $("#"+data.dom_id).trigger('mouseup');
								                }
								            });
								        });
								        $(this).data("backupVal",$(this).val());
								        $(this).change(function(){
								            var backup = $(this).data("backupVal");
								            var current = $(this).val();
								            if(backup == null){
								                backup = [];
								            }
								            if(current == null){
								                current = [];
								            }
								            if(backup.length > current.length){                         
								                $.each(backup, function(index, item) { 
								                    if($.inArray(item, current) < 0){
								                        for(var i=0; i<selected.length; i++){
								                            if(selected[i] == item){
								                                selected.splice(i,1);
								                            }
								                        }
								                    }
								                });
								            }else if(backup.length < current.length){
								                $.each(current, function(index, item) { 
								                    if($.inArray(item, backup) < 0){
								                        selected.push(item);
								                    }
								                });
								            }
								            $ordered.val(selected.join(','));
								            $(this).data("backupVal",current);
								        });
								    }else{
								        $(this).chosen();
								    }
								});
															
							});
						</script>						
					</td>
					<td align="center">&nbsp;</td>
					<td align="center"><input type="submit" value="Add Item" /></td>
				</tr>
			</form>			
		</table>		
		<script type="text/javascript">
			function FunctionDeleteRelatedProduct()
			{
				result = confirm("Are you sure want to delete the related item?")
				if(result == true)
					return true;
				return false;
			}				
		</script>
		<?php  
	break;
	case "Similar":
		$ProductObj = new DataTable(TABLE_PRODUCT. " p, ".TABLE_PRODUCT_RELATION. " ptc");
		$ProductObj->Where = "ptc.RelationID=p.ProductID and ptc.RelationType='product_similar' and ptc.ProductID='".$ProductObj->MysqlEscapeString($ProductID)."'";
		$ProductObj->TableSelectAll("","SortOrder ASC");
		$Count =1;			
		?>
		<br/>
		<table border="0" cellpadding="3" cellspacing="1" width="800" class="InsideTable">
			<tr class="InsideLeftTd">
				<td><b>Similar Product Name</b></td>
				<td align="center"><b>Position</b></td>
				<td></td>
			</tr>
			<?php    if($ProductObj->GetNumRows() > 0):?>
				<form method="POST" action="<?php  echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php  echo $Page?>&Section=<?php  echo $Section?>&Pg=<?php  echo $Pg?>&ParentID=<?php  echo $ParentID?>&ProductID=<?php  echo $ProductID?>&Target=UpdateSimilarProduct" enctype="multipart/form-data">
					<?php    while ($CurrentProduct = $ProductObj->GetObjectFromRecord()):?>
						<tr class="InsideRightTd">
							<td><?php    echo MyStripSlashes($CurrentProduct->ProductName)?></td>
							<td align="center"><input type="hidden" name="ID_<?php  echo $Count?>" size="3" value="<?php  echo $CurrentProduct->id?>" /><input type="text" name="SortOrder_<?php  echo $Count?>" size="3" value="<?php  echo $CurrentProduct->SortOrder?>" class="AlignCenter"></td>
							<td align="center"><a href="<?php  echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php  echo $Page?>&Target=DeleteSimilarProduct&ParentID=<?php  echo $ParentID?>&ProductID=<?php  echo $CurrentProduct->ProductID?>&ID=<?php  echo $CurrentProduct->id?>" onclick="return FunctionDeleteSimilarProduct();"><b>Delete</b></a></td>						
						</tr>
						<?php    $Count+=1;?>
					<?php    endwhile; ?>
					<tr class="InsideRightTd">
						<td><input type="hidden" name="Count" value="<?php  echo $Count?>"></td>
						<td align="center"><input type="submit" name="UpdateActive" value="Update"></td>
						<td></td>
					</tr>
				</form>
			<?php    else :?>
				<tr class="InsideRightTd">
					<td colspan="3">No products found.</td>
				</tr>	
			<?php    endif;?>
			<tr>
				<td align="center" colspan="3"><hr style="height:2px;color:#000;background-color:#000;"></td>
			</tr>
			<form method="POST" action="<?php  echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php  echo $Page?>&Section=<?php  echo $Section?>&Pg=<?php  echo $Pg?>&ParentID=<?php  echo $ParentID?>&ProductID=<?php  echo $ProductID?>&Target=AddSimilarProduct" enctype="multipart/form-data">
				<tr class="InsideRightTd">
					<td>
						<input type="hidden" value="" name="_RelationProductID" id="_RelationProductID">
						<select data-order="true" id="RelationProductID" name="RelationProductID[]" multiple size="10" data-placeholder="Choose a Product..." style="width:600px;">
							<?php    
								$ProductObj = new DataTable(TABLE_PRODUCT);
								$ProductObj->Where = "ProductID NOT IN (select RelationID FROM ".TABLE_PRODUCT_RELATION." WHERE RelationType='product_similar' and ProductID='".$ProductObj->MysqlEscapeString($ProductID)."')";
								$ProductObj->TableSelectAll("","ModelNo, ProductName ASC");
								while ($CurrentProduct = $ProductObj->GetObjectFromRecord()):
							?>		<option value="<?php    echo MyStripSlashes($CurrentProduct->ProductID)?>"><?php    echo MyStripSlashes($CurrentProduct->ModelNo)." -- ".MyStripSlashes($CurrentProduct->ProductName)?></option>
							<?php    endwhile;?>
						</select>
						<script type="text/javascript">
							$(function(){
								$("#RelationProductID").chosen();	
								 $("#RelationProductID").each(function(){
								    if($(this).data("order")==true){
								        $(this).val('');
								        $(this).chosen();
								        var $ordered = $('[name="_'+jQuery(this).attr('id')+'"]');
								        var selected = $ordered.val().split(',');
								        var chosen_object = $(this).data('chosen');
								        $.each(selected, function(index, item){
								            $.each(chosen_object.results_data, function(i, data){
								                if(data.value == item){
								                    $("#"+data.dom_id).trigger('mouseup');
								                }
								            });
								        });
								        $(this).data("backupVal",$(this).val());
								        $(this).change(function(){
								            var backup = $(this).data("backupVal");
								            var current = $(this).val();
								            if(backup == null){
								                backup = [];
								            }
								            if(current == null){
								                current = [];
								            }
								            if(backup.length > current.length){                         
								                $.each(backup, function(index, item) { 
								                    if($.inArray(item, current) < 0){
								                        for(var i=0; i<selected.length; i++){
								                            if(selected[i] == item){
								                                selected.splice(i,1);
								                            }
								                        }
								                    }
								                });
								            }else if(backup.length < current.length){
								                $.each(current, function(index, item) { 
								                    if($.inArray(item, backup) < 0){
								                        selected.push(item);
								                    }
								                });
								            }
								            $ordered.val(selected.join(','));
								            $(this).data("backupVal",current);
								        });
								    }else{
								        $(this).chosen();
								    }
								});
															
							});
						</script>						
					</td>
					<td align="center">&nbsp;</td>
					<td align="center"><input type="submit" value="Add Item" /></td>
				</tr>
			</form>			
		</table>		
		<script type="text/javascript">
			function FunctionDeleteSimilarProduct()
			{
				result = confirm("Are you sure want to delete the similar item?")
				if(result == true)
					return true;
				return false;
			}				
		</script>
		<?php  
	break;
	case "Shipping":
		?>
		<form method="POST" action="<?php  echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php  echo $Page?>&Section=<?php  echo $Section?>&Target=UpdateShipping&ParentID=<?php  echo $ParentID?>&ProductID=<?php  echo $ProductID?>">
		<table border="0" cellpadding="3" cellspacing="1" width="100%" class="InsideTable">
				<tr>
					<td align="left"><h3><?php  echo MyStripSlashes($CurrentProduct->ProductName)?>'s Shipping</h3></td>
				</tr>
				<tr>
					<td>
						<div id="accordion">
						<?php  
						$ShippingTypeObj = new DataTable(TABLE_SHIPPING_PZS_TYPES);
						$ShippingTypeObj->Where = "1";
						$ShippingTypeObj->TableSelectAll("","Position ASC");
						$ShippingTypeArray = array();
						while($CurrentShippingType = $ShippingTypeObj->GetObjectFromRecord())
						{
							$ShippingTypeArray[$CurrentShippingType->TypeID] = MyStripSlashes($CurrentShippingType->TypeName);
						}
		
						$ShippingGroupObj->Where = "1";
						$ShippingGroupObj->TableSelectAll("","CreatedDate ASC");
						if($ShippingGroupObj->GetNumRows() > 0)
						{
							while($CurrentShippingGroup = $ShippingGroupObj->GetObjectFromRecord())
							{
								$ShippingZoneObj->Where = "ShippingID = '$CurrentShippingGroup->ShippingID'";
								$ShippingZoneObj->TableSelectAll("","DefaultZone='1' DESC, ZoneName ASC");
								$ShippingZoneArray = array();
								while($CurrentShippingZone = $ShippingZoneObj->GetObjectFromRecord())
								{
									$ShippingZoneArray[$CurrentShippingZone->ZoneID] = MyStripSlashes($CurrentShippingZone->ZoneName);
								}
								
						?>
							<h3 style="padding-left:20px;"><a href="#"><b><?php  echo MyStripSlashes($CurrentShippingGroup->ShippingGroup);?> Shipping Group</b></a></h3>
						 	<div>
						 		<table border="1" cellpadding="5" cellspacing="2" width="100%" class="InsideTable">
						 		<tr>
						 			<td class="InsideLeftTd" width="200">&nbsp;</td>
					 				<?php   foreach ($ShippingTypeArray as $TypeID=>$TypeName):?>
							 			<td class="InsideLeftTd"><b><?php  echo $TypeName;?></b></td>
							 		<?php  endforeach;?>
						 		</tr>
						 		<?php   foreach ($ShippingZoneArray as $ZoneID=>$ZoneName):
						 		?>
							 		<tr>
							 			<td class="InsideLeftTd"><?php  echo $ZoneName?>
							 			<a rel="colorbox_ajax" href="<?php  echo DIR_WS_SITE_CONTROL?>index.php?Page=shipping&Meta=false&Popup=true&Section=PostCodeView&Shipping=ProductZoneShipping&ZoneID=<?php  echo $ZoneID?>" target="_blank" class="adm-link">?</a></td>
						 				<?php   foreach ($ShippingTypeArray as $TypeID=>$TypeName):
						 				$ShippingProductObj->Where = "ProductID='".$ProductID."' AND ZoneID = '$ZoneID' AND TypeID= '$TypeID' AND Active = '1'";
						 				$CurrentShipPrice = $ShippingProductObj->TableSelectOne();
						 				?>
								 			<td>
								 			<input type="checkbox" name="ChkShip[<?php  echo $ZoneID?>][<?php  echo $TypeID?>]" value="1" <?php  echo (isset($_POST['ChkShip'][$ZoneID][$TypeID]) && $_POST['ChkShip'][$ZoneID][$TypeID]==1)?"checked":((isset($CurrentShipPrice->Active) && $CurrentShipPrice->Active==1)?"checked":"")?>>
								 			<input type="text" name="Ship[<?php  echo $ZoneID?>][<?php  echo $TypeID?>]" value="<?php  echo isset($_POST['Ship'][$ZoneID][$TypeID])?$_POST['Ship'][$ZoneID][$TypeID]:(isset($CurrentShipPrice->ShipPrice)?MyStripSlashes($CurrentShipPrice->ShipPrice):"")?>" size="8">
								 			</td>
								 		<?php   endforeach;?>
							 		</tr>
							 	<?php   endforeach;?>	
						 		</table>
						 	</div>
						 	<?php  
							}
						}?>
							
						</div>
					</td>
				</tr>
				<tr>
					<td align="center"><input type="submit" value="Submit"></td>
				</tr>
				
			</table>
		</form>	
		<script type="text/javascript">
			jQuery(document).ready(function(){
				$("#accordion").accordion({
					autoHeight:false,
					clearStyle: true 
				});
			});
		</script>
		<?php  
		break;
	case "ShippingPopUp":
		@ob_clean();
	?>
	<h3><?php  echo isset($CurrentProduct->ProductName)?$CurrentProduct->ProductName:""?></h3>
	<table border="0" cellpadding="3" cellspacing="1" width="100%" class="InsideTable">
				<tr>
					<td align="left"><h3><?php  echo MyStripSlashes($CurrentProduct->ProductName)?>'s Shipping</h3></td>
				</tr>
				<tr>
					<td>
						<div>
						<?php  
						$ShippingTypeObj = new DataTable(TABLE_SHIPPING_PZS_TYPES);
						$ShippingTypeObj->Where = "1";
						$ShippingTypeObj->TableSelectAll("","Position ASC");
						$ShippingTypeArray = array();
						while($CurrentShippingType = $ShippingTypeObj->GetObjectFromRecord())
						{
							$ShippingTypeArray[$CurrentShippingType->TypeID] = MyStripSlashes($CurrentShippingType->TypeName);
						}
		
						$ShippingGroupObj->Where = "1";
						$ShippingGroupObj->TableSelectAll("","CreatedDate ASC");
						if($ShippingGroupObj->GetNumRows() > 0)
						{
							while($CurrentShippingGroup = $ShippingGroupObj->GetObjectFromRecord())
							{
								$ShippingZoneObj->Where = "ShippingID = '$CurrentShippingGroup->ShippingID'";
								$ShippingZoneObj->TableSelectAll("","DefaultZone='1' DESC, ZoneName ASC");
								$ShippingZoneArray = array();
								while($CurrentShippingZone = $ShippingZoneObj->GetObjectFromRecord())
								{
									$ShippingZoneArray[$CurrentShippingZone->ZoneID] = MyStripSlashes($CurrentShippingZone->ZoneName);
								}
								
						?>
							<h3 style="padding-left:20px;font-size:13px;"><b><?php  echo MyStripSlashes($CurrentShippingGroup->ShippingGroup);?> Shipping Group</b></h3>
						 	<div>
						 		<table border="1" cellpadding="5" cellspacing="2" width="100%" class="InsideTable">
						 		<tr>
						 			<td class="InsideLeftTd" width="200">&nbsp;</td>
					 				<?php   foreach ($ShippingTypeArray as $TypeID=>$TypeName):?>
							 			<td class="InsideLeftTd"><b><?php  echo $TypeName;?></b></td>
							 		<?php  endforeach;?>
						 		</tr>
						 		<?php   foreach ($ShippingZoneArray as $ZoneID=>$ZoneName):
						 		?>
							 		<tr>
							 			<td class="InsideLeftTd"><?php  echo $ZoneName?></td>
						 				<?php   foreach ($ShippingTypeArray as $TypeID=>$TypeName):
						 				$ShippingProductObj->Where = "ProductID='".$ProductID."' AND ZoneID = '$ZoneID' AND TypeID= '$TypeID' AND Active = '1'";
						 				$CurrentShipPrice = $ShippingProductObj->TableSelectOne();
						 				?>
								 			<td>
								 			<?php  
								 			if(isset($CurrentShipPrice->Active) && $CurrentShipPrice->Active==1)
								 				echo MyStripSlashes($CurrentShipPrice->ShipPrice);
								 			else 
								 				echo "NA";
								 			?>
								 			</td>
								 		<?php   endforeach;?>
							 		</tr>
							 	<?php   endforeach;?>	
						 		</table>
						 	</div>
						 	<?php  
							}
						}?>
							
						</div>
					</td>
				</tr>
			</table>
			<style type="text/css">
						td{font-size:11px;}
			</style>
		
	<?php  
	break;
	
	case "InfoPopUp":
		@ob_clean();
	$ProductInfoObj->Where = "ProductID='".$ProductID."'";
	$ProductInfoObj->TableSelectAll("","Position ASC");
	?>
	<h3><?php  echo isset($CurrentProduct->ProductName)?$CurrentProduct->ProductName:""?></h3>
	<table border="0" cellpadding="3" cellspacing="1" width="100%" class="InsideTable">
			<tr class="InsideLeftTd">
				<td align="center" width="50"><b>S.No.</b></td>
				<td align="left" width="100" valign="top"><b>Tab Title</b></td>
				<td align="left" valign="top"><b><b>Description</b></b></td>
				<td align="center" width="90" valign="top"><b>Active</b></td>
				<!--<td align="center" width="90" valign="top"><b>Position</b></td>-->
			</tr>
	<?php  
			$SNo=1;
			$Count=1;
			while($CurrentInfo = $ProductInfoObj->GetObjectFromRecord())
			{
					?>
					<tr class="InsideRightTd">
						<td align="center" ><b><?php  echo $SNo?></b></td>
						<td align="left" valign="top"><?php  echo MyStripSlashes($CurrentInfo->TabTitle);?></td>
						<td align="left" valign="top"><?php  SKDisplayPrdInfo($CurrentInfo);?></td>
						<td align="center" valign="top"><img src="<?php  echo DIR_WS_SITE_CONTROL_IMAGES.($CurrentInfo->Active=="1"?"info.gif":"error.gif")?>">
						</td>
						<!--<td align="center" valign="top"><?php  echo MyStripSlashes($CurrentInfo->Position);?>-->
						</td>
					</tr>
					<tr><td colspan="4"><hr></td></tr>
					<?php  
			$SNo++;
			$Count++;
			}
		?>
		</table>		
		<script type="text/javascript">
		DisplayInfoContent('InfoTypeNew','HTML');
		</script>
	<?php  
	break;
	
	case "Infos":
	case "EditInfo":
	$ProductInfoObj->Where = "ProductID='".$ProductID."'";
	$ProductInfoObj->TableSelectAll("","Position ASC");
	?>
	<script type="text/javascript">
	function FunctionDeleteInfo()
	{
		result = confirm("Are you sure want to delete this Info?")
		if(result == true)
		{
			return true;
		}
		return false;
	}				
	
	</script>
			
			
			
	<table class="table table-striped table-bordered table-responsive block">
			<thead>
			<tr class="InsideLeftTd">
				<td align="center" width="50"><b>S.No.</b></td>
				<td align="left" width="100"><b>Tab Title</b></td>
				<td align="left" width="530"><b><b>Description</b></b></td>
				<td align="center" width="90"><b>Active</b></td>
				<td align="center" width="90"><b>Position</b></td>
				<td align="center" width="90">&nbsp;</td>
				<td align="center" width="90">&nbsp;</td>
			</tr>
			</thead>
			<tbody>
	<?php  
			$SNo=1;
			$Count=1;
			while($CurrentInfo = $ProductInfoObj->GetObjectFromRecord())
			{
				if($Section=="EditInfo" && $InfoID==$CurrentInfo->InfoID)
				{
					?>
					<form method="POST" action="<?php  echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php  echo $Page?>&Section=<?php  echo $Section?>&Pg=<?php  echo $Pg?>&ParentID=<?php  echo $ParentID?>&ProductID=<?php  echo $ProductID?>&InfoID=<?php  echo $InfoID?>&Target=EditInfo" enctype="multipart/form-data">
						<tr>
							<td align="center"><b><?php  echo $SNo?></b></td>
							<td align="left"><input type="text" name="TabTitle" value="<?php  echo MyStripSlashes($CurrentInfo->TabTitle);?>" class="txtBox"></td>
							<td align="left" >
									<div id="InfoTypeNewDivHTML">
									<?php  
									$sContent = isset($_POST['EditDescription'])?MyStripSlashes($_POST['EditDescription'],true):(isset($CurrentInfo->Description)?MyStripSlashes($CurrentInfo->Description,true):"");
									$SKEditorObj->CArray = array("Width"=>"400",
																 "Height"=>"250",
																 "DMode"=>"medium",
																 "Help"=>true,
																 "EditorArray"=>array("css"=>DIR_WS_SITE_CONTROL."style.css",
																 					  ),
																 ); 
									$SKEditorObj->CreateEditor("EditDescription",$sContent);?>
									</div>
								</td>
							<td align="center"><input type="checkbox" name="Active" value="1" <?php  echo $CurrentInfo->Active=="1"?"checked":""?> class="chk"></td>
							<td align="center"><input type="text" name="Position" value="<?php  echo MyStripSlashes($CurrentInfo->Position);?>" class="txtBox" style="text-align:center;"></td>
							<td align="center"><input type="submit" name="Submit" value="Update Info" class="btn"></td>
							<td align="center"><input type="button" name="button" value="Cancel" onclick="javascript:history.back();" class="btn btn-warning"></td>
						</tr>
					</form>
					
					<?php  		
				}
				else 
				{
					?>
						<tr class="InsideRightTd">
							<td align="center" ><b><?php  echo $SNo?></b></td>
							<td align="left"><?php  echo MyStripSlashes($CurrentInfo->TabTitle);?></td>
							<td align="left""><?php  SKDisplayPrdInfo($CurrentInfo);?></td>
							<td align="center" ><img src="<?php  echo DIR_WS_SITE_CONTROL_IMAGES.($CurrentInfo->Active=="1"?"info.gif":"error.gif")?>">
							</td>
							<td align="center"><?php  echo MyStripSlashes($CurrentInfo->Position);?>
							</td>
							<td align="center"><a href="<?php  echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php  echo $Page?>&Section=EditInfo&Pg=<?php  echo $Pg?>&ParentID=<?php  echo $ParentID?>&ProductID=<?php  echo $ProductID?>&InfoID=<?php  echo $CurrentInfo->InfoID?>"><b>Edit</b></a></td>
							<td align="center"><a href="<?php  echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php  echo $Page?>&Section=<?php  echo $Section?>&Pg=<?php  echo $Pg?>&ParentID=<?php  echo $ParentID?>&ProductID=<?php  echo $ProductID?>&Target=DeleteInfo&InfoID=<?php  echo $CurrentInfo->InfoID?>" onclick="return FunctionDeleteInfo();"><b>Delete</b></a></td>
						</tr>
					<?php  
				}
			$SNo++;
			$Count++;
			}
		?>
		<form method="POST" action="<?php  echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php  echo $Page?>&Section=<?php  echo $Section?>&Pg=<?php  echo $Pg?>&ParentID=<?php  echo $ParentID?>&ProductID=<?php  echo $ProductID?>&Target=AddInfo" enctype="multipart/form-data">
			<tr class="InsideRightTd">
				<td align="center"><b><?php  echo $SNo?></b></td>
				<td align="left" ><input type="text" name="TabTitle" class="txtBox"></td>
				<td align="left">
					<div id="InfoTypeNewDivHTML">
					<?php  
					$sContent = isset($_POST['AddDescription'])?MyStripSlashes($_POST['AddDescription'],true):"";
					$SKEditorObj->CArray = array("Width"=>"'100%'",
												 "Height"=>"250",
												 "DMode"=>"tiny2",
												 "Help"=>true,
												 "EditorArray"=>array("css"=>DIR_WS_SITE_CONTROL."style.css",
												 					  ),
												 ); 
					$SKEditorObj->CreateEditor("AddDescription",$sContent);?>
					</div>
				</td>
				<td align="center"><input type="checkbox" name="Active" value="1" class="chk"></td>
				<td align="center"><input type="text" name="Position" class="SortBox"></td>
				<td align="center"><input type="submit" name="Submit" value="Add Info" class="btn"></td>
				<td align="center">&nbsp;</td>
			</tr>
		</form>	
		</tbody>
		</table>		
		<script type="text/javascript">
		DisplayInfoContent('InfoTypeNew','HTML');
		</script>
	<?php  
	break;
	
case "Reviews":
case "EditReview":
	$ProductReviewObj->Where = "ProductID='".$ProductID."'";
	$ProductReviewObj->TableSelectAll("","CreatedDate ASC");
	?>
	<script type="text/javascript">
				function FunctionDeleteReview()
				{
					result = confirm("Are you sure want to delete the Review?")
					if(result == true)
					{
						return true;
					}
					return false;
				}				
							
			</script>
	<table border="0" cellpadding="3" cellspacing="1" width="100%" class="InsideTable">
			<tr class="InsideLeftTd">
				<td align="center" width="5%"><b>S.No.</b></td>
				<td align="left"><b>Review</b></td>
				<td align="center"><b>Active</b></td>
				<td align="center"></td>
				<td align="center"></td>
			</tr>
	<?php  
			$SNo=1;
			$Count=1;
			while($CurrentReview = $ProductReviewObj->GetObjectFromRecord())
			{
				if($Section=="EditReview" && $ReviewID==$CurrentReview->ReviewID)
				{
					?>
					<form method="POST" action="<?php  echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php  echo $Page?>&Section=<?php  echo $Section?>&Pg=<?php  echo $Pg?>&ParentID=<?php  echo $ParentID?>&ProductID=<?php  echo $ProductID?>&ReviewID=<?php  echo $ReviewID?>&Target=EditReview" enctype="multipart/form-data">
						<tr class="InsideRightTd">
							<td align="center"><b><?php  echo $SNo?></b></td>
							<td align="left">
							<textarea cols="55" rows="5" name="Description"><?php  echo MyStripSlashes($CurrentReview->Description);?></textarea></td>
							<td align="center"><input type="checkbox" name="Active" value="1" <?php  echo $CurrentReview->Active=="1"?"checked":""?> class="chk"></td>
							<td align="center" ><input type="submit" name="Submit" value="Update Review"></td>
							<td align="center" ><input type="button" name="button" value="Cancel" onclick="javascript:history.back();"></td>
						</tr>
					</form>
					<?php  		
				}
				else 
				{
					?>
					<tr class="InsideRightTd">
						<td align="center"><b><?php  echo $SNo?></b></td>
						<td>
							<div>
                         		<b>From: </b><?php  echo MyStripSlashes($CurrentReview->CName)?><br>
                         		<?php  echo MyStripSlashes(nl2br($CurrentReview->Description))?><br>
                         		<br>
                         		<b>Rating: <?php  echo SKStarRating($CurrentReview->Rating)?></b> [<?php  echo $CurrentReview->Rating?> of 5 Stars!]
                         	</div>
						</td>
						<td align="center"><img src="<?php  echo DIR_WS_SITE_CONTROL_IMAGES.($CurrentReview->Active=="1"?"info.gif":"error.gif")?>">
						</td>
						</td>
						<td align="center"><a href="<?php  echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php  echo $Page?>&Section=EditReview&Pg=<?php  echo $Pg?>&ParentID=<?php  echo $ParentID?>&ProductID=<?php  echo $ProductID?>&ReviewID=<?php  echo $CurrentReview->ReviewID?>"><b>Edit</b></a></td>
						<td align="center"><a href="<?php  echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php  echo $Page?>&Section=<?php  echo $Section?>&Pg=<?php  echo $Pg?>&ParentID=<?php  echo $ParentID?>&ProductID=<?php  echo $ProductID?>&Target=DeleteReview&ReviewID=<?php  echo $CurrentReview->ReviewID?>" onclick="return FunctionDeleteReview();"><b>Delete</b></a></td>
					</tr>
					<?php  
				}
			$SNo++;
			$Count++;
			}
		?>
		</table>		
			
	
	<?php  
	break;
	
	case "Images":
	case "EditImage":
	$ProductImageObj->Where = "ProductID='".$ProductID."'";
	$ProductImageObj->TableSelectAll("","ImageType='Image' DESC,ImageType ASC, Position ASC");
	?>
	<script type="text/javascript">
				function FunctionDeleteImage()
				{
					result = confirm("Are you sure want to delete the File?")
					if(result == true)
					{
						return true;
					}
					return false;
				}				
							
			</script>
		<table class="table table-striped table-bordered table-responsive block">
			<thead>
			<tr>
				<td align="center" width="5%"><b>S.No.</b></td>
				<td align="left"><b>File</b></td>
				<td align="left"><b>Alt/Title</b></td>
				<td align="left"><b>Type</b></td>
				<td align="center"><b>Active</b></td>
				<td align="center"><b>Position</b></td>
				<td align="center"></td>
				<td align="center"></td>
			</tr>
			</thead>
			<tbody>
	<?php  
			$SNo=1;
			$Count=1;
			while($CurrentImage = $ProductImageObj->GetObjectFromRecord())
			{
				if($Section=="EditImage" && $ImageID==$CurrentImage->ImageID)
				{
					?>
					<form method="POST" action="<?php  echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php  echo $Page?>&Section=<?php  echo $Section?>&Pg=<?php  echo $Pg?>&ParentID=<?php  echo $ParentID?>&ProductID=<?php  echo $ProductID?>&ImageID=<?php  echo $ImageID?>&Target=EditImage" enctype="multipart/form-data">
						<tr class="InsideRightTd">
							<td align="center"><b><?php  echo $SNo?></b></td>
							<td align="left">
							<?php  SKDisplayPrdImage($CurrentImage)?>
							<br><input type="file" name="Upload" class="txtBox">
							</td>
							<td align="left"><input type="text" name="ImageName" value="<?php  echo MyStripSlashes($CurrentImage->ImageName);?>" size="30" class="txtBox" ></td>
							<td>
							<select name="ImageType" class="txtBox">
								<?php  
								foreach ($ProductFileTypeArray as $k=>$v)
								{
								?>
									<option value="<?php  echo $k?>" <?php  echo $CurrentImage->ImageType==$k?"selected":""?>><?php  echo $v?></option>
									<?php  
								}?>
								</select>
							</td>
							<td align="center"><input type="checkbox" class="txtBox" name="Active" value="1" <?php  echo $CurrentImage->Active=="1"?"checked":""?> class="chk"></td>
							<td align="center"><input type="text" class="SortBox" name="Position" value="<?php  echo MyStripSlashes($CurrentImage->Position);?>" size="5"></td>
							<td align="center" ><input type="submit"  name="Submit" value="Update"></td>
							<td align="center" ><input type="button" name="button" value="Cancel" onclick="javascript:history.back();"></td>
						</tr>
						<tr><td colspan="8"><hr></td></tr>
					</form>
					<?php  		
				}
				else 
				{
					?>
					<tr class="InsideRightTd">
						<td align="center"><b><?php  echo $SNo?></b></td>
						<td align="left">
						<?php  SKDisplayPrdImage($CurrentImage)?>
						</TD>
						<td align="left"><?php  echo MyStripSlashes($CurrentImage->ImageName);?></td>
						<td><?php  echo isset($ProductFileTypeArray[$CurrentImage->ImageType])?$ProductFileTypeArray[$CurrentImage->ImageType]:""?></td>
						<td align="center"><img src="<?php  echo DIR_WS_SITE_CONTROL_IMAGES.($CurrentImage->Active=="1"?"info.gif":"error.gif")?>">
						</td>
						<td align="center"><?php  echo MyStripSlashes($CurrentImage->Position);?>
						</td>
						<td align="center"><a href="<?php  echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php  echo $Page?>&Section=EditImage&Pg=<?php  echo $Pg?>&ParentID=<?php  echo $ParentID?>&ProductID=<?php  echo $ProductID?>&ImageID=<?php  echo $CurrentImage->ImageID?>"><b>Edit</b></a></td>
						<td align="center"><a href="<?php  echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php  echo $Page?>&Section=<?php  echo $Section?>&Pg=<?php  echo $Pg?>&ParentID=<?php  echo $ParentID?>&ProductID=<?php  echo $ProductID?>&Target=DeleteImage&ImageID=<?php  echo $CurrentImage->ImageID?>" onclick="return FunctionDeleteImage();"><b>Delete</b></a></td>
					</tr>
					<tr><td colspan="8"><hr></td></tr>
					<?php  
				}
			$SNo++;
			$Count++;
			}
		?>
		<form method="POST" action="<?php  echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php  echo $Page?>&Section=<?php  echo $Section?>&Pg=<?php  echo $Pg?>&ParentID=<?php  echo $ParentID?>&ProductID=<?php  echo $ProductID?>&Target=AddImage" enctype="multipart/form-data">
			<tr class="InsideRightTd">
				<td align="center"><b><?php  echo $SNo?></b></td>
				<td align="left"><input type="file" name="Upload"  class="txtBox"></td>
				<td align="left"><input type="text" name="ImageName" value="" class="txtBox"></td>
				<td>
					<select name="ImageType" class="txtBox">
					<?php  
					foreach ($ProductFileTypeArray as $k=>$v)
					{
					?>
						<option value="<?php  echo $k?>"><?php  echo $v?></option>
						<?php  
					}?>
					</select>
				</td>
				<td align="center"><input type="checkbox" name="Active" value="1" class="chk"></td>
				<td align="center"><input type="text" name="Position" size="5" class="SortBox"></td>
				<td align="center" colspan="2"><input type="submit" name="Submit" value="Add" class="btn"></td>
			</tr>
		</form>
		<tbody>
		</table>		
			
	
	<?php  
	break;
	
	case "Attributes":

			?>

			<form method="POST" action="<?php  echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php  echo $Page?>&Section=<?php  echo $Section?>&Target=UpdateAttribute&ParentID=<?php  echo $ParentID?>&ProductID=<?php  echo $ProductID?>">

			<table border="0" cellpadding="3" cellspacing="1" width="100%" class="InsideTable">

				<tr>

					<td align="left"><h3><?php  echo MyStripSlashes($CurrentProduct->ProductName)?>'s Attributes</h3></td>

				</tr>

				<tr>

					<td>

						<div id="accordion">

							<?php  

							//$AttributeObj->Where="AttributeType in('product','both') AND AttributeStatus='custom'";

							$AttributeObj->TableSelectAll("","AttributeStatus DESC, Position ASC");

							while ($CurrentAttribute = $AttributeObj->GetObjectFromRecord())

							{

								$AttributeValueObj->Where = "AttributeID='".$CurrentAttribute->AttributeID."'";

								$AttributeValueObj->TableSelectAll("","Position ASC");

								

								$ProductRelationObj->Where ="RelationType='attribute_value' AND ProductID='".$ProductObj->MysqlEscapeString($ProductID)."'";

								$CurrentProductRelation = $ProductRelationObj->TableSelectOne(array("GROUP_CONCAT(DISTINCT RelationID ORDER BY RelationID DESC SEPARATOR ',') as AttributeValues"));	

								$AttributeValueArray = isset($CurrentProductRelation->AttributeValues)?explode(",",$CurrentProductRelation->AttributeValues):array();

	

							?>

							    <h3 style="padding-left:20px;"><a href="#"><b><?php  echo MyStripSlashes($CurrentAttribute->AttributeName)?></b></a></h3>
							    <div>
							    <?php   while($CurrentAttributeValue = $AttributeValueObj->GetObjectFromRecord())
								{?>
									<input <?php  echo (is_array($AttributeValueArray) && in_array($CurrentAttributeValue->AttributeValueID,$AttributeValueArray))?"checked":""?> type="checkbox" value="<?php  echo $CurrentAttributeValue->AttributeValueID?>" name="AttributeValue[]" id="AttributeValue_<?php  echo $CurrentAttributeValue->AttributeValueID?>"><label for="AttributeValue_<?php  echo $CurrentAttributeValue->AttributeValueID?>"><?php  echo MyStripSlashes($CurrentAttributeValue->AttributeValue);?></label>
								<?php  }?>
							    </div>
						    <?php  
							}?>
						</div>
					</td>
				</tr>
				<tr>
					<td align="center"><input type="submit" value="Submit"></td>

				</tr>

				

			</table>

			</form>

			<script type="text/javascript">

				jQuery(document).ready(function(){

					$("#accordion").accordion({

						autoHeight:false,

						clearStyle: true 

					});

				});

			</script>

			<?php  

		break;
		case "Addition":
		$ProductAdditionObj->Where = "ProductID='".$ProductAdditionObj->MysqlEscapeString($CurrentProduct->ProductID)."'";
		$ProductAdditionObj->TableSelectAll(array(" DISTINCT(AttributeName) as AttributeName"),"AttributeName ASC");
		$TotalAttribute = $ProductAdditionObj->GetNumRows();
		?>
		<form method="POST" action="<?php  echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php  echo $Page?>&Section=<?php  echo $Section?>&Pg=<?php  echo $Pg?>&Target=UpdateAdditional&ParentID=<?php  echo $ParentID?>&ProductID=<?php  echo $ProductID?>">
		<table border="0" cellpadding="3" cellspacing="1" width="100%" class="table table-striped table-bordered table-responsive">
		<tr>
			<td align="right" class="pull-right">
				<?php    
				if($BackUpID !="")
				{
					$BackupObj->Where = "BackUpID ='$BackUpID'";
					$CurrentBackup = $BackupObj->TableSelectOne();
					$_POST = unserialize(base64_decode($CurrentBackup->Dumps));
					
					if(isset($_POST['AttributeName']) &&is_array($_POST['AttributeName']))
						$TotalAttribute = count($_POST['AttributeName']);
				
				}
				
				$BackupObj->Where = "ReferenceID='-1' AND Page='product_additions'";
				$BackupObj->TableSelectAll(array('BackUpID','ReferenceTitle','DATE_FORMAT(CreatedDate,"%M %d, %Y")as MyCreatedDate','CreatedDate'),' CreatedDate DESC');
				if($BackupObj->GetNumRows()):
				?>
				<select name="BackUpID" onchange="document.location.href='<?php  echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php  echo $Page?>&Section=<?php  echo $Section?>&Pg=<?php  echo $Pg?>&ParentID=<?php  echo $ParentID?>&ProductID=<?php  echo $ProductID?>&BackUpID='+ this.value">
					<option value="">Select Saved Attributes</option>
					<?php  
					while ($OldBackUp =$BackupObj->GetObjectFromRecord())
					{?>
					<option value="<?php  echo $OldBackUp->BackUpID?>" <?php  echo $OldBackUp->BackUpID==$BackUpID?"selected":""?>><?php  echo $OldBackUp->ReferenceTitle?> (<?php  echo $OldBackUp->MyCreatedDate?>)</option>
					<?php  
					}?>
				</select>
				<?php    endif;?>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<a href="javascript:;" onclick="return AddNewPrdAddition()">New Attribute</a>
			</td>
		</tr>
		<tr>
			<td>
				<div align="center" id="MainAdditionDiv">
					<?php  
					$SNo =1;
					$Count =1;
					if($BackUpID !="")
					{
						foreach ($_POST['AttributeName'] as $k=>$v)
						{
							?>
							<div id="DivPrdAtt_<?php  echo $SNo?>" style="padding:20px;">
								<table border="2" cellpadding="3" cellspacing="1" width="100%" class="table table-striped table-bordered table-responsive">
									<tr class="InsideLeftTd">
										<td><b>Title</b>: <input type="text" name="AttributeName[<?php  echo $SNo?>]" value="<?php  echo isset($_POST['AttributeName'][$k])?$_POST['AttributeName'][$k]:""?>">&nbsp;&nbsp;
										<a href="javascript:;" onclick="return DeletePrdAddition('DivPrdAtt_<?php  echo $SNo?>');">Delete Attribute</a>
										&nbsp;&nbsp;|&nbsp;&nbsp;
										<a href="javascript:;" onclick="return AddNewPrdAddValue(<?php  echo $SNo?>);">Add New Value</a>
										</td>
									</tr>
									<tr>
										<td id="TdPrdAtt_<?php  echo $SNo?>">
											<?php  
											foreach ($_POST['AttributeValue'][$k] as $kkk=>$vvv)
											{
											?>
											<div id="DivPrdAttVal_<?php  echo $SNo?>_<?php  echo $Count?>">
												<table border="0" cellpadding="3" cellspacing="1" width="100%">
													<tr>
														<td width="240" align="left">Value: <input type="text" name="AttributeValue[<?php  echo $SNo?>][<?php  echo $Count?>]" value="<?php  echo isset($_POST['AttributeValue'][$k][$kkk])?$_POST['AttributeValue'][$k][$kkk]:""?>" size="30"></td>
														<td width="160" align="left">SKU/Model No: <input type="text" name="ModelNo[<?php  echo $SNo?>][<?php  echo $Count?>]" value="<?php  echo (isset($_POST['ModelNo'][$k][$kkk]) && $_POST['ModelNo'][$k][$kkk] !="")?$_POST['ModelNo'][$k][$kkk]:""?>" size="10"></td>
														<td width="140" align="left">RRP Price: <input type="text" name="NormalPrice[<?php  echo $SNo?>][<?php  echo $Count?>]" value="<?php  echo (isset($_POST['NormalPrice'][$k][$kkk]) && $_POST['NormalPrice'][$k][$kkk] !="")?$_POST['NormalPrice'][$k][$kkk]:""?>" size="10"></td>
														<td width="120" align="left">Price: <input type="text" name="Price[<?php  echo $SNo?>][<?php  echo $Count?>]" value="<?php  echo (isset($_POST['Price'][$k][$kkk]) && $_POST['Price'][$k][$kkk] !="")?$_POST['Price'][$k][$kkk]:""?>" size="10"></td>
														<td width="50" align="left">
														<select name="PriceSet[<?php  echo $SNo?>][<?php  echo $Count?>]">
															<option value="+" <?php  echo (isset($_POST['PriceSet'][$k][$kkk]) && $_POST['PriceSet'][$k][$kkk] =="+")?"selected":""?>>+</option>
															<option value="-" <?php  echo (isset($_POST['PriceSet'][$k][$kkk]) && $_POST['PriceSet'][$k][$kkk] =="-")?"selected":""?>>-</option>
														</select></td>
														<td align="left"><a href="javascript:;" onclick="return DeletePrdAddValue('DivPrdAttVal_<?php  echo $SNo?>_<?php  echo $Count?>')">Delete Value</a></td>
													</tr>
												</table>	
											</div>
											<?php  
											$Count++;
											}?>
										</td>
									</tr>
								</table>
							</div>
							<?php  
							$SNo++;
						}
					}
					else 
					{
						while ($CurrentAddition = $ProductAdditionObj->GetObjectFromRecord())
						{
						?>
						<div id="DivPrdAtt_<?php  echo $SNo?>" style="padding:20px;">
							<table border="2" cellpadding="3" cellspacing="1" width="100%" class="table table-striped table-bordered table-responsive">
								<tr class="InsideLeftTd">
									<td><b>Title</b>: <input type="text" name="AttributeName[<?php  echo $SNo?>]" value="<?php  echo isset($CurrentAddition->AttributeName)?MyStripSlashes($CurrentAddition->AttributeName):""?>">&nbsp;&nbsp;
									<a href="javascript:;" onclick="return DeletePrdAddition('DivPrdAtt_<?php  echo $SNo?>');">Delete Attribute</a>
									&nbsp;&nbsp;|&nbsp;&nbsp;
									<a href="javascript:;" onclick="return AddNewPrdAddValue(<?php  echo $SNo?>);">Add New Value</a>
									</td>
								</tr>
								<tr>
									<td id="TdPrdAtt_<?php  echo $SNo?>">
										<?php  
										$ProductAdditionObj2->Where = $ProductAdditionObj->Where. " AND AttributeName='".$CurrentAddition->AttributeName."'";
										$ProductAdditionObj2->TableSelectAll("","ProductAdditionID ASC");
										while ($CurrentAdditionVal = $ProductAdditionObj2->GetObjectFromRecord())
										{
										?>
										<div id="DivPrdAttVal_<?php  echo $SNo?>_<?php  echo $Count?>">
											<table border="0" cellpadding="3" cellspacing="1" width="100%" class="">
												<tr>
													<td width="240" align="left">Value: <input type="text" name="AttributeValue[<?php  echo $SNo?>][<?php  echo $Count?>]" value="<?php  echo isset($CurrentAdditionVal->AttributeValue)?MyStripSlashes($CurrentAdditionVal->AttributeValue):""?>" size="30"></td>
													<td width="160" align="left">SKU/Model No: <input type="text" name="ModelNo[<?php  echo $SNo?>][<?php  echo $Count?>]" value="<?php  echo isset($CurrentAdditionVal->ModelNo)?$CurrentAdditionVal->ModelNo:""?>" size="10"></td>
													<td width="140" align="left">RRP Price: <input type="text" name="NormalPrice[<?php  echo $SNo?>][<?php  echo $Count?>]" value="<?php  echo isset($CurrentAdditionVal->NormalPrice)?$CurrentAdditionVal->NormalPrice:""?>" size="10"></td>
													<td width="120" align="left">Price: <input type="text" name="Price[<?php  echo $SNo?>][<?php  echo $Count?>]" value="<?php  echo isset($CurrentAdditionVal->Price)?$CurrentAdditionVal->Price:""?>" size="10"></td>
													<td width="50" align="left">
													<select name="PriceSet[<?php  echo $SNo?>][<?php  echo $Count?>]">
														<option value="+" <?php  echo (isset($CurrentAdditionVal->PriceSet) && $CurrentAdditionVal->PriceSet=="+")?"selected":""?>>+</option>
														<option value="-" <?php  echo (isset($CurrentAdditionVal->PriceSet) && $CurrentAdditionVal->PriceSet=="-")?"selected":""?>>-</option>
													</select></td>
													<td align="left"><a href="javascript:;" onclick="return DeletePrdAddValue('DivPrdAttVal_<?php  echo $SNo?>_<?php  echo $Count?>')">Delete Value</a></td>
												</tr>
											</table>	
										</div>
										<?php  
										$Count++;
										}?>
									</td>
								</tr>
							</table>
						</div>
						<?php  
						$SNo++;
						}
					}
					?>
				</div>
			</td>
		</tr>
		<tr>
			<td align="center">
			<b>Title:</b> <input type="text" name="SaveText" value="">
			<br>
			<label><input type="checkbox" name="SaveThis" value="1" class="chk"><b>Save This?</b> <br><small>For future reference</small></label>
			</td>
		</tr>
		<tr>
			<td align="center">
				<div class="text-center"><input type="submit" name="Submit" value="Submit" class="btn btn-primary"></div>
			</td>
		</tr>
		</table>
		</form>
		<script type="text/javascript" src="<?php  echo DIR_WS_SITE_CONTROL_INCLUDES?>product_additions.js"></script>
		<script type="text/javascript">
		var AddCt = <?php  echo $Count?>;
		</script>
		
		<?php  
	break;
	case "EditTabBulk":
		?>
		<table border="0" cellpadding="0" cellspacing="0" width="100%">
			<tr>
				<td height="20" align="center">
				<h4>Products Edit Section</h4>
			</td>
			</tr>
		</table>
		<fieldset style="text-align:left;">
			<legend style="margin-left:10px;padding:0px 5px;">Search Products</legend>
			<form id="FrmFilter" method="GET" action="index.php">
				<input type="hidden" name="Page" value="<?php  echo $Page?>" >
				<input type="hidden" name="Section" value="EditTabBulk" >
				<table border="0" cellpadding="5" cellspacing="1" width="100%" class="InsideTable">
					<tr>
						<td width="16%" class="InsideLeftTd" align="right"><b>Select Category</b></td>
						<td width="16%" class="InsideRightTd" align="left">
							<select name="r1" id="r1">
								<!--<option value="">--Categories--</option>-->
								<?php  echo CategoryChain(0,0,array(@$_REQUEST['r1']),false,0,0,1);?>
							  </select>
						</td>
						<td width="16%" class="InsideLeftTd" align="right"><b>Manufacturers</b></td>
						<td width="16%" class="InsideRightTd" align="left">
							<select name="r2" id="r2">
								<option value="">--Manufacturers--</option>
								<?php  
								$ManufacturerObj->Where = "1";
								$ManufacturerObj->TableSelectAll(array("MNID","MNName"),"MNName ASC");
								while($CurrentManufacturer = $ManufacturerObj->GetObjectFromRecord())
								{
								?>
								<option value="<?php  echo $CurrentManufacturer->MNID?>" <?php  echo @$_REQUEST['r2']==$CurrentManufacturer->MNID?"selected":""?>><?php  echo $CurrentManufacturer->MNName?></option><?php  
								}?>
								
						  	</select>
						</td>
						<td width="18%" class="InsideLeftTd" align="right"><b>keyword/name/model</b></td>
						<td width="18%" class="InsideRightTd" align="left"><input size="18" type="text" name="r3" value="<?php  echo @$_REQUEST['r3']?>"></td>
					</tr>
					<tr>
						<td class="InsideLeftTd" align="right" valign="top"><b>Active Status</b></td>
						<td class="InsideRightTd">
							<select name="r4" id="r4">
								<option value="">All</option>
								<option value="Active" <?php  echo @$_REQUEST['r4'] =="Active" ?"selected":""?>>Active Only</option>
								<option value="Inactive" <?php  echo @$_REQUEST['r4'] == "Inactive" ?"selected":""?>>Inactive Only</option>
							</select>
						</td>
						<td class="InsideLeftTd" align="right" valign="top"><b>Sort Order</b></td>
						<td class="InsideRightTd" align="left">
							<select name="r5" id="r5">
								<!--<option value="">--Order By--</option>-->
								<?php  
								$OrderByArray = array(
								"p.QuickBooksCode ASC"=>"QuickBooksCode ASC",
								"p.QuickBooksCode DESC"=>"QuickBooksCode DESC",
								"p.GoogleMPN ASC"=>"GoogleMPN ASC",
								"p.GoogleMPN DESC"=>"GoogleMPN DESC",
								"ProductName ASC"=>"Product Name ASC",
								"ProductName DESC"=>"Product Name DESC",
								"p.ModelNo ASC"=>"SKU ASC",
								"p.ModelNo DESC"=>"SKU DESC",
								"p.CreatedDate ASC"=>"Created Date ASC",
								"p.CreatedDate DESC"=>"Created Date DESC",
								);
								foreach ($OrderByArray as $k=>$v)
								{
								?>
								<option value="<?php  echo $k?>" <?php  echo @$_REQUEST['r5'] ==$k?"selected":""?>><?php  echo $v?></option>
								<?php  
								}?>
							</select>
						</td>
						<td class="InsideLeftTd" align="right"></td>
						<td class="InsideRightTd" align="left"></td>					
					</tr>
				</table>
				<br/>
				<center><input type="submit" name="Submit" value="Search Products"></center>
			</form>
		</fieldset>
		<?php  		
		if(isset($_REQUEST['Submit'])):
			$WherePart ="";
			if($_REQUEST['r1']!=""):
				$CatArr = implode(GetCategoryTreeArray($ProductObj->MysqlEscapeString($_REQUEST['r1']),array()),",");
				if(!empty($CatArr))
					$WherePart.=" and ptc.RelationID IN (".$CatArr.")";
			endif;
			
			if($_REQUEST['r2']!="")
				$WherePart.=" and p.mnid='".$ProductObj->MysqlEscapeString($_REQUEST['r2'])."'";
			
			if($_REQUEST['r3']!="")
			{				
				$WherePart .=" AND (";
				if(preg_match("/\+\b/i",$_REQUEST['r3']))
				{
					$Prcident= "AND";
					$SecGate = "1";
					$KeywordArr =  explode("+",$_REQUEST['r3']);
				}
				else 
				{
					$Prcident= "AND";
					$SecGate = "1";
					$KeywordArr =  explode(" ",$_REQUEST['r3']);
				}
				foreach ($KeywordArr as $Key=>$Value)
				{
					if(trim($Value) !="")
						$WherePart .="(ProductName LIKE '%$Value%' OR GoogleMPN LIKE '%$Value%' OR QuickBooksCode LIKE '%$Value%' OR ProductLine LIKE '%$Value%' OR LargeDescription LIKE '%$Value%' OR ModelNo LIKE '%$Value%') $Prcident ";

				}			
				$WherePart .=" $SecGate)";				
			}
			
			if($_REQUEST['r4']=="Active")
				$WherePart .=" AND p.Active=1";
			else if($_REQUEST['r4']=="Inactive")
				$WherePart .=" AND p.Active=0";
				
				
			$ProductObj = new DataTable(TABLE_PRODUCT. " p, ".TABLE_PRODUCT_RELATION. " ptc,".TABLE_MANUFACTURERS." m");
			$ProductObj->Where = "p.MNID = m.MNID AND ptc.ProductID=p.ProductID and ptc.RelationType='category'".$WherePart." GROUP BY p.ProductID";
			$ProductObj->TableSelectAll("",$_REQUEST['r5']);
		?>
			<br/><br/>
			<h2>Search Results</h2>			
			<table border="0" cellpadding="5" cellspacing="1" width="100%" class="InsideTable" style="margin-top:10px;border-bottom:none;">
				<tr class="InsideLeftTd">
					<td align="center" width="25"><b>Sr.</b></td>
					<td align="left"><b>Product Name</b></td>
					<td align="left" width="100"><b>Model No</b></td>
					<td align="left"  width="100"><b>Tabs Info</b></td>
					<td align="center" width="100">
						<a href="#" class="selectall">Select All</a>
						<hr>
						<a href=""  class="unselectall">Unselect All</a>
						</td>
					<td width="6">&nbsp;</td>
				</tr>
			</table>
			<script type="text/javascript">
			jQuery(document).ready(function($){
				$(".selectall").click(function(){
					$('#UpdateInfoContainer input:checkbox').attr({checked:true});
					return false;
				});
				$(".unselectall").click(function(){
					$('#UpdateInfoContainer input:checkbox').attr({checked:false});
					return false;
				});
				
				
			});
			</script>
			<?php  if($ProductObj->GetNumRows() > 0):?>
				<?php  $Sr=1?>
				<?php  
					$height = intval($ProductObj->GetNumRows()*30.25);
					$height = $height>301 ? 301:$height;
				?>
				<form method="POST" action="<?php  echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php  echo $Page?>&Section=<?php  echo $Section?>&r1=<?php  echo $_REQUEST['r1']?>&r2=<?php  echo $_REQUEST['r2']?>&r3=<?php  echo $_REQUEST['r3']?>&r4=<?php  echo $_REQUEST['r4']?>&r5=<?php  echo $_REQUEST['r5']?>&Submit=<?php  echo $_REQUEST['Submit']?>&Target=UpdateTabBulk">
					<div style="text-align:left;height:<?php  echo $height?>px;border:1px solid #C7DCF7;overflow-y:scroll;" id="UpdateInfoContainer">
						<table border="0" cellpadding="5" cellspacing="1" width="100%" class="InsideTable" style="border:none;">
							<?php  while($CurrentProduct = $ProductObj->GetObjectFromRecord()):?>
								<tr class="InsideRightTd">
									<td align="center" width="25"><?php  echo $Sr?></td>
									<td align="left">
									<?php  echo MyStripSlashes($CurrentProduct->ProductName);?> (<?php  echo MyStripSlashes($CurrentProduct->ModelNo);?>)
									<br>
									<a href="<?php  echo SKSEOURL($CurrentProduct->ProductID,"shop/product")?>" target="_blank">Preview</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="<?php  echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php  echo $Page?>&Section=EditProduct&Pg=<?php  echo $Pg?>&ParentID=<?php  echo $ParentID?>&ProductID=<?php  echo $CurrentProduct->ProductID?>">Edit</a>
									</td>
									<td align="left" width="100"><?php  echo MyStripSlashes($CurrentProduct->ModelNo);?></td>
									<td align="left"  width="100">
										<a rel="colorbox_ajax" href="<?php  echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php  echo $Page?>&Section=InfoPopUp&Meta=false&Popup=true&ProductID=<?php  echo $CurrentProduct->ProductID?>">Tabs Info</a>
									</td>
									<td align="center" width="100">
										<input type="checkbox" name="ChkProductID[]" value="<?php  echo $CurrentProduct->ProductID?>" />
									</td>
								</tr>								
								<input type="hidden" name="ProductID_<?php  echo $Sr?>" value="<?php  echo $CurrentProduct->ProductID?>" />
								<?php  $Sr+=1?>
							<?php  endwhile;?>					
						</table>
					</div>
					<div style="text-align:left;border:1px solid #C7DCF7;" id="accordion">
						<?php  
						$SNo = 1;
						$ProductInfoObj = new DataTable(TABLE_PRODUCT_INFO);
						$ProductInfoObj->Where ="TabTitle !=''";
						$ProductInfoObj->TableSelectAll(array(" DISTINCT(TabTitle) as TabTitle"),"TabTitle ASC");
						while($CurrentInfo = $ProductInfoObj->GetObjectFromRecord()):?>
						<h3 style="padding-left:20px;"><a href="#"><b><?php  echo MyStripSlashes($CurrentInfo->TabTitle);?></b></a></h3>
						<div>
						<?php  
									$SKEditorObj->CArray = array("Width"=>"500",
																 "Height"=>"200",
																 "DMode"=>"tiny",
																 "Help"=>false,
																 "EditorArray"=>array("css"=>DIR_WS_SITE_CONTROL."style.css",
																 					  ),
																 ); 
									$SKEditorObj->CreateEditor("EditDescription_".$SNo);?>
						<label style="color:#880000;"><input type="checkbox" name="ChkTab_<?php  echo $SNo?>" value="<?php  echo $CurrentInfo->TabTitle?>"> Please check this box to activate this section</label>
						
						</div>			
						<?php    $SNo++;endwhile;?>
					</div>
					<input type="hidden" name="Count" value="<?php  echo $SNo?>" />
					<div style="text-align:center;padding:10px;"><input type="submit" name="Submit" value="Update" /></div>
				</form>
				<script type="text/javascript">
				jQuery(document).ready(function(){
					$("#accordion").accordion({
						autoHeight:false,
						clearStyle: true 
					});
				});
			</script>
			<?php  else:?>
				<table border="0" cellpadding="5" cellspacing="1" width="100%" class="InsideTable">
					<tr class="InsideRightTd">
						<td>No records found.</td>
					</tr>
				</table>
			<?php  endif;?>
		<?php  	
		endif;		
		break;
	/////////Display Product 
	
	case "EditShippingBulk":
		?>
		<table border="0" cellpadding="0" cellspacing="0" width="100%">
			<tr>
				<td height="20" align="center">
				<h4>Products Shipping (Bulk) Section</h4>
			</td>
			</tr>
		</table>
		<fieldset style="text-align:left;">
			<legend style="margin-left:10px;padding:0px 5px;">Search Products</legend>
			<form id="FrmFilter" method="GET" action="index.php">
				<input type="hidden" name="Page" value="<?php  echo $Page?>" >
				<input type="hidden" name="Section" value="EditShippingBulk" >
				<table border="0" cellpadding="5" cellspacing="1" width="100%" class="InsideTable">
					<tr>
						<td width="16%" class="InsideLeftTd" align="right"><b>Select Category</b></td>
						<td width="16%" class="InsideRightTd" align="left">
							<select name="r1" id="r1">
								<!--<option value="">--Categories--</option>-->
								<?php  echo CategoryChain(0,0,array(@$_REQUEST['r1']),false,0,0,1);?>
							  </select>
						</td>
						<td width="16%" class="InsideLeftTd" align="right"><b>Manufacturers</b></td>
						<td width="16%" class="InsideRightTd" align="left">
							<select name="r2" id="r2">
								<option value="">--Manufacturers--</option>
								<?php  
								$ManufacturerObj->Where = "1";
								$ManufacturerObj->TableSelectAll(array("MNID","MNName"),"MNName ASC");
								while($CurrentManufacturer = $ManufacturerObj->GetObjectFromRecord())
								{
								?>
								<option value="<?php  echo $CurrentManufacturer->MNID?>" <?php  echo @$_REQUEST['r2']==$CurrentManufacturer->MNID?"selected":""?>><?php  echo $CurrentManufacturer->MNName?></option><?php  
								}?>
								
						  	</select>
						</td>
						<td width="18%" class="InsideLeftTd" align="right"><b>keyword/name/model</b></td>
						<td width="18%" class="InsideRightTd" align="left"><input size="18" type="text" name="r3" value="<?php  echo @$_REQUEST['r3']?>"></td>
					</tr>
					<tr>
						<td class="InsideLeftTd" align="right" valign="top"><b>Active Status</b></td>
						<td class="InsideRightTd">
							<select name="r4" id="r4">
								<option value="">All</option>
								<option value="Active" <?php  echo @$_REQUEST['r4'] =="Active" ?"selected":""?>>Active Only</option>
								<option value="Inactive" <?php  echo @$_REQUEST['r4'] == "Inactive" ?"selected":""?>>Inactive Only</option>
							</select>
						</td>
						<td class="InsideLeftTd" align="right" valign="top"><b>Sort Order</b></td>
						<td class="InsideRightTd" align="left">
							<select name="r5" id="r5">
								<!--<option value="">--Order By--</option>-->
								<?php  
								$OrderByArray = array(
								"p.QuickBooksCode ASC"=>"QuickBooksCode ASC",
								"p.QuickBooksCode DESC"=>"QuickBooksCode DESC",
								"p.GoogleMPN ASC"=>"GoogleMPN ASC",
								"p.GoogleMPN DESC"=>"GoogleMPN DESC",
								"ProductName ASC"=>"Product Name ASC",
								"ProductName DESC"=>"Product Name DESC",
								"p.ModelNo ASC"=>"SKU ASC",
								"p.ModelNo DESC"=>"SKU DESC",
								"p.CreatedDate ASC"=>"Created Date ASC",
								"p.CreatedDate DESC"=>"Created Date DESC",
								);
								foreach ($OrderByArray as $k=>$v)
								{
								?>
								<option value="<?php  echo $k?>" <?php  echo @$_REQUEST['r5'] ==$k?"selected":""?>><?php  echo $v?></option>
								<?php  
								}?>
							</select>
						</td>
						<td class="InsideLeftTd" align="right"></td>
						<td class="InsideRightTd" align="left"></td>					
					</tr>
				</table>
				<br/>
				<center><input type="submit" name="Submit" value="Search Products"></center>
			</form>
		</fieldset>
		<?php  		
		if(isset($_REQUEST['Submit'])):
			$WherePart ="";
			if($_REQUEST['r1']!=""):
				$CatArr = implode(GetCategoryTreeArray($ProductObj->MysqlEscapeString($_REQUEST['r1']),array()),",");
				if(!empty($CatArr))
					$WherePart.=" and ptc.RelationID IN (".$CatArr.")";
			endif;
			
			if($_REQUEST['r2']!="")
				$WherePart.=" and p.mnid='".$ProductObj->MysqlEscapeString($_REQUEST['r2'])."'";
			
			if($_REQUEST['r3']!="")
			{				
				$WherePart .=" AND (";
				if(preg_match("/\+\b/i",$_REQUEST['r3']))
				{
					$Prcident= "AND";
					$SecGate = "1";
					$KeywordArr =  explode("+",$_REQUEST['r3']);
				}
				else 
				{
					$Prcident= "AND";
					$SecGate = "1";
					$KeywordArr =  explode(" ",$_REQUEST['r3']);
				}
				foreach ($KeywordArr as $Key=>$Value)
				{
					if(trim($Value) !="")
						$WherePart .="(ProductName LIKE '%$Value%' OR GoogleMPN LIKE '%$Value%' OR QuickBooksCode LIKE '%$Value%' OR ProductLine LIKE '%$Value%' OR LargeDescription LIKE '%$Value%' OR ModelNo LIKE '%$Value%') $Prcident ";
				}			
				$WherePart .=" $SecGate)";				
			}
			
			if($_REQUEST['r4']=="Active")
				$WherePart .=" AND p.Active=1";
			else if($_REQUEST['r4']=="Inactive")
				$WherePart .=" AND p.Active=0";
				
				
			$ProductObj = new DataTable(TABLE_PRODUCT. " p, ".TABLE_PRODUCT_RELATION. " ptc,".TABLE_MANUFACTURERS." m");
			$ProductObj->Where = "p.MNID = m.MNID AND ptc.ProductID=p.ProductID and ptc.RelationType='category'".$WherePart." GROUP BY p.ProductID ";
			$ProductObj->TableSelectAll("",$_REQUEST['r5']);
		?>
			<br/><br/>
			<h2>Search Results</h2>			
			<table border="0" cellpadding="5" cellspacing="1" width="100%" class="InsideTable" style="margin-top:10px;border-bottom:none;">
				<tr class="InsideLeftTd">
					<td align="center" width="25"><b>Sr.</b></td>
					<td align="left"><b>Product Name</b></td>
					<td align="left" width="100"><b>Model No</b></td>
					<td align="left"  width="100"><b>Shipping Info</b></td>
					<td align="center" width="100">
						<a href="#" class="selectall">Select All</a>
						<hr>
						<a href=""  class="unselectall">Unselect All</a>
						</td>
					<td width="6">&nbsp;</td>
				</tr>
			</table>
			<script type="text/javascript">
			jQuery(document).ready(function($){
				$(".selectall").click(function(){
					$('#UpdateInfoContainer input:checkbox').attr({checked:true});
					return false;
				});
				$(".unselectall").click(function(){
					$('#UpdateInfoContainer input:checkbox').attr({checked:false});
					return false;
				});
				
				
			});
			</script>
			<?php  if($ProductObj->GetNumRows() > 0):?>
				<?php  $Sr=1?>
				<?php  
					$height = intval($ProductObj->GetNumRows()*30.25);
					$height = $height>301 ? 301:$height;
				?>
				<form method="POST" action="<?php  echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php  echo $Page?>&Section=<?php  echo $Section?>&r1=<?php  echo $_REQUEST['r1']?>&r2=<?php  echo $_REQUEST['r2']?>&r3=<?php  echo $_REQUEST['r3']?>&r4=<?php  echo $_REQUEST['r4']?>&r5=<?php  echo $_REQUEST['r5']?>&Submit=<?php  echo $_REQUEST['Submit']?>&Target=UpdateShippingBulk">
					<div style="text-align:left;height:<?php  echo $height?>px;border:1px solid #C7DCF7;overflow-y:scroll;" id="UpdateInfoContainer">
						<table border="0" cellpadding="5" cellspacing="1" width="100%" class="InsideTable" style="border:none;">
							<?php  while($CurrentProduct = $ProductObj->GetObjectFromRecord()):?>
								<tr class="InsideRightTd">
									<td align="center" width="25"><?php  echo $Sr?></td>
									<td align="left"><?php  echo MyStripSlashes($CurrentProduct->ProductName);?>
									<br>
									<a href="<?php  echo SKSEOURL($CurrentProduct->ProductID,"shop/product")?>" target="_blank">Preview</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="<?php  echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php  echo $Page?>&Section=EditProduct&Pg=<?php  echo $Pg?>&ParentID=<?php  echo $ParentID?>&ProductID=<?php  echo $CurrentProduct->ProductID?>">Edit</a>
									</td>
									<td align="left" width="100"><?php  echo MyStripSlashes($CurrentProduct->ModelNo);?></td>
									<td align="left"  width="100">
										<a rel="colorbox_ajax" href="<?php  echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php  echo $Page?>&Section=ShippingPopUp&Meta=false&Popup=true&ProductID=<?php  echo $CurrentProduct->ProductID?>">Shipping Info</a>
									</td>
									<td align="center" width="100">
										<input type="checkbox" name="ChkProductID[]" value="<?php  echo $CurrentProduct->ProductID?>" />
									</td>
								</tr>								
								<input type="hidden" name="ProductID_<?php  echo $Sr?>" value="<?php  echo $CurrentProduct->ProductID?>" />
								<?php  $Sr+=1?>
							<?php  endwhile;?>					
						</table>
					</div>
					<div style="text-align:left;border:1px solid #C7DCF7;" id="accordion">
						<?php  
						$ShippingTypeObj = new DataTable(TABLE_SHIPPING_PZS_TYPES);
						$ShippingTypeObj->Where = "1";
						$ShippingTypeObj->TableSelectAll("","Position ASC");
						$ShippingTypeArray = array();
						while($CurrentShippingType = $ShippingTypeObj->GetObjectFromRecord())
						{
							$ShippingTypeArray[$CurrentShippingType->TypeID] = MyStripSlashes($CurrentShippingType->TypeName);
						}
		
						$ShippingGroupObj->Where = "1";
						$ShippingGroupObj->TableSelectAll("","CreatedDate ASC");
						if($ShippingGroupObj->GetNumRows() > 0)
						{
							while($CurrentShippingGroup = $ShippingGroupObj->GetObjectFromRecord())
							{
								$ShippingZoneObj->Where = "ShippingID = '$CurrentShippingGroup->ShippingID'";
								$ShippingZoneObj->TableSelectAll("","DefaultZone='1' DESC, ZoneName ASC");
								$ShippingZoneArray = array();
								while($CurrentShippingZone = $ShippingZoneObj->GetObjectFromRecord())
								{
									$ShippingZoneArray[$CurrentShippingZone->ZoneID] = MyStripSlashes($CurrentShippingZone->ZoneName);
								}
								
						?>
							<h3 style="padding-left:20px;"><a href="#"><b><?php  echo MyStripSlashes($CurrentShippingGroup->ShippingGroup);?> Shipping Group</b></a></h3>
						 	<div>
						 		<table border="1" cellpadding="5" cellspacing="2" width="100%" class="InsideTable">
						 		<tr>
						 			<td class="InsideLeftTd" width="200">&nbsp;</td>
					 				<?php   foreach ($ShippingTypeArray as $TypeID=>$TypeName):?>
							 			<td class="InsideLeftTd"><b><?php  echo $TypeName;?></b></td>
							 		<?php  endforeach;?>
						 		</tr>
						 		<?php   foreach ($ShippingZoneArray as $ZoneID=>$ZoneName):
						 		?>
							 		<tr>
							 			<td class="InsideLeftTd"><?php  echo $ZoneName?>
							 			<a rel="colorbox_ajax" href="<?php  echo DIR_WS_SITE_CONTROL?>index.php?Page=shipping&Meta=false&Popup=true&Section=PostCodeView&Shipping=ProductZoneShipping&ZoneID=<?php  echo $ZoneID?>" target="_blank" class="adm-link">?</a></td>
						 				<?php   foreach ($ShippingTypeArray as $TypeID=>$TypeName):?>
								 			<td>
								 			<input type="checkbox" name="ChkShip[<?php  echo $ZoneID?>][<?php  echo $TypeID?>]" value="1" <?php  echo (isset($_POST['ChkShip'][$ZoneID][$TypeID]) && $_POST['ChkShip'][$ZoneID][$TypeID]==1)?"checked":""?>>
								 			<input type="text" name="Ship[<?php  echo $ZoneID?>][<?php  echo $TypeID?>]" value="<?php  echo isset($_POST['Ship'][$ZoneID][$TypeID])?$_POST['Ship'][$ZoneID][$TypeID]:""?>" size="8">
								 			</td>
								 		<?php   endforeach;?>
							 		</tr>
							 	<?php   endforeach;?>	
						 		</table>
						 	</div>
						 	<?php  
							}
						}?>
							
						</div>
					
					<div style="text-align:center;padding:10px;"><input type="submit" name="Submit" value="Update" onclick="return confirm('Are you sure you want to update to update the shipping?')" /></div>
				</form>
				<script type="text/javascript">
				jQuery(document).ready(function(){
					$("#accordion").accordion({
						autoHeight:false,
						clearStyle: true 
					});
				});
			</script>
			<?php  else:?>
				<table border="0" cellpadding="5" cellspacing="1" width="100%" class="InsideTable">
					<tr class="InsideRightTd">
						<td>No records found.</td>
					</tr>
				</table>
			<?php  endif;?>
		<?php  	
		endif;		
		break;
	/////////Display Product 
	
	case "UpdatePrices":
		?>
		<table border="0" cellpadding="0" cellspacing="0" width="100%">
			<tr>
				<td height="20" align="center">
				<h4>Update Prices Section</h4>
			</td>
			</tr>
		</table>
		<fieldset style="text-align:left;">
			<legend style="margin-left:10px;padding:0px 5px;">Search Products</legend>
			<form id="FrmFilter" method="GET" action="index.php">
				<input type="hidden" name="Page" value="<?php  echo $Page?>" >
				<input type="hidden" name="Section" value="UpdatePrices" >
				<table border="0" cellpadding="5" cellspacing="1" width="100%" class="InsideTable">
					<tr>
						<td width="16%" class="InsideLeftTd" align="right"><b>Select Category</b></td>
						<td width="16%" class="InsideRightTd" align="left">
							<select name="r1" id="r1">
								<!--<option value="">--Categories--</option>-->
								<?php  echo CategoryChain(0,0,array(@$_REQUEST['r1']),false,0,0,1);?>
							  </select>
						</td>
						<td width="16%" class="InsideLeftTd" align="right"><b>Manufacturers</b></td>
						<td width="16%" class="InsideRightTd" align="left">
							<select name="r2" id="r2">
								<option value="">--Manufacturers--</option>
								<?php  
								$ManufacturerObj->Where = "1";
								$ManufacturerObj->TableSelectAll(array("MNID","MNName"),"MNName ASC");
								while($CurrentManufacturer = $ManufacturerObj->GetObjectFromRecord())
								{
								?>
								<option value="<?php  echo $CurrentManufacturer->MNID?>" <?php  echo @$_REQUEST['r2']==$CurrentManufacturer->MNID?"selected":""?>><?php  echo $CurrentManufacturer->MNName?></option><?php  
								}?>
								
						  	</select>
						</td>
						<td width="18%" class="InsideLeftTd" align="right"><b>keyword/name/model</b></td>
						<td width="18%" class="InsideRightTd" align="left"><input size="18" type="text" name="r3" value="<?php  echo @$_REQUEST['r3']?>"></td>
					</tr>
					<tr>
						<td class="InsideLeftTd" align="right" valign="top"><b>Active Status</b></td>
						<td class="InsideRightTd">
							<select name="r4" id="r4">
								<option value="">All</option>
								<option value="Active" <?php  echo @$_REQUEST['r4'] =="Active" ?"selected":""?>>Active Only</option>
								<option value="Inactive" <?php  echo @$_REQUEST['r4'] == "Inactive" ?"selected":""?>>Inactive Only</option>
							</select>
						</td>
						<td class="InsideLeftTd" align="right" valign="top"><b>Sort Order</b></td>
						<td class="InsideRightTd" align="left">
							<select name="r5" id="r5">
								<!--<option value="">--Order By--</option>-->
								<?php  
								$OrderByArray = array(
								"p.QuickBooksCode ASC"=>"QuickBooksCode ASC",
								"p.QuickBooksCode DESC"=>"QuickBooksCode DESC",
								"p.GoogleMPN ASC"=>"GoogleMPN ASC",
								"p.GoogleMPN DESC"=>"GoogleMPN DESC",
								"ProductName ASC"=>"Product Name ASC",
								"ProductName DESC"=>"Product Name DESC",
								"p.ModelNo ASC"=>"SKU ASC",
								"p.ModelNo DESC"=>"SKU DESC",
								"p.CreatedDate ASC"=>"Created Date ASC",
								"p.CreatedDate DESC"=>"Created Date DESC",
								);
								foreach ($OrderByArray as $k=>$v)
								{
								?>
								<option value="<?php  echo $k?>" <?php  echo @$_REQUEST['r5'] ==$k?"selected":""?>><?php  echo $v?></option>
								<?php  
								}?>
							</select>
						</td>
						<td class="InsideLeftTd" align="right"></td>
						<td class="InsideRightTd" align="left"></td>					
					</tr>
				</table>
				<br/>
				<center><input type="submit" name="Submit" value="Search Products"></center>
			</form>
		</fieldset>
		<?php  		
		if(isset($_REQUEST['Submit'])):
			$WherePart ="";
			if($_REQUEST['r1']!=""):
				$CatArr = implode(GetCategoryTreeArray($ProductObj->MysqlEscapeString($_REQUEST['r1']),array()),",");
				if(!empty($CatArr))
					$WherePart.=" and ptc.RelationID IN (".$CatArr.")";
			endif;
			
			if($_REQUEST['r2']!="")
				$WherePart.=" and p.mnid='".$ProductObj->MysqlEscapeString($_REQUEST['r2'])."'";
			
			if($_REQUEST['r3']!="")
			{				
				$WherePart .=" AND (";
				if(preg_match("/\+\b/i",$_REQUEST['r3']))
				{
					$Prcident= "AND";
					$SecGate = "1";
					$KeywordArr =  explode("+",$_REQUEST['r3']);
				}
				else 
				{
					$Prcident= "AND";
					$SecGate = "1";
					$KeywordArr =  explode(" ",$_REQUEST['r3']);
				}
				foreach ($KeywordArr as $Key=>$Value)
				{
					if(trim($Value) !="")
						$WherePart .="(ProductName LIKE '%$Value%' OR GoogleMPN LIKE '%$Value%' OR QuickBooksCode LIKE '%$Value%' OR ProductLine LIKE '%$Value%' OR LargeDescription LIKE '%$Value%' OR ModelNo LIKE '%$Value%') $Prcident ";
				}			
				$WherePart .=" $SecGate)";				
			}
			
			if($_REQUEST['r4']=="Active")
				$WherePart .=" AND p.Active=1";
			else if($_REQUEST['r4']=="Inactive")
				$WherePart .=" AND p.Active=0";
				
				
			$ProductObj = new DataTable(TABLE_PRODUCT. " p, ".TABLE_PRODUCT_RELATION. " ptc,".TABLE_MANUFACTURERS." m");
			$ProductObj->Where = "p.MNID = m.MNID AND ptc.ProductID=p.ProductID and ptc.RelationType='category'".$WherePart." GROUP BY p.ProductID";
			$ProductObj->TableSelectAll("",$_REQUEST['r5']);
		?>
			<br/><br/>
			<h2>Search Results</h2>			
			<table border="0" cellpadding="5" cellspacing="1" width="100%" class="InsideTable" style="margin-top:10px;border-bottom:none;">
				<tr class="InsideLeftTd">
					<td align="center" width="25"><b>Sr.</b></td>
					<td align="left"><b>Product Name</b></td>
					<td align="right" width="60"><b>Org RRP</b></td>
					<td align="center" width="70"><b>New RRP</b></td>
					<td align="right" width="95"><b>Org Sale Price</b></td>
					<td align="center" width="100"><b>New Sale Price</b></td>
					<td align="right" width="105"><b>Org Sale Active</b></td>
					<td align="center" width="105"><b>New Sale Active</b></td>
					<td width="6">&nbsp;</td>
				</tr>
				<?php  if($ProductObj->GetNumRows() > 0):?>
					<tr class="InsideRightTd">
						<td></td>
						<td></td>
						<td colspan="2" align="center"">
							<input type="button" value="-" onclick="return UpdatePrices(false,'RRP')" />
							<input type="text" name="NewPerRRP" id="NewPerRRP" value="0" size="2" style="text-align:center;background:#fff url(images/percentage.png) center right no-repeat;padding-right:14px;" maxlength="3" />
							<input type="button" value="+" onclick="return UpdatePrices(true,'RRP')" />
						</td>
						<td colspan="2" align="center"">
							<input type="button" value="-" onclick="return UpdatePrices(false,'Sale')" />
							<input type="text" name="NewPerSale" id="NewPerSale" value="0" size="2" style="text-align:center;background:#fff url(images/percentage.png) center right no-repeat;padding-right:14px;" maxlength="3" />
							<input type="button" value="+" onclick="return UpdatePrices(true,'Sale')" />
						</td>
						<td colspan="2" align="center"">
							<input type="checkbox" value="1" onclick="return UpdatePrices(this.checked,'Active')" />
							<input type="button" value="Reset" onclick="return UpdatePrices('Reset','Active')" />
						</td>
						<td></td>
					</tr>
				<?php  endif;?>
			</table>
			<script type="text/javascript">
				
				function UpdatePrices(action, o){
					$("#UpdatePricingContainer tr.InsideRightTd").each(function(){
						var OrgPrice = $(this).find("input[name^='OrgPrice']");
						var Price = $(this).find("input[name^='Price']");
						
						var OrgSalePrice = $(this).find("input[name^='OrgSalePrice']");
						var SalePrice = $(this).find("input[name^='SalePrice']");
						
						var OrgSaleActive = $(this).find("input[name^='OrgSaleActive']");
						var SaleActive = $(this).find("input[name^='SaleActive']");
						
						var NewPer = parseFloat($("#NewPer" + o).val());
						switch(action){
							case true:
								if(o == "RRP")
									Price.val((parseFloat(OrgPrice.val()) + (NewPer * parseFloat(OrgPrice.val()) / 100)).toFixed(2));
								else if(o == "Sale")
									SalePrice.val((parseFloat(OrgSalePrice.val()) + (NewPer * parseFloat(OrgSalePrice.val()) / 100)).toFixed(2));
								else if(o == "Active")
									SaleActive.attr("checked", "checked");
								break;
							case false:
								if(o == "RRP")
									Price.val((parseFloat(OrgPrice.val()) - (NewPer * parseFloat(OrgPrice.val()) / 100)).toFixed(2));
								else if(o == "Sale")
									SalePrice.val((parseFloat(Price.val()) - (NewPer * parseFloat(Price.val()) / 100)).toFixed(2));
								else if(o == "Active")
									SaleActive.removeAttr("checked");
								break;
							case "Reset":
								if(OrgSaleActive.val() == "1")
									SaleActive.attr("checked", "checked");
								else
									SaleActive.removeAttr("checked");
								break;
						}							
					});
				}
			</script>
			<?php  if($ProductObj->GetNumRows() > 0):?>
				<?php  $Sr=1?>
				<?php  
					//$height = intval($ProductObj->GetNumRows()*30.25);
					//$height = $height>301 ? 301:$height;
					$height = 401;
				?>
				<form method="POST" action="<?php  echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php  echo $Page?>&Section=<?php  echo $Section?>&r1=<?php  echo $_REQUEST['r1']?>&r2=<?php  echo $_REQUEST['r2']?>&r3=<?php  echo $_REQUEST['r3']?>&r4=<?php  echo $_REQUEST['r4']?>&r5=<?php  echo $_REQUEST['r5']?>&Submit=<?php  echo $_REQUEST['Submit']?>&Target=UpdateProductPrices">
					<div style="text-align:left;height:<?php  echo $height?>px;border:1px solid #C7DCF7;overflow-y:scroll;" id="UpdatePricingContainer">
						<table border="0" cellpadding="5" cellspacing="1" width="100%" class="InsideTable" style="border:none;">
							<?php  while($CurrentProduct = $ProductObj->GetObjectFromRecord()):?>
								<tr class="InsideRightTd" style="background-color:#E8F2FF;">
									<td align="center" width="25"><?php  echo $Sr?></td>
									<td align="left">
									<?php  echo MyStripSlashes($CurrentProduct->ProductName);?> (<?php  echo MyStripSlashes($CurrentProduct->ModelNo);?>)
									<br>
									<a href="<?php  echo SKSEOURL($CurrentProduct->ProductID,"shop/product")?>" target="_blank">Preview</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="<?php  echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php  echo $Page?>&Section=EditProduct&Pg=<?php  echo $Pg?>&ParentID=<?php  echo $ParentID?>&ProductID=<?php  echo $CurrentProduct->ProductID?>">Edit</a>
									</td>
									<td align="right" width="60"><?php  echo Change2CurrentCurrency($CurrentProduct->Price)?></td>
									<td align="center" width="70">
										<input type="hidden" name="OrgPrice<?php  echo $Sr?>" value="<?php  echo $CurrentProduct->Price?>" />
										<input type="text" name="Price<?php  echo $Sr?>" value="<?php  echo number_format($CurrentProduct->Price,2)?>" size="10" />
									</td>
									<td align="right" width="95"><?php  echo Change2CurrentCurrency($CurrentProduct->SalePrice)?></td>
									<td align="center" width="100">
										<input type="hidden" name="OrgSalePrice<?php  echo $Sr?>" value="<?php  echo $CurrentProduct->SalePrice?>"/>
										<input type="text" name="SalePrice<?php  echo $Sr?>" value="<?php  echo number_format($CurrentProduct->SalePrice,2)?>" size="10"/>
									</td>
									<td align="center" width="105"><?php  echo $CurrentProduct->SaleActive == 1 ? "Yes":"No"?></td>
									<td align="center" width="105">
										<input type="hidden" name="OrgSaleActive<?php  echo $Sr?>" value="<?php  echo $CurrentProduct->SaleActive?>" />
										<input type="checkbox" name="SaleActive<?php  echo $Sr?>" <?php  echo $CurrentProduct->SaleActive == 1 ? "checked":""?> value="1" />
									</td>
								</tr>								
								<input type="hidden" name="ProductID_<?php  echo $Sr?>" value="<?php  echo $CurrentProduct->ProductID?>" />
								<?php  
									$ProductAdditionObj->Where = "ProductID='".$ProductAdditionObj->MysqlEscapeString($CurrentProduct->ProductID)."'";
									$ProductAdditionObj->TableSelectAll(array("*"),"AttributeName ASC");
									if($ProductAdditionObj->GetNumRows() > 0)
									{
									while ($CurrentAddition = $ProductAdditionObj->GetObjectFromRecord())
										{
									?>
										<input type="hidden" name="ProductAdditionID[]" value="<?php  echo $CurrentAddition->ProductAdditionID?>">
										<tr class="InsideRightTd" style="background-color:E8F2FF;font-style:italic;font-size:10px;">
											<td align="center" width="25"></td>
											<td align="left">
											<b><?php  echo MyStripSlashes($CurrentAddition->AttributeName);?></b>: <?php  echo MyStripSlashes($CurrentAddition->AttributeValue);?> 
											(<?php  echo MyStripSlashes($CurrentAddition->ModelNo);?>)
											</td>
											<td align="right" width="60"><?php  echo Change2CurrentCurrency($CurrentAddition->NormalPrice)?></td>
											<td align="center" width="70">
												<input type="hidden" name="OrgPrice_AttNormalPrice[<?php  echo $CurrentAddition->ProductAdditionID?>]" value="<?php  echo $CurrentAddition->NormalPrice?>" />
												<input type="text" name="Price_AttNormalPrice[<?php  echo $CurrentAddition->ProductAdditionID?>]" value="<?php  echo number_format($CurrentAddition->NormalPrice,2)?>" size="10" />
											</td>
											<td align="right" width="95"><?php  echo Change2CurrentCurrency($CurrentAddition->Price)?></td>
											<td align="center" width="100">
												<input type="hidden" name="OrgSalePrice_AttPrice[<?php  echo $CurrentAddition->ProductAdditionID?>]" value="<?php  echo $CurrentAddition->Price?>"/>
												<input type="text" name="SalePrice_AttPrice[<?php  echo $CurrentAddition->ProductAdditionID?>]" value="<?php  echo number_format($CurrentAddition->Price,2)?>" size="10"/>
											</td>
											<td align="center" width="105"></td>
											<td align="center" width="105">
											</td>
										</tr>	
									<?php  
										}
									}?>
								
								<?php  $Sr+=1?>
							<?php  endwhile;?>					
						</table>
					</div>
					<input type="hidden" name="Count" value="<?php  echo $Sr?>" />
					<div style="text-align:center;padding:10px;"><input type="submit" name="Submit" value="Update" /></div>
				</form>
			<?php  else:?>
				<table border="0" cellpadding="5" cellspacing="1" width="100%" class="InsideTable">
					<tr class="InsideRightTd">
						<td>No records found.</td>
					</tr>
				</table>
			<?php  endif;?>
		<?php  	
		endif;		
		break;
	/////////Display Product 
	case "ManageStock" :
	?>
		<fieldset style="text-align:left;">
			<legend style="margin-left:10px;padding:0px 5px;">Filter Products</legend>
			<form id="FrmFilter" method="GET" action="index.php">
				<input type="hidden" name="Page" value="<?php  echo $Page?>" >
				<input type="hidden" name="Section" value="<?php  echo $Section?>" >
				<table border="0" cellpadding="5" cellspacing="1" width="100%" class="InsideTable">
					<tr>
						<td width="16%" class="InsideLeftTd" align="right"><b>Select Category</b></td>
						<td width="16%" class="InsideRightTd" align="left">
							<select name="r1" id="r1">
								<option value="">--Categories--</option>
								<?php  echo CategoryChain(0,0,array(@$_REQUEST['r1']),false,0,0,1);?>
							  </select>
						</td>
						<td width="16%" class="InsideLeftTd" align="right"><b>Manufacturers</b></td>
						<td width="16%" class="InsideRightTd" align="left">
							<select name="r2" id="r2">
								<option value="">--Manufacturers--</option>
								<?php  
								$ManufacturerObj->Where = "1";
								$ManufacturerObj->TableSelectAll(array("MNID","MNName"),"MNName ASC");
								while($CurrentManufacturer = $ManufacturerObj->GetObjectFromRecord())
								{
								?>
								<option value="<?php  echo $CurrentManufacturer->MNID?>" <?php  echo @$_REQUEST['r2']==$CurrentManufacturer->MNID?"selected":""?>><?php  echo $CurrentManufacturer->MNName?></option><?php  
								}?>
								
						  	</select>
						</td>
						<td width="18%" class="InsideLeftTd" align="right"><b>keyword/name/model</b></td>
						<td width="18%" class="InsideRightTd" align="left"><input size="18" type="text" name="r3" value="<?php  echo @$_REQUEST['r3']?>"></td>
					</tr>
					<tr>
						<td class="InsideLeftTd" align="right" valign="top"><b>Active Status</b></td>
						<td class="InsideRightTd">
							<select name="r4" id="r4">
								<option value="">All</option>
								<option value="Active" <?php  echo @$_REQUEST['r4'] =="Active" ?"selected":""?>>Active Only</option>
								<option value="Inactive" <?php  echo @$_REQUEST['r4'] == "Inactive" ?"selected":""?>>Inactive Only</option>
							</select>
						</td>
						<td class="InsideLeftTd" align="right" valign="top"><b>Sort Order</b></td>
						<td class="InsideRightTd" align="left">
							<select name="r5" id="r5">
								<!--<option value="">--Order By--</option>-->
								<?php  
								$OrderByArray = array(
								"p.QuickBooksCode ASC"=>"QuickBooksCode ASC",
								"p.QuickBooksCode DESC"=>"QuickBooksCode DESC",
								"p.GoogleMPN ASC"=>"GoogleMPN ASC",
								"p.GoogleMPN DESC"=>"GoogleMPN DESC",
								"Stock ASC"=>"Stock Low to High",
								"Stock DESC"=>"Stock High to Low",
								"ProductName ASC"=>"Product Name ASC",
								"ProductName DESC"=>"Product Name DESC",
								"p.ModelNo ASC"=>"SKU ASC",
								"p.ModelNo DESC"=>"SKU DESC",
								"p.CreatedDate ASC"=>"Created Date ASC",
								"p.CreatedDate DESC"=>"Created Date DESC",
								);
								foreach ($OrderByArray as $k=>$v)
								{
								?>
								<option value="<?php  echo $k?>" <?php  echo @$_REQUEST['r5'] ==$k?"selected":""?>><?php  echo $v?></option>
								<?php  
								}?>
							</select>
						</td>
						<td class="InsideLeftTd" align="right"></td>
						<td class="InsideRightTd" align="left"></td>					
					</tr>
				</table>
				<br/>
				<center><input type="submit" name="Submit" value="Filter Products"></center>
			</form>
		</fieldset>
		<?php  	
		$WherePart = "";	
		if(isset($_REQUEST['Submit'])):
			if($_REQUEST['r1']!=""):
				$CatArr = implode(GetCategoryTreeArray($ProductObj->MysqlEscapeString($_REQUEST['r1']),array()),",");
				if(!empty($CatArr))
					$WherePart.=" and ptc.RelationID IN (".$CatArr.")";
			endif;
			
			if($_REQUEST['r2']!="")
				$WherePart.=" and p.mnid='".$ProductObj->MysqlEscapeString($_REQUEST['r2'])."'";
			
			if($_REQUEST['r3']!="")
			{				
				$WherePart .=" AND (";
				if(preg_match("/\+\b/i",$_REQUEST['r3']))
				{
					$Prcident= "AND";
					$SecGate = "1";
					$KeywordArr =  explode("+",$_REQUEST['r3']);
				}
				else 
				{
					$Prcident= "AND";
					$SecGate = "1";
					$KeywordArr =  explode(" ",$_REQUEST['r3']);
				}
				foreach ($KeywordArr as $Key=>$Value)
				{
					if(trim($Value) !="")
						$WherePart .="(ProductName LIKE '%$Value%' OR GoogleMPN LIKE '%$Value%' OR QuickBooksCode LIKE '%$Value%' OR ProductLine LIKE '%$Value%' OR LargeDescription LIKE '%$Value%' OR ModelNo LIKE '%$Value%') $Prcident ";
				}			
				$WherePart .=" $SecGate)";				
			}
			
			if($_REQUEST['r4']=="Active")
				$WherePart .=" AND p.Active=1";
			else if($_REQUEST['r4']=="Inactive")
				$WherePart .=" AND p.Active=0";
		endif;
				
		$ProductObj = new DataTable(TABLE_PRODUCT. " p, ".TABLE_PRODUCT_RELATION. " ptc,".TABLE_MANUFACTURERS." m");
		$ProductObj->Where = "p.MNID = m.MNID AND ptc.ProductID=p.ProductID and ptc.RelationType='category'".$WherePart." GROUP BY p.ProductID";
		$ProductObj->AllowPaging = true;
		$ProductObj->PageSize=35;
		$ProductObj->PageNo =$Pg;
		$ProductObj->DisplayQuery=true;
		$ProductObj->TableSelectAll(array("distinct p.ProductID","p.ProductName","p.ModelNo","p.Stock","p.MinStockLevel"),(isset($_REQUEST['r5']) && $_REQUEST['r5'] != "" ? $_REQUEST['r5']:"p.QuickBooksCode"));
		$TotalRecords = $ProductObj->TotalRecords ;
		$TotalPages =  $ProductObj->TotalPages;	
	?>
	<br>
		<?php  
		if($ProductObj->GetNumRows() > 0)
		{
		if($TotalRecords > $ProductObj->PageSize)
			{
			?>			
			<div class="paging"><b><?php  echo $TotalRecords?> results found</b>&nbsp;&nbsp;<?php  echo $ProductObj->GetPagingLinks("index.php?Page=".$Page."&Section=".$Section."&r1=".@$_REQUEST['r1']."&r2=".@$_REQUEST['r2']."&r3=".@$_REQUEST['r3']."&r4=".@$_REQUEST['r4']."&r5=".@$_REQUEST['r5']."&Submit=".@$_REQUEST['Submit']."&Pg=",PAGING_FORMAT_BOTH,"","");?></div>			
			<br>
			<?php  
			}
			?>
		<form method="POST" action="<?php  echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php  echo $Page?>&Section=<?php  echo $Section?>&r1=<?php  echo @$_REQUEST['r1']?>&r2=<?php  echo @$_REQUEST['r2']?>&r3=<?php  echo @$_REQUEST['r3']?>&r4=<?php  echo @$_REQUEST['r4']?>&r5=<?php  echo @$_REQUEST['r5']?>&Submit=<?php  echo @$_REQUEST['Submit']?>&Pg=<?php  echo $Pg?>&Target=UpdateStock">	
		<table border="0" cellpadding="5" cellspacing="1" width="100%" class="InsideTable">
			<tr class="InsideLeftTd">
				<td align="center" width="5%"><b>S.No.</b></td>
				<td align="left"><b>Product Name</b></td>
				<td align="center"><b>Stock</b></td>
				<td align="center">
					<input type="button" value="-" onclick="return AutoUpdate(0, $(this).next().val(),'-','All', 0)" />
					<input size="5" style="text-align:center" type="text" value="0" />
					<input type="button" value="+" onclick="return AutoUpdate(0, $(this).prev().val(),'+','All', 0)" />
				</td>
				<td align="center"><b>Stock</b></td>
				<td align="left"><b>Stock Limit</b></td>
			</tr>
			<?php  
			$SNo=($ProductObj->PageSize * ($ProductObj->PageNo-1)) +1;
			$Count=1;
			
			while($CurrentProduct = $ProductObj->GetObjectFromRecord())
			{
				$StArr = GetProductPriceNStock($CurrentProduct->ProductID,1);
			?>
			<tr class="<?php  echo $CurrentProduct->ProductID==$ProductID?"InsideLeftTd":"InsideRightTd";?>">
				<td align="center"><b><?php  echo $SNo?></b></td>
				<td align="left">
					<b><?php  echo MyStripSlashes($CurrentProduct->ProductName);?></b> (<?php  echo MyStripSlashes($CurrentProduct->ModelNo);?>) - &pound;<?php  echo isset($StArr['Price'])?number_format($StArr['Price'],2):"";?> 
					<br>
					<a href="<?php  echo SKSEOURL($CurrentProduct->ProductID,"shop/product")?>" target="_blank">Preview</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="<?php  echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php  echo $Page?>&Section=EditProduct&Pg=<?php  echo $Pg?>&ParentID=<?php  echo $ParentID?>&ProductID=<?php  echo $CurrentProduct->ProductID?>">Edit</a>
					<input type="hidden" name="ProductID_<?php  echo $Count?>" value="<?php  echo $CurrentProduct->ProductID?>">
				</td>
				<td width="50" align="center"><?php  echo $CurrentProduct->Stock?></td>
				<td width="100" align="center">
					<input type="button" value="-" name="Sub_<?php  echo $Count?>" id="Sub_<?php  echo $Count?>" onclick="return AutoUpdate(<?php  echo $CurrentProduct->Stock?>, $('#Auto_<?php  echo $Count?>').val(),'-','Single', <?php  echo $Count?>)" />
					<input size="5" style="text-align:center" type="text" value="0" name="Auto_<?php  echo $Count?>" id="Auto_<?php  echo $Count?>" />
					<input type="button" value="+" name="Add_<?php  echo $Count?>" id="Add_<?php  echo $Count?>" onclick="return AutoUpdate(<?php  echo $CurrentProduct->Stock?>, $('#Auto_<?php  echo $Count?>').val(),'+','Single', <?php  echo $Count?>)" />
				</td>
				<td width="60" align="center"><input size="6" style="text-align:center" type="text" name="Stock_<?php  echo $Count?>" id="Stock_<?php  echo $Count?>" value="<?php  echo $CurrentProduct->Stock?>" /></td>
				<td width="80" align="center"><input size="6" style="text-align:center" type="text" name="MinStockLevel_<?php  echo $Count?>" id="MinStockLevel_<?php  echo $Count?>" value="<?php  echo $CurrentProduct->MinStockLevel?>" /></td>
			</tr>
			<?php  
			$SNo++;
			$Count++;
			}
		?>
			<tr>
				<td align="center" width="5%"></td>
				<td></td>
				<td></td>
				<td align="left"><input type="hidden" name="Count" value="<?php  echo $Count?>"></td>
				<td align="left"><input type="submit" name="Update" value="Update" /></td>
			</tr>
		</table>
		</form>	
		<script type="text/javascript">
			function AutoUpdate(ov,nv,op,tp,idx){
				if(tp == "Single"){
					val = op == "+" ? parseInt(ov) + parseInt(nv) : parseInt(ov) - parseInt(nv);
					$("#Stock_" + idx).val(val);
				}else{
					$("input[name^='Stock_']").each(function(){
						$(this).parent().parent().find("input[name^='Auto_']").val(nv);
						if(op == "+")
							$(this).parent().parent().find("input[name^='Add_']").click();
						else
							$(this).parent().parent().find("input[name^='Sub_']").click();
					});
				}
				return false;
			}
		</script>					
		<?php  
		if($TotalRecords > $ProductObj->PageSize)
			{
			?>
			<br>
			<div class="paging"><b><?php  echo $TotalRecords?> results found</b>&nbsp;&nbsp;<?php  echo $ProductObj->GetPagingLinks("index.php?Page=".$Page."&Section=".$Section."&r1=".@$_REQUEST['r1']."&r2=".@$_REQUEST['r2']."&r3=".@$_REQUEST['r3']."&r4=".@$_REQUEST['r4']."&r5=".@$_REQUEST['r5']."&Submit=".@$_REQUEST['Submit']."&Pg=",PAGING_FORMAT_BOTH,"","");?></div>			
			<br>
			<?php  
			}
		}
		else 
		{
			?>
			<b>No Result Found.</b>
			<?php  
		}
	break;
	case "DisplayProduct" :
	default:		
		if($ParentID != "0"){
			$ProductObj = new DataTable(TABLE_PRODUCT. " p, ".TABLE_PRODUCT_RELATION. " ptc");
			$ProductObj->Where = "ProductType != 'Child' AND ptc.ProductID=p.ProductID and ptc.RelationType='category' and ptc.RelationID='".$ProductObj->MysqlEscapeString($ParentID)."'";
		} else { 
			/*Uncategorized products*/
			$ProductObj = new DataTable(TABLE_PRODUCT);
			$ProductObj->Where = "ProductType != 'Child' AND ProductID NOT IN (select ProductID from ".TABLE_PRODUCT_RELATION. " where RelationType='category')";
		}
		$Keyword = @$_REQUEST['k'];
		$WherePart ="";
		if(!empty($Keyword))
		{
			$WherePart .=" AND (";
			if(preg_match("/\+\b/i",$Keyword))
			{
				$Prcident= "AND";
				$SecGate = "1";
				$KeywordArr =  explode("+",$Keyword);
			}
			else 
			{
				$Prcident= "AND";
				$SecGate = "1";
				$KeywordArr =  explode(" ",$Keyword);
			}
			foreach ($KeywordArr as $Key=>$Value)
			{
				$Value = trim($Value);
				if($Value !="")
					$WherePart .="(ProductName LIKE '%$Value%' OR 
								   ProductLine LIKE '%$Value%' OR 
								   QuickBooksCode LIKE '%$Value%' OR 
								   GoogleMPN LIKE '%$Value%' OR 
								   LargeDescription LIKE '%$Value%' OR 
								   ModelNo LIKE '%$Value%') $Prcident ";
			}			
			$WherePart .=" $SecGate)";
			$ProductObj->Where .= $WherePart;
		}
		
		$ProductObj->AllowPaging =true;
		$ProductObj->PageSize=25;
		$ProductObj->PageNo =$Pg;
		if($ParentID != "0")
			$ProductObj->TableSelectAll("","p.ProductID=$ProductID DESC, ptc.SortOrder ASC,CreatedDate DESC");
		else
			$ProductObj->TableSelectAll("","ProductID=$ProductID DESC, CreatedDate DESC");
		
		$TotalRecords = $ProductObj->TotalRecords ;
		$TotalPages =  $ProductObj->TotalPages;
	
		?>
		<!-- Search -->
		<form class="form-horizontal form-search well" id="FrmFilter" method="GET" action="index.php">
			<legend>Search Products</legend>
			<input type="hidden" name="Page" value="<?php  echo $Page?>" >
			<input type="hidden" name="Section" value="<?php  echo $Section?>" >
			<input type="hidden" name="ParentID" value="<?php  echo $ParentID?>" >
			Keyword&nbsp;<input type="text" name="k" value="<?php  echo @$_GET['k']?>">
					&nbsp;&nbsp;&nbsp;&nbsp;
					<button type="submit" name="Go" class="btn"><icon class="icon-search icon-brown"></icon> Search</button>
		</form>
	<!-- End Search -->
		<?php  
		if($ProductObj->GetNumRows() > 0)
		{
		if($TotalRecords > $ProductObj->PageSize)
			{
			?>
			<table cellpadding="0" width="100%" cellspacing="0" border="0" class="InsideTable">
				<tr>
				<td width="100%"><?php  echo $ProductObj->GetPagingLinks("index.php?Page=".$Page."&Section=".$Section."&k=".$Keyword."&ParentID=".$ParentID."&Pg=",PAGING_FORMAT_LIST,"adm-link","<b>Page No : </b>");?>
					<div class="pull-right">
						Total <?php    echo $TotalRecords?> result found.
					</div>
				</td>	
				</tr>
			</table>
			<?php  
			}
			?>
		<form method="POST" action="<?php  echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php  echo $Page?>&Section=<?php  echo $Section?>&k=<?php  echo $Keyword?>&Pg=<?php  echo $Pg?>&Target=UpdateProduct&ParentID=<?php  echo $ParentID?>">
		
		<table class="table table-striped table-bordered table-responsive block">
			<thead>
			<tr class="InsideLeftTd">
				<th align="center" width="5%">S.No.</th>
				<th align="left">Product Name</th>
				<th align="left">Image</th>
				<th align="center">Move Product</th>
				<th align="center">Copy Products</th>
				<th align="center">More Information</th>
				<th align="center">Active</th>
				<!--<th align="center">New Arrivals</th>
				<th align="center">Best Sellers</th>
				<th align="center">Special Offers</th>-->
				<th align="center">Position</th>
				<th align="center">Edit</th>
				<th align="center">Delete</th>
			</tr>
			</thead>
			<script>
				function FunctionMoveProduct(ProductID,Obj)
				{
					ParentID =Obj.value;
					result = confirm("Are you sure want to move the product?")
					if(result == true)
					{
						URL ="<?php  echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php  echo $Page?>&Section=<?php  echo $Section?>&Pg=<?php  echo $Pg?>&Target=Moved&ParentID=<?php  echo $ParentID?>&MoveProductID=" + ProductID + "&MoveParentID=" + ParentID;
						location.href=URL;
					}
					else
					{
						Obj.value="";
					}
					return false;
				}				
				function FunctionCopyProduct(ProductID,Obj)
				{
					ParentID =Obj.value;
					result = confirm("Are you sure want to copy the product?")
					if(result == true)
					{
						URL ="<?php  echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php  echo $Page?>&Section=<?php  echo $Section?>&Pg=<?php  echo $Pg?>&Target=Copied&ParentID=<?php  echo $ParentID?>&CopyProductID=" + ProductID + "&CopyParentID=" + ParentID;
						location.href=URL;
					}
					else
					{
						Obj.value="";
					}
					return false;
				}
				function FunctionDeleteProduct()
				{
					result = confirm("Are you sure want to delete the Product?")
					if(result == true)
					{
						return true;
					}
					return false;
				}				
							
			</script>
			<?php  
			$SNo=($ProductObj->PageSize * ($ProductObj->PageNo-1)) +1;
			$Count=1;
			
			$CategoryObj->Where = "CategoryID='".$CategoryObj->MysqlEscapeString($ParentID)."'";
			$CurrentCategory = $CategoryObj->TableSelectOne();
			$ProductArray = array();
			while($CurrentProduct = $ProductObj->GetObjectFromRecord())
			{
				$ProductArray[] = $CurrentProduct->ProductID;
				$StArr = GetProductPriceNStock($CurrentProduct->ProductID,1);
			?>
			<tr class="<?php  echo $CurrentProduct->ProductID==$ProductID?"InsideLeftTd":"InsideRightTd";?>">
				<td align="center"><b><?php  echo $SNo?></b></td>
				<td align="left"><b><?php  echo MyStripSlashes($CurrentProduct->ProductName);?></b>
				<br><?php  echo MyStripSlashes($CurrentProduct->ModelNo);?>
				<br><?php  echo isset($StArr['DisplayPrice'])?$StArr['DisplayPrice']:"";?>
				<br><a href="<?php  echo SKSEOURL($CurrentProduct->ProductID,"shop/product")?>" target="_blank" class="btn btn-success"><icon class="icon-eye-open icon-white"></icon> Preview</a>
				<input type="hidden" name="ProductID_<?php  echo $Count?>" value="<?php  echo $CurrentProduct->ProductID?>"></td>
				<td align="center">
				<?php  
				if($CurrentProduct->Image !="" && file_exists(DIR_FS_SITE_UPLOADS_PRODUCT_ORIGINAL.$CurrentProduct->Image))
				{?>
					<a href="<?php  echo DIR_WS_SITE_UPLOADS_PRODUCT_ORIGINAL.$CurrentProduct->Image?>" target="_blank">
						<?php  SKImgDisplay(DIR_FS_SITE_UPLOADS_PRODUCT_ORIGINAL.$CurrentProduct->Image,70,'&nbsp;');?>
						<br>
						View Enlarge
					</a>
			  <?php  }?>
				</td>
								<td align="center">
				<?php  
				if($ParentID != "0"){
				if(isset($_GET['Target']) && $_GET['Target']=="MoveProduct" && isset($_GET['ProductID']) && $_GET['ProductID']==$CurrentProduct->ProductID)
				{	?>
					<select name="MoveProduct<?php  echo $Count?>" id="MoveProduct<?php  echo $Count?>" onchange="return FunctionMoveProduct('<?php  echo $CurrentProduct->ProductID?>',this)">
					<option value="">--Move Product--</option>
					<?php  
					if($ParentID!=0)
					{?>
					<?php  
					}?>
					<?php  $CategoryOption = CategoryChain(0,0,array(),false,0,0,1);
					echo $CategoryOption;?>
					</select>
				<?php  
				}
				else 
				{
					?>
					<a href="<?php  echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php  echo $Page?>&Section=<?php  echo $Section?>&Pg=<?php  echo $Pg?>&Target=MoveProduct&ParentID=<?php  echo $ParentID?>&ProductID=<?php  echo $CurrentProduct->ProductID?>"><b>Move Product</b></a>				
					<?php  
				}
				}
				?>
				</td>
				<td align="center">
				<?php  
				if($ParentID != "0"){
				if(isset($_GET['Target']) && $_GET['Target']=="CopyProduct" && isset($_GET['ProductID']) && $_GET['ProductID']==$CurrentProduct->ProductID)
				{	?>
					<select name="CopyProduct<?php  echo $Count?>" id="CopyProduct<?php  echo $Count?>" onchange="return FunctionCopyProduct('<?php  echo $CurrentProduct->ProductID?>',this)">
					<option value="">--Copy Product--</option>
					<?php  
					if($ParentID!=0)
					{?>
					<?php  
					}?>
					<?php  $CategoryOption =CategoryChain(0,0,array(),false,0,0,1);
					echo $CategoryOption;?>
					</select>
				<?php  
				}
				else 
				{
					?>
					<a href="<?php  echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php  echo $Page?>&Section=<?php  echo $Section?>&Pg=<?php  echo $Pg?>&Target=CopyProduct&ParentID=<?php  echo $ParentID?>&ProductID=<?php  echo $CurrentProduct->ProductID?>"><b>Copy Product</b></a>				
					<?php  
				}}
				?>
							
					</td>
				<td align="center" style="text-align:center;">
					<a href="<?php  echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php  echo $Page?>&Section=Images&Pg=<?php  echo $Pg?>&ParentID=<?php  echo $ParentID?>&ProductID=<?php  echo $CurrentProduct->ProductID?>"><b>[IMAGES]</b></a>				
					<br>
					<?php  
					if(isset($CurrentProduct->ProductType) && $CurrentProduct->ProductType=="Bundle")
					{
					?>
					<br>
					<a href="<?php  echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php  echo $Page?>&Section=ChildProduct&Pg=<?php  echo $Pg?>&ParentID=<?php  echo $ParentID?>&ProductID=<?php  echo $CurrentProduct->ProductID?>"><b>[Child Products /Attributes]</b></a>
					<br>
					<?php  
					}?>
					
					<?php  
					if(@constant("DEFINE_PRODUCT_REVIEWS") =="1")
					{
					?>
					<br>
					<a href="<?php  echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php  echo $Page?>&Section=Reviews&Pg=<?php  echo $Pg?>&ParentID=<?php  echo $ParentID?>&ProductID=<?php  echo $CurrentProduct->ProductID?>"><b>[REVIEWS]</b></a>
					<br>
					<?php  
					}?>
					<?php  
					if(@constant("DEFINE_PRODUCT_QUESTION") =="1")
					{
					?>
					<br>
					<a href="<?php  echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php  echo $Page?>&Section=Questions&Pg=<?php  echo $Pg?>&ParentID=<?php  echo $ParentID?>&ProductID=<?php  echo $CurrentProduct->ProductID?>"><b>[QUESTIONS]</b></a>
					<br>
					<?php  
					}?>
					<?php  
					if(@constant("DEFINE_PRODUCT_TAB") =="1")
					{
					?>
					<br>
					<a href="<?php  echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php  echo $Page?>&Section=Infos&Pg=<?php  echo $Pg?>&ParentID=<?php  echo $ParentID?>&ProductID=<?php  echo $CurrentProduct->ProductID?>"><b>[TAB INFO.]</b></a>
					<br>
					<?php  
					}?>
					<br>
					
				</td>	
				<td align="center">
					<input type="checkbox" name="Active_<?php  echo $Count?>" value="1" class="chk" <?php  echo $CurrentProduct->Active==1?"checked":""?>>
				</td>
				
				<!--<td align="center">
					<input type="checkbox" name="Featured_<?php  echo $Count?>" value="1" class="chk" <?php  echo $CurrentProduct->Featured==1?"checked":""?>>
				</td>
				<td align="center">
					<input type="checkbox" name="BestSeller_<?php  echo $Count?>" value="1" class="chk" <?php  echo $CurrentProduct->BestSeller==1?"checked":""?>>
				</td>
				<td align="center">
					<input type="checkbox" name="Homepage_<?php  echo $Count?>" value="1" class="chk" <?php  echo $CurrentProduct->Homepage==1?"checked":""?>>
				</td>-->
				
				<td align="center">
					<?php    if($ParentID != "0"){?>
						<input type="text" name="SortOrder_<?php  echo $Count?>" size="3" value="<?php  echo $CurrentProduct->SortOrder?>" class="SortBox">
					<?php    }?>
				</td>
				<td align="center"><a href="<?php  echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php  echo $Page?>&Section=EditProduct&Pg=<?php  echo $Pg?>&ParentID=<?php  echo $ParentID?>&ProductID=<?php  echo $CurrentProduct->ProductID?>" class="btn btn-success btn-phone-block"><icon class="icon-pencil icon-white"></icon> Edit</a></td>
				<td align="center">
				<?php  
				$ProductCategoryObj2->Where ="RelationType ='category' AND ProductID='$CurrentProduct->ProductID'";
				$CatTotalObj = $ProductCategoryObj2->TableSelectOne(array("Count(*) as CatTotal"));
				if(isset($CatTotalObj->CatTotal) && $CatTotalObj->CatTotal >1)
				{
				?>
					<a href="<?php  echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php  echo $Page?>&Target=DeleteCategoryProduct&ParentID=<?php  echo $ParentID?>&ProductID=<?php  echo $CurrentProduct->ProductID?>" onclick="return confirm('Are you sure remove this Product from this category?')" class="btn btn-default">Remove from <?php    echo "this category";//$CurrentCategory->CategoryName?></a>
					<br>
				<?php  
				}?>
					<br>
					<a href="<?php  echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php  echo $Page?>&Target=DeleteProduct&ParentID=<?php  echo $ParentID?>&ProductID=<?php  echo $CurrentProduct->ProductID?>" onclick="return FunctionDeleteProduct();" class="btn btn-danger btn-phone-block"><icon class="icon-remove icon-white"></icon> Permanent Delete</a>
				</td>
			</tr>
			<?php  
			$SNo++;
			$Count++;
			}
		?>
			<tr>
				<td align="center" width="5%"></td>
				<td align="left"></td>
				<td align="center"></td>
				<td align="center"></td>
				<td align="center"></td>
				<td align="center"><input type="hidden" name="Count" value="<?php  echo $Count?>"></td>
				<td align="center" colspan="2"><input type="submit" name="UpdateActive" value="Update" class="btn btn-primary"></td>
				<td align="center"></td>
				<td align="center"></td>
			</tr>
		</table>
		</form>						
		<?php  
		if(isset($ProductArray) &&is_array($ProductArray) && count($ProductArray))
			echo "<div style='display:none;'>ProductArray(".implode(",",$ProductArray).")</div>";
		
		if($TotalRecords > $ProductObj->PageSize)
			{
			?>
			<table cellpadding="1" width="100%" cellspacing="1" border="0" class="InsideTable">
				<tr>
				<td width="100%">
				<?php  echo $ProductObj->GetPagingLinks("index.php?Page=".$Page."&Section=".$Section."&k=".$Keyword."&ParentID=".$ParentID."&Pg=",PAGING_FORMAT_LIST,"adm-link","<b>Page No : </b>");?>
				</td>	
				</tr>
			</table>
			<?php  
			}
		}
		else 
		{
			?>
			<b>No Result Found.</b>
			<?php  
		}
	break;
}
///// Section end?>
</div>