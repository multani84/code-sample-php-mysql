<!-- Heading  -->
<div class="row-fluid">
	<div class="span12">
		<h1>Products Import</h1>
		<hr />
	</div>
	<div class="pull-left">
	</div>
	<div class="pull-right">
	</div>
<div class="clearfix separator bottom"></div>
<!--  End Heading-->	

<?php 
//// Section start
switch($Section)
{
	case "Import":
	default:
			?>
		<fieldset style="text-align:left;">
			<form method="POST" action="<?php DIR_WS_SITE_CONTROL?>index.php?Page=<?php echo $Page?>&Section=<?php echo $Section?>&Target=UploadCSVFile" enctype="multipart/form-data">
				<table class="table table-striped table-bordered table-responsive block">
					<tr>
						<td align="center" class="InsideLeftTd" width="200"><b>Upload CSV File</b>
						</td>
							<td align="left" class="InsideRightTd" width="200">
							<input type="file" name="UploadCSV">
						</td>
						<td class="InsideLeftTd" align="left" valign="top">
						<input type="submit" name="Submit" value="Upload">
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<a href="<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php echo $Page?>&Section=<?php echo $Section?>&Target=SampleCSVFile" target="_blank">[SEE SAMPLE FILE]</a>
						</td>						
					</tr>
				</table>
			</form>
			<?php 
			$CSVFile = isset($_REQUEST['CSVFile'])?$_REQUEST['CSVFile']:"";
			$DownloadPath = DIR_FS_SITE_SECURE_DOWNLOAD_PRODUCT."csv_files/";
			?>
		</fieldset>
			<?php 
			if(isset($CSVFile) && $CSVFile !="" && is_file($DownloadPath."/".$CSVFile))
			{
				?>
				<form method="POST" action="<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php echo $Page?>&Section=<?php echo $Section?>&Target=AddCSVProduct" enctype="multipart/form-data">
					<input type="hidden" name="CSVFile" value="<?php echo $DownloadPath.$CSVFile?>">
					<table border="0" cellpadding="3" cellspacing="1" width="100%" style="BORDER-RIGHT: #a9a9a9 1px solid; BORDER-TOP: #a9a9a9 1px solid; BORDER-LEFT: #a9a9a9 1px solid; BORDER-BOTTOM: #a9a9a9 1px solid">
					<tr><td align="left">
					<div id="accordion">
					<table border="0" cellpadding="5" cellspacing="1" width="100%" class="InsideTable">
					<?php 
					$row =1;
					$SNo =1;
					if (($handle = fopen($DownloadPath.$CSVFile, "r")) !== FALSE) {
				    while (($data = fgetcsv($handle)) !== FALSE) {
				    	    $num = count($data);
				    	    if($row ==1)
					        {
								CSVHeaderData($data);
					        	?>
					        		<tr class="InsideLeftTd">
										<td align="center" width="5%"><b>S.No.</b></td>
										<td align="left"><b>SKU</b></td>
										<td align="left"><b>Product Name</b></td>
										<td align="left"><b>Description</b></td>
									</tr>
					
					        	<?php 
					        }
				    	    if($row !=1)
					        {
								foreach($ProductHeader as $key=>$val)
								{
									$index = "DATA_CSV_".strtoupper(str_replace(array(" "),array("_"), trim($val)));
									${$key} = isset($data[@constant($index)])?trim($data[@constant($index)]):"";
								}
		
					        	?>
					        	<tr class="InsideLeftTd">
										<td align="center" width="5%"><?php echo $SNo++;?></td>
										<td align="left"><?php echo $SKU?></td>
										<td align="left"><?php echo $ProductName?></td>
										<td align="left"><?php echo $ProductDescription?></td>
									</tr>
					        	<?php 
					       }
				         $row++;
				         
				         if($row >200)
				         break;
				         ?>
				        
				         <?php 
				         
		            }
				    fclose($handle);
				}
					?>
					 </table>
					</div>
					</td></tr>
					<tr>
						<td align="center"><input type="submit" value="Import" style="font-size:20px;"></td>
					</tr>
					</table>
				</form>
				<script type="text/javascript">
				$(document).ready(function() {
					$("#accordion").accordion({active:false,collapsible:true});
				  });

				</script>	
				<?php 
			}
			
			?>
		<?php 
		break;
	
	}
///// Section end?>