<!-- Heading  -->
<div class="row-fluid">
	<div class="span12">
		<h1>Categories</h1>
		<hr />
	</div>
	<div class="pull-left">
		<a href="<?php  echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php  echo $Page?>" class="adm-link">Categories</a>
		&nbsp;&raquo;&nbsp;
		<?php  echo CategoryChain4Caption($ParentID,"index.php","Page=$Page&ParentID","adm-link",false,"",false,"");?>	
	</div>
	<div class="pull-right">
		<a href="<?php  echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php  echo $Page?>&Section=AddCategory&ParentID=<?php  echo $ParentID?>" class="btn btn-primary">Add Category <icon class="icon-plus-sign icon-white-t"></icon></a>
	</div>
<div class="clearfix separator bottom"></div>
<!--  End Heading-->	

<?php  
if(isset($CurrentCategory->CategoryID) && $CurrentCategory->CategoryID !="" && true)
{
?>
<table border="0" cellpadding="3" cellspacing="1" width="100%" class="InsideTable">
	<tr>
		<td align="center">
		<div class="DivTab">
		  	<ul>
		  		<li><a href="<?php  echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php  echo $Page?>&Section=Attributes&ParentID=<?php  echo $ParentID?>&CategoryID=<?php  echo $CurrentCategory->CategoryID?>"><?php  echo substr(MyStripSlashes($CurrentCategory->CategoryName),0,12)?>'s Attribute(s)</a></li>
		  	</ul>
		</div>
		</td>
	</tr>
</table>
<?php  
}?>	

<?php  
//// Section start
switch($Section)
{
	case "AddCategory" :
	case "EditCategory" :
	/////// Add Category  Start
	?>
	<form class="form-horizontal" method="POST" action="<?php  echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php  echo $Page?>&Section=<?php  echo $Section?>&Target=AddCategory&ParentID=<?php  echo $ParentID?>&CategoryID=<?php  echo $CategoryID?>" enctype="multipart/form-data">
		<div class="well">
			<div class="form-group">
				<label for="field" class="col-sm-4 col-md-5 col-lg-4 control-label">Category Name</label>
				<div class="col-sm-20 col-md-19 col-lg-20 text-left">
					<input type="text" name="CategoryName" value="<?php  echo isset($CurrentCategory->CategoryName)?MyStripSlashes($CurrentCategory->CategoryName):""?>" size="30">
				</div>
			</div>
			
			<div class="form-group">
				<label for="field" class="col-sm-4 col-md-5 col-lg-4 control-label">Category Title</label>
				<div class="col-sm-20 col-md-19 col-lg-20 text-left">
					<input type="text" name="CategoryTitle" value="<?php  echo isset($CurrentCategory->CategoryTitle)?MyStripSlashes($CurrentCategory->CategoryTitle):""?>" size="30">
				</div>
			</div>
			<?php     if(@constant("DEFINE_SEO_URL_ACTIVE") =="1"):?>
			<div class="form-group">
				<label for="field" class="col-sm-4 col-md-5 col-lg-4 control-label">Category URL</label>
				<div class="col-sm-20 col-md-19 col-lg-20 text-left">
					<input type="text" name="URLName" value="<?php  echo isset($_POST['URLName'])?MyStripSlashes($_POST['URLName']):(isset($CurrentCategory->URLName)?MyStripSlashes($CurrentCategory->URLName):"")?>" size="70" onblur="return CheckSpecialSymbol(this);" title="Please do not use special symbol. Only use alfa numeric characters,(_) underscores or (-) dashes.">
					<br><small>Please do not use special symbol. 
					<br>Only use alfa numeric characters,(_) underscores or (-) dashes.</small>
				</div>
			</div>
			<hr>
			<?php     endif;?>
			
			<?php     if(@constant("DEFINE_SEO_META_ACTIVE") =="1"):?>
			<div class="form-group">
				<label for="field" class="col-sm-4 col-md-5 col-lg-4 control-label">Meta Title</label>
				<div class="col-sm-20 col-md-19 col-lg-20 text-left">
					<input type="text" name="MetaTitle" value="<?php  echo isset($_POST['MetaTitle'])?MyStripSlashes($_POST['MetaTitle']):(isset($CurrentCategory->MetaTitle)?MyStripSlashes($CurrentCategory->MetaTitle):"")?>" size="100">
				</div>
			</div>
			<div class="form-group">
				<label for="field" class="col-sm-4 col-md-5 col-lg-4 control-label">Meta Keywords</label>
				<div class="col-sm-20 col-md-19 col-lg-20 text-left">
					<table border="0" width="100%" cellpadding="0" cellspacing="0">
				 	<tr>
					 	<td align="left" valign="top"><textarea name="MetaKeyword" id="MetaKeyword" cols="60" rows="6"><?php  echo isset($_POST['MetaKeyword'])?MyStripSlashes($_POST['MetaKeyword']):(isset($CurrentCategory->MetaKeyword)?MyStripSlashes($CurrentCategory->MetaKeyword):"")?></textarea></td>
					 	<td align="left">
					 		<div style="height:200px;overflow: auto;padding:3px;display:block">
					 		<script type="text/javascript">
					 			function SKKeywordSelection(ckPt,val,ObjID)
								{
									EleObj = document.getElementById(ObjID)
									Str = EleObj.value;
									LStr = EleObj.value;
									if(ckPt)
									{
										LastIndex = LStr.substring(LStr.length-1,LStr.length)
										
										if(Str.indexOf(val) > 0)
											Str = Str
										else
										{
											Str = Str + val + ",";
										}
									}
									else
									{
										Str = Str.replace(val+",",'');
										Str = Str.replace(val,'');
									}
										 
									EleObj.value = Str;
									
								}
					 		</script>
					 			<table width="100%" cellpadding="0" cellspacing="0" class="InsideTable">
					 			<?php  
					 			$KeywordObj = new DataTable(TABLE_KEYWORDS);
								$KeywordObj->TableSelectAll("","Keyword ASC");
								$SNo =1;
								while ($CurrentKeyword = $KeywordObj->GetObjectFromRecord())
								{
					 			?>
					 				<tr>
					 					<td><input type="checkbox" name="ChkAdd[]" id="ChkAdd_<?php  echo $SNo?>" class="chk" value="<?php  echo trim($CurrentKeyword->Keyword)?>" onclick="return SKKeywordSelection(this.checked,this.value,'MetaKeyword');" ></td>
					 					<td><?php  echo $CurrentKeyword->Keyword?></td>
					 				</tr>
					 			<?php  
					 			$SNo++;
								}?>	
					 			</table>
					 		</div>
					 	</td>
					 	</tr>
				 	</table>
				</div>
			</div>
			<div class="form-group">
				<label for="field" class="col-sm-4 col-md-5 col-lg-4 control-label">Meta Description</label>
				<div class="col-sm-20 col-md-19 col-lg-20 text-left">
					<textarea name="MetaDescription" cols="60" rows="3"><?php  echo isset($_POST['MetaDescription'])?MyStripSlashes($_POST['MetaDescription']):(isset($CurrentCategory->MetaDescription)?MyStripSlashes($CurrentCategory->MetaDescription):"")?></textarea>
				</div>
			</div>
			<hr>
			<?php     endif;?>
			
			<div class="form-group">
				<label for="field" class="col-sm-4 col-md-5 col-lg-4 control-label">Description</label>
				<div class="col-sm-20 col-md-19 col-lg-20 text-left">
					<?php  
					$sContent = isset($_POST['TmpDescription'])?MyStripSlashes($_POST['TmpDescription'],true):(isset($CurrentCategory->Description)?MyStripSlashes($CurrentCategory->Description,true):"");
					$SKEditorObj->CArray = array("Width"=>"750",
												 "Height"=>"300",
												 "DMode"=>"Large2",
												 "Help"=>true,
												 "EditorArray"=>array("css"=>DIR_WS_SITE_CONTROL."style.css",
												 					  ),
												 ); 
					$SKEditorObj->CreateEditor("TmpDescription",$sContent);?>
				</div>
			</div>
			
			<div class="form-group">
				<label for="field" class="col-sm-4 col-md-5 col-lg-4 control-label">Upload Image</label>
				<div class="col-sm-20 col-md-19 col-lg-20 text-left">
					<input type="file" name="Upload">
					<?php  
					if(isset($CurrentCategory->Image) && $CurrentCategory->Image !="" && file_exists(DIR_FS_SITE_UPLOADS_OTHER_ORIGINAL.$CurrentCategory->Image))
					{?>
						<div class="pull-left">
							<a href="<?php  echo DIR_WS_SITE_UPLOADS_OTHER_ORIGINAL.$CurrentCategory->Image?>" target="_blank">
								<?php  SKImgDisplay(DIR_FS_SITE_UPLOADS_OTHER_ORIGINAL.$CurrentCategory->Image,DEFINE_CATEGORY_THUMBNAIL_IMAGE_SIZE,'&nbsp;');?>
							</a>
							<br>
							<label><input type="checkbox" name="CategoryImageDelete" value="1" > Delete Image</label>
						</div>
				  <?php  }?>
				</div>
			</div>
			
			<?php     if(false):?>
			<div class="form-group">
				<label for="field" class="col-sm-4 col-md-5 col-lg-4 control-label">Banner</label>
				<div class="col-sm-20 col-md-19 col-lg-20 text-left">
					<select name="UpperBanner1[]" id="UpperBanner1" size="10" multiple>
					<option value="">--Select--</option>
					<?php  
						$BannerObj->Where = "Alignment='H'";
						$BannerObj->TableSelectAll("","Position ASC");
		
					while($CurrentBanner = $BannerObj->GetObjectFromRecord())
					{?>
						<option value="<?php  echo $CurrentBanner->BannerID?>" <?php  echo (isset($_POST['UpperBanner1']) && in_array($CurrentBanner->BannerID,$_POST['UpperBanner1']))?"selected":((isset($CurrentCategory->UpperBanner1) && in_array($CurrentBanner->BannerID,explode(",",$CurrentCategory->UpperBanner1)))?"selected":"")?>><?php  echo $CurrentBanner->BannerName?></option>
						<?php  
					}?>
					</select>
				</div>
			</div>
			<?php     endif;?>
			<?php     if(@constant("DEFINE_CMS_CATEGORY_TEMPLATE")=="1"):?>
			<div class="form-group">
				<label for="field" class="col-sm-4 col-md-5 col-lg-4 control-label">Template</label>
				<div class="col-sm-20 col-md-19 col-lg-20 text-left">
					<select name="Template">														
					<?php     foreach (SKGetTemplates('category') as $k=>$v)
					{?>
					<option value="<?php  echo $k?>" <?php  echo $Template==$k?"selected":""?>><?php  echo $v?></option>
					<?php  
					}?>
					</select>
				</div>
			</div>
			<?php     endif;?>
			
			<div class="form-group">
				<label for="field" class="col-sm-4 col-md-5 col-lg-4 control-label">Active</label>
				<div class="col-sm-20 col-md-19 col-lg-20 text-left">
					<input type="checkbox" name="Active" value="1" class="chk" <?php  echo (isset($CurrentCategory->Active) && $CurrentCategory->Active==1)?"checked":""?>>
				</div>
			</div>
			<?php     if($HCategoryStatus==true):?>
			<div class="form-group">
				<label for="field" class="col-sm-4 col-md-5 col-lg-4 control-label">Homepage</label>
				<div class="col-sm-20 col-md-19 col-lg-20 text-left">
					<input type="checkbox" name="Featured" value="1" class="chk" <?php  echo (isset($CurrentCategory->Featured) && $CurrentCategory->Featured==1)?"checked":""?>>
				</div>
			</div>
			<?php     endif;?>
			
			<div class="form-group">
				<div class="col-sm-6 pull-right">
					<button class="btn btn-warning  btn-block" type="submit" name="AddCategory">Submit</button>
				</div>
			</div>
			
		</div>
	</form>
	<?php  
	/////// Add Category End
	break;
	
	case "Attributes":
			?>
			<form method="POST" action="<?php  echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php  echo $Page?>&Section=<?php  echo $Section?>&Target=UpdateAttribute&ParentID=<?php  echo $ParentID?>&CategoryID=<?php  echo $CategoryID?>">
			<table border="0" cellpadding="3" cellspacing="1" width="100%" class="InsideTable">
				<tr>
					<td align="left"><h3><?php  echo MyStripSlashes($CurrentCategory->CategoryName)?>'s Attributes</h3></td>
				</tr>
				<tr>
					<td>
						<div id="accordion">
							<h3><a href="#">Select Filter Attribute</a></h3>
							<div styl>
							<?php  
								$AtrributeNotIn = array();
								$CategoryRelationObj->TableName = TABLE_CATEGORY_RELATION." cr, ".TABLE_ATTRIBUTE." a ";
								$CategoryRelationObj->Where ="cr.RelationID = a.AttributeID AND cr.RelationType='attribute' AND cr.CategoryID='".$CategoryObj->MysqlEscapeString($CategoryID)."'";
								$CategoryRelationObj->TableSelectAll("","cr.SortOrder ASC");	
								while ($CurrentAttribute = $CategoryRelationObj->GetObjectFromRecord())
								{
									$AtrributeNotIn[] = $CurrentAttribute->AttributeID;
									?>
									<input checked type="checkbox" value="<?php  echo $CurrentAttribute->AttributeID?>" name="Attribute[]" id="Attribute_<?php  echo $CurrentAttribute->AttributeID?>">
									<label for="Attribute_<?php  echo $CurrentAttribute->AttributeID?>"><?php  echo MyStripSlashes($CurrentAttribute->AttributeName)?></label>
									<small>Position:</small><input type="text" name="SortOrder_<?php  echo $CurrentAttribute->AttributeID?>" value="<?php  echo $CurrentAttribute->SortOrder?>" size="3" class="SortBox">	
									<br/>
									<br/>
									<?php  
								}		
						
							if(is_array($AtrributeNotIn) && count($AtrributeNotIn) > 0)
								$AttributeObj->Where="AttributeID Not In (".implode(",",$AtrributeNotIn).")";
							
							
							$AttributeObj->TableSelectAll("","AttributeStatus DESC, Position ASC");
							while ($CurrentAttribute = $AttributeObj->GetObjectFromRecord())
							{
								$AttributeValueObj->Where = "AttributeID='".$CurrentAttribute->AttributeID."'";
								$AttributeValueObj->TableSelectAll("","Position ASC");
								
								$CategoryRelationObj->Where ="RelationType='attribute' AND CategoryID='".$CategoryObj->MysqlEscapeString($CategoryID)."'";
								$CurrentCategoryRelation = $CategoryRelationObj->TableSelectOne(array("GROUP_CONCAT(DISTINCT RelationID ORDER BY RelationID DESC SEPARATOR ',') as Attributes"));	
								$AttributeArray = isset($CurrentCategoryRelation->Attributes)?explode(",",$CurrentCategoryRelation->Attributes):array();
	
							?>
							<input <?php  echo (is_array($AttributeArray) && in_array($CurrentAttribute->AttributeID,$AttributeArray))?"checked":""?> type="checkbox" value="<?php  echo $CurrentAttribute->AttributeID?>" name="Attribute[]" id="Attribute_<?php  echo $CurrentAttribute->AttributeID?>">
							<label for="Attribute_<?php  echo $CurrentAttribute->AttributeID?>"><?php  echo MyStripSlashes($CurrentAttribute->AttributeName)?></label>
							<small>Position:</small><input type="text" name="SortOrder_<?php  echo $CurrentAttribute->AttributeID?>" value="" size="3" class="SortBox">	
							<br/>
							<br/>
						    <?php  
							}?>
							</div>
						</div>
					</td>
				</tr>
				<tr>
					<td align="center"><input type="submit" value="Submit"></td>
				</tr>
				
			</table>
			</form>
			<script type="text/javascript">
			jQuery(document).ready(function(){
				$("#accordion").accordion({
					autoHeight:false,
					clearStyle: true 
				});
			});
		</script>
			<?php  
		break;
	/////////Display Category 
	case "DisplayCategory" :
	default:
	?>
	<table width="100%">
		<tr>
		<td align="center">
	<?php     
		$CategoryObj->Where = "ParentID='".$CategoryObj->MysqlEscapeString($ParentID)."'";
		$CategoryObj->TableSelectAll("","Position ASC");
		if($CategoryObj->GetNumRows() > 0)
		{
		?>
		
		<form method="POST" action="<?php  echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php  echo $Page?>&Section=<?php  echo $Section?>&Target=UpdateCategory&ParentID=<?php  echo $ParentID?>">
		<table class="table table-striped table-bordered table-responsive block">
			<thead>
			<tr>
				<th align="center" width="5%" class="InsideLeftTd">S.No.</th>
				<th align="left" class="InsideLeftTd">Category&nbsp;Name</th>
				<th align="left" class="InsideLeftTd">Image</th>
				<?php  
				if((@constant("DEFINE_CATEGORY_MULITIPLE") == "1") && ($FirstLevelOnly===true ))
				{
				?>
				<th align="center" class="InsideLeftTd">Sub&nbsp;Categories</th>
				<th align="center" class="InsideLeftTd">Move&nbsp;Category</th>
				<?php  
				}?>
				<th align="center" class="InsideLeftTd">Preview</th>
				<th align="center" class="InsideLeftTd">Display&nbsp;Products</th>
				<th align="center" class="InsideLeftTd">Active</th>
				<?php  
				if($HCategoryStatus==true)
				{?>
				<th align="center" class="InsideLeftTd">Homepage</th>
				<?php  
				}?>
				<th align="center" class="InsideLeftTd">Position</th>
				<th align="center" class="InsideLeftTd">Edit</th>
				<th align="center" class="InsideLeftTd">Delete</th>
			</tr>
			</thead>
			<tbody>
			<script>
				function FunctionMoveCategory(CategoryID,Obj)
				{
					ParentID =Obj.value;
					if(CategoryID==ParentID)
					{
						alert("You cannot shift the category.");
						return false;
					}
					result = confirm("Are you sure want to move the category?")
					if(result == true)
					{
						URL ="<?php  echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php  echo $Page?>&Section=<?php  echo $Section?>&Target=Moved&ParentID=<?php  echo $ParentID?>&MoveCategoryID=" + CategoryID + "&MoveParentID=" + ParentID;
						location.href=URL;
					}
					else
					{
						Obj.value="";
					}
					return false;
				}
				function FunctionDeleteCategory(CategoryID,Status)
				{
					if(Status==0)
					{
						alert("First delete all categories and products inside this category.");
						return false;
					}
					else
					{
						result = confirm("Are you sure want to delete the category?")
						if(result == true)
						{
							URL="<?php  echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php  echo $Page?>&Target=DeleteCategory&ParentID=<?php  echo $ParentID?>&CategoryID=" + CategoryID;
							location.href=URL;
							return true;
						}
					}
					return false;
				}				
			</script>
			<?php  
			$SNo=1;
			$Count=1;
			while($CurrentCategory = $CategoryObj->GetObjectFromRecord())
			{
				$CategoryObj2 = new DataTable(TABLE_CATEGORY);
				$CategoryObj2->Where = "ParentID='".$CategoryObj2->MysqlEscapeString($CurrentCategory->CategoryID)."'";
				$CategoryObj2->TableSelectAll(array("CategoryID"),"Position ASC");
				
				$ProductCategoryObj->Where = "Relationtype='category' AND RelationID='".$ProductObj->MysqlEscapeString($CurrentCategory->CategoryID)."'";
				$ProductCategoryObj->TableSelectAll(array("ProductID"));
				
				$TotalProduct = $ProductCategoryObj->GetNumRows();
				$TotalCategory = $CategoryObj2->GetNumRows();
				if($CategoryObj2->GetNumRows()==0 && $TotalProduct==0)
					$Status = 1;
				else 
					$Status = 0;
								?>
			<tr class="InsideRightTd">
				<td align="center"><?php  echo $SNo?></td>
				<td align="left"><?php  echo MyStripSlashes($CurrentCategory->CategoryName)?>
				<input type="hidden" name="CategoryID_<?php  echo $Count?>" value="<?php  echo $CurrentCategory->CategoryID?>"></td>
				
				<td align="center">
				<?php  
				if($CurrentCategory->Image !="" && file_exists(DIR_FS_SITE_UPLOADS_OTHER_ORIGINAL.$CurrentCategory->Image))
				{?>
					<a href="<?php  echo DIR_WS_SITE_UPLOADS_OTHER_ORIGINAL.$CurrentCategory->Image?>" target="_blank">
						<?php  SKImgDisplay(DIR_FS_SITE_UPLOADS_OTHER_ORIGINAL.$CurrentCategory->Image,DEFINE_CATEGORY_THUMBNAIL_IMAGE_SIZE,'&nbsp;');?>
						<br>
						View Enlarge
					</a>
			  <?php  }?>
				</td>
				<?php  
				if(@constant("DEFINE_CATEGORY_MULITIPLE") == "1" && ($FirstLevelOnly===true ))
				{
				?>
				<td align="center"><a href="index.php?Page=<?php  echo $Page?>&ParentID=<?php  echo $CurrentCategory->CategoryID?>">Sub Categories <span class="badge"><?php  echo $TotalCategory?></span></a></td>
				<td align="center">
				<?php  
				if(isset($_GET['Target']) && $_GET['Target']=="MoveCategory" && isset($_GET['CategoryID']) && $_GET['CategoryID']==$CurrentCategory->CategoryID)
				{	?>
					<select name="MoveCategory<?php  echo $Count?>" id="MoveCategory<?php  echo $Count?>" onchange="return FunctionMoveCategory('<?php  echo $CurrentCategory->CategoryID?>',this)">
					<option value="">--Move Category--</option>
					<option value="0">--Main--</option>
					<?php  
					if($ParentID!=0)
					{?>
					<?php  
					}?>
					<?php  $CategoryOption = CategoryChain(0,$CurrentCategory->CategoryID,array(),false,0,0,1);
					echo $CategoryOption;?>
					</select>
				<?php  
				}
				else 
				{
					?>
					<a href="<?php  echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php  echo $Page?>&Section=<?php  echo $Section?>&Target=MoveCategory&ParentID=<?php  echo $ParentID?>&CategoryID=<?php  echo $CurrentCategory->CategoryID?>">Move Category</a>				
					<?php  
				}
				?>
				</td>	
				<?php  
				}?>
				<td align="center">
					<?php     if($CurrentCategory->Template != "hide.php"):?>
					<a href="<?php  echo SKSEOURL($CurrentCategory->CategoryID,"shop/category")?>" target="_blank" class="btn btn-success"><icon class="icon-eye-open icon-white"></icon> Preview</a>
					<?php     endif;?>
				</td>
				<td align="center"><a href="<?php  echo DIR_WS_SITE_CONTROL?>index.php?Page=shop/product&ParentID=<?php  echo $CurrentCategory->CategoryID?>">View Products <span class="badge"><?php  echo $TotalProduct?></span></a>				
					</td>
				<td align="center">
					<input type="checkbox" name="Active_<?php  echo $Count?>" value="1" class="chk" <?php  echo $CurrentCategory->Active==1?"checked":""?> <?php  echo $CurrentCategory->Template == "hide.php"?"disabled":""?>>
				</td>
				<?php  
				if($HCategoryStatus==true)
				{?>
				<td align="center">
					<input type="checkbox" name="Featured_<?php  echo $Count?>" value="1" class="chk" <?php  echo $CurrentCategory->Featured==1?"checked":""?>>
				</td>
				<?php  
				}?>
				<td align="center"><input <?php  echo $CurrentCategory->Template == "hide.php"?"readonly":""?> type="text" name="Position_<?php  echo $Count?>" value="<?php  echo $CurrentCategory->Position?>" size="3" class="SortBox"></td>
				<td align="center"><a href="<?php  echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php  echo $Page?>&Section=EditCategory&ParentID=<?php  echo $ParentID?>&CategoryID=<?php  echo $CurrentCategory->CategoryID?>" class="btn btn-success btn-phone-block"><icon class="icon-pencil icon-white"></icon> Edit</a></td>
				<td align="center">
				<?php  
				//if($ParentID !=0)
				{
				?>
					<a href="#" onclick="return FunctionDeleteCategory('<?php  echo $CurrentCategory->CategoryID?>','<?php  echo $Status?>');" class="btn btn-danger btn-phone-block"><icon class="icon-remove icon-white"></icon> Delete</a>
					<?php  
				}?>
				</td>
			</tr>
			<?php  
			$SNo++;
			$Count++;
			}
		?>
		<tr class="InsideLeftTd">
				<td align="center" width="5%"></td>
				<td align="center"></td>
				<td align="center"></td>
				<td align="center"></td>
				<?php  
				if(@constant("DEFINE_CATEGORY_MULITIPLE") == "1" && ($FirstLevelOnly===true ))
				{
				?>
				<td align="center"></td>
				<td align="center"></td>
				<?php  
				}?>
				<td align="center"><input type="hidden" name="Count" value="<?php  echo $Count?>"></td>
				<?php  
				if($HCategoryStatus==true)
				{?>
				<td align="center"></td>
				<?php  
				}?>
				<td align="center" colspan="2" style="text-align:center;"><input type="submit" name="UpdateActive" value="Update" class="btn btn-primary"></td>
				<td align="center"></td>
				<td align="center"></td>
			</tr>
			<tbody>
		</table>
		</form>						
		<?php  
		}
		else 
		{
			?>
			<b>No Result Found.</b>
			<?php   
		}
		?>
		</td>
		</tr>
	</table>
		<?php   
	break;
}
///// Section end?>