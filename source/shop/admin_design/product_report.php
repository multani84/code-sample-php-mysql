<table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr>
		<td height="20" align="center">
		<h4>Products Reports Section</h4>
	</td>
	</tr>
</table>
<form id="FrmFilter" method="GET" action="index.php">
	<input type="hidden" name="Page" value="<?=$Page?>" >
	<input type="hidden" name="Section" value="Display" >
<table border="0" cellpadding="0" cellspacing="0" width="100%" class="InsideTable">
	<tr>
		<td align="left" colspan="6"><b>Filter Your results</b></td>
	</tr>
	<tr>
		<td align="left" valign="middle">Active Status&nbsp;&nbsp;<input type="checkbox" name="r1" value="1">
		</td>
		<td align="left">Categories</td>
		<td align="left">
			<select name="r2" id="r2">
				<option value="">--Categories--</option>
				<?$CategoryOption =CategoryChain(0,0,array(@$_REQUEST['r2']),false,0,0,1);
			  		echo $CategoryOption;
			  	?>
			  </select>
		</td>
		<td align="left">Manufacturers</td>
		<td align="left">
			<select name="r3" id="r3">
				<option value="">--Manufacturers--</option>
				<?
				$ManufacturerObj->Where = "1";
				$ManufacturerObj->TableSelectAll(array("MNID","MNName"),"MNName ASC");
				while($CurrentManufacturer = $ManufacturerObj->GetObjectFromRecord())
				{
				?>
				<option value="<?=$CurrentManufacturer->MNID?>" <?=@$_REQUEST['r3']==$CurrentManufacturer->MNID?"selected":""?>><?=$CurrentManufacturer->MNName?></option><?
				}?>
				
		  	</select>
		</td>
	</tr>
	<tr>
	<td align="left" valign="top" colspan="2">Any <b>keyword/ name or<br> model No</b> <input size="25" type="text" name="k" value="<?=@$_REQUEST['k']?>"></td>
		<td align="right">
		<select name="o" id="o">
			<option value="">--Order By--</option>
			<?
			$OrderByArray = array(
			"p.CreatedDate DESC"=>"Created Date DESC",
			"p.CreatedDate ASC"=>"Created Date ASC",
			"ProductName DESC"=>"Product Name DESC",
			"ProductName ASC"=>"Product Name ASC",
			);
			foreach ($OrderByArray as $k=>$v)
			{
			?>
			<option value="<?=$k?>" <?=@$_REQUEST['o'] ==$k?"selected":""?>><?=$v?></option>
			<?
			}?>
		</select>
		</td>
		<td align="left"><input type="submit" name="Submit" value="Generate Report"></td>
	</tr>
</table>
</form>
<?
switch($Section)
{
	case "Display":
		$SelectArray = array("p.ProductID,
 p.ProductName as 'Product Name',
 p.ModelNo as 'Model',
 p.Active as 'Active',
 m.MNName as 'Manufacturer',
(SELECT Pcount FROM (Select ProductID,COUNT(*) as Pcount from ".TABLE_PRODUCT_REPORTS." where ViewType='P' GROUP BY ProductID)  AS tblDumpP  WHERE  tblDumpP.ProductID = p.ProductID) as Purchased,
(SELECT Pcount FROM (Select ProductID,COUNT(*) as Pcount from ".TABLE_PRODUCT_REPORTS." where ViewType='V' GROUP BY ProductID)  AS tblDumpV  WHERE  tblDumpV.ProductID = p.ProductID) as Viewed,
(Select  GROUP_CONCAT(DISTINCT CategoryName ORDER BY CategoryName ASC SEPARATOR ', ')  from ".TABLE_CATEGORY." c LEFT JOIN ".TABLE_PRODUCT_RELATION." ptc on (ptc.Relationtype='category' and c.CategoryID = ptc.RelationID) where ptc.ProductID = p.ProductID) as Categories
");
		
		
		
		$ProductObj = new DataTable();
		$ProductObj->DisplayQuery = true;	
		$ProductObj->Where = "1";
		
		if(@$_REQUEST['r1']!="")
			$ProductObj->Where .=" and p.Active = '".@$_REQUEST['r1']."'";
		if(@$_REQUEST['r2']!="")
		{
			$ProductObj->TableName  = "(".TABLE_PRODUCT." p LEFT JOIN ".TABLE_MANUFACTURERS." m on m.MNID = p.MNID) 
									   LEFT JOIN ".TABLE_PRODUCT_RELATION." ptc2 on p.ProductID = ptc2.ProductID";
			$ProductObj->Where .=" and ptc2.CategoryID = '".@$_REQUEST['r2']."'";
		}
		else 
		{
			$ProductObj->TableName  = TABLE_PRODUCT." p LEFT JOIN ".TABLE_MANUFACTURERS." m on m.MNID = p.MNID";
		}
		
			if(@$_REQUEST['r3']!="")
			$ProductObj->Where .=" and p.MNID = '".@$_REQUEST['r3']."'";
	
			
		$Keyword = @$_REQUEST['k'];

	$WherePart ="";
	if(!empty($Keyword))
	{
		$WherePart .=" AND (";
		if(preg_match("/\+\b/i",$Keyword))
		{
			$Prcident= "AND";
			$SecGate = "1";
			$KeywordArr =  explode("+",$Keyword);
		}
		else 
		{
			$Prcident= "OR";
			$SecGate = "0";
			$KeywordArr =  explode(" ",$Keyword);
		}
		foreach ($KeywordArr as $Key=>$Value)
		{
			if(trim($Value) !="")
				$WherePart .="(
						  	p.ProductName LIKE '%$Value%' 
						  	OR p.ProductLine LIKE '%$Value%' 
						  	OR p.ModelNo LIKE '%$Value%' 
						  	OR p.SmallDescription LIKE '%$Value%' 
						  	OR p.LargeDescription LIKE '%$Value%' 
						  	OR p.MetaTitle LIKE '%$Value%' 
						  	OR p.MetaKeyword LIKE '%$Value%' 
						  	OR p.MetaDescription LIKE '%$Value%' 
						  	OR m.MNName LIKE '%$Value%' 
						  	) $Prcident ";
    
		}
		
		$WherePart .=" $SecGate)";
		$ProductObj->Where .= $WherePart;
	}
		
		if(@$_REQUEST['o']!="")
			$OrderBy = @$_REQUEST['o'];
		else 
			$OrderBy = "p.CreatedDate DESC";
				
		//$ProductObj->DisplayQuery = true;	
		$ProductObj->TableSelectAll($SelectArray,$OrderBy);
		
		if($ProductObj->GetNumRows() >0)
		{
			$ColFields = $ProductObj->GetNumFields();
			?>
			<div align="left">
			<br>
				<form target="_blank" action="<?=DIR_WS_SITE_CONTROL?>index.php?Page=<?=$Page?>&Section=<?=$Section?>&Target=Download" method="POST">
					<input type="hidden" name="Q" value="<?=base64_encode($ProductObj->Query)?>">
					&nbsp;&nbsp;<input type="submit" name="Download" value="Download ALL">
					&nbsp;&nbsp;<input type="submit" name="Print" value="Print">
					&nbsp;&nbsp;<input type="button" name="AskEmail" value="Email Report" onclick="return document.getElementById('DivEmailAddress').style.display='block';">
					<div id="DivEmailAddress" style="display:none;padding:15px;border:1px solid #000;">
						Enter email address:<input size="40" type="text" name="EmailAddress" id="EmailAddress" value="" />
						&nbsp;&nbsp;<input type="submit" name="Email" value="Send">
					</div>
				</form>
			<br>
			</div>
			<table border="0" cellpadding="3" cellspacing="2" width="100%" class="InsideTable">
			  <tr class="InsideLeftTd">
			     <?
			     for ($i = 0; $i < $ColFields; $i++) 
					{
		    			$ColName = $ProductObj->GetFieldName($i);
		    			echo "<td align='left' height=25><b>".$ColName."</b></td>";
		    		}
					 	
				?>
			    <td align='left' height=25><b>Details</b></td>
		    	 </tr>
		    	 
	          <?
	          	while ($Result = $ProductObj->GetArrayFromRecord())
				{
					?>
					<tr>
						<?
						for ($i = 0; $i < $ColFields; $i++) 
							echo "<td align='left' style='padding-bottom:10px;'>".MyStripSlashes($Result[$i])."</td>";
		    			?>
						<td align='left' height=25><a href="<?=DIR_WS_SITE_CONTROL?>index.php?Page=product&Section=EditProduct&ProductID=<?=$Result['ProductID']?>" class="adm-link">Detail</a></td>
					</tr>	
				<?
					
				}
	          ?>
	          
	        </table>
			<?
		}
		else 
		{
			echo "<br><br><br><br><b>No record found</b>";
		}
	break;
	
}
?>

