<table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr>
		<td height="20" colspan="2" align="center">
		<h4>Filter Attributes</h4>
		<?php echo ((isset($CurrentAttribute->AttributeName) && $CurrentAttribute->AttributeName !="")?"<h3>".MyStripSlashes($CurrentAttribute->AttributeName)."</h3>":"");?>
	</td>
	</tr>
	<tr>
		<td height="20" align="left">&nbsp;&nbsp;</td>
		<td height="20" align="right">
		<a href="<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php echo $Page?>&Section=AddAttribute" class="btn btn-primary">Add Attribute <icon class="icon-plus-sign icon-white-t"></icon></a>&nbsp;&nbsp;
		</td>
	</tr>
</table>				
<?php 
//// Section start
switch($Section)
{
	case "AddAttribute" :
	case "EditAttribute" :
	/////// Add Attribute  Start
	?>
	<form class="form-horizontal" method="POST" action="<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php echo $Page?>&Section=<?php echo $Section?>&Target=AddAttribute&AttributeID=<?php echo $AttributeID?>" enctype="multipart/form-data">
		<div class="well">
			
			<div class="form-group">
				<label for="field" class="col-sm-4 col-md-5 col-lg-4 control-label">Attribute Name</label>
				<div class="col-sm-20 col-md-19 col-lg-20 text-left"><input type="text" name="AttributeName" value="<?php echo isset($CurrentAttribute->AttributeName)?MyStripSlashes($CurrentAttribute->AttributeName):""?>" size="30"></div>
			</div>
			<?php 
			if(@constant("SEO_URL_ACTIVE") =="1"  && false)
			{
			?>
				
			<div class="form-group">
				<label for="field" class="col-sm-4 col-md-5 col-lg-4 control-label">Attribute URL</label>
				<div class="col-sm-20 col-md-19 col-lg-20 text-left">
				<input type="text" name="URLName" value="<?php echo isset($_POST['URLName'])?MyStripSlashes($_POST['URLName']):(isset($CurrentAttribute->URLName)?MyStripSlashes($CurrentAttribute->URLName):"")?>" size="70" onblur="return CheckSpecialSymbol(this);" title="Please do not use special symbol. Only use alfa numeric characters,(_) underscores or (-) dashes.">
				<br><small>Please do not use special symbol. 
				<br>Only use alfa numeric characters,(_) underscores or (-) dashes.</small>
				</div>
			</div>
			<?php 
			}?>
			<?php 
			if(@constant("SEO_META_ACTIVE") =="1" && false)
			{
			?>
			<hr>
			<div class="form-group">
				<label for="field" class="col-sm-4 col-md-5 col-lg-4 control-label">Meta Title</label>
				<div class="col-sm-20 col-md-19 col-lg-20 text-left"><input type="text" name="MetaTitle" value="<?php echo isset($_POST['MetaTitle'])?MyStripSlashes($_POST['MetaTitle']):(isset($CurrentAttribute->MetaTitle)?MyStripSlashes($CurrentAttribute->MetaTitle):"")?>" size="100"></div>
			</div>
			<div class="form-group">
				<label for="field" class="col-sm-4 col-md-5 col-lg-4 control-label">Meta Keywords</label>
				<div class="col-sm-20 col-md-19 col-lg-20 text-left">
				 	<table border="0" width="100%" cellpadding="0" cellspacing="0">
				 	<tr>
					 	<td align="left" valign="top"><textarea name="MetaKeyword" id="MetaKeyword" cols="60" rows="6"><?php echo isset($_POST['MetaKeyword'])?MyStripSlashes($_POST['MetaKeyword']):(isset($CurrentAttribute->MetaKeyword)?MyStripSlashes($CurrentAttribute->MetaKeyword):"")?></textarea></td>
					 	<td align="left">
					 		<div style="height:200px;overflow: auto;padding:3px;display:block">
					 		<script type="text/javascript">
					 			function SKKeywordSelection(ckPt,val,ObjID)
								{
									EleObj = document.getElementById(ObjID)
									Str = EleObj.value;
									LStr = EleObj.value;
									if(ckPt)
									{
										LastIndex = LStr.substring(LStr.length-1,LStr.length)
										
										if(Str.indexOf(val) > 0)
											Str = Str
										else
										{
											Str = Str + val + ",";
										}
									}
									else
									{
										Str = Str.replace(val+",",'');
										Str = Str.replace(val,'');
									}
										 
									EleObj.value = Str;
									
								}
					 		</script>
					 			<table width="100%" cellpadding="0" cellspacing="0" class="InsideTable">
					 			<?php 
					 			$KeywordObj = new DataTable(TABLE_KEYWORDS);
								$KeywordObj->TableSelectAll("","Keyword ASC");
								$SNo =1;
								while ($CurrentKeyword = $KeywordObj->GetObjectFromRecord())
								{
					 			?>
					 				<tr>
					 					<td><input type="checkbox" name="ChkAdd[]" id="ChkAdd_<?php echo $SNo?>" class="chk" value="<?php echo trim($CurrentKeyword->Keyword)?>" onclick="return SKKeywordSelection(this.checked,this.value,'MetaKeyword');" ></td>
					 					<td><?php echo $CurrentKeyword->Keyword?></td>
					 				</tr>
					 			<?php 
					 			$SNo++;
								}?>	
					 			</table>
					 		</div>
					 	</td>
					 	</tr>
				 	</table>
				 </div>
			</div>
			<div class="form-group">
				<label for="field" class="col-sm-4 col-md-5 col-lg-4 control-label">Meta Description</label>
				<div class="col-sm-20 col-md-19 col-lg-20 text-left"><textarea name="MetaDescription" cols="60" rows="3"><?php echo isset($_POST['MetaDescription'])?MyStripSlashes($_POST['MetaDescription']):(isset($CurrentAttribute->MetaDescription)?MyStripSlashes($CurrentAttribute->MetaDescription):"")?></textarea></div>
			</div>
			<?php 
			}?>
			<hr>
			<!--<tr>
				<td align="center" class="InsideLeftTd"><b>Description</b></td>
				<td align="left" class="InsideRightTd">
				<?php 
					$sContent = isset($_POST['TmpDescription'])?MyStripSlashes($_POST['TmpDescription'],true):(isset($CurrentAttribute->Description)?MyStripSlashes($CurrentAttribute->Description,true):"");
					$SKEditorObj->CArray = array("Width"=>"750",
												 "Height"=>"300",
												 "DMode"=>"Large2",
												 "Help"=>true,
												 "EditorArray"=>array("css"=>DIR_WS_SITE_CONTROL."style.css",
												 					  ),
												 ); 
					$SKEditorObj->CreateEditor("TmpDescription",$sContent);?>
				</td>
			</tr>
			<tr>
				<td align="center" class="InsideLeftTd"><b>Upload Image</b></td>
				<td align="left" class="InsideRightTd"><input type="file" name="Upload">
				<?php 
				if(isset($CurrentAttribute->Image) && $CurrentAttribute->Image !="" && file_exists(DIR_FS_SITE_UPLOADS_OTHER_ORIGINAL.$CurrentAttribute->Image))
				{?>
					<a href="<?php echo DIR_WS_SITE_UPLOADS_OTHER_ORIGINAL.$CurrentAttribute->Image?>" target="_blank">
						<?php SKImgDisplay(DIR_FS_SITE_UPLOADS_OTHER_ORIGINAL.$CurrentAttribute->Image,DEFINE_ATTRIBUTE_THUMBNAIL_IMAGE_SIZE,'&nbsp;');?>
						<br>
						View Inlarge
					</a>
			  <?php }?>
				</td>
			</tr>-->
			<?php 
			if(@constant("DEFINE_ATTRIBUTE_TEMPLATE")=="1")
			{
				$Template = ((isset($_POST['Template']) && $_POST['Template'] !="")?$_POST['Template']:((isset($CurrentAttribute->Template) && $CurrentAttribute->Template !="")?$CurrentAttribute->Template:DEFINE_ATTRIBUTE_TEMPLATE_DEFAULT));
				?>
			<div class="form-group">
				<label for="field" class="col-sm-4 col-md-5 col-lg-4 control-label">Template</label>
				<div class="col-sm-20 col-md-19 col-lg-20 text-left">
					<select name="Template">														
					<?php foreach (SKGetTemplates('attribute') as $k=>$v)
					{?>
					<option value="<?php echo $k?>" <?php echo $Template==$k?"selected":""?>><?php echo $v?></option>
					<?php 
					}?>
					</select>
				</div>
			</div>
			<?php 
			}?>
			<!--<tr>
				<td align="center" class="InsideLeftTd"><b>Attribute Scope</b></td>
				<td align="left" class="InsideRightTd">
					<select name="AttributeType">														
					<?php foreach ($AttributeTypeArray as $k=>$v)
					{?>
					<option value="<?php echo $k?>" <?php echo (isset($_POST['AttributeType']) && $_POST['AttributeType']==$k)?"selected":((isset($CurrentAttribute->AttributeType) && $CurrentAttribute->AttributeType==$k)?"selected":"")?>><?php echo $v?></option>
					<?php 
					}?>
					</select>
				</td>
			</tr>-->
			<div class="form-group">
				<label for="field" class="col-sm-4 col-md-5 col-lg-4 control-label">Active</label>
				<div class="col-sm-20 col-md-19 col-lg-20 text-left"><input type="checkbox" name="Active" value="1" class="chk" <?php echo (isset($CurrentAttribute->Active) && $CurrentAttribute->Active==1)?"checked":""?>></div>
			</div>
			<div class="form-group">
				<div class="col-sm-6 pull-right">
					<button class="btn btn-warning  btn-block" type="submit" name="AddAttribute">Submit</button>
				</div>
			</div>
		</div>
	</form>
	<?php 
	/////// Add Attribute End
	break;
	
	case "DisplayAttributeValue":
	case "EditAttributeValue":
	$AttributeValueObj->Where = "AttributeID='".$AttributeID."'";
	$AttributeValueObj->TableSelectAll("","Position ASC");
	?>
	<script type="text/javascript">
	function FunctionDeleteAttributeValue()
	{
		result = confirm("Are you sure want to delete this record?")
		if(result == true)
		{
			return true;
		}
		return false;
	}				
	
	</script>
			
	<table class="table table-striped table-bordered table-responsive block">
		<thead>
			<tr>
				<td align="center" width="35"><b>Sr.</b></td>
				<td align="left"><b>Value</b></td>
				<td align="center" width="80"><b>Active</b></td>
				<td align="center" width="80"><b>Position</b></td>
				<td align="center" width="70">&nbsp;</td>
				<td align="center" width="70">&nbsp;</td>
			</tr>
		</thead>
		<tbody>
	<?php 
			$SNo=1;
			$Count=1;
			while($CurrentAttributeValue = $AttributeValueObj->GetObjectFromRecord())
			{
				if($Section=="EditAttributeValue" && $AttributeValueID==$CurrentAttributeValue->AttributeValueID)
				{
					?>
					<form method="POST" action="<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php echo $Page?>&Section=<?php echo $Section?>&AttributeID=<?php echo $AttributeID?>&AttributeValueID=<?php echo $AttributeValueID?>&Target=EditAttributeValue" enctype="multipart/form-data">
						<tr class="InsideRightTd">
							<td align="center"><b><?php echo $SNo?></b></td>
							<td align="left"><input type="text" name="AttributeValue" value="<?php echo MyStripSlashes($CurrentAttributeValue->AttributeValue);?>" size="18"></td>
							<td align="center"><input type="checkbox" name="Active" value="1" <?php echo $CurrentAttributeValue->Active=="1"?"checked":""?> class="chk"></td>
							<td align="center"><input type="text" name="Position" value="<?php echo MyStripSlashes($CurrentAttributeValue->Position);?>" class="SortBox"></td>
							<td align="center"><input type="submit" name="Submit" value="Update AttributeValue" class="btn"></td>
							<td align="center"><input type="button" name="button" value="Cancel" onclick="javascript:history.back();" class="btn"></td>
						</tr>
					</form>					
					<?php 		
				}
				else 
				{
					?>
						<tr class="InsideRightTd">
							<td align="center"><b><?php echo $SNo?></b></td>
							<td align="left" ><?php echo MyStripSlashes($CurrentAttributeValue->AttributeValue);?></td>
							<td align="center" ><img src="<?php echo DIR_WS_SITE_CONTROL_IMAGES.($CurrentAttributeValue->Active=="1"?"info.gif":"error.gif")?>">
							</td>
							<td align="center"><?php echo MyStripSlashes($CurrentAttributeValue->Position);?>
							</td>
							<td align="center"><a href="<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php echo $Page?>&Section=EditAttributeValue&AttributeID=<?php echo $AttributeID?>&AttributeValueID=<?php echo $CurrentAttributeValue->AttributeValueID?>" class="btn btn-success btn-phone-block"><icon class="icon-pencil icon-white"></icon>Edit</b></a></td>
							<td align="center"><a href="<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php echo $Page?>&Section=<?php echo $Section?>&AttributeID=<?php echo $AttributeID?>&Target=DeleteAttributeValue&AttributeValueID=<?php echo $CurrentAttributeValue->AttributeValueID?>" onclick="return FunctionDeleteAttributeValue();" class="btn btn-danger btn-phone-block"><icon class="icon-remove icon-white"></icon><b>Delete</b></a></td>
						</tr>
					<?php 
				}
			$SNo++;
			$Count++;
			}
		?>
			<form method="POST" action="<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php echo $Page?>&Section=<?php echo $Section?>&AttributeID=<?php echo $AttributeID?>&Target=AddAttributeValue" enctype="multipart/form-data">
				<tr class="">
					<td align="center"><b><?php echo $SNo?></b></td>
					<td align="left" ><input type="text" name="AttributeValue" class="txtBox"></td>
					<td align="center" ><input type="checkbox" name="Active" value="1" class="chk"></td>
					<td align="center" ><input type="text" name="Position" class="SortBox"></td>
					<td align="center" colspan="2" ><input type="submit" name="Submit" class="btn" value="Add AttributeValue"></td>
				</tr>
			</form>	
			</tbody>
		</table>
	<?php 
	break;

	/////////Display Attribute 
	case "DisplayAttribute" :
	default:
		$AttributeObj->Where = "AttributeStatus='filter'";
		$AttributeObj->TableSelectAll("","Position ASC");
		if($AttributeObj->GetNumRows() > 0)
		{
		?>
		<form method="POST" action="<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php echo $Page?>&Section=<?php echo $Section?>&Target=UpdateAttribute&">
		<table class="table table-striped table-bordered table-responsive block">
		<thead>
			<tr>
				<td align="center" width="5%" class="InsideLeftTd"><b>S.No.</b></td>
				<td align="left" class="InsideLeftTd"><b>Attribute Name</b></td>
				<!--<td align="left" class="InsideLeftTd"><b>Image</b></td>-->
				<!--<td align="center" class="InsideLeftTd"><b>Preview</b></td>-->
				<td align="center" class="InsideLeftTd"><b>Attribute Value(s)</b></td>
				<td align="center" class="InsideLeftTd"><b>Active</b></td>
				<td align="center" class="InsideLeftTd"><b>Search Page</b></td>
				<td align="center" class="InsideLeftTd"><b>Position</b></td>
				<td align="center" class="InsideLeftTd"><b>Edit</b></td>
				<td align="center" class="InsideLeftTd"><b>Delete</b></td>
			</tr>
			</thead>
			<tbody>
			<script>
				function FunctionDeleteAttribute(AttributeID,Status)
				{
					if(Status==0)
					{
						alert("First delete all record inside this attribute.");
						return false;
					}
					else
					{
						result = confirm("Are you sure want to delete the attribute?")
						if(result == true)
						{
							URL="<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php echo $Page?>&Target=DeleteAttribute&AttributeID=" + AttributeID;
							location.href=URL;
							return true;
						}
					}
					return false;
				}				
			</script>
			<?php 
			$SNo=1;
			$Count=1;
			while($CurrentAttribute = $AttributeObj->GetObjectFromRecord())
			{
				$AttributeValueObj = new DataTable(TABLE_ATTRIBUTE_VALUE);
				$AttributeValueObj->Where = "AttributeID='".$AttributeValueObj->MysqlEscapeString($CurrentAttribute->AttributeID)."'";
				$AttributeValueObj->TableSelectAll(array("AttributeID"),"Position ASC");
				
				$ProductAttributeObj->Where = "Relationtype='attribute' AND RelationID='".$ProductObj->MysqlEscapeString($CurrentAttribute->AttributeID)."'";
				$ProductAttributeObj->TableSelectAll(array("ProductID"));
				
				$TotalProduct = $ProductAttributeObj->GetNumRows();
				$TotalAttributeValue = $AttributeValueObj->GetNumRows();
				if($TotalAttributeValue==0 && $TotalProduct==0)
					$Status = 1;
				else 
					$Status = 0;
								?>
			<tr class="InsideRightTd">
				<td align="center"><b><?php echo $SNo?></b></td>
				<td align="left"><b><?php echo MyStripSlashes($CurrentAttribute->AttributeName)?></b>
				<input type="hidden" name="AttributeID_<?php echo $Count?>" value="<?php echo $CurrentAttribute->AttributeID?>"></td>
				
				<!--<td align="center">
				<?php 
				if($CurrentAttribute->Image !="" && file_exists(DIR_FS_SITE_UPLOADS_OTHER_ORIGINAL.$CurrentAttribute->Image))
				{?>
					<a href="<?php echo DIR_WS_SITE_UPLOADS_OTHER_ORIGINAL.$CurrentAttribute->Image?>" target="_blank">
						<?php SKImgDisplay(DIR_FS_SITE_UPLOADS_OTHER_ORIGINAL.$CurrentAttribute->Image,"70",'&nbsp;');?>
						<br>
						View Inlarge
					</a>
			  <?php }?>
				</td>-->
				<!--<td align="center"><a href="<?php echo SKSEOURL($CurrentAttribute->AttributeID,"attribute")?>" target="_blank">Preview</a></td>-->
				<td align="center"><a href="<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php echo $Page?>&Section=DisplayAttributeValue&AttributeID=<?php echo $CurrentAttribute->AttributeID?>"><b>Attribute Value(s)</b> (<?php echo $TotalAttributeValue?>)</a>				
					</td>
				<td align="center">
					<input type="checkbox" name="Active_<?php echo $Count?>" value="1" class="chk" <?php echo $CurrentAttribute->Active==1?"checked":""?>>
				</td>
				<td align="center">
					<input type="checkbox" name="SearchPage_<?php echo $Count?>" value="1" class="chk" <?php echo $CurrentAttribute->SearchPage==1?"checked":""?>>
				</td>
				<td align="center"><input type="text" name="Position_<?php echo $Count?>" size="3" value="<?php echo $CurrentAttribute->Position?>" class="SortBox"></td>
				<td align="center"><a href="<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php echo $Page?>&Section=EditAttribute&AttributeID=<?php echo $CurrentAttribute->AttributeID?>" class="btn btn-success btn-phone-block"><icon class="icon-pencil icon-white"></icon><b>Edit</b></a></td>
				<td align="center">
					<a href="#" onclick="return FunctionDeleteAttribute('<?php echo $CurrentAttribute->AttributeID?>','<?php echo $Status?>');" class="btn btn-danger btn-phone-block"><icon class="icon-remove icon-white"></icon><b>Delete</b></a>
				</td>
			</tr>
			<?php 
			$SNo++;
			$Count++;
			}
		?>
			<tr>
				<td align="center" width="5%"></td>
				<td align="center"></td>
				<!--<td align="center"></td>-->
				<!--<td align="center"></td>-->
				<td align="center"><input type="hidden" name="Count" value="<?php echo $Count?>"></td>
				<td align="center" colspan="3"><input type="submit" name="UpdateActive" value="Update" class="btn"></td>
				<td align="center"></td>
				<td align="center"></td>
			</tr>
			</tbody>
		</table>
		</form>						
		<?php 
		}
		else 
		{
			?>
			<b>No Result Found.</b>
			<?php 
		}
	break;
}
///// Section end?>