<table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr>
		<td height="20" colspan="2" align="center">
		<h4>Products</h4>
	</td>
	</tr>
</table>
<?
//// Section start
switch($Section)
{
	case "Featured":
		$ProductNameArray = array();
		$ProductObj->Where ="Active = 1";
		$ProductObj->TableSelectAll(array("ProductID","ProductName"),"ProductName ASC");
		while($ProductName = $ProductObj->GetObjectFromRecord())
		{
			$ProductNameArray[$ProductName->ProductID] = MyStripSlashes($ProductName->ProductName);
		}
		
		$ProductNameGoArray = array();
		$ProductNameInArray = array();
		if(isset($_POST['ProductNameGo']) && is_array($_POST['ProductNameGo']))
		{	
			foreach ($_POST['ProductNameGo'] as $k=>$v)
				$ProductNameGoArray[$v] = $ProductNameArray[$v];
			
		}
		else 
		{
			$ProductObj->Where =" Featured ='1' AND Active = 1";
			
			$ProductObj->TableSelectAll(array("ProductID","ProductName"),"ProductName ASC");
			while($ProductNameGo = $ProductObj->GetObjectFromRecord())
				$ProductNameGoArray[$ProductNameGo->ProductID] = $ProductNameArray[$ProductNameGo->ProductID];
		}
		$ProductNameInArray = array_diff($ProductNameArray,$ProductNameGoArray);
		?>
		<form method="POST" action="<?=DIR_WS_SITE_CONTROL?>index.php?Page=<?=$Page?>&Section=<?=$Section?>&Target=UpdateFeaturedProduct" enctype="multipart/form-data">
		<table border="0" cellpadding="3" cellspacing="1" width="800" class="InsideTable">
		<td align="left" colspan="0"  class="InsideRightTd">
					<table border="0" cellpadding="3" cellspacing="1" width="100%" class="InsideTable">
							<tr>
							<td height="40" align="center">&nbsp;</td>
							<td width="30%"><b>Products IN</b></td>
							<td width="10%"></td>
							<td  width="30%"><b>Products OUT</b></td>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td height="40" width="50" align="center"></td>
						<td>
						<select name="ProductNameGo[]" id="ProductNameGo" size="20" multiple ondblclick="fun_singlego();">
						<?
						foreach ($ProductNameGoArray as $k=>$v)
						{?>
						<option value="<?=$k?>"><?=$v?></option>
						<?
						}
						?>
						</select>
						</td>
						<td align="center">
						<button type="button" name="go" onclick="fun_singlego();">&gt;&gt;</button>
						<br>
						<br>
						<button type="button" name="in" onclick="fun_singlein();">&lt;&lt;</button>
						</td>
						<td><select name="ProductNameIn[]" id="ProductNameIn"  size="20" multiple ondblclick="fun_singlein();">
						<?
						foreach ($ProductNameInArray as $k=>$v)
						{?>
						<option value="<?=$k?>"><?=$v?></option>
						<?
						}
						?>
						
						</select>
						</td>
						</tr>
					</table>
					<script type="text/javascript">
		sel_country = document.getElementById("ProductNameIn");
		country = document.getElementById("ProductNameGo");	
		sel_country_len = sel_country.length;
		country_len = country.length;
		
		if(sel_country_len > 0)
			sel_country.options[0].selected = true;
		if(country_len > 0)
			country.options[0].selected = true;

		function FunctionSelectIn()
			{
				for(i=0;i<country_len;i++)
				country.options[i].selected = true;
				return true;
			}
		function fun_sel_all()
		{
			for(i=0;i<sel_country_len;i++)
				sel_country.options[i].selected = true;
		}

		function fun_singlego()
		{
			for(i=0;i<sel_country_len;i++)
				sel_country.options[i].selected = false;
			i=0;		
			while(i<country_len)
			{
				if(country.options[i].selected == true)
				{
					sel_country[sel_country_len] = new Option(country.options[i].text,country.options[i].value);
					country.removeChild(country.options[i]);
					sel_country.options[sel_country_len].selected = true;
					i=0;
					sel_country_len = sel_country.length;
					country_len = country.length;
				}
				else
					i++;
			}
			if(country_len>0)
				country.options[0].selected = true;
		}

		function fun_singlein()
		{
			for(i=0;i<country_len;i++)
				country.options[i].selected = false;
			i=0;			
			while(i<sel_country_len)
			{
				if(sel_country.options[i].selected == true)
				{
					country[country_len] = new Option(sel_country.options[i].text,sel_country.options[i].value);
					sel_country.removeChild(sel_country.options[i]);
					country.options[country_len].selected = true;
					i=0;
					sel_country_len = sel_country.length;
					country_len = country.length;
				}
				else
					i++;
			}
			if(sel_country_len>0)
				sel_country.options[0].selected = true;
		}

		</script>
				</td>				
			</tr>
			<tr>
			<td align="center">
				<input type="submit" name="AddOffer" value="Submit" onclick="return FunctionSelectIn();">
			</td>
		</tr>	
		</table>		
		</form>	
		<?	
	break;
	case "Stock":
		$ProductObj = new DataTable(TABLE_PRODUCT ." p");
		$ProductObj->Where = "1";
		$OrderBy = isset($_GET['OrderBy'])?$_GET['OrderBy']:"Stock ASC";
	$Keyword = @$_REQUEST['k'];
	$WherePart ="";
	if(!empty($Keyword))
	{
		$WherePart .=" AND (";
		if(preg_match("/\+\b/i",$Keyword))
		{
			$Prcident= "AND";
			$SecGate = "1";
			$KeywordArr =  explode("+",$Keyword);
		}
		else 
		{
			/*
					$Prcident= "OR";
					$SecGate = "0";
					$KeywordArr =  explode(" ",$Keyword);
				*/
				$Prcident= "AND";
				$SecGate = "1";
				$KeywordArr =  explode(" ",$Keyword);
		}
		foreach ($KeywordArr as $Key=>$Value)
		{
			if(trim($Value) !="")
				$WherePart .="(
						  	ProductName LIKE '%$Value%' 
						  	OR SmallDescription LIKE '%$Value%' 
						  	OR LargeDescription LIKE '%$Value%' 
						  	OR ProductLine LIKE '%$Value%' 
						  	OR MetaTitle LIKE '%$Value%' 
						  	OR MetaKeyword LIKE '%$Value%' 
						  	OR MetaDescription LIKE '%$Value%' 
						  ) $Prcident ";
    
		}
		
		$WherePart .=" $SecGate)";
		$ProductObj->Where .= $WherePart;
	}
		$SelectArray = array("p.ProductID,p.Stock,p.ProductName,p.ModelNo,p.ProductPoints,
							 (SELECT Pcount FROM (Select ProductID,COUNT(*) as Pcount from ".TABLE_PRODUCT_REPORTS." where ViewType='P' GROUP BY ProductID)  AS tblDumpP  WHERE  tblDumpP.ProductID = p.ProductID) as Purchased,
							 (SELECT Pcount FROM (Select ProductID,COUNT(*) as Pcount from ".TABLE_PRODUCT_REPORTS." where ViewType='V' GROUP BY ProductID)  AS tblDumpV  WHERE  tblDumpV.ProductID = p.ProductID) as Viewed
							 ");
		
		$ProductObj->AllowPaging =true;
		$ProductObj->PageSize=20;
		$ProductObj->PageNo =$Pg;	
		$ProductObj->TableSelectAll($SelectArray,$OrderBy);
		$TotalRecords = $ProductObj->TotalRecords ;
		$TotalPages =  $ProductObj->TotalPages;
	
		?>
		<form id="FrmFilter" method="GET" action="index.php">
		<input type="hidden" name="Page" value="<?=$Page?>" >
		<input type="hidden" name="Section" value="<?=$Section?>" >
		<input type="hidden" name="ParentID" value="<?=$ParentID?>" >
		<table cellpadding="1" width="100%" cellspacing="1" border="0">
			<tr>
					<td width="60%" align="right">
					<b>Filter Your Result(s)</b>&nbsp;&nbsp;:&nbsp;&nbsp;
					Keyword&nbsp;<input type="text" name="k" value="<?=@$_GET['k']?>">
					&nbsp;&nbsp;&nbsp;&nbsp;
					<input type="submit" name="Go" value="Go">
				</td>	
			</tr>
		</table>
		</form>
		<br>
		<?
		if($ProductObj->GetNumRows() > 0)
		{
		if($TotalRecords > $ProductObj->PageSize)
			{
			?>
			<br>
			<table cellpadding="1" width="100%" cellspacing="1" border="0">
				<tr>
				<td width="100%" align="center">
				<div class="paging"><b><?=$TotalRecords?> results found</b>&nbsp;&nbsp;<?=$ProductObj->GetPagingLinks("index.php?Page=".$Page."&Section=".$Section."&k=".$Keyword."&OrderBy=".$OrderBy."&Pg=",PAGING_FORMAT_BOTH,"","");?></div>
				</td>	
				</tr>
			</table>
			<br>
			<?
			}
			?>
		<table border="0" cellpadding="3" cellspacing="1" width="100%" class="InsideTable">
			<tr class="InsideLeftTd">
				<td align="center" width="5%"><b>S.No.</b></td>
				<td align="left"><b>Product Name</b>
					<br><a href="index.php?Page=<?=$Page?>&Section=<?=$Section?>&k=<?=$Keyword?>&OrderBy=ProductName ASC" class="adm-link">ASC</a>&nbsp;|&nbsp;
					<a href="index.php?Page=<?=$Page?>&Section=<?=$Section?>&k=<?=$Keyword?>&OrderBy=ProductName DESC" class="adm-link">DESC</a></td>
					<td align="center"><b>Purchased</b>
					<br><a href="index.php?Page=<?=$Page?>&Section=<?=$Section?>&k=<?=$Keyword?>&OrderBy=Purchased ASC" class="adm-link">ASC</a>&nbsp;|&nbsp;
					<a href="index.php?Page=<?=$Page?>&Section=<?=$Section?>&k=<?=$Keyword?>&OrderBy=Purchased DESC" class="adm-link">DESC</a></td>
					<td align="center"><b>Viewed</b>
					<br><a href="index.php?Page=<?=$Page?>&Section=<?=$Section?>&k=<?=$Keyword?>&OrderBy=Viewed ASC" class="adm-link">ASC</a>&nbsp;|&nbsp;
					<a href="index.php?Page=<?=$Page?>&Section=<?=$Section?>&k=<?=$Keyword?>&OrderBy=Viewed DESC" class="adm-link">DESC</a></td>
					<td align="left"><b>Stock</b>
					</td>
					
			</tr>
			<?
			$SNo=($ProductObj->PageSize * ($ProductObj->PageNo-1)) +1;
			$Count=1;
			
			$CategoryObj->Where = "CategoryID='".$CategoryObj->MysqlEscapeString($ParentID)."'";
			$CurrentCategory = $CategoryObj->TableSelectOne();
			
			while($CurrentProduct = $ProductObj->GetObjectFromRecord())
			{
				$StArr = GetProductPriceNStock($CurrentProduct->ProductID,1);
			?>
			<tr>
				<td align="center"><b><?=$SNo?></b></td>
				<td align="left"><b><?=MyStripSlashes($CurrentProduct->ProductName);?></b>
				<br><?=MyStripSlashes($CurrentProduct->ModelNo);?>
				<br><?=isset($StArr['Price'])?number_format($StArr['Price'],2):"";?>
				<br>
				<a href="<?=SKSEOURL($CurrentProduct->ProductID,"product")?>" target="_blank">Preview</a>&nbsp;|&nbsp;
				<a href="<?=DIR_WS_SITE_CONTROL?>index.php?Page=product&Section=EditProduct&ProductID=<?=$CurrentProduct->ProductID?>">Edit</a>
				<input type="hidden" name="ProductID_<?=$Count?>" value="<?=$CurrentProduct->ProductID?>"></td>
				<td align="center"><?=$CurrentProduct->Purchased?></td>
				<td align="center"><?=$CurrentProduct->Viewed?></td>
				<td align="left">
				<?
				$ProductOptionObj->Where ="ProductID='".$CurrentProduct->ProductID."'";
				$ProductOptionObj->TableSelectAll(array("Attribute1,Attribute2,Stock"),"Stock ASC");
				if($ProductOptionObj->GetNumRows() >0)
				{
					while ($CurrentOption = $ProductOptionObj->GetObjectFromRecord())
					{
						echo ((isset($CurrentOption->Attribute1) && $CurrentOption->Attribute1 !="")?@constant("DEFINE_PRODUCTS_ATTRIBUTE_OPTION1").":".$CurrentOption->Attribute1." ":"").((isset($CurrentOption->Attribute2) && $CurrentOption->Attribute2 !="")?@constant("DEFINE_PRODUCTS_ATTRIBUTE_OPTION2").":".$CurrentOption->Attribute2." ":"")."<b>(".$CurrentOption->Stock.")</b><br>";
			        }
		       }
				else 
				{
					echo "<b>".$CurrentProduct->Stock."</b>";	
				}
				?>
				</td>
			</tr>
			<tr>
				<td align="center" colspan="6"><hr style="height:2px;color:#000;background-color:#000;"></td>
			</tr>
			<?
			$SNo++;
			$Count++;
			}
		?>
		</table>
		<?
		}
		else 
		{
			?>
			<b>No Result Found.</b>
			<?
		}
	break;
	
	case "Questions":
	$ProductQuestionObj->TableName = TABLE_PRODUCT." p, ".TABLE_PRODUCT_QUESTIONS." pq";
	$ProductQuestionObj->Where = "p.ProductID = pq.ProductID";
	
	$OrderBy = isset($_GET['OrderBy'])?$_GET['OrderBy']:"pq.CreatedDate DESC";
	$Keyword = @$_REQUEST['k'];
	$WherePart ="";
	if(!empty($Keyword))
	{
		$WherePart .=" AND (";
		if(preg_match("/\+\b/i",$Keyword))
		{
			$Prcident= "AND";
			$SecGate = "1";
			$KeywordArr =  explode("+",$Keyword);
		}
		else 
		{
			/*
					$Prcident= "OR";
					$SecGate = "0";
					$KeywordArr =  explode(" ",$Keyword);
				*/
				$Prcident= "AND";
				$SecGate = "1";
				$KeywordArr =  explode(" ",$Keyword);
		}
		foreach ($KeywordArr as $Key=>$Value)
		{
			if(trim($Value) !="")
				$WherePart .="(
						  	ProductName LIKE '%$Value%' 
						  	OR SmallDescription LIKE '%$Value%' 
						  	OR LargeDescription LIKE '%$Value%' 
						  	OR ProductLine LIKE '%$Value%' 
						  	OR MetaTitle LIKE '%$Value%' 
						  	OR MetaKeyword LIKE '%$Value%' 
						  	OR MetaDescription LIKE '%$Value%' 
						  	OR Question LIKE '%$Value%' 
						  	OR Answer LIKE '%$Value%' 
						  	OR CName LIKE '%$Value%' 
						  	OR CEmail LIKE '%$Value%' 
						  ) $Prcident ";
    
		}
		
		$WherePart .=" $SecGate)";
		$ProductQuestionObj->Where .= $WherePart;
	}
		
		$ProductQuestionObj->AllowPaging =true;
		$ProductQuestionObj->PageSize=20;
		$ProductQuestionObj->PageNo =$Pg;	
		$ProductQuestionObj->TableSelectAll(array("pq.*","p.ProductName"),$OrderBy);
		$TotalRecords = $ProductQuestionObj->TotalRecords ;
		$TotalPages =  $ProductQuestionObj->TotalPages;
	?>
	<form id="FrmFilter" method="GET" action="index.php">
		<input type="hidden" name="Page" value="<?=$Page?>" >
		<input type="hidden" name="Section" value="<?=$Section?>" >
		<table cellpadding="1" width="100%" cellspacing="1" border="0">
			<tr>
					<td width="60%" align="right">
					<b>Filter Your Result(s)</b>&nbsp;&nbsp;:&nbsp;&nbsp;
					Keyword&nbsp;<input type="text" name="k" value="<?=@$_GET['k']?>">
					&nbsp;&nbsp;&nbsp;&nbsp;
					<input type="submit" name="Go" value="Go">
				</td>	
			</tr>
		</table>
		</form>
		<br>
		<?
	if($ProductQuestionObj->GetNumRows() > 0)
	{
		if($TotalRecords > $ProductQuestionObj->PageSize)
			{
			?>
			<br>
			<table cellpadding="1" width="100%" cellspacing="1" border="0">
				<tr>
				<td width="100%" align="center">
				<div class="paging"><b><?=$TotalRecords?> results found</b>&nbsp;&nbsp;<?=$ProductQuestionObj->GetPagingLinks("index.php?Page=".$Page."&Section=".$Section."&k=".$Keyword."&OrderBy=".$OrderBy."&Pg=",PAGING_FORMAT_NUMBERED,"","");?></div>
				</td>	
				</tr>
			</table>
			<br>
			<?
			}
			
			?>
	<form method="POST" action="<?=DIR_WS_SITE_CONTROL?>index.php?Page=<?=$Page?>&Section=<?=$Section?>&k=<?=$Keyword?>&Pg=<?=$Pg?>&Target=UpdateQuestion">
	<table border="0" cellpadding="3" cellspacing="1" width="100%" class="InsideTable">
			<tr class="InsideLeftTd">
				<td align="center" width="5%"><b>S.No.</b></td>
				<td align="left" width="20%"><b>Product Name</b>
					<br><a href="index.php?Page=<?=$Page?>&Section=<?=$Section?>&k=<?=$Keyword?>&OrderBy=p.ProductName ASC" class="adm-link">ASC</a>&nbsp;|&nbsp;
					<a href="index.php?Page=<?=$Page?>&Section=<?=$Section?>&k=<?=$Keyword?>&OrderBy=p.ProductName DESC" class="adm-link">DESC</a></td>
				<td align="left"><b>Question</b></td>
				<td align="left"><b>Answer</b></td>
				<td align="center" width="10%"><b>Active</b>
					<br><a href="index.php?Page=<?=$Page?>&Section=<?=$Section?>&k=<?=$Keyword?>&OrderBy=pq.Active ASC" class="adm-link">ASC</a>&nbsp;|&nbsp;
					<a href="index.php?Page=<?=$Page?>&Section=<?=$Section?>&k=<?=$Keyword?>&OrderBy=pq.Active DESC" class="adm-link">DESC</a></td>
				<td align="center"></td>
			</tr>
	<?
			$SNo=($ProductQuestionObj->PageSize * ($ProductQuestionObj->PageNo-1)) +1;
			$Count=1;
			while($CurrentQuestion = $ProductQuestionObj->GetObjectFromRecord())
			{
				?>
				<tr class="InsideRightTd">
					<td align="center"><?=$SNo?></td>
					<td align="left"><?=$CurrentQuestion->ProductName?>
					<input type="hidden" name="QuestionID_<?=$Count?>" value="<?=$CurrentQuestion->QuestionID?>">
					<br>
					<a href="<?=SKSEOURL($CurrentQuestion->ProductID,"product")?>" target="_blank">Preview</a>&nbsp;|&nbsp;
					<a href="<?=DIR_WS_SITE_CONTROL?>index.php?Page=product&Section=EditProduct&ProductID=<?=$CurrentQuestion->ProductID?>">Edit</a>
					
					</td>
					<td>
						<div>
                     		<b>From: </b><?=$CurrentQuestion->CName?><br>
                     		<?=nl2br($CurrentQuestion->Question)?><br>
                     		<br>
                     	</div>
					</td>
					<td align="left"><?=nl2br($CurrentQuestion->Answer)?></td>
					<td align="center">
						<input type="checkbox" name="Active_<?=$Count?>" value="1" class="chk" <?=$CurrentQuestion->Active==1?"checked":""?>>
					</td>
					<td align="center"><a href="<?=DIR_WS_SITE_CONTROL?>index.php?Page=product&Section=EditQuestion&ProductID=<?=$CurrentQuestion->ProductID?>&QuestionID=<?=$CurrentQuestion->QuestionID?>"><b>Edit</b></a></td>
				</tr>
				<tr>
					<td align="center" colspan="6"><hr style="height:2px;color:#000;background-color:#000;"></td>
				</tr>
				<?
			$SNo++;
			$Count++;
			}
		?>
			<tr>
				<td align="center" width="5%"></td>
				<td align="left"></td>
				<td align="left"></td>
				<td align="center"><input type="hidden" name="Count" value="<?=$Count?>"></td>
				<td align="center" ><input type="submit" name="UpdateActive" value="Update"></td>
				<td align="center"></td>
			</tr>
		</table>		
		</form>
		<?
}
		else 
		{
			?>
			<b>No Result Found.</b>
			<?
		}?>	
	
	<?
	break;
	case "Reviews":
	$ProductReviewObj->TableName = TABLE_PRODUCT." p, ".TABLE_PRODUCT_REVIEWS." pr";
	$ProductReviewObj->Where = "p.ProductID = pr.ProductID";
	
	$OrderBy = isset($_GET['OrderBy'])?$_GET['OrderBy']:"pr.CreatedDate DESC";
	$Keyword = @$_REQUEST['k'];
	$WherePart ="";
	if(!empty($Keyword))
	{
		$WherePart .=" AND (";
		if(preg_match("/\+\b/i",$Keyword))
		{
			$Prcident= "AND";
			$SecGate = "1";
			$KeywordArr =  explode("+",$Keyword);
		}
		else 
		{
			/*
					$Prcident= "OR";
					$SecGate = "0";
					$KeywordArr =  explode(" ",$Keyword);
				*/
				$Prcident= "AND";
				$SecGate = "1";
				$KeywordArr =  explode(" ",$Keyword);
		}
		foreach ($KeywordArr as $Key=>$Value)
		{
			if(trim($Value) !="")
				$WherePart .="(
						  	ProductName LIKE '%$Value%' 
						  	OR SmallDescription LIKE '%$Value%' 
						  	OR LargeDescription LIKE '%$Value%' 
						  	OR ProductLine LIKE '%$Value%' 
						  	OR pr.Description LIKE '%$Value%' 
						  	OR CName LIKE '%$Value%' 
						  	OR CEmail LIKE '%$Value%' 
						  ) $Prcident ";
    
		}
		
		$WherePart .=" $SecGate)";
		$ProductReviewObj->Where .= $WherePart;
	}
		
		$ProductReviewObj->AllowPaging =true;
		$ProductReviewObj->PageSize=20;
		$ProductReviewObj->PageNo =$Pg;	
		$ProductReviewObj->TableSelectAll(array("pr.*","p.ProductName"),$OrderBy);
		$TotalRecords = $ProductReviewObj->TotalRecords ;
		$TotalPages =  $ProductReviewObj->TotalPages;
	?>
	<form id="FrmFilter" method="GET" action="index.php">
		<input type="hidden" name="Page" value="<?=$Page?>" >
		<input type="hidden" name="Section" value="<?=$Section?>" >
		<table cellpadding="1" width="100%" cellspacing="1" border="0">
			<tr>
					<td width="60%" align="right">
					<b>Filter Your Result(s)</b>&nbsp;&nbsp;:&nbsp;&nbsp;
					Keyword&nbsp;<input type="text" name="k" value="<?=@$_GET['k']?>">
					&nbsp;&nbsp;&nbsp;&nbsp;
					<input type="submit" name="Go" value="Go">
				</td>	
			</tr>
		</table>
		</form>
		<br>
		<?
	if($ProductReviewObj->GetNumRows() > 0)
	{
		if($TotalRecords > $ProductReviewObj->PageSize)
			{
			?>
			<br>
			<table cellpadding="1" width="100%" cellspacing="1" border="0">
				<tr>
				<td width="100%" align="center">
				<div class="paging"><b><?=$TotalRecords?> results found</b>&nbsp;&nbsp;<?=$ProductReviewObj->GetPagingLinks("index.php?Page=".$Page."&Section=".$Section."&k=".$Keyword."&OrderBy=".$OrderBy."&Pg=",PAGING_FORMAT_NUMBERED,"","");?></div>
				</td>	
				</tr>
			</table>
			<br>
			<?
			}
			
			?>
	<form method="POST" action="<?=DIR_WS_SITE_CONTROL?>index.php?Page=<?=$Page?>&Section=<?=$Section?>&k=<?=$Keyword?>&Pg=<?=$Pg?>&Target=UpdateReview">
	<table border="0" cellpadding="3" cellspacing="1" width="100%" class="InsideTable">
			<tr class="InsideLeftTd">
				<td align="center" width="5%"><b>S.No.</b></td>
				<td align="left" width="20%"><b>Product Name</b>
					<br><a href="index.php?Page=<?=$Page?>&Section=<?=$Section?>&k=<?=$Keyword?>&OrderBy=p.ProductName ASC" class="adm-link">ASC</a>&nbsp;|&nbsp;
					<a href="index.php?Page=<?=$Page?>&Section=<?=$Section?>&k=<?=$Keyword?>&OrderBy=p.ProductName DESC" class="adm-link">DESC</a></td>
				<td align="left"><b>Review</b></td>
				<td align="center" width="10%"><b>Active</b>
					<br><a href="index.php?Page=<?=$Page?>&Section=<?=$Section?>&k=<?=$Keyword?>&OrderBy=pr.Active ASC" class="adm-link">ASC</a>&nbsp;|&nbsp;
					<a href="index.php?Page=<?=$Page?>&Section=<?=$Section?>&k=<?=$Keyword?>&OrderBy=pr.Active DESC" class="adm-link">DESC</a></td>
				<td align="center"></td>
			</tr>
	<?
			$SNo=($ProductReviewObj->PageSize * ($ProductReviewObj->PageNo-1)) +1;
			$Count=1;
			while($CurrentReview = $ProductReviewObj->GetObjectFromRecord())
			{
				?>
				<tr class="InsideRightTd">
					<td align="center"><?=$SNo?></td>
					<td align="left"><?=MyStripSlashes($CurrentReview->ProductName)?>
					<input type="hidden" name="ReviewID_<?=$Count?>" value="<?=$CurrentReview->ReviewID?>">
					<br>
					<a href="<?=SKSEOURL($CurrentReview->ProductID,"product")?>" target="_blank">Preview</a>&nbsp;|&nbsp;
					<a href="<?=DIR_WS_SITE_CONTROL?>index.php?Page=product&Section=EditProduct&ProductID=<?=$CurrentReview->ProductID?>">Edit</a>
					
					</td>
					<td>
						<div>
                     		<b>From: </b><?=MyStripSlashes($CurrentReview->CName)?><br>
                     		<?=MyStripSlashes(nl2br($CurrentReview->Description))?><br>
                     		<br>
                     		<b>Rating: <?=SKStarRating($CurrentReview->Rating)?></b> [<?=$CurrentReview->Rating?> of 5 Stars!]
                     	</div>
					</td>
					<td align="center">
						<input type="checkbox" name="Active_<?=$Count?>" value="1" class="chk" <?=$CurrentReview->Active==1?"checked":""?>>
					</td>
					<td align="center"><a href="<?=DIR_WS_SITE_CONTROL?>index.php?Page=product&Section=EditReview&ProductID=<?=$CurrentReview->ProductID?>&ReviewID=<?=$CurrentReview->ReviewID?>"><b>Edit</b></a></td>
				</tr>
				<tr>
					<td align="center" colspan="5"><hr style="height:2px;color:#000;background-color:#000;"></td>
				</tr>
				<?
			$SNo++;
			$Count++;
			}
		?>
			<tr>
				<td align="center" width="5%"></td>
				<td align="left"></td>
				<td align="center"><input type="hidden" name="Count" value="<?=$Count?>"></td>
				<td align="center" ><input type="submit" name="UpdateActive" value="Update"></td>
				<td align="center"></td>
			</tr>
		</table>		
		</form>
		<?
}
		else 
		{
			?>
			<b>No Result Found.</b>
			<?
		}?>	
	
	<?
	break;
	case "DisplayDefault" :
		default:
	?>
		<fieldset style="text-align:left;">
			<legend style="margin-left:10px;padding:0px 5px;">Filter Products</legend>
			<form id="FrmFilter" method="GET" action="index.php">
				<input type="hidden" name="Page" value="<?=$Page?>" >
				<input type="hidden" name="Section" value="<?=$Section?>" >
				<table border="0" cellpadding="5" cellspacing="1" width="100%" class="InsideTable">
					<tr>
						<td width="16%" class="InsideLeftTd" align="right"><b>Select Category</b></td>
						<td width="16%" class="InsideRightTd" align="left">
							<select name="r1" id="r1">
								<option value="">--Categories--</option>
								<?=CategoryChain(0,0,array(@$_REQUEST['r1']),false,0,0,1);?>
							  </select>
						</td>
						<td width="16%" class="InsideLeftTd" align="right"><b>Manufacturers</b></td>
						<td width="16%" class="InsideRightTd" align="left">
							<select name="r2" id="r2">
								<option value="">--Manufacturers--</option>
								<?
								$ManufacturerObj->Where = "1";
								$ManufacturerObj->TableSelectAll(array("MNID","MNName"),"MNName ASC");
								while($CurrentManufacturer = $ManufacturerObj->GetObjectFromRecord())
								{
								?>
								<option value="<?=$CurrentManufacturer->MNID?>" <?=@$_REQUEST['r2']==$CurrentManufacturer->MNID?"selected":""?>><?=$CurrentManufacturer->MNName?></option><?
								}?>
								
						  	</select>
						</td>
						<td width="18%" class="InsideLeftTd" align="right"><b>keyword/name/model</b></td>
						<td width="18%" class="InsideRightTd" align="left"><input size="18" type="text" name="r3" value="<?=@$_REQUEST['r3']?>"></td>
					</tr>
					<tr>
						<td class="InsideLeftTd" align="right" valign="top"><b>Active Status</b></td>
						<td class="InsideRightTd">
							<select name="r4" id="r4">
								<option value="">All</option>
								<option value="Active" <?=@$_REQUEST['r4'] =="Active" ?"selected":""?>>Active Only</option>
								<option value="Inactive" <?=@$_REQUEST['r4'] == "Inactive" ?"selected":""?>>Inactive Only</option>
							</select>
						</td>
						<td class="InsideLeftTd" align="right" valign="top"><b>Sort Order</b></td>
						<td class="InsideRightTd" align="left">
							<select name="r5" id="r5">
								<!--<option value="">--Order By--</option>-->
								<?
								$OrderByArray = array(
								"p.QuickBooksCode ASC"=>"QuickBooksCode ASC",
								"p.QuickBooksCode DESC"=>"QuickBooksCode DESC",
								"p.GoogleMPN ASC"=>"GoogleMPN ASC",
								"p.GoogleMPN DESC"=>"GoogleMPN DESC",
								"Stock ASC"=>"Stock Low to High",
								"Stock DESC"=>"Stock High to Low",
								"ProductName ASC"=>"Product Name ASC",
								"ProductName DESC"=>"Product Name DESC",
								"p.ModelNo ASC"=>"SKU ASC",
								"p.ModelNo DESC"=>"SKU DESC",
								"p.CreatedDate ASC"=>"Created Date ASC",
								"p.CreatedDate DESC"=>"Created Date DESC",
								);
								foreach ($OrderByArray as $k=>$v)
								{
								?>
								<option value="<?=$k?>" <?=@$_REQUEST['r5'] ==$k?"selected":""?>><?=$v?></option>
								<?
								}?>
							</select>
						</td>
						<td class="InsideLeftTd" align="right"></td>
						<td class="InsideRightTd" align="left"></td>					
					</tr>
				</table>
				<br/>
				<center><input type="submit" name="Submit" value="Filter Products"></center>
			</form>
		</fieldset>
		<?	
		$WherePart = "";	
		if(isset($_REQUEST['Submit'])):
			if($_REQUEST['r1']!=""):
				$CatArr = implode(GetCategoryTreeArray($ProductObj->MysqlEscapeString($_REQUEST['r1']),array()),",");
				if(!empty($CatArr))
					$WherePart.=" and ptc.RelationID IN (".$CatArr.")";
			endif;
			
			if($_REQUEST['r2']!="")
				$WherePart.=" and p.mnid='".$ProductObj->MysqlEscapeString($_REQUEST['r2'])."'";
			
			if($_REQUEST['r3']!="")
			{				
				$WherePart .=" AND (";
				if(preg_match("/\+\b/i",$_REQUEST['r3']))
				{
					$Prcident= "AND";
					$SecGate = "1";
					$KeywordArr =  explode("+",$_REQUEST['r3']);
				}
				else 
				{
					$Prcident= "AND";
					$SecGate = "1";
					$KeywordArr =  explode(" ",$_REQUEST['r3']);
				}
				foreach ($KeywordArr as $Key=>$Value)
				{
					if(trim($Value) !="")
						$WherePart .="(ProductName LIKE '%$Value%' OR GoogleMPN LIKE '%$Value%' OR QuickBooksCode LIKE '%$Value%' OR ProductLine LIKE '%$Value%' OR LargeDescription LIKE '%$Value%' OR ModelNo LIKE '%$Value%') $Prcident ";
				}			
				$WherePart .=" $SecGate)";				
			}
			
			if($_REQUEST['r4']=="Active")
				$WherePart .=" AND p.Active=1";
			else if($_REQUEST['r4']=="Inactive")
				$WherePart .=" AND p.Active=0";
		endif;
				
		$ProductObj = new DataTable(TABLE_PRODUCT. " p, ".TABLE_PRODUCT_RELATION. " ptc,".TABLE_MANUFACTURERS." m");
		$ProductObj->Where = "p.MNID = m.MNID AND ptc.ProductID=p.ProductID and ptc.RelationType='category'".$WherePart." GROUP BY p.ProductID";
		$ProductObj->AllowPaging = true;
		$ProductObj->PageSize=50;
		$ProductObj->PageNo =$Pg;
		//$ProductObj->DisplayQuery=true;
		$ProductObj->TableSelectAll(array("distinct p.ProductID","p.ProductName","p.ModelNo","p.Stock","p.MinStockLevel","p.Active"),(isset($_REQUEST['r5']) && $_REQUEST['r5'] != "" ? $_REQUEST['r5']:"p.QuickBooksCode"));
		$TotalRecords = $ProductObj->TotalRecords ;
		$TotalPages =  $ProductObj->TotalPages;	
	?>
	<br>
		<?
		if($ProductObj->GetNumRows() > 0)
		{
		if($TotalRecords > $ProductObj->PageSize)
			{
			?>			
			<div class="paging"><b><?=$TotalRecords?> results found</b>&nbsp;&nbsp;<?=$ProductObj->GetPagingLinks("index.php?Page=".$Page."&Section=".$Section."&r1=".@$_REQUEST['r1']."&r2=".@$_REQUEST['r2']."&r3=".@$_REQUEST['r3']."&r4=".@$_REQUEST['r4']."&r5=".@$_REQUEST['r5']."&Submit=".@$_REQUEST['Submit']."&Pg=",PAGING_FORMAT_BOTH,"","");?></div>			
			<br>
			<?
			}
			?>
		<table border="0" cellpadding="5" cellspacing="1" width="100%" class="InsideTable">
			<tr class="InsideLeftTd">
				<td align="center" width="5%"><b>S.No.</b></td>
				<td align="left"><b>Product Name</b></td>
				<td align="center"><b>Status</b></td>
				<td align="center"><b>Stock</b></td>
				<td align="left"><b>Stock Limit</b></td>
				<td align="left"></td>
			</tr>
			<?
			$SNo=($ProductObj->PageSize * ($ProductObj->PageNo-1)) +1;
			$Count=1;
			
			while($CurrentProduct = $ProductObj->GetObjectFromRecord())
			{
				$StArr = GetProductPriceNStock($CurrentProduct->ProductID,1);
			?>
			<tr class="<?=$CurrentProduct->ProductID==$ProductID?"InsideLeftTd":"InsideRightTd";?>">
				<td align="center"><b><?=$SNo?></b></td>
				<td align="left">
					<b><?=MyStripSlashes($CurrentProduct->ProductName);?></b> (<?=MyStripSlashes($CurrentProduct->ModelNo);?>) - &pound;<?=isset($StArr['Price'])?number_format($StArr['Price'],2):"";?> 
					<br>
					<a href="<?=SKSEOURL($CurrentProduct->ProductID,"product")?>" target="_blank">Preview</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="<?=DIR_WS_SITE_CONTROL?>index.php?Page=product&Section=EditProduct&Pg=<?=$Pg?>&ParentID=<?=$ParentID?>&ProductID=<?=$CurrentProduct->ProductID?>">Edit</a>
					<input type="hidden" name="ProductID_<?=$Count?>" value="<?=$CurrentProduct->ProductID?>">
				</td>
				<td width="50" align="center"><?=$CurrentProduct->Active=="1"?"Enabled":"Disabled"?></td>
				<td width="50" align="center"><?=$CurrentProduct->Stock?></td>
				<td width="80" align="center"><?=$CurrentProduct->MinStockLevel?></td>
				<td>
					<a href="<?=DIR_WS_SITE_CONTROL?>index.php?Page=<?=$Page?>&Section=<?=$Section?>&r1=<?=@$_REQUEST['r1']?>&r2=<?=@$_REQUEST['r2']?>&r3=<?=@$_REQUEST['r3']?>&r4=<?=@$_REQUEST['r4']?>&r5=<?=@$_REQUEST['r5']?>&Submit=<?=@$_REQUEST['Submit']?>&Target=DeleteProduct&ProductID=<?=$CurrentProduct->ProductID?>" onclick="return FunctionDeleteProduct();"><b>Permanent Delete</b></a>
				</td>
			</tr>
			<?
			$SNo++;
			$Count++;
			}
		?>
			
		</table>
						
		<?
		if($TotalRecords > $ProductObj->PageSize)
			{
			?>
			<br>
			<div class="paging"><b><?=$TotalRecords?> results found</b>&nbsp;&nbsp;<?=$ProductObj->GetPagingLinks("index.php?Page=".$Page."&Section=".$Section."&r1=".@$_REQUEST['r1']."&r2=".@$_REQUEST['r2']."&r3=".@$_REQUEST['r3']."&r4=".@$_REQUEST['r4']."&r5=".@$_REQUEST['r5']."&Submit=".@$_REQUEST['Submit']."&Pg=",PAGING_FORMAT_BOTH,"","");?></div>			
			<br>
			<?
			}
		}
		else 
		{
			?>
			<b>No Result Found.</b>
			<?
		}
	break;
	
	
	}
///// Section end?>