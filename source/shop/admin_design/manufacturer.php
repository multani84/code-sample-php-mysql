<div class="row-fluid">
	<div class="span12">
		<h1>Manufacturers</h1>
		<hr />
	</div>
	<div class="pull-right">
		<a href="<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php echo $Page?>&Section=AddManufacturer" class="btn btn-primary">Add Manufacturer <icon class="icon-plus-sign icon-white-t"></icon></a>
	</div>
<div class="clearfix separator bottom"></div>
<?php 
//// Section start
switch($Section)
{
	case "AddManufacturer" :
	case "EditManufacturer" :
	/////// Add Manufacturer  Start
	?>
	<form id="f3233" name="f3233" method="POST" action="<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php echo $Page?>&Section=<?php echo $Section?>&Target=AddManufacturer&MNID=<?php echo $MNID?>" enctype="multipart/form-data">
	<table class="table table-striped table-bordered table-responsive block">
			<tr>
				<td align="center" class="InsideLeftTd"><b>Manufacturer Name</b></td>
				<td align="left" class="InsideRightTd"><input type="text" name="MNName" value="<?php echo isset($CurrentManufacturer->MNName)?MyStripSlashes($CurrentManufacturer->MNName):""?>" size="30"></td>
			</tr>
			<?php 
			if(@constant("DEFINE_SEO_URL_ACTIVE") =="1")
			{
			?>
			<tr>
				<td align="center" class="InsideLeftTd"><b>Manufacturer URL</b>
				</td>
				<td align="left" class="InsideRightTd">
				<input type="text" name="URLName" value="<?php echo isset($_POST['URLName'])?MyStripSlashes($_POST['URLName']):(isset($CurrentManufacturer->URLName)?MyStripSlashes($CurrentManufacturer->URLName):"")?>" size="70" onblur="return CheckSpecialSymbol(this);" title="Please do not use special symbol. Only use alfa numeric characters,(_) underscores or (-) dashes.">
				<br><small>Please do not use special symbol. 
				<br>Only use alfa numeric characters,(_) underscores or (-) dashes.</small>
				</td>
			</tr>
			<?php 
			}?>
			<?php 
			if(@constant("DEFINE_SEO_META_ACTIVE") =="1")
			{
			?>
			<tr>
				<td align="center" colspan="2"><hr></td>
			</tr>
			<tr>
				<td align="center" class="InsideLeftTd"><b>Meta Title</b></td>
				<td align="left" class="InsideRightTd"><input type="text" name="MetaTitle" value="<?php echo isset($_POST['MetaTitle'])?MyStripSlashes($_POST['MetaTitle']):(isset($CurrentManufacturer->MetaTitle)?MyStripSlashes($CurrentManufacturer->MetaTitle):"")?>" size="100"></td>
			</tr>
			<tr>
				<td align="center" class="InsideLeftTd"><b>Meta Keywords</b></td>
				<td align="left" class="InsideRightTd">
				 	<table border="0" width="100%" cellpadding="0" cellspacing="0">
				 	<tr>
					 	<td align="left" valign="top"><textarea name="MetaKeyword" id="MetaKeyword" cols="60" rows="6"><?php echo isset($_POST['MetaKeyword'])?MyStripSlashes($_POST['MetaKeyword']):(isset($CurrentManufacturer->MetaKeyword)?MyStripSlashes($CurrentManufacturer->MetaKeyword):"")?></textarea></td>
					 	<td align="left">
					 		<div style="height:200px;overflow: auto;padding:3px;display:block">
					 		<script type="text/javascript">
					 			function SKKeywordSelection(ckPt,val,ObjID)
								{
									EleObj = document.getElementById(ObjID)
									Str = EleObj.value;
									LStr = EleObj.value;
									if(ckPt)
									{
										LastIndex = LStr.substring(LStr.length-1,LStr.length)
										
										if(Str.indexOf(val) > 0)
											Str = Str
										else
										{
											Str = Str + val + ",";
										}
									}
									else
									{
										Str = Str.replace(val+",",'');
										Str = Str.replace(val,'');
									}
										 
									EleObj.value = Str;
									
								}
					 		</script>
					 			<table width="100%" cellpadding="0" cellspacing="0" class="InsideTable">
					 			<?php 
					 			$KeywordObj = new DataTable(TABLE_KEYWORDS);
								$KeywordObj->TableSelectAll("","Keyword ASC");
								$SNo =1;
								while ($CurrentKeyword = $KeywordObj->GetObjectFromRecord())
								{
					 			?>
					 				<tr>
					 					<td><input type="checkbox" name="ChkAdd[]" id="ChkAdd_<?php echo $SNo?>" class="chk" value="<?php echo trim($CurrentKeyword->Keyword)?>" onclick="return SKKeywordSelection(this.checked,this.value,'MetaKeyword');" ></td>
					 					<td><?php echo $CurrentKeyword->Keyword?></td>
					 				</tr>
					 			<?php 
					 			$SNo++;
								}?>	
					 			</table>
					 		</div>
					 	</td>
					 	</tr>
				 	</table>
				 </td>
			</tr>
			<tr>
				<td align="center" class="InsideLeftTd"><b>Meta Description</b></td>
				<td align="left" class="InsideRightTd"><textarea name="MetaDescription" cols="60" rows="3"><?php echo isset($_POST['MetaDescription'])?MyStripSlashes($_POST['MetaDescription']):(isset($CurrentManufacturer->MetaDescription)?MyStripSlashes($CurrentManufacturer->MetaDescription):"")?></textarea></td>
			</tr>
			<?php 
			}?>
			<tr>
				<td align="center" colspan="2"><hr></td>
			</tr>									
			
			<?php /*
			<tr>
				<td align="center" class="InsideLeftTd"><b>Address1</b></td>
				<td align="left" class="InsideRightTd"><input type="text" name="MNAddress1" value="<?php echo isset($CurrentManufacturer->MNAddress1)?MyStripSlashes($CurrentManufacturer->MNAddress1):""?>" size="30"></td>
			</tr>
			<tr>
				<td align="center" class="InsideLeftTd"><b>Address2</b></td>
				<td align="left" class="InsideRightTd"><input type="text" name="MNAddress2" value="<?php echo isset($CurrentManufacturer->MNAddress2)?MyStripSlashes($CurrentManufacturer->MNAddress2):""?>" size="30"></td>
			</tr>
			<tr>
				<td align="center" class="InsideLeftTd"><b>City</b></td>
				<td align="left" class="InsideRightTd"><input type="text" name="MNCity" value="<?php echo isset($CurrentManufacturer->MNCity)?MyStripSlashes($CurrentManufacturer->MNCity):""?>" size="30"></td>
			</tr>
			<tr>
				<td align="center" class="InsideLeftTd"><b>State</b></td>
				<td align="left" class="InsideRightTd"><input type="text" name="MNState" value="<?php echo isset($CurrentManufacturer->MNState)?MyStripSlashes($CurrentManufacturer->MNState):""?>" size="30"></td>
			</tr>
			<tr>
				<td align="center" class="InsideLeftTd"><b>Country</b></td>
				<td align="left" class="InsideRightTd">
				<select name="MNCountry" id="MNCountry" title="Please select Country." style="width:145px;">
					<option value="" selected>-- Select Country --</option>
					<?php 
						$CountryObj = new DataTable(TABLE_COUNTRIES);
						$CountryObj->TableSelectAll("","CountryName ASC");
						while ($CountryTemp = $CountryObj->GetObjectFromRecord()) 
						{
					?>		<option value="<?php echo $CountryTemp->CountryName?>" <?php echo (@$CurrentManufacturer->MNCountry==$CountryTemp->CountryName)?"selected":""?>><?php echo $CountryTemp->CountryName?></option>
					<?php 	}
					?>
		  		</select>
				</td>
			</tr>
			<tr>
				<td align="center" class="InsideLeftTd"><b>Zip/Post Code</b></td>
				<td align="left" class="InsideRightTd"><input type="text" name="MNZipCode" value="<?php echo isset($CurrentManufacturer->MNZipCode)?MyStripSlashes($CurrentManufacturer->MNZipCode):""?>" size="30"></td>
			</tr>
			<tr>
				<td align="center" class="InsideLeftTd"><b>Phone</b></td>
				<td align="left" class="InsideRightTd"><input type="text" name="MNPhone" value="<?php echo isset($CurrentManufacturer->MNPhone)?MyStripSlashes($CurrentManufacturer->MNPhone):""?>" size="30"></td>
			</tr>
			<tr>
				<td align="center" class="InsideLeftTd"><b>Fax</b></td>
				<td align="left" class="InsideRightTd"><input type="text" name="MNFax" value="<?php echo isset($CurrentManufacturer->MNFax)?MyStripSlashes($CurrentManufacturer->MNFax):""?>" size="30"></td>
			</tr>
			<tr>
				<td align="center" class="InsideLeftTd"><b>Website</b></td>
				<td align="left" class="InsideRightTd"><input type="text" name="MNWebsite" value="<?php echo isset($CurrentManufacturer->MNWebsite)?MyStripSlashes($CurrentManufacturer->MNWebsite):""?>" size="60"></td>
			</tr>
			*/?>
			<tr>
				<td align="center" class="InsideLeftTd"><b>Description</b></td>
				<td align="left" >
				<?php 
					$sContent = isset($_POST['TmpDescription'])?MyStripSlashes($_POST['TmpDescription'],true):(isset($CurrentManufacturer->Description)?MyStripSlashes($CurrentManufacturer->Description,true):"");
					$SKEditorObj->CArray = array("Width"=>"750",
												 "Height"=>"350",
												 "DMode"=>"Large",
												 "Help"=>true,
												 "EditorArray"=>array("css"=>DIR_WS_SITE_CONTROL."style.css",
												 					  ),
												 ); 
						$SKEditorObj->CreateEditor("TmpDescription",$sContent);?>				
							</td>
			</tr>
			<tr>
				<td align="center" class="InsideLeftTd"><b>Upload Image</b></td>
				<td align="left" class="InsideRightTd"><input type="file" name="Upload">
				<?php 
				if(isset($CurrentManufacturer->Image) && $CurrentManufacturer->Image !="" && file_exists(DIR_FS_SITE_UPLOADS_OTHER_ORIGINAL.$CurrentManufacturer->Image))
				{?>
					<a href="<?php echo DIR_WS_SITE_UPLOADS_OTHER_ORIGINAL.$CurrentManufacturer->Image?>" target="_blank">
						<?php SKImgDisplay(DIR_FS_SITE_UPLOADS_OTHER_ORIGINAL.$CurrentManufacturer->Image,70,'&nbsp;');?>
						<br>
						View Enlarge
					</a>
			  <?php }?>
				</td>
			</tr>
			<!--<tr>
				<td align="center" class="InsideLeftTd"><b>Banner</b></td>
				<td align="left" class="InsideRightTd">
					<select name="UpperBanner1[]" id="UpperBanner1" size="10" multiple>
					<option value="">--Select--</option>
					<?php 
						$BannerObj->Where = "Alignment='BR'";
						$BannerObj->TableSelectAll("","Position ASC");
		
					while($CurrentBanner = $BannerObj->GetObjectFromRecord())
					{?>
						<option value="<?php echo $CurrentBanner->BannerID?>" <?php echo (isset($_POST['UpperBanner1']) && in_array($CurrentBanner->BannerID,$_POST['UpperBanner1']))?"selected":((isset($CurrentManufacturer->UpperBanner1) && in_array($CurrentBanner->BannerID,explode(",",$CurrentManufacturer->UpperBanner1)))?"selected":"")?>><?php echo $CurrentBanner->BannerName?></option>
						<?php 
					}?>
					</select>
				</td>
			</tr>-->
			<?php  
			if(@constant("DEFINE_MANUFACTURER_TEMPLATE")=="1")
			{
				$Template = ((isset($_POST['Template']) && $_POST['Template'] !="")?$_POST['Template']:((isset($CurrentManufacturer->Template) && $CurrentManufacturer->Template !="")?$CurrentManufacturer->Template:DEFINE_MANUFACTURER_TEMPLATE_DEFAULT));
				?>
			<tr>
				<td align="center" class="InsideLeftTd"><b>Template</b></td>
				<td align="left" class="InsideRightTd">
					<select name="Template">														
					<?php foreach (SKGetTemplates('manufacturer') as $k=>$v)
					{?>
					<option value="<?php echo $k?>" <?php echo $Template==$k?"selected":""?>><?php echo $v?></option>
					<?php 
					}?>
					</select>
				</td>
			</tr>
			<?php 
			}?>
			<tr>
				<td align="center" class="InsideLeftTd"></td>
				<td align="left" class="InsideRightTd">
				<input type="submit" name="AddManufacturer" value="Submit">
				</td>
			</tr>
		</table>
	</form>
	<?php 
	/////// Add Manufacturer End
	break;

	/////////Display Manufacturer 
	case "DisplayManufacturer" :
	default:
		$ManufacturerObj->Where = "1";
		$ManufacturerObj->TableSelectAll("","MNName ASC");
		if($ManufacturerObj->GetNumRows() > 0)
		{
		?>
		<form method="POST" action="<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php echo $Page?>&Section=<?php echo $Section?>&Target=UpdateManufacturer">
	<table class="table table-striped table-bordered table-responsive block">
			<thead>
			<tr>
				<th align="center" width="5%" class="InsideLeftTd">S.No.</b></th>
				<th align="left" class="InsideLeftTd">Name</th>
				<?php /*
				<th align="left" class="InsideLeftTd">Image</th>
				<th align="left" class="InsideLeftTd">Website Name</th>
				*/?>
				<th align="center" class="InsideLeftTd">Edit</th>
				<th align="center" class="InsideLeftTd">Delete</th>
			</tr></thead>
			<tbody>
			<script type="text/javascript">
				function FunctionDeleteManufacturer(MNID)
				{
						result = confirm("Are you sure want to delete the manufacturer?")
						if(result == true)
						{
							URL="<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php echo $Page?>&Target=DeleteManufacturer&MNID=" + MNID;
							location.href=URL;
							return true;
						}
						return false;
				}				
			</script>				
			</script>
			<?php 
			$SNo=1;
			$Count=1;
			while($CurrentManufacturer = $ManufacturerObj->GetObjectFromRecord())
			{
					?>
			<tr class="InsideRightTd">
				<td align="center"><b><?php echo $SNo?></b></td>
				<td align="left">
					<b><?php echo MyStripSlashes($CurrentManufacturer->MNName)?></b>
					<a href="<?php  echo SKSEOURL($CurrentManufacturer->MNID,"shop/brand")?>" target="_blank">Preview</a>
				</td>
				<?php /*
				<td align="left">
				<?php 
				if($CurrentManufacturer->Image !="" && file_exists(DIR_FS_SITE_UPLOADS_OTHER_ORIGINAL.$CurrentManufacturer->Image))
				{?>
					<a href="<?php echo DIR_WS_SITE_UPLOADS_OTHER_ORIGINAL.$CurrentManufacturer->Image?>" target="_blank">
						<?php SKImgDisplay(DIR_FS_SITE_UPLOADS_OTHER_ORIGINAL.$CurrentManufacturer->Image,70,'&nbsp;');?>
						<br>
						View Enlarge
					</a>
			  <?php }?>
				</td>
				
				
				<td align="left"><a href="<?php echo $CurrentManufacturer->MNWebsite?>" target="_blank"><?php echo $CurrentManufacturer->MNWebsite?></a></td>
				*/?>
				<td align="center"><a href="<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php echo $Page?>&Section=EditManufacturer&MNID=<?php echo $CurrentManufacturer->MNID?>" class="btn btn-success btn-phone-block"><icon class="icon-pencil icon-white"></icon> Edit</a></td>
				<td align="center"><a href="javascript:;" onclick="return FunctionDeleteManufacturer('<?php echo $CurrentManufacturer->MNID?>');" class="btn btn-danger btn-phone-block"><icon class="icon-remove icon-white"></icon> Delete</a></td>
			</tr>
			<?php 
			$SNo++;
			$Count++;
			}
		?>
		</table>
		</form>						
		<?php 
		}
		else 
		{
			?>
			<b>No Result Found.</b>
			<?php 
		}
	break;
}
///// Section end?>