<!-- Heading  -->
<div class="row-fluid">
	<div class="span12">
		<h1>Orders</h1>
		<hr />
	</div>
</div>
<!--  End Heading -->
<?php
switch($Section)
{
	case "Invoice":
		@ob_clean();
		$InvoiceSent = true;
		$NotMailSent = true;
		?>
		<html>
		<head>
			<style type="text/css">
			.InvoiceStart{page-break-after: always}
			</style>
		</head>
		
		<body onload22="print();">
		<?php
		if(isset($_POST['OrderIDArr']) && $_POST['OrderIDArr'] !="" && is_array($_POST['OrderIDArr']))
		{
			foreach ($_POST['OrderIDArr'] as $k=>$OrderID)
			{
				include(dirname(__FILE__)."/../invoice.php");
			}
		}
		elseif(isset($_GET['OrderID']) && $_GET['OrderID'] !="")
		{
			$OrderID = $_GET['OrderID'];
			include(dirname(__FILE__)."/../invoice.php");
		}
		?>
		</body></html>
		<?php
		exit;
	break;
	case "FullDetail":
	
		$OrderID =isset($_GET['OrderID'])?$_GET['OrderID']:"";
		$OrderObj->Where ="OrderID = '".$OrderID."'";
		$CurrentOrder = $OrderObj->TableSelectOne(array("*, DATE_FORMAT(CreatedDate,'%M %d, %Y %r')as MyCreatedDate, DATE_FORMAT(CreatedDate,'%M %d, %Y')as MyShortCreatedDate, DATE_FORMAT(CreatedDate,'%y%m%d')as FormattedCreatedDate"));
		$CurrencyOrderID = $CurrentOrder->OrderID;		
		
		$CreditCardObj = new DataTable(TABLE_CREDIT_CARD);
		$CreditCardObj->Where = $OrderObj->Where;
		$CurrentCreditCard = $CreditCardObj->TableSelectOne();
		
		
		$ShoppingCalculationObj->OrderID = $OrderID;
		$ShoppingCalculationObj->Calculate();
			
		$SKCartTable =TABLE_ORDER_DETAILS;
		$CartObj = new DataTable(TABLE_ORDER_DETAILS);
		$CartObj2 = new DataTable(TABLE_ORDER_DETAILS);

		$CartObj->Where =" OrderID='".$CartObj->MysqlEscapeString($OrderID)."'";
		$CartObj->TableSelectAll(array("*"));

					
		$CartObj->Where =" OrderID='".$CartObj->MysqlEscapeString($OrderID)."' AND (ParentItemID='' OR ISNULL(ParentItemID))";
		$CartObj->TableSelectAll("","OrderDetailID ASC");
					
		?>
		<!--  Tabs -->
		<div class="span3">
			<ul class="tabs-arrow">
				<li class="active"><a class="glyphicons list" href="#OrderSummary" data-toggle="tab"><i></i> 1. Order Details</a> </li>
				<li class=""><a class="glyphicons pencil" href="#OrderStatus" data-toggle="tab"><i></i> 2. Status/notes</a></li>
			</ul>
			<?php if(isset($CurrentOrder->OrderStatus) && $CurrentOrder->OrderStatus =='Paid'):?>
		<a href="index.php?Page=<?php echo $Page?>&Section=Invoice&OrderID=<?php echo $CurrentOrder->OrderID?>" target="_blank" class="btn btn-primary btn-block">Print Invoice <icon class="icon-print icon-white-t"></icon></a>
		<?php endif;?>
		<div class="clearfix"></div><br/>
		</div>
		<!-- End Tabs -->
	
	<!--  Tab Content -->
	<div class="span8">
		<div class="tab-content">	
			<!--  Tab Content Block -->
			<div class="tab-pane active" id="OrderSummary">
				<h3>Order Summary</h3>
				<?php require_once(dirname(__FILE__)."/../order_summary.php");?>
			</div>	
			<!--  End Tab Content Block -->
			
			<!--  Tab Content Block -->
			<div class="tab-pane" id="OrderStatus">
				<h3>Order Status</h3>
				<?php
					if(@constant("DEFINE_SHIPPING_INFO_DISPLAY")=="1")
					{
					?>
					<br/>
					<table>
						<?php
						$OrderChangeStatusObj = new DataTable(TABLE_ORDER_CHANGE_STATUS);
						$OrderChangeStatusObj->Where = $OrderObj->Where;
						$OrderChangeStatusObj->TableSelectAll(array("*,DATE_FORMAT(CreatedDate,'%M %d, %Y')as MyCreatedDate"),"CreatedDate ASC");
						if($OrderChangeStatusObj->GetNumRows() > 0)
						{
						?>
						<tr>
							<td>
								 <table class="table table-striped table-bordered table-hover">
									<thead>
									<tr class="InsideLeftTd">
										<th  height="25">Status</th>
										<th>Comments</th>
										<th width="50"><b>Notify</b></th>
										<th width="110"><b>Created Date</b></th>								
									</tr>
									</thead>
									<tbody>
									<?php
									while ($CurrentOrderStatus = $OrderChangeStatusObj->GetObjectFromRecord())
									{?>
									<tr class="InsideRightTd">
										<td><?php echo MyStripSlashes($CurrentOrderStatus->OrderStatus)?></td>
										<td><?php echo MyStripSlashes(nl2br($CurrentOrderStatus->Comments))?></td>
										<td align="center"><?php echo $CurrentOrderStatus->Notify == 1 ? "Yes":"No"?></td>
										<td><?php echo MyStripSlashes($CurrentOrderStatus->MyCreatedDate)?></td>								
									</tr>
									<?php
									}?>
									</tbody>
								</table>
							</td>
						</tr>
						<?php
						}?>
						<tr>
							<td>
								<!------Order Status Form-------->
								<?php if($CurrentOrder->ShippingStatus != $ShippingStatusArray['Cancelled'])
									{ ?>		
									<form method="POST" action="<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php echo $Page?>&Section=<?php echo $Section?>&Target=UpdateOrderStatus&OrderID=<?php echo $OrderID?>">
										 <table class="table table-striped table-bordered table-hover">
											<tr class="InsideLeftTd">
												<td><b>Order Status</b></td>
												<td><b>Comments</b></td>
												<td><b>Notify Customer</b></td>
												<td></td>								
											</tr>
											<tr>
												<td height="2">
												<select name="OrderStatus">
													<?php
													foreach ($OrderStatusArray as $key=>$val)
													{
														?>
														<option value="<?php echo $key?>" <?php echo $CurrentOrder->OrderStatus == $key?"selected":""?>><?php echo $val?></option>
														<?php
													}?>
												</select></td>
												<td>
												<textarea name="Comments" rows="3" cols="30"></textarea></td>
												<td align="center"><input type="checkbox" value="1" name="Notify"></td>
												<td><input class="btn btn-primary" type="submit" name="Submit" value="Submit" onclick="return FunctionChangeStatus();"></td>								
											</tr>
										</table>
									</form>	
								<?php } ?>
								<script type="text/javascript">
									function FunctionChangeStatus()
									{
										result = confirm("You have changed the status of the order.")
										if(result == true)
										{
											return true;
										}
										return false;
									}				
								</script>
								<!------Order Status Form end-------->
							</td>
						</tr>
					</table>
				<?php }
				?>
			</div>	
			<!--  End Tab Content Block -->
			
			
		</div>
	</div>
		
	<!--  End Tab Content -->
				
		<?php	
	break;	
	case "CreateCreditMemo":
		$OrderID =isset($_GET['OrderID'])?$_GET['OrderID']:"";
		$OrderObj->Where ="OrderID = '".$OrderID."'";
		$CurrentOrder = $OrderObj->TableSelectOne(array("*, DATE_FORMAT(CreatedDate,'%M %d, %Y %r')as MyCreatedDate, DATE_FORMAT(CreatedDate,'%M %d, %Y')as MyShortCreatedDate, DATE_FORMAT(CreatedDate,'%y%m%d')as FormattedCreatedDate"));
		$CurrencyOrderID = $CurrentOrder->OrderID;		
		
		$CreditCardObj = new DataTable(TABLE_CREDIT_CARD);
		$CreditCardObj->Where = $OrderObj->Where;
		$CurrentCreditCard = $CreditCardObj->TableSelectOne();
		?>
	<a name="Top"></a>
	<table border="0" cellpadding="3" cellspacing="1" width="100%" class="InsideTable">
		<tr>
			<td align="center">
				<table cellpadding="3" cellspacing="1" width="93%" border="0">
					<tr>
						<td align="right"><a href="index.php?Page=<?php echo $Page?>&Section=FullDetail&OrderID=<?php echo $CurrentOrder->OrderID?>">&laquo; Back to Order Details</a></td>
					</tr>
				</table>
				<table cellpadding="3" cellspacing="1" width="93%" border="0" class="InsideTable">
					<tr>
						<td align="right" width="25%" class="InsideRightTd"><b>Order No</b></td>
						<td><?php echo $CurrentOrder->OrderNo?></td>
						<td align="right"  width="25%"  class="InsideRightTd"><b>Order Date</b></td>
						<td><?php echo $CurrentOrder->MyCreatedDate?></td>
					</tr>
					<tr >
						<td align="right" width="25%"  class="InsideRightTd"><b>PaymentMethod</b></td>
						<td width="25%" ><?php echo MyStripSlashes($CurrentOrder->PaymentMethod)?></td>
						<td align="right"  width="25%"  class="InsideRightTd"><b>Email</b></td>
						<td><a href="mailto:<?php echo MyStripSlashes($CurrentOrder->Email)?>"><?php echo MyStripSlashes($CurrentOrder->Email)?></a></td>
					</tr>
					<tr>
						<td align="right" width="25%" valign="top"  class="InsideRightTd"><b>Billing Address</b></td>
						<td width="25%" valign="top" <?php echo @constant("SHIPPING_INFO_DISPLAY")=="1"?"":"colspan=3";?>>
						<b><?php echo MyStripSlashes($CurrentOrder->BillingFirstName)."&nbsp;&nbsp;".MyStripSlashes($CurrentOrder->BillingLastName)?></b><br>
						<?php echo MyStripSlashes($CurrentOrder->BillingAddress1)."&nbsp;&nbsp;".MyStripSlashes($CurrentOrder->BillingAddress2)?><br>
						<?php echo MyStripSlashes($CurrentOrder->BillingCity)."&nbsp;&nbsp;".MyStripSlashes($CurrentOrder->BillingState)?><br>
						<?php echo MyStripSlashes($CurrentOrder->BillingArea)."&nbsp;&nbsp;".MyStripSlashes($CurrentOrder->BillingCountry)."&nbsp;&nbsp;".MyStripSlashes($CurrentOrder->BillingZip)?><br>
						Phone : <?php echo MyStripSlashes($CurrentOrder->BillingPhone)."&nbsp;&nbsp;Fax :".MyStripSlashes($CurrentOrder->BillingFax)?><br>
						</td>
						<?php
						if(@constant("SHIPPING_INFO_DISPLAY")=="1")
						{
						?>
			
						<td align="right" width="25%" valign="top"  class="InsideRightTd"><b>Shipping Address</b></td>
						<td width="25%" valign="top">
						<b><?php echo MyStripSlashes($CurrentOrder->ShippingFirstName)."&nbsp;&nbsp;".MyStripSlashes($CurrentOrder->ShippingLastName)?></b><br>
						<?php echo MyStripSlashes($CurrentOrder->ShippingAddress1)."&nbsp;&nbsp;".MyStripSlashes($CurrentOrder->ShippingAddress2)?><br>
						<?php echo MyStripSlashes($CurrentOrder->ShippingCity)."&nbsp;&nbsp;".MyStripSlashes($CurrentOrder->ShippingState)?><br>
						<?php echo MyStripSlashes($CurrentOrder->ShippingArea)."&nbsp;&nbsp;".MyStripSlashes($CurrentOrder->ShippingCountry)."&nbsp;&nbsp;".MyStripSlashes($CurrentOrder->ShippingZip)?><br>
						Phone : <?php echo MyStripSlashes($CurrentOrder->ShippingPhone)."&nbsp;&nbsp;Fax :".MyStripSlashes($CurrentOrder->ShippingFax)?><br>
						</td>
						<?php
						}?>
					</tr>
					<tr>
						<td align="right" width="25%"  class="InsideRightTd"><b>Payment Status</b></td>
						<td width="25%" <?php echo @constant("SHIPPING_INFO_DISPLAY")=="1"?"":"colspan=3";?>><?php echo MyStripSlashes($CurrentOrder->PaymentStatus)?></td>
						<?php
						if(@constant("SHIPPING_INFO_DISPLAY")=="1")
						{
						?>
						<td align="right"  width="25%"  class="InsideRightTd"><b>Shipping Status</b></td>
						<td><?php echo MyStripSlashes($CurrentOrder->ShippingStatus)?></td>
						<?php
						}?>
					</tr>
					<?php
					if($CurrentOrder->VoucherCode !="")
					{?>
					<tr>
						<td align="right" width="25%"  class="InsideRightTd"><b>Voucher Code</b></td>
						<td width="25%"><?php echo MyStripSlashes($CurrentOrder->VoucherCode)?></td>
						<td align="right"  width="25%"  class="InsideRightTd"><b>Voucher Text</b></td>
						<td><?php echo MyStripSlashes($CurrentOrder->VoucherText)?></td>
					</tr>
					<?php
					}?>
					<tr>
						<td align="right"  width="25%"  class="InsideRightTd"><b>Comments</b></td>
						<td colspan="3"><?php echo MyStripSlashes(nl2br($CurrentOrder->Comments))?></td>
					</tr>
					</table>
					<br>
					<?php
					if(isset($CurrentCreditCard->CardDisplay) && $CurrentCreditCard->CardDisplay=="1")
					{?>
					<table cellpadding="3" cellspacing="1" width="93%" border="0" class="InsideTable">
							</tr>
								<td class="InsideLeftTd"><b>Credit Cart Details.</b></td>
							</tr>
					</table>							
					<table cellpadding="3" cellspacing="1" width="93%" border="0" class="InsideTable">
						<tr  class="InsideRightTd">
							<td align="center" width="25%" valign="top"><b>Card Name:</b>&nbsp;</td>
							<td align="left" valign="top"><?php echo $CurrentCreditCard->CardName?></td>
						</tr>
						<tr  class="InsideRightTd">
							<td align="center" width="25%" valign="top"><b>Card Number:</b>&nbsp;</td>
							<td align="left" valign="top"><?php echo $CurrentCreditCard->CardNo?></td>
						</tr>
						<tr  class="InsideRightTd">
							<td align="center" width="25%" valign="top"><b>Card Type:</b>&nbsp;</td>
							<td align="left" valign="top"><?php echo $CurrentCreditCard->CardType?></td>
						</tr>
						<tr  class="InsideRightTd">
							<td align="center" width="25%" valign="top"><b>Expiry Date(MM/YY):</b>&nbsp;</td>
							<td align="left" valign="top"><?php echo $CurrentCreditCard->ExpiryMonth?>&nbsp;/&nbsp;<?php echo $CurrentCreditCard->ExpiryYear?></td>
						</tr>
						<tr  class="InsideRightTd">
							<td align="center" width="25%" valign="top"><b>CVV:</b>&nbsp;</td>
							<td align="left" valign="top"><?php echo $CurrentCreditCard->CVV?></td>
						</tr>
						<?php
						if($CurrentCreditCard->StartMonth !="" && $CurrentCreditCard->StartMonth !="MM")
						{
						?>
						<tr  class="InsideRightTd">
							<td align="center" width="25%" valign="top"><b>Start Date(MM/YY):</b>&nbsp;</td>
							<td align="left" valign="top"><?php echo $CurrentCreditCard->StartMonth?>&nbsp;/&nbsp;<?php echo $CurrentCreditCard->StartYear?></td>
						</tr>
						<?php
						}?>
					</table>
					<br>
					<?php
					}?>
						<table cellpadding="3" cellspacing="1" width="93%" border="0" class="InsideTable">
							</tr>
								<td class="InsideLeftTd"><b>Shopping Cart Details.</b></td>
							</tr>
						</table>	
						<table cellpadding="3" cellspacing="1" width="93%" border="0" class="InsideTable">
							<tr>
							<td>
								<?php
									
									$ShoppingCalculationObj->OrderID = $OrderID;
									$ShoppingCalculationObj->Calculate();
									
									$SKCartTable =TABLE_ORDER_DETAILS;
									$CartObj = new DataTable(TABLE_ORDER_DETAILS);
									$CartObj2 = new DataTable(TABLE_ORDER_DETAILS);

									$CartObj->Where =" OrderID='".$CartObj->MysqlEscapeString($OrderID)."' AND (ParentItemID='' OR ISNULL(ParentItemID))";
									$CartObj->TableSelectAll("","OrderDetailID ASC");
									
									$ShowCreditMemoOptions = true;
									if ($CartObj->GetNumRows()>0):
										echo "<form method='POST' action='index.php?Page=$Page&Section=$Section&OrderID=$OrderID&Target=CreateMemo' id='CreateMemo'>";
											require_once(DIR_FS_SITE_INCLUDES."show_cart.php");
										echo "</form>";
									endif;									
								?>
								<script type="text/javascript">
									var totalRefundValue = 0;
									function CalculateRefund(){
										totalRefundValue = 0;
										$("#CreateMemo select").each(function(){
											totalRefundValue += parseInt($(this).val()) * parseFloat($(this).prev().val());
										});
										
										totalRefundValue += parseFloat($("#ShippingRefund").val());
										totalRefundValue += parseFloat($("#TaxRefund").val());
										totalRefundValue += parseFloat($("#RefundAdjustment").val());
										totalRefundValue -= parseFloat($("#AdjustmentFees").val());
										
										if(isNaN(totalRefundValue) || totalRefundValue == 0)
											$("#CreateMemoBtn").attr("disabled","disabled");
										else
											$("#CreateMemoBtn").removeAttr("disabled");
									}
									
									function UpdateRefund(){
										CalculateRefund();										
										if(isNaN(totalRefundValue))
											$("#TotalRefund").html("--");
										else
											$("#TotalRefund").html(totalRefundValue.toFixed(2));
									}
									
									function CheckRefund(){
										CalculateRefund();
										if(isNaN(totalRefundValue))
										{
											alert("Invalid amount!");
											return false;	
										}
										if(parseFloat($("#GrandTotal").val()) < totalRefundValue)
											return confirm("Total refund (�" + totalRefundValue + ") exceeds the total paid (�" + parseFloat($("#GrandTotal").val()) + ") value!\n\nAre you sure, you want to continue?");
										return confirm("�" + totalRefundValue + " will be refunded offline by you!\n\nAre you sure, you want to continue?");
									}
									
									$("#CreateMemo input").keyup(UpdateRefund);
									$("#CreateMemo select").change(UpdateRefund);
									$("#CreateMemo input,#CreateMemo select").blur(UpdateRefund);
								</script>
								<br>				
							</td>	
						</tr>
					</table>
		<?php	
	break;	
		
case "DisplayOnline":
	default:
	$PaymentStatus =isset($_GET['PaymentStatus'])?$_GET['PaymentStatus']:"";
	$OrderStatus = isset($_GET['OrderStatus'])?$_GET['OrderStatus']:"";
	$PaymentMethod=isset($_GET['PaymentMethod'])?$_GET['PaymentMethod']:"";
	$Attempted=isset($_GET['Attempted'])?$_GET['Attempted']:"";
	$OrderBy = isset($_GET['OrderBy'])?$_GET['OrderBy']:"OrderID DESC";
	
	$OrderObj->Where =" OrderStatus != 'Deleted' ";
	
	if($PaymentStatus !="")
			$OrderObj->Where .=" AND PaymentStatus = '$PaymentStatus' ";
	if($OrderStatus !="")
		$OrderObj->Where.=" AND OrderStatus='".$OrderStatus."' ";
	if($PaymentMethod !="")		
		$OrderObj->Where.=" AND PaymentMethod='".$PaymentMethod."'";
	if($Attempted !="")		
		$OrderObj->Where.=" AND Attempted='".$Attempted."'";
		

	$Keyword = @$_REQUEST['k'];
	$WherePart ="";
	if(!empty($Keyword))
	{
		$WherePart .=" AND (";
		if(preg_match("/\+\b/i",$Keyword))
		{
			$Prcident= "AND";
			$SecGate = "1";
			$KeywordArr =  explode("+",$Keyword);
		}
		else 
		{
			$Prcident= "OR";
			$SecGate = "0";
			$KeywordArr =  explode(" ",$Keyword);
		}
		foreach ($KeywordArr as $Key=>$Value)
		{
			if(trim($Value) !="")
				$WherePart .="(
						  	OrderNo LIKE '%$Value%' 
						  	OR BillingFirstName LIKE '%$Value%' 
						  	OR BillingLastName LIKE '%$Value%' 
						  	OR BillingAddress1 LIKE '%$Value%' 
						  	OR BillingAddress2 LIKE '%$Value%' 
						  	OR BillingCity LIKE '%$Value%' 
						  	OR BillingState LIKE '%$Value%' 
						  	OR BillingCountry LIKE '%$Value%' 
						  	OR BillingArea LIKE '%$Value%' 
						  	OR BillingZip LIKE '%$Value%' 
						  	OR BillingPhone LIKE '%$Value%' 
						  	OR BillingFax LIKE '%$Value%' 
						  	OR ShippingFirstName LIKE '%$Value%' 
						  	OR ShippingLastName LIKE '%$Value%' 
						  	OR ShippingAddress1 LIKE '%$Value%' 
						  	OR ShippingAddress2 LIKE '%$Value%' 
						  	OR ShippingCity LIKE '%$Value%' 
						  	OR ShippingState LIKE '%$Value%' 
						  	OR ShippingCountry LIKE '%$Value%' 
						  	OR ShippingArea LIKE '%$Value%' 
						  	OR ShippingZip LIKE '%$Value%' 
						  	OR ShippingPhone LIKE '%$Value%' 
						  	OR ShippingFax LIKE '%$Value%' 
						  	OR Email LIKE '%$Value%' 
						  	OR PaymentMethod LIKE '%$Value%' 
						  	OR VoucherCode LIKE '%$Value%' 
						  	OR TrackingCode LIKE '%$Value%' 
						  	OR DownloadID LIKE '%$Value%' 
						  	OR PartnerCode LIKE '%$Value%' 
						  ) $Prcident ";
    
		}
		
		$WherePart .=" $SecGate)";
		$OrderObj->Where .= $WherePart;
	}	
	
	$OrderObj->AllowPaging =true;
	$OrderObj->PageSize=25;
	$OrderObj->PageTotalDisplay = 10; 
	$OrderObj->PageNo =isset($_GET['PageNo'])?$_GET['PageNo']:1;	
	$OrderObj->TableSelectAll(array("*, DATE_FORMAT(CreatedDate,'%b %d, %Y')as MyCreatedDate, DATE_FORMAT(CreatedDate,'%d %M, %Y')as MyCreatedMonth"),$OrderBy);
	
	$TotalRecords = $OrderObj->TotalRecords ;
	$TotalPages =  $OrderObj->TotalPages;
	?>
	<div class="row-fluid">
	<div class="span12">

	
		<?php
		if($OrderObj->GetNumRows() >0)
		{
		?>
	
	<!-- Search -->
		<form class="form-horizontal form-search well" id="FrmFilter" method="GET" action="index.php">
			<legend>Search orders</legend>
			<input type="hidden" name="Page" value="<?php echo $Page?>" >
			<input type="hidden" name="Section" value="<?php echo $Section?>" >
			<input type="hidden" name="Attempted" value="<?php echo $Attempted?>" >
			Keyword&nbsp;<input type="text" name="k" value="<?php echo @$_GET['k']?>">
					&nbsp;&nbsp;&nbsp;&nbsp;
					<select name="PaymentStatus" class="span3">
						<option value="">Payment Status</option>
						<?php
						foreach ($PaymentStatusArray as $key=>$val)
						{
							?>
							<option value="<?php echo $key?>" <?php echo $PaymentStatus==$key?"selected":""?> ><?php echo $val?></option>
							<?php
						}?>					
					</select>
					&nbsp;&nbsp;&nbsp;&nbsp;
					<select name="OrderStatus" class="span3">
						<option value="">Order Status</option>
						<?php
						foreach ($OrderStatusArray as $key=>$val)
						{
							?>
							<option value="<?php echo $key?>" <?php echo $OrderStatus==$key?"selected":""?> ><?php echo $val?></option>
							<?php
						}?>					
					</select>
					&nbsp;&nbsp;&nbsp;&nbsp;
					<button type="submit" class="btn"><icon class="icon-search icon-brown"></icon> Search</button>
		</form>
	<!-- End Search -->
	<div class="clearfix"></div>
	<?php
	if($TotalRecords > $OrderObj->PageSize)
		{
		?>
		<table cellpadding="1" width="100%" cellspacing="1" border="0" class="InsideTable">
			<tr>
			<td width="100%">
			<?php echo $OrderObj->GetPagingLinks("index.php?Page=".$Page."&Attempted=".$Attempted."&PaymentMethod=".$PaymentMethod."&k=".$Keyword."&PaymentStatus=".$PaymentStatus."&OrderStatus=".$OrderStatus."&Target=".$Target."&OrderBy=".$OrderBy."&PageNo=",PAGING_FORMAT_LIST,"adm-link","<b>Page No : </b>");?>
			</td>	
			</tr>
		</table>
		<?php
		}
		?>
	<table class="table table-striped table-bordered table-responsive block">
			<tr>
				<td width="96%" align="left" height="30">
				<div style="float:left;"><b>Status:&nbsp;</b></div>
				<?php
				foreach ($OrderStatusArray as $key=>$val)
				{?>
				<div style="float:left;width:16px;height:16px;background-color:<?php echo $OrderStatusColorArray[$key]?>">&nbsp;</div>
				<div style="float:left;height:16px;">&nbsp;<b><?php echo $val?></b>&nbsp;</div>
				<?php
				}?>
				</td>
			</tr>
		</table>	
		<form id="InvoiceForm" action="index.php?Page=<?php echo $Page?>&Section=Invoice" target="_blank" method="POST">
		<table class="table table-bordered table-responsive block">
			<thead>
				<tr>
					<th width="2%" height="25" data-title="q"><input type="checkbox" name="ChkAll" value="1" onclick="if(this.checked){$('.ChkOrd').attr({checked:true});}else{$('.ChkOrd').attr({checked:false});};"></th>
					<th width="2%" height="25">S.No</th>
					<th width="10%" align="center">Order No</th>
					<th width="22%" align="center">Date
						<br><a href="index.php?Page=<?php echo $Page?>&PaymentMethod=<?php echo $PaymentMethod?>&Attempted=<?php echo $Attempted?>&k=<?php echo $Keyword?>&PaymentStatus=<?php echo $PaymentStatus?>&OrderStatus=<?php echo $OrderStatus?>&OrderBy=CreatedDate ASC" class="adm-link"><icon class="icon-arrow-up icon-gray"></icon></a>
						<a href="index.php?Page=<?php echo $Page?>&PaymentMethod=<?php echo $PaymentMethod?>&Attempted=<?php echo $Attempted?>&k=<?php echo $Keyword?>&PaymentStatus=<?php echo $PaymentStatus?>&OrderStatus=<?php echo $OrderStatus?>&OrderBy=CreatedDate DESC" class="adm-link"><icon class="icon-arrow-down icon-gray"></icon></a></th>
					<th width="22%">Customer Name
						<br><a href="index.php?Page=<?php echo $Page?>&PaymentMethod=<?php echo $PaymentMethod?>&Attempted=<?php echo $Attempted?>&k=<?php echo $Keyword?>&PaymentStatus=<?php echo $PaymentStatus?>&OrderStatus=<?php echo $OrderStatus?>&OrderBy=BillingFirstName ASC" class="adm-link"><icon class="icon-arrow-up icon-gray"></icon></a>
						<a href="index.php?Page=<?php echo $Page?>&PaymentMethod=<?php echo $PaymentMethod?>&Attempted=<?php echo $Attempted?>&k=<?php echo $Keyword?>&PaymentStatus=<?php echo $PaymentStatus?>&OrderStatus=<?php echo $OrderStatus?>&OrderBy=BillingFirstName DESC" class="adm-link"><icon class="icon-arrow-down icon-gray"></icon></a></th>
					<th>Member Details</th>
					<th>Partner Code</th>
					<th>PaymentStatus
					<br><a href="index.php?Page=<?php echo $Page?>&PaymentMethod=<?php echo $PaymentMethod?>&Attempted=<?php echo $Attempted?>&k=<?php echo $Keyword?>&OrderStatus=<?php echo $OrderStatus?>&OrderStatus=<?php echo $OrderStatus?>&OrderBy=PaymentStatus ASC" class="adm-link"><icon class="icon-arrow-up icon-gray"></icon></a>
					<a href="index.php?Page=<?php echo $Page?>&PaymentMethod=<?php echo $PaymentMethod?>&Attempted=<?php echo $Attempted?>&k=<?php echo $Keyword?>&PaymentStatus=<?php echo $PaymentStatus?>&OrderStatus=<?php echo $OrderStatus?>&OrderBy=PaymentStatus DESC" class="adm-link"><icon class="icon-arrow-down icon-gray"></icon></a>
					</th>
					<th>PaymentMethod
					<br><a href="index.php?Page=<?php echo $Page?>&PaymentMethod=<?php echo $PaymentMethod?>&Attempted=<?php echo $Attempted?>&k=<?php echo $Keyword?>&OrderStatus=<?php echo $OrderStatus?>&OrderStatus=<?php echo $OrderStatus?>&OrderBy=PaymentMethod ASC" class="adm-link"><icon class="icon-arrow-up icon-gray"></icon></a>
					<a href="index.php?Page=<?php echo $Page?>&PaymentMethod=<?php echo $PaymentMethod?>&Attempted=<?php echo $Attempted?>&k=<?php echo $Keyword?>&PaymentStatus=<?php echo $PaymentStatus?>&OrderStatus=<?php echo $OrderStatus?>&OrderBy=PaymentMethod DESC" class="adm-link"><icon class="icon-arrow-down icon-gray"></icon></a>
					</th>
					<th>OrderStatus
					<br><a href="index.php?Page=<?php echo $Page?>&PaymentMethod=<?php echo $PaymentMethod?>&Attempted=<?php echo $Attempted?>&k=<?php echo $Keyword?>&OrderStatus=<?php echo $OrderStatus?>&OrderStatus=<?php echo $OrderStatus?>&OrderBy=OrderStatus ASC" class="adm-link"><icon class="icon-arrow-up icon-gray"></icon></a>
					<a href="index.php?Page=<?php echo $Page?>&PaymentMethod=<?php echo $PaymentMethod?>&Attempted=<?php echo $Attempted?>&k=<?php echo $Keyword?>&PaymentStatus=<?php echo $PaymentStatus?>&OrderStatus=<?php echo $OrderStatus?>&OrderBy=OrderStatus DESC" class="adm-link"><icon class="icon-arrow-down icon-gray"></icon></a>
					</th>

					<th  class="header"  width="5%" align="center">Amount
						<br><a href="index.php?Page=<?php echo $Page?>&PaymentMethod=<?php echo $PaymentMethod?>&Attempted=<?php echo $Attempted?>&k=<?php echo $Keyword?>&PaymentStatus=<?php echo $PaymentStatus?>&OrderStatus=<?php echo $OrderStatus?>&OrderBy=GrandTotal ASC" class="adm-link"><icon class="icon-arrow-up icon-gray"></icon></a>
						<a href="index.php?Page=<?php echo $Page?>&PaymentMethod=<?php echo $PaymentMethod?>&Attempted=<?php echo $Attempted?>&k=<?php echo $Keyword?>&PaymentStatus=<?php echo $PaymentStatus?>&OrderStatus=<?php echo $OrderStatus?>&OrderBy=GrandTotal DESC" class="adm-link"><icon class="icon-arrow-down icon-gray"></icon></a></th>
					<th class="header"  width="10%" align="right">Email</th>
					<th class="header"  width="10%" align="right"></th>
				</tr>
			</thead>
			<tbody>
			
		<?php
		
			//$SNo=($OrderObj->PageSize*($OrderObj->PageNo-1)) +1;
			$SNo=$TotalRecords -($OrderObj->PageSize*($OrderObj->PageNo-1));
			$Count=1;
			$CurrentMonth="";
			$MonthAmount ="";
			$OrderObj1 = new DataTable(TABLE_ORDERS ." o, ".TABLE_ORDER_CURRENCIES." oc ");	
			$OrderLogObj = new DataTable(TABLE_ORDER_LOG);
			
		while ($CurrentOrder = $OrderObj->GetObjectFromRecord())
		{
			$CurrencyOrderID = $CurrentOrder->OrderID;
			
			$OrderLogObj->Where =" OrderID='".$OrderLogObj->MysqlEscapeString($CurrentOrder->OrderID)."'";
			$OrderLogObj->TableSelectAll(array("MSG"));
				
				
			if($CurrentOrder->MyCreatedMonth!=$CurrentMonth)
			{	
				$CurrentMonth= $CurrentOrder->MyCreatedMonth;
				$OrderObj1->Where = $OrderObj->Where." AND o.OrderID = oc.OrderID AND  DATE_FORMAT(o.CreatedDate,'%d %M, %Y') = '".$CurrentMonth."'";
		
				//$OrderObj1->DisplayQuery = true;
				
				$MonthAmountObj = $OrderObj1->TableSelectOne(array("SUM(o.GrandTotal/oc.ExchangeRate) as MonthTotal","Count(*) as TotalOrder"));								
				$MonthAmount = $MonthAmountObj->MonthTotal;		
							
				?>						
				<tr  class="OrderAccountTR" height="35">
				<th colspan="2"></th>
				<th colspan="3"><b><?php echo $CurrentOrder->MyCreatedMonth?></b></th>
				<th align="right" colspan="2"><b>No. of orders: <?php echo $MonthAmountObj->TotalOrder?></b></th>
				<th align="right" colspan="4"><b>Order Value: &pound;<?php echo number_format($MonthAmount,2)?></b></th>
				</tr>
				<?php
			}
			
			?>
		<tr  class="InsideRightTd" style="background-color:<?php echo @$OrderStatusColorArray[$CurrentOrder->OrderStatus]?>">
			<td height="25" align="center"><input type="checkbox" class="ChkOrd" name="OrderIDArr[]" value="<?php echo $CurrentOrder->OrderID?>"></td>
			<td height="25" align="center"><?php echo $SNo?>
				<?php 
				while($CurrentLog = $OrderLogObj->GetObjectFromRecord())
				{
					?>
					<span class="alert-danger" rel="tooltip" title="<?php echo $CurrentLog->MSG?>"><icon class="icon-warning-sign icon-black"></icon></span>
					<?php
				}
				?>
				
			</td>
			<td align="center">
			<a href ="index.php?Page=<?php echo $Page?>&Section=FullDetail&OrderID=<?php echo $CurrentOrder->OrderID?>">
				<label class="label label-info"><icon class="icon-eye-open icon-white"></icon> <?php echo $CurrentOrder->OrderNo?></label></a></td>
			<td><?php echo $CurrentOrder->MyCreatedDate?></td>
				<td><?php echo MyStripSlashes($CurrentOrder->BillingFirstName)?>&nbsp;<?php echo MyStripSlashes($CurrentOrder->BillingLastName)?></td>
				<td>
				<?php echo MyStripSlashes($CurrentOrder->BillingAddress1)."&nbsp;&nbsp;".MyStripSlashes($CurrentOrder->BillingAddress2)?><br>
				<?php echo MyStripSlashes($CurrentOrder->BillingCity)."&nbsp;&nbsp;".MyStripSlashes($CurrentOrder->BillingState)?><br>
				<?php echo MyStripSlashes($CurrentOrder->BillingArea)."&nbsp;&nbsp;".MyStripSlashes($CurrentOrder->BillingCountry)."&nbsp;&nbsp;".MyStripSlashes($CurrentOrder->BillingZip)?><br>
				T: <?php echo MyStripSlashes($CurrentOrder->BillingPhone)?>
				</td>
			<td><?php echo MyStripSlashes($CurrentOrder->PartnerCode)?></td>	
			<td><?php echo MyStripSlashes($CurrentOrder->PaymentStatus)?></td>	
			<td><?php echo MyStripSlashes($CurrentOrder->PaymentMethod)?></td>	
			<td><?php echo MyStripSlashes($CurrentOrder->OrderStatus)?></td>	
			<td align="right"><?php echo Change2CurrentCurrency($CurrentOrder->GrandTotal)?></td>
			<td align="right"><a href="mailto:<?php echo MyStripSlashes($CurrentOrder->Email)?>" class="btn btn-success btn-phone-block"><icon class="icon-envelope icon-white"></icon> Send&nbsp;Email</a></td>
			<td align="right">
			<?php
			if($CurrentOrder->PaymentStatus == "Pending")
			{
			?>
				<a href="index.php?Page=<?php echo $Page?>&PaymentMethod=<?php echo $PaymentMethod?>&Attempted=<?php echo $Attempted?>&k=<?php echo $Keyword?>&OrderStatus=<?php echo $OrderStatus?>&PaymentStatus=<?php echo $PaymentStatus?>&OrderID=<?php echo $CurrentOrder->OrderID?>&Target=DeleteOrder" class="btn btn-danger btn-phone-block" onclick="return confirm('Wooooo - hold on please - the action you want to do is highly hazardous!!\nIf you press ok, You will NOT be able to recover this order.')"><icon class="icon-trash icon-white"></icon>DELETE</a>
			<?php
			}?>
			</td>
		</tr>
	<?php
			$SNo--;
			$Count++;
			}			
			?>						
			</tbody>
		</table>
		</form>
		<?php
	if($TotalRecords > $OrderObj->PageSize)
		{
		?>
		<table cellpadding="1" width="100%" cellspacing="1" border="0" class="InsideTable">
			<tr>
			<td width="100%">
			<?php echo $OrderObj->GetPagingLinks("index.php?Page=".$Page."&Attempted=".$Attempted."&PaymentMethod=".$PaymentMethod."&k=".$Keyword."&PaymentStatus=".$PaymentStatus."&OrderStatus=".$OrderStatus."&Target=".$Target."&OrderBy=".$OrderBy."&PageNo=",PAGING_FORMAT_LIST,"adm-link","<b>Page No : </b>");?>
			</td>	
			</tr>
		</table>
		<?php
		}
		?>
	<?php
		}
		else 
		{
			?>
			<br>
			<table class="table table-striped table-bordered table-responsive block">
				<tr class="InsideLeftTd">
				<td width="100%" align="center">
				<b>No Record Found.</b></td>	
				</tr>
			</table>
			<br>	
			<?php
		}
		?>
			</div>
		</div>
		<?php
	break;
}
///// Section end?>