<table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr>
		<td height="20" colspan="2" align="center">
		<div class="row-fluid">
		<div class="span12">
		<h1> TAX <?php echo isset($CurrentTaxs->TaxClassTitle)?$CurrentTaxs->TaxClassTitle:""?>(%) </h1>
		
	</div>
</div>
<div class="clearfix separator bottom"></div>
	</td>
	</tr>
	<tr>
		<td height="20" align="left">&nbsp;&nbsp;</td>
		<td height="20" align="right">
		<div class="span12">
		<a href="<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php echo $Page?>&Section=AddTax" class="btn btn-primary btn-add-page"><icon class="icon-plus-sign icon-white"></icon> Add Tax Class</a>
	</div>
		
		
		</td>
	</tr>
</table>				
<?php 
//// Section start
switch($Section)
{
	case "AddTax" :
	case "EditTax" :
	/////// Add Tax  Start
	?>
	<div class="well">
	<fieldset>
	<form method="POST" action="<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php echo $Page?>&Section=<?php echo $Section?>&Target=AddTax&TaxClassID=<?php echo $TaxClassID?>" enctype="multipart/form-data">
	<table border="0" cellpadding="3" cellspacing="1" width="100%" class="InsideTable">
		<tr>
			<td align="center" width="15%" class="FieldName"><b>Tax Tile</b></td>
			<td align="left" class=""><input type="text" name="TaxClassTitle" value="<?php echo isset($CurrentTaxs->TaxClassTitle)?MyStripSlashes($CurrentTaxs->TaxClassTitle):""?>" size="30"></td>
		</tr>
		<!--<tr>
			<td align="center" width="15%" class="FieldName"><b>Tax Percent (%)</b></td>
			<td align="left" class=""><input type="text" id="TaxPercent" name="TaxPercent" value="<?php echo isset($_POST['TaxPercent'])?$_POST['TaxPercent']:(isset($CurrentTaxs->TaxPercent)?MyStripSlashes($CurrentTaxs->TaxPercent):"")?>" size="10"></td>
		</tr>-->
		<tr>
			<td align="center" class="FieldName"></td>
			<td><input type="submit" name="AddTax" value="Submit" class="btn" style="height:auto;"></td>
		</tr>
	</table>
	</form>
	</fieldset>
	</div>
	<?php 
	/////// Add Tax End
	break;
	
	case "SetTax":
	$CountryObj = new DataTable(TABLE_COUNTRIES);
	$CountryObj->Where = "1";
	$CountryObj->TableSelectAll("","Active=1 DESC,CountryName ASC");
	?>
	<table width="100%">
		<tr>
		<td align="center">
			<form method="POST" action="<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php echo $Page?>&Section=<?php echo $Section?>&TaxClassID=<?php echo $TaxClassID?>&Target=UpdateTaxByCountry">
				<table class="table table-striped table-bordered table-responsive block">
						<thead>
						<tr>
							<th height="40"  class="InsideLeftTd">S.No</th>
							<th class="InsideLeftTd" align="center">Country</th>
							<th class="InsideLeftTd" align="center">Tax Percent(%)</th>
						</tr>
						</thead>
						<tbody>
						<?php
						
							$SNo=1;
							$Count=1;
							$k=1;
							while ($CurrentCountry = $CountryObj->GetObjectFromRecord())
							{
								$TaxClassCalcObj->Where ="TaxClassID='".(int)$TaxClassID."' AND CountryID='".(int)$CurrentCountry->CountryID."'  
														  AND (RegionID='0' OR RegionID='' OR ISNULL(RegionID))";
								//$TaxClassCalcObj->DisplayQuery = true;
								$CurrentTaxCal = $TaxClassCalcObj->TableSelectOne(array("*"),"");
								
								?>
							<tr>
								<td class="InsideRightTd"><?php echo $SNo?>
								</td>
								<td class="InsideRightTd"><?php echo MyStripSlashes($CurrentCountry->CountryName)?> (<?php echo $CurrentCountry->CountryISOCode1?>)</td>
								<td align="center"><input  type="text" name="TaxPercent[<?php echo $CurrentCountry->CountryID?>]" size="3" value="<?php echo isset($CurrentTaxCal->TaxPercent)?$CurrentTaxCal->TaxPercent:"0"?>" class="AlignCenter" style="text-align:center;width:60px"></td>
								
						</tr>
							<?php
							$SNo++;
							$Count++;
							}
							?>
						<tr>
							<td height="35" class="InsideRightTd"></td>
							<td class="InsideRightTd"><input type="hidden" name="Count" value="<?php echo $Count?>"></td>
							<td align="center" class="InsideRightTd" colspan="2"><input type="submit"  name="UpdateCountry<?php echo $Count?>" value="Update" class="check"></td>
						</tr>
					</tbody>						
					</table>
				</form>				
				<br>
				
			</td>
		</tr>
	</table>
	<?php
	break;
	////////Display Tax 
	case "DisplayTax" :
	default:
		$TaxClassObj->Where = "1";
		$TaxClassObj->TableSelectAll();
		if($TaxClassObj->GetNumRows() > 0)
		{
		?>
		<form method="POST" action="<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php echo $Page?>&Section=<?php echo $Section?>&Target=UpdateTax&">
		<table class="table table-striped table-bordered table-responsive block">
			<thead>
				<tr>
				<td align="center" width="5%" class="InsideLeftTd"><b>S.No.</b></td>
				<td align="left" class="InsideLeftTd"><b>Tax Title</b></td>
				<td align="left" class="InsideLeftTd"><b>Tax Percent(%)</b></td>
				<td align="center" class="InsideLeftTd"><b>Edit</b></td>
				<td align="center" class="InsideLeftTd"><b>Delete</b></td>
				</tr>
			</thead>
			<tbody>
			<script>
				function FunctionDeleteTax(TaxClassID,Status)
				{
					if(Status==0)
					{
						alert("There are some product assign with this tax class.");
						return false;
					}
					else
					{
						result = confirm("Are you sure want to delete the tax class?")
						if(result == true)
						{
							URL="<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php echo $Page?>&Target=DeleteTax&TaxClassID=" + TaxClassID;
							location.href=URL;
							return true;
						}
					}
					return false;
				}				
			</script>
			<?php 
			$SNo=1;
			$Count=1;
			while($CurrentTaxs = $TaxClassObj->GetObjectFromRecord())
			{
				
				$ProductObj->Where ="TaxClass='".$CurrentTaxs->TaxClassID."'";
				$ProductObj->TableSelectAll(array("ProductID"));
				$Products = $ProductObj->GetNumRows();
				if($Products==0)
					$Status = 1;
				else 
					$Status = 0;
			?>
			<tr class="<?php echo ($Count%2?'GridRow':'GridAlternateRow')?>">
				<td align="center"><b><?php echo $SNo?></b></td>
				<td align="left"><b><?php echo MyStripSlashes($CurrentTaxs->TaxClassTitle)?></b>
				<input type="hidden" name="TaxClassID_<?php echo $Count?>" value="<?php echo $CurrentTaxs->TaxClassID?>"></td>
				<td align="left"><a href="<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php echo $Page?>&Section=SetTax&TaxClassID=<?php echo $CurrentTaxs->TaxClassID?>">Set Percent(%)</a></td>
				<td align="center"><a href="<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php echo $Page?>&Section=EditTax&TaxClassID=<?php echo $CurrentTaxs->TaxClassID?>" class="btn btn-success"><b><icon class="icon-pencil icon-white"></icon>Edit</b></a></td>
				<td align="center">
					<a href="#" rel="<?php echo $Products?>" onclick="return FunctionDeleteTax('<?php echo $CurrentTaxs->TaxClassID?>','<?php echo $Status?>');" class="btn btn-danger"><b><icon class="icon-remove icon-white"></icon><span class="hidden-phone">Delete</span></b></a>
				</td>
			</tr>
			<?php 
			$SNo++;
			$Count++;
			}
		?>
		</tbody>	
	  </table>
	</form>						
		<?php 
		}
		else 
		{
			?>
			<b>No Result Found.</b>
			<?php 
		}
	break;
}
///// Section end?>