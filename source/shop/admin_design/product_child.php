<div style="text-align:left;">
<?php 
//// Section start
switch($Section)
{
	case "Refresh" :
		?>
		<b>Products updated successfully.</b>
		<script type="text/javascript">
		jQuery(document).ready(function($){
			window.parent.$.fancybox.close();
		});
		</script>
		<?php 
	break;
	case "AddChildProduct" :
	case "EditChildProduct" :
	/////// Add Product  Start
	?>
	<div class="well">
	<h3><?php echo MyStripSlashes($CurrentMaster->ProductName)?>'s Attributes </h3>
	<?php 
	if(isset($CurrentProduct->ProductID))
	{
		foreach ($AttributeArray as $k=>$v)
		{
		?>
		<b><?php echo $v?></b>:<i><?php echo isset($AttributeValueArray[$k]['text'])?$AttributeValueArray[$k]['text']:""?></i>&nbsp;&nbsp;|&nbsp;&nbsp;			
		<?php 
		}
	}?>
<form class="form-horizontal" method="POST" action="<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php echo $Page?>&Section=<?php echo $Section?>&Target=AddChildProduct&MasterID=<?php echo $MasterID?>&ProductID=<?php echo $ProductID?>" enctype="multipart/form-data">
		
		<div class="form-group">
				<label for="field" class="col-sm-4 col-md-5 col-lg-4 control-label">SKU/Model No</label>
				<div class="col-sm-20 col-md-19 col-lg-20 text-left"><input type="text" name="ModelNo" value="<?php echo isset($_POST['ModelNo'])?$_POST['ModelNo']:(isset($CurrentProduct->ModelNo)?MyStripSlashes($CurrentProduct->ModelNo):"")?>" class="txtBox"></div>
			</div>
			<?php 
			foreach ($AttributeArray as $k=>$v)
			{
				$AttributeValueObj->Where = "AttributeID='".$AttributeValueObj->MysqlEscapeString($k)."'";
				$AttributeValueObj->TableSelectAll(array("*"),"Position ASC");
				
				?>
				<div class="form-group">
				<label for="field" class="col-sm-4 col-md-5 col-lg-4 control-label"><?php echo $v?></label>
					<div class="col-sm-20 col-md-19 col-lg-20 text-left">
						<select name="AttributeValue[]">
						<?php 
						while ($CurrentAttributeValue = $AttributeValueObj->GetObjectFromRecord())
						{
						?>
							<option value="<?php echo $CurrentAttributeValue->AttributeValueID?>" <?php echo ((isset($AttributeValueArray[$k]['id']) && $AttributeValueArray[$k]['id']==$CurrentAttributeValue->AttributeValueID)?"selected":"")?>><?php echo $CurrentAttributeValue->AttributeValue?></option>
						<?php 
						}?>
						</select>
					</div>
				</div>
			
				<?php 
			}
			?>
			<hr>
			<div class="form-group">
				<label for="field" class="col-sm-4 col-md-5 col-lg-4 control-label">Weight</label>
				<div class="col-sm-20 col-md-19 col-lg-20 text-left"><input type="text" name="Weight" value="<?php echo isset($_POST['Weight'])?$_POST['Weight']:(isset($CurrentProduct->Weight)?MyStripSlashes($CurrentProduct->Weight):"")?>" class="txtBox"></div>
			</div>
			<div class="form-group">
				<label for="field" class="col-sm-4 col-md-5 col-lg-4 control-label">Price</label>
				<div class="col-sm-20 col-md-19 col-lg-20 text-left"><input type="text" id="Price" onblur="UpdateDiscount();" name="Price" value="<?php echo isset($_POST['Price'])?$_POST['Price']:(isset($CurrentProduct->Price)?MyStripSlashes($CurrentProduct->Price):"")?>" class="txtBox"></div>
			</div>
			<div class="form-group">
				<label for="field" class="col-sm-4 col-md-5 col-lg-4 control-label">Sale Price</label>
				<div class="col-sm-20 col-md-19 col-lg-20 text-left">
					<input type="text" id="SalePrice" name="SalePrice" onblur="UpdateDiscount();" value="<?php echo isset($_POST['SalePrice'])?$_POST['SalePrice']:(isset($CurrentProduct->SalePrice)?MyStripSlashes($CurrentProduct->SalePrice):"")?>" class="txtBox">
					<?php  
						$disc = "";
						$rPrice = floatval(isset($_POST['Price'])?$_POST['Price']:(isset($CurrentProduct->Price)?MyStripSlashes($CurrentProduct->Price):"0"));
						$sPrice = floatval(isset($_POST['SalePrice'])?$_POST['SalePrice']:(isset($CurrentProduct->SalePrice)?MyStripSlashes($CurrentProduct->SalePrice):"0"));
						$svPrice = $rPrice - $sPrice;
						if($rPrice > 0 && $sPrice > 0)
							$disc = number_format(($svPrice/$rPrice)*100, 2);
					?>
					
				</div>
			</div>
			
			<div class="form-group">
				<label for="field" class="col-sm-4 col-md-5 col-lg-4 control-label">Discount(%) :</label>
				<div class="col-sm-20 col-md-19 col-lg-20 text-left">
					<input type="text" id="SaleDiscount" name="SaleDiscount" value="<?php echo $disc?>" class="txtBox" onblur="UpdateSalePrice();">
					<br/><input type="checkbox" name="SaleActive" value="1" class="chk" <?php echo (isset($_POST['SaleActive']) && $_POST['SaleActive']==1)?"checked":((isset($CurrentProduct->SaleActive) && $CurrentProduct->SaleActive==1)?"checked":"")?>> Check to display Sale Price
				</div>
			</div>
			
			<div class="form-group">
				<label for="field" class="col-sm-4 col-md-5 col-lg-4 control-label">Stock</label>
				<div class="col-sm-20 col-md-19 col-lg-20 text-left"><input type="text" name="Stock" value="<?php echo isset($_POST['Stock'])?$_POST['Stock']:(isset($CurrentProduct->Stock)?MyStripSlashes($CurrentProduct->Stock):"25")?>" class="txtBox">				
				</div>
			</div>
			
			<div class="form-group">
				<label for="email" class="col-sm-4 col-md-5 col-lg-4 control-label">Tax </label>
				<div class="col-sm-20 col-md-19 col-lg-20 text-left">
					<select id="TaxClass" name="TaxClass">
						<?php 
						$TaxClassObj = new DataTable(TABLE_TAX_CLASS);
						$TaxClassObj->Where="1";
						$TaxClassObj->TableSelectAll("","TaxPercent ASC");
						while($Currentvat = $TaxClassObj->GetObjectFromRecord())
						{
						?>
						<option value="<?php echo $Currentvat->TaxClassID?>"  <?php echo isset($CurrentProduct->TaxClass) && $CurrentProduct->TaxClass==$Currentvat->TaxClassID?"selected":""?>><?php echo isset($Currentvat->TaxClassTitle)?MyStripSlashes($Currentvat->TaxClassTitle):""?></option>
						<?php  
						}?>
					</select>						
				</div>								
			</div>
			
			<hr>
			
			<div class="form-group">
				<label for="field" class="col-sm-4 col-md-5 col-lg-4 control-label">Upload Image</label>
				<div class="col-sm-20 col-md-19 col-lg-20 text-left">
				<input type="file" name="Upload">
				
				<br>
				</div>
			</div>
			
			
			<div class="form-group">
				<label for="field" class="col-sm-4 col-md-5 col-lg-4 control-label"> Alt/Title:</label>
				<div class="col-sm-20 col-md-19 col-lg-20 text-left">
				<?php 
				if(isset($CurrentProduct->Image) && $CurrentProduct->Image !="" && file_exists(DIR_FS_SITE_UPLOADS_PRODUCT_ORIGINAL.$CurrentProduct->Image))
				{?>
					<a href="<?php echo DIR_WS_SITE_UPLOADS_PRODUCT_ORIGINAL.$CurrentProduct->Image?>" target="_blank" style="display:block; float:left;">
						<?php SKImgDisplay(DIR_FS_SITE_UPLOADS_PRODUCT_ORIGINAL.$CurrentProduct->Image,70,'&nbsp;');?>
						<br>
						View Enlarge
					</a>
					
			  <?php }?>
				<input type="text" name="ImageTitle" value="<?php echo isset($_POST['ImageTitle'])?$_POST['ImageTitle']:(isset($CurrentProduct->ImageTitle)?MyStripSlashes($CurrentProduct->ImageTitle):"")?>" class="txtBox">
				
				<br>
				</div>
			</div>
			
			<div class="form-group">
				<label for="field" class="col-sm-4 col-md-5 col-lg-4 control-label">Active</label>
				<div class="col-sm-20 col-md-19 col-lg-20 text-left"><input type="checkbox" name="Active" value="1" class="chk" <?php echo (isset($_POST['Active']) && $_POST['Active']==1)?"checked":((isset($CurrentProduct->Active) && $CurrentProduct->Active==1)?"checked":"")?>></div>
			</div>
			
			<div class="form-group">
				<div class="col-sm-6 pull-right">
					<button class="btn btn-warning  btn-block" type="submit" name="AddProduct">Submit</button>
				</div>
			</div>
			
		
	</form>
	</div>
	<script type="text/javascript">
	jQuery(document).ready(function($){
		$("#text11111").click(function(){
			//window.parent.$.fancybox.close();
		})
	});

	function UpdateSalePrice(){
		var disc = $("#SaleDiscount").val();
		if(!isNaN(parseFloat($("#Price").val())) && !isNaN(parseFloat(disc))){
			var rPrice = parseFloat($("#Price").val());								
			var svPrice = (disc/100)*rPrice;
			var sPrice = rPrice - svPrice;
			$("#SalePrice").val(sPrice.toFixed(2));
		}
	}
	
	function UpdateDiscount(){
		var sPrice = $("#SalePrice").val();
		if(!isNaN(parseFloat($("#Price").val())) && !isNaN(parseFloat(sPrice))){
			var rPrice = parseFloat($("#Price").val());								
			var svPrice = rPrice - parseFloat(sPrice);
			var disc = (svPrice/rPrice)*100;
			$("#SaleDiscount").val(disc.toFixed(2));
		}
	}
</script>
	
	<?php 
	/////// Add Product End
	break;
		
	
	case "DisplayChildProduct" :
	default:	
		@ob_clean();	
			/*Uncategorized products*/
			$ProductObj = new DataTable(TABLE_PRODUCT);
			$ProductObj->Where = "MasterID ='".(int)$MasterID."'";
			$ProductObj->TableSelectAll("","CreatedDate ASC");
			if($ProductObj->GetNumRows() > 0)
			{
				?>
							
					<table class="table table-striped table-bordered table-responsive block">
						<thead>
							<tr>
							<td align="center" width="5%"><b>S.No.</b></td>
							<?php 
								foreach ($AttributeArray as $k=>$v)
								{
								?>
									<td align="left"><b><?php echo $v?></b></td>
								<?php 
								}?>
							<td align="left"><b>Price</b></td>
							<td align="left"><b>Image</b></td>
							<td align="center"><b>Active</b></td>
							<td align="center"><b>Replicate</b></td>
							<td align="center"><b>Edit</b></td>
							<td align="center"><b>Delete</b></td>
						</tr>
						</thead>
						<tbody>
						<?php 
						$SNo=1;
						while($CurrentProduct = $ProductObj->GetObjectFromRecord())
						{
							$StArr = GetProductPriceNStock($CurrentProduct->ProductID,1);
							
							$ProductRelationObj->TableName = TABLE_PRODUCT_RELATION." pr, ".TABLE_ATTRIBUTE_VALUE." av";
							$ProductRelationObj->Where = "pr.ProductID='".$CurrentProduct->ProductID."' AND pr.RelationID = av.AttributeValueID  AND pr.RelationType = 'attribute_value'";
							$ProductRelationObj->TableSelectAll();
							$AttributeValueArray = array();
							while ($CurrentRelation = $ProductRelationObj->GetObjectFromRecord())
							{
								$AttributeValueArray[$CurrentRelation->AttributeID]['id'] = $CurrentRelation->AttributeValueID;
								$AttributeValueArray[$CurrentRelation->AttributeID]['text'] = $CurrentRelation->AttributeValue;
							}
						?>
						<tr>
							<td align="center" width="5%"><?php echo $SNo?></td>
							<?php 
								foreach ($AttributeArray as $k=>$v)
								{
								?>
									<td align="left"><?php echo isset($AttributeValueArray[$k]['text'])?$AttributeValueArray[$k]['text']:""?></td>
								<?php 
								}?>
							
							<td align="center">&pound;<?php echo isset($StArr['Price'])?number_format($StArr['Price'],2):"";?></td>
							<td align="left">
							<?php 
							if($CurrentProduct->Image !="" && file_exists(DIR_FS_SITE_UPLOADS_PRODUCT_ORIGINAL.$CurrentProduct->Image))
							{?>
								<a href="<?php echo DIR_WS_SITE_UPLOADS_PRODUCT_ORIGINAL.$CurrentProduct->Image?>" target="_blank">
									<?php SKImgDisplay(DIR_FS_SITE_UPLOADS_PRODUCT_ORIGINAL.$CurrentProduct->Image,70,'&nbsp;');?>
								</a>
						  <?php }?>
						</td>
							<td align="center"><?php echo $CurrentProduct->Active==1?"Active":"Inactive"?></td>
							<td align="center"><a class="child_fancy" href="<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php echo $Page?>&Target=ReplicateProduct&MasterID=<?php echo $MasterID?>&ProductID=<?php echo $CurrentProduct->ProductID?>&Popup=true"><b>Replicate</b></a></td>
							<td align="center"><a href="<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php echo $Page?>&Section=EditChildProduct&MasterID=<?php echo $MasterID?>&ProductID=<?php echo $CurrentProduct->ProductID?>&Popup=true" class="child_fancy btn btn-success btn-phone-block"><icon class="icon-pencil icon-white"></icon>Edit</b></a></td>
							<td align="center"><a href="<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php echo $Page?>&Target=DeleteChildProduct&MasterID=<?php echo $MasterID?>&ProductID=<?php echo $CurrentProduct->ProductID?>&Popup=true" class="child_fancy btn btn-danger btn-phone-block"><icon class="icon-remove icon-white"></icon><b>Delete</b></a></td>
						</tr>
						
						<?php 
						$SNo++;
						}?>
						</tbody>
					</table>	
				<?php 
			
			}
			else 
			{
				?>
				<b>There is no any record associated with this product. </b><br> 
				<!--Please enter new <a href="<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php echo $Page?>&Section=AddChildProduct&MasterID=<?php echo $MasterID?>" class="adm-link">Add New Attribute Product</a>&nbsp;&nbsp;-->
				<?php 
			}
	exit;		
	break;
}
///// Section end?>
</div>