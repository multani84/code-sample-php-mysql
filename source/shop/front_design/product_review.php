<div id="MainBanners" class="clearfix"><img src="<?php echo DIR_WS_SITE_TEMPLATES.DEFINE_TEMPLATE_STYLE?>/images/bnr.jpg" alt="Banner" /></div>
<div id="MainContent" class="clearfix"> 
	<div class="CMSContent">
	<?php require_once(DIR_FS_SITE_INCLUDES."breadcrumb.php");?>
    <h2 class='Title'><?php echo (isset($CurrentProduct->ProductName)?MyStripSlashes($CurrentProduct->ProductName)." - Delivery Charges":" Delivery Charges");?></h2>

			<?php/* middle content start*/?>
			<?php
				require_once(DIR_FS_SITE_INCLUDES."message.php");?>
		    	<div style="display:inline-block;width:100%;">
					<?php
	    if(isset($ThankMsg) && $ThankMsg !="")
	    {
	    	echo "<div><b>".$ThankMsg."</b></div>";
	    }
	    else 
	    {
	    ?>
			<form name="RegistrationForm" action="<?php echo MakePageURL("index.php","Page=$Page&ProductID=$ProductID&Popup=true&Target=AddReview")?>" id="RegistrationForm" method="POST" onSubmit="return ValidateForm(this);">
                <table cellpadding="3" width="100%" align="center" border="0" cellspacing="2" class="TableFormat">
                  <tbody>
                   <tr valign=top>
                       <td align="left">
                      		<table border="0" width="100%" cellspacing="2" cellpadding="2">
		                      <tr valign=top>
		                          <td nowrap width="30%" valign="middle">Your Name<font size="2">*</font></td>
		                          <td><input type="text" size=24 name="CName"  value="<?php echo isset($IsCurrentUser->FirstName)?$IsCurrentUser->FirstName." ".$IsCurrentUser->LastName:""?>" id="CName_R" title="Please enter your Name"></td>
		                      </tr>
          					 <tr valign=top>
		                          <td nowrap width="30%" valign="middle">Your Email<font size="2">*</font></td>
		                          <td><input type="text" size=24 name="CEmail"  value="<?php echo isset($IsCurrentUser->Email)?$IsCurrentUser->Email:""?>" id="CEmail_E" title="Invalid Email Address ! Please try user@domain.com"></td>
		                      </tr>
          					 <tr>
							   <td colspan="2"><b>Your Review:</b></td>
				              </tr>
				             <tr>
		                        <td colspan="2"><textarea title="Please enter your review" name="Description" id="Description_R" wrap="soft" cols="40" rows="7"><?php echo isset($_POST['Description'])?$_POST['Description']:""?></textarea></td>
							 </tr>
		                      <tr>
		                        <td colspan="2"><b>Rating:</b> <small><font color="#ff0000"><b>BAD</b></font></small> 
		                        <?php
		                        for ($i=1;$i<=5;$i++)
		                        {
		                        ?>
		                        <input type="radio" title="Please select your review" name="Rating" id="Rating<?php echo $i?>_A" value="<?php echo $i?>">
		                        <?php
		                        }?><small><font color="#ff0000"><b>GOOD</b></font></small></td>
		                      </tr>
		                    </table>
                      </td>
                    </tr>
                    <tr valign=top>
                      <td><br>
                      <table  border="0" cellspacing="0" cellpadding="0">
			           <tr>
			             <td width="50px"></td>
			             <td width="50px" style="padding-left:10px"><a name="Register" href="javascript:;" onclick="return ValidateForm('RegistrationForm');" ><img src="<?php echo DIR_WS_SITE_TEMPLATES.DEFINE_TEMPLATE_STYLE?>/images/bn_submit.png" alt="" border="0" /></a>
			             <input type="image" name="DefaultSubmit" src="<?php echo DIR_WS_SITE_GRAPHICS?>dot.png" class="DefaultSubmit"></td>
			             </tr></table><br>
                        </td>
                    </tr>
                  </tbody>
                </table>
            </form>	
            <?php
	    }?>
				</div>
			<?php/* middle content end*/?>
	</div>
</div>

