<?php

/*

Template: Default

*/?>

  <div id="MainContent">
    	<div class="container">        	
            <ul class="breadcrumb">
                <li><a href="<?php echo SKSEOURL("1001","cms")?>">Home</a></li>
                <?php echo ((isset($CurrentManufacturer->MNName) && $CurrentManufacturer->MNName !="")?"<li class='active'>".MyStripSlashes($CurrentManufacturer->MNName)."</li>":"");?>
            </ul>
        	<h1 class='active'>Products</h1>
           <div class="clear" style="clear:both;">&nbsp;</div>
			


		<?php  /* product display start*/
			$ProductObj= new DataTable(TABLE_PRODUCT. " p");;
			$ProductObj->Where = "p.Active='1' AND p.ProductType !='Child'";
			$OrderBy= "p.CreatedDate DESC";	
			switch($F)
			{
				case "New":
					$ProductObj->Where .= " AND p.Featured=1";
				break;
				case "BS":
					$ProductObj->Where .= " AND p.BestSeller=1";
				break;
				case "SO":
					$ProductObj->Where .= " AND p.Homepage=1";
				break;
			}

			$ProductObj->AllowPaging =true;
			$ProductObj->PageSize=constant("DEFINE_PRODUCT_DISPLAY_LIST") > 0?constant("DEFINE_PRODUCT_DISPLAY_LIST"):16;
			$ProductObj->PageNo =isset($_GET['PageNo'])?$_GET['PageNo']:1;	
			$ProductObj->TableSelectAll(array("p.*"),$OrderBy);
			$TotalRecords = $ProductObj->TotalRecords ;
			$TotalPages =  $ProductObj->TotalPages;
			if($ProductObj->GetNumRows() > 0)
			{?>
				
				
				
			 <div class="row products text-center">
				 		<?php 
					 	while($CurrentProduct = $ProductObj->GetObjectFromRecord())
						{
							 include(dirname(__DIR__)."/product_template.php");
			            }?>
					</div>
				
				  <div style="text-align:right; "><b><?php echo $TotalRecords?> results found</b>&nbsp;&nbsp;</div>
			<?php 
				if($TotalRecords > $ProductObj->PageSize){?>
				
				 <ul class="paging">
					<?php echo $ProductObj->GetPagingLinks(MakePageURL("index.php","Page=shop/product_list","F=$F&PageNo="),PAGING_FORMAT_NUMBERED,"","");?>
				 </ul>
				
				<?php }?>
				
			<?php  }
		
		else 
		{
			
		}
		/* product display end*/
		?>
		
	</div>
		
</div>



