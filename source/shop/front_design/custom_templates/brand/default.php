<?php

/*

Template: Default

*/?>

  <div id="MainContent">
    	<div class="container">        	
            <ul class="breadcrumb">
                <li><a href="<?php echo SKSEOURL("1001","cms")?>">Home</a></li>
                <?php echo ((isset($CurrentManufacturer->MNName) && $CurrentManufacturer->MNName !="")?"<li class='active'>".MyStripSlashes($CurrentManufacturer->MNName)."</li>":"");?>
            </ul>
        	 <?php echo ((isset($CurrentManufacturer->MNName) && $CurrentManufacturer->MNName !="")?"<h1 class='active'>".MyStripSlashes($CurrentManufacturer->MNName)."</h1>":"");?>
           <div>
			   <div class="col-12">
				<?php 
				if(false && isset($CurrentManufacturer->Image) && $CurrentManufacturer->Image !="" && file_exists(DIR_FS_SITE_UPLOADS_OTHER_ORIGINAL.$CurrentManufacturer->Image))
				{?>
					<div class="pull-left" style="margin:0px 10px 10px 0px;">
						<?php SKImgDisplay(DIR_FS_SITE_UPLOADS_OTHER_ORIGINAL.$CurrentManufacturer->Image,300,'&nbsp;');?>
					</div>
			  <?php }?>
					<?php echo (isset($CurrentManufacturer->Description)?MyStripSlashes($CurrentManufacturer->Description):"&nbsp;");?>
				</div>
			</div>
			<div class="clear" style="clear:both;">&nbsp;</div>
			<?php if(isset($CurrentManufacturer->UpperBanner1) && $CurrentManufacturer->UpperBanner1 !=""):?>
			<div id="MainBnr" class="slideshow" data-cycle-fx="scrollHorz" data-cycle-slides="> a">
		    <?php	
				$BannerObj = new DataTable(TABLE_BANNERS);
				$BannerObj->Where ="Alignment='H' and Active='1'";
				$BannerObj->Where ="Alignment='BR' AND BannerType ='Image' AND Active='1' AND BannerID in (".$CurrentManufacturer->UpperBanner1.")";
				$BannerObj->TableSelectAll("","Position");	
				if($BannerObj->GetNumRows() >0)
				{
				$SNo=1;
				while ($CurrentBanner = $BannerObj->GetObjectFromRecord())
				{?>
				<a href="<?php echo MyStripSlashes($CurrentBanner->URL)?>"><img src="<?php echo DIR_WS_SITE_UPLOADS_BANNER.$CurrentBanner->Content?>" alt="" /></a>
				<?php
				$SNo++;
				}
				}?>
           </div>
			<div class="clear" style="clear:both;">&nbsp;</div>
			<?php endif?>


		<?php  /* product display start*/
		if(isset($ChildProduct) && $ChildProduct >0)
		{
			$ProductObj= new DataTable(TABLE_PRODUCT. " p");;
			$ProductObj->Where = "p.MNID='".$CurrentManufacturer->MNID."' AND p.Active='1'";
			$OrderBy= "p.CreatedDate DESC";	

			$ProductObj->AllowPaging =true;
			$ProductObj->PageSize=constant("DEFINE_PRODUCT_DISPLAY_LIST") > 0?constant("DEFINE_PRODUCT_DISPLAY_LIST"):16;;
			$ProductObj->PageNo =isset($_GET['PageNo'])?$_GET['PageNo']:1;	
			$ProductObj->TableSelectAll(array("p.*"),$OrderBy);
			$TotalRecords = $ProductObj->TotalRecords ;
			$TotalPages =  $ProductObj->TotalPages;
			if($ProductObj->GetNumRows() > 0)
			{?>
				
				
				
			 <div class="row products text-center">
				 		<?php 
					 	while($CurrentProduct = $ProductObj->GetObjectFromRecord())
						{
							 include(dirname(__DIR__)."/../../product_template.php");
			            }?>
					</div>
				
				  <div style="text-align:right; "><b><?php echo $TotalRecords?> results found</b>&nbsp;&nbsp;</div>
			<?php 
				if($TotalRecords > $ProductObj->PageSize){?>
				
				 <ul class="paging">
					<?php echo $ProductObj->GetPagingLinks(SKSEOURL($MNID,"shop/brand","PageNo="),PAGING_FORMAT_NUMBERED,"","");?>
				 </ul>
				
				<?php }?>
				
			<?php  }
		
		 }
		else 
		{
			
		}
		/* product display end*/
		?>
		
	</div>
		
</div>
<script>$.fn.cycle.defaults.autoSelector = '.slideshow';</script>
			




