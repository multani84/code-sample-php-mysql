<?php

/*

Template: Default

*/?>

<link href="<?=DIR_WS_SITE_INCLUDES_JAVASCRIPT?>jquery/flick/css/flick/jquery-ui-1.9.2.custom.css" rel="stylesheet">
<script src="<?=DIR_WS_SITE_INCLUDES_JAVASCRIPT?>jquery/flick/js/jquery-ui-1.9.2.custom.js"></script>
<script src="<?=DIR_WS_SITE_INCLUDES_JAVASCRIPT?>jquery/jquery.ui.touch-punch.min.js"></script>

 
    	
<div class="products1">   
	<div class="container">
    	    	
            <?php /*<ul class="breadcrumb">
                <li><a href="<?php echo SKSEOURL("1001","cms")?>">Home</a></li>
				 &raquo;<?php echo CategoryChain4Caption($ReferenceID,"index.php","SEOURL");?>
            </ul> */?>
        	 <div class="pull-right"><a href="javascript:history.back();">&larr; BACK</a></div>
			 <?php echo ((isset($CurrentCategory->CategoryName) && $CurrentCategory->CategoryName !="")?"<h1 class='active'>".MyStripSlashes(ucwords(strtolower($CurrentCategory->CategoryName)))."</h1>":"");?>
			 <?php if(strip_tags($CurrentCategory->Description) != ""):?>
			 <?php echo (isset($CurrentCategory->Description)?MyStripSlashes($CurrentCategory->Description):"&nbsp;");?>
			 <?php endif;?>
			 
           
<?php  /* category display start*/ 
if((isset($ChildCategory) && $ChildCategory >0 && $ProductOnly !="1") OR $CategoryID==0)
		{
			$CategoryObj2= new DataTable(TABLE_CATEGORY);
			$CategoryObj2->Where = "ParentID='".$CategoryID."' AND Active='1'";
			$CategoryObj2->TableSelectAll("","Position ASC");
		?>
		<div style="clear:both;"></div>
		<div class="Category row products text-center">
			<?php
			$SNo=1;
			while ($CurrentSubCategory = $CategoryObj2->GetObjectFromRecord())
			{
			?>
				<div class="record col-sm-3">
					<div class="image">
			    		<a href="<?php echo SKSEOURL($CurrentSubCategory->CategoryID,"shop/category")?>" title="<?php echo MyStripSlashes($CurrentSubCategory->CategoryName);?>">
				        	<?php SKImgDisplay(DIR_FS_SITE_UPLOADS_OTHER_ORIGINAL.$CurrentSubCategory->Image,"200",MyStripSlashes($CurrentSubCategory->CategoryName)," class='img-responsive center-block'");?>
				        </a>
					</div>
					<div class="name">
						<a href="<?php echo SKSEOURL($CurrentSubCategory->CategoryID,"shop/category")?>" title="<?php echo MyStripSlashes($CurrentSubCategory->CategoryName);?>">
							<?php echo MyStripSlashes($CurrentSubCategory->CategoryName);?>
						</a>
					</div>
			   </div>
	        	<?php
	        	$SNo++;
			}?>
		</div>
		<div style="clear:both"></div>
		<?php
		}
  /* category display end*/ ?>


		<?php  /* product display start*/
		if(isset($ChildProduct) && $ChildProduct >0)
		{
			
			?>
			<?php 
		if(file_exists(dirname(__FILE__)."/filter.php"))
			require_once(dirname(__FILE__)."/filter.php");
	?>
			<?php /* product display start*/

						if(isset($ExcludeAttVal) && $ExcludeAttVal !="")
						{?>
							<div class="FilteredBy" style="height:60px">
								<?php foreach ($_GET['Att'] as $attr => $attrVal) {
									$AttributeObj1 = new DataTable(TABLE_ATTRIBUTE);
									$AttributeObj1->Where ="AttributeID = '".$attr."'";
									$CurrentAttribute = $AttributeObj1->TableSelectOne(array("AttributeName"));
										echo "&nbsp;&nbsp;&nbsp;<b>$CurrentAttribute->AttributeName : </b>&nbsp;";

									foreach (explode(",",$attrVal) as $k=>$v)
									{
										$AttributeValueObj = new DataTable(TABLE_ATTRIBUTE_VALUE);
										$AttributeValueObj->Where ="AttributeValueID = '".(int)$v."'";
										$CurrentAttributeValue = $AttributeValueObj->TableSelectOne(array("AttributeID,AttributeValue"));
									?>							
									<a href="<?php echo SKAttrValLinkRemoveURL($FilterURL,$CurrentAttributeValue->AttributeID,$v)?>"><?php echo MyStripSlashes($CurrentAttributeValue->AttributeValue)?></a>
								<?php
									}
								}?>	
							</div>
							<?php
							}
							?>
			<?php
			
								$SortByArray = array(
										"00"=>array("display"=>"Sort by..","column"=>"ptc.SortOrder ASC,p.CreatedDate DESC"),
										"11"=>array("display"=>"Name (A - Z)","column"=>"p.ProductName ASC"),
										"12"=>array("display"=>"Name (Z - A)","column"=>"p.ProductName DESC"),
										"21"=>array("display"=>"Price (Low &gt; High)","column"=>"FinalPrice ASC"),
										"22"=>array("display"=>"Price (High &gt; Low)","column"=>"FinalPrice DESC"),
										"31"=>array("display"=>"Date (Latest)","column"=>"p.CreatedDate DESC"),
										"32"=>array("display"=>"Date (Oldest)","column"=>"p.CreatedDate ASC"),
								);
								
								if($CategoryID =="")
										$SortByArray['00'] = array("display"=>"Sort by..","column"=>"p.CreatedDate DESC");
									
								$ps = isset($_GET['ps'])?(int)$_GET['ps']:"15";
								$sb = isset($_GET['sb'])?$_GET['sb']:"00";
		
								if($CategoryID !="")
								{
									$ProductObj= new DataTable(TABLE_PRODUCT. " p, ".TABLE_PRODUCT_RELATION. " ptc");;
									$ProductObj->Where = "ptc.ProductID=p.ProductID and ptc.RelationType='category' and ptc.RelationID='".$CurrentCategory->CategoryID."' 
									AND p.ProductType !='Child' AND p.Active='1'".$ProductWhere;
									$OrderBy= " ptc.SortOrder ASC,p.CreatedDate DESC";	
								}
								else
								{
									$ProductObj= new DataTable(TABLE_PRODUCT. " p");;
									$ProductObj->Where = " p.ProductType !='Child' AND  p.Active='1'".$ProductWhere;
									$OrderBy= " p.CreatedDate DESC";	
								}
								
								$OrderBy= " ptc.SortOrder ASC,p.CreatedDate DESC";	
								
								if(isset($SortByArray[$sb]['column']) && $SortByArray[$sb]['column'] != "")
									$OrderBy = $SortByArray[$sb]['column'];
		
								
								if($ps=="ALL")
									$ProductObj->AllowPaging =false;
								else 
									$ProductObj->AllowPaging =true;
								$ProductObj->PageSize=(int)$ps;
		
								$ProductObj->PageNo =isset($_GET['PageNo'])?$_GET['PageNo']:1;	
								$ProductObj->TableSelectAll(array("p.*,$PriceSubQueryField as FinalPrice"),$OrderBy);
								$TotalRecords = $ProductObj->TotalRecords ;
								$TotalPages =  $ProductObj->TotalPages;
								
								//echo $ProductObj->Query;
								if($ProductObj->GetNumRows() > 0)
								{
									
							
									?>
										<?php /*paging start*/?>
										<?php /*
										<div class="pull-right">
										<div class="page-heading">
											<div class="SortBy SearchSelect" >
												<select name="sb" onchange="document.location.href = '<?php echo SKAttrValLinkURL($FilterURL,"-1","-1",array("filter"=>"1"),array("PageNo","sb"))."&sb="?>'+this.value;">
													<?php foreach ($SortByArray as $kk=>$val):?>
													<option value="<?php echo $kk?>" <?php echo $kk==$sb?"selected":"";?> ><?php echo $val['display']?></option>
													<?php endforeach;?>
												  </select>
											</div>
											<div class="SortBy">
											<select name="ps" onchange="document.location.href = '<?php echo SKAttrValLinkURL($FilterURL,"-1","-1",array("filter"=>"1"),array("PageNo","ps"))."&ps="?>'+this.value;">
													<option value="15">View</option>            
													<option value="15" <?php echo $ps=='15'?"selected":"";?>>15</option>
													<option value="30" <?php echo $ps=='30'?"selected":"";?>>30</option>
													<option value="60" <?php echo $ps=='60'?"selected":"";?>>60</option>
													<option value="ALL" <?php echo $ps=='ALL'?"selected":"";?>>ALL</option>
												  </select>
										
											</div>

											
											<?php if($TotalRecords > $ProductObj->PageSize)
											{
												?>
												<div class="SortBy PaginationWrap"><?php echo $ProductObj->GetPagingLinks(SKAttrValLinkURL($FilterURL,"-1","-1",array("filter"=>"1"),array("PageNo"))."&PageNo=",PAGING_FORMAT_MULTIPLE,"","");?></div>
											<?php }?>
											
									 </div>
									</div>
									*/ ?>
									<?php /*paging end*/?>

									 <div style="clear:both"></div>
									  <div class="Category row products text-center">
									  <ul>
													<?php while($CurrentProduct = $ProductObj->GetObjectFromRecord()){
															include(dirname(__DIR__)."/../../product_template.php");}?>
										</ul>
									   </div>
									   <div style="clear:both"></div>   					
										
										<?php /*paging start*/?>
										<div class="pull-right">
										<div class="page-heading">
											<?php /*<div class="SortBy SearchSelect" >
												<select name="sb" onchange="document.location.href = '<?php echo SKAttrValLinkURL($FilterURL,"-1","-1",array("filter"=>"1"),array("PageNo","sb"))."&sb="?>'+this.value;">
													<?php foreach ($SortByArray as $kk=>$val):?>
													<option value="<?php echo $kk?>" <?php echo $kk==$sb?"selected":"";?> ><?php echo $val['display']?></option>
													<?php endforeach;?>
												  </select>
											</div>
											<div class="SortBy">
											<select name="ps" onchange="document.location.href = '<?php echo SKAttrValLinkURL($FilterURL,"-1","-1",array("filter"=>"1"),array("PageNo","ps"))."&ps="?>'+this.value;">
													<option value="15">View</option>            
													<option value="15" <?php echo $ps=='15'?"selected":"";?>>15</option>
													<option value="30" <?php echo $ps=='30'?"selected":"";?>>30</option>
													<option value="60" <?php echo $ps=='60'?"selected":"";?>>60</option>
													<option value="ALL" <?php echo $ps=='ALL'?"selected":"";?>>ALL</option>
												  </select>
										
											</div>

											
											<?php if($TotalRecords > $ProductObj->PageSize)
											{
												?>
												<div class="SortBy PaginationWrap"><?php echo $ProductObj->GetPagingLinks(SKAttrValLinkURL($FilterURL,"-1","-1",array("filter"=>"1"),array("PageNo"))."&PageNo=",PAGING_FORMAT_MULTIPLE,"","");?></div>
											<?php }?>
											*/ ?>
											
									 </div>
									</div>
									<?php /*paging end*/?>
									

									

		<?php  } ?>
                                


 <div style="clear:both"></div>
                                
                                <?php

							
		 }
		else 
		{
			if($ChildCategory ==0 OR @$_REQUEST['filter']=="1")
				echo "<p>Sorry, no products matched your search</p><br><br><br><br>";
		}
		/* product display end*/
		?>
		
	</div>
</div>		


<script type="text/javascript">
jQuery(document).ready(function($){
	  $( "#slider-range" ).slider({
	  stop: function( event, ui ) {
		 	url= '<?php echo SKAttrValLinkURL($FilterURL,"-1","-1",array("filter"=>"1"),array("PageNo","MiP","MxP"))?>&MiP=' + ui.values[ 0 ] + '&MxP=' + ui.values[ 1 ];
		 setTimeout(function(){ 
			document.location.href=url;
		 }, 1000);
	  },	  
      range: true,
      min: <?php echo (isset($MinPrice) && $MinPrice > 0)?$MinPrice:0;?>,
      max: <?php echo (isset($MaxPrice) && $MaxPrice > 0)?$MaxPrice:2500;?>,
      values: [ <?php echo isset($_GET['MiP'])?$_GET['MiP']:$MinPrice?>, <?php echo isset($_GET['MxP'])?$_GET['MxP']:$MaxPrice?> ],
      slide: function( event, ui ) {
        $( "#amount" ).html( "<?php echo $CurrentCurrency->Symbol?>" + ui.values[ 0 ] + " - <?php echo $CurrentCurrency->Symbol?>" + ui.values[ 1 ] );
      }
    });
    $( "#amount" ).html( "<?php echo $CurrentCurrency->Symbol?>" + $( "#slider-range" ).slider( "values", 0 ) +      " - <?php echo $CurrentCurrency->Symbol?>" + $( "#slider-range" ).slider( "values", 1 ) );
	
});
</script>




