
<?php 
			$ProductWhere="";
			if($MiP > 0 && isset($PriceSubQueryField) && $PriceSubQueryField != '')
			{
				$ProductWhere .= " AND ".$PriceSubQueryField." >= '".(int)$MiP."'";
			}
			if($MxP > 0 && isset($PriceSubQueryField) && $PriceSubQueryField != '')
			{
				$ProductWhere .= " AND ".$PriceSubQueryField." <= '".(int)$MxP."'";
			}
			
			if($WherePart !="")
				$ProductWhere .= $WherePart;
	
			
			if(isset($CategoryID) && $CategoryID > 0)
			{
				$AttributeObj = new DataTable(TABLE_ATTRIBUTE." a, ".TABLE_CATEGORY_RELATION." cr");
				$AttributeObj->Where = "a.AttributeID = cr.RelationID AND a.Active = '1' AND cr.RelationType = 'attribute' AND cr.CategoryID = '".$CurrentCategory->CategoryID."'";
				$AttributeObj->TableSelectAll("","SortOrder ASC");
			
			}
			else
			{
				$AttributeObj = new DataTable(TABLE_ATTRIBUTE." a");
				$AttributeObj->Where = "a.Active = '1' AND a.SearchPage = '1'";
				$AttributeObj->TableSelectAll("","AttributeStatus DESC, Position ASC");
			}
			
			if($AttributeObj->GetNumRows() > 0)
			{				
				echo '<div class="filter"><h4>Filter By</h4><div class="row">';
				if(isset($_GET['Att']) && $_GET['Att'] !="")
				{
					foreach ($_GET['Att'] as $k=>$v)
					{
						$v = preg_replace('/[^0-9,]/s', '', $v);	
						$ProductRelationObj = new DataTable(TABLE_PRODUCT_RELATION);
						$ProductRelationObj->Where ="RelationType='attribute_value' AND RelationID in (".$ProductRelationObj->MysqlEscapeString($v).")";
						$CurrentProductRelation = $ProductRelationObj->TableSelectOne(array("GROUP_CONCAT(DISTINCT ProductID ORDER BY ProductID DESC SEPARATOR ',') as ProductIDs"));	
						$CurrentProductRelation->ProductIDs = $CurrentProductRelation->ProductIDs == "" ? "0":$CurrentProductRelation->ProductIDs;
						$ProductWhere .= " AND (p.ProductID in (".$ProductRelationObj->MysqlEscapeString($CurrentProductRelation->ProductIDs).") )";
					}
				}			
				?>
				<?php
				while ($CurrentAttribute = $AttributeObj->GetObjectFromRecord())
				{
					$AttributeValueObj = new DataTable(TABLE_ATTRIBUTE_VALUE);
					$AttributeValueObj->Where = "AttributeID = '".$CurrentAttribute->AttributeID."' AND Active='1'";
					$AttributeValueObj->TableSelectAll("","Position");
				
					$aStr = "";
					while ($CurrentAttributeValue = $AttributeValueObj->GetObjectFromRecord())
					{
						if(isset($CategoryID) && $CategoryID > 0)
						{
							$ProductCountObj = new DataTable(TABLE_PRODUCT." p,".TABLE_PRODUCT_RELATION." pra,".TABLE_PRODUCT_RELATION." prc");
							$ProductCountObj->Where = " p.ProductID = pra.ProductID AND pra.RelationType='attribute_value' AND pra.RelationID = '$CurrentAttributeValue->AttributeValueID'";
							$ProductCountObj->Where .= " AND p.ProductID = prc.ProductID AND prc.RelationType='category' AND prc.RelationID = '$CurrentCategory->CategoryID'".$ProductWhere;					
						}
						else
						{
							$ProductCountObj = new DataTable(TABLE_PRODUCT." p,".TABLE_PRODUCT_RELATION." pra");
							$ProductCountObj->Where = " p.ProductID = pra.ProductID AND pra.RelationType='attribute_value' AND pra.RelationID = '$CurrentAttributeValue->AttributeValueID'";
							$ProductCountObj->Where .= " ".$ProductWhere;					
					
						}
							
						$ProductCount = $ProductCountObj->TableSelectOne(array("COUNT(*) as ProductCount"))->ProductCount;
						if($ProductCount > 0){
							if(in_array($CurrentAttributeValue->AttributeValueID,explode(",",$ExcludeAttVal)))
								$aStr .= "<a href='javascript:;' class='active'>".MyStripSlashes($CurrentAttributeValue->AttributeValue)." (".$ProductCount.")</a>";
							else 
								$aStr .= "<a href='".SKAttrValLinkURL($FilterURL,$CurrentAttributeValue->AttributeID,$CurrentAttributeValue->AttributeValueID)."'>".MyStripSlashes($CurrentAttributeValue->AttributeValue)." (".$ProductCount.")</a>";
						}
						else
						{
							//$aStr .= "<a style='opacity:.5;filter:alpha(opacity=50);' onclick='return confirm(\"You are about to change your selection. Are you sure?\")' href='".SKAttrValLinkURL(SKSEOURL($CategoryID,"shop/category","filter=1"),$CurrentAttributeValue->AttributeID,$CurrentAttributeValue->AttributeValueID)."'>".MyStripSlashes($CurrentAttributeValue->AttributeValue)." (".$ProductCount.")</a>";
						}
					}					
					if($aStr!="")
						echo "<div class='LeftList col-sm-15'>
								<strong class='text-uppercase'>".trim($CurrentAttribute->AttributeName)."</strong>
								<div class='div_wrap'>".$aStr."</div>
							</div>";
				}
				
				
				echo '<div class="LeftList col-sm-15"> <strong class="text-uppercase">By Price Range</strong>
							<p><div id="amount"></div></p>
							<div id="slider-range"></div>
					 </div>';
				
				echo "</div>
					</div>";
			}
			
?>
