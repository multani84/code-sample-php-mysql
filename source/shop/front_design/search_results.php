<?php

/*

Template: Default

*/?>

<link href="<?=DIR_WS_SITE_INCLUDES_JAVASCRIPT?>jquery/flick/css/flick/jquery-ui-1.9.2.custom.css" rel="stylesheet">
<script src="<?=DIR_WS_SITE_INCLUDES_JAVASCRIPT?>jquery/flick/js/jquery-ui-1.9.2.custom.js"></script>
<script src="<?=DIR_WS_SITE_INCLUDES_JAVASCRIPT?>jquery/jquery.ui.touch-punch.min.js"></script>

<div class="products1">   
	<div class="container">       	
           
			<h1>Search Results</h1>

		<?php  /* product display start*/
		
		$CategoryID = isset($_REQUEST['CategoryID'])?$_REQUEST['CategoryID']:"0";
		$MNID = isset($_REQUEST['MNID'])?$_REQUEST['MNID']:"0";
		$Keyword = isset($_REQUEST['k'])?$_REQUEST['k']:"";
		$ProductObj = new DataTable();
		
		$WherePart ="";
		
		if(!empty($Keyword))
		{
			$WherePart .=" AND (";
			if(preg_match("/\+\b/i",$Keyword))
			{
				$Prcident= "AND";
				$SecGate = "1";
				$KeywordArr =  explode("+",$Keyword);
			}
			else 
			{
				/*
					$Prcident= "OR";
					$SecGate = "0";
					$KeywordArr =  explode(" ",$Keyword);
				*/
				$Prcident= "AND";
				$SecGate = "1";
				$KeywordArr =  explode(" ",$Keyword);
			
			}
			foreach ($KeywordArr as $Key=>$Value)
			{
				if(trim($Value) !="")
					$WherePart .="(
							  	ProductName LIKE '%$Value%' 
							  	OR ModelNo LIKE '%$Value%' 
							  	OR p.SmallDescription LIKE '%$Value%' 
							  	OR ProductLine LIKE '%$Value%' 
							  	OR QuickBooksCode LIKE '%$Value%' 
							  	OR GoogleMPN LIKE '%$Value%' 
							  	) $Prcident ";
	    
			}
			
			$WherePart .=" $SecGate)";
		}
			
		
		
		if($CategoryID !=0)
			$ProductObj->TableName =TABLE_PRODUCT. " p, ".TABLE_PRODUCT_RELATION. " ptc";
		else 
			$ProductObj->TableName =TABLE_PRODUCT. " p";
		
		 	
		if($CategoryID !=0)
		 $ProductObj->Where = "p.ProductType!='Child' AND p.Active='1' and ptc.ProductID=p.ProductID and ptc.Relationtype='category' and ptc.RelationID='".$ProductObj->MysqlEscapeString($CategoryID)."'";
		else 
		 $ProductObj->Where = "p.ProductType!='Child' AND p.Active='1' ";
		
		 if(isset($MNID) && $MNID != "0" && $MNID != "")
			 $ProductObj->Where .= " AND p.MNID='".$ProductObj->MysqlEscapeString((int)$MNID)."' ";
		
		 
		 if($WherePart !="")
			$ProductObj->Where .= $WherePart;
		$ProductObj->AllowPaging =true;
		$ProductObj->PageSize=15;
		$ProductObj->PageNo =isset($_GET['PageNo'])?$_GET['PageNo']:1;	
		$ProductObj->TableSelectAll("","Position ASC");
		$TotalRecords = $ProductObj->TotalRecords ;
		$TotalPages =  $ProductObj->TotalPages;
		
		$ProductWhere ="";
		
		if($TotalRecords >0)
		{
			
			?>
			<?php 
		//if(file_exists(dirname(__FILE__)."/custom_templates/category/filter.php"))
			//require_once(dirname(__FILE__)."/custom_templates/category/filter.php");
	?>
			<?php /* product display start*/

						if(isset($ExcludeAttVal) && $ExcludeAttVal !="")
						{?>
							<div class="FilteredBy" style="height:60px">
								<?php foreach ($_GET['Att'] as $attr => $attrVal) {
									$AttributeObj1 = new DataTable(TABLE_ATTRIBUTE);
									$AttributeObj1->Where ="AttributeID = '".$attr."'";
									$CurrentAttribute = $AttributeObj1->TableSelectOne(array("AttributeName"));
										echo "&nbsp;&nbsp;&nbsp;<b>$CurrentAttribute->AttributeName : </b>&nbsp;";

									foreach (explode(",",$attrVal) as $k=>$v)
									{
										$AttributeValueObj = new DataTable(TABLE_ATTRIBUTE_VALUE);
										$AttributeValueObj->Where ="AttributeValueID = '".(int)$v."'";
										$CurrentAttributeValue = $AttributeValueObj->TableSelectOne(array("AttributeID,AttributeValue"));
									?>							
									<a href="<?php echo SKAttrValLinkRemoveURL($FilterURL,$CurrentAttributeValue->AttributeID,$v)?>"><?php echo MyStripSlashes($CurrentAttributeValue->AttributeValue)?></a>
								<?php
									}
								}?>	
							</div>
							<?php
							}
							?>
			<?php
			
								$SortByArray = array(
										"00"=>array("display"=>"Sort by..","column"=>"ptc.SortOrder ASC,p.CreatedDate DESC"),
										"11"=>array("display"=>"Name (A - Z)","column"=>"p.ProductName ASC"),
										"12"=>array("display"=>"Name (Z - A)","column"=>"p.ProductName DESC"),
										"21"=>array("display"=>"Price (Low &gt; High)","column"=>"FinalPrice ASC"),
										"22"=>array("display"=>"Price (High &gt; Low)","column"=>"FinalPrice DESC"),
										"31"=>array("display"=>"Date (Latest)","column"=>"p.CreatedDate DESC"),
										"32"=>array("display"=>"Date (Oldest)","column"=>"p.CreatedDate ASC"),
								);
								
								$SortByArray['00'] = array("display"=>"Sort by..","column"=>"p.CreatedDate DESC");
									
								$ps = isset($_GET['ps'])?(int)$_GET['ps']:"15";
								$sb = isset($_GET['sb'])?$_GET['sb']:"00";
		
								$ProductObj= new DataTable(TABLE_PRODUCT. " p");;
								$ProductObj->Where = " p.ProductType !='Child' AND  p.Active='1'".$ProductWhere;
								$OrderBy= " p.CreatedDate DESC";	
								
								$OrderBy= " ptc.SortOrder ASC,p.CreatedDate DESC";	
								
								if(isset($SortByArray[$sb]['column']) && $SortByArray[$sb]['column'] != "")
									$OrderBy = $SortByArray[$sb]['column'];
		
								
								if($ps=="ALL")
									$ProductObj->AllowPaging =false;
								else 
									$ProductObj->AllowPaging =true;
								$ProductObj->PageSize=(int)$ps;
		
								$ProductObj->PageNo =isset($_GET['PageNo'])?$_GET['PageNo']:1;	
								$ProductObj->TableSelectAll(array("p.*,$PriceSubQueryField as FinalPrice"),$OrderBy);
								$TotalRecords = $ProductObj->TotalRecords ;
								$TotalPages =  $ProductObj->TotalPages;
								
								//echo $ProductObj->Query;
								if($ProductObj->GetNumRows() > 0)
								{
									
							
									?>
										<?php /*paging start*/?>
								<?php /*		<div class="pull-right">
										<div class="page-heading">
											<div class="SortBy SearchSelect" >
												<select name="sb" onchange="document.location.href = '<?php echo SKAttrValLinkURL($FilterURL,"-1","-1",array("filter"=>"1"),array("PageNo","sb"))."&sb="?>'+this.value;">
													<?php foreach ($SortByArray as $kk=>$val):?>
													<option value="<?php echo $kk?>" <?php echo $kk==$sb?"selected":"";?> ><?php echo $val['display']?></option>
													<?php endforeach;?>
												  </select>
											</div>
											<div class="SortBy">
											<select name="ps" onchange="document.location.href = '<?php echo SKAttrValLinkURL($FilterURL,"-1","-1",array("filter"=>"1"),array("PageNo","ps"))."&ps="?>'+this.value;">
													<option value="15">View</option>            
													<option value="15" <?php echo $ps=='15'?"selected":"";?>>15</option>
													<option value="30" <?php echo $ps=='30'?"selected":"";?>>30</option>
													<option value="60" <?php echo $ps=='60'?"selected":"";?>>60</option>
													<option value="ALL" <?php echo $ps=='ALL'?"selected":"";?>>ALL</option>
												  </select>
										
											</div>

											
											<?php if($TotalRecords > $ProductObj->PageSize)
											{
												?>
												<div class="SortBy PaginationWrap"><?php echo $ProductObj->GetPagingLinks(SKAttrValLinkURL($FilterURL,"-1","-1",array("filter"=>"1"),array("PageNo"))."&PageNo=",PAGING_FORMAT_MULTIPLE,"","");?></div>
											<?php }?>
											
									 </div>
									</div>
*/ ?>									<?php /*paging end*/?>

									 <div style="clear:both"></div>
									  <div class="Category row products text-center">
									  <ul>
													<?php while($CurrentProduct = $ProductObj->GetObjectFromRecord()){
															include(dirname(__DIR__)."/product_template.php");}?>
										</ul>
									   </div>
									   <div style="clear:both"></div>   					
										<?php /*paging start*/?>
							<?php /*			<div class="pull-right">
										<div class="page-heading">
											<div class="SortBy SearchSelect" >
												<select name="sb" onchange="document.location.href = '<?php echo SKAttrValLinkURL($FilterURL,"-1","-1",array("filter"=>"1"),array("PageNo","sb"))."&sb="?>'+this.value;">
													<?php foreach ($SortByArray as $kk=>$val):?>
													<option value="<?php echo $kk?>" <?php echo $kk==$sb?"selected":"";?> ><?php echo $val['display']?></option>
													<?php endforeach;?>
												  </select>
											</div>
											<div class="SortBy">
											<select name="ps" onchange="document.location.href = '<?php echo SKAttrValLinkURL($FilterURL,"-1","-1",array("filter"=>"1"),array("PageNo","ps"))."&ps="?>'+this.value;">
													<option value="12">View</option>            
													<option value="15" <?php echo $ps=='15'?"selected":"";?>>15</option>
													<option value="30" <?php echo $ps=='30'?"selected":"";?>>30</option>
													<option value="60" <?php echo $ps=='60'?"selected":"";?>>60</option>
													<option value="ALL" <?php echo $ps=='ALL'?"selected":"";?>>ALL</option>
												  </select>
										
											</div>

											
											<?php if($TotalRecords > $ProductObj->PageSize)
											{
												?>
												<div class="SortBy PaginationWrap"><?php echo $ProductObj->GetPagingLinks(SKAttrValLinkURL($FilterURL,"-1","-1",array("filter"=>"1"),array("PageNo"))."&PageNo=",PAGING_FORMAT_MULTIPLE,"","");?></div>
											<?php }?>
											
									 </div>
									</div>
									<?php /*paging end*/?>
									

									

		<?php  } ?>
                                


 <div style="clear:both"></div>
                                
                                <?php

							
		 }
		else 
		{
			if($ChildCategory ==0 OR @$_REQUEST['filter']=="1")
				echo "<p>Sorry, no products matched your search</p><br><br><br><br>";
		}
		/* product display end*/
		?>
		
	</div>
		
</div>

<script type="text/javascript">
jQuery(document).ready(function($){
	  $( "#slider-range" ).slider({
	  stop: function( event, ui ) {
		 	url= '<?php echo SKAttrValLinkURL($FilterURL,"-1","-1",array("filter"=>"1"),array("PageNo","MiP","MxP"))?>&MiP=' + ui.values[ 0 ] + '&MxP=' + ui.values[ 1 ];
		 setTimeout(function(){ 
			document.location.href=url;
		 }, 1000);
	  },	  
      range: true,
      min: <?php echo (isset($MinPrice) && $MinPrice > 0)?$MinPrice:0;?>,
      max: <?php echo (isset($MaxPrice) && $MaxPrice > 0)?$MaxPrice:2500;?>,
      values: [ <?php echo isset($_GET['MiP'])?$_GET['MiP']:$MinPrice?>, <?php echo isset($_GET['MxP'])?$_GET['MxP']:$MaxPrice?> ],
      slide: function( event, ui ) {
        $( "#amount" ).html( "<?php echo $CurrentCurrency->Symbol?>" + ui.values[ 0 ] + " - <?php echo $CurrentCurrency->Symbol?>" + ui.values[ 1 ] );
      }
    });
    $( "#amount" ).html( "<?php echo $CurrentCurrency->Symbol?>" + $( "#slider-range" ).slider( "values", 0 ) +      " - <?php echo $CurrentCurrency->Symbol?>" + $( "#slider-range" ).slider( "values", 1 ) );
	
});
</script>




