<div id="MainContent">
    	<div class="container"> 
            <ul class="breadcrumb">
                <li><a href="<?php echo SKSEOURL("1001","cms")?>">Home</a></li>
                <li class="active">Your Details</li>
            </ul>
        	<h1><?php  
			echo (isset($CurrentPage->LeftTitle1)?MyStripSlashes($CurrentPage->LeftTitle1):"&nbsp;")?></h1>
				<?php 
				echo (isset($CurrentPage->LeftDescription1)?MyStripSlashes($CurrentPage->LeftDescription1):"&nbsp;");
				require_once(DIR_FS_SITE_INCLUDES."message.php");?>
		   
				<div id="CheckoutPage">
					<?php 
					$ShoppingCalculationObj->Calculate();
					$Editable = false;
					$SessionID = session_id();
					
					$SKCartTable =TABLE_TMPCART;
					$CartObj = new DataTable(TABLE_TMPCART);
					$CartObj2 = new DataTable(TABLE_TMPCART);
					
					$CartObj->Where =" SessionID='".$CartObj->MysqlEscapeString($SessionID)."' AND (ParentItemID='' OR ISNULL(ParentItemID))";
					$CartObj->TableSelectAll("","TmpCartID ASC");
					
					if ($CartObj->GetNumRows()>0)
					{
						require_once(dirname(__FILE__)."/../show_cart.php");
						?>
						<form id="CartForm" name="CartForm" method="POST" action="">			
						<table cellpadding="0" cellspacing="0" border="0" width="95%">
						 <tr>
							<td align="center"><br>
							<a name="CartUpdatel" href="<?php  echo MakePageURL("index.php","Page=shop/shop_cart")?>" class="Button">Edit Cart</a>
							<br />
							<br />
						</td>
						</tr>
					</table>
					</form>
					<?php 
					if(function_exists("CheckCouponStatus") && CheckCouponStatus() ===true && false)
					{?>
						<form id="CouponForm" name="CouponForm" method="POST" action="<?php  echo MakePageURL("index.php","Page=$Page&Target=CouponProcess")?>" onSubmit="return ValidateForm(this);">
									
							 <table class="table table-striped table-bordered">
								<thead>
									<tr>
										<th colspan="3">COUPON CODE/VOUCHER CODE</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td class="text-right">Enter Code</td>
										<td><input type="text" name="VoucherCode" id="VoucherCode_R" value="<?php  echo isset($VoucherCode)?$VoucherCode:""?>" size="10" title="<?php  echo @constant("DEFINE_BLANK_VOUCHER_CODE_MSG")?>" class="form-control"></td>
										<td><button type="submit" class="btn btn-default Button">Submit</button></td>
									</tr>
								</tbody>
							</table><br /><br />
						</form>
					<?php 
					}
					
					}
					else
					{
						 MyRedirect(MakePageURL("index.php","Page=shop/shop_cart")); 	
		  				 exit;
					}?>
					<br>
					<br>
					<?php /* Shipping Information Display start*/?>
					<script type="text/javascript">
					function SameBillingAddress(Obj)
					{
						if(Obj.checked==true)
						{
						
							jQuery('#ShippingFirstName_R').val(jQuery('#BillingFirstName_R').val());
							jQuery('#ShippingLastName_R').val(jQuery('#BillingLastName_R').val());
							jQuery('#ShippingAddress1_R').val(jQuery('#BillingAddress1_R').val());
							jQuery('#ShippingAddress2').val(jQuery('#BillingAddress2').val());
							jQuery('#ShippingCity_R').val(jQuery('#BillingCity_R').val());
							jQuery('#ShippingCountry_R').val(jQuery('#BillingCountry_R').val()).trigger("change");
							jQuery('#ShippingState').val(jQuery('#BillingState').val());
							jQuery('#ShippingZip_I').val(jQuery('#BillingZip_I').val());
							jQuery('#ShippingPhone_R').val(jQuery('#BillingPhone_R').val());
							
							jQuery("#CheckoutForm").find("label.error").hide();
							
						}
					}
					</script>
				<form class="form-horizontal" role="form" id="CheckoutForm" name="CheckoutForm" method="POST" action="<?php  echo MakePageURL("index.php","Page=$Page&Target=OrderProcess")?>">
				
	     
 <div class="row">
                    <div class="col-md-6">
                        <div class="panel panel-warning">
                            <div class="panel-heading">Billing Address</div>
                            <div class="panel-body">
                                
                            	<?php if(isset($CurrentUserObj->UserID) && $CurrentUserObj->UserID !="" && @constant("DEFINE_MULTIPLE_USER_ADDRESS")=="1")
							{
								$AddressObj->Where ="UserID='".$CurrentUserObj->UserID."'";
								$AddressObj->TableSelectAll("","CreatedDate ASC");
							?> 
						
						 	<div class="form-group">
                                    <label class="control-label col-sm-4" for="email">Select billing address from your address book</label>
                                    <div class="col-sm-8"><select name="BAID" id="BAID" style="width:300px;" onchange="document.location.href='<?php  echo MakePageURL("index.php","Page=$Page&SAID=$SAID&BAID=")?>'+this.value;">
									<option value="0">Select billing address from your address book</option>
									<option value="-1"><?php echo $CurrentUserObj->FirstName." ".$CurrentUserObj->LastName. " " .$CurrentUserObj->Address1." ".$CurrentUserObj->Address2." ".$CurrentUserObj->City." ".$CurrentUserObj->State." ".$CurrentUserObj->Country." ".$CurrentUserObj->ZipCode?></option>
									<?php 
									 while ($CurrentAddress = $AddressObj->GetObjectFromRecord())
                        			{
									?>
									<option value="<?php  echo $CurrentAddress->AddressID?>" <?php  echo $BAID==$CurrentAddress->AddressID?"selected":""?>><?php echo $CurrentAddress->FirstName." ".$CurrentAddress->LastName. " " .$CurrentAddress->Address1." ".$CurrentAddress->Address2." ".$CurrentAddress->City." ".$CurrentAddress->State." ".$CurrentAddress->Country." ".$CurrentAddress->ZipCode?></option>
									<?php 
                        			}?>
									<!-- <option value="-2">Add new address to My Address Book</option> -->
									</select></div>
                                </div>
							
							
						 <?php  }?>
                            <div class="form-group">
                                    <label class="control-label col-sm-4" for="email">Email: <b>*</b></label>
                                    <div class="col-sm-8"><input type="email" class="form-control" name="Email" id="Email_E" value="<?php  echo isset($Email)?$Email:""?>" title="Invalid email ! Please try user@domain.com" placeholder="e.g. user@domain.com" required /></div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-4" for="Name">First Name: <b>*</b></label>
                                    <div class="col-sm-8"><input type="text" class="form-control" value="<?php  echo isset($BillingFirstName)?$BillingFirstName:""?>" name="BillingFirstName" id="BillingFirstName_R" title="Please enter Billing First name" placeholder="First Name" required/></div>
                                </div>
                                 <div class="form-group">
                                   <label class="control-label col-sm-4" for="Name">Last Name: </label>
                                    <div class="col-sm-8"><input type="text" class="form-control" value="<?php  echo isset($BillingLastName)?$BillingLastName:""?>" name="BillingLastName" id="BillingLastName_R" title="Please enter Billing Last name" placeholder="Last Name"/></div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-4" for="email">Country: <b>*</b></label>
                                    <div class="col-sm-8"><select class="form-control" rel="sk_country" data-regionid="#BillingState" data-value="<?php  echo isset($BillingCountry)?$BillingCountry:""?>"  name="BillingCountry" id="BillingCountry_R" class="txtBox" title="Please enter Billing Country" required></select></div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-4" for="email">Zip/Post Code: <b>*</b></label>
                                    <div class="col-sm-8"><input type="text" class="form-control" value="<?php  echo isset($BillingZip)?$BillingZip:""?>"  name="BillingZip" id="BillingZip_I" title="Please enter valid Billing Zip Code" placeholder="Zip/Post code" required /></div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-4" for="email">Address Line1: <b>*</b></label>
                                    <div class="col-sm-8"><input type="text" class="form-control"  value="<?php  echo isset($BillingAddress1)?$BillingAddress1:""?>" name="BillingAddress1" id="BillingAddress1_R" title="Please enter Billing Address1" placeholder="Address Line 1" required/></div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-4" for="email">Address Line2: </label>
                                    <div class="col-sm-8"><input type="text" class="form-control" value="<?php  echo isset($BillingAddress2)?$BillingAddress2:""?>" name="BillingAddress2" id="BillingAddress2" placeholder="Address Line 2" /></div>
                                </div>
                                  <div class="form-group">
                                    <label class="control-label col-sm-4" for="email">City/Town: <b>*</b></label>
                                    <div class="col-sm-8"><input type="text" class="form-control" value="<?php  echo isset($BillingCity)?$BillingCity:""?>" name="BillingCity" id="BillingCity_R" title="Please enter Billing City" placeholder="City/Town" required /></div>
                                </div>
                                  <div class="form-group">
                                    <label class="control-label col-sm-4" for="email">State/County: <b>*</b></label>
                                    <div class="col-sm-8"
										data-input='<input name="BillingState" id="BillingState" value="<?=isset($BillingState)?$BillingState:""?>" class="form-control" title="Please enter Billing State" required />'
										data-select='<select data-val="<?=isset($BillingState)?$BillingState:""?>" name="BillingState" id="BillingState" class="form-control" title="Please select Billing State" required></select>'
									>
									<input type="text" class="form-control" value="<?php  echo isset($BillingState)?$BillingState:""?>" name="BillingState" id="BillingState" title="Please enter Billing State" placeholder="State" required /></div>
                                </div>
                                                        
                                <div class="form-group">
                                    <label class="control-label col-sm-4" for="Telephone">Telephone: <b>*</b></label>
                                    <div class="col-sm-8"><input type="tel" class="form-control" value="<?php  echo isset($BillingPhone)?$BillingPhone:""?>" name="BillingPhone" id="BillingPhone_R" title="Please enter Billing Phone" placeholder="555-5555-555" required/></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="panel panel-warning">
                            <div class="panel-heading">Shipping Address <label class="checkbox-inline SameAsBilling pull-right"><input type="checkbox" name="SameAddress" id="SameAddress" value="1" class="chk" onClick="return SameBillingAddress(this);"/>Same as Billing</label></div>
                            <div class="panel-body">
                             	<?php if(isset($CurrentUserObj->UserID) && $CurrentUserObj->UserID !="" && @constant("DEFINE_MULTIPLE_USER_ADDRESS")=="1")
							{
								$AddressObj->Where ="UserID='".$CurrentUserObj->UserID."'";
								$AddressObj->TableSelectAll("","CreatedDate ASC");
							?> 
						
						 	<div class="form-group">
                                    <label class="control-label col-sm-4" for="email">Select billing address from your address book</label>
                                    <div class="col-sm-8"><select name="SAID" id="SAID" style="width:300px;" onchange="document.location.href='<?php  echo MakePageURL("index.php","Page=$Page&SAID=$SAID&BAID=")?>'+this.value;">
									<option value="0">Select shipping address from your address book</option>
									<option value="-1"><?php echo $CurrentUserObj->FirstName." ".$CurrentUserObj->LastName. " " .$CurrentUserObj->Address1." ".$CurrentUserObj->Address2." ".$CurrentUserObj->City." ".$CurrentUserObj->State." ".$CurrentUserObj->Country." ".$CurrentUserObj->ZipCode?></option>
									<?php 
									 while ($CurrentAddress = $AddressObj->GetObjectFromRecord())
                        			{
									?>
									<option value="<?php  echo $CurrentAddress->AddressID?>" <?php  echo $BAID==$CurrentAddress->AddressID?"selected":""?>><?php echo $CurrentAddress->FirstName." ".$CurrentAddress->LastName. " " .$CurrentAddress->Address1." ".$CurrentAddress->Address2." ".$CurrentAddress->City." ".$CurrentAddress->State." ".$CurrentAddress->Country." ".$CurrentAddress->ZipCode?></option>
									<?php 
                        			}?>
									<!-- <option value="-2">Add new address to My Address Book</option> -->
									</select></div>
                                </div>
							
							
						 <?php  }?>
                               
                                <div class="form-group">
                                    <label class="control-label col-sm-4" for="Name">First Name: <b>*</b></label>
                                    <div class="col-sm-8"><input type="text" class="form-control" value="<?php  echo isset($ShippingFirstName)?$ShippingFirstName:""?>" name="ShippingFirstName" id="ShippingFirstName_R" title="Please enter Shipping First name" placeholder="First Name" required /></div>
                                </div>
                                <div class="form-group">
                                   <label class="control-label col-sm-4" for="Name">Last Name:</label>
                                    <div class="col-sm-8"><input type="text" class="form-control" value="<?php  echo isset($ShippingLastName)?$ShippingLastName:""?>" name="ShippingLastName" id="ShippingLastName_R" title="Please enter Shipping Last name" placeholder="Last Name"/></div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-4" for="email">Country: <b>*</b></label>
                                    <div class="col-sm-8"><select rel="sk_country" data-regionid="#ShippingState" data-value="<?php  echo isset($ShippingCountry)?$ShippingCountry:""?>" name="ShippingCountry" id="ShippingCountry_R" class="form-control" title="Please enter Shipping Country" required></select>	</div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-4" for="email">Zip/Post Code: <b>*</b></label>
                                    <div class="col-sm-8"><input type="text" class="form-control" value="<?php  echo isset($ShippingZip)?$ShippingZip:""?>"  name="ShippingZip" id="ShippingZip_I" title="Please enter valid zip code" placeholder="Zip/Post code" required/></div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-4" for="email">Address Line1: <b>*</b></label>
                                    <div class="col-sm-8"><input type="text" class="form-control"  value="<?php  echo isset($ShippingAddress1)?$ShippingAddress1:""?>" name="ShippingAddress1" id="ShippingAddress1_R" title="Please enter Shipping Address1" placeholder="Address Line 1" required /></div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-4" for="email">Address Line2: </label>
                                    <div class="col-sm-8"><input type="text" class="form-control" value="<?php  echo isset($ShippingAddress2)?$ShippingAddress2:""?>" name="ShippingAddress2" id="ShippingAddress2" placeholder="Address Line 2" /></div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-4" for="email">City/Town: <b>*</b></label>
                                    <div class="col-sm-8"><input type="text" class="form-control" value="<?php  echo isset($ShippingCity)?$ShippingCity:""?>" name="ShippingCity" id="ShippingCity_R" title="Please enter Shipping City" placeholder="City/Town" required /></div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-4" for="email">State/County: <b>*</b></label>
                                    <div class="col-sm-8"
										data-input='<input class="form-control" name="ShippingState" id="ShippingState" value="<?php  echo isset($ShippingState)?$ShippingState:""?>" title="Please enter Shipping State" required />'
										data-select='<select data-val="<?=isset($ShippingState)?$ShippingState:""?>" name="ShippingState" id="ShippingState" class="form-control" title="Please select Shipping State" required></select>'
									>
											<input class="form-control" name="ShippingState" id="ShippingState" value="<?php  echo isset($ShippingState)?$ShippingState:""?>" title="Please enter Shipping State" required />
							           </div>
								   </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-4" for="Telephone">Telephone: <b>*</b></label>
                                    <div class="col-sm-8"><input type="tel" class="form-control" value="<?php  echo isset($ShippingPhone)?$ShippingPhone:""?>" name="ShippingPhone" id="ShippingPhone_R" title="Please enter Shipping Phone" placeholder="555-5555-555" required /></div>
                                </div>
								
								<div class="form-group">
                                    <label class="control-label col-sm-4" for="Telephone">&nbsp;</label>
                                    <div class="col-sm-8">&nbsp;</div>
                                </div>
								
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
			  			<div class="col-xs-12"><h2>Shipping Method</h2>
						 <div id="ShippingMethod">Please complete shipping Address to view shipping quote.</div>
					</div>


					<div class="form-group">
			  			<div class="col-xs-12"><h2>GSTIN</h2>
						 <div class="col-sm-12">
						<?php if(d("QTY_FOR_GSTIN")>$CustomObj->CartQuantity()){?>
						 <input type="text" class="form-control" value="<?php  echo isset($GSTIN)?$GSTIN:""?>" name="GSTIN" id="GSTIN" title="Please enter GSTIN" placeholder="GSTIN"  />
						 <?php } else{?>
						 <input type="text" class="form-control" value="<?php  echo isset($GSTIN)?$GSTIN:""?>" name="GSTIN" id="GSTIN_R" title="Please enter GSTIN" placeholder="GSTIN" required />
						 <?php } ?>
						 </div>
						
					</div>

				</div>
				
			
				<input type="hidden" class="form-control partnercode" value="<?php  echo isset($ReferenceCode)?$ReferenceCode:""?>" name="PartnerCode" id="PartnerCode" title="Please enter Partner code" placeholder="" />						
				

			 <div class="form-group">
				<div class="col-xs-12"><h2>Comments</h2>
				<textarea name="Comments" id="Comments" class="form-control" placeholder="Click to enter comments..." style=" height:100px; "><?php  echo isset($Comments)?$Comments:""?></textarea>						
				</div>
			</div>
			<div class="form-group">
				<div class="col-xs-12 text-center"><button type="submit" class="ButtonMain">Continue</button></div>
			</div>
			<p>&nbsp;</p>
			<p>&nbsp;</p>
		</div>
</form>
<?php 
include(dirname(__FILE__)."/checkout.js.php");
?>
	
	
	

