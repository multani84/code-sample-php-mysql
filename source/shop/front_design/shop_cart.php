 <div id="MainContent">
    	<div class="container"> 
            <ul class="breadcrumb">
                <li><a href="<?php echo SKSEOURL("1001","cms")?>">Home</a></li>
                <li class="active">Shopping Cart</li>
            </ul>
        	<h1>Shopping Cart</h1>
            <div class="row">
			<?php echo (isset($CurrentPage->LeftDescription1)?MyStripSlashes($CurrentPage->LeftDescription1):"&nbsp;");?>
		   
				<?php  require_once(DIR_FS_SITE_INCLUDES."message.php");?>
					<form id="FormViewCart" name="FormViewCart" method="POST" action="<?php  echo MakePageURL("index.php","Page=$Page&Target=UpdateCart")?>" class="col-sm-12">
			      	 	<?php 													      	 	
						$ShoppingCalculationObj->Calculate();
						$Editable = true;
						$SessionID = session_id();
						$SKCartTable =TABLE_TMPCART;
						$CartObj = new DataTable(TABLE_TMPCART);
						$CartObj2 = new DataTable(TABLE_TMPCART);
						$CartObj->Where =" SessionID='".$CartObj->MysqlEscapeString($SessionID)."' AND (ParentItemID='' OR ISNULL(ParentItemID))";
						$CartObj->TableSelectAll("","TmpCartID ASC");
						if ($CartObj->GetNumRows()>0){
							require_once(dirname(__FILE__)."/../show_cart.php");
						?>
						 <div class="text-center">
					           <a href="javascript:document.getElementById('FormViewCart').submit();" class="btn btn-default Button">Recalculate</a>
							   <a href="<?php echo isset($_SESSION['RecentCatID'])?SKSEOURL($_SESSION['RecentCatID'],"shop/category"):SKSEOURL(1001,'cms')?>" class="btn btn-default Button">Continue Shopping</a>
							   <a href="?Target=CartSubmit" class="btn btn-default Button">Checkout</a>
					     </div><br />
						</form>
						<?php 
							if(function_exists("CheckCouponStatus") && CheckCouponStatus() ===true)
							{?>
							<br>
							 <form id="CouponForm" name="CouponForm" method="POST" action="<?php  echo MakePageURL("index.php","Page=$Page&Target=CouponProcess")?>" >
							 <table class="table table-striped table-bordered">
									<thead>
										<tr>
											<th colspan="3">COUPON CODE/VOUCHER CODE</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td class="text-right">Enter Code</td>
											<td><input type="text" name="VoucherCode" id="VoucherCode_R" value="<?php  echo isset($VoucherCode)?$VoucherCode:""?>" class="form-control" title="<?php  echo @constant("DEFINE_BLANK_VOUCHER_CODE_MSG")?>" required></td>
											<td><button type="submit" class="btn btn-default Button">Submit</button></td>
										</tr>
									</tbody>
								</table><br /><br />
							</form>
						<?php
							}
						}
						else
						{?>
						<table cellpadding="3" cellspacing="2" border="0" width="95%">
							<tr>
								<td height="15"></td>
							</tr>
							<tr>
								<td align="center">
								<b>Your shopping cart is empty.</b></td>
							</tr>
						</table>			
							<?php 
						}
						?>
			</div>
			<?php /* middle content end*/?>
  
        </div>
    </div>


