<div id="MainBanners" class="clearfix"><img src="<?php echo DIR_WS_SITE_TEMPLATES.DEFINE_TEMPLATE_STYLE?>/images/bnr.jpg" alt="Banner" /></div>
<div id="MainContent" class="clearfix"> 
	<div class="CMSContent">
	<?php require_once(DIR_FS_SITE_INCLUDES."breadcrumb.php");?>
    <h2 class='Title'>Download Area</h2>
			<?php/* middle content start*/?>
				<?php
				require_once(DIR_FS_SITE_INCLUDES."message.php");?>
		    	<div style="display:inline-block;width:100%;">
					<?php
	    if(isset($DownloadArray) && count($DownloadArray) >0)
	    {
	    	?>
	    	<table border="0" cellpadding="2" cellspacing="0" width="400" class="TableFormat">
			<?php
	    	foreach ($DownloadArray as $k=>$v)
	    	{
	    		?>
	    		<tr>
					<td style="padding:5px;"><?php echo ($k +1)?></td>
					<td style="padding:5px;"><a href="<?php echo MakePageURL("index.php","Page=$Page&DownloadID=$DownloadID&Index=$k");?>"><?php echo $v?></a></td>
				</tr>
		   		<?php		
	    	}
	    	?>
	    	</table>
	    	<?php
	    }
	    else 
	    {
	    ?>
			<br>Please enter the Download-ID you've got in the e-mail and click 'Start Download'.<br><br>
				<form id="DownLoadForm" name="DownLoadForm" action="<?php echo MakePageURL("index.php","Page=$Page");?>" method="GET">
				<table border="0" cellpadding="2" cellspacing="0" width="400" class="TableFormat">
					<tr>
						<td style="padding:5px;">Download ID <input type="text" name="DownloadID" id="DownloadID_R" title="please enter your download ID" value="<?php echo @$_REQUEST['DownloadID']?>" size="25">
							<a name="submit" href="Javascript:;" onclick="return ValidateForm('DownLoadForm');" class="frmButton">Start Download</a>
				             <input type="image" name="DefaultSubmit" src="<?php echo DIR_WS_SITE_GRAPHICS?>dot.png" class="DefaultSubmit">
				        </td>
					</tr>
					</table>
				</form>
				<?php
	    }?>
							
		<br>
				</div>
				<?php/* middle content end*/?>
	</div>
</div>
