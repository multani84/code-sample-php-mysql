<div id="MainContent">
    	<div class="container"> 
            <ul class="breadcrumb">
                <li><a href="<?php echo SKSEOURL("1001","cms")?>">Home</a></li>
                <li class="active">Your Details</li>
            </ul>
        	<h1><?php  echo MyStripSlashes($CurrentPage->LeftTitle1)?></h1>
		
	

				<?php 
				echo (isset($CurrentPage->LeftDescription1)?MyStripSlashes($CurrentPage->LeftDescription1):"&nbsp;");
				require_once(DIR_FS_SITE_INCLUDES."message.php");?>
					<div id="CheckoutPage">
						<?php 
						$ShoppingCalculationObj->OrderID = $OrderID;
						$CurrencyOrderID = $OrderID;
						$ShoppingCalculationObj->Calculate();
						
						$SKCartTable =TABLE_ORDER_DETAILS;
						$CartObj = new DataTable(TABLE_ORDER_DETAILS);
						$CartObj2 = new DataTable(TABLE_ORDER_DETAILS);
						
						$CartObj->Where =" OrderID='".$CartObj->MysqlEscapeString($OrderID)."' AND (ParentItemID='' OR ISNULL(ParentItemID))";
						$CartObj->TableSelectAll("","OrderDetailID ASC");
						if ($CartObj->GetNumRows()>0)
						{
							require_once(dirname(__FILE__)."/../show_cart.php");
						?>
							<table cellpadding="0" cellspacing="0" align="center" border="0" width="90%">
							 <tr>
								<td align="center"><br>
								<a name="CartUpdatel" href="<?php  echo MakePageURL("index.php","Page=shop/shop_cart")?>" class="Button">Edit Cart</a>
								<br>
							</td>
							</tr>
						</table>
					  
				<?php 
		}
		else
		{
			MyRedirect(MakePageURL("index.php","Page=shop/shop_cart"));
			exit;
		}?>
		<br>
		<br>
		<?php require_once(dirname(__FILE__)."/../shipping_detail.php");?>
		
		<div class="text-center"><a name="CartUpdatel" href="<?php  echo MakePageURL("index.php","Page=shop/shop_checkout")?>"  class="Button">Edit Information </a></div>
				
		<?php if(isset($ShippingAlert) && is_array($ShippingAlert) && count($ShippingAlert) > 0){?>
			<div id="ShippingAlertDiv">
					<br>
					<span style="color:#ff0000;">We are sorry. The below product(s) are not available for delivery to your area.
					<br>
					Please remove the following item(s) from your shopping basket.<br></span>
					<br>
					<ul>
					<?php 
					foreach ($ShippingAlert as $k=>$arr)
					{
						$obj = $arr['Obj'];
						?>
						<li><b><?php echo $obj->ItemName?></b></li>
						<?php 
					}
					?>
					</ul>
					<br>
					
					<div class="text-center"><a href="<?php  echo MakePageURL("index.php","Page=shop/shop_cart")?>" class="Button">OK</a></div>
					
			</div>
			<script type="text/javascript">
			jQuery(document).ready(function($){
				jQuery.colorbox({	inline:true,
									href:"#ShippingAlertDiv",
									width:350, 
									height:350
								});
			});
			</script>
	<?php 
		}
		else 
		{
			?>
			<p>&nbsp;</p>
			<div class="StyledBox">
				 <table width="100%" cellpadding="0" class="table table-striped table-bordered gstin-div" cellspacing="0" >
					<tr>
						<td style="text-align:left;"><strong>GSTIN</strong></td>
					
						<td align="left">
							<div id="PaymentMethod">
							 
							<?php echo $CurrentOrder->GSTIN;?>
							 </div>
						</td>
					</tr>
				</table>
			</div>

			<form class="form-horizontal" role="form" id="CheckoutForm" name="CheckoutForm" method="POST" action="<?php  echo MakePageURL("index.php","Page=$Page&Target=PaymentProcess")?>">
			<?php /* <div class="StyledBox">
				 <table width="100%" cellpadding="0" cellspacing="0" >
					<tr>
						<th style="text-align:left;"><h2>Payment Method</h2></th>
					</tr>
					<tr>
						<td align="left">
							<div id="PaymentMethod">
							 <?php */ $DirectPaymentCall="html"; 
									require_once(DIR_FS_SITE_PAYMENT."payment.php");
							/* ?>
							 </div>
						</td>
					</tr>
				</table>
			</div> */ ?>
			<div class="text-center">
					<button type="submit" class="ButtonMain">Pay Now</button>
			</div>
			</form>
			<div id="ValidateError" class="error"></div>
			<?php 
		}?>
				</div>
			 
		<?php /* middle content end*/?>
			<p>&nbsp;</p>
			<p>&nbsp;</p>
	</div>
</div>

<style type="text/css">
.payment_method_li .note{font-style:italic;font-size:10px;padding:0px 0px 10px 50px;}
.payment_method_li .hidenote{display:none;padding:0px 0px 10px 50px;}
</style>
<script type="text/javascript">
jQuery(document).ready(function($){
	
	var validator = $("#CheckoutForm").validate({
		errorLabelContainer: $('#ValidateError'),
		  submitHandler: function(form) {
				  return true;
			  
			}
	});

	$( "#PaymentMethod").delegate( ".payment_method_li .radio", "click", function() {
	  if($(this).is(':checked'))
	  {
		 
			$( "#PaymentMethod .hidenote").hide();
			$(this).parents("li").find(".hidenote").show();
	  }
	});
	
	<?php if(isset($CurrentOrder->PaymentMethod) && $CurrentOrder->PaymentMethod != ""):?>	
			$( "input[name=payment_method][value=<?php echo $CurrentOrder->PaymentMethod?>]").attr("checked",true).trigger("click");
		<?php endif;?>		

	
});
</script>

