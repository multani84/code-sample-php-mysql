<section id="MainContent">
        <div id="CMSPage" class="container vh100">
            <div class="EditorContent">
                <h1>Order History</h1>
				<?php/* middle content start*/?>
				<div style="display:inline-block;width:100%;">
					<?php
		switch ($Section)
		{
			case "FullDetail":
				$OrderID = $OID;
				
				$OrderObj->Where ="UserID='".$OrderObj->MysqlEscapeString($CurrentUserObj->UserID)."' AND OrderID = '".$OrderID."'";
				$CurrentOrder = $OrderObj->TableSelectOne(array("*, DATE_FORMAT(CreatedDate,'%M %d, %Y %r')as MyCreatedDate, DATE_FORMAT(CreatedDate,'%M %d, %Y')as MyShortCreatedDate, DATE_FORMAT(CreatedDate,'%y%m%d')as FormattedCreatedDate"));
				if(is_object($CurrentOrder)):
				
					$CurrencyOrderID = $CurrentOrder->OrderID;		
					$ShoppingCalculationObj->OrderID = $OrderID;
					$ShoppingCalculationObj->Calculate();
						
					$SKCartTable =TABLE_ORDER_DETAILS;
					$CartObj = new DataTable(TABLE_ORDER_DETAILS);
					$CartObj2 = new DataTable(TABLE_ORDER_DETAILS);

					$CartObj->Where =" OrderID='".$CartObj->MysqlEscapeString($OrderID)."'";
					$CartObj->TableSelectAll(array("*"));

					$OrderParcelObj = new DataTable(TABLE_ORDER_PARCEL);	
					$OrderParcelObj->Where =" OrderID='".$OrderParcelObj->MysqlEscapeString($OrderID)."'";
					$OrderParcelObj->TableSelectAll();
					
								
					$CartObj->Where =" OrderID='".$CartObj->MysqlEscapeString($OrderID)."' AND (ParentItemID='' OR ISNULL(ParentItemID))";
					$CartObj->TableSelectAll("","OrderDetailID ASC");
								
					$OrderTrackingObj = new DataTable();
					$OrderTrackingObj->TableName = TABLE_ORDER_TRACKING." t, ".TABLE_ORDER_DETAILS." od";
					$OrderTrackingObj->Where = "od.OrderID='".$OrderTrackingObj->MysqlEscapeString($OrderID)."' AND t.OrderDetailID = od.OrderDetailID";
					$OrderTrackingObj->TableSelectAll();
				
				
			?>
			
<ul class="nav nav-tabs">
  <li class="active"><a data-toggle="tab" href="#OrderSummary"> 1. Order Details</a></li>
  <li><a data-toggle="tab" href="#TrackingDetails">2. Tracking Details</a></li>
  <li><a data-toggle="tab" href="#OrderStatus">3. Status/notes</a></li>
</ul>

<div class="tab-content">
  <div id="OrderSummary" class="tab-pane fade in active">
    <h3>Order Summary</h3>
		<?php
			if ($CartObj->GetNumRows()>0)
				require_once(DIR_FS_SITE_INCLUDES."order_summary.php");
			
		?>
  </div>
  <!--  Tab Content Block -->
			<div class="tab-pane fade" id="TrackingDetails">
				<h3>Tracking Details</h3>
				<?php
 
				if($OrderTrackingObj->GetNumRows() > 0)
				{
					
					while($CurrentTracking = $OrderTrackingObj->GetObjectFromRecord())
					{
						if(isset($CurrentTracking->tracking_code))
						{
							$data = SKGetTrackingInfo($CurrentTracking);
							?>
							<div class="well">
								<fieldset>
										<div class="row">
											<div class="col-sm-5"><strong>Track #</strong></div>
											<div class="col-sm-7 text-left"><?php echo $CurrentTracking->tracking_code?></div>
											<div class="col-sm-5"><strong>Label</strong></div>
											<div class="col-sm-7 text-left">
													<?php if($CurrentTracking->label_url != ""):?>
														<a href="<?php echo $CurrentTracking->label_url?>" target="_blank">[LABEL]</a>&nbsp;&nbsp;&nbsp;&nbsp;
													<?php endif;?>
													<?php if($CurrentTracking->label_pdf_url != ""):?>
														<a href="<?php echo $CurrentTracking->label_pdf_url?>" target="_blank">[LABEL PDF]</a>&nbsp;&nbsp;&nbsp;&nbsp;
													<?php endif;?>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-5"><strong>Carrier</strong></div>
											<div class="col-sm-7 text-left"><?php echo $CurrentTracking->ItemName?></div>
											<div class="col-sm-5"><strong>Delivery Date(Est.)</strong></div>
											<div class="col-sm-7 text-left"><?php echo SKGetQuoteDate($CurrentTracking->delivery_date)?></div>
										</div>
										
										
									</fieldset>
							</div>
							<?php
							if(isset($data->tracking_details) && count($data->tracking_details) > 0)
							{
								?>
								 <div class="table-responsive">  
								 <table class="table table-striped table-bordered table-hover">
								<thead>
									<tr>
										<th></th>
										<th>Message</th>
										<th>Status</th>
										<th>Location</th>
										<th>Date</th>
									</tr>
								</thead>
								<tbody>
								<?php
								foreach($data->tracking_details as $kkk=>$obj)
								{
									?>
									<tr>
										<td></td>
										<td><?php echo $obj->message?></td>
										<td><?php echo $obj->status?></td>
										<td>
											<?php echo (isset($obj->tracking_location->city)?$obj->tracking_location->city:"")?>
											-
											<?php echo (isset($obj->tracking_location->state)?$obj->tracking_location->state:"")?>
											<?php echo (isset($obj->tracking_location->country)?" ".$obj->tracking_location->country:"")?>
										</td>
										<td><?php echo SKGetQuoteDate($obj->datetime)?></td>
									</tr>
									<?php
								}
								?>
								</tbody>
								</table>
								</div>
								<?php
								
							}
						}
					}
				}
				else
				{
					?>
					No record found.
					<?php
				}
				?>
				
			</div>	
			<!--  End Tab Content Block -->
			
			<!--  Tab Content Block -->
			<div class="tab-pane fade" id="OrderStatus">
				<h3>Order Status</h3>
				<?php
					if(@constant("SHIPPING_INFO_DISPLAY")=="1")
					{
					?>
					<br/>
					<table>
						<?php
						$OrderChangeStatusObj = new DataTable(TABLE_ORDER_CHANGE_STATUS);
						$OrderChangeStatusObj->Where = "OrderID='".$CurrentOrder->OrderID."' AND Notify='1'";
						$OrderChangeStatusObj->TableSelectAll(array("*,DATE_FORMAT(CreatedDate,'%M %d, %Y')as MyCreatedDate"),"CreatedDate DESC");
						if($OrderChangeStatusObj->GetNumRows() > 0)
						{
						?>
						<tr>
							<td>
								 <table class="table table-striped table-bordered table-hover">
									<thead>
									<tr class="InsideLeftTd">
										<th  height="25">Status</th>
										<th>Comments</th>
										<th width="50"><b>Notify</b></th>
										<th width="110"><b>Created Date</b></th>								
									</tr>
									</thead>
									<tbody>
									<?php
									while ($CurrentOrderStatus = $OrderChangeStatusObj->GetObjectFromRecord())
									{?>
									<tr class="InsideRightTd">
										<td><?php echo MyStripSlashes($CurrentOrderStatus->OrderStatus)?></td>
										<td><?php echo MyStripSlashes(nl2br($CurrentOrderStatus->Comments))?></td>
										<td align="center"><?php echo $CurrentOrderStatus->Notify == 1 ? "Yes":"No"?></td>
										<td><?php echo MyStripSlashes($CurrentOrderStatus->MyCreatedDate)?></td>								
									</tr>
									<?php
									}?>
									</tbody>
								</table>
							</td>
						</tr>
						<?php
						}
						else
						{
							?>
							No record found.
							<?php
						}
						?>
						<tr>
							<td>
								
							</td>
						</tr>
					</table>
				<?php}?>
				
			</div>	
			<!--  End Tab Content Block -->
  
  
</div>
		<?php	endif;			
			break;
		default:
			$OrderObj = new DataTable(TABLE_ORDERS);
			//$OrderObj->DisplayQuery = true;
			$OrderObj->Where = "UserID='".$OrderObj->MysqlEscapeString($CurrentUserObj->UserID)."'";
			$OrderObj->TableSelectAll(array("*, DATE_FORMAT(CreatedDate,'%b %d, %Y')as MyCreatedDate, DATE_FORMAT(CreatedDate,'%M, %Y')as MyCreatedMonth"),"CreatedDate DESC",10);
			if($OrderObj->GetNumRows() > 0)
			{
			?>
			<div class="table-responsive">  
			<table class="table table-striped table-bordered table-hover">
			 	<thead>
				<tr class="text-success">
					<th align="center" height="25"><b>S.No</b></th>
					<th align="left"><b>Order Date</b></th>
					<th align="left"><b>Order No</b></th>
					<th align="left"><b>Method</b></th>
					<th align="center"><b>Order Status</b></th>
					<th align="right"><b>Amount&nbsp;</b></th>
					<th align="center"></th>
				</tr>
				</thead>
				<tbody>
				<?php
				$SNo = 1;
			 	while($CurrentOrder = $OrderObj->GetObjectFromRecord())
				{
					$CurrencyOrderID = $CurrentOrder->OrderID;
					?>
					<tr>
						<td align="left"  style="height:50px;"><b><?php echo $SNo?></b></td>
						<td align="left" ><?php echo $CurrentOrder->MyCreatedDate?></td>
						<td align="left" ><?php echo $CurrentOrder->OrderNo?></td>
						<td align="left" ><?php echo $CurrentOrder->PaymentMethod?></td>
						<td align="left" ><?php echo $CurrentOrder->OrderStatus?>
						<?php
						if($CurrentOrder->TrackingCode !="")
						{
							?>
							<br>
							<br>
							Track your Order<br>
							<?php echo $CurrentOrder->TrackingCode?>
							<?php
						}
						?>
						</td>
						<td align="right" ><?php echo Change2CurrentCurrency($CurrentOrder->GrandTotal)?></td>
						<td align="left" ><a href='<?php echo MakePageURL("index.php","Page=$Page&Section=FullDetail&OID=$CurrentOrder->OrderID")?>' class='btn btn-compare'>Details</a></td>
					</tr>
			
					<?php
					$SNo++;
			    }?>
				</tbody>
			 </table>
			 </div>
			<?php
			}
			else
			{
				echo "There is no any order in your account.";
			}
		 break;
		}
			?>
				</div>
				<?php/* middle content end*/?>
				
            </div>
        </div>
</section>
	
	
	


