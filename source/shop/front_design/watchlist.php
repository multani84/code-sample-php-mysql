<div id="MainBanners" class="clearfix"><img src="<?php echo DIR_WS_SITE_TEMPLATES.DEFINE_TEMPLATE_STYLE?>/images/bnr.jpg" alt="Banner" /></div>
<div id="MainContent" class="clearfix"> 
	<div class="CMSContent">
	<?php require_once(DIR_FS_SITE_INCLUDES."breadcrumb.php");?>
    <h2 class='Title'>My Watchlist</h2>
			<?php/* middle content start*/?>
				<?php
				require_once(DIR_FS_SITE_INCLUDES."message.php");?>
		    	<div style="display:inline-block;width:100%;">
					<?php	/* product start*/ ?>
		<?php
		$ProductObj = new DataTable(TABLE_PRODUCT. " p, ".TABLE_USERS_WATCHLIST. " uw");
		
		$ProductObj->Where = "p.Active='1' and uw.ProductID=p.ProductID and uw.UserID='".$ProductObj->MysqlEscapeString($CurrentUserObj->UserID)."'";
		$ProductObj->TableSelectAll("","Position ASC");
		if($ProductObj->GetNumRows() > 0)
		{
		?>			
		<table cellpadding="0" cellspacing="0" width="100%" border="0">
		<tr>
		<td align="center">
		 <div class="FeaturedProduct">
		 	<ul>
		 		<?php
		 		$ProductMode = "Watchlist";
		 	while($CurrentProduct = $ProductObj->GetObjectFromRecord())
			{
				echo "<li>";
				 require(DIR_FS_SITE_INCLUDES."product_template.php");
			   echo "</li>";
            }?>
			</ul>
		 </div>
		 </td>
		 </tr>
		 </table>
		<?php
		}
		else
		{
			echo "There are no products in your watchlist.";
		}
		?>		
		<?php	/* product end*/ ?>	
				</div>
				<?php/* middle content end*/?>
	</div>
</div>

