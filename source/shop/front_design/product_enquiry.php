 <div id="MainContent">
    	<div class="container"> 
            <ul class="breadcrumb">
                <li><a href="<?php echo SKSEOURL("1001","cms")?>">Home</a></li>
                <li class="active">Product Enquiry</li>
            </ul>
        	<h1>Product Enquiry</h1>
            <div>
				<?php  require_once(DIR_FS_SITE_INCLUDES."message.php");?>
					 	<?php
						if(isset($_SESSION['EnquiryProducts']) && is_array($_SESSION['EnquiryProducts']) && count($_SESSION['EnquiryProducts']) > 0)	
						{							
						?>
						<div class="row">
						<div class="col-sm-12">
						   <div class="panel panel-default">
							<div class="panel-heading">Please fill in the below details:</div>
								<div class="panel-body">
								<form name="FormEnquiry" action="<?php  echo MakePageURL("index.php","Page=$Page&Target=EnquirySubmit")?>" id="FormEnquiry" method="POST" class="form-horizontal" role="form">
								<input type='hidden' name='Mail_ToEmail' value=<?php echo DEFINE_ADMIN_EMAIL?>>
								<input type='hidden' name='Mail_Target' value='Submit'>
								<input type='hidden' name='Mail_Subject' value='Product Enquiry: Information submitted at <?php echo DEFINE_SITE_NAME?>.'>
								<input type='hidden' name='Mail_Header' value='Thank you for visiting us at <?php echo DEFINE_SITE_NAME?><br>You submitted the following information :<br><br>'>
								<input type='hidden' name='Mail_Footer' value='We shall get in touch with you soon.<br><br><?php echo DEFINE_SITE_NAME?>.'>
								<input type='hidden' name='Mail_Redirect' value='<?php echo SKSEOURL(1001,"cms","message=1")?>'>
								
										<div class="form-group">
											<label class="control-label col-sm-3" for="Name">
												<span class="hidden-xs">Name: <b>*</b></span>
												<span class="visible-xs">First Name: <b>*</b></span>
											</label>
											<div class="col-sm-9">
												<div class="row">
													<div class="col-sm-6">
														<div class="input-group">
															<span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span>
															<input type="text" class="form-control" id="FirstName_R" title="Please enter your first name." name="FirstName" placeholder="First Name" value="<?php echo @$_POST['FirstName']?>" required>
														</div>
													</div>
													<div class="col-sm-6">
														<div class="visible-xs" style="margin-top:15px;"><label class="control-label" for="LastName">Last Name: <b>*</b></label></div>
														<div class="input-group">
															<span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span>
															<input type="text" class="form-control" id="LastName_R" name="LastName" title="Please enter your last name." placeholder="Last Name" value="<?php echo @$_POST['LastName']?>" required>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="form-group">
											<label class="control-label col-sm-3" for="email">Email: <b>*</b></label>
											<div class="col-sm-9">
												<div class="input-group">
													<span class="input-group-addon">@</span>
													<input type="email" class="form-control" id="Email_E" name="Email" title="Please enter your email address in valid format." placeholder="e.g. user@domain.com" value="<?php echo @$_POST['Email']?>" required/>
												</div>
											</div>
										</div>
										<div class="form-group">
											<label class="control-label col-sm-3" for="Telephone">Telephone: <b>*</b></label>
											<div class="col-sm-9">
												<div class="input-group">
													<span class="input-group-addon"><span class="glyphicon glyphicon-phone-alt"></span></span>
													<input type="tel" class="form-control" id="Telephone" name="Telephone" title="Please enter your telephone number." placeholder="555-5555-555" value="<?php echo @$_POST['Telephone']?>" required />
												</div>
											</div>
										</div>
										<div class="form-group">
											<label class="control-label col-sm-3 text-right" for="email">Enquiry:</label>
											<div class="col-sm-9">
												<textarea class="form-control" rows="4" id="Message" name="Message" placeholder="message/comments..."><?php echo @$_POST['Message']?></textarea>
											</div>
										</div>
											<?php /* Product list start*/?>
											<div style="clear:both"></div>
											  <div class="Category row products text-center">
												<?php foreach($_SESSION['EnquiryProducts'] as $ProductID=>$Val)
												{
													$ProductObj->Where ="ProductID='".(int)$ProductObj->MysqlEscapeString($ProductID)."' AND Active='1'";
													$CurrentProduct = $ProductObj->TableSelectOne();
													include(dirname(__DIR__)."/product_template.php");
												}?>
											   </div>
										   <div style="clear:both"></div>   
										   <?php /* Product list end*/?>
											
										<div class="form-group">
											<div class="col-sm-offset-3 col-sm-9">
												<button type="submit" class="btn btn-default Button">Submit</button>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
				
			
						</div>
						<?php 
						}
						else
						{?>
						<table cellpadding="3" cellspacing="2" border="0" width="95%">
							<tr>
								<td height="15"></td>
							</tr>
							<tr>
								<td align="center">
								<b>Your enquiry list is empty.</b></td>
							</tr>
						</table>			
							<?php 
						}
						?>
			</div>
			<?php /* middle content end*/?>
  
        </div>
    </div>
	
	<script type="text/javascript">
jQuery(document).ready(function($){
	$("#FormEnquiry").validate();
});
</script>	


