<style type="text/css">
.payment_method_li .note, .shipping_method_li .note{font-style:italic;font-size:10px;padding:0px 0px 10px 50px;}
.payment_method_li .hidenote, .shipping_method_li .hidenote{display:none;padding:0px 0px 10px 50px;}
</style>
<script type="text/javascript">
jQuery(document).ready(function($){
	
	var form_content;
	var validator = $("#CheckoutForm").validate({
		errorLabelContainer: $('#ValidateError'),
		  submitHandler: function(form) {
				 if($( "input[name=shipping_method]" ).length==1 && $( "input[name=shipping_method]" ).attr("type").toLowerCase()=="hidden")
				 {
					 return true;
				 }
				 else if($( "input[name=shipping_method]:checked" ).length==0)
				 {
					 alert('Choose your preferred shipping method.');
						return false;	
				 }
				 else
				 {
					 return true;
				 }
			  
			}
	});	
		setTimeout(function(){
		if(validator.checkForm()==true)
		{
			form_content = $("#CheckoutForm").serialize();
			GetShippingDetails();
		}	
		}, 2000);
		
	
	

$( "#ShippingMethod").delegate( ".shipping_method_li .radio", "click", function() {
  if($(this).attr('checked'))
  {
	  $( "#ShippingMethod .hidenote").hide();
	  $(this).parents("li").find(".hidenote").show();
  }
});

$( "#PaymentMethod").delegate( ".payment_method_li .radio", "click", function() {
  if($(this).attr('checked'))
  {
	  $( "#PaymentMethod .hidenote").hide();
	  $(this).parents("li").find(".hidenote").show();
  }
});

$("#CheckoutForm input,#CheckoutForm input").blur(function(){
		var validator = $("#CheckoutForm").validate();
		if(validator.checkForm()==true && form_content != $("#CheckoutForm").serialize())
		{
			form_content = $("#CheckoutForm").serialize();
			GetShippingDetails();
		}
		
	});
	
});

function GetShippingDetails()
{
	jQuery("#ShippingMethod").html("Shipping loading...");
	jQuery.post( "<?php echo DIR_WS_SITE_SHIPPING?>shipping.php?type=html", jQuery("#CheckoutForm").serializeArray())
	  .done(function( data ) {
			jQuery("#ShippingMethod").html(data);
	  });
}
</script>