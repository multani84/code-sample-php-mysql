<div id="MainBanners" class="clearfix"><img src="<?php echo DIR_WS_SITE_TEMPLATES.DEFINE_TEMPLATE_STYLE?>/images/bnr.jpg" alt="Banner" /></div>
<div id="MainContent" class="clearfix"> 
	<div class="CMSContent">
	<?php require_once(DIR_FS_SITE_INCLUDES."breadcrumb.php");?>
    <h2 class='Title'>My Wishlist</h2>
		<?php/* middle content start*/?>
				<?php
				require_once(DIR_FS_SITE_INCLUDES."message.php");?>
		    	<div style="display:inline-block;width:100%;">
					<?php
    $UserWCategoryObj->Where ="UserID='".$CurrentUserObj->UserID."'";
	$UserWCategoryObj->TableSelectAll();
			
	?>
<table cellpadding="5" cellspacing="0" border="0">
	<tr>
	<?php
	while ($CurrentWCategory = $UserWCategoryObj->GetObjectFromRecord())
	{?>
	<td><a href="<?php echo MakePageURL("index.php","Page=$Page&WCategoryID=".$CurrentWCategory->WCategoryID)?>" class="frmButton"><?php echo $CurrentWCategory->CategoryName?></a></td>
	<?php
	}?>
	</tr>
</table>
<br>
<?php
$UserWCategoryObj->Where ="UserID='".$CurrentUserObj->UserID."' AND WCategoryID ='".$UserWCategoryObj->MysqlEscapeString($WCategoryID)."'";
$CurrentWCategory = $UserWCategoryObj->TableSelectOne();
if(isset($CurrentWCategory->WCategoryID) && $CurrentWCategory->WCategoryID !="")
{
		$MessageBody ="Dear ".chr(13).chr(13)."Please check this list of products  at ".DEFINE_SITE_NAME.":".chr(13).MakePageURL("index.php","Page=wish_product&WCategoryID=".$CurrentWCategory->WCategoryID).chr(13)."[Please copy and paste the above URL in your browser.]".chr(13).chr(13)."Thanks!";
	?>
	<table cellpadding="5" cellspacing="0" border="0">
		<tr>
			<td><b><?php echo $CurrentWCategory->CategoryName?></b></td>
			<td><a href="<?php echo MakePageURL("index.php","Page=$Page&Target=RemoveWCategory&WCategoryID=".$CurrentWCategory->WCategoryID)?>" class="frmButton">Remove this category from my wishlist</a></td>
			<td><a href="javascript:;" onclick="document.getElementById('DivWFriend').style.display='block'; return false;" class="frmButton">Send this wishlist to my friend.</a>
				<div style="position:relative;">
				<div id="DivWFriend" style="display:none;">
               		<div align="right"><a href="javascript:;" onclick="document.getElementById('DivWFriend').style.display='none'; return false;"><b>X</b></a></div>
               		<form name="RegistrationForm" action="<?php echo MakePageURL("index.php","Page=$Page&WCategoryID=$WCategoryID&Target=SendRequest")?>" id="RegistrationForm" method="POST" onSubmit="return ValidateForm(this);">
    				<table border="0" width="100%" cellspacing="2" cellpadding="2" class="TableFormat">
	                      <tr valign=top>
	                          <td nowrap width="30%" valign="middle">Your Name<font size="2">*</font></td>
	                          <td><input type="text" size=24 name="Name"  value="<?php echo isset($IsCurrentUser->FirstName)?$IsCurrentUser->FirstName." ".$IsCurrentUser->LastName:""?>" id="Name_R" title="Please enter your Name"></td>
	                      </tr>
			              <tr valign=top>
	                          <td nowrap width="30%" valign="middle">Your Email<font size="2">*</font></td>
	                          <td><input type="text" size=24 name="Email"  value="<?php echo isset($IsCurrentUser->Email)?$IsCurrentUser->Email:""?>" id="Email_E" title="Invalid Email Address ! Please try user@domain.com"></td>
	                      </tr>
			              <tr valign=top>
	                          <td nowrap width="30%" valign="middle">Friend's Name<font size="2">*</font></td>
	                          <td><input type="text" size=24 name="FName"  value="" id="FName_R" title="Please enter your Friend's Name"></td>
	                      </tr>
			              <tr valign=top>
	                          <td nowrap width="30%" valign="middle">Friend's Email<font size="2">*</font></td>
	                          <td><input type="text" size=24 name="FEmail"  value="" id="FEmail_E" title="Invalid Email Address ! Please try user@domain.com"></td>
	                      </tr>
			              <tr>
	                        <td colspan="2"><textarea title="Please enter your message" name="MessageBody" id="MessageBody_R" wrap="soft" cols="50" rows="7"><?php echo isset($_POST['MessageBody'])?$_POST['MessageBody']:$MessageBody?></textarea></td>
						 </tr>
						 <tr>
	                        <td colspan="2" align="center"><a name="Register" href="javascript:;" onclick="return ValidateForm('RegistrationForm');"  class="frmButton">Submit</a></td>
						 </tr>
	                    </table>
	                     </form>	
               	</div>
               	</div>
			</td>
		</tr>
	</table>
	<br>
	<?php
}
?>
<?php	/* product start*/ ?>
<?php
$ProductObj = new DataTable(TABLE_PRODUCT. " p, ".TABLE_USERS_WISHLIST. " uw");

$ProductObj->Where = "p.Active='1' and uw.ProductID=p.ProductID and uw.UserID='".$ProductObj->MysqlEscapeString($CurrentUserObj->UserID)."'";
if($WCategoryID !=0)
{
	$ProductObj->Where .=" AND uw.WCategoryID ='".$WCategoryID."'";
}
$ProductObj->TableSelectAll("","Position ASC");
if($ProductObj->GetNumRows() > 0)
{
?>			
<table cellpadding="0" cellspacing="0" width="100%" border="0">
<tr>
<td align="center">
 <div class="FeaturedProduct">
 	<ul>
 		<?php
 		$ProductMode = "WishList";
 	while($CurrentProduct = $ProductObj->GetObjectFromRecord())
	{
		echo "<li>";
		 require(DIR_FS_SITE_INCLUDES."product_template.php");
	   echo "</li>";
    }?>
	</ul>
 </div>
 </td>
 </tr>
 </table>
<?php
}
else
{
	echo "There are no products in your wishlist.";
}
?>	
				</div>
				<?php/* middle content end*/?>
	</div>
</div>

