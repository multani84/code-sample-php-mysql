<?php
require_once(dirname(__FILE__)."/class.shopping_calculations.php");
CheckTmpCartAlways();

$TmpCartObj = new DataTable(TABLE_TMPCART);


if(@$_REQUEST['Target'] == "AddBulkCart")
{
	//var_dump($_POST);die();

	foreach ($_POST['qty'] as $key => $value) {
		$ProductID = isset($key)?$key:"";
		$Qty = (isset($value) && $value >0)?ceil($value):"0";
		if($Qty>0){
		AddProductInTmpCart($ProductID,$Qty,@implode(",",$_POST['ProductAdditionName']),$_POST);
		}
		
	}

	if(isset($_POST['SKAjax']) && $_POST['SKAjax']=="1")
	{
		@ob_clean();
		echo "1";
		exit;
	}
	else 
	{
		MyRedirect(MakePageURL("index.php","Page=shop/shop_cart")); 	
		exit;
	}
		
}


if(@$_REQUEST['Target'] == "AddCart")
{
		$ProductID = isset($_REQUEST['ProductID'])?$_REQUEST['ProductID']:"";
		$Qty = (isset($_POST['Qty']) && $_POST['Qty'] >0)?ceil($_POST['Qty']):"1";
		AddProductInTmpCart($ProductID,$Qty,@implode(",",$_POST['ProductAdditionName']),$_POST);
		if(isset($_POST['SKAjax']) && $_POST['SKAjax']=="1")
		{
			@ob_clean();
			echo "1";
			exit;
		}
		else 
		{
			MyRedirect(MakePageURL("index.php","Page=shop/shop_cart")); 	
			exit;
		}
}
if(@$_REQUEST['Target'] == "CouponProcess")
{
	if((function_exists("CheckCouponStatus") && CheckCouponStatus() ===true) && (isset($_POST['VoucherCode']) and $_POST['VoucherCode']!=''))
	{				
		@ob_clean();
		$_SESSION['CouponCode'] = $_POST['VoucherCode'];
		$ReturnArray = $ShoppingCalculationObj->CheckCartDiscount($_POST['VoucherCode']);
		if(isset($ReturnArray['InfoMessage']) && $ReturnArray['InfoMessage'] !="")
		{
			if($ReturnArray['DiscountTotal'] && $ReturnArray['DiscountTotal'] >0)
			{
				$_SESSION['InfoMessage'] = $ReturnArray['InfoMessage'];
			}
		}
		elseif(isset($ReturnArray['ErrorMessage']) && $ReturnArray['ErrorMessage'] !="")
		{
			@ob_clean();
			$_SESSION['ErrorMessage'] = $ReturnArray['ErrorMessage'];
		}
		MyRedirect(MakePageURL("index.php","Page=shop/shop_cart")); 	
		exit;
	}
}
if(@$_REQUEST['Target'] == "DeleteCart" && @$_REQUEST['ItemID'] !="")
{
	$ItemID = @$_REQUEST['ItemID'];
	$TmpCartObj = new DataTable(TABLE_TMPCART);
	$TmpCartObj2 = new DataTable(TABLE_TMPCART);
	$TmpCartObj->Where =" (ItemID='".$TmpCartObj->MysqlEscapeString($ItemID)."' OR  ParentItemID='".$TmpCartObj->MysqlEscapeString($ItemID)."') and SessionID='".session_id()."'";
	$TmpCartObj->TableSelectAll();
	while ($CurrentCartItem = $TmpCartObj->GetObjectFromRecord()) 
	{
		if(isset($CurrentCartItem->ItemID))
		{
			$TmpCartObj2->Where =" (ItemID='".$TmpCartObj2->MysqlEscapeString($CurrentCartItem->ItemID)."' OR  ParentItemID='".$TmpCartObj2->MysqlEscapeString($CurrentCartItem->ItemID)."') and SessionID='".session_id()."'";
			$TmpCartObj2->TableDelete();
		}
	}
	@ob_clean();
	$_SESSION['InfoMessage'] = DEFINE_DELETE_ITEM_IN_CART;
	 MyRedirect(MakePageURL("index.php","Page=shop/shop_cart")); 	
	 exit;
}
if(isset($_GET['Target']) && $_GET['Target'] == "UpdateCart")
{
		$TmpCartObj = new DataTable(TABLE_TMPCART);
		for($i=1;$i<$_POST['Count'];$i++)
		{
			$ItemID = isset($_POST['ItemID'.$i])?$_POST['ItemID'.$i]:"";
			$Qty = isset($_POST['Qty'][$ItemID])?$_POST['Qty'][$ItemID]:"1";
				
			if(trim($Qty) <= "0" OR trim($Qty)=="" OR !is_numeric($Qty))
			{
				@ob_clean();
			   	$_SESSION['ErrorMessage'] = constant("DEFINE_INVALID_QUANTITY");
			   	MyRedirect(MakePageURL("index.php","Page=shop/shop_cart")); 	
			  	exit;
			}
			else 
			{
				
				var_dump($ItemID."===".$Qty);
				$TmpCartObj2 = new DataTable(TABLE_TMPCART);
				$TmpCartObj->Where ="ItemID='".$TmpCartObj->MysqlEscapeString($ItemID)."' OR  ParentItemID='".$TmpCartObj->MysqlEscapeString($ItemID)."'";
				$TmpCartObj->TableSelectAll();
				while ($CurrentCart = $TmpCartObj->GetObjectFromRecord()) 
				{
						$TmpCartObj2->Where = "TmpCartID='".$TmpCartObj->MysqlEscapeString($CurrentCart->TmpCartID)."'";
						$SubDataArray = array();
						$SubDataArray['Price'] = $CurrentCart->Price;
						$SubDataArray['Qty'] = $Qty;
						$SubDataArray['Total'] = $SubDataArray['Price'] * $SubDataArray['Qty'];
						$TmpCartObj2->TableUpdate($SubDataArray);
				}
			}
		}
		
		  @ob_clean();
		  $_SESSION['InfoMessage'] = constant("DEFINE_UPDATE_ITEM_IN_CART");
		  if(isset($_SESSION['CouponCode']) && $_SESSION['CouponCode'] !="")
		  	$ShoppingCalculationObj->CheckCartDiscount($_SESSION['CouponCode']);
		  MyRedirect(MakePageURL("index.php","Page=shop/shop_cart")); 	
		  exit;
}
	if(@$_REQUEST['Target']=='CartContinue')
	{
		 MyRedirect(MakePageURL("index.php","Page=shop/shop"));
	  	 exit;
	}
	if(@$_REQUEST['Target']=='CartSubmit')
	{
		if(!isset($_SESSION['OrderID']))
		{
			if(@constant("DEFINE_CHECKOUT_LOGIN")=="1" && @constant("DEFINE_CHECKOUT_WITHOUT_LOGIN")=="1")
			{
				if($CustomObj->CheckUserLogin(MakePageURL("index.php","Page=shop/shop_checkout")))
				{
					MyRedirect(MakePageURL("index.php","Page=shop/shop_checkout")); 	
		  			exit;				
				}	
				else 
				{
					MyRedirect(MakePageURL("index.php","Page=shop/login")); 	
		  			exit;
				}
			}
		}
		MyRedirect(MakePageURL("index.php","Page=shop/shop_checkout")); 	
		exit;
	}
	
	
if(isset($_GET['Target']) && $_GET['Target'] == "PaymentProcess")
{
	$payment_method = isset($_POST['payment_method'])?$_POST['payment_method']:"";
	if($payment_method != "")
	{
			$PaymentModuleObj = new DataTable(TABLE_MODULES);
			$PaymentModuleObj->Where ="ModuleType='Payment' AND Active='1' AND 
					ModuleCode='".$PaymentModuleObj->MysqlEscapeString(substr($payment_method,0,80))."'";
			$CurrentModule = $PaymentModuleObj->TableSelectOne("","ModuleID ASC");
			if(isset($CurrentModule->ModuleCode) &&$CurrentModule->ModuleCode !="")
			{
					$file = pathinfo($CurrentModule->ModuleFile);
					if(file_exists(DIR_FS_SITE_PAYMENT.$file['filename']."/".$file['basename']))
					{
							$OrderID =$_SESSION['OrderID'];
							$OrderObj = new DataTable(TABLE_ORDERS);
							$OrderObj->Where ="OrderID='".$OrderID."'";
							$CurrentOrder = $OrderObj->TableSelectOne();
							
							$DataArray = array();
							$DataArray['Attempted'] = "1";
							$DataArray['PaymentStatus'] = "Pending";
							$DataArray['PaymentMethod'] = $payment_method;
							$OrderObj->Where ="OrderID='".$OrderID."'";
							//$OrderObj->DisplayQuery =true;
							$OrderObj->TableUpdate($DataArray);	
							$PaymentProcessStart = "Payment";
							require_once(DIR_FS_SITE_PAYMENT.$file['filename']."/".$file['basename']);
					}
					
				
			}
	}
	MyRedirect(MakePageURL("index.php","Page=shop/confirm_order")); 	
	exit;
}
	
if(isset($_GET['Target']) && $_GET['Target'] == "OrderProcess")
{
	
		if( isset($_POST['ShippingCountry']) && $_POST['ShippingCountry'] != "" )
		{
			$_SESSION['TaxCountry'] = $_POST['ShippingCountry'];
			CheckTmpCartAlways(true);
		}

		$OrderObj = new DataTable(TABLE_ORDERS);
		$OrderDetailObj = new DataTable(TABLE_ORDER_DETAILS);
		if(isset($_SESSION['OrderID']) && $_SESSION['OrderID'] !="")
		{
			$OrderID = $_SESSION['OrderID'];
		}
		else
		{
			$BlankArray = array();
			$BlankArray['CreatedDate']	=date("Y-m-d H:i:s");
			$OrderID = $OrderObj->TableInsert($BlankArray);
			@ob_clean();
			$_SESSION['OrderID']= $OrderID;
		}
			$SessionID = session_id();
			$DownloadID = "";
			$ShoppingCalculationObj->SessionID = $SessionID;
			$ShoppingCalculationObj->Calculate($_POST);
			$TmpCartObj = new DataTable(TABLE_TMPCART);
			$TmpCartObj->Where =" SessionID='".$SessionID."'";
			$TmpCartObj->TableSelectAll("","TmpCartID ASC");
			$OrderDetailObj->Where ="OrderID='".$OrderID."'";
			$OrderDetailObj->TableDelete();
			while($CurrentCartItem = $TmpCartObj->GetObjectFromRecord())
			{
				$DataArray = array();
				$DataArray['OrderID'] = $OrderID;
				$DataArray['ItemID'] = $CurrentCartItem->ItemID;
				$DataArray['ParentItemID'] = $CurrentCartItem->ParentItemID;
				$DataArray['ItemType'] = $CurrentCartItem->ItemType;
				$DataArray['ItemStatus'] = $CurrentCartItem->ItemStatus;
				$DataArray['ReferenceID'] = $CurrentCartItem->ReferenceID;
				$DataArray['ItemName'] = $CurrentCartItem->ItemName;
				$DataArray['ItemNo'] = $CurrentCartItem->ItemNo;
				if($DownloadID =="" && $CurrentCartItem->ItemStatus=="Download")
					$DownloadID = uniqid("DW_");
				$DataArray['Attribute1'] = $CurrentCartItem->Attribute1;
				$DataArray['Attribute2'] = $CurrentCartItem->Attribute2;
				$DataArray['Width'] = $CurrentCartItem->Width > 0?$CurrentCartItem->Width:0;
				$DataArray['Height'] = $CurrentCartItem->Height > 0?$CurrentCartItem->Height:0;
				$DataArray['Length'] = $CurrentCartItem->Length > 0?$CurrentCartItem->Length:0;
				$DataArray['Weight'] = $CurrentCartItem->Weight > 0?$CurrentCartItem->Weight:0;
				
				$DataArray['UnitPrice'] = $CurrentCartItem->UnitPrice;
				$DataArray['TaxPrice'] = $CurrentCartItem->TaxPrice;
				$DataArray['TaxPercent'] = $CurrentCartItem->TaxPercent;
				$DataArray['Discount'] = $CurrentCartItem->Discount;
				$DataArray['Price'] = $CurrentCartItem->Price > 0?$CurrentCartItem->Price:0;
				$DataArray['Qty'] = $CurrentCartItem->Qty;
				$DataArray['Total'] = $CurrentCartItem->Total > 0?$CurrentCartItem->Total:0;
				$DataArray['Comments'] = $CurrentCartItem->Comments;
				$DataArray['SessionID'] = $CurrentCartItem->SessionID;
				$DataArray['SessionInfo'] = $CurrentCartItem->SessionInfo;
				$DataArray['CreatedDate'] = date('Y-m-d');
				$OrderDetailObj->TableInsert($DataArray);
			}
			/* Order Currencies start*/
			if(isset($CurrentCurrency->CurrencyID) && $CurrentCurrency->CurrencyID !="")
			{
				$OrderCurrencyObj = new DataTable(TABLE_ORDER_CURRENCIES);
				$OrderCurrencyObj->Where ="OrderID='".$OrderID."'";
				$OrderCurrencyObj->TableDelete();
				$OrderCurrencyArray = array();
				$OrderCurrencyArray['OrderID'] = $OrderID;
				$OrderCurrencyArray['CurrencyName'] = $CurrentCurrency->CurrencyName;
				$OrderCurrencyArray['Code'] = $CurrentCurrency->Code;
				$OrderCurrencyArray['Symbol'] = $CurrentCurrency->Symbol;
				$OrderCurrencyArray['Value'] = "1";
				$OrderCurrencyArray['ExchangeRate'] = $CurrentCurrency->Value;
				$OrderCurrencyArray['Prefix'] = $CurrentCurrency->Prefix;
				$OrderCurrencyObj->TableInsert($OrderCurrencyArray);
			}
			/* Order Currencies end*/
			$OrderDataArray = array();
			$OrderDataArray['UserID'] = isset($_SESSION['UserID'])?$_SESSION['UserID']:"";
			$OrderDataArray['BillingFirstName'] = isset($_POST['BillingFirstName'])?$_POST['BillingFirstName']:"";
			$OrderDataArray['BillingLastName'] = isset($_POST['BillingLastName'])?$_POST['BillingLastName']:"";
			$OrderDataArray['BillingAddress1'] = isset($_POST['BillingAddress1'])?$_POST['BillingAddress1']:"";
			$OrderDataArray['BillingAddress2'] = isset($_POST['BillingAddress2'])?$_POST['BillingAddress2']:"";
			$OrderDataArray['BillingCity'] = isset($_POST['BillingCity'])?$_POST['BillingCity']:"";
			$OrderDataArray['BillingState'] = isset($_POST['BillingState'])?$_POST['BillingState']:"";
			$OrderDataArray['BillingArea'] = isset($_POST['BillingArea'])?$_POST['BillingArea']:"";
			$OrderDataArray['BillingCountry'] = isset($_POST['BillingCountry'])?$_POST['BillingCountry']:"";
			$OrderDataArray['BillingZip'] = isset($_POST['BillingZip'])?$_POST['BillingZip']:"";
			$OrderDataArray['BillingPhone'] = isset($_POST['BillingPhone'])?$_POST['BillingPhone']:"";
			$OrderDataArray['BillingFax'] = isset($_POST['BillingFax'])?$_POST['BillingFax']:"";
			$OrderDataArray['ShippingFirstName'] = isset($_POST['ShippingFirstName'])?$_POST['ShippingFirstName']:"";
			$OrderDataArray['ShippingLastName'] = isset($_POST['ShippingLastName'])?$_POST['ShippingLastName']:"";
			$OrderDataArray['ShippingAddress1'] = isset($_POST['ShippingAddress1'])?$_POST['ShippingAddress1']:"";
			$OrderDataArray['ShippingAddress2'] = isset($_POST['ShippingAddress2'])?$_POST['ShippingAddress2']:"";
			$OrderDataArray['ShippingCity'] = isset($_POST['ShippingCity'])?$_POST['ShippingCity']:"";
			$OrderDataArray['ShippingState'] = isset($_POST['ShippingState'])?$_POST['ShippingState']:"";
			$OrderDataArray['ShippingArea'] = isset($_POST['ShippingArea'])?$_POST['ShippingArea']:"";
			$OrderDataArray['ShippingCountry'] = isset($_POST['ShippingCountry'])?$_POST['ShippingCountry']:"";
			$OrderDataArray['ShippingZip'] = isset($_POST['ShippingZip'])?$_POST['ShippingZip']:"";
			$OrderDataArray['ShippingPhone'] = isset($_POST['ShippingPhone'])?$_POST['ShippingPhone']:"";
			$OrderDataArray['ShippingFax'] = isset($_POST['ShippingFax'])?$_POST['ShippingFax']:"";
			$OrderDataArray['Email'] = isset($_POST['Email'])?$_POST['Email']:"";
			$OrderDataArray['PartnerCode'] = isset($_POST['PartnerCode'])?$_POST['PartnerCode']:"";
			$OrderDataArray['GSTIN'] = isset($_POST['GSTIN'])?$_POST['GSTIN']:"";
			
			$OrderDataArray['Comments'] = isset($_POST['Comments'])?$_POST['Comments']:"";
			$OrderDataArray['DownloadID'] = $DownloadID;
			$OrderDataArray['OrderStatus'] = $OrderStatusArray['PendingPayment'];
			$OrderDataArray['ShippingStatus'] = $ShippingStatusArray['Pending'];
			$OrderDataArray['PaymentStatus'] = $PaymentStatusArray['Pending'];
			$OrderDataArray['PaymentMethod'] = isset($_POST['PaymentMethod'])?$_POST['PaymentMethod']:"";
			$OrderDataArray['Attempted'] = "0";
			$OrderDataArray['PointText'] = isset($ShoppingCalculationObj->PointText)?$ShoppingCalculationObj->PointText:"";
			$OrderDataArray['PointAmount'] = isset($ShoppingCalculationObj->PointAmount)?$ShoppingCalculationObj->PointAmount:"";
			$OrderDataArray['VoucherText'] = isset($ShoppingCalculationObj->VoucherText)?$ShoppingCalculationObj->VoucherText:"";
			$OrderDataArray['VoucherCode'] = isset($ShoppingCalculationObj->VoucherCode)?$ShoppingCalculationObj->VoucherCode:"";
			
			$OrderDataArray['ShippingText'] = isset($ShoppingCalculationObj->ShippingText)?$ShoppingCalculationObj->ShippingText:"";
			$OrderDataArray['ShippingCode'] = isset($ShoppingCalculationObj->ShippingCode)?$ShoppingCalculationObj->ShippingCode:"";
			
			$OrderDataArray['SubTotal'] = isset($ShoppingCalculationObj->SubTotal)?$ShoppingCalculationObj->SubTotal:"0";
			$OrderDataArray['TaxAmount'] = isset($ShoppingCalculationObj->VATCharges)?$ShoppingCalculationObj->VATCharges:"0";
			$OrderDataArray['TaxPercentage'] = $ShoppingCalculationObj->TaxPercentage;
			$OrderDataArray['ShippingAmount'] = isset($ShoppingCalculationObj->ShippingPrice)?$ShoppingCalculationObj->ShippingPrice:"0";
			$OrderDataArray['Total'] = isset($ShoppingCalculationObj->Total)?$ShoppingCalculationObj->Total:"0";
			$OrderDataArray['VoucherAmount'] = isset($ShoppingCalculationObj->VoucherAmount)?$ShoppingCalculationObj->VoucherAmount:"0";
			$OrderDataArray['GrandTotal'] = isset($ShoppingCalculationObj->GrandTotal)?$ShoppingCalculationObj->GrandTotal:"0";
			$OrderDataArray['OrderNo'] =date('dm').(10000 + $OrderID);
			$OrderDataArray['SessionID'] =$SessionID;
			$OrderObj->Where ="OrderID='".$OrderID."'";
			$CurrentOrder = $OrderObj->TableSelectOne();
			if(isset($CurrentOrder->OrderID) && $CurrentOrder->OrderID !="")
			{
				$OrderObj->TableUpdate($OrderDataArray);
			}
			else 
			{
				$OrderDataArray['OrderID'] =$OrderID;
				$OrderDataArray['CreatedDate']	=date("Y-m-d H:i:s");
				$OrderObj->TableInsert($OrderDataArray);
			}
			//$ShoppingCalculationObj->UpdateOrderDiscount($OrderID);
			MyRedirect(MakePageURL("index.php","Page=shop/confirm_order")); 	
			exit;
}
?>