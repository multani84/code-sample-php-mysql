<?php 
	global $AttributeTypeArray;
	$AttributeTypeArray = array("category"=>"Category",
								"product"=>"Product",
								"both"=>"Category,Product Both",
							 ); 
	global $AttributeStatusArray;
	$AttributeStatusArray = array("custom"=>"Custom",
								"fixed"=>"Fixed",
							 ); 
	global $ProductFileTypeArray;
	$ProductFileTypeArray = array("Image"=>"Image",
								  "Pdf"=>"Pdf",
							 ); 
	global $ProductStockTypeArray;
	$ProductStockTypeArray = array("InStock"=>"In Stock",
							      "OutStock"=>"Out of Stock",
							 ); 
	 global $PaymentStatusArray;
	$PaymentStatusArray = array("Pending"=>"Pending",
								"Paid"=>"Paid",
									  ); 
	global $OrderStatusArray;
	$OrderStatusArray = array("Paid"=>"Paid",
							  "Delivered"=>"Delivered",
							  "Dispatched"=>"Dispatched",
							  "Completed"=>"Completed",
							  "Closed"=>"Closed",
							  "Canceled"=>"Canceled",
							  "OnHold"=>"On Hold",
							  "PendingPayment"=>"PendingPayment",
						  ); 
	global $OrderStatusColorArray;
	$OrderStatusColorArray = array(
								  "PendingPayment"=>"#E0E0E0",
								  "Paid"=>"#D6D1FC",
								  "Delivered"=>"#B3F9CD",
								  "Dispatched"=>"#8AF2E7",
								  "Completed"=>"#809EED",
								  "Closed"=>"#EFB8C3",
								  "Canceled"=>"#FF6886",
								  "OnHold"=>"#E0E5AE",
							  ); 
	global $CouponTypeArray;
	$CouponTypeArray = array("Percentage"=>"Percentage",
							 "FlatPrice"=>"FlatPrice",
							 ); 
	global $PriceTypeArray;
	$PriceTypeArray = array("Percentage"=>"Percentage(%)",
							"Unit"=>"Per item",
						   ); 
	global $ProductTypeArray;
	$ProductTypeArray = array("Simple"=>"Simple",
							  "Bundle"=>"Bundle",
						   ); 
global $PointStatusArray;
	$PointStatusArray = array("Get"=>"Get",
							  "Access"=>"Access",
							  ); 
global $ShippingStatusArray;
	$ShippingStatusArray = array("Pending"=>"Pending",
								"Dispatched"=>"Dispatched",
								"Delivered"=>"Delivered",
								"Cancelled"=>"Cancelled",
									  ); 
global $ShippingColorArray;
	$ShippingColorArray = array("Pending"=>"#bdd1ef",
								"Dispatched"=>"#99f4cf",
								"Delivered"=>"#cfe6a1",
								"Cancelled"=>"#ffacac",
									  ); 
	global $CareditCardTypeArray;
	$CareditCardTypeArray = array("Visa"=>"Visa",
							      "MasterCard"=>"MasterCard",
							      "AmericanExpress"=>"American Express",
							      "Switch/Maestro"=>"Switch/Maestro",
							      "Solo"=>"Solo",
							 ); 
							 
global $ProductColumnCSVArray;
$ProductColumnCSVArray = array("ModelNo"=>"Model No",
							  "ProductName"=>"Product Name",
							  "SmallDescription"=>"Small Description",
							  "LargeDescription"=>"Large Description",
							  "MetaTitle"=>"Meta Title",
							  "MetaKeyword"=>"Meta Keyword",
							  "MetaDescription"=>"Meta Description",
							  "Weight"=>"Weight",
							  "Stock"=>"Stock",
							  "MinStockLevel"=>"Min Stock Level",
							  "Price"=>"Price",
							  "SalePrice"=>"Sale Price",
							  "SaleActive"=>"Sale Price Enabled",
							  "GoogleBrand"=>"Google Brand",
							  "GoogleGtin"=>"Google Gtin",
							  "GoogleMPN"=>"Google MPN",
							  "QuickBooksCode"=>"Quick Books Code",
							  "Active"=>"Product Enable",
							  ); 
	

	if(isset($_SESSION['OrderID']) && $_SESSION['OrderID'] !="")
	{
		$OrderObj = new DataTable(TABLE_ORDERS);
		$OrderObj->Where ="OrderID='".$_SESSION['OrderID']."'";
		$CurrentObj = $OrderObj->TableSelectOne(array("PaymentStatus"));
		if(isset($CurrentObj->PaymentStatus) && $CurrentObj->PaymentStatus =="Paid")
		{
			@ob_clean();
			$_SESSION['OrderID']="";
			unset($_SESSION['OrderID']);	
		}		
	}
	/* Default Country start here */
	if(!isset($_SESSION['DefaultCountryName']) or empty($_SESSION['DefaultCountryName']))
	{
		$_SESSION['DefaultCountryName'] = @constant("DEFINE_DEFAULT_COUNTRY");
	}
	/* Default Country start end */
	/* Default Currency start here */
	if(!isset($_SESSION['CurrentCurrencyID']) or empty($_SESSION['CurrentCurrencyID']))
	{
		$CurrencyObj = new DataTable(TABLE_CURRENCIES);
		$CurrencyObj->Where ="Active=1 AND DefaultValue=1";
		$DefaultCurrency = $CurrencyObj->TableSelectOne(array("CurrencyID"));
		$_SESSION['CurrentCurrencyID'] = $DefaultCurrency->CurrencyID;
	}
	$CurrencyObj = new DataTable(TABLE_CURRENCIES);
	$CurrencyObj->Where ="CurrencyID = '".$CurrencyObj->MysqlEscapeString($_SESSION['CurrentCurrencyID'])."'";
	$CurrentCurrency = $CurrencyObj->TableSelectOne("");
	/* Default Currency start end */
	if(isset($_REQUEST['Target']) && $_REQUEST['Target']=="CurrencyUpdate")
	{
		if(@$_REQUEST['SelCurrencyID'] !="")
		{
			$_SESSION['CurrentCurrencyID'] = $_REQUEST['SelCurrencyID'];
			MyRedirect(str_replace("Target=CurrencyUpdate&","",(isset($_SERVER['REQUEST_URI'])?$_SERVER['REQUEST_URI']:(isset($_SERVER['HTTP_X_REWRITE_URL'])?$_SERVER['HTTP_X_REWRITE_URL']:""))));
			exit;
		}
	}
	
	function SaveMetaKeywords($keywords)
	{
		$Arr = explode(",",$keywords);
		foreach ($Arr as $k=>$v)
		{
			if($v !="")
			{
				$KeywordObj = new DataTable(TABLE_KEYWORDS);
				$KeywordObj->Where ="Keyword='".$KeywordObj->MysqlEscapeString(trim($v))."'";
				$CurrentKeyword = $KeywordObj->TableSelectOne();
				if(!isset($CurrentKeyword->Keyword))
					$KeywordObj->TableInsert(array("Keyword"=>trim($v)));
			}
		}
	}
	function GetLoyalityPoint($UserID)
	{
		global $PaymentStatusArray,$PointStatusArray;
		$UserPointObj = new DataTable(TABLE_USERS_POINT);
		$UserPointObj->Where ="UserID ='".$UserID."' AND OrderStatus='".$PaymentStatusArray['Paid']."' AND PointStatus='".$PointStatusArray['Get']."'";
		$GetPointObj = $UserPointObj->TableSelectOne(array("SUM(Points) as GetPoint"));
		$UserPointObj->Where ="UserID ='".$UserID."' AND OrderStatus='".$PaymentStatusArray['Paid']."' AND PointStatus='".$PointStatusArray['Access']."'";
		$AccessPointObj = $UserPointObj->TableSelectOne(array("SUM(Points) as AccessPoint"));
		$Total = (isset($GetPointObj->GetPoint) && $GetPointObj->GetPoint >0)?$GetPointObj->GetPoint:0;
		$AccessPoint = (isset($AccessPointObj->AccessPoint) && $AccessPointObj->AccessPoint >0)?$AccessPointObj->AccessPoint:0;
		return array("Total"=>$Total,
						"AccessPoint"=>$AccessPoint,
						"LeftPoint"=>$Total-$AccessPoint,
					   );
	}
	
	function UpdatePaidOrderFromAdmin($OrderID,$PaymentStatus,$ShippingStatus)
	{
		if($PaymentStatus=="Paid")
		{
			$OrderChangeStatusObj = new DataTable(TABLE_ORDER_CHANGE_STATUS);
			$OrderChangeStatusObj->Where ="OrderID = '$OrderID' AND OrderStatus= 'Paid'";
			$OrderChangeStatusObj->TableSelectAll(array("OrderID"));
			if($OrderChangeStatusObj->GetNumRows()==0)
			{
				UpdatePaidOrderSetting($OrderID,false,true);
			}
		}
	}
	function UpdatePaidOrderSetting($OrderID,$TransactionID,$InvoiceSent = true)
	{
		$PaymentStatus ="Paid";
		//$PaymentStatus ="Pending";
		global $PaymentStatusArray,$ShippingStatusArray,$OrderStatusArray;
		@ob_clean();
		
		
		/* Stock updation Start*/
		$MinStockProducts = array();
		$OrderDetailObj = new DataTable(TABLE_ORDER_DETAILS);
		$OrderDetailObj->Where ="OrderID='".$OrderID."'";
		$OrderDetailObj->TableSelectAll("","OrderDetailID ASC");
		while($CurrentCartItem = $OrderDetailObj->GetObjectFromRecord())
		{
			$ProductID ="";
			if($CurrentCartItem->ItemType=="Option")
			{
				$ProductOptionObj = new DataTable(TABLE_PRODUCT_OPTIONS);
				$ProductOptionObj->Where = "ProductOptionID ='".$CurrentCartItem->ReferenceID."'";
				$CurrentOption = $ProductOptionObj->TableSelectOne(array("ProductID","Stock"));
				$ProductID = $CurrentOption->ProductID;
				
				if(@constant("DEFINE_PRODUCT_STOCK_ALLOW")=="1")
				{
					$StockArray = array();
					$StockArray['Stock'] = $CurrentOption->Stock - $CurrentCartItem->Qty;
					if($StockArray['Stock'] <=0)
					{
						$StockArray['Active'] = "0";
					}					
					$ProductOptionObj->TableUpdate($StockArray);
				}
			}
			else if($CurrentCartItem->ItemType=="Product")
			{
			  $ProductID = $CurrentCartItem->ReferenceID;
			  $ProductObj = new DataTable(TABLE_PRODUCT);
			  $ProductObj->Where = "ProductID ='".$CurrentCartItem->ReferenceID."'";
			  $CurrentProduct = $ProductObj->TableSelectOne(array("ProductID","Stock","MinStockLevel"));
			   
			  if(@constant("DEFINE_PRODUCT_STOCK_ALLOW")=="1")
				{
					$StockArray = array();
					$StockArray['Stock'] = $CurrentProduct->Stock - $CurrentCartItem->Qty;
					if($StockArray['Stock'] <=0)
					{
						$StockArray['Active'] = "0";
					}
					if(intval($CurrentProduct->MinStockLevel) > 0 && intval($StockArray['Stock']) <= intval($CurrentProduct->MinStockLevel)){
						$MinStockProducts[] = $CurrentProduct->ProductID;
					}
					$ProductObj->TableUpdate($StockArray);
					//$ProductObj->TableUpdate(array("Stock"=>"Stock-".$CurrentCartItem->Qty),false);
				}
			}
			
			if($ProductID !="")
			{
				for($i=1;$i<=$CurrentCartItem->Qty;$i++)
				{
					$PDataArray = array();
					$ProducReportObj = new DataTable(TABLE_PRODUCT_REPORTS);
					$PDataArray['ProductID'] = $ProductID;
					$PDataArray['ViewType'] = 'P';
					$PDataArray['CreatedDate'] = date("Y-m-d H:i:s");
					$ProducReportObj->TableInsert($PDataArray);			
				}
			}
			
		}
		
		/* Stock updation End */
		
		$OrderObj = new DataTable(TABLE_ORDERS);
		$OrderObj->Where = "OrderID ='$OrderID'";
		$DataArray = array();
		$DataArray['Attempted'] = "1";
		$DataArray['PaymentStatus'] = $PaymentStatus;
		$DataArray['PaymentDate'] = date("Y-m-d H:i:s");
		$DataArray['OrderStatus'] = $OrderStatusArray["Paid"];
		$DataArray['TransactionID'] = $TransactionID;
		$OrderObj->TableUpdate($DataArray);	
		$CurrentOrder = $OrderObj->TableSelectOne();	
		/* ORDER change status insert start*/
		$OrderChangeStatusObj = new DataTable(TABLE_ORDER_CHANGE_STATUS);
		$DataArray = array();
		$DataArray['OrderID'] = $OrderID;
		$DataArray['OrderStatus'] = $OrderStatusArray["Paid"];
		$DataArray['ShippingStatus'] = $ShippingStatusArray['Pending'];
		$DataArray['Comments'] = "";
		$DataArray['Notify'] ="0";
		$DataArray['CreatedBy'] ="SCRIPT";
		$DataArray['CreatedDate'] = date("Y-m-d H:i:s");
		$OrderChangeStatusObj->TableInsert($DataArray);
		/* ORDER change status insert end*/
		/*Coupon Limit Start*/
		if(isset($CurrentOrder->VoucherAmount) && $CurrentOrder->VoucherAmount > 0 && isset($CurrentOrder->VoucherCode) && $CurrentOrder->VoucherCode  !="")
		{
			$CouponObj = new DataTable(TABLE_COUPONS);
			$CouponObj->Where ="CouponCode='".$CouponObj->MysqlEscapeString(trim($CurrentOrder->VoucherCode))."'";
	 		$CouponObj->TableUpdate(array("CouponUsed"=>"CouponUsed+1"),false);
		}
		/*Coupon Limit End*/
		/* User Points Table Start*/
		$UserPointObj = new DataTable(TABLE_USERS_POINT);
		$UserPointObj->Where = "OrderID ='$OrderID'";
		$DataArray = array();
		$DataArray['OrderStatus'] = $PaymentStatus;
		$UserPointObj->TableUpdate($DataArray);	
		/* User Points Table End*/
		//require_once(DIR_FS_SITE_INCLUDES."text-invoice.php");
		include(dirname(__FILE__)."/invoice.php");
		$CartObj = new DataTable(TABLE_TMPCART);
		$SessionID = session_id();
		$CartObj->Where =" SessionID='".$CartObj->MysqlEscapeString($SessionID)."'";
		$CartObj->TableDelete();	
		
	}
	
	function UpdatePendingOrderSetting($OrderID,$TransactionID,$InvoiceSent = true)
	{
		$PaymentStatus ="Pending";
		global $PaymentStatusArray,$ShippingStatusArray,$OrderStatusArray;
		@ob_clean();
		
		
		$OrderObj = new DataTable(TABLE_ORDERS);
		$OrderObj->Where = "OrderID ='$OrderID'";
		$DataArray = array();
		$DataArray['Attempted'] = "1";
		$DataArray['PaymentStatus'] = $PaymentStatus;
		$DataArray['OrderStatus'] = $OrderStatusArray["PendingPayment"];
		$DataArray['TransactionID'] = $TransactionID;
		$OrderObj->TableUpdate($DataArray);	
		$CurrentOrder = $OrderObj->TableSelectOne();	
		/* ORDER change status insert start*/
		$OrderChangeStatusObj = new DataTable(TABLE_ORDER_CHANGE_STATUS);
		$DataArray = array();
		$DataArray['OrderID'] = $OrderID;
		$DataArray['OrderStatus'] = $OrderStatusArray["PendingPayment"];
		$DataArray['ShippingStatus'] = $ShippingStatusArray['Pending'];
		$DataArray['Comments'] = "";
		$DataArray['Notify'] ="0";
		$DataArray['CreatedBy'] ="SCRIPT";
		$DataArray['CreatedDate'] = date("Y-m-d H:i:s");
		$OrderChangeStatusObj->TableInsert($DataArray);
		/* ORDER change status insert end*/
		
		include(dirname(__FILE__)."/invoice.php");
		$CartObj = new DataTable(TABLE_TMPCART);
		$SessionID = session_id();
		$CartObj->Where =" SessionID='".$CartObj->MysqlEscapeString($SessionID)."'";
		$CartObj->TableDelete();	
		
	}
	
	function GiftVoucherQueue($OrderID)
	{
		global $CouponTypeArray;
		$OrderObj = new DataTable(TABLE_ORDERS);
		$OrderObj->Where ="OrderID='".$OrderID."'";
		$CurrentOrder = $OrderObj->TableSelectOne();
		/*Friend Gift Certificate Start*/
		if(defined("DEFINE_FRIEND_DISCOUNT_ENABLED") && constant("DEFINE_FRIEND_DISCOUNT_ENABLED")=="1")
		{
			$OrderObj2 = new DataTable(TABLE_ORDERS);
			$OrderObj2->Where ="Email='".$CurrentOrder->Email."' AND PaymentStatus ='Paid'";
			$CurrentRefObj = $OrderObj2->TableSelectOne(array("Count(*) as TotalRefOrder"));
			if($CurrentRefObj->TotalRefOrder==0)
			{
				$FriendRequestObj = new DataTable(TABLE_FRIEND_REQUESTS);
				$FriendRequestObj->Where  ="FriendEmail ='".$CurrentOrder->Email."'";
				$CurrentSender = $FriendRequestObj->TableSelectOne(array("FromEmail"),"CreatedDate DESC");
				if(isset($CurrentSender->FromEmail) && $CurrentSender->FromEmail !="")
				{
					$UserObj = new DataTable(TABLE_USERS);
					$UserObj->Where="Email='".$CurrentSender->FromEmail."'";
					$CurrentUser = $UserObj->TableSelectOne();
					if(isset($CurrentUser->UserID) && $CurrentUser->UserID !="")
					{
						$CouponObj = new DataTable(TABLE_COUPONS);
						$CouponCode = uniqid("GC_");
						$DataArray['CouponName'] = "Gift Certificate";
						$DataArray['Message'] = "Gift Certificate";
						$DataArray['CouponType'] = $CouponTypeArray['Percentage'];
						$DataArray['CouponCode'] = $CouponCode;
						$DataArray['Active'] = "1";
						$DataArray['CouponValue'] = @constant("DEFINE_FRIEND_DISCOUNT");
						$DataArray['GvEnabled'] = "1";
						$DataArray['CouponLimit'] = "1";
						$DataArray['CreatedDate'] = date('Y-m-d H:i:s');
						$CouponID = $CouponObj->TableInsert($DataArray);
						$GvTrackObj = new DataTable(TABLE_GV_TRACK);
						$DataArray = array();
						$DataArray['CouponID'] = $CouponID;
						$DataArray['UserID'] = $CurrentUser->UserID;
						$DataArray['FromEmail'] = $CurrentOrder->Email;
						$DataArray['FromName'] = $CurrentOrder->BillingFirstName." ".$CurrentOrder->BillingLastName;
						$DataArray['Message'] = "";
						$DataArray['ToName'] = $CurrentUser->FirstName." ".$CurrentUser->LastName;
						$DataArray['ToEmail'] = $CurrentUser->Email;
						$DataArray['CreatedDate'] = date('Y-m-d H:i:s');
						$GvTrackObj->TableInsert($DataArray);
						$CustomObj = new Custom();
						$SendArray = array("Amount"=>@constant("DEFINE_FRIEND_DISCOUNT")." % OFF",
							   "From"=>DEFINE_SITE_NAME,
							   "ToName"=>$CurrentUser->FirstName." ".$CurrentUser->LastName,
							   "Message"=>"<a href='".DIR_WS_SITE."' target='_blank'>".DEFINE_SITE_NAME."</a>",
							   "CouponCode"=>$CouponCode,
							   "Mail_Subject"=>"Congratulations, you have recieved a gift certificate for ".DEFINE_SITE_NAME,
							   "Mail_ToEmail"=>$CurrentUser->Email,
							   "Mail_FromEmail"=>DEFINE_ADMIN_EMAIL,
							   "Mail_FromName"=>DEFINE_SITE_NAME,
							   );
					    $CustomObj->SendMailByUsingTemplate("gift_certificate.php",$SendArray);
					}
				}
			}
		}
		/*Friend Gift Certificate End*/
	}
	function SKSpecialOffer()
	{
		global $OfferTypeArray;
		$Page = (isset($_GET['Page']) && $_GET['Page'] !="")?$_GET['Page']:"";
		if($Page =="shop_cart")
		{
			$MainCartArray = array();
			$OfferQtyArray = array();
			$OfferArray = array();
			$PriceCartArray = array();
			$SessionID = session_id();
			$CartObj = new DataTable(TABLE_TMPCART);
			/* offer deleted*/
			$CartObj->Where ="ItemType ='Offer' AND SessionID='".$CartObj->MysqlEscapeString($SessionID)."' AND (ParentItemID='' OR ISNULL(ParentItemID))";
			$CartObj->TableDelete();
			/* offer deleted*/
			/* cart start*/
			$CartObj->Where =" SessionID='".$CartObj->MysqlEscapeString($SessionID)."' AND (ParentItemID='' OR ISNULL(ParentItemID))";
			$CartObj->TableSelectAll("","TmpCartID ASC");
			if ($CartObj->GetNumRows()>0)
			{
				while($CurrentCartItem = $CartObj->GetObjectFromRecord())
				{
					$ProductID = $CurrentCartItem->ReferenceID;
					if($CurrentCartItem->ItemType=="Option")
					{
						$ProductOptionObj = new DataTable(TABLE_PRODUCT_OPTIONS);
						$ProductOptionObj->Where = "ProductOptionID ='".$CurrentCartItem->ReferenceID."'";
						$CurrentOption = $ProductOptionObj->TableSelectOne(array("ProductID"));
						$ProductID = $CurrentOption->ProductID;
					}
					$OfferQtyArray[] = $CurrentCartItem->Qty;									
					for($i=1;$i<=$CurrentCartItem->Qty;$i++)
					{								
						$MainCartArray[] = $ProductID;	
						$PriceCartArray[] = $CurrentCartItem->Price;
					}
				}
					/*Fetch offer start*/
					$OfferObj = new DataTable(TABLE_SPECIAL_OFFERS);
					$OfferProductObj  = new DataTable(TABLE_OFFER_PRODUCTS);
					$OfferObj->Where ="Active ='1' and (CURDATE() BETWEEN StartDate AND EndDate OR (StartDate ='0000-00-00' OR EndDate ='0000-00-00'))";
					$OfferObj->TableSelectAll();
					while ($CurrentOffer = $OfferObj->GetObjectFromRecord())
					{
						$OfferProductObj->Where ="OfferID='$CurrentOffer->OfferID'";
						$OfferProductAll = $OfferProductObj->TableSelectOne(array("GROUP_CONCAT(ProductID SEPARATOR ', ') as AllProductID"));
						$OfferArray[$CurrentOffer->OfferID] = explode(",",$OfferProductAll->AllProductID);
					}
					/*Fetch offer end*/
					/*Match offer array with main array start*/
					$Loop =0;
					while (true)
					{
						$Init = count($MainCartArray);
						$End = count($MainCartArray);
						foreach ($OfferArray as $k=>$v)
						{
							if(CheckSubArrayInMasterArray($MainCartArray,$v))
							{
								$Obj =  ReduceArrayFromMasterArray($MainCartArray,$v,$PriceCartArray);
								$MainCartArray =  $Obj->MasterArray;
								$TotalPrice = $Obj->TotalPrice;
								$PriceCartArray = $Obj->PriceArray;
								$End = count($MainCartArray);
								$OfferID = $k;
								/*Add offer to tmpcart start*/
								if($TotalPrice >0)
								{
									$OfferObj->Where ="OfferID ='$OfferID' AND Active ='1' and (CURDATE() BETWEEN StartDate AND EndDate OR (StartDate ='0000-00-00' OR EndDate ='0000-00-00'))";
									$COffer = $OfferObj->TableSelectOne();
										if($COffer->OfferType==$OfferTypeArray['Percentage'])
										{
											$DisVal = ($COffer->OfferValue/100)*$TotalPrice;
										}
										elseif ($COffer->OfferType==$OfferTypeArray['FlatPrice'])
										{
											if($TotalPrice > $COffer->OfferValue)
											{
												$DisVal =  $COffer->OfferValue;
											}
										}
										/*insert new record start*/		
										if($DisVal >0)
										{
											$TmpCartObj = new DataTable(TABLE_TMPCART);
											$DataArray = array();
											$DataArray['ItemID'] = uniqid("Offer_");
											$DataArray['ItemType'] = "Offer";
											$DataArray['ReferenceID'] = $COffer->OfferID;
											$DataArray['ItemName'] = "Discount:".(isset($COffer->Message)?$COffer->Message:"");
											$DataArray['ItemNo'] = isset($COffer->Message)?$COffer->Message:"";
											$DataArray['Price'] = -$DisVal;
											$DataArray['Qty'] = "1";
											$DataArray['Total'] = $DataArray['Price'] * $DataArray['Qty'];
											$DataArray['Comments'] = "";		
											$DataArray['SessionID'] = session_id();
											$TmpCartObj->TableInsert($DataArray);
										}
										/*insert new record end*/		
								}								
								/*Add offer to tmpcart end*/
							}
						}
						$Loop++;
						if($Init == $End)
							break;
					}
					/*Match offer array with main array end*/
			}
			/* cart end*/
		}
	}
	
	function CheckTmpCartAlways($force=false)
	{
		$CartObj = new DataTable(TABLE_TMPCART);
		$TmpCartObj = new DataTable(TABLE_TMPCART);
		$CartObj->Where ="ItemType='Product'  AND SessionID='".$CartObj->MysqlEscapeString(session_id())."'";
		$CartObj->TableSelectAll();
				
		if((count($_POST)==0 && $CartObj->GetNumRows() > 0) || $force===true)
		{
			if(function_exists("SKSpecialOffer"))
				SKSpecialOffer();
				/* Update Shopping Cart*/
				while ($CurrentCart = $CartObj->GetObjectFromRecord()) 
				{
					if(isset($CurrentCart->ItemID) && $CurrentCart->ItemID !="")
					{
						$PostArray = array();
						if(isset($CurrentCart->SessionInfo) && $CurrentCart->SessionInfo != "")
							$PostArray = unserialize(base64_decode($CurrentCart->SessionInfo));
						
						$PriceArray = GetProductPriceNStock($CurrentCart->ReferenceID,$CurrentCart->Qty,$PostArray);
						$TmpCartObj->Where ="ItemID='".$CurrentCart->ItemID."'";
						$DataArray = array();
						$DataArray['UnitPrice'] = isset($PriceArray['UnitPrice'])?$PriceArray['UnitPrice']:"";
						$DataArray['TaxPrice'] = isset($PriceArray['TaxPrice'])?$PriceArray['TaxPrice']:"";
						$DataArray['TaxPercent'] = isset($PriceArray['TaxPercent'])?$PriceArray['TaxPercent']:"";
						$DataArray['Discount'] = isset($PriceArray['Discount'])?$PriceArray['Discount']:"";
						$DataArray['Price'] = isset($PriceArray['Price'])?$PriceArray['Price']:"";
						$DataArray['Qty'] = isset($PriceArray['Qty'])?$PriceArray['Qty']:"";
						$DataArray['Total'] = $DataArray['Price'] * $DataArray['Qty'];
						$TmpCartObj->TableUpdate($DataArray);
						
					}
				}
				/* Update Shopping Cart*/
		
			if(isset($_SESSION['CouponCode']) && $_SESSION['CouponCode'] !="")
			{
				$ShoppingCalculationObj = new ShoppingCalculations();
				$ShoppingCalculationObj->CheckCartDiscount($_SESSION['CouponCode']);
			}

		}
		//var_dump("dfdfdf");exit;
		
	}
	
function SKDisplayMainPrdImage($Image,$Size="",$Title="",$Type="",$Parameter ="",$URLOnly=false)
{
	if($Image !="" && stristr($Image,"http"))
	{
		if(stristr($Image,"http"))
		{
			$httpPath = $Image;
			$Image = DIR_FS_SITE_UPLOADS_TMP.basename($Image);
			if(!file_exists($Image))
			{
				$ch = curl_init($httpPath);
				$fp = fopen($Image, "w");
				curl_setopt($ch, CURLOPT_FILE, $fp);
				curl_setopt($ch, CURLOPT_HEADER, 0);
				curl_exec($ch);
				curl_close($ch);
			}
		}
		return SKImgDisplay($Image,$Size,$Title,'',true,$URLOnly);
		/*
		if(isset($Type['Thumbnail']))
			return SKImgDisplay($Thumbnail,'',$Title,$Parameter,false,$URLOnly);	
		else 
			return SKImgDisplay($Image,'',$Title,$Parameter,false,$URLOnly);	
	  */
	}
	else
	{
		return SKImgDisplay(DIR_FS_SITE_UPLOADS_PRODUCT_ORIGINAL.$Image,$Size,$Title,'',true,$URLOnly);
	}
}
if(file_exists(dirname(__FILE__)."/function.attribute.php"))
	require_once(dirname(__FILE__)."/function.attribute.php");
if(file_exists(dirname(__FILE__)."/function.category.php"))
	require_once(dirname(__FILE__)."/function.category.php");
if(file_exists(dirname(__FILE__)."/function.product.php"))
	require_once(dirname(__FILE__)."/function.product.php");
if(file_exists(dirname(__FILE__)."/function.manufacturer.php"))
	require_once(dirname(__FILE__)."/function.manufacturer.php");

if(file_exists(dirname(__FILE__)."/process_cart.php"))
	require_once(dirname(__FILE__)."/process_cart.php");

