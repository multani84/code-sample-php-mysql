
        <div class="col-md-12 pull-left">
				   
                       <table class="table table-striped table-bordered responsive">
						<thead><tr><th colspan="2">Billing Address</th></tr></thead>
						<tbody>
						<tr>
							<td class="right"><b>Email</td>
							<td><?=$CurrentOrder->Email?></td>
						</tr>
						<tr>
							<td class="right"><b>Name</td>
							<td><?=$CurrentOrder->BillingFirstName?>&nbsp;&nbsp;<?=$CurrentOrder->BillingLastName?></td>
						</tr>
						<tr>
							<td class="right"><b>Address</td>
							<td><?=$CurrentOrder->BillingAddress1?></td>
						</tr>
						<tr>
							<td class="right"><b>Address</td>
							<td><?=$CurrentOrder->BillingAddress2?></td>
						</tr>
						<tr>
							<td class="right"><b>Town/City</td>
							<td><?=$CurrentOrder->BillingCity?></td>
						</tr>
						<tr>
							<td class="right"><b>County/State</td>
							<td><?=$CurrentOrder->BillingState?></td>
						</tr>
						<tr>
							<td class="right"><b>Country</b></td>
							<td><?=$CurrentOrder->BillingCountry?></td>
						</tr>
						<tr>
							<td class="right"><b>Postal/Zip Code</td>
							<td><?=$CurrentOrder->BillingZip?></td>
						</tr>
						<tr>
							<td class="right"><b>Phone</b></td>
							<td><?=$CurrentOrder->BillingPhone?></td>
						</tr>
						
						</tbody>
					</table>
				 </div>
        <div class="col-md-12 pull-right">
				<?php
				if(@constant("SHIPPING_INFO_DISPLAY")!="0")
				{?>
				
                   
					    <table class="table table-striped table-bordered responsive">
						<thead>
						<th colspan="2">Shipping Address</th>
						</thead>
						<tbody>
						<tr>
							<td class="right"><b>Name</td>
							<td><?=$CurrentOrder->ShippingFirstName?>&nbsp;&nbsp;<?=$CurrentOrder->ShippingLastName?></td>
						</tr>
						<tr>
							<td class="right"><b>Address1</td>
							<td><?=$CurrentOrder->ShippingAddress1?></td>
						</tr>
						<tr>
							<td class="right"><b>Address2</b></td>
							<td><?=$CurrentOrder->ShippingAddress2?></td>
						</tr>
						<tr>
							<td class="right"><b>Town/City</td>
							<td><?=$CurrentOrder->ShippingCity?></td>
						</tr>
						<tr>
							<td class="right"><b>County/State</td>
							<td><?=$CurrentOrder->ShippingState?></td>
						</tr>
						<tr>
							<td class="right"><b>Country</b></td>
							<td><?=$CurrentOrder->ShippingCountry?></td>
						</tr>
						<tr>
							<td class="right"><b>Postal/Zip Code</td>
							<td><?=$CurrentOrder->ShippingZip?></td>
						</tr>
						<tr>
							<td class="right"><b>Phone</b></td>
							<td><?=$CurrentOrder->ShippingPhone?></td>
						</tr>
						
					
						</tbody>
					</table>
				</div>
				<?php
				}?>
		
		<?php
			if(!empty($CurrentOrder->Comments))
			{
		?>
				<hr/>
				<div class="col-sm-24">
					<table class="table table-striped table-bordered">
					<thead>
					<tr>
						<th><b>COMMENTS / INSTRUCTIONS</b></th>
					</tr>
					</thead>
					<tbody>
					<tr>
						<td><?=nl2br($CurrentOrder->Comments)?></td>
					</tr>
					</tbody>
					</table>
				</div>					
				<?php
				}
				?>	


