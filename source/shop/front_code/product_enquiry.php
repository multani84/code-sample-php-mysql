<?php
$ProductObj= new DataTable(TABLE_PRODUCT);
$Section = isset($_REQUEST['Section'])?$_REQUEST['Section']:"";
$Target = isset($_REQUEST['Target'])?$_REQUEST['Target']:"";

if($Target=="EnquirySubmit")
{	
  	$MyPostArray = array();
  	$MyPostArray['Mail_ToEmail'] = @$_POST['Mail_ToEmail'];
	$MyPostArray['Mail_BCC'] = DEFINE_BCC_EMAIL;
  	$MyPostArray['Mail_Subject'] = @$_POST['Mail_Subject'];
  	$MyPostArray['Mail_Header'] = "";//@$_POST['Mail_Header'];
  	$MyPostArray['Mail_Footer'] = "";//@$_POST['Mail_Footer'];
  	
  	$MyPostArray['First Name'] = @($_POST['FirstName']);
	$MyPostArray['Last Name'] = @($_POST['LastName']);
	$MyPostArray['Email'] = @($_POST['Email']);
	$MyPostArray['Telephone'] = @($_POST['Telephone']);
   	$MyPostArray['Message'] = @$_POST['Message'];
	
	$ProductData = "";
	foreach($_SESSION['EnquiryProducts'] as $ProductID=>$Val)
	{
		$ProductObj->Where ="ProductID='".(int)$ProductObj->MysqlEscapeString($ProductID)."' AND Active='1'";
		$CurrentProduct = $ProductObj->TableSelectOne();
		
		$ProductData .= MyStripSlashes($CurrentProduct->ProductName)." (SKU:".MyStripSlashes($CurrentProduct->ModelNo).") <a href='".SKSEOURL($CurrentProduct->ProductID,"shop/product")."'>View</a><hr />";
	}
	$MyPostArray['Products'] = $ProductData;
	$ErrorArr = array();
	$_SESSION['InfoMessage'] = "Thank you, your message has been sent. We will be in touch with you shortly.";
	$CustomObj->SendMailByUsingTemplate("enquiry.php",$MyPostArray);
	unset($_SESSION['EnquiryProducts']);
	MyRedirect(MakePageURL("index.php","Page=shop/product_enquiry"));
	exit;
 
}

?>