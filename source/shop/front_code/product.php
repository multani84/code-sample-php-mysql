<?php
$Section = isset($_REQUEST['Section'])?$_REQUEST['Section']:"";
$Target = isset($_REQUEST['Target'])?$_REQUEST['Target']:"";
$ProductID = isset($CurrentPage->ReferenceID)?$CurrentPage->ReferenceID:(int)@$_REQUEST['ID'];

$ProductObj= new DataTable(TABLE_PRODUCT);


if($ProductID !="")
{
	$ProductObj->Where ="ProductID='".$ProductObj->MysqlEscapeString($ProductID)."' AND Active='1'";
	$CurrentProduct = $ProductObj->TableSelectOne();
	

	$ProdCatObj = new DataTable(TABLE_CATEGORY. " c, ".TABLE_PRODUCT_RELATION. " ptc");
	$ProdCatObj->Where = "c.Active='1' and ptc.RelationType='category' and ptc.RelationID=c.CategoryID and ptc.ProductID='".$ProdCatObj->MysqlEscapeString($ProductID)."'";
	
	if(isset($_SESSION['RecentCatID']) && $_SESSION['RecentCatID'] != "")
		$CatOrderBy = "c.CategoryID='".(int)$_SESSION['RecentCatID']."' DESC";
	else 
		$CatOrderBy = "";
	
	$CurrentProductCategory = $ProdCatObj->TableSelectOne(array("c.CategoryID","CategoryName","c.ParentID"),$CatOrderBy);	
					
	if(@$_SESSION['RecentCatID']=="")
		$_SESSION['RecentCatID'] = isset($CurrentProductCategory->CategoryID)?$CurrentProductCategory->CategoryID:"";
	
	if(isset($CurrentProduct->ProductID) && $CurrentProduct->ProductID !="")
	{
		@ob_clean();
		$_SESSION['RecentViewed'] = $ProductID;
		
		$PDataArray = array();
		$ProducReportObj = new DataTable(TABLE_PRODUCT_REPORTS);
		$PDataArray['ProductID'] = $ProductID;
		$PDataArray['ViewType'] = 'V';
		$PDataArray['CreatedDate'] = date("Y-m-d H:i:s");
		$ProducReportObj->TableInsert($PDataArray);	
	}

}

if(isset($_GET['Target']) && $_GET['Target'] =="AddEnquiry")
{
	if(!isset($_SESSION['EnquiryProducts']))
		$_SESSION['EnquiryProducts'] = array();
	
	$_SESSION['EnquiryProducts'][$ProductID] = $ProductID;
	
	$_SESSION['InfoMessage'] = "Product added in your enquiry list.";
	MyRedirect(SKSEOURL($CurrentProduct->ProductID,"shop/product"));			
	exit;
	
}
	

if(isset($_GET['Target']) && $_GET['Target'] =="Login")
{
	$CurrentUserObj = $CustomObj->CheckUserLogin(SKSEOURL($ProductID,"shop/product"));
}
if(isset($_GET['Target']) && $_GET['Target'] =="AddQuestion")
{
	$DataArray = array();
	$DataArray['ProductID']	= $CurrentProduct->ProductID;
	$DataArray['UserID'] 	= isset($IsCurrentUser->UserID)?$IsCurrentUser->UserID:"";
	$DataArray['Question'] 	= isset($_POST['Question'])?$_POST['Question']:"";
	$DataArray['CEmail'] 	= isset($_POST['CEmail'])?$_POST['CEmail']:"";
	$DataArray['CName'] 	= isset($_POST['CName'])?$_POST['CName']:"";
	$DataArray['CreatedDate'] 	= date('YmdHis');
	
		$ProductQuestionObj = new DataTable(TABLE_PRODUCT_QUESTIONS);
		$ProductQuestionObj->TableInsert($DataArray);
		
		$Mail_Subject = "A new question has been asked at ".SITE_NAME.":".$CurrentProduct->ProductName;
		$Mail_ToEmail = isset($_POST['FEmail'])?$_POST['FEmail']:DEFINE_ADMIN_EMAIL;
		$Mail_FromEmail = isset($_POST['Email'])?$_POST['Email']:DEFINE_ADMIN_EMAIL;
		$Mail_FromName = isset($_POST['Name'])?$_POST['Name']:SITE_NAME;
		$MessageBody ="A new question has been asked at site. ".chr(13).chr(13)."Please check this product at ".SITE_NAME.":".$CurrentProduct->ProductName.chr(13).SKSEOURL($CurrentProduct->ProductID,"product").chr(13).chr(13)."Thanks!";
		SendEmail($Mail_Subject,$Mail_ToEmail,$Mail_FromEmail,$Mail_FromName,$MessageBody);
			
		@ob_clean();
		
		$_SESSION['InfoMessage'] = "We shall get in touch with you soon.";
		MyRedirect(SKSEOURL($CurrentProduct->ProductID,"shop/product"));			
		exit;
}
?>