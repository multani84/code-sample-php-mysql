<?php
	$CurrentUserObj = $CustomObj->CheckUserLogin(MakePageURL("index.php","Page=$Page&Target=".@$_REQUEST['Target']."&ProductID=".@$_REQUEST['ProductID']."&WCategory=".@$_REQUEST['WCategory']));
	$DataArray = array();
	
$Section = isset($_REQUEST['Section'])?$_REQUEST['Section']:"";
$Target = isset($_REQUEST['Target'])?$_REQUEST['Target']:"";
$ProductID = isset($_GET['ProductID'])?$_GET['ProductID']:0;
$WCategory = isset($_GET['WCategory'])?$_GET['WCategory']:"";
$WCategoryID = isset($_GET['WCategoryID'])?$_GET['WCategoryID']:0;

$ProductObj= new DataTable(TABLE_PRODUCT);
$UserWCategoryObj = new DataTable(TABLE_USERS_WCATEGORY);
	
if(isset($_GET['Target']) && $_GET['Target'] =="AddList")
{
	
	if($WCategory !="")
	{
		$DataArray = array();
		$DataArray['UserID'] 	= isset($CurrentUserObj->UserID)?$CurrentUserObj->UserID:"";
		
		$UserWCategoryObj->Where ="UserID='".$DataArray['UserID']."' AND CategoryName ='".$UserWCategoryObj->MysqlEscapeString($WCategory)."'";
		$CurrentWCategory = $UserWCategoryObj->TableSelectOne();
		if(isset($CurrentWCategory->WCategoryID) && $CurrentWCategory->WCategoryID !="")
		{
			$WCategoryID  = $CurrentWCategory->WCategoryID;
		}
		else 
		{
			
			$UserWCategoryObj->Where ="1";
			$WCategoryID = $UserWCategoryObj->GetMax("WCategoryID") + 1;
			$DataArray['WCategoryID'] = $WCategoryID;
			$DataArray['CategoryName']	= $WCategory;
			$DataArray['CreatedDate'] 	= date('YmdHis');
			$UserWCategoryObj->TableInsert($DataArray);
			
		}
		
	}
	
	$DataArray = array();
	$DataArray['ProductID']	= $ProductID;
	$DataArray['WCategoryID']	= $WCategoryID;
	$DataArray['UserID'] 	= isset($CurrentUserObj->UserID)?$CurrentUserObj->UserID:"";
	$DataArray['CreatedDate'] 	= date('YmdHis');
	
	$UserListObj = new DataTable(TABLE_USERS_WISHLIST);
	$UserListObj->Where ="WCategoryID='".$DataArray['WCategoryID']."' AND ProductID='".$DataArray['ProductID']."' AND UserID='".$DataArray['UserID']."'";
	$CurrentList = $UserListObj->TableSelectOne(array("UserID"));
	if(!isset($CurrentList->UserID))
	{
		$UserListObj->TableInsert($DataArray);
		@ob_clean();
		
		$_SESSION['InfoMessage'] = "you have updated your wishlist.";
		MyRedirect(MakePageURL("index.php","Page=wishlist&WCategoryID=$WCategoryID"));			
		exit;
	}
	
}
if(isset($_GET['Target']) && $_GET['Target'] =="RemoveList")
{
	$DataArray = array();
	$DataArray['ProductID']	= $ProductID;
	$DataArray['UserID'] 	= isset($CurrentUserObj->UserID)?$CurrentUserObj->UserID:"";
	
	$UserListObj = new DataTable(TABLE_USERS_WISHLIST);
	$UserListObj->Where ="ProductID='".$DataArray['ProductID']."' AND UserID='".$DataArray['UserID']."'";
	$CurrentList = $UserListObj->TableSelectOne(array("UserID"));
	if(isset($CurrentList->UserID))
	{
		$UserListObj->TableDelete();
		@ob_clean();
		
		$_SESSION['InfoMessage'] = "you have updated your wishlist.";
		MyRedirect(MakePageURL("index.php","Page=$Page&WCategoryID=$WCategoryID"));			
		exit;
	}
	
}

if(isset($_GET['Target']) && $_GET['Target'] =="SendRequest")
{
	$Mail_Subject = "Please check this list of prosuct  at ".SITE_NAME;
	$Mail_ToEmail = isset($_POST['FEmail'])?$_POST['FEmail']:"";
	$Mail_FromEmail = isset($_POST['Email'])?$_POST['Email']:"";
	$Mail_FromName = isset($_POST['Name'])?$_POST['Name']:SITE_NAME;
	$MessageBody =isset($_POST['MessageBody'])?$_POST['MessageBody']:"";
	SendEmail($Mail_Subject,$Mail_ToEmail,$Mail_FromEmail,$Mail_FromName,$MessageBody);
	
	$_SESSION['InfoMessage'] = "You have successfully sent email to your friend.";
	MyRedirect(MakePageURL("index.php","Page=$Page&WCategoryID=$WCategoryID"));			
	exit;
}

if(isset($_GET['Target']) && $_GET['Target'] =="RemoveWCategory")
{
	$DataArray = array();
	$DataArray['WCategoryID']	= $WCategoryID;
	$DataArray['UserID'] 	= isset($CurrentUserObj->UserID)?$CurrentUserObj->UserID:"";
	
	$UserListObj = new DataTable(TABLE_USERS_WISHLIST);
	$UserListObj->Where ="WCategoryID='".$DataArray['WCategoryID']."' AND UserID='".$DataArray['UserID']."'";
	$UserWCategoryObj->Where = $UserListObj->Where;
	$UserListObj->TableDelete();
	$UserWCategoryObj->TableDelete();
	
	@ob_clean();
	
	$_SESSION['InfoMessage'] = "you have updated your wishlist.";
	MyRedirect(MakePageURL("index.php","Page=$Page"));			
	exit;
	
}

?>