<?php
$ReferenceID = "105";
extract(GetPageDetail($ReferenceID));

$TmpCartObj = new DataTable(TABLE_TMPCART);
$AddressObj = new DataTable(TABLE_USERS_ADDRESS);

if(@constant("DEFINE_CHECKOUT_LOGIN")=="1" && @constant("DEFINE_CHECKOUT_WITHOUT_LOGIN")=="0")
	{
		$CurrentUserObj = $CustomObj->CheckUserLogin(MakePageURL("index.php","Page=shop/shop_checkout"));	
	}
	else 
	{
			$CurrentUserObj = $CustomObj->IsUserLogin();	
	}
	
	if(isset($_SESSION['OrderID']) && $_SESSION['OrderID'] !="")
	{
		$OrderID =$_SESSION['OrderID'];
		$OrderObj = new DataTable(TABLE_ORDERS);
		$OrderObj->Where ="OrderID='".$OrderID."'";
		$CurrentOrder = $OrderObj->TableSelectOne();
		
		$BillingFirstName = isset($CurrentOrder->BillingFirstName)?$CurrentOrder->BillingFirstName:"";
		$BillingLastName = isset($CurrentOrder->BillingLastName)?$CurrentOrder->BillingLastName:"";
		$BillingAddress1 = isset($CurrentOrder->BillingAddress1)?$CurrentOrder->BillingAddress1:"";
		$BillingAddress2 = isset($CurrentOrder->BillingAddress2)?$CurrentOrder->BillingAddress2:"";
		$BillingCity = isset($CurrentOrder->BillingCity)?$CurrentOrder->BillingCity:"";
		$BillingState = isset($CurrentOrder->BillingState)?$CurrentOrder->BillingState:"";
		$BillingCountry = isset($CurrentOrder->BillingCountry)?$CurrentOrder->BillingCountry:"";
		$BillingArea = isset($CurrentOrder->BillingArea)?$CurrentOrder->BillingArea:"";
		$BillingZip = isset($CurrentOrder->BillingZip)?$CurrentOrder->BillingZip:"";
		$BillingPhone = isset($CurrentOrder->BillingPhone)?$CurrentOrder->BillingPhone:"";
		$BillingFax = isset($CurrentOrder->BillingFax)?$CurrentOrder->BillingFax:"";
	
		$ShippingFirstName = isset($CurrentOrder->ShippingFirstName)?$CurrentOrder->ShippingFirstName:"";
		$ShippingLastName = isset($CurrentOrder->ShippingLastName)?$CurrentOrder->ShippingLastName:"";
		$ShippingAddress1 = isset($CurrentOrder->ShippingAddress1)?$CurrentOrder->ShippingAddress1:"";
		$ShippingAddress2 = isset($CurrentOrder->ShippingAddress2)?$CurrentOrder->ShippingAddress2:"";
		$ShippingCity = isset($CurrentOrder->ShippingCity)?$CurrentOrder->ShippingCity:"";
		$ShippingState = isset($CurrentOrder->ShippingState)?$CurrentOrder->ShippingState:"";
		$ShippingCountry = isset($CurrentOrder->ShippingCountry)?$CurrentOrder->ShippingCountry:"";
		$ShippingArea = isset($CurrentOrder->ShippingArea)?$CurrentOrder->ShippingArea:"";
		$ShippingZip = isset($CurrentOrder->ShippingZip)?$CurrentOrder->ShippingZip:"";
		$ShippingPhone = isset($CurrentOrder->ShippingPhone)?$CurrentOrder->ShippingPhone:"";
		$ShippingFax = isset($CurrentOrder->ShippingFax)?$CurrentOrder->ShippingFax:"";
		
		$SchoolCode = isset($CurrentOrder->SchoolCode)?$CurrentOrder->SchoolCode:"";
	
		$Email = isset($CurrentOrder->Email)?$CurrentOrder->Email:"";
		$Comments = isset($CurrentOrder->Comments)?$CurrentOrder->Comments:"";
		$PaymentMethod = isset($CurrentOrder->PaymentMethod)?$CurrentOrder->PaymentMethod:"";
		$ReferenceCode = isset($CurrentOrder->PartnerCode)?$CurrentOrder->PartnerCode:"";
			
	}
	else 
	{
		$BillingFirstName = isset($CurrentUserObj->FirstName)?$CurrentUserObj->FirstName:"";
		$BillingLastName = isset($CurrentUserObj->LastName)?$CurrentUserObj->LastName:"";
		$BillingAddress1 = isset($CurrentUserObj->Address1)?$CurrentUserObj->Address1:"";
		$BillingAddress2 = isset($CurrentUserObj->Address2)?$CurrentUserObj->Address2:"";
		$BillingCity = isset($CurrentUserObj->City)?$CurrentUserObj->City:"";
		$BillingState = isset($CurrentUserObj->State)?$CurrentUserObj->State:"";
		$BillingCountry = isset($CurrentUserObj->Country)?$CurrentUserObj->Country:@constant("DEFINE_DEFAULT_COUNTRY");
		$BillingArea = isset($CurrentUserObj->Area)?$CurrentUserObj->Area:"";
		$BillingZip = isset($CurrentUserObj->ZipCode)?$CurrentUserObj->ZipCode:"";
		$BillingPhone = isset($CurrentUserObj->Phone)?$CurrentUserObj->Phone:"";
		$BillingFax = isset($CurrentUserObj->Fax)?$CurrentUserObj->Fax:"";
	
		$ShippingFirstName = isset($CurrentUserObj->FirstName)?$CurrentUserObj->FirstName:"";
		$ShippingLastName = isset($CurrentUserObj->LastName)?$CurrentUserObj->LastName:"";
		$ShippingAddress1 = isset($CurrentUserObj->Address1)?$CurrentUserObj->Address1:"";
		$ShippingAddress2 = isset($CurrentUserObj->Address2)?$CurrentUserObj->Address2:"";
		$ShippingCity = isset($CurrentUserObj->City)?$CurrentUserObj->City:"";
		$ShippingState = isset($CurrentUserObj->State)?$CurrentUserObj->State:"";
		$ShippingCountry = isset($CurrentUserObj->Country)?$CurrentUserObj->Country:@constant("DEFINE_DEFAULT_COUNTRY");
		$ShippingArea = isset($CurrentUserObj->Area)?$CurrentUserObj->Area:"";
		$ShippingZip = isset($CurrentUserObj->ZipCode)?$CurrentUserObj->ZipCode:"";
		$ShippingPhone = isset($CurrentUserObj->Phone)?$CurrentUserObj->Phone:"";
		$ShippingFax = isset($CurrentUserObj->Fax)?$CurrentUserObj->Fax:"";
		
		$Email = isset($CurrentUserObj->Email)?$CurrentUserObj->Email:"";
		$ReferenceCode = isset($CurrentUserObj->ReferenceCode)?$CurrentUserObj->ReferenceCode:"";
	
	}
	
	$BAID = isset($_GET['BAID'])?$_GET['BAID']:0;
	$SAID = isset($_GET['SAID'])?$_GET['SAID']:0;
	
	/*$BAID Start*/
	if($BAID != 0)
	{
		if($BAID =="-2")
		{
			MyRedirect(MakePageURL("index.php","Page=shop/address_book&Section=AddAddress"));			
			exit;
		}
		
		if($BAID =="-1")
		{
			$BillingFirstName = isset($CurrentUserObj->FirstName)?$CurrentUserObj->FirstName:"";
			$BillingLastName = isset($CurrentUserObj->LastName)?$CurrentUserObj->LastName:"";
			$BillingAddress1 = isset($CurrentUserObj->Address1)?$CurrentUserObj->Address1:"";
			$BillingAddress2 = isset($CurrentUserObj->Address2)?$CurrentUserObj->Address2:"";
			$BillingCity = isset($CurrentUserObj->City)?$CurrentUserObj->City:"";
			$BillingState = isset($CurrentUserObj->State)?$CurrentUserObj->State:"";
			$BillingCountry = isset($CurrentUserObj->Country)?$CurrentUserObj->Country:@constant("DEFINE_DEFAULT_COUNTRY");
			$BillingArea = isset($CurrentUserObj->Area)?$CurrentUserObj->Area:"";
			$BillingZip = isset($CurrentUserObj->ZipCode)?$CurrentUserObj->ZipCode:"";
			$BillingPhone = isset($CurrentUserObj->Phone)?$CurrentUserObj->Phone:"";
			$BillingFax = isset($CurrentUserObj->Fax)?$CurrentUserObj->Fax:"";
	
		}
		else 
		{
			$AddressObj->Where ="AddressID='".$BAID."' AND UserID='".$CurrentUserObj->UserID."'";
			$CurrentAddress = $AddressObj->TableSelectOne();
			
			if(isset($CurrentAddress->AddressID) && $CurrentAddress->AddressID !="")
			{
				$BillingFirstName = isset($CurrentAddress->FirstName)?$CurrentAddress->FirstName:"";
				$BillingLastName = isset($CurrentAddress->LastName)?$CurrentAddress->LastName:"";
				$BillingAddress1 = isset($CurrentAddress->Address1)?$CurrentAddress->Address1:"";
				$BillingAddress2 = isset($CurrentAddress->Address2)?$CurrentAddress->Address2:"";
				$BillingCity = isset($CurrentAddress->City)?$CurrentAddress->City:"";
				$BillingState = isset($CurrentAddress->State)?$CurrentAddress->State:"";
				$BillingCountry = isset($CurrentAddress->Country)?$CurrentAddress->Country:@constant("DEFINE_DEFAULT_COUNTRY");
				$BillingArea = isset($CurrentAddress->Area)?$CurrentAddress->Area:"";
				$BillingZip = isset($CurrentAddress->ZipCode)?$CurrentAddress->ZipCode:"";
				$BillingPhone = isset($CurrentAddress->Phone)?$CurrentAddress->Phone:"";
				$BillingFax = isset($CurrentAddress->Fax)?$CurrentAddress->Fax:"";
			}
	
    	
		}
	}
	/*$BAID End*/
	
	/*$SAID Start*/
	if($SAID != 0)
	{
		if($SAID =="-2")
		{
			MyRedirect(MakePageURL("index.php","Page=shop/address_book&Section=AddAddress"));			
			exit;
		}
		
		if($SAID =="-1")
		{
			$ShippingFirstName = isset($CurrentUserObj->FirstName)?$CurrentUserObj->FirstName:"";
			$ShippingLastName = isset($CurrentUserObj->LastName)?$CurrentUserObj->LastName:"";
			$ShippingAddress1 = isset($CurrentUserObj->Address1)?$CurrentUserObj->Address1:"";
			$ShippingAddress2 = isset($CurrentUserObj->Address2)?$CurrentUserObj->Address2:"";
			$ShippingCity = isset($CurrentUserObj->City)?$CurrentUserObj->City:"";
			$ShippingState = isset($CurrentUserObj->State)?$CurrentUserObj->State:"";
			$ShippingCountry = isset($CurrentUserObj->Country)?$CurrentUserObj->Country:@constant("DEFINE_DEFAULT_COUNTRY");
			$ShippingArea = isset($CurrentUserObj->Area)?$CurrentUserObj->Area:"";
			$ShippingZip = isset($CurrentUserObj->ZipCode)?$CurrentUserObj->ZipCode:"";
			$ShippingPhone = isset($CurrentUserObj->Phone)?$CurrentUserObj->Phone:"";
			$ShippingFax = isset($CurrentUserObj->Fax)?$CurrentUserObj->Fax:"";
	
		}
		else 
		{
			$AddressObj->Where ="AddressID='".$SAID."' AND UserID='".$CurrentUserObj->UserID."'";
			$CurrentAddress = $AddressObj->TableSelectOne();
			
			if(isset($CurrentAddress->AddressID) && $CurrentAddress->AddressID !="")
			{
				$ShippingFirstName = isset($CurrentAddress->FirstName)?$CurrentAddress->FirstName:"";
				$ShippingLastName = isset($CurrentAddress->LastName)?$CurrentAddress->LastName:"";
				$ShippingAddress1 = isset($CurrentAddress->Address1)?$CurrentAddress->Address1:"";
				$ShippingAddress2 = isset($CurrentAddress->Address2)?$CurrentAddress->Address2:"";
				$ShippingCity = isset($CurrentAddress->City)?$CurrentAddress->City:"";
				$ShippingState = isset($CurrentAddress->State)?$CurrentAddress->State:"";
				$ShippingCountry = isset($CurrentAddress->Country)?$CurrentAddress->Country:@constant("DEFINE_DEFAULT_COUNTRY");
				$ShippingArea = isset($CurrentAddress->Area)?$CurrentAddress->Area:"";
				$ShippingZip = isset($CurrentAddress->ZipCode)?$CurrentAddress->ZipCode:"";
				$ShippingPhone = isset($CurrentAddress->Phone)?$CurrentAddress->Phone:"";
				$ShippingFax = isset($CurrentAddress->Fax)?$CurrentAddress->Fax:"";
			}
	
    	
		}
	}
	/*$SAID End*/
?>
