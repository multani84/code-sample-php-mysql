

<?php
	//$CurrentUserObj = $CustomObj->CheckUserLogin(MakePageURL("index.php","Page=$Page&ProductID=".@$_REQUEST['ProductID']));
	$DataArray = array();
	
$Section = isset($_REQUEST['Section'])?$_REQUEST['Section']:"";
$Target = isset($_REQUEST['Target'])?$_REQUEST['Target']:"";
$ProductID = isset($_GET['ProductID'])?$_GET['ProductID']:0;

$ProductObj= new DataTable(TABLE_PRODUCT);
$ProductID = (isset($CurrentProduct->ProductID) && $CurrentProduct->ProductID !="")?$CurrentProduct->ProductID:$ProductID;
if($ProductID !="")
{
	$ProductObj->Where ="ProductID='".$ProductObj->MysqlEscapeString($ProductID)."'";
	$CurrentProduct = $ProductObj->TableSelectOne();
	
	$ProdCatObj = new DataTable(TABLE_CATEGORY. " c, ".TABLE_PRODUCT_RELATION. " ptc");
	$ProdCatObj->Where = "c.Active='1' and ptc.Relationtype='category' and ptc.RelationID=c.CategoryID and ptc.ProductID='".$ProdCatObj->MysqlEscapeString($ProductID)."'";
	$CurrentCategory = $ProdCatObj->TableSelectOne(array("c.CategoryID","CategoryName"),"Position ASC");

}

if(isset($_GET['Target']) && $_GET['Target'] =="AddReview")
{
	
	$DataArray = array();
	$DataArray['ProductID']	= $ProductID;
	$DataArray['Description'] 	= isset($_POST['Description'])?$_POST['Description']:"";
	$DataArray['Rating'] 	= isset($_POST['Rating'])?$_POST['Rating']:"";
	$DataArray['CEmail'] 	= isset($_POST['CEmail'])?$_POST['CEmail']:"";
	$DataArray['CName'] 	= isset($_POST['CName'])?$_POST['CName']:"";
	$DataArray['UserID'] 	= isset($IsCurrentUser->UserID)?$IsCurrentUser->UserID:"";
	$DataArray['Active'] 	= "0";
	$DataArray['CreatedDate'] 	= date('YmdHis');
		
	$ErrorArr = array();
		
	if(empty($DataArray['Rating']))
		array_push($ErrorArr,"Please select your rating");
	
	if(empty($DataArray['Description']))
			array_push($ErrorArr,"Please select your review");
		
	
	if(count($ErrorArr) == 0)
		{
		
			$ProductReviewObj = new DataTable(TABLE_PRODUCT_REVIEWS);
			$ProductReviewObj->TableInsert($DataArray);
			
			$Mail_Subject = "A new review has been submitted at ".SITE_NAME.":".$CurrentProduct->ProductName;
			$Mail_ToEmail = isset($_POST['FEmail'])?$_POST['FEmail']:DEFINE_ADMIN_EMAIL;
			$Mail_FromEmail = isset($_POST['CEmail'])?$_POST['CEmail']:DEFINE_ADMIN_EMAIL;
			$Mail_FromName = isset($_POST['Name'])?$_POST['Name']:SITE_NAME;
			$MessageBody ="A new review has been submitted at site. ".chr(13).chr(13)."Please check this product at ".SITE_NAME.":".$CurrentProduct->ProductName.chr(13).SKSEOURL($CurrentProduct->ProductID,"product").chr(13).chr(13)."Thanks!";
			SendEmail($Mail_Subject,$Mail_ToEmail,$Mail_FromEmail,$Mail_FromName,$MessageBody);
			
			@ob_clean();
			
			$ThankMsg = "Thank you for your feedback";
			//MyRedirect(SKSEOURL($ProductID,"product"));			
			//exit;
		}
		else 
		{
			@ob_clean();
			
			$_SESSION['ErrorMessage'] = implode($ErrorArr,"<br>");
			
		}
	
}

?>