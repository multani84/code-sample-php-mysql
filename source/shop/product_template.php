<?php 
switch (@$ProductMode)
{
	case "FullDetail":
	$StArr = GetProductPriceNStock($CurrentProduct->ProductID,1,true);
	?>
   
	<h1 class="product-name"><?php echo ((isset($CurrentProduct->ProductName) && $CurrentProduct->ProductName !="")?"".MyStripSlashes($CurrentProduct->ProductName)."":"");?></h1>

   <form id="FrmMainProduct" name="FrmMainProduct" action="<?=SKSEOURL($ProductID,"shop/product","Target=AddCart")?>" method="POST">
		<input type="hidden" name="ProductID" value="<?=$CurrentProduct->ProductID?>" >
   		<div class="row proDetails">
            <div class="col-sm-4 centerTxt">
				
				<?php ///////   MAIN IMAGE CONTAIMER STARTS HERE /////////////?>		
				<div class="product-img" onclick='window.open($("#zoom1").attr("href"),"my_new_window","toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=no, copyhistory=no, width=400, height=400,")'>
					<a href='<?php echo SKImgDisplay(DIR_FS_SITE_UPLOADS_PRODUCT_ORIGINAL.$CurrentProduct->Image,'1024','','',true,true);?>' class = 'cloud-zoom' id='zoom1' rel="adjustX:50,adjustY:-10, zoomWidth:400,zoomHeight:400,smoothMove:3, showTitle:false" target="_blank">
						<?php SKImgDisplay(DIR_FS_SITE_UPLOADS_PRODUCT_ORIGINAL.$CurrentProduct->Image,'400,400,true',MyStripSlashes($CurrentProduct->ProductName),' class="img-responsive center-block" style="margin:auto; text-align:center;"');?>
					</a>
				   <?php if($CurrentProduct->MNID >  0):
					$ManufacturerObj= new DataTable(TABLE_MANUFACTURERS);
					$ManufacturerObj->Where ="MNID='".$ManufacturerObj->MysqlEscapeString($CurrentProduct->MNID)."'";
					$CurrentManufacturer = $ManufacturerObj->TableSelectOne();
					?>
					<div class="manu-image">
					<?php 
						if(isset($CurrentManufacturer->Image) && $CurrentManufacturer->Image !="" && file_exists(DIR_FS_SITE_UPLOADS_OTHER_ORIGINAL.$CurrentManufacturer->Image))
						{?>
							<a href="<?php echo SKSEOURL($CurrentProduct->MNID,"shop/brand")?>">
								<?php SKImgDisplay(DIR_FS_SITE_UPLOADS_OTHER_ORIGINAL.$CurrentManufacturer->Image,50,'&nbsp;');?>
							</a>
					  <?php }
					  else
					  {
						  ?>
						  <a href="<?php echo SKSEOURL($CurrentProduct->MNID,"shop/brand")?>"><?php echo isset($CurrentManufacturer->MNName)?$CurrentManufacturer->MNName:""?></a>
						 <?php
					  }
					  
					  ?>
					  </div>
					<?php endif;?>
				</div>
				<?php ///////   MAIN IMAGE CONTAIMER End /////////////?>								
											
						<?php ///////   SMALL IMAGES STARTS HERE /////////////?>	
							<?php					 
								$ProductAddtionalImageObj = new DataTable(TABLE_PRODUCT_IMAGES);
								$ProductAddtionalImageObj->Where = "ImageType ='Image' AND Active ='1' AND ProductID='".$ProductAddtionalImageObj->MysqlEscapeString($CurrentProduct->ProductID)."'";
								$ProductAddtionalImageObj->TableSelectAll();
								$TotalRecord = $ProductAddtionalImageObj->GetNumRows();
								
								$ProductObj2 = new DataTable(TABLE_PRODUCT);
								$ProductObj2->Where = "MasterID='".$ProductObj->MysqlEscapeString($CurrentProduct->ProductID)."' AND Image!='' AND Active='1'";
								$ProductObj2->TableSelectAll();
									if($ProductAddtionalImageObj->GetNumRows()>0 or  $ProductObj2->GetNumRows()>0 ){?>	
										<ul class="clearfix" id="flexiselDemo2">
											<?php require_once('addtional_images.php')?> 			
										 </ul>
										 
									<?php }?>
									
						<?php ///////   SMALL IMAGES ends HERE /////////////?>	
								                
				 <?php ///////    IMAGE LEFT AREA ENDS HERE  /////////////?>			
				
			</div>
            <div class="col-sm-8 ">
                <div class="product-left-content product-shop">
                    <div class="centerTxt">	
                       <div class="form-group">
							<div class="rowItem ProMgn">
								
								<label>Item No.</label>
								<span><?php echo isset($CurrentProduct->ModelNo)?"".$CurrentProduct->ModelNo."":""?></span>
							 </div>
								
							<?php echo isset($CurrentProduct->SmallDescription)?MyStripSlashes($CurrentProduct->SmallDescription):""?>

							<div class="rowItem ProMgn">
								<label for="quantity" >Qty</label>
							<input type="text" style="width:40px !important; color:#000000;text-align:center" id="Qty" name="Qty" value="1"  maxlength="3" size="3"></div>
							<?php require_once(dirname(__FILE__)."/product_child.php");?>
							<?php require_once(dirname(__FILE__)."/product_additional.php");?>
							<div id="DisplayPriceDetails">
								<label>Price:&nbsp;</label><?php echo isset($StArr['DisplayPriceDetails'])?$StArr['DisplayPriceDetails']:"";?>
							</div>
							<?php if(d('SHOP_BUY_PROCESS_ENABLED')=="1"):?>	
								<div class="add-to-cart pull-left"><a class="btn btn-success" onclick="return CheckStockQty();" href="javascript:;">Add to cart</a>&nbsp;&nbsp;</div>
							<?php endif;?>
							<?php /*if(d('SHOP_ENQUIRY_PROCESS_ENABLED')=="1" && !isset($_SESSION['EnquiryProducts'][$ProductID])):?>	
								<div class="add-to-cart pull-left"><a class="btn btn-success" href="<?=SKSEOURL($ProductID,"shop/product","Target=AddEnquiry")?>">Add To Enquiry</a></div>
							<?php endif;*/?>
							<!-- Go to www.addthis.com/dashboard to customize your tools -->
							<div class="addthis_inline_share_toolbox"></div>

							

							
						</div>
						<br />
						 <?php
								
						$ProductImageObj = new DataTable(TABLE_PRODUCT_IMAGES);
						$ProductImageObj->Where = "ImageType ='Pdf' AND Active ='1' AND ProductID='".$CurrentProduct->ProductID."'";
						$ProductImageObj->TableSelectAll("","Position ASC");
						if($ProductImageObj->GetNumRows() >0)
						{
						?>
							<div class="PdfGuides" style="clear:both;display:none;">
								<div class="PdfGuideTitle">Download</div>
								<ul>
									<?php
									while($CurrentImage = $ProductImageObj->GetObjectFromRecord())
									{
									?>
									 <li><a href='<?=DIR_WS_SITE_UPLOADS_PRODUCT_ORIGINAL.$CurrentImage->Image?>' target="_blank"><i class="fa fa-file-pdf-o" aria-hidden="true"></i> <?=MyStripSlashes($CurrentImage->ImageName)?></a></li>
									<?php
									}
									?>
								</ul>
							</div>
							<?php
						}
                 		?>
				    </div>
                    <?php /* hide temporary /?>
					<div class="col-sm-12">
                        <ul class="nav nav-tabs">
                          <li class="active"><a data-toggle="tab" href="#Des">Description</a></li>
                          <li><a data-toggle="tab" href="#Delivery">Delivery Information</a></li>
                        </ul>
                        
                        <div class="tab-content">
                          <div id="Des" class="tab-pane fade in active">
                           <?php echo isset($CurrentProduct->LargeDescription)?MyStripSlashes($CurrentProduct->LargeDescription):""?>
                          </div>
                          <div id="Delivery" class="tab-pane fade">
                             <?php $CurrenAddOn = GetAddOn(5,array("Description1"));
									echo isset($CurrenAddOn->Description1)?MyStripSlashes($CurrenAddOn->Description1):"";?>  
                          </div>
                        </div>
                    </div>
					<?php /* hide temporary */?>

					
					
                </div>
            </div>
            <div class="col-sm-12">
            	<div class="large-description">
            		<h4>Directions</h4>
                	<?php echo isset($CurrentProduct->LargeDescription)?MyStripSlashes($CurrentProduct->LargeDescription):""?>
                </div>
            </div>
        </div>
   </form>
   
   <?php
	$RelatedRows = 0;
	$ProductObj = new DataTable(TABLE_PRODUCT. " p, ".TABLE_PRODUCT_RELATION. " ptc");
	$ProductObj->Where = "p.Active='1' and ptc.RelationID=p.ProductID and ptc.ProductID ='$ProductID' and ptc.RelationType='product_related'";
	$ProductObj->TableSelectAll(array("p.*"),"SortOrder","3");
	if($ProductObj->GetNumRows() == 0)
		{ 
			$ProductObj = new DataTable(TABLE_PRODUCT. " p, ".TABLE_PRODUCT_RELATION. " ptc");
			$ProductObj->Where = "p.Active='1' and p.ProductID !='$ProductID' and ptc.ProductID=p.ProductID and ptc.RelationType='category' and ptc.RelationID='".$ProductObj->MysqlEscapeString(@$CurrentProductCategory->CategoryID)."'";
			$ProductObj->TableSelectAll ("","RAND()","4");
		}
		 if($ProductObj->GetNumRows() > 0 && @constant("DEFINE_PRODUCT_RELATED")=="1"){?>
		 <div class="clearfix"></div>
		 <hr/ style="border:1px dashed #434343;">   
        	<h1>I hope you may like this too</h1>
			<br />
            <div class="row products text-center">
				<?php 
				$ProductMode ="Related"; 
				$Sno=1;
				  while($CurrentProduct = $ProductObj->GetObjectFromRecord())
				  { 
					include("product_template.php");
					$Sno++;
				  }?> 
			<?php } ?>		
		</div>
		
	<br/>
	
	<p>&nbsp;</p>
	<p>&nbsp;</p>
	<hr/ style="border:1px dashed #434343;">  
   <script type="text/javascript">
	   	function CheckStockQty()
     	{
     		<?php echo isset($AttJavascipt)?$AttJavascipt:""?>
     		$("#FrmMainProduct").submit();
     		return true;
     	}
    </script>
	<script type="text/javascript">
$(window).load(function() {
    $("#flexiselDemo2").flexisel({
        enableResponsiveBreakpoints: true,
		clone : false,
    });
});
</script>
    <script type="text/javascript">
	function ActivatePrdDesc(liob,divob)
		{
			document.getElementById('Li_PrdDesc').className ="";
			document.getElementById('Li_PrdRev').className ="";
			
			document.getElementById('Div_PrdDesc').style.display ="none";
			document.getElementById('Div_PrdRev').style.display ="none";
			
			document.getElementById(liob).className ="selected";
			document.getElementById(divob).style.display ="block";
			return false;
		}
	</script>
<script type="text/javascript" src="<?=DIR_WS_SITE_INCLUDES_JAVASCRIPT?>jquery/cloud-zoom/cloud-zoom.js"></script>
<link rel="stylesheet" type="text/css" href="<?=DIR_WS_SITE_INCLUDES_JAVASCRIPT?>jquery/cloud-zoom/cloud-zoom.css" />

		<?php 
	break;
	case "Featured":
		$StArr = GetProductPriceNStock($CurrentProduct->ProductID,1);
		?>
		<li>    
			<?php echo SKImgDisplay(DIR_FS_SITE_UPLOADS_PRODUCT_ORIGINAL.$CurrentProduct->Image,"150",MyStripSlashes($CurrentProduct->ProductName));?>        
			<h3><?php echo MyStripSlashes($CurrentProduct->ProductName);?></h3>
			<br />
			<?php echo MyStripSlashes($CurrentProduct->SmallDescription);?>
			<br />
			<a href="<?php echo SKSEOURL($CurrentProduct->ProductID,"shop/product")?>">View Details</a>    
		</li>

	<?php 
	break;

	case "Bulk":
		$StArr = GetProductPriceNStock($CurrentProduct->ProductID,1);
		?>
		<tr class="con<?php echo $CurrentProduct->ProductID?>">
			<td><?php echo $sr++; ?></td>			
			<td><?php echo MyStripSlashes($CurrentProduct->ProductName);?></td>	
			<td><input type="hidden" value="<?php echo $StArr['Price']?>" class="proprice<?php echo $CurrentProduct->ProductID?>" name="proprice[<?php echo $CurrentProduct->ProductID?>]"> <?php echo isset($StArr['DisplayPrice'])?$StArr['DisplayPrice']:"";?></td>
			<td>X</td>		
			<td><input name="rowtotalprice[<?php echo $CurrentProduct->ProductID?>]" class="rowtotalprice rowtotalprice<?php echo $CurrentProduct->ProductID?>" type="hidden" value="0" /><input name="qty[<?php echo $CurrentProduct->ProductID?>]" class="qty<?php echo $CurrentProduct->ProductID?>" type="text" value="0" />=</td>			
			<td> Rs. <span class="Rowtotal<?php echo $CurrentProduct->ProductID?>"></span></td>			
		</tr>
		<script type="text/javascript">
			function changeRowTotal<?php echo $CurrentProduct->ProductID?>(){
				var rowprice = jQuery(".proprice<?php echo $CurrentProduct->ProductID?>").val();
				var rowQty = jQuery(".qty<?php echo $CurrentProduct->ProductID?>").val();
				console.log(rowprice*rowQty);
				var rowTotal = rowprice*rowQty;
				jQuery(".Rowtotal<?php echo $CurrentProduct->ProductID?>").text(rowTotal);
				jQuery(".rowtotalprice<?php echo $CurrentProduct->ProductID?>").val(rowTotal);

				var total_price = 0;
				jQuery(".rowtotalprice").each(function(){
					total_price += parseInt($(this).val());
				});
				jQuery(".TotalOrderPrice").text(total_price);

			}
			changeRowTotal<?php echo $CurrentProduct->ProductID?>();
			jQuery(document).ready(function(){
			   jQuery(".con<?php echo $CurrentProduct->ProductID?> input").change(function(){
			   	changeRowTotal<?php echo $CurrentProduct->ProductID?>();
			   })
			});
		</script>
	<?php 
	break;

	case "Related":
	default:
		$StArr = GetProductPriceNStock($CurrentProduct->ProductID,1);
		?>
         

         <?php /* <div class="record col-sm-3">
			<a href="<?php echo SKSEOURL($CurrentProduct->ProductID,"shop/product")?>" title="<?php echo MyStripSlashes($CurrentProduct->ProductName);?>">
				<div class="image" style="background:url('<?php echo SKImgDisplay(DIR_FS_SITE_UPLOADS_PRODUCT_ORIGINAL.$CurrentProduct->Image,'200','','',true,true);?>') no-repeat center center">
					<?php //SKImgDisplay(DIR_FS_SITE_UPLOADS_PRODUCT_ORIGINAL.$CurrentProduct->Image,"180",MyStripSlashes($CurrentProduct->ProductName)," class='img-responsive center-block'");?>
				<div class="masking" data-toggle="tooltip" title="<?php echo MyStripSlashes($CurrentProduct->ProductName);?>"><a href="<?php echo SKSEOURL($CurrentProduct->ProductID,"shop/product")?>" class="btn btn-primary">View Details</a></div>
			</div></a>
				
			<div class="name">
				<a href="<?php echo SKSEOURL($CurrentProduct->ProductID,"shop/product")?>" data-toggle="tooltip" title="<?php echo MyStripSlashes($CurrentProduct->ProductName);?>">
					<?php echo (strlen($CurrentProduct->ProductName)>=34)?substr($CurrentProduct->ProductName,0,30)."...":MyStripSlashes($CurrentProduct->ProductName);?>
				</a>
			</div>
			<div class="price"><?php echo isset($StArr['DisplayPrice'])?$StArr['DisplayPrice']:"";?></div>
					
			</div> */?>
		<li>
		
    		
				<?php SKImgDisplay(DIR_FS_SITE_UPLOADS_PRODUCT_ORIGINAL.$CurrentProduct->Image,"342",MyStripSlashes($CurrentProduct->ProductName)," class='img-responsive center-block'");?>
			
		
	        <h3><?php echo MyStripSlashes($CurrentProduct->ProductName);?></h3>
			<?php echo isset($CurrentProduct->SmallDescription)?MyStripSlashes($CurrentProduct->SmallDescription):""?><br /><br /><a href="<?php echo SKSEOURL($CurrentProduct->ProductID,"shop/product")?>" title="<?php echo MyStripSlashes($CurrentProduct->ProductName);?>">View Details
	        </a>
    	
    	</li>

	<?php 
	break;
	}
?>

