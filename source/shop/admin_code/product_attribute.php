<?php
$Section = isset($_GET['Section'])?$_GET['Section']:"";
$Target = isset($_GET['Target'])?$_GET['Target']:"";
$AttributeID = isset($_GET['AttributeID'])?$_GET['AttributeID']:0;

$AttributeValueID = isset($_GET['AttributeValueID'])?$_GET['AttributeValueID']:0;


$SeoObj = new DataTable(TABLE_SEO);
$AttributeObj = new DataTable(TABLE_ATTRIBUTE);
$AttributeValueObj = new DataTable(TABLE_ATTRIBUTE_VALUE);
$ProductObj = new DataTable(TABLE_PRODUCT);
$ProductAttributeObj = new DataTable(TABLE_PRODUCT_RELATION);
$CategoryAttributeObj = new DataTable(TABLE_CATEGORY_RELATION);
$DataArray = array();

	if($AttributeID !=0)
	{
		$AttributeObj = new DataTable(TABLE_ATTRIBUTE." c LEFT JOIN ".TABLE_SEO." s on (s.ReferenceID = c.AttributeID AND s.RefrenceType ='attribute')");
		$AttributeObj->Where ="c.AttributeID='".$AttributeObj->MysqlEscapeString($AttributeID)."'";
		$CurrentAttribute = $AttributeObj->TableSelectOne();
	}	
/// Target  start 
switch ($Target)
{
	case "DeleteAttribute":
			$AttributeObj = new DataTable(TABLE_ATTRIBUTE);
			$AttributeObj->Where = "AttributeID='".$AttributeObj->MysqlEscapeString($AttributeID)."'";
			$AttributeObj->TableDelete();	
			@unlink(DIR_FS_SITE_UPLOADS_OTHER_ORIGINAL.$CurrentAttribute->Image);
			
			$SeoObj->Where = "ReferenceID = ".$AttributeID." AND RefrenceType ='attribute'";
			$SeoObj->TableDelete();								
			
			@ob_clean();
			
			$_SESSION['InfoMessage'] ="Attribute deleted successfully.";
			MyRedirect(DIR_WS_SITE_CONTROL."index.php?Page=$Page&Section=$Section");
			exit;
		
	break;	
	case "UpdateAttribute":
		for ($i=1;$i <$_POST['Count'];$i++)
		{
			$AttributeID =$_POST['AttributeID_'.$i];
			
			$DataArray['Active'] = isset($_POST['Active_'.$i])?$_POST['Active_'.$i]:0;
			$DataArray['Position'] = isset($_POST['Position_'.$i])?$_POST['Position_'.$i]:0;			
			
			$AttributeObj->Where = "AttributeID='".$AttributeObj->MysqlEscapeString($AttributeID)."'";
			$AttributeObj->TableUpdate($DataArray);
		}
		ob_clean();
		
		$_SESSION['InfoMessage'] ="Attribute updated successfully.";
		MyRedirect(DIR_WS_SITE_CONTROL."index.php?Page=$Page&Section=$Section");
		exit;
		
	break;
	
	case "AddAttribute":
			$DataArray['AttributeName']= isset($_POST['AttributeName'])?$_POST['AttributeName']:"";
			$DataArray['AttributeType']= isset($_POST['AttributeType'])?$_POST['AttributeType']:"both";
			$DataArray['Description'] = isset($_POST['TmpDescription'])?$_POST['TmpDescription']:"";
			$DataArray['Active'] = isset($_POST['Active'])?$_POST['Active']:0;
			$DataArray['AttributeStatus'] = "product_child";
			
			if(isset($_POST['Template']) && $_POST['Template'] !="")
				$DataArray['Template'] = $_POST['Template'];
			
			/* attribute image added*/
			if(isset($_FILES['Upload']) && $_FILES['Upload']['name'] !="")
			{
				$ArrayType = explode(".",$_FILES['Upload']['name']);
				$Type=$ArrayType[count($ArrayType)-1];
				
				$ImageName = uniqid($DataArray['URLName']."_").".".$Type;				
				$OriginalImage =DIR_FS_SITE_UPLOADS_OTHER_ORIGINAL.$ImageName;
				copy($_FILES['Upload']['tmp_name'],$OriginalImage);
				@chmod($OriginalImage,0777);
				$DataArray['Image'] = $ImageName;
				
			}
			/* attribute image added end*/	
		
		if($AttributeID==0)
		{
			
			$AttributeID = $AttributeObj->GetMax("AttributeID") + 1;
			if($AttributeID <=1)
				$DataArray['AttributeID'] = 101;
			
			$AttributeObj->Where ="1";
			$Position = $AttributeObj->GetMax("Position") + 1;
			$DataArray['Position'] = $Position;
			$DataArray['CreatedDate'] =date('Y-m-d H:i:s');
			$AttributeID = $AttributeObj->TableInsert($DataArray);
			
			@ob_clean();
			
			$_SESSION['InfoMessage'] ="Attribute added successfully.";
		}
		else 
		{
			
			$AttributeObj->TableUpdate($DataArray);
			@ob_clean();
			
			$_SESSION['InfoMessage'] ="Attribute updated successfully.";
		}
		
		$SeoObj->Where = "ReferenceID = ".$AttributeID." AND RefrenceType ='attribute'";
		$CurrentSEO = $SeoObj->TableSelectOne(array("ReferenceID"));
		
		$DataArray = array();
		$DataArray['URLName'] = SkURLCreate((isset($_POST['URLName']) && $_POST['URLName'] !="")?$_POST['URLName']:"collection-".$_POST['AttributeName']);
		$DataArray['RedirectEnabled'] = isset($_POST['RedirectEnabled'])?$_POST['RedirectEnabled']:0;
		$DataArray['RedirectURL'] = isset($_POST['RedirectURL'])?$_POST['RedirectURL']:"";
		
		if(@constant("SEO_META_ACTIVE") =="1")
		{
			$DataArray['MetaTitle'] = isset($_POST['MetaTitle'])?$_POST['MetaTitle']:"";
			$DataArray['MetaKeyword'] = isset($_POST['MetaKeyword'])?$_POST['MetaKeyword']:"";
			$DataArray['MetaDescription'] = isset($_POST['MetaDescription'])?$_POST['MetaDescription']:"";
		}
		
		if(isset($CurrentSEO->ReferenceID) && $CurrentSEO->ReferenceID !="")
		{
			$SeoObj->TableUpdate($DataArray);
		}
		else 
		{
			$DataArray['RefrenceType'] = "attribute";
			$DataArray['ReferenceID'] = $AttributeID;
			$SeoObj->TableInsert($DataArray);
		}
		
		MyRedirect(DIR_WS_SITE_CONTROL."index.php?Page=$Page&AttributeID=$AttributeID");
		exit;
	break;
	
	case "DeleteAttributeValue":
		$AttributeValueObj->Where = "AttributeValueID='".$AttributeValueObj->MysqlEscapeString($AttributeValueID)."'";
		$AttributeValueObj->TableDelete();
		
		$ProductRelationObj = new DataTable(TABLE_PRODUCT_RELATION);
		$ProductRelationObj->Where = "RelationType='attribute_value' AND RelationID='".$AttributeValueObj->MysqlEscapeString($AttributeValueID)."'";
		$ProductRelationObj->TableDelete();

		$CategoryRelationObj = new DataTable(TABLE_CATEGORY_RELATION);
		$CategoryRelationObj->Where = "RelationType='attribute_value' AND RelationID='".$AttributeValueObj->MysqlEscapeString($AttributeValueID)."'";
		$CategoryRelationObj->TableDelete();
			
		@ob_clean();
							
		$_SESSION['InfoMessage'] ="Record updated successfully.";
		MyRedirect(DIR_WS_SITE_CONTROL."index.php?Page=$Page&Section=$Section&AttributeID=$AttributeID");
		exit;
	
	break;
	case "EditAttributeValue":
		$DataArray = array();
		$DataArray['AttributeID'] = $AttributeID;
		$DataArray['AttributeValue'] = isset($_POST['AttributeValue'])?$_POST['AttributeValue']:"";
		$DataArray['Active'] = isset($_POST['Active'])?$_POST['Active']:"0";
		$DataArray['Position'] = (isset($_POST['Position']) && $_POST['Position'] !="")?$_POST['Position']:"0";
		
		$AttributeValueObj->Where = "AttributeValueID='".$AttributeValueObj->MysqlEscapeString($AttributeValueID)."'";
		$AttributeValueObj->TableUpdate($DataArray);
			
		@ob_clean();
							
		$_SESSION['InfoMessage'] ="Record updated successfully.";
		MyRedirect(DIR_WS_SITE_CONTROL."index.php?Page=$Page&Section=$Section&AttributeID=$AttributeID");
		exit;
	
	break;
	case "AddAttributeValue":
	
		$DataArray = array();
		$DataArray['AttributeID'] = $AttributeID;
		$DataArray['AttributeValue'] = isset($_POST['AttributeValue'])?$_POST['AttributeValue']:"0";
		$DataArray['Active'] = isset($_POST['Active'])?$_POST['Active']:"0";
		$DataArray['Position'] = (isset($_POST['Position']) && $_POST['Position'] !="")?$_POST['Position']:"0";
		
		$AttributeValueObj->TableInsert($DataArray);
			
		@ob_clean();
							
		$_SESSION['InfoMessage'] ="Product updated successfully.";
		MyRedirect(DIR_WS_SITE_CONTROL."index.php?Page=$Page&Section=$Section&AttributeID=$AttributeID");
		exit;
	
	break;
	

}
