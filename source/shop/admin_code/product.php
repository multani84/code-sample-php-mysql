<?php
$FTPPath = DIR_FS_SITE_UPLOADS."ftp/images/";
set_time_limit(1800);
//$AttributeObj2 = new Attributes();
$Section = isset($_GET['Section'])?$_GET['Section']:"";
$Pg = isset($_GET['Pg'])?$_GET['Pg']:"1";
$Target = isset($_GET['Target'])?$_GET['Target']:"";
$ParentID = isset($_GET['ParentID'])?$_GET['ParentID']:0;
$ProductID = isset($_GET['ProductID'])?$_GET['ProductID']:0;
$ImageID = isset($_GET['ImageID'])?$_GET['ImageID']:0;
$ReviewID = isset($_GET['ReviewID'])?$_GET['ReviewID']:0;
$InfoID = isset($_GET['InfoID'])?$_GET['InfoID']:0;
$QuestionID = isset($_GET['QuestionID'])?$_GET['QuestionID']:0;
$PriceType = isset($_GET['PriceType'])?$_GET['PriceType']:0;
$BackUpID = isset($_GET['BackUpID'])?$_GET['BackUpID']:"";

$ProductCatgoryArr = array();
$BannerObj = new DataTable(TABLE_BANNERS);
$CategoryObj = new DataTable(TABLE_CATEGORY);
$ProductObj = new DataTable(TABLE_PRODUCT);
$SeoObj = new DataTable(TABLE_SEO);
$ManufacturerObj = new DataTable(TABLE_MANUFACTURERS);
$ProductRelationObj = new DataTable(TABLE_PRODUCT_RELATION);
$ProductAdditionObj = new DataTable(TABLE_PRODUCT_ADDITIONS);
$ProductAdditionObj2 = new DataTable(TABLE_PRODUCT_ADDITIONS);
$ProductCategoryObj = new DataTable(TABLE_PRODUCT_RELATION);
$ProductCategoryObj2 = new DataTable(TABLE_PRODUCT_RELATION);
$ProductImageObj = new DataTable(TABLE_PRODUCT_IMAGES);
$ProductQuestionObj = new DataTable(TABLE_PRODUCT_QUESTIONS);
$ProductReviewObj = new DataTable(TABLE_PRODUCT_REVIEWS);
$ProductInfoObj = new DataTable(TABLE_PRODUCT_INFO);
$ProductPriceObj =new DataTable(TABLE_PRODUCT_PRICES);
$AttributeObj = new DataTable(TABLE_ATTRIBUTE);
$AttributeValueObj = new DataTable(TABLE_ATTRIBUTE_VALUE);
$UserWatchListObj = new DataTable(TABLE_USERS_WATCHLIST);
$UserWishListObj = new DataTable(TABLE_USERS_WISHLIST);
$BackupObj =new DataTable(TABLE_BACKUP);

if($ImageID !=0)
{
		$ProductImageObj->Where = "ImageID='".$ProductImageObj->MysqlEscapeString($ImageID)."'";
		$CurrentImage = $ProductImageObj->TableSelectOne();
}
if($ParentID !=0)
{
	$CategoryObj->Where = "CategoryID='".$CategoryObj->MysqlEscapeString($ParentID)."'";
	$CurrentCategory = $CategoryObj->TableSelectOne();
	$ProductCatgoryArr[] = $ParentID;
}	
if($ProductID !=0)
{
	$TableName = TABLE_PRODUCT." p LEFT JOIN ".TABLE_SEO." s on (s.ReferenceID = p.ProductID AND s.RefrenceType ='shop/product')
								   LEFT JOIN ".TABLE_PRODUCT_PRICES." pr on (pr.ProductID = p.ProductID )
								   LEFT JOIN ".TABLE_USER_TYPE." ut on (ut.UserTypeID = pr.UserTypeID AND IsDefault=1) 		
				";
	$ProductObj = new DataTable($TableName);
	$ProductObj->Where = "p.ProductID='".$ProductObj->MysqlEscapeString($ProductID)."'";
	$CurrentProduct = $ProductObj->TableSelectOne(array("p.*,s.*,pr.Price,pr.SalePrice,pr.SaleActive"),"QtyMin ASC");
	
	$ProductCategoryObj2->Where ="RelationType ='category' AND ProductID='$CurrentProduct->ProductID'";
	$ProductCategoryObj2->TableSelectAll();
	$ProductCatgoryArr = array();
	while ($CategoryTmp = $ProductCategoryObj2->GetObjectFromRecord())
	{
		$ProductCatgoryArr[] = $CategoryTmp->RelationID;
		if($ParentID ==0)
		 $ParentID = $CategoryTmp->RelationID;
	}
}
/// Target  start 
switch ($Target)
{
	case "DeleteProduct":
		SKDeleteProduct($ProductID);	
			@ob_clean();
			$_SESSION['InfoMessage'] ="Product deleted successfully.";
			MyRedirect(DIR_WS_SITE_CONTROL."index.php?Page=$Page&Section=$Section&Pg=$Pg&ParentID=".$ParentID."");
			exit;
	break;	
	case "UpdateAdditional":
		if(count($_POST) >0)
		{
			$ProductAdditionObj->Where = "ProductID='".$ProductAdditionObj->MysqlEscapeString($ProductID)."'";
			$ProductAdditionObj->TableDelete();
			if(isset($_POST['AttributeName']))
			{
				foreach ($_POST['AttributeName'] as $k=>$v)
				{
					foreach ($_POST['AttributeValue'][$k] as $kkk=>$vvv)
					{
						if(isset($_POST['AttributeValue'][$k][$kkk]) && $_POST['AttributeValue'][$k][$kkk] !="")
						{
							$DataArray = array();
							$DataArray['ProductID'] = $ProductID;
							$DataArray['AttributeName'] = isset($_POST['AttributeName'][$k])?$_POST['AttributeName'][$k]:"";
							$DataArray['AttributeValue'] = isset($_POST['AttributeValue'][$k][$kkk])?$_POST['AttributeValue'][$k][$kkk]:"";
							$DataArray['ModelNo'] = (isset($_POST['ModelNo'][$k][$kkk]) && $_POST['ModelNo'][$k][$kkk] !="")?$_POST['ModelNo'][$k][$kkk]:"";
							$DataArray['NormalPrice'] = (isset($_POST['NormalPrice'][$k][$kkk]) && $_POST['NormalPrice'][$k][$kkk] !="")?$_POST['NormalPrice'][$k][$kkk]:0;
							$DataArray['Price'] = (isset($_POST['Price'][$k][$kkk]) && $_POST['Price'][$k][$kkk] !="")?$_POST['Price'][$k][$kkk]:0;
							$DataArray['PriceSet'] = isset($_POST['PriceSet'][$k][$kkk])?$_POST['PriceSet'][$k][$kkk]:"";
							$DataArray['CreatedDate'] = date('Y-m-d H:i:s');
							$ProductAdditionObj->TableInsert($DataArray);
						}
					}
					//echo "<br>---------<br>";
				}
				if(isset($_POST['SaveThis']) && $_POST['SaveThis']=="1" && isset($_POST['SaveText']) && $_POST['SaveText'] != "")
				{
					$DataTableObj= new DataTable(TABLE_BACKUP);
					$DataTableObj->Where = "ReferenceID='-1' AND Page='product_additions' AND ReferenceTitle='".$DataTableObj->MysqlEscapeString($_POST['SaveText'])."'";
					$CurrentBackup = $DataTableObj->TableSelectOne(array("ReferenceID"));
					$DataArray = array();
					if(isset($CurrentBackup->ReferenceID) && $CurrentBackup->ReferenceID != "")
					{
						$DataArray['ReferenceTitle'] = $_POST['SaveText'];
						$DataArray['Dumps'] = base64_encode(serialize($_POST));
						$DataArray['CreatedDate'] = date('YmdHis');
						$DataTableObj->TableUpdate($DataArray);	
					}
					else 
					{
						$DataArray['ReferenceID'] = "-1";
						$DataArray['ReferenceTitle'] = $_POST['SaveText'];
						$DataArray['Page'] = "product_additions";
						$DataArray['Dumps'] = base64_encode(serialize($_POST));
						$DataArray['CreatedDate'] = date('YmdHis');
						$DataTableObj->TableInsert($DataArray);	
					}
				}	
			}
			ob_clean();
			$_SESSION['InfoMessage'] ="Product updated successfully.";
			MyRedirect(DIR_WS_SITE_CONTROL."index.php?Page=$Page&Pg=$Pg&ParentID=$ParentID&Section=$Section&ProductID=$ProductID");
			exit;
		}
	break;
	case "UpdateAttribute":
		$ProductRelationObj->Where ="RelationType='attribute_value' AND ProductID='".$ProductObj->MysqlEscapeString($ProductID)."'";
		$ProductRelationObj->TableDelete();
		if(isset($_POST['AttributeValue']) && $_POST['AttributeValue'] !="" && count($_POST['AttributeValue']) >0)
		{
			foreach ($_POST['AttributeValue'] as $ck=>$cv)
			{
				if($cv !="")
				{
					$DataArray = array();
					$DataArray['ProductID'] = $ProductID;
					$DataArray['RelationID'] = $cv;
					$DataArray['RelationType'] = "attribute_value";
					$DataArray['SortOrder'] = SKMaxGetPrdAttValSortOrder($cv,"attribute_value") + 1;
					$ProductRelationObj->TableInsert($DataArray);
				}
			}
		}
		ob_clean();
		$_SESSION['InfoMessage'] ="Attributes updated successfully.";
		MyRedirect(DIR_WS_SITE_CONTROL."index.php?Page=$Page&Section=$Section&ParentID=$ParentID&ProductID=$ProductID");
		exit;
		break;
	case "DeleteCategoryProduct":
			$ProductCategoryObj->Where = "ProductID='".$ProductCategoryObj->MysqlEscapeString($ProductID)."' and RelationType ='category' AND RelationID='".$ProductCategoryObj->MysqlEscapeString($ParentID)."'";
			$ProductCategoryObj->TableDelete();								
			@ob_clean();
			$_SESSION['InfoMessage'] ="Product removed from this category successfully.";
			MyRedirect(DIR_WS_SITE_CONTROL."index.php?Page=$Page&Section=$Section&Pg=$Pg&ParentID=".$ParentID."");
			exit;
	break;	
	case "DeleteQuestion":
		$ProductQuestionObj->Where = "QuestionID='".$ProductQuestionObj->MysqlEscapeString($QuestionID)."'";
		$ProductQuestionObj->TableDelete();
		@ob_clean();
		$_SESSION['InfoMessage'] ="Product updated successfully.";
		MyRedirect(DIR_WS_SITE_CONTROL."index.php?Page=$Page&Section=$Section&Pg=$Pg&ParentID=$ParentID&ProductID=$ProductID");
		exit;
	break;
	case "EditQuestion":
		$DataArray = array();
		$DataArray['ProductID'] = $ProductID;
		$DataArray['CEmail'] = isset($_POST['CEmail'])?$_POST['CEmail']:"0";
		$DataArray['Question'] = isset($_POST['Question'])?$_POST['Question']:"0";
		$DataArray['Answer'] = isset($_POST['Answer'])?$_POST['Answer']:"0";
		$DataArray['Active'] = isset($_POST['Active'])?$_POST['Active']:"0";
		$DataArray['Position'] = (isset($_POST['Position']) && $_POST['Position'] !="")?$_POST['Position']:"0";
		$ProductQuestionObj->Where = "QuestionID='".$ProductQuestionObj->MysqlEscapeString($QuestionID)."'";
		$ProductQuestionObj->TableUpdate($DataArray);
		@ob_clean();
		session_register("QuestionMessage");					
		$_SESSION['QuestionMessage'] ="Product updated successfully.";
		MyRedirect(DIR_WS_SITE_CONTROL."index.php?Page=$Page&Section=$Section&Pg=$Pg&ParentID=$ParentID&ProductID=$ProductID");
		exit;
	break;
	case "AddQuestion":
		$DataArray = array();
		$DataArray['ProductID'] = $ProductID;
		$DataArray['CEmail'] = isset($_POST['CEmail'])?$_POST['CEmail']:"";
		$DataArray['Question'] = isset($_POST['Question'])?$_POST['Question']:"";
		$DataArray['Answer'] = isset($_POST['Answer'])?$_POST['Answer']:"";
		$DataArray['Active'] = isset($_POST['Active'])?$_POST['Active']:"";
		$DataArray['Position'] = (isset($_POST['Position']) && $_POST['Position'] !="")?$_POST['Position']:"0";
		$ProductQuestionObj->TableInsert($DataArray);
		@ob_clean();
		session_register("QuestionMessage");					
		$_SESSION['QuestionMessage'] ="Product updated successfully.";
		MyRedirect(DIR_WS_SITE_CONTROL."index.php?Page=$Page&Section=$Section&Pg=$Pg&ParentID=$ParentID&ProductID=$ProductID");
		exit;
	break;
	case "DeleteInfo":
		$ProductInfoObj->Where = "InfoID='".$ProductInfoObj->MysqlEscapeString($InfoID)."'";
		$ProductInfoObj->TableDelete();
		@ob_clean();
		$_SESSION['InfoMessage'] ="Product updated successfully.";
		MyRedirect(DIR_WS_SITE_CONTROL."index.php?Page=$Page&Section=$Section&Pg=$Pg&ParentID=$ParentID&ProductID=$ProductID");
		exit;
	break;
	case "EditInfo":
		$DataArray = array();
		$DataArray['ProductID'] = $ProductID;
		$DataArray['TabTitle'] = isset($_POST['TabTitle'])?$_POST['TabTitle']:"0";
		$DataArray['InfoType'] = isset($_POST['InfoType'])?$_POST['InfoType']:"HTML";
		$DataArray['Active'] = isset($_POST['Active'])?$_POST['Active']:"0";
		$DataArray['Position'] = (isset($_POST['Position']) && $_POST['Position'] !="")?$_POST['Position']:"0";
		$DataArray['Description'] = isset($_POST['EditDescription'])?$_POST['EditDescription']:"";
		if($DataArray['InfoType']=="WMV")
			$DataArray['Description'] = isset($_POST['EditDemoFile'])?$_POST['EditDemoFile']:"";
		$ProductInfoObj->Where = "InfoID='".$ProductInfoObj->MysqlEscapeString($InfoID)."'";
		$ProductInfoObj->TableUpdate($DataArray);
		@ob_clean();
		$_SESSION['InfoMessage'] ="Product updated successfully.";
		MyRedirect(DIR_WS_SITE_CONTROL."index.php?Page=$Page&Section=$Section&Pg=$Pg&ParentID=$ParentID&ProductID=$ProductID");
		exit;
	break;
	case "AddInfo":
		$DataArray = array();
		$DataArray['ProductID'] = $ProductID;
		$DataArray['TabTitle'] = isset($_POST['TabTitle'])?$_POST['TabTitle']:"0";
		$DataArray['InfoType'] = isset($_POST['InfoType'])?$_POST['InfoType']:"HTML";
		$DataArray['Active'] = isset($_POST['Active'])?$_POST['Active']:"0";
		$DataArray['Position'] = (isset($_POST['Position']) && $_POST['Position'] !="")?$_POST['Position']:"0";
		$DataArray['Description'] = isset($_POST['AddDescription'])?$_POST['AddDescription']:"";
		if($DataArray['InfoType']=="WMV")
			$DataArray['Description'] = isset($_POST['DemoFile'])?$_POST['DemoFile']:"";
		$ProductInfoObj->TableInsert($DataArray);
		@ob_clean();
		$_SESSION['InfoMessage'] ="Product updated successfully.";
		MyRedirect(DIR_WS_SITE_CONTROL."index.php?Page=$Page&Section=$Section&Pg=$Pg&ParentID=$ParentID&ProductID=$ProductID");
		exit;
	break;
	case "AddRelatedProduct":
		if(isset($_POST["RelationProductID"])):
			$productRelatedArray = explode(",",$_POST["_RelationProductID"]);
			if (count($productRelatedArray) >0)
			{
				foreach ($productRelatedArray as $value) {
					if(trim($value) != "")
					{
						$DataArray = array();
						$DataArray['ProductID'] = $ProductID;
						$DataArray['RelationID'] = $value;
						$DataArray['RelationType'] = "product_related";
						$ProductRelationObj->Where = "ProductID = '$ProductID' AND RelationType='product_related'";
						$DataArray['SortOrder'] = $ProductRelationObj->GetMax("SortOrder") + 1;
						$ProductRelationObj->TableInsert($DataArray);
					}
				}
			}
			@ob_clean();
			$_SESSION['InfoMessage'] ="Related item added successfully.";
		else:
			@ob_clean();
			$_SESSION['ErrorMessage'] ="No product selected.";
		endif;
		MyRedirect(DIR_WS_SITE_CONTROL."index.php?Page=$Page&Section=Related&Pg=$Pg&ParentID=$ParentID&ProductID=$ProductID");
		exit;
	break;
	case "UpdateRelatedProduct":
		for ($i=1;$i <$_POST['Count'];$i++)
		{
			$DataArray = array();
			$DataArray['SortOrder'] = (isset($_POST['SortOrder_'.$i]) && $_POST['SortOrder_'.$i] !="")?$_POST['SortOrder_'.$i]:0;
			$DataArray['Custom'] = (isset($_POST['Custom_'.$i]) && $_POST['Custom_'.$i] !="")?$_POST['Custom_'.$i]:0;
			$ProductRelationObj = new DataTable(TABLE_PRODUCT_RELATION);			
			$ProductRelationObj->Where = "ID='".$_POST['ID_'.$i]."'";
			$ProductRelationObj->TableUpdate($DataArray);
		}
		@ob_clean();
		$_SESSION['InfoMessage'] ="Sort Order updated successfully.";
		MyRedirect(DIR_WS_SITE_CONTROL."index.php?Page=$Page&Section=Related&Pg=$Pg&ParentID=$ParentID&ProductID=$ProductID");
		exit;
		break;		
	case "DeleteRelatedProduct":
		$ProductRelationObj = new DataTable(TABLE_PRODUCT_RELATION);		
		$ProductRelationObj->Where = "ID='".$_GET["ID"]."' AND RelationType='product_related'";
		$ProductRelationObj->TableDelete();
		@ob_clean();
		$_SESSION['InfoMessage'] ="Related item deleted successfully.";
		MyRedirect(DIR_WS_SITE_CONTROL."index.php?Page=$Page&Section=Related&Pg=$Pg&ParentID=$ParentID&ProductID=$ProductID");
		exit;
	break;
	case "AddSimilarProduct":
		if(isset($_POST["RelationProductID"])):
			$productSimilarArray = explode(",",$_POST["_RelationProductID"]);
			if (count($productSimilarArray) >0)
			{
				foreach ($productSimilarArray as $value) {
					if(trim($value) != "")
					{
						$DataArray = array();
						$DataArray['ProductID'] = $ProductID;
						$DataArray['RelationID'] = $value;
						$DataArray['RelationType'] = "product_similar";
						$ProductRelationObj->Where = "ProductID = '$ProductID' AND RelationType='product_similar'";
						$DataArray['SortOrder'] = $ProductRelationObj->GetMax("SortOrder") + 1;
						$ProductRelationObj->TableInsert($DataArray);
					}
				}
			}
			@ob_clean();
			$_SESSION['InfoMessage'] ="Similar item added successfully.";
		else:
			@ob_clean();
			$_SESSION['ErrorMessage'] ="No product selected.";
		endif;
		MyRedirect(DIR_WS_SITE_CONTROL."index.php?Page=$Page&Section=Similar&Pg=$Pg&ParentID=$ParentID&ProductID=$ProductID");
		exit;
	break;
	case "UpdateSimilarProduct":
		for ($i=1;$i <$_POST['Count'];$i++)
		{
			$DataArray = array();
			$DataArray['SortOrder'] = (isset($_POST['SortOrder_'.$i]) && $_POST['SortOrder_'.$i] !="")?$_POST['SortOrder_'.$i]:0;
			$ProductRelationObj = new DataTable(TABLE_PRODUCT_RELATION);			
			$ProductRelationObj->Where = "ID='".$_POST['ID_'.$i]."'";
			$ProductRelationObj->TableUpdate($DataArray);
		}
		@ob_clean();
		$_SESSION['InfoMessage'] ="Sort Order updated successfully.";
		MyRedirect(DIR_WS_SITE_CONTROL."index.php?Page=$Page&Section=Similar&Pg=$Pg&ParentID=$ParentID&ProductID=$ProductID");
		exit;
		break;		
	case "DeleteSimilarProduct":
		$ProductRelationObj = new DataTable(TABLE_PRODUCT_RELATION);		
		$ProductRelationObj->Where = "ID='".(int)$_GET["ID"]."' AND RelationType='product_similar'";
		$ProductRelationObj->TableDelete();
		@ob_clean();
		$_SESSION['InfoMessage'] ="Similar item deleted successfully.";
		MyRedirect(DIR_WS_SITE_CONTROL."index.php?Page=$Page&Section=Similar&Pg=$Pg&ParentID=$ParentID&ProductID=$ProductID");
		exit;
	break;
	case "DeleteReview":
		$ProductReviewObj->Where = "ReviewID='".$ProductReviewObj->MysqlEscapeString($ReviewID)."'";
		$ProductReviewObj->TableDelete();
		@ob_clean();
		$_SESSION['InfoMessage'] ="Product updated successfully.";
		MyRedirect(DIR_WS_SITE_CONTROL."index.php?Page=$Page&Section=$Section&Pg=$Pg&ParentID=$ParentID&ProductID=$ProductID");
		exit;
	break;
	case "EditReview":
		$DataArray = array();
		$DataArray['ProductID'] = $ProductID;
		$DataArray['Active'] = isset($_POST['Active'])?$_POST['Active']:"0";
		$DataArray['Description'] = isset($_POST['Description'])?$_POST['Description']:"0";
		$ProductReviewObj->Where = "ReviewID='".$ProductReviewObj->MysqlEscapeString($ReviewID)."'";
		$ProductReviewObj->TableUpdate($DataArray);
		@ob_clean();
		$_SESSION['InfoMessage'] ="Product updated successfully.";
		MyRedirect(DIR_WS_SITE_CONTROL."index.php?Page=$Page&Section=$Section&Pg=$Pg&ParentID=$ParentID&ProductID=$ProductID");
		exit;
	break;
	case "DeleteImage":
		$ProductImageObj->Where = "ImageID='".$ProductImageObj->MysqlEscapeString($ImageID)."'";
		$ProductImageObj->TableDelete();
		@unlink(DIR_FS_SITE_UPLOADS_PRODUCT_ORIGINAL.$CurrentImage->Image);
		@ob_clean();
		$_SESSION['InfoMessage'] ="Product updated successfully.";
		MyRedirect(DIR_WS_SITE_CONTROL."index.php?Page=$Page&Section=$Section&Pg=$Pg&ParentID=$ParentID&ProductID=$ProductID");
		exit;
	break;
	case "EditImage":
		$DataArray = array();
		$DataArray['ProductID'] = $ProductID;
		$DataArray['Active'] = isset($_POST['Active'])?$_POST['Active']:"0";
		$DataArray['DefaultImage'] = isset($_POST['DefaultImage'])?$_POST['DefaultImage']:"0";
		$DataArray['ImageName'] = (isset($_POST['ImageName']) && $_POST['ImageName'] !="")?$_POST['ImageName']:"";
		$DataArray['Position'] = (isset($_POST['Position']) && $_POST['Position'] !="")?$_POST['Position']:"0";
		$DataArray['ImageType'] = (isset($_POST['ImageType']) && $_POST['ImageType'] !="")?$_POST['ImageType']:"Image";
		if($DataArray['DefaultImage']=="1")
		{
			$ProductImageObj->Where = "ProductID='".$ProductImageObj->MysqlEscapeString($ProductID)."'";
			$ProductImageObj->TableUpdate(array("DefaultImage"=>"0"));
		}
		if(isset($_FILES['Upload']) && $_FILES['Upload']['name'] !="")
		{
				@unlink(DIR_FS_SITE_UPLOADS_PRODUCT_ORIGINAL.$CurrentImage->Image);
				$ArrayType = explode(".",$_FILES['Upload']['name']);
				$Type=$ArrayType[count($ArrayType)-1];
				$ImageName = uniqid("File_".$ProductID."_").".".$Type;				
				$OriginalImage =DIR_FS_SITE_UPLOADS_PRODUCT_ORIGINAL.$ImageName;
				copy($_FILES['Upload']['tmp_name'],$OriginalImage);
				chmod($OriginalImage,0777);
				$DataArray['Image'] = $ImageName;
			}
		$ProductImageObj->Where = "ImageID='".$ProductImageObj->MysqlEscapeString($ImageID)."'";
		$ProductImageObj->TableUpdate($DataArray);
		@ob_clean();
		$_SESSION['InfoMessage'] ="Product updated successfully.";
		MyRedirect(DIR_WS_SITE_CONTROL."index.php?Page=$Page&Section=$Section&Pg=$Pg&ParentID=$ParentID&ProductID=$ProductID");
		exit;
	break;
	case "AddImage":
		$DataArray = array();
		$DataArray['ProductID'] = $ProductID;
		$DataArray['DefaultImage'] = isset($_POST['DefaultImage'])?$_POST['DefaultImage']:"0";
		$DataArray['Active'] = isset($_POST['Active'])?$_POST['Active']:"0";
		$DataArray['Position'] = (isset($_POST['Position']) && $_POST['Position'] !="")?$_POST['Position']:"0";
		$DataArray['ImageName'] = (isset($_POST['ImageName']) && $_POST['ImageName'] !="")?$_POST['ImageName']:"";
		$DataArray['ImageType'] = (isset($_POST['ImageType']) && $_POST['ImageType'] !="")?$_POST['ImageType']:"Image";
		if($DataArray['DefaultImage']=="1")
		{
			$ProductImageObj->Where = "ProductID='".$ProductImageObj->MysqlEscapeString($ProductID)."'";
			$ProductImageObj->TableUpdate(array("DefaultImage"=>"0"));
		}
		if(isset($_FILES['Upload']) && $_FILES['Upload']['name'] !="")
		{
			$ArrayType = explode(".",$_FILES['Upload']['name']);
			$Type=$ArrayType[count($ArrayType)-1];
			$ImageName = uniqid("File_".$ProductID."_").".".$Type;				
			$OriginalImage =DIR_FS_SITE_UPLOADS_PRODUCT_ORIGINAL.$ImageName;
			copy($_FILES['Upload']['tmp_name'],$OriginalImage);
			chmod($OriginalImage,0777);
			$DataArray['Image'] = $ImageName;
		}
		$ProductImageObj->TableInsert($DataArray);
		@ob_clean();
		$_SESSION['InfoMessage'] ="Product updated successfully.";
		MyRedirect(DIR_WS_SITE_CONTROL."index.php?Page=$Page&Section=$Section&Pg=$Pg&ParentID=$ParentID&ProductID=$ProductID");
		exit;
	break;
	case "Moved":
		$MoveProductID = isset($_GET['MoveProductID'])?$_GET['MoveProductID']:"";
		$MoveParentID = isset($_GET['MoveParentID'])?$_GET['MoveParentID']:"";
		if($MoveProductID == $MoveParentID)
		{	
			ob_clean();
			session_register("ErrorMessage");			
			$_SESSION['ErrorMessage'] ="Product does not move.";
			MyRedirect(DIR_WS_SITE_CONTROL."index.php?Page=$Page&Section=$Section&Pg=$Pg&Target=&ParentID=".$ParentID."&ProductID=".$MoveProductID."");
			exit;
		}
		else 
		{
			$DataArray = array();
			$DataArray['RelationID'] = $MoveParentID;
			$ProductCategoryObj->Where = "ProductID='".$ProductCategoryObj->MysqlEscapeString($MoveProductID)."' and RelationType ='category' and RelationID='".$ProductCategoryObj->MysqlEscapeString($ParentID)."'";
			$ProductCategoryObj->TableUpdate($DataArray);;								
			ob_clean();
			$_SESSION['InfoMessage'] ="Product moved successfully.";
			MyRedirect(DIR_WS_SITE_CONTROL."index.php?Page=$Page&Section=$Section&Pg=$Pg&ParentID=".$ParentID."&ProductID=".$MoveProductID."");
			exit;
		}
	break;	
	case "Copied":
		$CopyProductID = isset($_GET['CopyProductID'])?$_GET['CopyProductID']:"";
		$CopyParentID = isset($_GET['CopyParentID'])?$_GET['CopyParentID']:"";
		$DataArray = array();
		$DataArray['ProductID'] = $CopyProductID;
		$DataArray['RelationID'] = $CopyParentID;
		$DataArray['RelationType'] = "category";
		$ProductCategoryObj->TableInsert($DataArray);;								
		@ob_clean();
		$_SESSION['InfoMessage'] ="Product copied successfully.";
		MyRedirect(DIR_WS_SITE_CONTROL."index.php?Page=$Page&Section=$Section&Pg=$Pg&ParentID=".$ParentID."&ProductID=".$CopyProductID."");
		exit;
	break;	
	case "UpdateProduct":
		for ($i=1;$i <$_POST['Count'];$i++)
		{
			$ProductID =$_POST['ProductID_'.$i];
			$DataArray = array();
			$DataArray['Homepage'] = isset($_POST['Homepage_'.$i])?$_POST['Homepage_'.$i]:0;
			$DataArray['Featured'] = isset($_POST['Featured_'.$i])?$_POST['Featured_'.$i]:0;
			$DataArray['BestSeller'] = isset($_POST['BestSeller_'.$i])?$_POST['BestSeller_'.$i]:0;
			$DataArray['Active'] = isset($_POST['Active_'.$i])?$_POST['Active_'.$i]:0;
			$DataArray['Position'] = (isset($_POST['Position_'.$i]) && $_POST['Position_'.$i] !="")?$_POST['Position_'.$i]:0;			
			$ProductObj->Where = "ProductID='".$ProductObj->MysqlEscapeString($ProductID)."'";
			$ProductObj->TableUpdate($DataArray);
			$DataArray = array();
			$DataArray['SortOrder'] = (isset($_POST['SortOrder_'.$i]) && $_POST['SortOrder_'.$i] !="")?$_POST['SortOrder_'.$i]:0;			
			$ProductCategoryObj->Where = "RelationType='category' AND RelationID='".$ParentID."' AND ProductID='".$ProductObj->MysqlEscapeString($ProductID)."'";
			$ProductCategoryObj->TableUpdate($DataArray);
		}
		ob_clean();
		$_SESSION['InfoMessage'] ="Product updated successfully.";
		MyRedirect(DIR_WS_SITE_CONTROL."index.php?Page=$Page&Section=$Section&Pg=$Pg&ParentID=".$ParentID."");
		exit;
		break;
	case "UpdateProductPrices":
		for ($i=1;$i <$_POST['Count'];$i++)
		{
			$ProductID = $_POST['ProductID_'.$i];
			$DataArray = array();
			$DataArray['Price'] = (isset($_POST['Price'.$i]) && $_POST['Price'.$i] !="")?$_POST['Price'.$i]:"0";
			$DataArray['SalePrice'] = (isset($_POST['SalePrice'.$i]) && $_POST['SalePrice'.$i] !="")?$_POST['SalePrice'.$i]:"0";
			$DataArray['SaleActive'] = isset($_POST['SaleActive'.$i])?$_POST['SaleActive'.$i]:0;
			$ProductObj->Where = "ProductID='".$ProductObj->MysqlEscapeString($ProductID)."'";
			$ProductObj->TableUpdate($DataArray);
		}
		if(isset($_POST['ProductAdditionID']) && is_array($_POST['ProductAdditionID']) && count($_POST['ProductAdditionID']) > 0)
		{
			foreach ($_POST['ProductAdditionID'] as $k=>$v)
			{
				$ProductAdditionID = $v;
				$ProductAdditionObj->Where = "ProductAdditionID = '".$v."'";
				$DataArray = array();
				$DataArray['NormalPrice'] = (isset($_POST['Price_AttNormalPrice'][$v]) && $_POST['Price_AttNormalPrice'][$v] !="")?$_POST['Price_AttNormalPrice'][$v]:0;
				$DataArray['Price'] = (isset($_POST['SalePrice_AttPrice'][$v]) && $_POST['SalePrice_AttPrice'][$v] !="")?$_POST['SalePrice_AttPrice'][$v]:0;
				$ProductAdditionObj->TableUpdate($DataArray);
			}
		}
		ob_clean();
		$_SESSION['InfoMessage'] ="Product prices updated successfully.";
		MyRedirect(DIR_WS_SITE_CONTROL."index.php?Page=$Page&Section=$Section&r1=".@$_REQUEST['r1']."&r2=".@$_REQUEST['r2']."&r3=".@$_REQUEST['r3']."&r4=".@$_REQUEST['r4']."&r5=".@$_REQUEST['r5']."&Submit=".@$_REQUEST['Submit']);
		exit;		
		break;
	case "UpdateShippingBulk":
		if(isset($_POST['ChkProductID']) && is_array($_POST['ChkProductID']) && count($_POST['ChkProductID']) >0)
		{
			foreach ($_POST['ChkProductID'] as $k=>$ProductID)	
			{
				/* shipping start*/
				if(isset($_POST['Ship']) &&  is_array($_POST['Ship']) && count($_POST['Ship']) >0)
				{
					$ShippingProductObj->Where = "ProductID='".$ShippingProductObj->MysqlEscapeString($ProductID)."'";
					$ShippingProductObj->TableDelete();
					foreach ($_POST['Ship'] as $ZoneID=>$TypeArray)
					{	
						foreach ($TypeArray as $TypeID=>$ShipPrice)
						{
							if(isset($_POST['ChkShip'][$ZoneID][$TypeID]) && $_POST['ChkShip'][$ZoneID][$TypeID]=="1")
							{
								$DataArray = array();
								$DataArray['ProductID'] = $ProductID;
								$DataArray['ZoneID'] = $ZoneID;
								$DataArray['TypeID'] = $TypeID;
								$DataArray['ShipPrice'] = $ShipPrice;
								$DataArray['Active'] = "1";
								$DataArray['CreatedDate'] = date('Y-m-d H:i:s');
								$ShippingProductObj->TableInsert($DataArray);
							}
						}
					}
				}
				/* shipping end*/
			}
		}
		@ob_clean();
		$_SESSION['InfoMessage'] ="Product(s) shipping updated successfully.";
		MyRedirect(DIR_WS_SITE_CONTROL."index.php?Page=$Page&Section=$Section&r1=".@$_REQUEST['r1']."&r2=".@$_REQUEST['r2']."&r3=".@$_REQUEST['r3']."&r4=".@$_REQUEST['r4']."&r5=".@$_REQUEST['r5']."&Submit=".@$_REQUEST['Submit']);
		exit;
		break;	
	case "UpdateTabBulk":
		if(isset($_POST['ChkProductID']) && is_array($_POST['ChkProductID']) && count($_POST['ChkProductID']) >0)
		{
			$ProductInfoObj = new DataTable(TABLE_PRODUCT_INFO);
			foreach ($_POST['ChkProductID'] as $k=>$ProductID)	
			{
				for ($i=1;$i <$_POST['Count'];$i++)
				{
					$TabTitle = isset($_POST['ChkTab_'.$i])?$_POST['ChkTab_'.$i]:"";
					if($TabTitle != "")
					{
						$DataArray = array();
						$ProductInfoObj->Where = "ProductID ='".$ProductID."' && TabTitle= '".$TabTitle."'";
						$CurrentInfo = $ProductInfoObj->TableSelectOne();
						if(isset($CurrentInfo->ProductID) && $CurrentInfo->ProductID != "")
						{
							$DataArray['Active'] = "1";
							$DataArray['Description'] = isset($_POST['EditDescription_'.$i])?$_POST['EditDescription_'.$i]:"";
							$ProductInfoObj->TableUpdate($DataArray);
						}
						else 
						{
							$DataArray['ProductID'] = $ProductID;
							$DataArray['TabTitle'] = $TabTitle;
							$DataArray['InfoType'] = "HTML";
							$DataArray['Active'] = "1";
							$DataArray['Description'] = isset($_POST['EditDescription_'.$i])?$_POST['EditDescription_'.$i]:"";
							$ProductInfoObj->TableInsert($DataArray);
						}
					}
				}
			}
		}
		@ob_clean();
		$_SESSION['InfoMessage'] ="Product information tabs updated successfully.";
		MyRedirect(DIR_WS_SITE_CONTROL."index.php?Page=$Page&Section=$Section&r1=".@$_REQUEST['r1']."&r2=".@$_REQUEST['r2']."&r3=".@$_REQUEST['r3']."&r4=".@$_REQUEST['r4']."&r5=".@$_REQUEST['r5']."&Submit=".@$_REQUEST['Submit']);
		exit;
		break;	
	case "UpdateStock":
		for ($i=1;$i <$_POST['Count'];$i++)
		{
			$ProductID = $_POST['ProductID_'.$i];
			$DataArray = array();
			$DataArray['Stock'] = (isset($_POST['Stock_'.$i]) && $_POST['Stock_'.$i] !="")?$_POST['Stock_'.$i]:"0";
			$DataArray['MinStockLevel'] = (isset($_POST['MinStockLevel_'.$i]) && $_POST['MinStockLevel_'.$i] !="")?$_POST['MinStockLevel_'.$i]:"0";
			$ProductObj->Where = "ProductID='".$ProductObj->MysqlEscapeString($ProductID)."'";
			$ProductObj->TableUpdate($DataArray);
		}
		ob_clean();
		$_SESSION['InfoMessage'] ="Product stock updated successfully.";
		MyRedirect(DIR_WS_SITE_CONTROL."index.php?Page=$Page&Section=$Section&r1=".$_REQUEST['r1']."&r2=".$_REQUEST['r2']."&r3=".$_REQUEST['r3']."&r4=".$_REQUEST['r4']."&r5=".$_REQUEST['r5']."&Submit=".$_REQUEST['Submit']."&Pg=".$_REQUEST['Pg']);
		exit;		
		break;
	case "AddProduct":
	if(isset($_POST['ProductName']))
	{
		$DataArray = array();
		$DataArray['ProductName'] = isset($_POST['ProductName'])?$_POST['ProductName']:"";
		$DataArray['ProductLine'] = isset($_POST['ProductLine'])?$_POST['ProductLine']:"";
		$DataArray['ModelNo'] = isset($_POST['ModelNo'])?$_POST['ModelNo']:"";
		$DataArray['SmallDescription'] = isset($_POST['SmallDescription'])?$_POST['SmallDescription']:"";
		$DataArray['LargeDescription'] = isset($_POST['LargeDescription'])?$_POST['LargeDescription']:"";
		$DataArray['MNID'] = isset($_POST['MNID'])?$_POST['MNID']:"";
		$DataArray['TaxClass'] = isset($_POST['TaxClass'])?$_POST['TaxClass']:1;
		
		$DataArray['MinStockLevel'] = (isset($_POST['MinStockLevel']) && $_POST['MinStockLevel'] !="")?$_POST['MinStockLevel']:"0";
		$DataArray['Stock'] = (isset($_POST['Stock']) && $_POST['Stock'] !="")?$_POST['Stock']:"0";
		$DataArray['StockType'] = (isset($_POST['StockType']) && $_POST['StockType'] !="")?$_POST['StockType']:"";
		$DataArray['QuickBooksCode'] = (isset($_POST['QuickBooksCode']) && $_POST['QuickBooksCode'] !="")?$_POST['QuickBooksCode']:"";
		$DataArray['ImageTitle'] = (isset($_POST['ImageTitle']) && $_POST['ImageTitle'] !="")?$_POST['ImageTitle']:"";
		$DataArray['GoogleMPN'] = (isset($_POST['GoogleMPN']) && $_POST['GoogleMPN'] !="")?$_POST['GoogleMPN']:"";
		$DataArray['GoogleGtin'] = (isset($_POST['GoogleGtin']) && $_POST['GoogleGtin'] !="")?$_POST['GoogleGtin']:"";
		$DataArray['GoogleBrand'] = (isset($_POST['GoogleBrand']) && $_POST['GoogleBrand'] !="")?$_POST['GoogleBrand']:"";
		$DataArray['Banner1'] = (isset($_POST['Banner1']) && $_POST['Banner1'] !="")?$_POST['Banner1']:"";
		$DataArray['UpperBanner1'] = (isset($_POST['UpperBanner1']) && $_POST['UpperBanner1'] !="")?implode(",",$_POST['UpperBanner1']):"";
		$DataArray['Weight'] = (isset($_POST['Weight']) && $_POST['Weight'] !="")?$_POST['Weight']:"0";
		$DataArray['ProductPoints'] = (isset($_POST['ProductPoints']) && $_POST['ProductPoints'] !="")?$_POST['ProductPoints']:"0";
		$DataArray['OverWeight'] = isset($_POST['OverWeight'])?$_POST['OverWeight']:0;
		$DataArray['OverSize'] = isset($_POST['OverSize'])?$_POST['OverSize']:0;
		$DataArray['Homepage'] = isset($_POST['Homepage'])?$_POST['Homepage']:0;
		$DataArray['BestSeller'] = isset($_POST['BestSeller'])?$_POST['BestSeller']:0;
		$DataArray['Featured'] = isset($_POST['Featured'])?$_POST['Featured']:0;
		$DataArray['Active'] = isset($_POST['Active'])?$_POST['Active']:0;
		$DataArray['PurchaseOnline'] = isset($_POST['PurchaseOnline'])?$_POST['PurchaseOnline']:0;
		if(isset($_POST['Template']) && $_POST['Template'] !="")
				$DataArray['Template'] = $_POST['Template'];
		if(isset($_FILES['Upload']) && $_FILES['Upload']['name'] !="")
			{
				$ArrayType = explode(".",$_FILES['Upload']['name']);
				$Type=$ArrayType[count($ArrayType)-1];
				$ImageName = uniqid("p-".SkURLCreate(substr($DataArray['ProductName'],0,10))."_").".".$Type;				
				$OriginalImage =DIR_FS_SITE_UPLOADS_PRODUCT_ORIGINAL.$ImageName;
				copy($_FILES['Upload']['tmp_name'],$OriginalImage);
				chmod($OriginalImage,0777);
				$DataArray['Image'] = $ImageName;
			}
		if($ProductID==0)
		{
			$DataArray['ProductType'] = (isset($_POST['ProductType']) && $_POST['ProductType'] !="")?$_POST['ProductType']:"Simple";
			if(isset($_POST['ProductAttribute']) && is_array($_POST['ProductAttribute']) && count($_POST['ProductAttribute']) > 0)
				$DataArray['ProductAttribute'] = implode(",",$_POST['ProductAttribute']);
			$DataArray['CreatedDate'] = date('Y-m-d H:i:s');
			$ProductID = $ProductObj->TableInsert($DataArray);		
			@ob_clean();
			$_SESSION['InfoMessage'] ="Product added successfully.";
		}
		else 
		{
			$ProductObj->TableUpdate($DataArray);
			@ob_clean();
			$_SESSION['InfoMessage'] ="Product updated successfully.";
		}
		/* seo entry start*/
		$SeoObj->Where = "ReferenceID = ".$ProductID." AND RefrenceType ='shop/product'";
		$CurrentSEO = $SeoObj->TableSelectOne(array("ReferenceID"));
		$DataArray = array();
		$DataArray['URLName'] = SkURLCreate((isset($_POST['URLName']) && $_POST['URLName'] !="")?$_POST['URLName']:($_POST['ProductName']."-".$_POST['ModelNo']));
		$DataArray['RedirectEnabled'] = isset($_POST['RedirectEnabled'])?$_POST['RedirectEnabled']:0;
		$DataArray['RedirectURL'] = isset($_POST['RedirectURL'])?$_POST['RedirectURL']:"";
		$DataArray['MetaTitle'] = isset($_POST['MetaTitle'])?$_POST['MetaTitle']:"";
		$DataArray['MetaKeyword'] = isset($_POST['MetaKeyword'])?$_POST['MetaKeyword']:"";
		$DataArray['MetaDescription'] = isset($_POST['MetaDescription'])?$_POST['MetaDescription']:"";

		if(isset($CurrentSEO->ReferenceID) && $CurrentSEO->ReferenceID !="")
		{
			$SeoObj->TableUpdate($DataArray);
		}
		else 
		{
			$DataArray['RefrenceType'] = "shop/product";
			$DataArray['ReferenceID'] = $ProductID;
			$SeoObj->TableInsert($DataArray);
		}
		/* seo entry end*/
		
		/* ProductPrice entry start*/
		$ProductPriceObj->Where = "ProductID = ".$ProductID." AND UserTypeID ='".(int)(isset($DefaultUserType->UserTypeID)?$DefaultUserType->UserTypeID:"1")."' AND QtyMin='0'";
		$CurrentPrice = $ProductPriceObj->TableSelectOne(array("ProductPriceID"));
		$DataArray = array();
		$DataArray['Price'] = (isset($_POST['Price']) && $_POST['Price'] !="")?$_POST['Price']:"0";
		$DataArray['SalePrice'] = (isset($_POST['SalePrice']) && $_POST['SalePrice'] !="")?$_POST['SalePrice']:"0";
		$DataArray['SaleActive'] = isset($_POST['SaleActive'])?$_POST['SaleActive']:0;
		if(isset($CurrentPrice->ProductPriceID) && $CurrentPrice->ProductPriceID !="")
		{
			$ProductPriceObj->TableUpdate($DataArray);
		}
		else 
		{
			$DataArray['QtyMin'] = "0";
			$DataArray['QtyMax'] = DEFINE_PRODUCT_QTY_MAX;
			$DataArray['PriceType'] = 'Unit';
			$DataArray['UserTypeID'] = isset($DefaultUserType->UserTypeID)?$DefaultUserType->UserTypeID:"1";
			$DataArray['ProductID'] = $ProductID;
			$ProductPriceObj->TableInsert($DataArray);
		}
		/* ProductPriceObj entry end*/
		
		if(isset($_POST['CategoryArr']) && $_POST['CategoryArr'] !="" && count($_POST['CategoryArr']) >0)
		{			
			$ProductCategoryObj->Where = "RelationType='category' and ProductID='".$ProductCategoryObj->MysqlEscapeString($ProductID)."'";
			if(implode(",", $_POST['CategoryArr']) != "")
				 $ProductCategoryObj->Where .= "and RelationID NOT IN (".implode(",",$_POST['CategoryArr']).")";
			$ProductCategoryObj->TableDelete();	
			foreach ($_POST['CategoryArr'] as $ck=>$cv)
			{
				if($cv !="")
				{	
					if($ParentID == 0)
						$ParentID =$cv;
					$ProductCategoryObj->Where = "RelationType='category' and ProductID='".$ProductCategoryObj->MysqlEscapeString($ProductID)."' and RelationID=".$cv;
					if(!is_object($ProductCategoryObj->TableSelectOne(array("id")))){
						$DataArray = array();
						$DataArray['ProductID'] = $ProductID;
						$DataArray['RelationID'] = $cv;
						$DataArray['RelationType'] = "category";
						$DataArray['SortOrder'] = SKMaxGetProductSortOrder($cv,"category") + 1;
						$ProductCategoryObj->TableInsert($DataArray);
					}
				}
			}
		}
		if(@$_POST['ProductType']=="Bundle" && $Section=="AddProduct")
			MyRedirect(DIR_WS_SITE_CONTROL."index.php?Page=$Page&Pg=$Pg&Section=ChildProduct&ParentID=$ParentID&ProductID=$ProductID");
	else 
			MyRedirect(DIR_WS_SITE_CONTROL."index.php?Page=$Page&Pg=$Pg&ParentID=$ParentID&ProductID=$ProductID");
		exit;
	}	
	break;
	case "SampleCSVFile":
		download_file(DIR_FS_SITE_SECURE_DOWNLOAD_PRODUCT."csv_files/product_sample.csv");
		break;
	case "UploadCSVFile":
	if(isset($_FILES['UploadCSV']) && $_FILES['UploadCSV']['name'] !="")
	{
		$ArrayType = explode(".",$_FILES['UploadCSV']['name']);
		$Type=$ArrayType[count($ArrayType)-1];
		if(in_array(trim(ucwords(strtolower($Type))),array("Csv")))
		{
			$UploadName = "Product_".date('Y-m-d_H-i-s').".csv";				
			if($UploadName != "" && file_exists(DIR_FS_SITE_SECURE_DOWNLOAD_PRODUCT."csv_files/".$UploadName))
			{
				@ob_clean();
				session_register("ErrorMessage");
				$_SESSION['ErrorMessage'] = "File name already exist in the csv directory.";
			}
			else 
			{
				copy($_FILES['UploadCSV']['tmp_name'],DIR_FS_SITE_SECURE_DOWNLOAD_PRODUCT."csv_files/".$UploadName);
				MyRedirect(DIR_WS_SITE_CONTROL."index.php?Page=$Page&Section=$Section&Pg=$Pg&ParentID=$ParentID&ProductID=$ProductID&CSVFile=".$UploadName);
				exit;
			}
		}
		else
		 {
		 	@ob_clean();
			session_register("ErrorMessage");
			$_SESSION['ErrorMessage'] = INVALID_FILE_FORMAT;
		 }		
	}
		break;
	case "AddCSVProduct":
		$CSVFile = isset($_POST['CSVFile'])?$_POST['CSVFile']:"";
		if(isset($CSVFile) && $CSVFile !="" && is_file($CSVFile))
		{
			$row =1;
			if (($handle = fopen($CSVFile, "r")) !== FALSE) {
		    while (($data = fgetcsv($handle)) !== FALSE) {
		    	    $num = count($data);
		    	    if($row ==1)
			        {
			        	foreach ($data as $key => $value){
							$prop = "CSV_".strtoupper(str_replace(array("£","%","00","."," "),array("","","","","_"), trim($value)));
							if(!defined($prop))	define($prop,$key);
							else define($prop."1",$key); 
						}
			        }
		    	    if($row !=1)
			        {
			        	/*CSV product start*/
			        	$ProductID= 0;
			        	$DataArray = array();
						$DataArray['ProductName'] = $data[CSV_PRODUCT_NAME];
						$DataArray['ModelNo'] = $data[CSV_MODEL_NO];
						$DataArray['SmallDescription'] = $data[CSV_SMALL_DESCRIPTION];
						$DataArray['LargeDescription'] = $data[CSV_LARGE_DESCRIPTION];
						//$DataArray['MNID'] = $data[CSV_MANUFACTURER];
						$DataArray['Price'] = $data[CSV_PRICE];
						$DataArray['SalePrice'] = $data[CSV_SALE_PRICE];
						//$DataArray['SaleActive'] = $DataArray['SalePrice'] > 0 ?"1":"0";
						$DataArray['SaleActive'] = $data[CSV_SALE_PRICE_ENABLED];
						$DataArray['MinStockLevel'] = $data[CSV_MIN_STOCK_LEVEL];
						$DataArray['Stock'] = $data[CSV_STOCK];
						$DataArray['QuickBooksCode'] = $data[CSV_QUICK_BOOKS_CODE];
						$DataArray['GoogleMPN'] = $data[CSV_GOOGLE_MPN];
						$DataArray['GoogleGtin'] = $data[CSV_GOOGLE_GTIN];
						$DataArray['GoogleBrand'] = $data[CSV_GOOGLE_BRAND];
						$DataArray['Weight'] = $data[CSV_WEIGHT];
						$DataArray['Active'] = $data[CSV_PRODUCT_ENABLE];
						$DataArray['ImageTitle'] = $data[CSV_IMAGETITLE];
						if(isset($data[CSV_IMAGE]) && $data[CSV_IMAGE] != "" && file_exists($FTPPath.$data[CSV_IMAGE]))
						{
							$ArrayType = explode(".",$data[CSV_IMAGE]);
							$Type=$ArrayType[count($ArrayType)-1];
							$ImageName = uniqid("p-".SkURLCreate(substr($DataArray['ProductName'],0,10))."_").".".$Type;				
							$OriginalImage =DIR_FS_SITE_UPLOADS_PRODUCT_ORIGINAL.$ImageName;
							if(copy($FTPPath.$data[CSV_IMAGE],$OriginalImage))
								@unlink($FTPPath.$data[CSV_IMAGE]);
							chmod($OriginalImage,0777);
							$DataArray['Image'] = $ImageName;
						}
						if($DataArray['ModelNo'] != "")
						{
							$ProductObj->Where ="ModelNo='".$DataArray['ModelNo']."'";
							$Obj = $ProductObj->TableSelectOne(array("ProductID"));
							if(isset($Obj->ProductID) && $Obj->ProductID != "")
								$ProductID = $Obj->ProductID;
						}
						if($ProductID==0)
						{
							$DataArray['CreatedDate'] = date('Y-m-d H:i:s');
							$ProductID = $ProductObj->TableInsert($DataArray);		
						}
						else 
						{
							$ProductObj->Where ="ProductID='".$ProductID."'";
							$ProductObj->TableUpdate($DataArray);
						}
						/* seo entry start*/
						$SeoObj->Where = "ReferenceID = ".$ProductID." AND RefrenceType ='shop/product'";
						$CurrentSEO = $SeoObj->TableSelectOne(array("ReferenceID"));
						$DataArray = array();
						if(@constant("SEO_META_ACTIVE") =="1")
						{
							$DataArray['MetaTitle'] = $data[CSV_META_TITLE];
							$DataArray['MetaKeyword'] = $data[CSV_META_KEYWORD];
							$DataArray['MetaDescription'] = $data[CSV_META_DESCRIPTION];
						}
						if(isset($CurrentSEO->ReferenceID) && $CurrentSEO->ReferenceID !="")
						{
								$SeoObj->TableUpdate($DataArray);
						}
						else 
						{
								$DataArray['URLName'] = SkURLCreate($data[CSV_PRODUCT_NAME]."-".$data[CSV_MODEL_NO]);
							$DataArray['RefrenceType'] = "shop/product";
							$DataArray['ReferenceID'] = $ProductID;
							$SeoObj->TableInsert($DataArray);
						}
			        	/*CSV product end*/
			        }
		         $row++;
		        }
			    fclose($handle);
			}
			@ob_clean();
			$_SESSION['InfoMessage'] ="CSV imported successfully.";
			MyRedirect(DIR_WS_SITE_CONTROL."index.php?Page=product");
		exit;
		}
		break;
//	case "AddCSVProduct":
//		if (($handle = fopen(DIR_FS_SITE_UPLOADS_DOWNLOAD."tmp/bath screens.csv", "r")) !== FALSE){
//			$row=1;
//			while (($data = fgetcsv($handle)) !== FALSE) {
//				if($row == 1){/*Column Headers*/
//					foreach ($data as $key => $value){
//						$prop = strtoupper(str_replace(array("£","%","00","."," "),array("","","","","_"), trim($value)));
//						if(!defined($prop))	define($prop,$key);
//						else define($prop."1",$key); 
//					}
//				} else if($row == 2){
//					$ManufacturerObj->Where ="MNName='".$ManufacturerObj->MysqlEscapeString($data[BRAND])."'";
//	        		$CurrentManufacturer = $ManufacturerObj->TableSelectOne(array("MNID"));
//					if(isset($CurrentManufacturer->MNID) && $CurrentManufacturer->MNID !="")
//		        		$DataArray['MNID'] = $CurrentManufacturer->MNID;
//		        	else 
//		        	{
//		        		$SubDataArray = array();
//		        		$SubDataArray['MNName'] = $data[BRAND];
//		        		$SubDataArray['CreatedDate'] = date('Y-m-d H:i:s');
//						$DataArray['MNID'] = $ManufacturerObj->TableInsert($SubDataArray);
//						
//						$SubDataArray = array();
//		        		$SubDataArray['URLName'] = str_replace(" ","-",SkURLCreate("brand-".$data[BRAND]));
//						$SubDataArray['RefrenceType'] = "manufacturer";
//						$SubDataArray['ReferenceID'] = $DataArray['MNID'];						
//						$SeoObj->TableInsert($SubDataArray);
//		        	}
//	        		
//		        	$ProductID = 0;
//		        	
//		        	
//		        	foreach (split("<br />", $data[CATEGORY]) as $value) {
//		        		$arr=split("->", $value);
//		        		if(count($arr) == 1)
//		        			$catID = GetCategoryID($value,0);
//		        		else{ 
//		        			$idx=0;
//		        			$catIDArr = array();
//		        			foreach ($arr as $cat) {
//		        				$catIDArr[$idx] = GetCategoryID($cat, ($idx == 0 ? 0 : $catIDArr[$idx-1]));
//		        				$idx+=1;
//		        			}		        			
//		        			$catID = $catIDArr[count($catIDArr)-1];
//		        		}
//	        			
//		        		if($ProductID > 0){
//			        		$ProductRelationObj->Where = "RelationID='".$catID."' AND ProductID='".$ProductID."' AND RelationType ='category'";
//	    					$CurrentRelation = $ProductRelationObj->TableSelectOne(array("id"));
//	    					if(!is_object($CurrentRelation)){
//	    						$SubDataArray = array();
//	    						$SubDataArray['ProductID'] = $ProductID;
//	    						$SubDataArray['RelationID'] = $catID;
//	    						$SubDataArray['RelationType'] = "category";	
//	    						$ProductRelationObj->Where = "RelationID='".$catID."'";
//	    						$SubDataArray['SortOrder'] = $ProductRelationObj->GetMax("SortOrder") + 1;
//	    						echo $ProductRelationObj->TableInsert($SubDataArray);					
//	    					}
//		        		}
//		        	}		        	
//				}
//				$row+=1;
//			}
//			fclose($handle);
//		}
//		exit;
//	break;
}
//function GetCategoryID($catName,$ParentID){
//	global $CategoryObj, $SeoObj;
//	$CatID=0;
//	$CategoryObj->Where ="CategoryName='".$CategoryObj->MysqlEscapeString($catName)."' AND ParentID='".$ParentID."'";
//    $CurrentCategory = $CategoryObj->TableSelectOne(array("CategoryID"));
//
//    $CatDataArray = array();
//    if(isset($CurrentCategory->CategoryID) && $CurrentCategory->CategoryID !="")
//		 $CatID = $CurrentCategory->CategoryID;
//	else 
//	{
//		$SubDataArray = array();
//		$SubDataArray['CategoryName'] = $catName;
//		$SubDataArray['ParentID'] = $ParentID;
//		$SubDataArray['Active'] = "1";
//		$SubDataArray['Position'] = $CategoryObj->GetMax("Position") + 1;
//		$SubDataArray['CreatedDate'] = date('Y-m-d H:i:s');
//		$CatID = $CategoryObj->TableInsert($SubDataArray);
//						
//		$SubDataArray = array();
//		$SubDataArray['URLName'] = str_replace(" ","-",SkURLCreate("category-".$catName."-".$CatID));
//		$SubDataArray['RefrenceType'] = "category";
//		$SubDataArray['ReferenceID'] = $CatID;						
//		$SeoObj->TableInsert($SubDataArray);
//	}
//	return $CatID;
//}
//// target end
?>