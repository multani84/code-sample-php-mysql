<?php 
set_time_limit(1800);
$Section = isset($_GET['Section'])?$_GET['Section']:"";
$Target = isset($_GET['Target'])?$_GET['Target']:"";
$MasterID = isset($_GET['MasterID'])?$_GET['MasterID']:0;
$ProductID = isset($_GET['ProductID'])?$_GET['ProductID']:0;


$ProductObj = new DataTable(TABLE_PRODUCT);
$ProductRelationObj = new DataTable(TABLE_PRODUCT_RELATION);
$ProductPriceObj = new DataTable(TABLE_PRODUCT_PRICES);
$AttributeObj = new DataTable(TABLE_ATTRIBUTE);
$AttributeValueObj = new DataTable(TABLE_ATTRIBUTE_VALUE);

if($MasterID > 0)
{
	$ProductObj2 = new DataTable(TABLE_PRODUCT);
	$ProductObj2->Where = "ProductID='".$ProductObj2->MysqlEscapeString($MasterID)."'";
	$CurrentMaster = $ProductObj2->TableSelectOne();
	if(isset($CurrentMaster->ProductAttribute) && $CurrentMaster->ProductAttribute != "")
	{
		$AttributeObj = new DataTable(TABLE_ATTRIBUTE);
		$AttributeObj->Where ="AttributeID in (".$CurrentMaster->ProductAttribute.")";
		$AttributeObj->TableSelectAll();
		$AttributeArray = array();
		if($AttributeObj->GetNumRows() > 0)
		{
			while($CurrentAttribute = $AttributeObj->GetObjectFromRecord())
			{
				$AttributeArray[$CurrentAttribute->AttributeID] = $CurrentAttribute->AttributeName; 
			}
		}
		else 
		{
			echo "No any attribute selected";
			exit;
		}
		
		$ProductObj = new DataTable(TABLE_PRODUCT);
		$ProductObj->Where = "ProductID='".$ProductObj->MysqlEscapeString($ProductID)."'";
		$CurrentProduct = $ProductObj->TableSelectOne();
		
		$TableName = TABLE_PRODUCT." p LEFT JOIN ".TABLE_PRODUCT_PRICES." pr on (pr.ProductID = p.ProductID )
								   LEFT JOIN ".TABLE_USER_TYPE." ut on (ut.UserTypeID = pr.UserTypeID AND IsDefault=1) 		
				";
	$ProductObj = new DataTable($TableName);
	$ProductObj->Where = "p.ProductID='".$ProductObj->MysqlEscapeString($ProductID)."'";
	$CurrentProduct = $ProductObj->TableSelectOne(array("p.*,pr.Price,pr.SalePrice,pr.SaleActive"),"QtyMin ASC");
	
	
		
		if(isset($CurrentProduct->ProductID))
		{
			$ProductRelationObj->TableName = TABLE_PRODUCT_RELATION." pr, ".TABLE_ATTRIBUTE_VALUE." av";
			$ProductRelationObj->Where = "pr.ProductID='".$CurrentProduct->ProductID."' AND pr.RelationID = av.AttributeValueID  AND pr.RelationType = 'attribute_value'";
			$ProductRelationObj->TableSelectAll();
			$AttributeValueArray = array();
			while ($CurrentRelation = $ProductRelationObj->GetObjectFromRecord())
			{
				$AttributeValueArray[$CurrentRelation->AttributeID]['id'] = $CurrentRelation->AttributeValueID;
				$AttributeValueArray[$CurrentRelation->AttributeID]['text'] = $CurrentRelation->AttributeValue;
			}
		}

						
		
	}
	else 
	{
		echo "No any attribute selected";
		exit;
	}
}
else 
{
	echo "No product found.";
	exit;
}


/// Target  start 

switch ($Target)
{

	case "DeleteChildProduct":
	
		SKDeleteProduct($ProductID);

		MyRedirect(DIR_WS_SITE_CONTROL."index.php?Page=$Page&Section=Refresh&Popup=true&MasterID=$MasterID");
		exit;

	break;	
	case "ReplicateProduct":
		
		$ProductObj = new DataTable(TABLE_PRODUCT);
		if(isset($CurrentProduct->ProductID) && $CurrentProduct->ProductID != "")
		{
			$DataArray = array();
			$DataArray['ProductName'] = $CurrentProduct->ProductName;
			$DataArray['ModelNo'] = $CurrentProduct->ModelNo;
			$DataArray['Active'] = $CurrentProduct->Active;
			
			$DataArray['TaxClass'] = $CurrentProduct->TaxClass;
			$DataArray['WithVat'] = $CurrentProduct->WithVat;
			$DataArray['MinStockLevel'] = $CurrentProduct->MinStockLevel;
			$DataArray['Stock'] = $CurrentProduct->Stock;
			$DataArray['StockType'] = $CurrentProduct->StockType;
			$DataArray['ImageTitle'] = $CurrentProduct->ImageTitle;
			
			$CopyImage=DIR_FS_SITE_UPLOADS_PRODUCT_ORIGINAL.$CurrentProduct->Image;
				
			if(file_exists($CopyImage) && $CurrentProduct->Image != "")
			{
				$ArrayType = explode(".",$CurrentProduct->Image);
				$Type=$ArrayType[count($ArrayType)-1];
				$ImageName = uniqid("p-".SkURLCreate(substr($DataArray['ProductName'],0,10))."_").".".$Type;				
				$OriginalImage =DIR_FS_SITE_UPLOADS_PRODUCT_ORIGINAL.$ImageName;
				
				copy($CopyImage,$OriginalImage);
				chmod($OriginalImage,0777);
				$DataArray['Image'] = $ImageName;	
			}
			
			$DataArray['MasterID'] = $MasterID;
			$DataArray['ProductType'] = "Child";
			$DataArray['CreatedDate'] = date('Y-m-d H:i:s');
			$ProductID = $ProductObj->TableInsert($DataArray);
			
			
			/* ProductPrice entry start*/
			$DataArray = array();
			$DataArray['Price'] = $CurrentProduct->Price;
			$DataArray['SalePrice'] = $CurrentProduct->SalePrice;
			$DataArray['SaleActive'] = $CurrentProduct->SaleActive;
			$DataArray['QtyMin'] = "0";
			$DataArray['QtyMax'] = DEFINE_PRODUCT_QTY_MAX;
			$DataArray['PriceType'] = 'Unit';
			$DataArray['UserTypeID'] = isset($DefaultUserType->UserTypeID)?$DefaultUserType->UserTypeID:"1";
			$DataArray['ProductID'] = $ProductID;
			$ProductPriceObj->TableInsert($DataArray);
		/* ProductPriceObj entry end*/
		
			
			$ProductRelationObj2 = new DataTable(TABLE_PRODUCT_RELATION);
			$ProductRelationObj = new DataTable(TABLE_PRODUCT_RELATION);
			$ProductRelationObj->Where ="RelationType='attribute_value' AND ProductID='".$ProductObj->MysqlEscapeString($CurrentProduct->ProductID)."'";
			//$ProductRelationObj->DisplayQuery = true;
			$ProductRelationObj->TableSelectAll();
			while ($CurrentAttributeValue = $ProductRelationObj->GetObjectFromRecord())
			{
					$DataArray = array();
					$DataArray['ProductID'] = $ProductID;
					$DataArray['RelationID'] = $CurrentAttributeValue->RelationID;
					$DataArray['RelationType'] = $CurrentAttributeValue->RelationType;
					$DataArray['SortOrder'] = $CurrentAttributeValue->SortOrder;
					$ProductRelationObj2->TableInsert($DataArray);
			}
		}
		
		MyRedirect(DIR_WS_SITE_CONTROL."index.php?Page=$Page&Section=Refresh&Popup=true&MasterID=$MasterID");
		exit;

	break;	
	case "AddChildProduct":
	$ProductObj = new DataTable(TABLE_PRODUCT);
	if(count($_POST) > 0)
	{
		$ProductChildName = "";
		if(isset($_POST['AttributeValue']) && is_array($_POST['AttributeValue']) && count($_POST['AttributeValue']))
		{
			foreach ($_POST['AttributeValue'] as $k=>$v)
			{
				$AttributeValueObj->Where = "AttributeValueID='".$AttributeValueObj->MysqlEscapeString($v)."'";
				$CurrentAttributeValue = $AttributeValueObj->TableSelectOne();
				$ProductChildName .= (isset($AttributeArray[$CurrentAttributeValue->AttributeID])?$AttributeArray[$CurrentAttributeValue->AttributeID]:'').":::".(isset($CurrentAttributeValue->AttributeValue)?$CurrentAttributeValue->AttributeValue:'')."&&&";
				
			}
		}
		
		
		
		$DataArray = array();
		$DataArray['ProductName'] = isset($_POST['ProductName'])?$_POST['ProductName']:$ProductChildName;
		$DataArray['ModelNo'] = isset($_POST['ModelNo'])?$_POST['ModelNo']:"";
		$DataArray['Active'] = isset($_POST['Active'])?$_POST['Active']:0;
		
		/*
		$DataArray['Price'] = (isset($_POST['Price']) && $_POST['Price'] !="")?$_POST['Price']:"0";
		$DataArray['SalePrice'] = (isset($_POST['SalePrice']) && $_POST['SalePrice'] !="")?$_POST['SalePrice']:"0";
		$DataArray['SaleActive'] = isset($_POST['SaleActive'])?$_POST['SaleActive']:0;
		*/
		
		$DataArray['WithVat'] = isset($_POST['WithVat'])?$_POST['WithVat']:0;
		$DataArray['TaxClass'] = isset($_POST['TaxClass'])?$_POST['TaxClass']:1;
		$DataArray['MinStockLevel'] = (isset($_POST['MinStockLevel']) && $_POST['MinStockLevel'] !="")?$_POST['MinStockLevel']:"0";
		$DataArray['Stock'] = (isset($_POST['Stock']) && $_POST['Stock'] !="")?$_POST['Stock']:"0";
		$DataArray['StockType'] = (isset($_POST['StockType']) && $_POST['StockType'] !="")?$_POST['StockType']:"";
		$DataArray['ImageTitle'] = (isset($_POST['ImageTitle']) && $_POST['ImageTitle'] !="")?$_POST['ImageTitle']:"";
		
		if(isset($_FILES['Upload']) && $_FILES['Upload']['name'] !="")
			{
				$ArrayType = explode(".",$_FILES['Upload']['name']);
				$Type=$ArrayType[count($ArrayType)-1];
				
				$ImageName = uniqid("p-".SkURLCreate(substr($DataArray['ProductName'],0,10))."_").".".$Type;				
				$OriginalImage =DIR_FS_SITE_UPLOADS_PRODUCT_ORIGINAL.$ImageName;
				copy($_FILES['Upload']['tmp_name'],$OriginalImage);
				chmod($OriginalImage,0777);
				$DataArray['Image'] = $ImageName;
				
				
			}
		
		if($ProductID==0)
		{
	
			$DataArray['MasterID'] = $MasterID;
			$DataArray['ProductType'] = "Child";
			$DataArray['CreatedDate'] = date('Y-m-d H:i:s');
			$ProductID = $ProductObj->TableInsert($DataArray);		
		}
		else 
		{
			$ProductObj->Where = "ProductID='".$ProductObj->MysqlEscapeString($ProductID)."'";
			$ProductObj->TableUpdate($DataArray);
		}
		
		/* ProductPrice entry start*/
		$ProductPriceObj->Where = "ProductID = ".$ProductID." AND UserTypeID ='".(int)(isset($DefaultUserType->UserTypeID)?$DefaultUserType->UserTypeID:"1")."' AND QtyMin='0'";
		$CurrentPrice = $ProductPriceObj->TableSelectOne(array("ProductPriceID"));
		$DataArray = array();
		$DataArray['Price'] = (isset($_POST['Price']) && $_POST['Price'] !="")?$_POST['Price']:"0";
		$DataArray['SalePrice'] = (isset($_POST['SalePrice']) && $_POST['SalePrice'] !="")?$_POST['SalePrice']:"0";
		$DataArray['SaleActive'] = isset($_POST['SaleActive'])?$_POST['SaleActive']:0;
		if(isset($CurrentPrice->ProductPriceID) && $CurrentPrice->ProductPriceID !="")
		{
			$ProductPriceObj->TableUpdate($DataArray);
		}
		else 
		{
			$DataArray['QtyMin'] = "0";
			$DataArray['QtyMax'] = DEFINE_PRODUCT_QTY_MAX;
			$DataArray['PriceType'] = 'Unit';
			$DataArray['UserTypeID'] = isset($DefaultUserType->UserTypeID)?$DefaultUserType->UserTypeID:"1";
			$DataArray['ProductID'] = $ProductID;
			$ProductPriceObj->TableInsert($DataArray);
		}
		/* ProductPriceObj entry end*/
		

		$ProductRelationObj = new DataTable(TABLE_PRODUCT_RELATION);
		$ProductRelationObj->Where ="RelationType='attribute_value' AND ProductID='".$ProductObj->MysqlEscapeString($ProductID)."'";
		$ProductRelationObj->TableDelete();
		if(isset($_POST['AttributeValue']) && is_array($_POST['AttributeValue']) && count($_POST['AttributeValue']))
		{
			foreach ($_POST['AttributeValue'] as $k=>$v)
			{
					$DataArray = array();
					$DataArray['ProductID'] = $ProductID;
					$DataArray['RelationID'] = $v;
					$DataArray['RelationType'] = "attribute_value";
					$DataArray['SortOrder'] = SKMaxGetPrdAttValSortOrder($v,"attribute_value") + 1;
					$ProductRelationObj->TableInsert($DataArray);
			}
		}

		MyRedirect(DIR_WS_SITE_CONTROL."index.php?Page=$Page&Section=Refresh&Popup=true&MasterID=$MasterID&ProductID=$ProductID");
		exit;
	}	

	break;


}






//// target end

?>