<?php
set_time_limit(1800);
//$AttributeObj2 = new Attributes();
$Section = isset($_GET['Section'])?$_GET['Section']:"";
$Pg = isset($_GET['Pg'])?$_GET['Pg']:"1";
$Target = isset($_GET['Target'])?$_GET['Target']:"";
$ParentID = isset($_GET['ParentID'])?$_GET['ParentID']:0;
$ProductID = isset($_GET['ProductID'])?$_GET['ProductID']:0;
$ImageID = isset($_GET['ImageID'])?$_GET['ImageID']:0;
$ReviewID = isset($_GET['ReviewID'])?$_GET['ReviewID']:0;
$InfoID = isset($_GET['InfoID'])?$_GET['InfoID']:0;
$QuestionID = isset($_GET['QuestionID'])?$_GET['QuestionID']:0;
$PriceType = isset($_GET['PriceType'])?$_GET['PriceType']:0;
$ProductCatgoryArr = array();

$CategoryObj = new DataTable(TABLE_CATEGORY);
$ProductObj = new DataTable(TABLE_PRODUCT);
$ProductCategoryObj = new DataTable(TABLE_PRODUCT_RELATION);
$ProductCategoryObj2 = new DataTable(TABLE_PRODUCT_RELATION);
$ManufacturerObj = new DataTable(TABLE_MANUFACTURERS);
$ProductAdditionObj = new DataTable(TABLE_PRODUCT_ADDITIONS);
$ProductAdditionObj2 = new DataTable(TABLE_PRODUCT_ADDITIONS);
$ProductOptionObj = new DataTable(TABLE_PRODUCT_OPTIONS);
$ProductOptionObj2 = new DataTable(TABLE_PRODUCT_OPTIONS);
$FTPPath = DIR_FS_SITE_UPLOADS."ftp/images/";



/* product import function start */

	$ProductHeader = array("SKU"=>"SKU",
					   "Status"=>"Status",
					   "ProductName"=>"ProductName",
					   "ProductDescription"=>"ProductDescription",
					   "Weight"=>"Weight",
					   "Price"=>"Price",
					   "SalePrice"=>"SalePrice",
					   "SalePriceStatus"=>"SalePriceStatus",
					   "Stock"=>"Stock",
					   "Brand"=>"Brand",
					   "Categories"=>"Categories",
					   "MetaTitle"=>"MetaTitle",
					   "MetaKeywords"=>"MetaKeywords",
					   "Image"=>"Image",
					   "OtherImages"=>"OtherImages",
					   "Files"=>"Files",
					   );
					   
		/*
		$CSVArray = array();
		$CSVArray[] = array_values($ProductHeader);		
		for($i=1;$i<=20;$i++)
		{
			$Data = array();
			foreach($ProductHeader as $k=>$v)
			{
				$Data[] = uniqid($v."_");
			}			
			$CSVArray[] = array_values($Data);
		}
		
		$FileName = DIR_FS_SITE_UPLOADS_DOWNLOAD."tmp/products_sample.csv";				   
		$fp = fopen($FileName, 'w');
			foreach ($CSVArray as $fields) {
			    fputcsv($fp, $fields);
			}
			fclose($fp);	
*/

			
		
	/// Target  start 
switch ($Target)
{
	case "Download":
		if(isset($_POST['Download']) && $_POST['Download'] !="")
		{
			@ob_clean();
			$FileName = DIR_FS_SITE_UPLOADS_DOWNLOAD."tmp/products_".date("Md-Y-his").".csv";
			$ProductObj->ExecuteQuery(base64_decode(@$_REQUEST['Q']));
			$CSVArray = array();
			$CSVArray[] = array("SKU","Quick Books Code","Google Brand","Google MPN","GOOGLE GTIN","PRODUCT NAME","MANUFACTURER","IMAGE LOCATION","SALE PRICE","QUANTITY","SMALL DESCRIPTION","META TITLE","META KEYWORDS","META DESCRIPTION","PRODUCT URL");
				while($CurrentProduct = $ProductObj->GetObjectFromRecord())
				{
					$StArr = GetProductPriceNStock($CurrentProduct->ProductID,1);
					$Image = "";
					if($CurrentProduct->Image !="" && file_exists(DIR_FS_SITE_UPLOADS_PRODUCT_ORIGINAL.$CurrentProduct->Image))
						$Image = DIR_WS_SITE_UPLOADS_PRODUCT_ORIGINAL.$CurrentProduct->Image;
						
					$CSVArray[] = array(MyStripSlashes($CurrentProduct->ModelNo),MyStripSlashes($CurrentProduct->QuickBooksCode),MyStripSlashes($CurrentProduct->GoogleBrand),MyStripSlashes($CurrentProduct->GoogleMPN),MyStripSlashes($CurrentProduct->GoogleGtin),MyStripSlashes($CurrentProduct->ProductName),MyStripSlashes($CurrentProduct->MNName),$Image,$StArr['Price'],MyStripSlashes($CurrentProduct->Stock),MyStripSlashes($CurrentProduct->SmallDescription),MyStripSlashes($CurrentProduct->MetaTitle),MyStripSlashes($CurrentProduct->MetaKeyword),MyStripSlashes($CurrentProduct->MetaDescription),SKSEOURL($CurrentProduct->ProductID,"product"));
				}

			$fp = fopen($FileName, 'w');
			foreach ($CSVArray as $fields) {
			    fputcsv($fp, $fields);
			}
			fclose($fp);
			download_file($FileName);
			exit;		

		}

		
	break;
	case "SampleCSVFile":
		download_file(DIR_FS_SITE_SECURE_DOWNLOAD_PRODUCT."csv_files/product_import.csv");
	break;
	
	case "UploadCSVFile":
	if(isset($_FILES['UploadCSV']) && $_FILES['UploadCSV']['name'] !="")
	{

		$ArrayType = explode(".",$_FILES['UploadCSV']['name']);
		$Type=$ArrayType[count($ArrayType)-1];
		if(in_array(trim(ucwords(strtolower($Type))),array("Csv")))
		{

			$UploadName = "ProductCSV_".date('Y-m-d_H-i-s').".csv";				
			if($UploadName != "" && file_exists(DIR_FS_SITE_SECURE_DOWNLOAD_PRODUCT."csv_files/".$UploadName))
			{
				@ob_clean();
				$_SESSION['ErrorMessage'] = "File name already exist in the csv directory.";
			}

			else 
			{

				copy($_FILES['UploadCSV']['tmp_name'],DIR_FS_SITE_SECURE_DOWNLOAD_PRODUCT."csv_files/".$UploadName);
				MyRedirect(DIR_WS_SITE_CONTROL."index.php?Page=$Page&Section=$Section&Pg=$Pg&ParentID=$ParentID&ProductID=$ProductID&CSVFile=".$UploadName);
				exit;

			}

		}
		else
		 {

		 	@ob_clean();
			
			$_SESSION['ErrorMessage'] = DEFINE_INVALID_FILE_FORMAT;
		 }		
	}
	break;
	case "AddCSVProduct":
		$CSVFile = isset($_POST['CSVFile'])?$_POST['CSVFile']:"";
		
		if(isset($CSVFile) && $CSVFile !="" && is_file($CSVFile))
		{
			$row =1;
			if (($handle = fopen($CSVFile, "r")) !== FALSE) {
		    while (($data = fgetcsv($handle)) !== FALSE) 
		    {
    	    	$num = count($data);
	    	    if($row ==1)
		        {
		        	CSVHeaderData($data);
					/* first row header column*/
		        }
		        if($row !=1)
		        {
		        	
		        	/*CSV product start*/
					foreach($ProductHeader as $key=>$val)
					{
						$index = "DATA_CSV_".strtoupper(str_replace(array(" "),array("_"), trim($val)));
						${$key} = isset($data[@constant($index)])?trim($data[@constant($index)]):"";
					}
					
		        	$ProductID= 0;
		        	$NewEntry = false;
					
					if(isset($SKU) && $SKU != "")
					{
						$ProductObj->Where ="ModelNo='".$SKU."'";
						$Obj = $ProductObj->TableSelectOne(array("ProductID"));
						if(isset($Obj->ProductID) && $Obj->ProductID != "")
							$ProductID = $Obj->ProductID;
					}
					
					$DataArray = array();
					
					if(isset($SKU) && $SKU != "")
						$DataArray['ModelNo'] = $SKU;
					
					if(isset($Brand) && $Brand != "")
					{
						$MNID = GetManufacturerMNIDForCSV($Brand);
						$DataArray['MNID'] = $MNID;
					}
					
					if(isset($ProductName) && $ProductName != "")
						$DataArray['ProductName'] = substr($ProductName,0,255);
					
					if(isset($SmallDescription) && $SmallDescription != "")
						$DataArray['SmallDescription'] = $SmallDescription;
					
					if(isset($ProductDescription) && $ProductDescription != "")
						$DataArray['LargeDescription'] = $ProductDescription;
					
					if(isset($Stock) && $Stock != "")
						$DataArray['Stock'] = $Stock;
				
					if(isset($Weight) && $Weight != "")
						$DataArray['Weight'] = $Weight;

					if(isset($Status) && $Status != "")
						$DataArray['Active'] = ($Status=="1"?"1":"0");
	
					if(isset($Image) && $Image != "" && strpos($Image,".")==false)	
						$Image = $Image.".jpg";
					
					
					if(isset($Image) && $Image != "" && file_exists($FTPPath.$Image))
					{
						$ArrayType = explode(".",$Image);
						$Type=$ArrayType[count($ArrayType)-1];
						$ImageName = uniqid("p-".SkURLCreate(substr($DataArray['ProductName'],0,10))."_").".".$Type;				
						$OriginalImage =DIR_FS_SITE_UPLOADS_PRODUCT_ORIGINAL.$ImageName;
						if(copy($FTPPath.$Image,$OriginalImage))
						{
							@unlink($FTPPath.$Image);
						}
						chmod($OriginalImage,0777);
						$DataArray['Image'] = $ImageName;
					}
	
					if($ProductID > 0)
					{
						$ProductObj->TableUpdate($DataArray);
						/* update product code*/
					}
					else
					{
						$DataArray['ProductType'] = "Simple";
						$DataArray['CreatedDate'] = date('Y-m-d H:i:s');
						$ProductID = $ProductObj->TableInsert($DataArray);		
						$NewEntry = true;
						/* new product code*/
					}
					
					
					/* additional images Start */
					if(isset($OtherImages) && $OtherImages != "")
					{
						foreach(explode(",",$OtherImages) as $Image)
						{
							if(isset($Image) && $Image != "" && file_exists($FTPPath.$Image))
							{
								
								
								$ArrayType = explode(".",$Image);
								$Type=$ArrayType[count($ArrayType)-1];
								$ImageName = uniqid("p-".SkURLCreate(substr($DataArray['ProductName'],0,10))."_").".".$Type;				
								$OriginalImage =DIR_FS_SITE_UPLOADS_PRODUCT_ORIGINAL.$ImageName;
								if(copy($FTPPath.$Image,$OriginalImage))
								{
									@unlink($FTPPath.$Image);
								}
								chmod($OriginalImage,0777);
								
								$ImageArray = array();
								$ImageArray['ProductID'] = $ProductID;
								$ImageArray['Active'] = "0";
								$ImageArray['Position'] = "0";
								$ImageArray['ImageName'] = $DataArray['ProductName'];
								$ImageArray['ImageType'] = "Image";
								$ImageArray['Image'] = $ImageName;
								$ProductImageObj->TableInsert($ImageArray);
							}
						}
					}
					
					/* additional images End*/
					
					/* seo entry start*/
					$SeoObj = new DataTable(TABLE_SEO);
					$SeoObj->Where = "ReferenceID = ".$ProductID." AND RefrenceType ='shop/product'";
					$CurrentSEO = $SeoObj->TableSelectOne(array("ReferenceID"));
					$SEOArray = array();
					if(isset($MetaTitle) && $MetaTitle != "")
						$SEOArray['MetaTitle'] = $MetaTitle;
					if(isset($MetaKeyword) && $MetaKeyword != "")
						$SEOArray['MetaKeyword'] = $MetaKeyword;
					if(isset($MetaDescription) && $MetaDescription != "")
						$SEOArray['MetaDescription'] = $MetaDescription;
					
					if(isset($CurrentSEO->ReferenceID) && $CurrentSEO->ReferenceID !="")
					{
						$SEOArray['RefrenceType'] = "shop/product";
						$SeoObj->TableUpdate($SEOArray);
					}
					else 
					{
						$SEOArray['URLName'] = SkURLCreate(substr($ProductName,0,100)."-".$SKU);
						$SEOArray['RedirectEnabled'] = 0;
						$SEOArray['RefrenceType'] = "shop/product";
						$SEOArray['ReferenceID'] = $ProductID;
						$SeoObj->TableInsert($SEOArray);
					}
					/* seo entry end*/
					
					/* ProductPrice entry start*/
					$ProductPriceObj =new DataTable(TABLE_PRODUCT_PRICES);
					$ProductPriceObj->Where = "ProductID = ".$ProductID." AND UserTypeID ='".(int)(isset($DefaultUserType->UserTypeID)?$DefaultUserType->UserTypeID:"1")."' AND QtyMin='0'";
					$CurrentPrice = $ProductPriceObj->TableSelectOne(array("ProductPriceID"));
					$PriceArray = array();
					
					if(isset($Price) && $Price != "")
						$PriceArray['Price'] = $Price;
					
					if(isset($SalePrice) && $SalePrice != "")
						$PriceArray['SalePrice'] = $SalePrice;
					if(isset($SalePriceStatus) && $SalePriceStatus != "")
						$PriceArray['SaleActive'] = ($SalePriceStatus=="1"?"1":"0");
					
					if(isset($CurrentPrice->ProductPriceID) && $CurrentPrice->ProductPriceID !="")
					{
						$ProductPriceObj->TableUpdate($PriceArray);
					}
					else 
					{
						$PriceArray['QtyMin'] = "0";
						$PriceArray['QtyMax'] = DEFINE_PRODUCT_QTY_MAX;
						$PriceArray['PriceType'] = 'Unit';
						$PriceArray['UserTypeID'] = isset($DefaultUserType->UserTypeID)?$DefaultUserType->UserTypeID:"1";
						$PriceArray['ProductID'] = $ProductID;
						$ProductPriceObj->TableInsert($PriceArray);
					}
					/* ProductPriceObj entry end*/
					
					
					/* Category  start*/
					
					if(isset($Categories) && $Categories != "")
					{
						$CategoryArray  = GetCategoryIDsForCSV($Categories);
						
						if(is_array($CategoryArray) && count($CategoryArray) > 0)
						{
							$ProductCategoryObj = new DataTable(TABLE_PRODUCT_RELATION);
							$ProductCategoryObj->Where = "RelationType='category' and ProductID='".$ProductCategoryObj->MysqlEscapeString($ProductID)."'";
							if(implode(",", $CategoryArray) != "")
								$ProductCategoryObj->Where .= "and RelationID NOT IN (".implode(",",$CategoryArray).")";
							
							$ProductCategoryObj->TableDelete();	
							foreach ($CategoryArray as $ck=>$cv)
							{
								if($cv !="")
								{	
									$ProductCategoryObj->Where = "RelationType='category' and ProductID='".$ProductCategoryObj->MysqlEscapeString($ProductID)."' and RelationID=".$cv;
									if(!is_object($ProductCategoryObj->TableSelectOne(array("id")))){
										$DataArray = array();
										$DataArray['ProductID'] = $ProductID;
										$DataArray['RelationID'] = $cv;
										$DataArray['RelationType'] = "category";
										$DataArray['SortOrder'] = SKMaxGetProductSortOrder($cv,"category") + 1;
										$ProductCategoryObj->TableInsert($DataArray);
									}
								}
							}
							
			
						}
						
					}
					/* Category end*/
					
					
					
					
					
					
		        	/*CSV product end*/
	
		        	
		        }
	         $row++;
	        }
		    fclose($handle);
			    
			    
			
			}
			
			@ob_clean();
								
			$_SESSION['InfoMessage'] ="products updated successfully.";
			MyRedirect(DIR_WS_SITE_CONTROL."index.php?Page=shop/product_import&Section=Import");
			exit;	
		}
		

}

//// target end

function CSVHeaderData($arr)
{
		foreach ($arr as $key => $value)
		{
			$prop = "DATA_CSV_".strtoupper(str_replace(array(" "),array("_"), trim($value)));
			if(!defined($prop))	
			{
				define($prop,$key);
			}
		}
		
}

function GetManufacturerMNIDForCSV($MNName)
{
	$MNID = "0";
	$ManufacturerObj = new DataTable(TABLE_MANUFACTURERS);
	$ManufacturerObj->Where ="MNName='".$ManufacturerObj->MysqlEscapeString(trim($MNName))."'";
   	$CurrentManufacturer = $ManufacturerObj->TableSelectOne(array("MNID"));
	if(isset($CurrentManufacturer->MNID) && $CurrentManufacturer->MNID !="")
	$MNID = $CurrentManufacturer->MNID;

	return $MNID;
}

function GetCategoryIDsForCSV($Categories)
{
	
	$CategoryArray = array();
	foreach (explode("||", $Categories) as $value) 
	{
		$arr=explode("->", $value);
		$CategoryID = "";
		$CategoryParentName = "";
		$CategoryName = "";
		
		if(count($arr) > 0)
		{
			switch (count($arr))
			{
				case "1":
					$CategoryParentName = "";
					$CategoryName = $arr[count($arr)-1];
				break;
				case (count($arr) > 1):
					$CategoryParentName = $arr[count($arr)-2];
					$CategoryName = $arr[count($arr)-1];
				break;
				
			}
		}
		
		
		if($CategoryName != "")
		{
			if(substr($CategoryName,0,1)=="-")
				$CategoryName= substr($CategoryName,1);
			
			$ParentID = 0;
			if($CategoryParentName != "")
			{
				$CategoryObj = new DataTable(TABLE_CATEGORY." ch, ".TABLE_CATEGORY." pa ");
				$CategoryObj->Where ="pa.CategoryID = ch.ParentID AND ch.CategoryName = '".$CategoryObj->MysqlEscapeString(trim($CategoryName))."' AND pa.CategoryName = '".$CategoryObj->MysqlEscapeString(trim($CategoryParentName))."'";
				$CurrentCategory = $CategoryObj->TableSelectOne(array("ch.CategoryID"));
			}
			if(!isset($CurrentCategory->CategoryID))
			{
				$CategoryObj = new DataTable(TABLE_CATEGORY." ch");
				$CategoryObj->Where ="ch.ParentID='0' AND ch.CategoryName = '".$CategoryObj->MysqlEscapeString(trim($CategoryName))."' ";
				$CurrentCategory = $CategoryObj->TableSelectOne(array("ch.CategoryID"));
			}
			
			
			if(isset($CurrentCategory->CategoryID) && $CurrentCategory->CategoryID != "")
				$CategoryArray[] = $CurrentCategory->CategoryID;
		}
			
	}
	return $CategoryArray;
}
