<?php
$Section = isset($_GET['Section'])?$_GET['Section']:"";
$Target = isset($_GET['Target'])?$_GET['Target']:"";

$OrderObj = new DataTable(TABLE_ORDERS);



switch ($Target)
{
	case "DeleteFullOrder":
		$OrderID =isset($_GET['OrderID'])?$_GET['OrderID']:"";
		$PaymentStatus =isset($_GET['PaymentStatus'])?$_GET['PaymentStatus']:"";
		$ShippingStatus = isset($_GET['ShippingStatus'])?$_GET['ShippingStatus']:"";
		$PaymentMethod=isset($_GET['PaymentMethod'])?$_GET['PaymentMethod']:"";
		$Attempted=isset($_GET['Attempted'])?$_GET['Attempted']:"";
	
		
		
		$OrderObj = new DataTable(TABLE_ORDERS);
		$OrderObj->Where ="OrderID = '".$OrderID."'";
		$CurrentOrder = $OrderObj->TableSelectOne();

		if(isset($CurrentOrder->OrderID) && $CurrentOrder->OrderID != "" && $CurrentOrder->PaymentStatus=="Pending")
		{
			$OrderObj->TableDelete();		
			
			$OrderChangeStatusObj = new DataTable(TABLE_ORDER_CHANGE_STATUS);
			$OrderChangeStatusObj->Where ="OrderID = '".$OrderID."'";
			$OrderChangeStatusObj->TableDelete();		
			
			$OrderCurrencyObj = new DataTable(TABLE_ORDER_CURRENCIES);
			$OrderCurrencyObj->Where ="OrderID = '".$OrderID."'";
			$OrderCurrencyObj->TableDelete();		
			
			
			$OrderDetailObj = new DataTable(TABLE_ORDER_DETAILS);
			$OrderDetailObj->Where ="OrderID = '".$OrderID."'";
			$OrderDetailObj->TableDelete();		
		
			$CreditMemoObj = new DataTable(TABLE_CREDIT_MEMO);
			$CreditMemoObj->Where ="OrderID = '".$OrderID."'";
			$CreditMemoObj->TableDelete();		
			
		}
		
		@ob_clean();
		
		$_SESSION['InfoMessage'] ="Order deleted successfully.";
		MyRedirect(DIR_WS_SITE_CONTROL."index.php?Page=$Page&Section=$Section&PaymentStatus=$PaymentStatus&Attempted=$Attempted");
		exit;		
		
	break;
	case "DeleteOrder":
		$OrderID =isset($_GET['OrderID'])?$_GET['OrderID']:"";
		$PaymentStatus =isset($_GET['PaymentStatus'])?$_GET['PaymentStatus']:"";
		$OrderStatus = isset($_GET['OrderStatus'])?$_GET['OrderStatus']:"";
		$PaymentMethod=isset($_GET['PaymentMethod'])?$_GET['PaymentMethod']:"";
		$Attempted=isset($_GET['Attempted'])?$_GET['Attempted']:"";
	
		
		$OrderChangeStatusObj = new DataTable(TABLE_ORDER_CHANGE_STATUS);
		$DataArray['OrderID'] = $OrderID;
		$DataArray['ShippingStatus'] = $ShippingStatus;
		$DataArray['OrderStatus'] = "Deleted";
		$DataArray['Comments'] = "Deleted";
		$DataArray['CreatedDate'] = date("YmdHis");
		$OrderChangeStatusObj->TableInsert($DataArray);
		
		$OrderArray['OrderStatus'] = $DataArray['OrderStatus'];
		$OrderObj->Where ="OrderID = '".$OrderID."'";
		
		$OrderObj->TableUpdate($OrderArray);		
		
		@ob_clean();
		
		$_SESSION['InfoMessage'] ="Order deleted successfully.";
		MyRedirect(DIR_WS_SITE_CONTROL."index.php?Page=$Page&Section=$Section&PaymentStatus=$PaymentStatus&OrderStatus=$OrderStatus&Attempted=$Attempted");
		exit;		
		
	break;
	case "UpdateOrderStatus":
		$OrderID =isset($_GET['OrderID'])?$_GET['OrderID']:"";
		
		UpdatePaidOrderFromAdmin($OrderID,@$_POST['OrderStatus'],@$_POST['ShippingStatus']);
		
		$OrderChangeStatusObj = new DataTable(TABLE_ORDER_CHANGE_STATUS);
		$DataArray['OrderID'] = $OrderID;
		$DataArray['OrderStatus'] = @$_POST['OrderStatus'];
		$DataArray['ShippingStatus'] = @$_POST['ShippingStatus'];
		$DataArray['Comments'] = @$_POST['Comments'];
		$DataArray['Notify'] =isset($_POST['Notify'])?$_POST['Notify']:0;
		$DataArray['CreatedBy'] = "Admin";
		$DataArray['CreatedDate'] = date("YmdHis");
		
		//$OrderChangeStatusObj->DisplayQuery = true;
		$OrderChangeStatusObj->TableInsert($DataArray);
		
		if($DataArray['OrderStatus']=="Paid")
			$OrderArray['PaymentStatus'] = "Paid";
		
		if($DataArray['OrderStatus']=="PendingPayment")
			$OrderArray['PaymentStatus'] = "Pending";
		
		$OrderArray['OrderStatus'] = $DataArray['OrderStatus'];
		$OrderArray['ShippingStatus'] = $DataArray['ShippingStatus'];
		$OrderObj->Where ="OrderID = '".$OrderID."'";
		
		$OrderObj->TableUpdate($OrderArray);		
		if($DataArray['Notify'] =="1")
		{
			$CurrentOrder = $OrderObj->TableSelectOne(array("*, DATE_FORMAT(CreatedDate,'%M %d, %Y %r')as MyCreatedDate, DATE_FORMAT(CreatedDate,'%M %d, %Y')as MyShortCreatedDate, DATE_FORMAT(CreatedDate,'%y%m%d')as FormattedCreatedDate"));
			$Mail_Subject = "Your Order Details at ".SITE_NAME." (Ref: ".$CurrentOrder->OrderNo.")";
			$Mail_ToEmail = isset($CurrentOrder->Email)?$CurrentOrder->Email:ADMIN_EMAIL;
			$Mail_FromEmail = ADMIN_EMAIL;
			$Mail_FromName = SITE_NAME;
			$MessageBody = "Dear ".MyStripSlashes($CurrentOrder->BillingFirstName)." ".MyStripSlashes($CurrentOrder->BillingLastName)."<br><br>";
			
			$MessageBody .="Your order is being processed.									 
							<br><br><b>OrderDetails:</b><br>Order Number: ".$CurrentOrder->OrderNo." <br>Status: ".$DataArray['OrderStatus']."";
			$MessageBody .= $DataArray['Comments']!=""?"<br><br>Additional Comment: ".$DataArray['Comments']:"";
			$MessageBody .= "<br><br>Thank you, if you have any queries you can contact us at: <a href='".SALES_EMAIL."'>".SALES_EMAIL."</a>.<br><br>".SITE_NAME;
			SendEmail($Mail_Subject,$Mail_ToEmail,$Mail_FromEmail,$Mail_FromName,$MessageBody,BCC_EMAIL);
		
		}
		@ob_clean();
		
		$_SESSION['InfoMessage'] ="Order status changed successfully.";
		MyRedirect(DIR_WS_SITE_CONTROL."index.php?Page=$Page&Section=$Section&OrderID=".$OrderID."");
		exit;
	break;
	case "UpdateTrackingCode":
		$OrderID =isset($_GET['OrderID'])?$_GET['OrderID']:"";
		$OrderArray = array();
		$OrderArray['TrackingCode'] = $_POST['TrackingCode'];
		$OrderObj->Where ="OrderID = '".$OrderID."'";
		$OrderObj->TableUpdate($OrderArray);		
				
		@ob_clean();
		
		$_SESSION['InfoMessage'] ="Tracking Code changed successfully.";
		MyRedirect(DIR_WS_SITE_CONTROL."index.php?Page=$Page&Section=$Section&OrderID=".$OrderID."");
		exit;
	break;
	case "CreateMemo":
		$OrderID =isset($_GET['OrderID'])?$_GET['OrderID']:"";
		$ProductRefund = 0;
		$TotalRefund = 0;
		
		$CreditMemoObj = new DataTable(TABLE_CREDIT_MEMO);
		$DataArray = array();
		$DataArray['OrderID'] = $OrderID;
		$DataArray['ProductRefund'] = $ProductRefund;
		$DataArray['ShippingRefund'] = $_POST['ShippingRefund'];
		$DataArray['TaxRefund'] = $_POST['TaxRefund'];
		$DataArray['AdjustmentRefund'] = $_POST['RefundAdjustment'];
		$DataArray['AdjustmentFees'] = $_POST['AdjustmentFees'];
		$DataArray['TotalRefund'] = $TotalRefund;
		$DataArray['Comments'] = $_POST['CreditMemoComments'];
		$DataArray['Notified'] = 1;//isset($_POST['CustomerNotify']) ? "1":"0";
		$DataArray['Status'] = "Offline Refund";
		$DataArray['CreatedDate'] = date('Y-m-d H:i:s');
		$DataArray['PaymentGateway'] = "";
		$DataArray['TransactionID'] = "";
		$CreditMemoID = $CreditMemoObj->TableInsert($DataArray);
				
		for ($i=1;$i <$_POST['Count'];$i++):
			if(isset($_POST['Qty'.$i]) && intval($_POST['Qty'.$i]) > 0):
				$OrderDetailsObj = new DataTable(TABLE_ORDER_DETAILS);
				$OrderDetailsObj->Where = "ItemID = '".$_POST['ItemID'.$i]."'";
				$CurrentOrderDetailsObj = $OrderDetailsObj->TableSelectOne();
								
				$ProductObj = new DataTable(TABLE_PRODUCT);
				$ProductObj->Where = "ProductID = '$CurrentOrderDetailsObj->ReferenceID'";
				$CurrentProductObj = $ProductObj->TableSelectOne();
				
				$DataArray = array();
				$DataArray['Stock'] = $CurrentProductObj->Stock + intval($_POST['Qty'.$i]);
				$ProductObj->Where = "ProductID = '$CurrentOrderDetailsObj->ReferenceID'";
				$ProductObj->TableUpdate($DataArray);
				
				$CreditMemoDetailsObj = new DataTable(TABLE_CREDIT_MEMO_DETAILS);
				$DataArray = array();
				$DataArray['CreditMemoID'] = $CreditMemoID;
				$DataArray['ItemID'] = $CurrentOrderDetailsObj->ItemID;
				$DataArray['ProductID'] = $CurrentOrderDetailsObj->ReferenceID;
				$DataArray['Qty'] = intval($_POST['Qty'.$i]);
				$CreditMemoDetailsObj->TableInsert($DataArray);
				
				$ProductRefund += intval($_POST['Qty'.$i]) * $CurrentOrderDetailsObj->Price;				
			endif;
		endfor;
		$TotalRefund = $ProductRefund + floatval($_POST['ShippingRefund']) + floatval($_POST['TaxRefund']) + floatval($_POST['RefundAdjustment']) - floatval($_POST['AdjustmentFees']);

		$CreditMemoObj = new DataTable(TABLE_CREDIT_MEMO);
		$DataArray = array();
		$DataArray['ProductRefund'] = $ProductRefund;
		$DataArray['TotalRefund'] = $TotalRefund;
		$CreditMemoObj->Where = "CreditMemoID = '$CreditMemoID'";
		$CreditMemoObj->TableUpdate($DataArray);

		$OrderObj = new DataTable(TABLE_ORDERS);
		$DataArray = array();
		$DataArray['RefundAmount'] = "RefundAmount + ".$TotalRefund;
		$OrderObj->Where = "OrderID = '$OrderID'";
		$OrderObj->TableUpdate($DataArray,false);
		
		@ob_clean();
		
		$_SESSION['InfoMessage'] ="Credit Memo created successfully.";
		MyRedirect(DIR_WS_SITE_CONTROL."index.php?Page=$Page&Section=FullDetail&OrderID=".$OrderID."");
		exit;
		break;
}
?>