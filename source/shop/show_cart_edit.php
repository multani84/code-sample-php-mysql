 <table class="table table-striped table-bordered responsive table-cart">
	<thead>
		<tr>
			<th align="center" height="25">S.No</th>
			<th class="title">Item</th>
			<?php
			if(@constant("DEFINE_PRODUCTS_ATTRIBUTE")=="1")
			{
			?>
			<th align="left"><?php echo @constant("DEFINE_PRODUCTS_ATTRIBUTE_OPTION1");?></th>
			<th align="left"><?php echo @constant("DEFINE_PRODUCTS_ATTRIBUTE_OPTION2");?></th>
			<?php
			}?>
			<th class="qty">Qty</th>
			<th class="price">Price</th>
			<th class="price">Total</th>
			<th class="action"></th>
		</tr>  
	</thead>
    <tbody>
	<?php		
	$SNo = 1;
	$TaxPercentDisplay = 0;
		while ($CurrentCartItem = $CartObj->GetObjectFromRecord()) 
		{
			$TaxPercentDisplay = max($CurrentCartItem->TaxPercent,$TaxPercentDisplay);
				?>
					<tr>
					<td align="center" width="5%"><input type="hidden" name="ItemID<?php echo $SNo?>" value="<?php echo $CurrentCartItem->ItemID?>"><?php echo $SNo?>.</td>
					<td class="title"><?php echo $CurrentCartItem->ItemName?>
						<?php echo $CurrentCartItem->Comments !=""?"<br>".nl2br($CurrentCartItem->Comments):""?>
					</td>
					<?php
					if(@constant("DEFINE_PRODUCTS_ATTRIBUTE")=="1")
					{
					?>
					<td align="left"><?php echo $CurrentCartItem->Attribute1?></td>
					<td align="left"><?php echo $CurrentCartItem->Attribute2?></td>
					<?php
					}?>
					<td class="qty">
					<?php
					if($CurrentCartItem->ItemStatus =="Normal")
					{
						echo "<input style='text-align:center;color:#000;' type='text' size='2' name='Qty[".$CurrentCartItem->ItemID."]' value='$CurrentCartItem->Qty' onblur='return CheckInvalidQuantity(this,\"$CurrentCartItem->Qty\",\"".@constant('INVALID_QUANTITY')."\");'>";
					}
					else 
					{
						echo "<input style='text-align:center;color:#000;' type='hidden' size='2' name='Qty[".$CurrentCartItem->ItemID."]' value='$CurrentCartItem->Qty' onblur='return CheckInvalidQuantity(this,\"$CurrentCartItem->Qty\",\"".@constant('INVALID_QUANTITY')."\");'>";
					}
					?>
					</td>
					<td class="price" style="text-align:right;<?php echo ($CurrentCartItem->ItemType=="Offer")?"color:#ff0000;":"";?>">
						<?php
						if($CurrentCartItem->Price > $CurrentCartItem->UnitPrice)
						{
							echo Change2CurrentCurrency($CurrentCartItem->Price);
							echo "<br><small>(".Change2CurrentCurrency($CurrentCartItem->UnitPrice). " + " . Change2CurrentCurrency($CurrentCartItem->TaxPrice)."-GST)</small>" ;
						}
						else
						{
							echo Change2CurrentCurrency($CurrentCartItem->UnitPrice);
						}
						?>
					</td>
					<td class="price" style="text-align:right;<?php echo ($CurrentCartItem->ItemType=="Offer")?"color:#ff0000;":"";?>"><?php echo $CurrentCartItem->Total != 0?Change2CurrentCurrency($CurrentCartItem->Total):""?></td>
					<td class="action">
					<?php
					if($CurrentCartItem->ItemType !="Offer")
					{
						echo "<a href='".MakePageURL("index.php","Page=$Page&Target=DeleteCart&ItemID=$CurrentCartItem->ItemID")."' class='cart-link btn btn-default' title='".@constant('DEFINE_DELETE_CART_ITEM')."' onclick='return confirm(\"".@constant('DEFINE_DELETE_CART_ITEM')."\")';><span class='glyphicon glyphicon-trash'></span></a>";
					}?></td>
				</tr>
				<?php
				$CartObj2->Where =" ParentItemID='".$CartObj2->MysqlEscapeString($CurrentCartItem->ItemID)."'";
				$CartObj2->TableSelectAll(array("*"));
				while($CurrentCartItem2 = $CartObj2->GetObjectFromRecord())
				{
					?>
					<tr>
					<td width="5%"></td>
					<td class="title">--<i><?php echo $CurrentCartItem2->ItemName?></i>
						<?php echo $CurrentCartItem2->Comments !=""?"<br>".nl2br($CurrentCartItem2->Comments):""?>
					</td>
					<?php
					if(@constant("DEFINE_PRODUCTS_ATTRIBUTE")=="1")
					{
					?>
					<td align="left"><?php echo $CurrentCartItem2->Attribute1?></td>
					<td align="left"><?php echo $CurrentCartItem2->Attribute2?></td>
					<?php
					}?>
					<td class="qty">
					<?php
					if($CurrentCartItem2->ItemStatus =="Normal")
					{
						echo "<input style='text-align:center' type='text' size='2' name='Qty$SNo' value='$CurrentCartItem2->Qty' onblur='return CheckInvalidQuantity(this,\"$CurrentCartItem2->Qty\",\"".@constant('INVALID_QUANTITY')."\");'>";
					}
					else 
					{
						echo "<input style='text-align:center' type='hidden' size='2' name='Qty$SNo' value='$CurrentCartItem2->Qty' onblur='return CheckInvalidQuantity(this,\"$CurrentCartItem2->Qty\",\"".@constant('INVALID_QUANTITY')."\");'>";
					}
					?>
					</td>
					<td class="price" style="text-align:right;<?php echo ($CurrentCartItem2->ItemType=="Offer")?"color:#ff0000;":"";?>">
						<?php
						if($CurrentCartItem2->Price > $CurrentCartItem2->UnitPrice)
						{
							echo Change2CurrentCurrency($CurrentCartItem2->Price);
							echo "<br><small>(".Change2CurrentCurrency($CurrentCartItem2->UnitPrice). " + " . Change2CurrentCurrency($CurrentCartItem2->TaxPrice)."-GST)</small>" ;
						}
						else
						{
							echo Change2CurrentCurrency($CurrentCartItem2->UnitPrice);
						}
						?>
					</td>
					<td class="price"  style="text-align:right;<?php echo ($CurrentCartItem2->ItemType=="Offer")?"color:#ff0000;":"";?>"><?php echo $CurrentCartItem2->Total != 0?Change2CurrentCurrency($CurrentCartItem2->Total):""?></td>
					<td class="action"></td>
				</tr>
					<?php
				}
				$SNo =$SNo+1;
		}
		?>
		</tbody>
		<tfoot>
		<input type="hidden" name="Count" value="<?php echo $SNo?>">
	    <tr>
			<td colspan="<?php echo @constant("DEFINE_PRODUCTS_ATTRIBUTE")=="1"?"6":"4"?>" align="right" class="CartCalTd">Goods Total Net Amount(Excl.GST)</td>
			<td class="price"><?php echo Change2CurrentCurrency($ShoppingCalculationObj->SubTotal);?></td>
			<td class="action"></td>
		</tr>
		<?php
		if($ShoppingCalculationObj->ShippingPrice >0 OR (isset($ShippingOptionApply) && $ShippingOptionApply=="1"))
		{?>
		<tr>
			<td colspan="<?php echo @constant("DEFINE_PRODUCTS_ATTRIBUTE")=="1"?"6":"4"?>" class="text-right">Shipping Costs
			<?php echo isset($ShippingOptionTEXT)?$ShippingOptionTEXT:$ShoppingCalculationObj->ShippingText?>
			</td>
			<td class="price"><?php echo Change2CurrentCurrency($ShoppingCalculationObj->ShippingPrice);?></td>
			<td class="action"></td>
		</tr>
		<tr>
			<td colspan="<?php echo @constant("DEFINE_PRODUCTS_ATTRIBUTE")=="1"?"6":"4"?>" class="text-right">Sub Total </td>
			<td class="price"><?php echo Change2CurrentCurrency($ShoppingCalculationObj->SubTotal + $ShoppingCalculationObj->ShippingPrice);?></td>
			<td class="action"></td>
		</tr>
	<?php
		}
		?>
		<?php
		if($ShoppingCalculationObj->VATCharges >0)
		{?>
		<tr>
			<td colspan="<?php echo @constant("DEFINE_PRODUCTS_ATTRIBUTE")=="1"?"6":"4"?>" class="text-right">GST(<?php echo $TaxPercentDisplay?>%)</td>
			<td class="price"><?php echo Change2CurrentCurrency($ShoppingCalculationObj->VATCharges);?></td>
			<td class="action"></td>
		</tr>
		<?php
		}
		?>
		<tr class="grand-total">
			<td colspan="<?php echo @constant("DEFINE_PRODUCTS_ATTRIBUTE")=="1"?"6":"4"?>" class="text-right">Total </td>
			<td class="price"><?php echo Change2CurrentCurrency($ShoppingCalculationObj->Total);?></td>
			<td class="action"></td>
		</tr>
		<?php
		if($ShoppingCalculationObj->VoucherAmount >0)
		{?>
		<tr>
			<td colspan="<?php echo @constant("DEFINE_PRODUCTS_ATTRIBUTE")=="1"?"6":"4"?>" class="text-right">Discount <?php /*echo $ShoppingCalculationObj->VoucherText !=""?'<br>('.$ShoppingCalculationObj->VoucherText.')':'';*/?></td>
			<td class="price"><?php echo Change2CurrentCurrency($ShoppingCalculationObj->VoucherAmount);?></td>
			<td class="action"></td>
		</tr>
	<?php
		}
		if($ShoppingCalculationObj->PointAmount >0)
		{?>
		<tr>
			<td colspan="<?php echo @constant("DEFINE_PRODUCTS_ATTRIBUTE")=="1"?"6":"4"?>" class="text-right">Discount on points -<?php echo $ShoppingCalculationObj->PointText?></td>
			<td class="price"><?php echo Change2CurrentCurrency($ShoppingCalculationObj->PointAmount);?></td>
			<td class="action"></td>
		</tr>
		<?php
		}
		if($ShoppingCalculationObj->GrandTotal !=$ShoppingCalculationObj->Total)
		{?>
		<tr class="grand-total">
			<td colspan="<?php echo @constant("DEFINE_PRODUCTS_ATTRIBUTE")=="1"?"6":"4"?>" class="text-right">Grand Total </td>
			<td class="price"><?php echo Change2CurrentCurrency($ShoppingCalculationObj->GrandTotal);?></td>
			<td class="action"></td>
		</tr>
		<?php
		}?>
	</tfoot>
</table>
