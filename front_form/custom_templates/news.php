<?php
/*
Template: Default
*/
$View = isset($_GET['View'])?$_GET['View']:"image";
$GalleryObj = new DataTable(TABLE_OTHER_GALLERY);
?>
<section id="MainContent">
	<div class="container"> 
		<ul class="breadcrumb">
			<li><a href="<?php echo SKSEOURL("1001","cms")?>">Home</a></li>
			<li class="active"><?php echo ((isset($CurrentContent->LeftTitle1) && $CurrentContent->LeftTitle1 !="")?"".MyStripSlashes($CurrentContent->LeftTitle1)."":"");?></li>
		</ul>
		<?php echo ((isset($CurrentContent->LeftTitle1) && $CurrentContent->LeftTitle1 !="")?"<h1 class='Title'>".MyStripSlashes($CurrentContent->LeftTitle1)."</h1>":"");?>
		<?php require_once(DIR_FS_SITE_INCLUDES."message.php");?>
		<div class="row">
			<div class="col-md-12">
				<?php echo (isset($CurrentContent->LeftDescription1)?MyStripSlashes($CurrentContent->LeftDescription1):"&nbsp;");?>
			</div>
			<?php /* gallery start */?>
				<div class="pull-right">
					<a href="<?php echo SKSEOURL($CurrentContent->PageID,"cms","View=image")?>" class="Button">View Images</a>
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<a href="<?php echo SKSEOURL($CurrentContent->PageID,"cms","View=video")?>" class="Button">View Videos</a>
				</div>
				<div class="clear" style="clear:both;"></div>
				<?php if($View == "video"):
				$GalleryObj->Where = "Alignment='VD' AND Active = '1'";
				$GalleryObj->TableSelectAll(array("*"),"Position ASC");
				if($GalleryObj->GetNumRows() > 0)
				{
		
				?>
				<div class="row text-center">
					<?php while($CurrentGallery = $GalleryObj->GetObjectFromRecord()):?>
						<div class="col-sm-12">
								<div class="video">
										<?php  echo MyStripSlashes($CurrentGallery->EmbedSource);?>
								</div>
								<div class="name">
										<?php echo MyStripSlashes($CurrentGallery->ItemTitle);?>
								</div>								
						</div>
					<?php endwhile;?>
				</div>	
					
				<?php 
				}
				endif;?>
				
				<?php if($View == "image"):
				$GalleryObj->Where = "Alignment='PH' AND Active = '1'";
				$GalleryObj->TableSelectAll(array("*"),"Position ASC");
				if($GalleryObj->GetNumRows() > 0)
				{
		
				?>
				<div class="row text-center">
					<?php while($CurrentGallery = $GalleryObj->GetObjectFromRecord()):?>
						<div class="col-sm-3 media-image">
								<div class="image">
										<a href="<?php echo SKImgDisplay(DIR_FS_SITE_UPLOADS_OTHER_ORIGINAL.$CurrentGallery->Image,'1024','','',true,true);?>" title="<?php echo MyStripSlashes($CurrentGallery->ItemTitle);?>" data-lity>
											<?php SKImgDisplay(DIR_FS_SITE_UPLOADS_OTHER_ORIGINAL.$CurrentGallery->Image,"200",MyStripSlashes($CurrentGallery->ItemTitle)," class='img-responsive center-block'");?>
										</a>
								</div>
								<div class="name">
										<?php echo MyStripSlashes($CurrentGallery->ItemTitle);?>
								</div>								
						</div>
					<?php endwhile;?>
				</div>	
					
				<?php 
				}
				endif;?>
				
			<?php /* gallery end */?>
			
		</div>
	</div>
</section>
	

