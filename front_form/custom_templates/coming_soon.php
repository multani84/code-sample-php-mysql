<?php
/*
Template: Coming Soon
*/
@ob_clean();
?>
<html>
<head>
	<title>Coming soon</title>
</head>
<body>
<section id="MainContent">
<div id="CMSPage" class="container vh100">
	<div class="EditorContent">
		<?php echo ((isset($CurrentContent->LeftTitle1) && $CurrentContent->LeftTitle1 !="")?"<h1 class='Title'>".MyStripSlashes($CurrentContent->LeftTitle1)."</h1>":"");?>
		<?php /* middle content start*/?>
		<?php require_once(DIR_FS_SITE_INCLUDES."message.php");?>
		<div style="display:inline-block;width:100%;">
		<?php 
		echo (isset($CurrentContent->LeftDescription1)?MyStripSlashes($CurrentContent->LeftDescription1):"&nbsp;");
		if(isset($CurrentContent->PageID) && file_exists(DIR_FS_SITE_CMS_FORMS."form_".$CurrentContent->PageID.".php") && $CurrentContent->PageID !="")
			require_once(DIR_FS_SITE_CMS_FORMS."form_".$CurrentContent->PageID.".php");
  ?>
		</div>
<?php /* middle content end*/?>
		
	</div>
</div>
</section>
</body>
</html>
<?php 
	exit;?>
	

