<?php
/*
Template: Default
*/


if(@$_POST['Mail_Target']=="Submit")
{	
  	$MyPostArray = array();
  	$MyPostArray['Mail_ToEmail'] = @$_POST['Mail_ToEmail'];
	$MyPostArray['Mail_BCC'] = DEFINE_BCC_EMAIL;
  	$MyPostArray['Mail_Subject'] = @$_POST['Mail_Subject'];
  	$MyPostArray['Mail_Header'] = "";//@$_POST['Mail_Header'];
  	$MyPostArray['Mail_Footer'] = "";//@$_POST['Mail_Footer'];
  	
  	$MyPostArray['First Name'] = @($_POST['FirstName']);
	$MyPostArray['Last Name'] = @($_POST['LastName']);
	$MyPostArray['Email'] = @($_POST['Email']);
	$MyPostArray['Telephone'] = @($_POST['Telephone']);
   	$MyPostArray['Message'] = @$_POST['Message'];

	
   $ErrorArr = array();
	if(($_SESSION['SecurityCode'] != $_POST['SecurityCode']) OR @$_POST['SecurityCode']== "")
		{
			array_push($ErrorArr,"Invalid Security Code ! Please try again");
			//@ob_clean();
			$_SESSION['ErrorMessage'] = implode($ErrorArr,"<br>");
			//MyRedirect(SKSEOURL($CurrentContent->PageID,"cms"));
		}
		else
		{
	$_SESSION['InfoMessage'] = "Thank you, your message has been sent. We will be in touch with you shortly.";
	$CustomObj->SendMailByUsingTemplate("enquiry.php",$MyPostArray);
	MyRedirect(@$_POST['Mail_Redirect']);
	exit;
 }
}
?>
<section id="MainContent">
	<div class="container"> 
	<?php /* <ul class="breadcrumb">
		<li><a href="<?php echo SKSEOURL("1001","cms")?>">Home</a></li>
		<li class="active">Login</li>
	</ul> */ ?>
	<?php echo ((isset($CurrentContent->LeftTitle1) && $CurrentContent->LeftTitle1 !="")?"<h1>".MyStripSlashes($CurrentContent->LeftTitle1)."</h1>":"");?>
	<?php require_once(DIR_FS_SITE_INCLUDES."message.php");?>
	<div class="row">
		<div class="col-sm-12">
			<?php echo (isset($CurrentContent->LeftDescription1)?MyStripSlashes($CurrentContent->LeftDescription1):"&nbsp;");?>
		</div>
		<div class="col-sm-12">
		   <div class="panel panel-default contact-form">
			<div class="panel-heading">Please fill in the below details:</div>
				<div class="panel-body">
				<form name="frmContact" action="<?php  echo SKSEOURL($CurrentContent->PageID,"cms","Target=Submit")?>" id="frmContact" method="POST" class="form-horizontal" role="form">
					<input type='hidden' name='Mail_ToEmail' value=<?php echo DEFINE_ADMIN_EMAIL?>>
					<input type='hidden' name='Mail_Target' value='Submit'>
					<input type='hidden' name='Mail_Subject' value='Contact Us:Information submitted at <?php echo DEFINE_SITE_NAME?>.'>
					<input type='hidden' name='Mail_Header' value='Thank you for visiting us at <?php echo DEFINE_SITE_NAME?><br>You submitted the following information :<br><br>'>
					<input type='hidden' name='Mail_Footer' value='We shall get in touch with you soon.<br><br><?php echo DEFINE_SITE_NAME?>.'>
					<input type='hidden' name='Mail_Redirect' value='<?php echo SKSEOURL($CurrentContent->PageID,"cms","message=1")?>'>
						
						 
										<div class="form-group">
											<label class="control-label col-sm-3" for="Name">
												<span class="hidden-xs">Name: <b>*</b></span>
												<span class="visible-xs">First Name: <b>*</b></span>
											</label>
											<div class="col-sm-9">
												<div class="row">
													<div class="col-sm-6">
														<div class="input-group">
															<span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span>
															<input type="text" class="form-control" id="FirstName_R" title="Please enter your first name." name="FirstName" placeholder="First Name" value="<?php echo @$_POST['FirstName']?>" required>
														</div>
													</div>
													<div class="col-sm-6">
														<div class="visible-xs" style="margin-top:15px;"><label class="control-label" for="LastName">Last Name: <b>*</b></label></div>
														<div class="input-group">
															<span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span>
															<input type="text" class="form-control" id="LastName_R" name="LastName" title="Please enter your last name." placeholder="Last Name" value="<?php echo @$_POST['LastName']?>" required>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="form-group">
											<label class="control-label col-sm-3" for="email">Email: <b>*</b></label>
											<div class="col-sm-9">
												<div class="input-group">
													<span class="input-group-addon">@</span>
													<input type="email" class="form-control" id="Email_E" name="Email" title="Please enter your email address in valid format." placeholder="e.g. user@domain.com" value="<?php echo @$_POST['Email']?>" required/>
												</div>
											</div>
										</div>
										<div class="form-group">
											<label class="control-label col-sm-3" for="Telephone">Telephone: <b>*</b></label>
											<div class="col-sm-9">
												<div class="input-group">
													<span class="input-group-addon"><span class="glyphicon glyphicon-phone-alt"></span></span>
													<input type="tel" class="form-control" id="Telephone" name="Telephone" title="Please enter your telephone number." placeholder="555-5555-555" value="<?php echo @$_POST['Telephone']?>" required />
												</div>
											</div>
										</div>
										<div class="form-group">
											<label class="control-label col-sm-3 text-right" for="email">Enquiry:</label>
											<div class="col-sm-9">
												<textarea class="form-control" rows="4" id="Message" name="Message" placeholder="message/comments..."><?php echo @$_POST['Message']?></textarea>
											</div>
										</div>
										
										<div class="form-group">
											<label class="control-label col-sm-3 text-right" for="Robot">Security check <br/>
											<a onclick="return ChangeCaptcha(document.getElementById('imgCapcha'),'<?=DIR_WS_SITE_INCLUDES?>classes/class.thumbnail.php?Captcha=1&Time=');" href="#">
												<img src="<?=DIR_WS_SITE_INCLUDES?>classes/class.thumbnail.php?Captcha=1&Time=<?=date('YmdHis')?>" id="imgCapcha">&nbsp;								</a></label>
											<div class="col-sm-9">
												<input type="text" name="SecurityCode" id="SecurityCode_R" title="Please enter Security Code" class="form-control" required/>
											</div>
										</div>
										
										<div class="form-group">
											<div class="col-sm-offset-3 col-sm-9">
												<button type="submit" class="btn btn-default Button">Submit</button>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
				
			
		</div>
	</div>
</div>
	

