
<?php
/*
Template: Left Side Content
*/
?>
<div class="border"></div>
<div id="other_pages">
	<?phprequire_once(DIR_FS_SITE_INCLUDES."category_nav.php");?>  
	<div id="right_other_page2"><?php // right panel start?>  
		<?php echo ((isset($CurrentContent->LeftTitle1) && $CurrentContent->LeftTitle1 !="")?"<div class='heading_text_products'>".MyStripSlashes($CurrentContent->LeftTitle1)."</div>":"");?>
		<div style="clear:both"></div>
		<?php
		if(isset($CurrentContent->UpperBanner1) && $CurrentContent->UpperBanner1 !="")
		{
			$BannerObj = new DataTable(TABLE_BANNERS);
			$BannerObj->Where ="Alignment='H' AND BannerType ='Image' AND Active='1' AND BannerID in (".$CurrentContent->UpperBanner1.")";
			$BannerObj->TableSelectAll("","Position ASC");	
			if($BannerObj->GetNumRows() >0)
			{
				echo '<div id="HomePageBanner" style="height:368px;">';
				while ($CurrentBanner = $BannerObj->GetObjectFromRecord())
				{
					SKDisplayBanner($CurrentBanner);
				}
				echo '</div>
					<div style="clear:both"></div>';
				
			}
		}
		?>
		<?php
          echo (isset($CurrentContent->LeftDescription1)?MyStripSlashes($CurrentContent->LeftDescription1):"&nbsp;");
		if(file_exists(DIR_FS_SITE_CMS_FORMS."form_".$CurrentContent->PageID.".php") && $CurrentContent->PageID !="")
			require_once(DIR_FS_SITE_CMS_FORMS."form_".$CurrentContent->PageID.".php");
          ?>

	</div><?php // right panel end?> 
</div>
<script type="text/javascript">
	 	jQuery(document).ready(function($){
	$('#HomePageBanner').cycle({fx:'fade',speed:3000});
});
</script>
<style type="text/css">
#HomePageBanner{display:inline-block;margin-bottom:10px;}
</style>

