<!--  Header -->
	
	<!-- Top Gray Line -->
	<div class="navbar navbar-fixed-top top-line">
		<div class="container-fluid">
			<!-- Logo -->
			<div class="pull-left">
				<a href="<?php echo DIR_WS_SITE_CONTROL."index.php"?>" class="brand"><?php echo d('ADMIN_TITLE')?> - <?php echo DEFINE_SITE_NAME?></a>
			</div>
			<!-- End Logo -->
			<!-- Top Right Menu -->
			<div class="pull-right">
								<span class="welcome">Welcome <?php echo isset($_SESSION['AdminObj']->UserName)?$_SESSION['AdminObj']->UserName:""?>.</span>
				<div class="toplinks">
					<a href="<?php echo DIR_WS_SITE_CONTROL."index.php?Page=change_password"?>">My account <icon class="icon-lock icon-gray"></icon></a>
				</div>
				<span class="divider-topline"></span>
				<div class="toplinks"> 
					<a href="<?php echo DIR_WS_SITE_CONTROL."index.php?Page=logout"?>">Logout <icon class="icon-share-alt icon-gray"></icon></a>
				</div>
				</div>
			<!-- End Top Right Menu -->
		</div>
	</div>
	<!-- End Top Gray Line -->

		
	<!-- End Header -->
	
<div class="contentWrapper">			
	<div class="container-fluid mainContainerFluid">
		<div class="row-fluid mainMenuWrapper">
		<?php 
		if(isset($_SESSION['AdminObj']) && $_SESSION['AdminObj'] != "")
			require_once(DIR_FS_SITE_CONTROL_INCLUDES."menu.php");
		?>
		<div class="span10 mainContent">
			<div class="inner" id="InsideContent">

				