<div class="span2 mainMenu">
			<ul>
			<?php foreach ($AdminMenuArray as $k=>$v):?>
					<li class="dropdown<?php echo SkCheckMasterMenuItem($Page,$v['link_active']," open active");?>">
						<a href="#menu_<?php echo $k?>" data-toggle="collapse"><?php echo isset($v['name'])?$v['name']:""?> <b class="caret"></b></a>
						<!-- Dropdown Menu -->
						<?php if(isset($v['sub']) && is_array($v['sub']) && count($v['sub']) >0):?>	
						<ul class="collapse<?php echo SkCheckMasterMenuItem($Page,$v['link_active']," in");?>" id="menu_<?php echo $k?>">
							<?php foreach ($v['sub'] as $sk=>$sv):?>
							<?php echo '<li><a href="'.(isset($sv['link'])?$sv['link']:"").'" target="'.(isset($sv['target'])?$sv['target']:"_self").'">'.(isset($sv['name'])?$sv['name']:"").'</a></li>';?>
							<?php endforeach;?>
						</ul> 
						<?php endif;?>
						<!-- End Dropdown Menu -->
					</li>
			<?php endforeach;?>
				
				</ul>
			</div>

<?php 
function SkCheckMasterMenuItem($Page,$link_active_array=array(),$ClassName)
{
	if(isset($link_active_array) && is_array($link_active_array) && in_array($Page,$link_active_array))
		return $ClassName;
	
}
?>