<?php
	define("SKAdminSideInclude",true,true);
	define("DIR_WS_SITE_CONTROL_FORMS", DIR_WS_SITE_CONTROL."forms/",true);
	define("DIR_WS_SITE_CONTROL_ADMINSCRIPT", DIR_WS_SITE_CONTROL."admin_script/",true);
	define("DIR_WS_SITE_CONTROL_ADMINHELP", DIR_WS_SITE_CONTROL."admin_help/",true);
	define("DIR_WS_SITE_CONTROL_IMAGES", DIR_WS_SITE_CONTROL."images/",true);
	define("DIR_WS_SITE_CONTROL_INCLUDES", DIR_WS_SITE_CONTROL."includes/",true);
	define("DIR_WS_SITE_CONTROL_INCLUDES_MENU", DIR_WS_SITE_CONTROL_INCLUDES."menu/",true);
	define("DIR_WS_SITE_CONTROL_THEMES", DIR_WS_SITE_CONTROL."themes/",true);
	define("DIR_FS_SITE_CONTROL_FORMS", DIR_FS_SITE_CONTROL."admin_form/",true);
	define("DIR_FS_SITE_CONTROL_ADMINSCRIPT", DIR_FS_SITE_CONTROL."admin_script/",true);
	define("DIR_FS_SITE_CONTROL_ADMINHELP", DIR_FS_SITE_CONTROL."admin_help/",true);
	define("DIR_FS_SITE_CONTROL_IMAGES", DIR_FS_SITE_CONTROL."images/",true);
	define("DIR_FS_SITE_CONTROL_INCLUDES", DIR_FS_SITE_CONTROL."includes/",true);
	define("DIR_FS_SITE_CONTROL_INCLUDES_MENU", DIR_FS_SITE_CONTROL_INCLUDES."menu/",true);
	define("DIR_FS_SITE_CONTROL_THEMES", DIR_FS_SITE_CONTROL."themes/",true);
	############ Message Declare
	define("USER_LOGIN_THANK_MSG","Welcome to website control panel.",true);
	## Admin Declare
	define("FONT_SIZE","30",true);
	define("FONT_FILE","FRAMDCN.TTF",true);
	define("ANGLE","0",true);
	define("COLOR","#BBBBBB",true);
	define("THEME_STYLE","default",true);	
	define("MENU_LAYOUT","H",true);	
	global $AllowPagesArray;
	$n=0;				 		
	$AdminMenuArray[$n++] = array("name"=>"NAVIGATION",
									 "link"=>"",
									 "width"=>"150",
									  "link_active"=>array("login","whos_online"),
									  "sub"=>array(0=>array("name"=>"Dashboard",
														   "link"=>DIR_WS_SITE_CONTROL."index.php?Page=login",
														    "width"=>"120"
														  ),
												  1=>array("name"=>"Site Home",
														   "link"=>DIR_WS_SITE."index.php",
														    "width"=>"120"
														  ),
												 4=>array("name"=>"Whos Online",
														   "link"=>DIR_WS_SITE_CONTROL."index.php?Page=whos_online",
														    "width"=>"120"
														  ),
												 ),		
									);
		$AdminMenuArray[$n++] = array("name"=>"CATALOG",
								 "link"=>DIR_WS_SITE_CONTROL."index.php?Page=shop/category",
								 "width"=>"120",
								 "link_active"=>array("shop/category","shop/manufacturer","shop/product","shop/product_attribute","shop/filter_attribute","shop/product_import"),
								 "sub"=>array(0=>array("name"=>"View Categories",
													   "link"=>DIR_WS_SITE_CONTROL."index.php?Page=shop/category",
													   "width"=>"120"
													  ),
											/*  1=>array("name"=>"View Manufacturer",
													   "link"=>DIR_WS_SITE_CONTROL."index.php?Page=shop/manufacturer",
													   "width"=>"120"
													  ), */
											  2=>array("name"=>"Add New Product",
														   "link"=>DIR_WS_SITE_CONTROL."index.php?Page=shop/product&Section=AddProduct",
														    "width"=>"120"
														  ),
												 /*4=>array("name"=>"Product Attributes",
														   "link"=>DIR_WS_SITE_CONTROL."index.php?Page=shop/product_attribute",
														    "width"=>"120"
														  ),
											  
											  3=>array("name"=>"Filter Attributes",
														   "link"=>DIR_WS_SITE_CONTROL."index.php?Page=shop/filter_attribute",
														    "width"=>"120"
														  ),		  
											  4=>array("name"=>"Product Import",
														   "link"=>DIR_WS_SITE_CONTROL."index.php?Page=shop/product_import",
														    "width"=>"120"
														  ),	*/	  
											  5=>array("name"=>" Uncategorized Products",
														   "link"=>DIR_WS_SITE_CONTROL."index.php?Page=shop/product",
														    "width"=>"120"
														  ),		  
											  ),
							  );
	$order_sub_menu['All']=array("name"=>"All Orders",
								   "link"=>DIR_WS_SITE_CONTROL."index.php?Page=shop/display_order",
								   "width"=>"120"
								  );						  
	foreach ($OrderStatusArray as $key=>$val)
	{
		$order_sub_menu[$key]=array("name"=>$val,
								   "link"=>DIR_WS_SITE_CONTROL."index.php?Page=shop/display_order&OrderStatus=".$key,
								   "width"=>"120"
								  );
	}	
	$AdminMenuArray[$n++] = array("name"=>"ORDERS",
									 "link"=>DIR_WS_SITE_CONTROL."index.php?Page=shop/display_order&PaymentStatus=Paid",
								   	  "width"=>"120",
									 "link_active"=>array("shop/display_order"),
									 "sub"=>$order_sub_menu,
									);
									
	$UserTypeArray = array();
	$Obj = new DataTable(TABLE_USER_TYPE);
	$Obj->Where="1";
	$Obj->TableSelectAll(array("*"),"SortOrder ASC");
	if($Obj->GetNumRows() > 0)
	{
		while($UserType = $Obj->GetObjectFromRecord())
		{
			$UserTypeArray[$UserType->UserTypeID] = array("name"=>$UserType->UserType,
													   "link"=>DIR_WS_SITE_CONTROL."index.php?Page=account/user&UserTypeID=".$UserType->UserTypeID,
														"width"=>"120"
													);
		}

		$maxTypeNo = $Obj->GetNumRows() + 1;

		$UserTypeArray[$maxTypeNo++] = array("name"=>"Partner Request",
													   "link"=>DIR_WS_SITE_CONTROL."index.php?Page=account/user&Section=DisplayPartnerRequest",
														"width"=>"120"
													);

		$UserTypeArray[$maxTypeNo++] = array("name"=>"Doctor Request",
													   "link"=>DIR_WS_SITE_CONTROL."index.php?Page=account/user&Section=DisplayDoctorRequest",
														"width"=>"120"
													);


		$AdminMenuArray[$n++] = array("name"=>"CUSTOMERS",
								 "link"=>"#",
								 "link_active"=>array("account/user"),
								 "width"=>"120",
								 "sub"=>$UserTypeArray,
								);
	}
	
	
	$AdminMenuArray[$n++] = array("name"=>"CMS PAGES",
									 "link"=>DIR_WS_SITE_CONTROL."index.php?Page=cms",
									 "width"=>"120",
									 "link_active"=>array("cms","addon","cms_area"),
									 "sub"=>array(0=>array("name"=>"Cms Pages",
														   "link"=>DIR_WS_SITE_CONTROL."index.php?Page=cms",
														   "width"=>"120"
														  ),
												  /* 1=>array("name"=>"Cms Pages Contents",
														   "link"=>DIR_WS_SITE_CONTROL."index.php?Page=cms_area",
														   "width"=>"120"
														  ), */
												  2=>array("name"=>"Additional Contents",
														   "link"=>DIR_WS_SITE_CONTROL."index.php?Page=addon",
														   "width"=>"120"
														  ),
												  ),
								  );
	$SettingArray = array();
	$Obj = new DataTable(TABLE_CONF_GROUP);
	$Obj->Where="AdminDisplay='1'";
	$Obj->TableSelectAll(array("*"),"SortOrder ASC");
	if($Obj->GetNumRows() > 0)
	{
		while($Setting = $Obj->GetObjectFromRecord())
		{
			$SettingArray[$Setting->GroupID] = array("name"=>$Setting->GroupName,
													   "link"=>DIR_WS_SITE_CONTROL."index.php?Page=setting&GroupKey=".$Setting->GroupKey,
														"width"=>"120"
													);
		}
	}
	
	if(count($SettingArray) > 0 && false)
		{
		$AdminMenuArray[$n++] = array("name"=>"SETTINGS",
								 "link"=>"#",
								 "link_active"=>array("setting"),
								 "width"=>"120",
								 "sub"=>$SettingArray,
								);
		}
	$AdminMenuArray[$n++] = array("name"=>"TOOLS",
									 "link"=>"#",
									 "link_active"=>array("shipping","shop/tax","currencies","countries","banner","setting","other"),
									 "width"=>"120",
									 "sub"=>array(0=>array("name"=>"Settings",
														   "link"=>DIR_WS_SITE_CONTROL."index.php?Page=setting",
														    "width"=>"120"
														  ),
												 1=>array("name"=>"Banners",
														   "link"=>DIR_WS_SITE_CONTROL."index.php?Page=banner",
														    "width"=>"120"
														  ),
												 2=>array("name"=>"Shipping",
														   "link"=>DIR_WS_SITE_CONTROL."index.php?Page=shipping",
															"width"=>"120"
														),	
												 3=>array("name"=>"Tax",
														   "link"=>DIR_WS_SITE_CONTROL."index.php?Page=shop/tax",
															"width"=>"120"
														),	
												 4=>array("name"=>"Countries",
														   "link"=>DIR_WS_SITE_CONTROL."index.php?Page=countries",
															"width"=>"120"
														),	  
												/*5=>array("name"=>"Download Files",
											   				"link"=>DIR_WS_SITE_CONTROL."index.php?Page=other&Other=download_file",
															"width"=>"120"
											 			 ),
												6=>array("name"=>"Gallery/Media",
											   				"link"=>DIR_WS_SITE_CONTROL."index.php?Page=other&Other=Gallery",
															"width"=>"120"
											 			 ),		*/													 
												  ),
									);	
	/*	
	$AdminMenuArray[$n++] = array("name"=>"TOOLS",
									 "link"=>"#",
									 "link_active"=>array("other","currencies","countries","banner"),
									 "width"=>"120",
									 "sub"=>array(3=>array("name"=>"Help/FAQs",
														   "link"=>DIR_WS_SITE_CONTROL."index.php?Page=other&Other=Faqs",
															"width"=>"120"
														),
												  4=>array("name"=>"Countries",
														   "link"=>DIR_WS_SITE_CONTROL."index.php?Page=countries",
															"width"=>"120"
														),		
											 ),
									);
	$AdminMenuArray[$n++] = array("name"=>"Vouchers/Coupons",
									 "link"=>"#",
									 "width"=>"120",
									 "sub"=>array(0=>array("name"=>"Coupons",
														   "link"=>DIR_WS_SITE_CONTROL."index.php?Page=coupon",
														    "width"=>"120"
														  ),
												 ),
									);
	$AdminMenuArray[$n++] = array("name"=>"TOOLS",
									 "link"=>"#",
									 "width"=>"120",
									 "sub"=>array(1=>array("name"=>"Banners",
														   "link"=>DIR_WS_SITE_CONTROL."index.php?Page=banner",
														    "width"=>"120"
														  ),		  
												3=>array("name"=>"Help/FAQs",
											   "link"=>DIR_WS_SITE_CONTROL."index.php?Page=other&Other=Faqs",
												"width"=>"120"
											  ),
											 ),
									);
	$AdminMenuArray[$n++] = array("name"=>"REPORTS (CSV)",
									 "link"=>"#",
									 "width"=>"120",
									 "sub"=>array(0=>array("name"=>"Order Reports",
														   "link"=>DIR_WS_SITE_CONTROL."index.php?Page=order_report",
														   "width"=>"120"
														  ),
												 ),
									);
	*/
	//require_once(DIR_FS_SITE_CONTROL_THEMES.THEME_STYLE."/color.menu.php");
	/* Access level start*/
	$Page = (isset($_GET['Page']) && $_GET['Page'] !="")?$_GET['Page']:"login";
	//$Page = basename($Page);
	if(!isset($_SESSION['AdminObj']) && $Page !="login")
	{
		@ob_clean();
		$_SESSION['ErrorMessage'] ="Please login to access the website control panel.";
		MyRedirect(DIR_WS_SITE_CONTROL."index.php?Page=login&Return=".base64_encode(str_replace("Target=","",$_SERVER['QUERY_STRING'])));
		exit;
	}
	$AdminPermissionObj = isset($_SESSION['AdminObj'])?$_SESSION['AdminObj']:"";
	$PagePermissionArray = isset($AdminPermissionObj->AllowPages)?explode("@@@",$AdminPermissionObj->AllowPages):array();
	array_push($PagePermissionArray,"login","logout");
	/* Access level end*/
	?>