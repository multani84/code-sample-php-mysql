function AddNewPrdAddition()
{
	
	var PrdAddAddData = '<div  style="padding:20px;" id="DivPrdAtt_'+AddCt+'"><table border="0" cellpadding="3" cellspacing="1" width="100%" class="table table-striped table-bordered table-responsive"><tr class="InsideLeftTd"><td><b>Title</b>: <input type="text" name="AttributeName['+AddCt+']" value="">&nbsp;&nbsp;<a id="AncDeletePrdAtt_'+AddCt+'" href="javascript:;">Delete Attribute</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a id="AncCreatePrdAtt_'+AddCt+'" href="javascript:;" >Add New Value</a></td></tr><tr><td id="TdPrdAtt_'+AddCt+'"></td></tr></table></div>';
	$('#MainAdditionDiv').append(PrdAddAddData);

	$("#AncDeletePrdAtt_"+AddCt).bind("click",function(){
		DeletePrdAddition($(this).attr("id").replace("AncDeletePrdAtt_","DivPrdAtt_"));
	});
	
	$("#AncCreatePrdAtt_"+AddCt).bind("click",function(){
		AddNewPrdAddValue($(this).attr("id").replace("AncCreatePrdAtt_",""));
	});
	
	AddNewPrdAddValue(AddCt,"1");
	AddCt++;
	return false;
}
function DeletePrdAddition(id)
{
	
	if(confirm("Are you sure want to delete this part?"))
	{
		$("#"+id).remove();
	}

	return false;
	
}
function AddNewPrdAddValue(index,WithAtt)
{
	var PrdAddValueData = '<div id="DivPrdAttVal_'+index+'_'+AddCt+'"><table border="0" cellpadding="3" cellspacing="1" width="100%"><tr><td width="240" align="left">Value: <input type="text" name="AttributeValue['+index+']['+AddCt+']" value="" size="30"></td><td width="160" align="left">SKU/Model No: <input type="text" name="ModelNo['+index+']['+AddCt+']" value="" size="10"></td><td width="120" align="left">Price: <input type="text" name="Price['+index+']['+AddCt+']" value="" size="10"></td><td width="50" align="left"><select name="PriceSet['+index+']['+AddCt+']"><option value="+">+</option><option value="-">-</option></select></td><td align="left"><a id="AncPrdAttVal_'+index+'_'+AddCt+'" href="javascript:;">Delete Value</a></td></tr></table></div>';
	$("#TdPrdAtt_"+index).append(PrdAddValueData);
	$("#AncPrdAttVal_"+index+"_"+AddCt).bind("click",function(){
		DeletePrdAddValue($(this).attr("id").replace("AncPrdAttVal_","DivPrdAttVal_"));
	});
	
	if(WithAtt =="1")
		return PrdAddValueData;
	else
		AddCt++;
		
	return false;
}

function DeletePrdAddValue(id)
{
	DeletePrdAddition(id);
	return false;
}
