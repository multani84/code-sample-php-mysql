<?php 
	$PageTitle 			= isset($PageTitle) ? $PageTitle:".:: ".DEFINE_SITE_NAME." Control Panel ::.";
	$PageDescription 	= isset($PageDescription) ? $PageDescription:"";
	$PageKeywords		= isset($PageKeywords) ? $PageKeywords:"";		
?>
<!DOCTYPE html>
<html>
<head>
	<title><?php echo $PageTitle?></title>
	<meta NAME="DESCRIPTION" CONTENT="<?php echo $PageDescription?>">
	<meta name="keywords" content="<?php echo $PageKeywords?>">

	<!-- Bootstrap -->
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="<?php echo DIR_WS_SITE_CONTROL?>site_resources/css/bootstrap.min.css" type="text/css" />
	
	<link href="<?php echo DIR_WS_SITE_CONTROL?>site_resources/css/wc.min.css" rel="stylesheet">
	<link href="<?php echo DIR_WS_SITE_CONTROL?>site_resources/css/wc-responsive.min.css" rel="stylesheet">
	<!-- wysihtml5 -->
	<link href="<?php echo DIR_WS_SITE_CONTROL?>site_resources/extend/bootstrap-wysihtml5/css/bootstrap-wysihtml5-0.0.2.css" rel="stylesheet">
	<!-- Theme :: Beauty Admin  -->
	<link href="<?php echo DIR_WS_SITE_CONTROL_THEMES.THEME_STYLE?>/css/theme.css?1359456325" rel="stylesheet">
	<!--  Google Open Sans Font -->
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
	<!-- Jquery Latest -->
	<script src="<?php echo DIR_WS_SITE_CONTROL_THEMES.THEME_STYLE?>/scripts/jquery-1.8.0.min.js" type="text/javascript"></script>
	<!-- Glyphicons -->
	<link rel="stylesheet" href="<?php echo DIR_WS_SITE_CONTROL_THEMES.THEME_STYLE?>/css/glyphicons.css" />
	<!-- Cookies -->
	<script src="<?php echo DIR_WS_SITE_CONTROL_THEMES.THEME_STYLE?>/scripts/jquery.cookie.js"></script>
	<!-- Bootstrap JS -->
	<script src="<?php echo DIR_WS_SITE_CONTROL?>site_resources/js/bootstrap.min.js" type="text/javascript"></script>
	<!-- Resize Script -->
	<script src="<?php echo DIR_WS_SITE_CONTROL_THEMES.THEME_STYLE?>/scripts/jquery.ba-resize.js"></script>
	<!-- Bootstrap Extended -->
	
	<!-- jasny plugins -->
	<script src="<?php echo DIR_WS_SITE_CONTROL?>site_resources/extend//jasny-bootstrap1/js/jasny-bootstrap.min.js" type="text/javascript"></script>
	<script src="<?php echo DIR_WS_SITE_CONTROL?>site_resources/extend//jasny-bootstrap1/js/bootstrap-fileupload.js" type="text/javascript"></script>
	<!-- bootbox -->
	<script src="<?php echo DIR_WS_SITE_CONTROL?>site_resources/extend/bootbox.js" type="text/javascript"></script>
	<!-- wysihtml5 -->
	<script src="<?php echo DIR_WS_SITE_CONTROL?>site_resources/extend/bootstrap-wysihtml5/js/wysihtml5-0.3.0_rc2.min.js" type="text/javascript"></script>
	<script src="<?php echo DIR_WS_SITE_CONTROL?>site_resources/extend/bootstrap-wysihtml5/js/bootstrap-wysihtml5-0.0.2.min.js" type="text/javascript"></script>
	<!-- General script -->
	<script src="<?php echo DIR_WS_SITE_CONTROL_THEMES.THEME_STYLE?>/scripts/load.js?1359456319"></script>
	
	<!-- Extra css start--->
	<?php echo $SKEditorObj->HeadScript();?>
		
		<link rel="stylesheet" type="text/css" media="all" href="<?php echo DIR_WS_SITE_INCLUDES_JAVASCRIPT?>calendar/calendar-win2k-cold-1.css">
		<script type="text/javascript" src="<?php echo DIR_WS_SITE_INCLUDES_JAVASCRIPT?>swfobject.js"></script>
		<script type="text/javascript" src="<?php echo DIR_WS_SITE_INCLUDES_JAVASCRIPT?>calendar/calendar.js"></script>
		<script type="text/javascript" src="<?php echo DIR_WS_SITE_INCLUDES_JAVASCRIPT?>calendar/lang/calendar-en.js"></script>
		<script type="text/javascript" src="<?php echo DIR_WS_SITE_INCLUDES_JAVASCRIPT?>calendar/calendar-setup.js"></script>
		<?php echo $SKEditorObj->HeadScript();?>
		<script type="text/javascript" src='<?php echo DIR_WS_SITE_INCLUDES_JAVASCRIPT?>FormValidations.js'></script>
		<script type="text/javascript" src='<?php echo DIR_WS_SITE_INCLUDES_JAVASCRIPT?>NumberFormat.js'></script>
		
		<link type="text/css" href="<?=DIR_WS_SITE_INCLUDES_JAVASCRIPT?>jquery/themes/base/ui.all.css" rel="stylesheet" />
		<link type="text/css" href="<?=DIR_WS_SITE_INCLUDES_JAVASCRIPT?>jquery/themes/redmond/jquery-ui-1.8.custom.css" rel="stylesheet" />
		<script type="text/javascript" src="<?=DIR_WS_SITE_INCLUDES_JAVASCRIPT?>jquery/ui/jquery-ui-1.7.2.custom.js"></script>
		
		
		<link rel="stylesheet" type="text/css" media="all" href="<?php echo DIR_WS_SITE_INCLUDES_JAVASCRIPT?>jquery/chosen/chosen.css">
		<script type="text/javascript" src="<?php echo DIR_WS_SITE_INCLUDES_JAVASCRIPT?>jquery/chosen/chosen.jquery.min.js"></script>
		
		<script type="text/javascript" src="<?php echo DIR_WS_SITE_INCLUDES_JAVASCRIPT?>jquery/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
		<link rel="stylesheet" href="<?php echo DIR_WS_SITE_INCLUDES_JAVASCRIPT?>jquery/fancybox/jquery.fancybox-1.3.4.css" type="text/css" media="screen" />
		<link rel="stylesheet" type="text/css" href="<?php echo DIR_WS_SITE_CONTROL?>custom.css">
	<!-- Extra css end--->
	
</head>
