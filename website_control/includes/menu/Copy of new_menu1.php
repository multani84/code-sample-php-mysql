<script>
function cdd_menu49613(){//////////////////////////Start Menu Data/////////////////////////////////

//**** NavStudio, (c) 2004, OpenCube Inc.,  -  www.opencube.com ****
//Note: This data file may be manually customized outside of the visual interface.

    //Unique Menu Id
	this.uid = 49613


/**********************************************************************************************

                               Icon Images

**********************************************************************************************/


    //Inline positioned icon images (flow with the item text)

	this.rel_icon_image0 = "<?php echo DIR_WS_SITE_CONTROL_IMAGES?>arrow.gif"
	this.rel_icon_rollover0 = "<?php echo DIR_WS_SITE_CONTROL_IMAGES?>arrow-hover.gif"
	this.rel_icon_image_wh0 = "10,6"

	this.rel_icon_image1 = "<?php echo DIR_WS_SITE_CONTROL_IMAGES?>circle_2.gif"
	this.rel_icon_rollover1 = "<?php echo DIR_WS_SITE_CONTROL_IMAGES?>circle_2.gif"
	this.rel_icon_image_wh1 = "7,6"




/**********************************************************************************************

                              Global - Menu Container Settings

**********************************************************************************************/


	this.menu_background_color = "<?php echo COLOR_MENU_BGCOLOR?>"
	this.menu_border_color = "<?php echo COLOR_MENU_BORDER?>"
	this.menu_border_width = 1
	this.menu_padding = "2,5,2,4"
	this.menu_border_style = "solid"
	this.divider_caps = false
	this.divider_width = 1
	this.divider_height = 1
	this.divider_background_color = "<?php echo COLOR_MENU_DIVIDER_BGCOLOR?>"
	this.divider_class = "menu"
	this.divider_border_style = "none"
	this.divider_border_width = 0
	this.divider_border_color = "<?php echo COLOR_MENU_DIVIDER_BORDER?>"
	this.menu_is_horizontal = false
	this.menu_width = "200"
	this.menu_xy = "-100,2"




/**********************************************************************************************

                              Global - Menu Item Settings

**********************************************************************************************/


	this.icon_rel = 0
	this.menu_items_background_color_roll = "<?php echo COLOR_MENU_BGCOLOR_HOVER?>"
	this.menu_items_text_color = "<?php echo COLOR_MENU_TEXT?>"
	this.menu_items_text_decoration = "none"
	this.menu_items_font_family = "Verdana"
	this.menu_items_font_size = "10px"
	this.menu_items_font_style = "normal"
	this.menu_items_font_weight = "bold"
	this.menu_items_text_align = "left"
	this.menu_items_padding = "4,5,4,5"
	this.menu_items_border_style = "solid"
	this.menu_items_border_color = "<?php echo COLOR_MENU_TEXT_BORDER?>"
	this.menu_items_border_width = 0
	this.menu_items_width = "183"
	this.menu_items_text_color_roll = "<?php echo COLOR_MENU_TEXT_HOVER?>"




/**********************************************************************************************

                              Main Menu Settings

**********************************************************************************************/


        this.menu_padding_main = "2,5,2,5"
        this.menu_items_width_main = 114
        this.menu_is_horizontal_main = true
        
/****************** Menu1 Start**********************************************/
       ///// Main Menu 1
        this.item0 = "CONTROLLERS"
		this.item_width1 = 122
        this.url0 = "<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=controller"
/****************** Menu1 End**********************************************/
       
/****************** Menu2 Start**********************************************/
       ///// Main Menu 2        
        this.item1 = "ORDERS"
        this.url1 = "#"
       
        
	        //Sub Menu 1
	        this.menu_xy1 = "-109,2"
	        this.menu_width1 = 165
	
	        this.item1_0 = "ORDERS"	
	            //Sub Menu 1_0
	
	                this.menu_xy1_0 = "-7,-16"
	                this.menu_width1_0 = 225
	
	                this.item1_0_0 = "PAID ONLINE ORDERS"
	                this.item1_0_1 = "ATTEMPTED ONLINE ORDERS"
	                this.item1_0_2 = "ALL ONLINE ORDERS"
	                this.item1_0_3 = "PAID MANUAL ORDERS"
	                this.item1_0_4 = "UNPAID MANUAL ORDERS"
	                this.item1_0_5 = "ALL MANUAL ORDERS"
	                this.item1_0_6 = "NEW MANUAL ORDERS"
	               
	                this.url1_0_0 = "<?php echo DIR_WS_SITE_CONTROL?>index.php"
	                this.url1_0_1 = "<?php echo DIR_WS_SITE_CONTROL?>index.php"
	                this.url1_0_2 = "<?php echo DIR_WS_SITE_CONTROL?>index.php"
	                this.url1_0_3 = "<?php echo DIR_WS_SITE_CONTROL?>index.php"
	                this.url1_0_4 = "<?php echo DIR_WS_SITE_CONTROL?>index.php"
	                this.url1_0_5 = "<?php echo DIR_WS_SITE_CONTROL?>index.php"
	                this.url1_0_6 = "<?php echo DIR_WS_SITE_CONTROL?>index.php"
	               
	                

       this.item1_1 = "SAMPLES & ENQUIRIES"
	   this.item1_2 = "FREE CARDS"

       this.url1_0 = "#"
       this.url1_1 = "<?php echo DIR_WS_SITE_CONTROL?>samples-n-enquiries.php"
	   this.url1_2 = "#"


      
	                
/****************** Menu2 End**********************************************/


/****************** Menu3 Start**********************************************/        
       ///// Main Menu 3                
        this.item2 = "NEWSLETTERS"
        this.url2 = "<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=Newsletter"
       

/****************** Menu3 End**********************************************/        
        
/****************** Menu4 Start**********************************************/        
        ///// Main Menu 4 
        this.item3 = "REPORTS"
        this.item_width3 = 119
        this.url3 = "#"
        
         //Sub Menu 3

       	 this.menu_xy3 = "-104,2"
       	 this.menu_width3 = 187

      	   this.item3_0 = "PAYMENTS"
    	   this.url3_0 = "<?php echo DIR_WS_SITE_CONTROL?>payments.php"
	       
    	   this.item3_1 = "REPEAT ORDER PROPOSAL"
		   this.url3_1 = "#"
		   
		   this.item3_2 = "PROOFS"
      	   this.url3_2 = "<?php echo DIR_WS_SITE_CONTROL?>proof.php"
	       
      	   this.item3_3 = "PRODUCTS"
     	   this.url3_3 = "<?php echo DIR_WS_SITE_CONTROL?>sales-product.php"
	       
	       this.item3_4 = "SALES"
    	   this.url3_4 = "<?php echo DIR_WS_SITE_CONTROL?>sales-week.php"
	       
	       this.item3_5 = "PROFIT & LOSS"
		   this.url3_5 = "<?php echo DIR_WS_SITE_CONTROL?>profit-loss.php"

	   	   

/****************** Menu4 End**********************************************/        
       
        
/****************** Menu5 End**********************************************/        
		///// Main Menu 5
        this.item4 = "ADMIN"
        this.item_width4 = 118
        this.url4 = "#"
        
         //Sub Menu 4
	
   		  this.menu_xy4 = "-99,2"
          this.menu_width4 = 145

          this.item4_0 = "CONFIGURATION"        
     	  this.url4_0 = "<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=Configure"
		 
		 
          this.item4_1 = "CATEGORIES"        
     	  this.url4_1 = "<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=Category"
		 
		  this.item4_2 = "PRICES"        
		  this.url4_2 = "<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=Price"
      
  		  this.item4_3 = "SUPPLIERS"        
		  this.url4_3 = "<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=Supplier"

		  this.item4_4 = "CHARITIES"        
		  this.url4_4 = "<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=Charity"
        
		  this.item4_5 = "GREETINGS"        
		  this.url4_5 = "<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=Greeting"
        
		  this.item4_6 = "TYPEFACES"        
		  this.url4_6 = "<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=Typeface"
        
		  this.item4_7 = "SHIPPING"        
		  this.url4_7 = "<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=Shipping"
        
		  this.item4_8 = "COUPONS"        
		  this.url4_8 = "<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=Coupon"

		  this.item4_9 = "JOKE"        
		  this.url4_9 = "<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=Joke"

		  this.item4_10 = "FILE CONTENTS"        
		  this.url4_10 = "<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=FileContent"
        
		  this.item4_11 = "IMAGE MANAGER"        
		  this.url4_11 = "<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=ImageManager"

		  this.item4_12 = "MESSAGES"        
		  this.url4_12 = "<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=Messages"

		 

/****************** Menu5 End**********************************************/        

/****************** Menu6 Start**********************************************/        
        ///// Main Menu 6
        this.item5 = "REGISTRATION DETAILS"
        this.item_width5 = 172
        this.url5 = "<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=User"
/****************** Menu6 End**********************************************/        

/****************** Menu7 Start**********************************************/        
        ///// Main Menu 7
        this.item6 = "AGENTS"
        this.item_width6 = 114
        this.url6 = "<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=Agent"
/****************** Menu7 End**********************************************/               
        
/****************** Menu8 Start**********************************************/        
        ///// Main Menu 8
        this.item7 = "LOGOUT"
        this.item_width7 = 112
        this.url7 = "<?php echo DIR_WS_SITE_CONTROL?>index.php"
/****************** Menu8 End**********************************************/               
        
       



/**********************************************************************************************

                              Sub Menu Settings

**********************************************************************************************/


    //Sub Menu 0




    

    

   

         


    //Sub Menu 5




    //Sub Menu 6




    //Sub Menu 7




}///////////////////////// END Menu Data /////////////////////////////////////////



//Document Level Settings

cdd__activate_onclick = false
cdd__showhide_delay = 250
cdd__url_target = "_self"
cdd__url_features = "resizable=1, scrollbars=1, titlebar=1, menubar=1, toolbar=1, location=1, status=1, directories=1, channelmode=0, fullscreen=0"
cdd__display_urls_in_status_bar = true
cdd__default_cursor = "hand"


//NavStudio Code (Warning: Do Not Alter!)

if (window.showHelp){b_type = "ie"; if (!window.attachEvent) b_type += "mac";}if (document.createElementNS) b_type = "dom";if (navigator.userAgent.indexOf("afari")>-1) b_type = "safari";if (window.opera) b_type = "opera"; qmap1 = "\<\script language=\"JavaScript\" vqptag='loader_sub' src=\""; qmap2 = ".js\">\<\/script\>";;function iesf(){};;function vqp_error(val){}
if (b_type){document.write(qmap1+cdd__codebase+"pbrowser_"+b_type+qmap2);document.close();}
</script>
