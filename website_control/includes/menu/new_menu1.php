<script>
function cdd_menu49613(){//////////////////////////Start Menu Data/////////////////////////////////

//**** NavStudio, (c) 2004, OpenCube Inc.,  -  www.opencube.com ****
//Note: This data file may be manually customized outside of the visual interface.

    //Unique Menu Id
	this.uid = 49613


/**********************************************************************************************

                               Icon Images

**********************************************************************************************/


    //Inline positioned icon images (flow with the item text)

	this.rel_icon_image0 = "<?php echo DIR_WS_SITE_CONTROL_IMAGES?>arrow.gif"
	this.rel_icon_rollover0 = "<?php echo DIR_WS_SITE_CONTROL_IMAGES?>arrow-hover.gif"
	this.rel_icon_image_wh0 = "10,6"

	this.rel_icon_image1 = "<?php echo DIR_WS_SITE_CONTROL_IMAGES?>circle_2.gif"
	this.rel_icon_rollover1 = "<?php echo DIR_WS_SITE_CONTROL_IMAGES?>circle_2.gif"
	this.rel_icon_image_wh1 = "7,6"




/**********************************************************************************************

                              Global - Menu Container Settings

**********************************************************************************************/


	this.menu_background_color = "<?php echo COLOR_MENU_BGCOLOR?>"
	this.menu_border_color = "<?php echo COLOR_MENU_BORDER?>"
	this.menu_border_width = 1
	this.menu_padding = "2,5,2,4"
	this.menu_border_style = "solid"
	this.divider_caps = false
	this.divider_width = 1
	this.divider_height = 1
	this.divider_background_color = "<?php echo COLOR_MENU_DIVIDER_BGCOLOR?>"
	this.divider_class = "menu"
	this.divider_border_style = "none"
	this.divider_border_width = 0
	this.divider_border_color = "<?php echo COLOR_MENU_DIVIDER_BORDER?>"
	this.menu_is_horizontal = false
	this.menu_width = "265"
	this.menu_xy = "-142,2"




/**********************************************************************************************

                              Global - Menu Item Settings

**********************************************************************************************/


	this.icon_rel = 0
	this.menu_items_background_color_roll = "<?php echo COLOR_MENU_BGCOLOR_HOVER?>"
	this.menu_items_text_color = "<?php echo COLOR_MENU_TEXT?>"
	this.menu_items_text_decoration = "none"
	this.menu_items_font_family = "Verdana"
	this.menu_items_font_size = "10px"
	this.menu_items_font_style = "normal"
	this.menu_items_font_weight = "bold"
	this.menu_items_text_align = "left"
	this.menu_items_padding = "4,5,4,5"
	this.menu_items_border_style = "solid"
	this.menu_items_border_color = "<?php echo COLOR_MENU_TEXT_BORDER?>"
	this.menu_items_border_width = 0
	this.menu_items_width = "183"
	this.menu_items_text_color_roll = "<?php echo COLOR_MENU_TEXT_HOVER?>"




/**********************************************************************************************

                              Main Menu Settings

**********************************************************************************************/



        this.menu_padding_main = "2,5,2,5"
        this.menu_items_width_main = 140
        this.menu_is_horizontal_main = true
        
/****************** Menu1 Start**********************************************/

<?php
foreach ($AdminMenuArray as $k=>$v)
  {
  	
  	?>
  	 this.item<?php echo $k?> = "<?php echo isset($v['name'])?$v['name']:""?>";
     this.url<?php echo $k?> = "<?php echo isset($v['link'])?$v['link']:""?>";
     this.item_width<?php echo $k?> = <?php echo isset($v['width'])?$v['width']:""?>;
     this.url_target<?php echo $k?> = "<?php echo isset($v['target'])?$v['target']:"_self"?>";
  	<?php
  	if(isset($v['sub']) && is_array($v['sub']) && count($v['sub']) >0)
  	{
  		foreach ($v['sub'] as $sk=>$sv)
  		{
  		?>
	  	 this.item<?php echo $k?>_<?php echo $sk?> = "<?php echo isset($sv['name'])?$sv['name']:""?>";
	     this.url<?php echo $k?>_<?php echo $sk?> = "<?php echo isset($sv['link'])?$sv['link']:""?>";
	     this.item_width<?php echo $k?>_<?php echo $sk?> = <?php echo isset($sv['width'])?$sv['width']:""?>;
     	 this.url_target<?php echo $k?>_<?php echo $sk?> = "<?php echo isset($sv['target'])?$sv['target']:"_self"?>";
  		<?php
  		}
  	}
  }
?>

}///////////////////////// END Menu Data /////////////////////////////////////////



//Document Level Settings

cdd__activate_onclick = false
cdd__showhide_delay = 140
cdd__url_target = "_self"
cdd__url_features = "resizable=1, scrollbars=1, titlebar=1, menubar=1, toolbar=1, location=1, status=1, directories=1, channelmode=0, fullscreen=0"
cdd__display_urls_in_status_bar = true
cdd__default_cursor = "hand"


//NavStudio Code (Warning: Do Not Alter!)

if (window.showHelp){b_type = "ie"; if (!window.attachEvent) b_type += "mac";}if (document.createElementNS) b_type = "dom";if (navigator.userAgent.indexOf("afari")>-1) b_type = "safari";if (window.opera) b_type = "opera"; qmap1 = "\<\script language=\"JavaScript\" vqptag='loader_sub' src=\""; qmap2 = ".js\">\<\/script\>";;function iesf(){};;function vqp_error(val){}
if (b_type){document.write(qmap1+cdd__codebase+"pbrowser_"+b_type+qmap2);document.close();}
</script>
