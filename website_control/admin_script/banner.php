<?php
$BannerObj = new DataTable(TABLE_BANNERS);
$DataArray = array();

$Section = isset($_GET['Section'])?$_GET['Section']:"";
$Target = isset($_GET['Target'])?$_GET['Target']:"";
$Alignment = isset($_GET['Alignment'])?$_GET['Alignment']:'H';
$BannerID = isset($_GET['BannerID'])?$_GET['BannerID']:0;

$Label = "";
$Label = $Alignment=='H'?" Home Slider ":$Label;	
$Label = $Alignment=='S'?"HomePage Static ":$Label;	
$Label = $Alignment=='BR'?"Brands ":$Label;	
	
	if($BannerID !=0)
	{
		$BannerObj->Where = "BannerID='".$BannerObj->MysqlEscapeString($BannerID)."'";
		$CurrentBanner = $BannerObj->TableSelectOne();
	}	
/// Target  start 
switch ($Target)
{
	case "DeleteBanner":
			@unlink(DIR_FS_SITE_UPLOADS_BANNER.$CurrentBanner->Content);
			$BannerObj->TableDelete();								
			@ob_clean();
			
			$_SESSION['InfoMessage'] ="Banner deleted successfully.";
			MyRedirect(DIR_WS_SITE_CONTROL."index.php?Page=$Page&Section=$Section&Alignment=".$Alignment."");
			exit;
		
	break;	
	case "UpdateBanner":
		for ($i=1;$i <$_POST['Count'];$i++)
		{
			$BannerID =$_POST['BannerID_'.$i];
			$DataArray = array();
			$DataArray['Active'] = isset($_POST['Active_'.$i])?$_POST['Active_'.$i]:0;
			$DataArray['Position'] = isset($_POST['Position_'.$i])?$_POST['Position_'.$i]:0;
		
			$BannerObj->Where ="BannerID='".$BannerObj->MysqlEscapeString($BannerID)."'";
			$BannerObj->TableUpdate($DataArray);
		}
		ob_clean();
		
		$_SESSION['InfoMessage'] ="Banner updated successfully.";
		MyRedirect(DIR_WS_SITE_CONTROL."index.php?Page=$Page&Section=$Section&Alignment=".$Alignment."");
		exit;
		
	break;

		case "AddBanner":
			
			$DataArray['BannerName'] = isset($_POST['BannerName'])?$_POST['BannerName']:"";
			$DataArray['Parameters'] = isset($_POST['Parameters'])?$_POST['Parameters']:"";
			$DataArray['Alignment'] =$Alignment;
			$DataArray['URL'] =isset($_POST['URL'])?$_POST['URL']:"";
			$DataArray['URLType'] =isset($_POST['URLType'])?$_POST['URLType']:"";
			$DataArray['BannerType'] =isset($_POST['BannerType'])?$_POST['BannerType']:"";
			$DataArray['FlashWidth'] =@$_POST['FlashWidth']>0?(int)$_POST['FlashWidth']:"0";
			$DataArray['FlashHeight'] =@$_POST['FlashHeight']>0?(int)$_POST['FlashHeight']:"0";
			$DataArray['Active'] = isset($_POST['Active'])?$_POST['Active']:0;
			
			if($BannerID==0)
			{
				$BannerID = $BannerObj->GetMax("BannerID") + 1;
				$BannerObj->Where ="Alignment='".$Alignment."'";
				$Position = $BannerObj->GetMax("Position") + 1;
			
				
				$DataArray['BannerID'] = $BannerID;
				$DataArray['Position'] = $Position;
			
				if($DataArray['BannerType']=="Image" && isset($_FILES['Upload']) && $_FILES['Upload']['name'] !="")
				{
					$ArrayType = explode(".",$_FILES['Upload']['name']);
					$Type=$ArrayType[count($ArrayType)-1];
					$ImageName = uniqid("Image".$BannerID."_").".".$Type;
					
					$OriginalFileName = DIR_FS_SITE_UPLOADS_BANNER.$ImageName;
					copy($_FILES['Upload']['tmp_name'],$OriginalFileName);
					$DataArray['Content'] = $ImageName;
				}
				
				if($DataArray['BannerType']=="Flash" && isset($_FILES['FlashUpload']) && $_FILES['FlashUpload']['name'] !="")
				{
					$ArrayType = explode(".",$_FILES['FlashUpload']['name']);
					$Type=$ArrayType[count($ArrayType)-1];
					$ImageName = uniqid("Flash".$BannerID."_").".".$Type;
					
					$OriginalFileName = DIR_FS_SITE_UPLOADS_BANNER.$ImageName;
					copy($_FILES['FlashUpload']['tmp_name'],$OriginalFileName);
					$DataArray['Content'] = $ImageName;
				}
				
				
				if($DataArray['BannerType']=="HTML")
				{
					$DataArray['Content'] = isset($_POST['HTMLContent'])?$_POST['HTMLContent']:"";
				}
				
				
				$BannerObj->TableInsert($DataArray);	
				@ob_clean();
				
				$_SESSION['InfoMessage'] ="Banner added successfully.";
			}
			else 
			{
							
				if($DataArray['BannerType']=="Image" && isset($_FILES['Upload']) && $_FILES['Upload']['name'] !="")
				{
					$ArrayType = explode(".",$_FILES['Upload']['name']);
					$Type=$ArrayType[count($ArrayType)-1];
					$ImageName = uniqid("Image".$BannerID."_").".".$Type;
					
					$OriginalFileName = DIR_FS_SITE_UPLOADS_BANNER.$ImageName;
					copy($_FILES['Upload']['tmp_name'],$OriginalFileName);
					$DataArray['Content'] = $ImageName;
				}
				
				if($DataArray['BannerType']=="Flash" && isset($_FILES['FlashUpload']) && $_FILES['FlashUpload']['name'] !="")
				{
					$ArrayType = explode(".",$_FILES['FlashUpload']['name']);
					$Type=$ArrayType[count($ArrayType)-1];
					$ImageName = uniqid("Flash".$BannerID."_").".".$Type;
					
					$OriginalFileName = DIR_FS_SITE_UPLOADS_BANNER.$ImageName;
					copy($_FILES['FlashUpload']['tmp_name'],$OriginalFileName);
					$DataArray['Content'] = $ImageName;
				}
				
				if($DataArray['BannerType']=="HTML")
				{
					$DataArray['Content'] = isset($_POST['HTMLContent'])?$_POST['HTMLContent']:"";
				}
				
				$BannerObj->TableUpdate($DataArray);		
				ob_clean();
				
				$_SESSION['InfoMessage'] ="Banner updated successfully.";
				
			}
		
			MyRedirect(DIR_WS_SITE_CONTROL."index.php?Page=$Page&Alignment=$Alignment&BannerID=$BannerID");
			exit;

	break;

}
