<?php
$Section = isset($_GET['Section'])?$_GET['Section']:"";
$Target = isset($_GET['Target'])?$_GET['Target']:"";
$CouponID = isset($_GET['CouponID'])?$_GET['CouponID']:0;
$CouponObj = new DataTable(TABLE_COUPONS);

/// Target  start 
switch ($Target)
{

	case "SendCoupon":
			$Sender = isset($_POST['Sender'])?$_POST['Sender']:"";
			$Subject = isset($_POST['Subject'])?$_POST['Subject']:"";
			$Description = isset($_POST['txtContent'])?$_POST['txtContent']:"";
			$TmpMail =explode(",",str_replace("\n","",$Sender));
			$Count =0;
			foreach ($TmpMail as $Value)
			{
				if($Value !="")
				{
					SendEmail($Subject,$Value,DEFINE_ADMIN_EMAIL,DEFINE_SITE_NAME,$Description);
					$Count++;
				}
				
			}							
			ob_clean();
								
			$_SESSION['InfoMessage'] ="Mail has been sent to $Count persons successfully.";
			MyRedirect(DIR_WS_SITE_CONTROL."index.php?Page=$Page");
			exit;
		
	break;	
	case "DeleteCoupon":
			$CouponObj->Where ="CouponID='".$CouponObj->MysqlEscapeString($CouponID)."'";
			$CouponObj->TableDelete();
			
			ob_clean();
								
			$_SESSION['InfoMessage'] ="Coupon deleted successfully.";
			MyRedirect(DIR_WS_SITE_CONTROL."index.php?Page=$Page&Section=$Section");
			exit;
		
	break;	
	case "AddCoupon":
		
		$DataArray = array();


	$DataArray['CouponName'] = isset($_POST['CouponName'])?$_POST['CouponName']:"";
	$DataArray['CouponType'] = isset($_POST['CouponType'])?$_POST['CouponType']:"";
	$DataArray['CouponCode'] = isset($_POST['CouponCode'])?$_POST['CouponCode']:"";
	$DataArray['Active'] = isset($_POST['Active'])?$_POST['Active']:"0";
	$DataArray['CouponValue'] = (isset($_POST['CouponValue']) &&  $_POST['CouponValue'] !="")?$_POST['CouponValue']:"0";
	$DataArray['StartDate'] = (isset($_POST['StartDate']) && $_POST['StartDate'] !="")?$_POST['StartDate']:"0000-00-00";
	$DataArray['EndDate'] = (isset($_POST['EndDate']) && $_POST['EndDate'] != "")?$_POST['EndDate']:"0000-00-00";
	$DataArray['Message'] = isset($_POST['Message'])?$_POST['Message']:"";
	$DataArray['CouponLimit'] = isset($_POST['CouponLimit'])?(int)$_POST['CouponLimit']:"0";
	$DataArray['MinTotal'] = isset($_POST['MinTotal'])?(int)$_POST['MinTotal']:"0";
	$DataArray['ProductSpecific'] = isset($_POST['ProductSpecific'])?$_POST['ProductSpecific']:"0";
	$DataArray['ProductIDs'] = isset($_POST['ProductIDs'])?implode(",",$_POST['ProductIDs']):"0";
	
	if($CouponID==0)
	{
		$DataArray['CreatedDate'] = date('Y-m-d');
		$CouponObj->TableInsert($DataArray);
		@ob_clean();
							
		$_SESSION['InfoMessage'] ="Coupon added successfully.";
		MyRedirect(DIR_WS_SITE_CONTROL."index.php?Page=$Page&CouponID=$CouponID");
		exit;
	}
	else 
	{
			$CouponObj->Where ="CouponID='".$CouponObj->MysqlEscapeString($CouponID)."'";
			$CouponObj->TableUpdate($DataArray);
			@ob_clean();
								
			$_SESSION['InfoMessage'] ="Coupon updated successfully.";
			MyRedirect(DIR_WS_SITE_CONTROL."index.php?Page=$Page&CouponID=$CouponID");
			exit;		
	}	
	break;
	
	case "UpdateCoupon":
		for ($i=1;$i <$_POST['Count'];$i++)
		{
			$CouponID =$_POST['CouponID_'.$i];
			$Active = isset($_POST['Active_'.$i])?$_POST['Active_'.$i]:0;
			$CouponObj->Active = $Active;
			
			$DataArray = array();
			$DataArray['Active'] = isset($_POST['Active_'.$i])?$_POST['Active_'.$i]:0;
			$CouponObj->Where ="CouponID='".$CouponObj->MysqlEscapeString($CouponID)."'";
			$CouponObj->TableUpdate($DataArray);
		}
		ob_clean();
		
			$_SESSION['InfoMessage'] ="Coupon updated successfully.";
		MyRedirect(DIR_WS_SITE_CONTROL."index.php?Page=$Page&Section=$Section");
		exit;		
	break;
}

//// target end
