<?php
$ImageObj = new DataTable(TABLE_IMAGES);
$DataArray = array();

$Section = isset($_GET['Section'])?$_GET['Section']:"";
$Target = isset($_GET['Target'])?$_GET['Target']:"";
$ImageID = isset($_GET['ImageID'])?$_GET['ImageID']:0;


	if($ImageID !=0)
	{
		$ImageObj->Where = "ImageID='".$ImageObj->MysqlEscapeString($ImageID)."'";
		$CurrentImage = $ImageObj->TableSelectOne();
	}	
/// Target  start 
switch ($Target)
{
	case "DeleteImage":
			@unlink(DIR_FS_SITE_UPLOADS_OTHER.$CurrentImage->Content);
			$ImageObj->TableDelete();								
			@ob_clean();
			$_SESSION['InfoMessage'] ="Image deleted successfully.";
			MyRedirect(DIR_WS_SITE_CONTROL."index.php?Page=$Page&Section=$Section");
			exit;
		
	break;	
	case "UpdateImage":
		for ($i=1;$i <$_POST['Count'];$i++)
		{
			$ImageID =$_POST['ImageID_'.$i];
			$DataArray = array();
			$DataArray['Active'] = isset($_POST['Active_'.$i])?$_POST['Active_'.$i]:0;
			$ImageObj->Where ="ImageID='".$ImageObj->MysqlEscapeString($ImageID)."'";
			$ImageObj->TableUpdate($DataArray);
		}
		ob_clean();
		$_SESSION['InfoMessage'] ="Image updated successfully.";
		MyRedirect(DIR_WS_SITE_CONTROL."index.php?Page=$Page&Section=$Section");
		exit;
		
	break;

		case "AddMultipleImage":
			
			for ($i=1;$i<=5;$i++)
			{
				$DataArray = array();
				$DataArray['ImageName'] = isset($_POST['ImageName'][$i])?$_POST['ImageName'][$i]:"";
				$DataArray['ImageTitle'] = isset($_POST['ImageTitle'][$i])?$_POST['ImageTitle'][$i]:"";
				
				$DataArray['URL'] =isset($_POST['URL'][$i])?$_POST['URL'][$i]:"";
				$DataArray['URLType'] =isset($_POST['URLType'][$i])?$_POST['URLType'][$i]:"";
				
				$DataArray['ImageType'] =isset($_POST['ImageType'][$i])?$_POST['ImageType'][$i]:"Image";
				$DataArray['Active'] = isset($_POST['Active'][$i])?$_POST['Active'][$i]:0;
				
				if(isset($_FILES['Upload']['name'][$i]) && $_FILES['Upload']['name'][$i] !="")
				{
					$ArrayType = explode(".",$_FILES['Upload']['name'][$i]);
					$Type=$ArrayType[count($ArrayType)-1];
					$ImageName = uniqid("Image_").".".$Type;
					
					$OriginalFileName = DIR_FS_SITE_UPLOADS_OTHER.$ImageName;
					copy($_FILES['Upload']['tmp_name'][$i],$OriginalFileName);
					$DataArray['Content'] = $ImageName;
					//$ImageObj->DisplayQuery = true;
					$ImageObj->TableInsert($DataArray);	
					
				}
				
			}
			
			@ob_clean();
			$_SESSION['InfoMessage'] ="Image(s) added successfully.";
			MyRedirect(DIR_WS_SITE_CONTROL."index.php?Page=$Page&ImageID=$ImageID");
			exit;

	break;
case "AddImage":
			
			$DataArray['ImageName'] = isset($_POST['ImageName'])?$_POST['ImageName']:"";
			$DataArray['ImageTitle'] = isset($_POST['ImageTitle'])?$_POST['ImageTitle']:"";
			$DataArray['URL'] =isset($_POST['URL'])?$_POST['URL']:"";
			$DataArray['URLType'] =isset($_POST['URLType'])?$_POST['URLType']:"";
			$DataArray['ImageType'] =isset($_POST['ImageType'])?$_POST['ImageType']:"";
			$DataArray['FlashWidth'] =@$_POST['FlashWidth']>0?(int)$_POST['FlashWidth']:"0";
			$DataArray['FlashHeight'] =@$_POST['FlashHeight']>0?(int)$_POST['FlashHeight']:"0";
			$DataArray['Active'] = isset($_POST['Active'])?$_POST['Active']:0;
			
			if($ImageID==0)
			{
				$ImageID = $ImageObj->GetMax("ImageID") + 1;
				$DataArray['ImageID'] = $ImageID;
				if($DataArray['ImageType']=="Image" && isset($_FILES['Upload']) && $_FILES['Upload']['name'] !="")
				{
					$ArrayType = explode(".",$_FILES['Upload']['name']);
					$Type=$ArrayType[count($ArrayType)-1];
					$ImageName = uniqid("Image".$ImageID."_").".".$Type;
					
					$OriginalFileName = DIR_FS_SITE_UPLOADS_OTHER.$ImageName;
					copy($_FILES['Upload']['tmp_name'],$OriginalFileName);
					$DataArray['Content'] = $ImageName;
				}
				
				if($DataArray['ImageType']=="Flash" && isset($_FILES['FlashUpload']) && $_FILES['FlashUpload']['name'] !="")
				{
					$ArrayType = explode(".",$_FILES['FlashUpload']['name']);
					$Type=$ArrayType[count($ArrayType)-1];
					$ImageName = uniqid("Flash".$ImageID."_").".".$Type;
					
					$OriginalFileName = DIR_FS_SITE_UPLOADS_OTHER.$ImageName;
					copy($_FILES['FlashUpload']['tmp_name'],$OriginalFileName);
					$DataArray['Content'] = $ImageName;
				}
				
				
				if($DataArray['ImageType']=="HTML")
				{
					$DataArray['Content'] = isset($_POST['HTMLContent'])?$_POST['HTMLContent']:"";
				}
				
				
				$ImageObj->TableInsert($DataArray);	
				@ob_clean();
				$_SESSION['InfoMessage'] ="Image added successfully.";
			}
			else 
			{
							
				if($DataArray['ImageType']=="Image" && isset($_FILES['Upload']) && $_FILES['Upload']['name'] !="")
				{
					$ArrayType = explode(".",$_FILES['Upload']['name']);
					$Type=$ArrayType[count($ArrayType)-1];
					$ImageName = uniqid("Image".$ImageID."_").".".$Type;
					
					$OriginalFileName = DIR_FS_SITE_UPLOADS_OTHER.$ImageName;
					copy($_FILES['Upload']['tmp_name'],$OriginalFileName);
					$DataArray['Content'] = $ImageName;
				}
				
				if($DataArray['ImageType']=="Flash" && isset($_FILES['FlashUpload']) && $_FILES['FlashUpload']['name'] !="")
				{
					$ArrayType = explode(".",$_FILES['FlashUpload']['name']);
					$Type=$ArrayType[count($ArrayType)-1];
					$ImageName = uniqid("Flash".$ImageID."_").".".$Type;
					
					$OriginalFileName = DIR_FS_SITE_UPLOADS_OTHER.$ImageName;
					copy($_FILES['FlashUpload']['tmp_name'],$OriginalFileName);
					$DataArray['Content'] = $ImageName;
				}
				
				if($DataArray['ImageType']=="HTML")
				{
					$DataArray['Content'] = isset($_POST['HTMLContent'])?$_POST['HTMLContent']:"";
				}
				
				$ImageObj->TableUpdate($DataArray);		
				ob_clean();
				$_SESSION['InfoMessage'] ="Image updated successfully.";
				
			}
		
			MyRedirect(DIR_WS_SITE_CONTROL."index.php?Page=$Page&ImageID=$ImageID");
			exit;

	break;

}
