<?php
$BannerObj = new DataTable(TABLE_BANNERS);
$PageObj = new DataTable(TABLE_PAGES);
$SeoObj = new DataTable(TABLE_SEO);
$DataTableObj= new DataTable(TABLE_BACKUP);
$DataArray = array();	
$CmsTemplate = SKGetTemplates("cms");		

$Section = isset($_GET['Section'])?$_GET['Section']:"";
$Target = isset($_GET['Target'])?$_GET['Target']:"";
$ParentID = isset($_GET['ParentID'])?$_GET['ParentID']:0;
$PageID = isset($_GET['PageID'])?$_GET['PageID']:0;
$BackupObj =new DataTable(TABLE_BACKUP);
$BackUpID = isset($_GET['BackUpID'])?$_GET['BackUpID']:"";

	if($PageID !=0)
	{
		$PageObj = new DataTable(TABLE_PAGES." p LEFT JOIN ".TABLE_SEO." s on (s.ReferenceID = p.PageID AND s.RefrenceType ='cms')");
		$PageObj->Where ="p.PageID='".$PageObj->MysqlEscapeString($PageID)."'";
		$CurrentPage = $PageObj->TableSelectOne();
	}	
/// Target  start 
switch ($Target)
{
	case "DeletePage":
			$PageObj = new DataTable(TABLE_PAGES);
			$PageObj->Where ="PageID='".$PageObj->MysqlEscapeString($PageID)."'";
			$PageObj->TableDelete();								
			
			$SeoObj->Where = "ReferenceID = ".$PageID." AND RefrenceType ='cms'";
			$SeoObj->TableDelete();								
			
			@ob_clean();
			
			$_SESSION['InfoMessage'] ="Page deleted successfully.";
			MyRedirect(DIR_WS_SITE_CONTROL."index.php?Page=$Page&Section=$Section&ParentID=".$ParentID."");
			exit;
		
	break;	
	case "DeleteBackup":
		
			$BackupObj->Where ="BackUpID='".$BackUpID."'";
			$BackupObj->TableDelete();								
			@ob_clean();
			
			$_SESSION['InfoMessage'] ="Old Backup deleted successfully.";
			MyRedirect(DIR_WS_SITE_CONTROL."index.php?Page=$Page&Section=$Section&BackUpID=$BackUpID&PageID=$PageID&ParentID=".$ParentID."");
			exit;
		
	break;	
	case "Moved":
		$MovePageID = isset($_GET['MovePageID'])?$_GET['MovePageID']:"";
		$MoveParentID = isset($_GET['MoveParentID'])?$_GET['MoveParentID']:"";
		if($MovePageID == $MoveParentID)
		{	
			ob_clean();
			
			$_SESSION['ErrorMessage'] ="Page does not move.";
			MyRedirect(DIR_WS_SITE_CONTROL."index.php?Page=$Page&Section=$Section&Target=&ParentID=".$ParentID."&PageID=".$MovePageID."");
			exit;
		}
		else 
		{
			$DataArray = array();
			$DataArray['ParentID'] = $MoveParentID;
			$PageObj->Where ="PageID='".$PageObj->MysqlEscapeString($MovePageID)."'";
			$PageObj->TableUpdate($DataArray);	
			ob_clean();
			
			$_SESSION['InfoMessage'] ="Page moved successfully.";
			MyRedirect(DIR_WS_SITE_CONTROL."index.php?Page=$Page&Section=$Section&ParentID=".$MoveParentID."&PageID=".$MovePageID."");
			exit;
		}
	break;	
	case "UpdatePage":
		for ($i=1;$i <$_POST['Count'];$i++)
		{
			$PageID =$_POST['PageID_'.$i];
			$DataArray = array();
			$DataArray['Active'] = isset($_POST['Active_'.$i])?$_POST['Active_'.$i]:0;
			$DataArray['Position'] = isset($_POST['Position_'.$i])?$_POST['Position_'.$i]:0;
			$PageObj->Where ="PageID='".$PageObj->MysqlEscapeString($PageID)."'";
			$PageObj->TableUpdate($DataArray);
		}
		ob_clean();
		
		$_SESSION['InfoMessage'] ="Page updated successfully.";
		MyRedirect(DIR_WS_SITE_CONTROL."index.php?Page=$Page&Section=$Section&ParentID=".$ParentID."");
		exit;
		
	break;
	
	case "AddPage":
			$DataArray['ParentID'] =$ParentID;
			$DataArray['Active'] = isset($_POST['Active'])?$_POST['Active']:0;
			
			$DataArray['PageName'] = isset($_POST['PageName'])?$_POST['PageName']:"";
			$DataArray['LinkTitle'] = isset($_POST['LinkTitle'])?$_POST['LinkTitle']:"";
			
			if(isset($_POST['Template']) && $_POST['Template'] !="")
				$DataArray['Template'] = $_POST['Template'];
			
				
			$DataArray['LeftTitle1'] = isset($_POST['LeftTitle1'])?$_POST['LeftTitle1']:"";
			$DataArray['LeftDescription1'] = isset($_POST['LeftDescription1'])?$_POST['LeftDescription1']:"";
			$DataArray['LeftDescription2'] = isset($_POST['LeftDescription2'])?$_POST['LeftDescription2']:"";
			$DataArray['UpperBanner1'] = (isset($_POST['UpperBanner1']) && $_POST['UpperBanner1'] !="")?implode(",",$_POST['UpperBanner1']):"";
			
			
		if($PageID==0)
		{
			$PageObj->Where ="ParentID='".$ParentID."'";
			$Position = $PageObj->GetMax("Position") + 1;
			
			$DataArray['Position'] = $Position;
			$DataArray['CreatedDate'] =date('YmdHis');
			
			$PageID = $PageObj->TableInsert($DataArray);		
			ob_clean();
			
			$_SESSION['InfoMessage'] ="Page added successfully.";
		}
		else 
		{
						
			$PageObj->TableUpdate($DataArray);
			ob_clean();
			
			$_SESSION['InfoMessage'] ="Page updated successfully.";
			
		}
		
		$SeoObj->Where = "ReferenceID = ".$PageID." AND RefrenceType ='cms'";
		$CurrentSEO = $SeoObj->TableSelectOne(array("ReferenceID"));
		
		$DataArray = array();
		$DataArray['URLName'] = SkURLCreate((isset($_POST['URLName']) && $_POST['URLName'] !="")?$_POST['URLName']:$_POST['PageName']);
		$DataArray['RedirectEnabled'] = isset($_POST['RedirectEnabled'])?$_POST['RedirectEnabled']:0;
		$DataArray['RedirectURL'] = isset($_POST['RedirectURL'])?$_POST['RedirectURL']:"";
		
		if(@constant("DEFINE_SEO_META_ACTIVE") =="1")
		{
			$DataArray['MetaTitle'] = isset($_POST['MetaTitle'])?$_POST['MetaTitle']:"";
			$DataArray['MetaKeyword'] = isset($_POST['MetaKeyword'])?$_POST['MetaKeyword']:"";
			$DataArray['MetaDescription'] = isset($_POST['MetaDescription'])?$_POST['MetaDescription']:"";
		}
		
		if(isset($CurrentSEO->ReferenceID) && $CurrentSEO->ReferenceID !="")
		{
			$SeoObj->TableUpdate($DataArray);
		}
		else 
		{
			$DataArray['RefrenceType'] = "cms";
			$DataArray['ReferenceID'] = $PageID;
			$SeoObj->TableInsert($DataArray);
		}
				

		if(isset($_POST['SaveThis']) && $_POST['SaveThis']=="1")
		{
			$DataArray = array();
			$DataArray['ReferenceID'] = $PageID;
			$DataArray['Page'] = "cms";
			$DataArray['Dumps'] = base64_encode(serialize($_POST));
			$DataArray['CreatedDate'] = date('YmdHis');
		
			$DataTableObj->TableInsert($DataArray);
		}	
		MyRedirect(DIR_WS_SITE_CONTROL."index.php?Page=$Page&ParentID=$ParentID&PageID=$PageID");
		exit;

	break;

}
?>
