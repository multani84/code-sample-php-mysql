<?php
$Section = isset($_GET['Section'])?$_GET['Section']:"";
$Target = isset($_GET['Target'])?$_GET['Target']:"";
$StPt = isset($_GET['StPt'])?$_GET['StPt']:"0";
$NewsletterID = isset($_GET['NewsletterID'])?$_GET['NewsletterID']:0;
$NewsletterObj = new DataTable(TABLE_NEWSLETTERS);
$UpperLimit = 200;

switch ($Target)
{
	case "AddNewsletter":
	if(isset($_POST['AddNewsletter']))
	{
			
			$Sender = isset($_POST['Sender'])?$_POST['Sender']:"";
			$Subject = isset($_POST['Subject'])?$_POST['Subject']:"";
			$Description = isset($_POST['txtContent'])?MyStripSlashes($_POST['txtContent']):"";
			$TmpMail =explode(",",str_replace("\n","",$Sender));
			$Count =0;
			
			$NewsletterObj->Where ="1";
			$NewsletterID = $NewsletterObj->GetMax("NewsletterID") + 1;
			$DataArray['NewsletterID'] = $NewsletterID;
			
			$DataArray = array();
			$DataArray['Subject'] = $Subject;
			$DataArray['Sender'] = $Sender;
			$DataArray['Description'] = $Description;
			$DataArray['CreatedDate'] = date('Y-m-d H:i:s');
			if(isset($_POST['Save']) && $_POST['Save']=="1")
			{
				$NewsletterObj->TableInsert($DataArray);
			}
			//print_r($Value);exit;
			foreach ($TmpMail as $Value)
			{
				if($Value !="")
				{
					SendEmail($Subject,$Value,DEFINE_ADMIN_EMAIL,DEFINE_SITE_NAME,$Description);
					$Count++;
				}
				
			}							
			ob_clean();
								
			$_SESSION['InfoMessage'] ="Mail has been sent to $Count persons successfully.";
			MyRedirect(DIR_WS_SITE_CONTROL."index.php?Page=$Page&StPt=$StPt&NewsletterID=$NewsletterID");
			exit;
			
	}	
	break;
	
	case "AddEmail":
	//
		//print_r($_POST);exit;
	if(isset($_POST['AddEmail']))
	{	
		for($i=1;$i<=$_POST['NoOfOption'];$i++)
	
			{			
				$DataSubArray = array();
				$DataSubArray['Email'] = isset($_POST['emailaddress'.$i])?$_POST['emailaddress'.$i]:"";
				$DataSubArray['CreatedDate'] = date('YmdHis');
				$NewsletterUserObj = new DataTable(TABLE_USERS_NEWSLETTER);
				$NewsletterUserObj->Where ="Email ='".$NewsletterUserObj->MysqlEscapeString($DataSubArray['Email'])."'";
				$CurrentNewsletterUser = $NewsletterUserObj->TableSelectOne();
				if(isset($CurrentNewsletterUser->Email) && $CurrentNewsletterUser->Email !="")
					$NewsletterUserObj->TableUpdate($DataSubArray);
				else 
					$NewsletterUserObj->TableInsert($DataSubArray);
				
			}
			ob_clean();
			
			$_SESSION['InfoMessage'] ="Email added successfully.";
			MyRedirect(DIR_WS_SITE_CONTROL."index.php?Page=$Page&Section=AddEmail&Popup=true&Meta=true");
			exit;
	}
	break;
	case "UploadEmail":
		if(isset($_FILES['Upload']) && $_FILES['Upload']['name'] !="")
		{
				$ArrayType = explode(".",$_FILES['Upload']['name']);
				$Type=$ArrayType[count($ArrayType)-1];
				
				$ImageName = uniqid("CSV_").".".$Type;				
				$OriginalImage =DIR_FS_SITE_UPLOADS_TMP.$ImageName;
				copy($_FILES['Upload']['tmp_name'],$OriginalImage);
				chmod($OriginalImage,0777);
				$Content = file_get_contents($OriginalImage);
				$EmailArray = explode("\n",$Content);
				foreach ($EmailArray as $k=>$v)
				{
				
					if($v !="" && $k !=0)
					{
						$DataSubArray = array();
						$DataSubArray['Email'] = isset($v)?str_replace("'","",$v):"";
						$NewsletterUserObj = new DataTable(TABLE_USERS_NEWSLETTER);
						$NewsletterUserObj->Where ="Email ='".$NewsletterUserObj->MysqlEscapeString($DataSubArray['Email'])."'";
						$CurrentNewsletterUser = $NewsletterUserObj->TableSelectOne();
						if(isset($CurrentNewsletterUser->Email) && $CurrentNewsletterUser->Email !="")
						{
							$NewsletterUserObj->TableUpdate($DataSubArray);
						}
						else
						{ 
							$DataSubArray['CreatedDate'] = date('YmdHis');
							$NewsletterUserObj->TableInsert($DataSubArray);
						}
					}
				}
				
				ob_clean();
								
			$_SESSION['InfoMessage'] ="File has been imported successfully.";
			MyRedirect(DIR_WS_SITE_CONTROL."index.php?Page=$Page");
			exit;
				
			}
		
	break;
}