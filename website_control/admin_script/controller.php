<?php
require_once(DIR_FS_SITE_INCLUDES_CLASSES."class.controllers.php");
$Section = isset($_GET['Section'])?$_GET['Section']:"";
$Target = isset($_GET['Target'])?$_GET['Target']:"";
$ControllerID = isset($_GET['ControllerID'])?$_GET['ControllerID']:0;

if($ControllerID !=0)
	{
		$CurrentController= $ControllerObj->GetControllerDetailByControllerIDInControllerTable($ControllerID);
	}
	
/// Target  start 
switch ($Target)
{

	case "DeleteController":
			$ControllerObj->DeleteControllerByControllerID($ControllerID);								
			ob_clean();
			
			$_SESSION['InfoMessage'] ="Controller deleted successfully.";
			MyRedirect(DIR_WS_SITE_CONTROL."index.php?Page=$Page&Section=$Section");
			exit;
		
	break;	
	
	case "AddController":
		
		$ControllerObj->UserName = isset($_POST['UserName'])?$_POST['UserName']:"";
		$ControllerObj->Password = isset($_POST['Password'])?$_POST['Password']:"";
		$ControllerObj->EmailAddress = isset($_POST['EmailAddress'])?$_POST['EmailAddress']:"";
		$ControllerObj->Name = isset($_POST['Name'])?$_POST['Name']:"";
		$ControllerObj->Address1 = isset($_POST['Address1'])?$_POST['Address1']:"";
		$ControllerObj->Address2 = isset($_POST['Address2'])?$_POST['Address2']:"";
		$ControllerObj->City = isset($_POST['City'])?$_POST['City']:"";
		$ControllerObj->State = isset($_POST['State'])?$_POST['State']:"";
		$ControllerObj->Country = isset($_POST['Country'])?$_POST['Country']:"";
		$ControllerObj->ZipCode = isset($_POST['ZipCode'])?$_POST['ZipCode']:"";
		$ControllerObj->AllowPages = isset($_POST['AllowPages'])?implode("@@@",$_POST['AllowPages']):"";
		$ControllerObj->Active = isset($_POST['Active'])?$_POST['Active']:"";
		if($ControllerID==0)
		{
			$Obj = $ControllerObj->GetControllerByUserName($ControllerObj->UserName);
			if(isset($Obj->UserName) && $Obj->UserName !="")
			{
				$ErrorMessage ="This Username already exists. Please try again.";
				
			}
			else 
			{
				$ControllerID = $ControllerObj->GetMaxControllerIDByControllerTable() + 1;
				$ControllerObj->AddController();		
				ob_clean();
				
				$_SESSION['InfoMessage'] ="Controller added successfully.";
				MyRedirect(DIR_WS_SITE_CONTROL."index.php?Page=$Page&ControllerID=$ControllerID");
				exit;
			}
		}
		else 
		{
			$ControllerObj->UpdateController($ControllerID);			
			ob_clean();
			
			$_SESSION['InfoMessage'] ="Controller updated successfully.";
			MyRedirect(DIR_WS_SITE_CONTROL."index.php?Page=$Page&ControllerID=$ControllerID");
			exit;

		}
	break;	
		
}

//// target end
?>