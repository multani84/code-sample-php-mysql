<table border="0" cellpadding="0" cellspacing="0" width="100%">

	<tr>

		<td height="20" align="center">

		<h4>Order Reports Section</h4>

	</td>

	</tr>

</table>

<form id="FrmFilter" method="GET" action="index.php">

	<input type="hidden" name="Page" value="<?php echo $Page?>" >

	<input type="hidden" name="Section" value="Display" >

<table border="0" cellpadding="0" cellspacing="0" width="100%" class="InsideTable">

	<tr>

		<td align="left" colspan="5"><b>Select Field(s) to display</b></td>

	</tr>

	<tr>

		<td align="left"><input type="checkbox" name="c1" value="1" <?php echo @$_REQUEST['c1']=="1"?"checked":"";?>>&nbsp;Order No</td>

		<td align="left"><input type="checkbox" name="c2" value="1" <?php echo @$_REQUEST['c2']=="1"?"checked":"";?>>&nbsp;Date</td>

		<td align="left"><input type="checkbox" name="c3" value="1" <?php echo @$_REQUEST['c3']=="1"?"checked":"";?>>&nbsp;Name</td>

		<td align="left"><input type="checkbox" name="c4" value="1" <?php echo @$_REQUEST['c4']=="1"?"checked":"";?>>&nbsp;Email</td>

		<td align="left"><input type="checkbox" name="c5" value="1" <?php echo @$_REQUEST['c5']=="1"?"checked":"";?>>&nbsp;Amount</td>

	</tr>

	<tr>

		<td align="left"><input type="checkbox" name="c11" value="1" <?php echo @$_REQUEST['c11']=="1"?"checked":"";?>>&nbsp;Billing Address</td>

		<td align="left"><input type="checkbox" name="c12" value="1" <?php echo @$_REQUEST['c12']=="1"?"checked":"";?>>&nbsp;Shipping Address</td>

		<td align="left"><input type="checkbox" name="c13" value="1" <?php echo @$_REQUEST['c13']=="1"?"checked":"";?>>&nbsp;Order Status</td>

		<td align="left"><input type="checkbox" name="c14" value="1" <?php echo @$_REQUEST['c14']=="1"?"checked":"";?>>&nbsp;Shipping Status</td>

		<td align="left">&nbsp;</td>

	</tr>

</table>

<table border="0" cellpadding="0" cellspacing="0" width="100%" class="InsideTable">

	<tr>

		<td align="left" colspan="6"><b>Filter Your results</b></td>

	</tr>

	<tr>

		<td align="left">From Date</td>

		<td align="left">

			<input type="text" name="r1"  ID="r1" value="<?php echo @$_REQUEST['r1']?>" size="15" ReadOnly>

			<a href="#" id="ancr1"><img align="absmiddle" style="BORDER:none;" src="<?php echo DIR_WS_SITE_INCLUDES_JAVASCRIPT?>calendar/img.gif"></a>

			<script type="text/javascript">

				Calendar.setup({

						inputField     :    "r1",     // id of the input field

						ifFormat       :    "%Y-%m-%d",      // format of the input field

						button         :    "ancr1",  // trigger for the calendar (button ID)

						align          :    "Bl",           // alignment (defaults to "Bl")

						singleClick    :    true,

						timeFormat	   :	24,

						showsTime	   :	false

					});

			</script>

		</td>

		<td align="left">To Date</td>

		<td align="left">

			<input type="text" name="r2"  ID="r2" value="<?php echo @$_REQUEST['r2']?>" size="15" ReadOnly>

			<a href="#" id="ancr2"><img align="absmiddle" style="BORDER:none;" src="<?php echo DIR_WS_SITE_INCLUDES_JAVASCRIPT?>calendar/img.gif"></a>

			<script type="text/javascript">

				Calendar.setup({

						inputField     :    "r2",     // id of the input field

						ifFormat       :    "%Y-%m-%d",      // format of the input field

						button         :    "ancr2",  // trigger for the calendar (button ID)

						align          :    "Bl",           // alignment (defaults to "Bl")

						singleClick    :    true,

						timeFormat	   :	24,

						showsTime	   :	false

					});

			</script>

		</td>

		<td align="left">Order Status</td>

		<td align="left">

			<select name="r3" id="r3">

				<option value="">--Select--</option>

				<?php

				foreach ($OrderStatusArray as $k=>$v)

				{

				?>

				<option value="<?php echo $k?>" <?php echo @$_REQUEST['r3'] ==$k?"selected":""?>><?php echo $v?></option>

				<?php

				}?>

			</select>

		</td>

	</tr>

	<tr>

	<td align="left">Email Address</td>

	<td align="left">

		<select name="r4" id="r4">

			<option value="">--Select--</option>

			<?php

			$OrderObj->Where = "PaymentStatus ='Paid'";

			$OrderObj->TableSelectAll(array("distinct(Email) as Email"),"Email ASC");

			while ($CurrentEmail = $OrderObj->GetObjectFromRecord())

			{

			?>

			<option value="<?php echo MyStripSlashes($CurrentEmail->Email)?>" <?php echo @$_REQUEST['r4'] ==$CurrentEmail->Email?"selected":""?>><?php echo MyStripSlashes($CurrentEmail->Email)?></option>

			<?php

			}?>

		</select>

	</td>

	<td align="left" valign="top" colspan="2">Any <b>keyword/ name or<br> Name/Address</b> <input size="25" type="text" name="k" value="<?php echo @$_REQUEST['k']?>"></td>

		<td align="right">

		<select name="o" id="o">

			<option value="">--Order By--</option>

			<?php

			$OrderByArray = array(
			"CreatedDate DESC"=>"Created Date DESC",
			"CreatedDate ASC"=>"Created Date ASC",
			"Email DESC"=>"Email DESC",
			"Email ASC"=>"Email ASC",
			"GrandTotal DESC"=>"Amount DESC",
			"GrandTotal ASC"=>"Amount ASC",
			"OrderStatus DESC"=>"Order Status DESC",
			"OrderStatus ASC"=>"Order Status ASC",
								);

			foreach ($OrderByArray as $k=>$v)
			{
			?>
			<option value="<?php echo $k?>" <?php echo @$_REQUEST['o'] ==$k?"selected":""?>><?php echo $v?></option>
			<?php
			}?>

		</select>
		</td>
		<td align="left"><input type="submit" name="Submit" value="Generate Report"></td>
	</tr>
</table>
</form>

<?php

switch($Section)

{

	case "Display":
		$CalculatedArray = array();
		$SelectArray = array("OrderNo");

		if(@$_REQUEST['c2']=="1")
			array_push($SelectArray,"DATE_FORMAT(CreatedDate,'%b %d, %Y')as 'Created Date'");
			
		if(@$_REQUEST['c3']=="1")
		{
		array_push($SelectArray,"concat(BillingFirstName,
											' ', BillingLastName
										    ) as 'Name'");
		}
		
		if(@$_REQUEST['c4']=="1")
			array_push($SelectArray,"Email");
		if(@$_REQUEST['c5']=="1")
		{
			array_push($SelectArray,"FORMAT(GrandTotal,2) as 'Amount'");
			//array_push($SelectArray,"FORMAT(RefundAmount,2) as 'Refund(credit)'");
			array_push($SelectArray,"FORMAT(((SubTotal - (case when (SELECT ProductRefund FROM  `zsk_credit_memo` cm WHERE cm.OrderID = zsk_orders.OrderID) is null then 0 else (SELECT ProductRefund FROM  `zsk_credit_memo` cm WHERE cm.OrderID = zsk_orders.OrderID) end)) * TaxPercentage)/(100+TaxPercentage),2) as 'VAT'");
			array_push($SelectArray,"FORMAT(GrandTotal-RefundAmount,2) as 'Total'");
		}
		
		if(@$_REQUEST['c11']=="1")
		{
			array_push($SelectArray,"concat(BillingFirstName,
											' ', BillingLastName,
										    ' ', BillingAddress1,
										    ', ', BillingAddress2,
										    ' ', BillingCity,
										    ', ', BillingState,
										    ' ', BillingArea,
										    ' ', BillingCountry,
										    ' ', BillingZip,
										    ' ', BillingPhone
										    ) as 'Billing Address'");
			
			
		}
		if(@$_REQUEST['c12']=="1")
		{
			array_push($SelectArray,"concat(ShippingFirstName,
											' ', ShippingLastName,
										    ' ', ShippingAddress1,
										    ', ', ShippingAddress2,
										    ' ', ShippingCity,
										    ', ', ShippingState,
										    ' ', ShippingArea,
										    ' ', ShippingCountry,
										    ' ', ShippingZip,
										    ' ', ShippingPhone
										    ) as 'Shipping Address'");
			
		}
		

		if(@$_REQUEST['c13']=="1")
			array_push($SelectArray,"OrderStatus as 'Order Status'");
		if(@$_REQUEST['c14']=="1")
			array_push($SelectArray,"ShippingStatus as 'Shipping Status'");
		
		$OrderObj->Where = "PaymentStatus ='Paid'";
		
		if(@$_REQUEST['r1']!="" && @$_REQUEST['r2']!="")
		$OrderObj->Where .=" and CreatedDate BETWEEN CAST('".@$_REQUEST['r1']."' AS DateTime) AND DATE_ADD(CAST('".@$_REQUEST['r2']."' AS DateTime),INTERVAL 1 DAY)";
		elseif (@$_REQUEST['r1'] !="")
		$OrderObj->Where .=" and CreatedDate >= CAST('".@$_REQUEST['r1']."' AS DateTime)";
		elseif (@$_REQUEST['r2'] !="")
		$OrderObj->Where .=" and CreatedDate <= DATE_ADD(CAST('".@$_REQUEST['r2']."' AS DateTime), INTERVAL 1 DAY)";

		if(@$_REQUEST['r3']!="")
			$OrderObj->Where .=" and OrderStatus = '".@$_REQUEST['r3']."'";
		
		elseif(@$_REQUEST['r4']!="")
			$OrderObj->Where .=" and Email = '".@$_REQUEST['r4']."'";
		
				
		$Keyword = @$_REQUEST['k'];

	$WherePart ="";
	if(!empty($Keyword))
	{
		$WherePart .=" AND (";
		if(preg_match("/\+\b/i",$Keyword))
		{
			$Prcident= "AND";
			$SecGate = "1";
			$KeywordArr =  explode("+",$Keyword);
		}
		else 
		{
			$Prcident= "OR";
			$SecGate = "0";
			$KeywordArr =  explode(" ",$Keyword);
		}
		foreach ($KeywordArr as $Key=>$Value)
		{
			if(trim($Value) !="")
				$WherePart .="(
						  	Email LIKE '%$Value%' 
						  	OR BillingFirstName LIKE '%$Value%' 
						  	OR BillingLastName LIKE '%$Value%' 
						  	OR BillingAddress1 LIKE '%$Value%' 
						  	OR BillingAddress2 LIKE '%$Value%' 
						  	OR BillingCity LIKE '%$Value%' 
						  	OR BillingState LIKE '%$Value%' 
						  	OR BillingArea LIKE '%$Value%' 
						  	OR BillingCountry LIKE '%$Value%' 
						  	OR BillingZip LIKE '%$Value%' 
						  	OR BillingPhone LIKE '%$Value%' 
						  	OR ShippingFirstName LIKE '%$Value%' 
						  	OR ShippingLastName LIKE '%$Value%' 
						  	OR ShippingAddress1 LIKE '%$Value%' 
						  	OR ShippingAddress2 LIKE '%$Value%' 
						  	OR ShippingCity LIKE '%$Value%' 
						  	OR ShippingState LIKE '%$Value%' 
						  	OR ShippingArea LIKE '%$Value%' 
						  	OR ShippingCountry LIKE '%$Value%' 
						  	OR ShippingZip LIKE '%$Value%' 
						  	OR ShippingPhone LIKE '%$Value%' 
						  	) $Prcident ";
    
		}
		
		$WherePart .=" $SecGate)";
		$OrderObj->Where .= $WherePart;
	}
		
		if(@$_REQUEST['o']!="")
			$OrderBy = @$_REQUEST['o'];
		else 
			$OrderBy = "CreatedDate DESC";
				
		//$OrderObj->DisplayQuery = true;	
		$OrderObj->TableSelectAll($SelectArray,$OrderBy);
		
		if($OrderObj->GetNumRows() >0)
		{
			$ColFields = $OrderObj->GetNumFields();
			?>
			<div align="left">
			<br>
				<form target="_blank" action="<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php echo $Page?>&Section=<?php echo $Section?>&Target=Download" method="POST">
					<input type="hidden" name="Q" value="<?php echo base64_encode($OrderObj->Query)?>">
					&nbsp;&nbsp;<input type="submit" name="Download" value="Download ALL">
					&nbsp;&nbsp;<input type="submit" name="Print" value="Print">
					&nbsp;&nbsp;<input type="button" name="AskEmail" value="Email Report" onclick="return document.getElementById('DivEmailAddress').style.display='block';">
					<div id="DivEmailAddress" style="display:none;padding:15px;border:1px solid #000;">
						Enter email address:<input size="40" type="text" name="EmailAddress" id="EmailAddress" value="" />
						&nbsp;&nbsp;<input type="submit" name="Email" value="Send">
					</div>
				</form>
			<br>
			</div>
			<table border="0" cellpadding="3" cellspacing="2" width="100%" class="InsideTable">
			  <tr class="InsideLeftTd">
			     <?php
			     $CoulnArray = array();
			     for ($i = 0; $i < $ColFields; $i++) 
					{
		    			$CoulnArray[$i] = $OrderObj->GetFieldName($i);
		    			$CalculatedArray[$i] = "";
		    			echo "<td align='left' height=25><b>".$CoulnArray[$i]."</b></td>";
		    		}
					 	
				?>
	           </tr>
	          <?php
	          	while ($Result = $OrderObj->GetArrayFromRecord())
				{
					echo "<tr>";
					for ($i = 0; $i < $ColFields; $i++) 
					{
						if(is_numeric($Result[$i]))
						{
							$CalculatedArray[$i] = $CalculatedArray[$i] + $Result[$i];
						}
						
						echo "<td align='left' style='padding-bottom:10px;'>".MyStripSlashes($Result[$i])."</td>";
					}
		    		
					echo "</tr>";
					
				}
	          ?>
	          
	          <tr class="InsideLeftTd">
			     <?php
			     for ($i = 0; $i < $ColFields; $i++) 
					{
						if(is_numeric($CalculatedArray[$i]) && $i != 0)
		    				echo "<td align='left' height=25><b>".$CalculatedArray[$i]."</b></td>";
		    			else 
		    				echo "<td align='left' height=25>&nbsp;</td>";
		    		}
					 	
				?>
	           </tr>
	        </table>
	 		<?php
		}
		else 
		{
			echo "<br><br><br><br><b>No record found</b>";
		}
	break;
	
}
?>

