<!-- Heading  -->
<div class="row-fluid">
	<div class="span12">
		<h1>Cms Pages Contents</h1>
		<hr />
		<?php echo isset($ContentArea->AreaName)?$ContentArea->AreaName:"";?>
	</div>
	<div class="pull-left">
		&nbsp;&nbsp;
		<a href="<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php echo $Page?>">Cms Pages Contents</a>&nbsp;&nbsp;
		&nbsp;&nbsp;
		<?php echo isset($ContentArea->AreaName)?$ContentArea->AreaName:"";?>
	</div>
	<div class="pull-right">
		<?php
		if($AreaID !=0)
		{
		?>
		<a href="<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php echo $Page?>&Section=AddContent&AreaID=<?php echo $AreaID?>" class="btn btn-primary">Add Content</a>&nbsp;&nbsp;
		<?php
		}?>
	</div>
<div class="clearfix separator bottom"></div>
<!--  End Heading-->	
			
<?php
//// Section start
switch($Section)
{
	case "AddContent" :
	case "EditContent" :
	/////// Add Page  Start
	?>
	<form class="form-horizontal" method="POST" action="<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php echo $Page?>&Section=<?php echo $Section?>&Target=AddContent&AreaID=<?php echo $AreaID?>&ContentID=<?php echo $ContentID?>" enctype="multipart/form-data">
		
		<div class="well">
			<div class="form-group">
				<label for="email" class="col-sm-4 col-md-5 col-lg-4 control-label">Title</label>
				<div class="col-sm-20 col-md-19 col-lg-20 text-left">
					<input type="text" name="ContentTitle"  value="<?php echo isset($_POST['ContentTitle'])?MyStripSlashes($_POST['ContentTitle']):(isset($ContentContent->ContentTitle)?MyStripSlashes($ContentContent->ContentTitle):"")?>" size="50">
				</div>
			</div>
			
			<div class="form-group">
				<label for="email" class="col-sm-4 col-md-5 col-lg-4 control-label">Content</label>
				<div class="col-sm-20 col-md-19 col-lg-20 text-left">
					<?php
					$sContent = isset($_POST['ContentText'])?MyStripSlashes($_POST['ContentText'],true):(isset($ContentContent->ContentText)?MyStripSlashes($ContentContent->ContentText,true):"");
					$SKEditorObj->CArray = array("Width"=>"750",
												 "Height"=>"400",
												 "DMode"=>"Large2",
												 "Help"=>true,
												 "EditorArray"=>array("css"=>DIR_WS_SITE_CONTROL."style.css",
																	  ),
												 ); 
					$SKEditorObj->CreateEditor("ContentText",$sContent);?>
				</div>
			</div>
			<div class="form-group">
				<label for="email" class="col-sm-4 col-md-5 col-lg-4 control-label">Active</label>
				<div class="col-sm-20 col-md-19 col-lg-20 text-left">
					<input type="checkbox" name="Active" value="1" class="chk" <?php echo (isset($_POST['Active']) && $_POST['Active']==1)?"checked":((isset($ContentContent->Active) && $ContentContent->Active==1)?"checked":"")?>>
				</div>
			</div>
						
			<div class="form-group">
				<div class="col-sm-6 pull-right">
					<button class="btn btn-warning  btn-block" type="submit" onclick="return SubmitForm(this.form);">Submit</button>
				</div>
			</div>
						
		</div>
	</form>
	<br />
	<script>
	function PreviewSample(frm)
	{
		
		frm.action="<?php echo DIR_WS_SITE?>index.php?Preview=1&ContentID=<?php echo $ContentID?>";
		frm.target="_blank";
		return true;
	}
	function SubmitForm(frm)
	{
		
		frm.action="<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php echo $Page?>&Section=<?php echo $Section?>&Target=AddContent&AreaID=<?php echo $AreaID?>&ContentID=<?php echo $ContentID?>";
		frm.target="";
		return true;
	}
	</script>

	<?php
	/////// Add Page End
	break;

	/////////Display Page 
	case "DisplayContent" :
		$ContentObj->Where ="AreaID='".$AreaID."'";
		$ContentObj->TableSelectAll('','Position ASC');
		if($ContentObj->GetNumRows() > 0)
		{
		?>
		<script>
				function FunctionDeleteContent(ContentID,Status)
				{
					
						result = confirm("Are you sure want to delete the content?")
						if(result == true)
						{
							URL="<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php echo $Page?>&Section=<?php echo $Section?>&Target=DeleteContent&AreaID=<?php echo $AreaID?>&ContentID=" + ContentID;
							location.href=URL;
							return true;
						}
					return false;
				}				
			</script>							
		<form method="POST" action="<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php echo $Page?>&Section=<?php echo $Section?>&Target=UpdateContent&AreaID=<?php echo $AreaID?>">
		<table class="table table-striped table-bordered table-responsive block">
			<thead>
			<tr>
				<th align="center" width="5%" class="InsideLeftTd"><b>S.No.</b></th>
				<th align="left"  class="InsideLeftTd"><b>Content</b></th>
				<th align="center"  class="InsideLeftTd"><b>Active</b></th>
				<th align="center"  class="InsideLeftTd"><b>Position</b></th>
				<th align="center"  class="InsideLeftTd"><b>Edit</b></th>
				<th align="center"  class="InsideLeftTd"><b>Delete</b></th>
			</tr>
			</thead>
			<tbody>
			<?php
			$SNo=1;
			$Count=1;
			while($ContentContent = $ContentObj->GetObjectFromRecord())
			{
				$Status = 1;
				?>
			<tr >
				<td align="center" class="InsideRightTd"><b><?php echo $SNo?></b></td>
				<td align="left" class="InsideRightTd">
				<b><?php echo $ContentContent->ContentTitle?></b>
				<input type="hidden" name="ContentID_<?php echo $Count?>" value="<?php echo $ContentContent->ContentID?>"></td>
				<td align="center" class="InsideRightTd">
					<input type="checkbox" name="Active_<?php echo $Count?>" value="1" class="chk" <?php echo $ContentContent->Active==1?"checked":""?>>
				</td>
				<td align="center" class="InsideRightTd">
				<input  type="text" name="Position_<?php echo $Count?>" size="3" value="<?php echo $ContentContent->Position?>" class="AlignCenter" style="text-align:center;width:60px">
				</td>
				<td align="center" class="InsideRightTd"><a href="<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php echo $Page?>&Section=EditContent&AreaID=<?php echo $AreaID?>&ContentID=<?php echo $ContentContent->ContentID?>" class="btn btn-success btn-phone-block"><icon class="icon-pencil icon-white"></icon> Edit</a></td>
				<td align="center" class="InsideRightTd">
					<a href="#" onclick="return FunctionDeleteContent('<?php echo $ContentContent->ContentID?>','<?php echo $Status?>');"><b>Delete</b></a>
				</td>
			</tr>
			<?php
			$SNo++;
			$Count++;
			}
		?>
		<tr>
				<td align="center" width="5%"></td>
				<td align="center"></td>
				<td align="center" colspan="2">
					<input type="hidden" name="Count" value="<?php echo $Count?>">
					<input type="submit" name="UpdateActive" value="Update" class="btn btn-primary">
				</td>
				<td align="center"></td>
				<td align="center"></td>
			</tr>
			</tbody>
		</table>
		</form>					
		<?php
		}
		else 
		{
			?>
			<b>No Result Found.</b>
			<?php
		}
	break;
	case "DisplayArea" :
	default:
		$AreaObj->Where ="1";
		$AreaObj->TableSelectAll('','Position ASC');
		if($AreaObj->GetNumRows() > 0)
		{
		?>
		<form method="POST" action="<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php echo $Page?>&Section=<?php echo $Section?>&Target=UpdatePage&AreaID=<?php echo $AreaID?>">
		<table class="table table-striped table-bordered table-responsive block">
			<thead>
			<tr>
				<th align="center" width="5%" class="InsideLeftTd"><b>S.No.</b></th>
				<th align="left"  class="InsideLeftTd"><b>Content Area</b></th>
				<th align="center"  class="InsideLeftTd"><b></b></th>
			</tr>
			</thead>
			<tbody>
			<?php
			$SNo=1;
			$Count=1;
			while($ContentArea = $AreaObj->GetObjectFromRecord())
			{
				$ContentObj->Where ="AreaID='".$ContentArea->AreaID."'";
				$ContentObj->TableSelectAll(array("ContentID"));
				$SubPages = $ContentObj->GetNumRows();
				if($SubPages==0)
					$Status = 1;
				else 
					$Status = 0;
				?>
			<tr >
				<td align="center" class="InsideRightTd"><b><?php echo $SNo?></b></td>
				<td align="left" class="InsideRightTd">
				<b><?php echo $ContentArea->AreaName?></b>
				<input type="hidden" name="AreaID_<?php echo $Count?>" value="<?php echo $ContentArea->ContentID?>"></td>
				<td align="left"  class="InsideRightTd"><a href="index.php?Page=<?php echo $Page?>&Section=DisplayContent&AreaID=<?php echo $ContentArea->AreaID?>"><b>Content </b>(<?php echo $SubPages?>)</a></td>
			</tr>
			<?php
			$SNo++;
			$Count++;
			}
		?>
		</tbody>
		</table>
		</form>					
		<?php
		}
		else 
		{
			?>
			<b>No Result Found.</b>
			<?php
		}
	break;
}
///// Section end?>