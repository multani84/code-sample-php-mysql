<!-- Heading  -->
<div class="row-fluid">
	<div class="span12">
		<h1>Countries</h1>
		<hr />
	</div>
	<div>
	</div>
<div class="clearfix separator bottom"></div>
<!--  End Heading-->		



<?php 
				
		switch($Section)
{
	case "Addcountries" :
	case "Editcountries" :		
				
	/////// Add Product  Start
	?>
	<div class="row-fluid">
		
	<div class="span9">
		<div class="tab-content">
			<!--  Tab Content Block -->
			<form method="POST" action="<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php echo $Page?>&Section=<?php echo $Section?>&Target=AddCountry&CountryID=<?php echo $CountryID?>" enctype="multipart/form-data">
			<div class="tab-pane active" id="lA">
				<h3>Add/Edit details</h3>
				<div class="form-horizontal well">

					<legend>Country details</legend>
					<div class="control-group">
						<label class="control-label" for="inputEmail">Country Name</label>
						<div class="controls">
							<input type="text" name="CountryName" value="<?php echo isset($CurrentCountry->CountryName)?$CurrentCountry->CountryName:""?>" size="30">
						</div>
					</div>
					<div class="control-group">
						<label class="control-label" for="inputEmail">Country Code</label>
						<div class="controls">
							<input type="text" name="Code" value="<?php echo isset($CurrentCountry->Code)?$CurrentCountry->Code:""?>" size="30">
						</div>
					</div>
					<div class="control-group">
						<label class="control-label" for="inputEmail">Country Symbol</label>
						<div class="controls">
							<input type="text" name="Symbol" value="<?php echo isset($CurrentCountry->Symbol)?$CurrentCountry->Symbol:""?>" size="30">
						</div>
					</div>
					<div class="control-group">
						<label class="control-label" for="inputEmail">Country Value</label>
						<div class="controls">
							<input type="text" name="Value" value="<?php echo isset($CurrentCountry->Value)?$CurrentCountry->Value:""?>" size="30">
						</div>
					</div>
					<div class="control-group">
						<label class="control-label" for="inputEmail">Prefix</label>
						<div class="controls">
							<input type="radio" value="1" name="Prefix" class="radio" checked>&nbsp;YES 
							<br /> 
							<input type="radio" value="0" name="Prefix" class="radio" <?php echo isset($CurrentCountry->Prefix) && $CurrentCountry->Prefix==0?"checked":""?>>&nbsp;No
									
						</div>
					</div>

					<div class="control-group">
						<label class="control-label" for="inputEmail">Active</label>
						<div class="controls">
							<input type="checkbox" name="Active" value="1" class="chk" <?php echo ((isset($_POST['Active']) and $_POST['Active']=="1")?"checked":((isset($CurrentCountry->Active) and $CurrentCountry->Active=="1")?"checked":""))?>>
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;		
						</div>
					</div>

					<div class="clearfix"></div>
				
					

				</div>
					
					<div class="pull-right">
						<button type="submit" class="btn btn-success pull-right">Submit <icon class="icon-share-alt icon-white-t"></icon></button>
					</div>
					<div class="clearfix"></div><br/>
				
			</div>
			<!--  End Tab Content Block -->
			</form>
			
		</div>
	</div>	
	
	<?php
	
	break;
	case "DisplayCountry" :
	default:
		$CountryObj = new DataTable(TABLE_COUNTRIES);
		$CountryObj->Where = "1";
		$CountryObj->TableSelectAll("","Active=1 DESC,CountryID ASC");
		?>
	<table width="100%">
		<tr>
		<td align="center">
			<form method="POST" action="<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php echo $Page?>&Section=<?php echo $Section?>&Target=UpdateCountry">
				<table class="table table-striped table-bordered table-responsive block">
						<thead>
						<tr>
							<th height="40"  class="InsideLeftTd">S.No</th>
							<th class="InsideLeftTd" align="center">Country Value</th>
							<!--<th class="InsideLeftTd" align="center">Tax Status</th>
							<th class="InsideLeftTd" align="center">Tax Percent(%)</th>
							<th class="InsideLeftTd" align="center">Tax Suffix</th>-->
							<th class="InsideLeftTd" align="center">Active</th>
						</tr>
						</thead>
						<tbody>
						<script type="text/javascript">
						function FunctionDeleteCountry()
						{
							result = confirm("Are you sure want to delete the currency?")
							if(result == true)
							{
								return true;
							}
							return false;
						}				
						</script>
				
						<?php
						
							$SNo=1;
							$Count=1;
							$k=1;
							while ($CurrentCountry = $CountryObj->GetObjectFromRecord())
							{?>
							<tr>
								<td class="InsideRightTd"><?php echo $SNo?>
								<input type="hidden" name="CountryID_<?php echo $Count?>" value="<?php echo $CurrentCountry->CountryID?>"></td>
								<td class="InsideRightTd"><?php echo $CurrentCountry->CountryName?> (<?php echo $CurrentCountry->CountryISOCode1?>)</td>
								<!--<td align="center"><input type="checkbox" name="TaxStatus_<?php echo $Count?>" value="1" class="chk" <?php echo @$CurrentCountry->TaxStatus==1?"checked":""?>></td>
								<td align="center"><input  type="text" name="TaxPercent_<?php echo $Count?>" size="3" value="<?php echo $CurrentCountry->TaxPercent?>" class="AlignCenter" style="text-align:center;width:60px"></td>
								<td align="center"><input  type="text" name="TaxSuffix_<?php echo $Count?>" value="<?php echo $CurrentCountry->TaxSuffix?>" class="AlignCenter" style="text-align:center;width:60px"></td>-->
								<td align="center"><input type="checkbox" name="Active_<?php echo $Count?>" value="1" class="chk" <?php echo @$CurrentCountry->Active==1?"checked":""?>></td>
								
								<!--<td align="center"><input type="radio" name="DefaultValue" value="<?php echo $CurrentCountry->CountryID?>" class="chk" <?php echo @$CurrentCountry->DefaultValue==1?"checked":""?>></td>	
								<td class="InsideRightTd">
									<a href="<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php echo $Page?>&Section=Editcountries&CountryID=<?php echo $CurrentCountry->CountryID?>" class="btn btn-success btn-phone-block"><icon class="icon-pencil icon-white"></icon> Edit</a>
								</td>-->
								
						</tr>
							<?php
							$SNo++;
							$Count++;
							}
							?>
						<tr>
							<td height="35" class="InsideRightTd"></td>
							<td class="InsideRightTd"><input type="hidden" name="Count" value="<?php echo $Count?>"></td>
							<td align="center" class="InsideRightTd" colspan="2"><input type="submit"  name="UpdateCountry<?php echo $Count?>" value="Update" class="check"></td>
						</tr>
					</tbody>						
					</table>
				</form>				
				<br>
				
			</td>
		</tr>
	</table>
	<?php 
	break;
} ?>
	
	
	
	
	
