<script type="text/JavaScript" src="<?php echo DIR_WS_SITE_INCLUDES_JAVASCRIPT?>lytebox/lytebox.js"></script>
<link rel="stylesheet" type="text/css" media="all" href="<?php echo DIR_WS_SITE_TEMPLATES.DEFINE_TEMPLATE_STYLE?>/lytebox.css" />	

<table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr>
		<td height="20" align="center">
		<h4>Newsletters</h4>
	</td>
	</tr>
	<tr>
		<td align="left">
			<table border="0" cellpadding="5" cellspacing="2" width="100%" class="InsideTable">
				<tr>
				<?php
				$UserNewsletterObj = new DataTable(TABLE_USERS_NEWSLETTER);
				$UserNewsletterObj->Where ='Email != ""';
				$EmailTotalObj = $UserNewsletterObj->TableSelectOne(array("count(*) as EmailTotal"));
				if(isset($EmailTotalObj->EmailTotal) && $EmailTotalObj->EmailTotal > $UpperLimit)
				{
						$Sr =1;
					for($i=0;$i<ceil($EmailTotalObj->EmailTotal/$UpperLimit);$i++)
					{	
						//$UserNewsletterObj->DisplayQuery = true;
						$LoLm = $UpperLimit*$i;
						$UpLm = ($UpperLimit*($i+1) <$EmailTotalObj->EmailTotal)?$UpperLimit*($i+1):$EmailTotalObj->EmailTotal;
						$UserNewsletterObj->TableSelectAll(array("*"),"Email LIMIT $LoLm,1");
						$LoLmObj = $UserNewsletterObj->GetObjectFromRecord();
						$UserNewsletterObj->TableSelectAll(array("*"),"Email LIMIT ".($UpLm-1).",1");
						$UpLmObj = $UserNewsletterObj->GetObjectFromRecord();
						?>
						<td align="center"><a href="<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php echo $Page?>&StPt=<?php echo $LoLm?>">
							<b><?php echo substr(strtoupper($LoLmObj->Email),0,2)?> - <?php echo substr(strtoupper($UpLmObj->Email),0,2)?></b>
							<br>
							(<?php echo $LoLm?>-<?php echo $UpLm?>)
						</a></td><br>
					
						<?php
					}
					?>
					
				<?php}
				?>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td height="20" align="right">
		<?php
				$NewsletterObj->Where ="1";
				$NewsletterObj->TableSelectAll("","NewsletterID DESC");
				if($NewsletterObj->GetNumRows()>0)
				{?>
				<form name="FrmType" method="GET" action="<?php echo DIR_WS_SITE_CONTROL?>index.php">
				<table width="100%">
					<tr height="20">
						<td height="20">
						<div align="right">
						<input type="hidden" name="Page" value="<?php echo $Page?>">
						<input type="hidden" name="StPt" value="<?php echo $StPt?>">
						<select name="NewsletterID" onchange="document.FrmType.submit();">
						<option value="">Select Old Newsletters</option>
						<?php
						while($CurrentNewsletter = $NewsletterObj->GetObjectFromRecord())
						{?>
						<option value="<?php echo $CurrentNewsletter->NewsletterID?>" <?php echo $CurrentNewsletter->NewsletterID==$NewsletterID?"selected":""?>><?php echo $CurrentNewsletter->Subject?></option>
						<?php
						}?></select>
						<noscript>
						<input type="submit" name="SubmitType" value="Go">
						</noscript>
						</td>
					</tr>
				</table>
				</form>
				<?php
				}?>
				
		</td>
	</tr>
</table>
<?php
//// Section start
switch($Section)
{
	case "AddNewsletter" :
	case "EditNewsletter" :
	default:
	/////// Add Newsletter  Start
	
		if($NewsletterID !=0)
		{
			$NewsletterObj->Where ="NewsletterID ='$NewsletterID'";
			$CurrentNewsletter= $NewsletterObj->TableSelectOne();
		}
		

	
					
	?>
	<?php/* CSV Upload*/
	/*?>
	<form method="POST" action="<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php echo $Page?>&Section=<?php echo $Section?>&Target=UploadEmail" enctype="multipart/form-data">
	<table border="0" cellpadding="3" cellspacing="1" width="100%" class="InsideTable">
			<tr>
				<td align="center" class="InsideLeftTd" width="20%"><b>Upload</b>(csv file)
				<br>
				<a href="<?php echo DIR_WS_SITE_UPLOADS_TMP?>sample.csv" target="_blank"><small>Please see sample file</small></a>
				</td>
				<td align="left" class="InsideRightTd" width="30%"><input type="file" name="Upload"></td>
				<td align="left" class="InsideRightTd"><input type="submit" name="Submit" value="Submit"></td>
			</tr>
	</table>
	</form>
	<br>	
	<?php*/?>					
	<form method="POST" action="<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php echo $Page?>&Section=<?php echo $Section?>&Target=AddNewsletter" enctype="multipart/form-data">
		<table border="0" cellpadding="3" cellspacing="1" width="100%" class="InsideTable">
			<tr>
				<td align="center" class="InsideLeftTd" width="20%"><b>Subject</b></td>
				<td colspan="2" align="left" class="InsideRightTd"><input type="text" name="Subject" value="<?php echo isset($_POST['Subject'])?$_POST['Subject']:(isset($CurrentNewsletter->Subject)?stripslashes($CurrentNewsletter->Subject):"")?>" id="Subject" size="96"></td>
			</tr>
			<tr>
				<td align="center" class="InsideLeftTd"><b>Senders</b><br>(Enter one email ID or multiple email IDs separated by commas)</td>
				<td align="left" class="InsideRightTd"><textarea name="Sender" id="Sender" cols="45" rows="10"><?php echo isset($_POST['Sender'])?$_POST['Sender']:(isset($Sender)?stripslashes($Sender):"")?></textarea>
				</td>
				<td valign="top" >
				<div style="height:200px;overflow: auto;padding:3px;display:block" ><?php
					$UserNewsletterObj = new DataTable(TABLE_USERS_NEWSLETTER);
					$UserNewsletterObj->Where ='Email != ""';
					$UserNewsletterObj->TableSelectAll(array("*"),"Email LIMIT ".($StPt).",".$UpperLimit."");
					//$UserNewsletterObj->TableSelectAll(array("Distinct(Email) as Email"),"Email ASC");
				?>
					<script type="text/javascript">
					function ChkAll(chkObj)
					{
						FieldArray = chkObj.form.elements['ChkAdd[]'];
						if(chkObj.checked)
						{
							if(FieldArray.length==undefined)
							{
								FieldArray.checked =true;
								SwitchLine(FieldArray.checked,FieldArray.value)
								return fales;
							}
								
							for (var j=0;j<FieldArray.length;j++)
							{
								FieldArray[j].checked =true;
								SwitchLine(FieldArray[j].checked,FieldArray[j].value)
						
							}
						}
					}
					function SwitchLine(ckPt,val)
					{
						Str = document.getElementById('Sender').value;
						if(ckPt)
						{
							if(Str.indexOf(val) > -1)
							{
								//Str = Str
							}
							else
							{
							  Str = Str + val;
							}
						}
						else
							 Str = Str.replace(val,'');							 
						document.getElementById('Sender').value = Str;
						
					}
					</script>	
					
					
					<table width="100%" border="0" cellspacing="1" cellpadding="3" class="InsideTable">
							<tr>
								<td colspan="2"><b>Contact Address</b></td>
								
							</tr>
							<?php
							if($UserNewsletterObj->GetNumRows() >0)
							{?>
							<tr height="22">
								<td class="InsideLeftTd" width="5%"><input type="checkbox" name="SelectAll" id="SelectAll" class="chk" value="1" onclick="return ChkAll(this);"></td>
								<td class="InsideLeftTd"><b>Select All</b>
								
								
								</td>
							</tr>
							<?php
								$SNo =1;
								while($CurrentUser = $UserNewsletterObj->GetObjectFromRecord())
								{
									?>
								<tr height="22">
									<td class="InsideRightTd" height="22">
										<input type="checkbox" name="ChkAdd[]" id="ChkAdd_<?php echo $SNo?>" class="chk" value="<?php echo $CurrentUser->Email?>," onclick="return SwitchLine(this.checked,this.value);" >
									</td>
									<td class="InsideRightTd"><?php echo $CurrentUser->Email?></td>
								</tr>
								<?php
								$SNo++;
								}
							}
							
							else 
							{
								?>		
							<tr>
								<td colspan="2" align="center" class="txtNoRecord">No record found.</td>
							</tr>
							<?php
							}?>
						</table>
						
						<br>
					</div>
				</td>
			</tr>
				<tr>
				<td align="center" class="InsideLeftTd"><b>Description</b></td>
				<td align="left" colspan="2" class="InsideRightTd">
				<?php
					$sContent = isset($_POST['txtContent'])?MyStripSlashes($_POST['txtContent'],true):(isset($CurrentNewsletter->Description)?MyStripSlashes($CurrentNewsletter->Description,true):"");
					$SKEditorObj->CArray = array("Width"=>"750",
												 "Height"=>"600",
												 "DMode"=>"Large2",
												 "Help"=>true,
												 "EditorArray"=>array("css"=>DIR_WS_SITE_CONTROL."style.css",
												 					  ),
												 ); 
					$SKEditorObj->CreateEditor("txtContent",$sContent);?>
				</td>
			</tr>
			</tr>
				<tr>
				<td align="center" class="InsideLeftTd"><b>Save this?</b>
				<br>
				<small>For future reference</small></td>
				<td align="left" colspan="2" class="InsideRightTd">
					<input type="checkbox" name="Save" value="1"> 
					
				</td>
			</tr>
			<tr>
				<td align="center" class="InsideLeftTd"></td>
				<td align="left" colspan="2">
				<input type="submit" name="AddNewsletter" value="Submit" >
				</td>
			</tr>
		</table>
	</form>
	
	
	<?php
	/////// Add Newsletter End
	break;
	/////////Display Banner 
	case "AddEmail" :?>
	<form method="POST" action="<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php echo $Page?>&Section=<?php echo $Section?>&Target=AddEmail&Popup=true&Meta=true" enctype="multipart/form-data">
		<table border="0" cellpadding="3" cellspacing="1" width="100%" style="BORDER-RIGHT: #a9a9a9 1px solid; BORDER-TOP: #a9a9a9 1px solid; BORDER-LEFT: #a9a9a9 1px solid; BORDER-BOTTOM: #a9a9a9 1px solid">
				<tr>
				<td align="center" class="InsideLeftTd"><b>No. Of Values</td>
				<td align="left" class="InsideRightTd"><select name="NoOfOption">
				<?php
				$NoOfOption = ($_POST['NoOfOption']);
				for($i=0;$i<=100;$i++)
				{?>
					<option value="<?php echo $i?>" <?php echo $i==$NoOfOption?"selected":""?>><?php echo $i?></option>
				<?php
				}?>
				</select>
				<input type="submit" name="NosOfOption" value="Select Option"></td>
			</tr>
				<tr>
					<td><h4>Add Email Address</h4></td>
				</tr>
				<?php
				if($NoOfOption >0)
				{?>
				
							<?php
				           for($j=1;$j<=$NoOfOption;$j++)
				           {
				           	
				            ?>
							<tr>
								<td><input type="text" value="" name="emailaddress<?php echo $j?>" size="29">
								<br /><br /></td>
							</tr>
							  <?php
			           }?>
						
				  <?php
			           }?>
				<tr>
					<td><input type="submit" value="submit" name="AddEmail"></td>
				</tr>
				
		</table>
		</form>
	<?phpbreak;
	
}
///// Section end?>
















