<!--  Flot (Charts) JS -->
	<!--[if lte IE 8]><script language="javascript" type="text/javascript" src="<?php echo DIR_WS_SITE_CONTROL_THEMES.THEME_STYLE?>//scripts/flot/excanvas.min.js"></script><![endif]-->
	<script src="<?php echo DIR_WS_SITE_CONTROL_THEMES.THEME_STYLE?>/scripts/flot/jquery.flot.js" type="text/javascript"></script>
	<script src="<?php echo DIR_WS_SITE_CONTROL_THEMES.THEME_STYLE?>/scripts/flot/jquery.flot.pie.js" type="text/javascript"></script>
	<script src="<?php echo DIR_WS_SITE_CONTROL_THEMES.THEME_STYLE?>/scripts/flot/jquery.flot.tooltip.js" type="text/javascript"></script>
	<script src="<?php echo DIR_WS_SITE_CONTROL_THEMES.THEME_STYLE?>/scripts/flot/jquery.flot.resize.js" type="text/javascript"></script>
	<script src="<?php echo DIR_WS_SITE_CONTROL_THEMES.THEME_STYLE?>/scripts/flot/jquery.flot.orderBars.js" type="text/javascript"></script>

<?php 
$Mode = isset($_GET['Mode'])?$_GET['Mode']:"Y";
$YearArray = isset($_GET['Y'])?$_GET['Y']:array(date('Y'));
$MonthArray = isset($_GET['M'])?$_GET['M']:array(date('Y-m'));
?>	
<?php switch($Mode)
{
	case "M":
	?>
	<!-- Search -->
		<form class="form-horizontal form-search well" id="FrmFilter" method="GET" action="index.php">
			<legend>Dashboard</legend>
			<input type="hidden" name="Page" value="<?php echo $Page?>" >
			<input type="hidden" name="Mode" value="M">
			Month(s)
			<?php foreach($MonthArray as $k=>$MonthIndex):?>
			<select name="M[]" class="span3" style="width:130px;">
						<?php
						for($y=date('Y')-1;$y<=date('Y')+1;$y++)
						{
							for($m=1;$m<=12;$m++)
							{	
							?>
							<option value="<?php echo date("Y-m", mktime(0, 0, 0, $m, 1, $y))?>" <?php echo date("Y-m", mktime(0, 0, 0, $m, 1, $y))==$MonthIndex?"selected":""?>><?php echo date("Y-M", mktime(0, 0, 0, $m, 1, $y))?></option>
							<?php
							}
						}?>					
					</select>
					&nbsp;&nbsp;&nbsp;&nbsp;
			<?php endforeach;?>
				<span id="CompareMonth"><a href="javascript:" onclick="GetCompareDump('CompareMonth','MonthDump');">Compare</a></span>
				&nbsp;&nbsp;&nbsp;&nbsp;
				<button type="submit" class="btn"><icon class="icon-search icon-brown"></icon> Go</button>
				<textarea id="MonthDump" style="display:none;"><select name="M[]" class="span3" style="width:130px;">
						<?php
						for($y=date('Y')-1;$y<=date('Y')+1;$y++)
						{
							for($m=1;$m<=12;$m++)
							{	
							?>
							<option value="<?php echo date("Y-m", mktime(0, 0, 0, $m, 1, $y))?>"><?php echo date("Y-M", mktime(0, 0, 0, $m, 1, $y))?></option>
							<?php
							}
						}?>					
					</select>
					&nbsp;&nbsp;&nbsp;&nbsp;</textarea>
		</form>
	<!-- End Search -->	
	<?php
	break;
	case "Y":
	default:
?>
<!-- Search -->
		<form class="form-horizontal form-search well" id="FrmFilter" method="GET" action="index.php">
			<legend>Dashboard</legend>
			<input type="hidden" name="Page" value="<?php echo $Page?>" >
			<input type="hidden" name="Mode" value="Y">
			Year(s)
			<?php foreach($YearArray as $k=>$Year):?>
			<select name="Y[]" class="span3" style="width:130px;">
						<?php
						for($i=date('Y')-5;$i<=date('Y')+5;$i++)
						{
							?>
							<option value="<?php echo $i?>" <?php echo $i==$Year?"selected":""?>><?php echo $i?></option>
							<?php
						}?>					
					</select>
					&nbsp;&nbsp;&nbsp;&nbsp;
			<?php endforeach;?>
				<span id="CompareYear"><a href="javascript:" onclick="GetCompareDump('CompareYear','YearDump');">Compare</a></span>
				&nbsp;&nbsp;&nbsp;&nbsp;
				<button type="submit" class="btn"><icon class="icon-search icon-brown"></icon> Go</button>
				<textarea id="YearDump" style="display:none;"><select name="Y[]" class="span3" style="width:130px;"><?php
					for($i=date('Y')-5;$i<=date('Y')+5;$i++)
					{
						?>
						<option value="<?php echo $i?>"><?php echo $i?></option>
						<?php
					}?>					
				</select>&nbsp;&nbsp;&nbsp;&nbsp;</textarea>
		</form>
	<!-- End Search -->
<?php 
break;
}?>	
<?php 
$Data = $XaxisGraph = array();
switch($Mode)
{
	
	case "M":
	
	foreach($MonthArray as $k=>$Year_Month)
	{
		if($Year_Month !="")
		{
			$arr = explode("-",$Year_Month);
			$Year = $arr[0];
			$Month = $arr[1];
			$Data[$Year_Month]['Title']  =  date('Y-M',strtotime($Year.'-'.$Month.'-01'));
			$Data[$Year_Month]['Values']  =  array();
			$XaxisGraph = array();
			$Xaxis = array();
			$NosOfDays = 31;//cal_days_in_month(CAL_GREGORIAN, $Month, $Year);
			for($i=1;$i<=$NosOfDays;$i++)
			{
				$val = rand(0,100);
				$Data[$Year_Month]['Values'][$i] = $val;
				$Data[$Year_Month]['Graph'][] = "[".$i.",".$val."]";
				$XaxisGraph[] = "[".$i.",'".$i."']";
				$Xaxis[$i] = $i;
			}
			
		}
	}
	
	break;
	case "Y":
	default:
	foreach($YearArray as $k=>$Year)
	{
		if($Year !="")
		{
			$Data[$Year]['Title']  =  $Year;
			$Data[$Year]['Values']  =  array();
			$XaxisGraph = array();
			$Xaxis = array();
			for($i=1;$i<=12;$i++)
			{
				$val = rand(0,100);
				$Data[$Year]['Values'][$i] = $val;
				$Data[$Year]['Graph'][] = "[".$i.",".$val."]";
				$XaxisGraph[] = "[".$i.",'".date('M',strtotime('2015-'.$i.'-01'))."']";
				$Xaxis[$i] = date('M',strtotime('2015-'.$i.'-01'));
			}
		}
	}
	
	break;
}
?>
<script>
function GetCompareDump(El1,El2)
{
	$("#"+El1).html($("#"+El2).val());
	return false;
}
jQuery(document).ready(function($){

		var plot = $.plot("#order_sale_graph", [ 
			<?php foreach($Data as $k=>$val):?>
			{ data: [<?php echo implode (",",$Data[$k]['Graph'])?>], label: "<?php echo $Data[$k]['Title']?>"}<?php echo $Sno++<count($Data)?",":""?>
			<?php endforeach;?>
		], {
			series: {
				lines: {
					show: true
				},
				points: {
					show: true
				}
			},
			grid: {
				hoverable: true,
				clickable: true
			},
			xaxis : {
				ticks : [<?php echo implode (",",$XaxisGraph)?>]
			},
			yaxis : {
				ticks : 10,
				min : 0,
				max : 100
			},
			tooltip : true
			
			
		});
		
		$("#order_sale_graph").bind("plothover", function (event, pos, item) {
			if (item) {
					var x = plot.getOptions().xaxis.ticks[item.dataIndex][1],
						y = item.datapoint[1].toFixed(2);
					$("#flotTip").html(item.series.label + " of " + x + " - sale: " + y);
				} else {
					$("#flotTip").hide();
				}
		});
});
</script>

<div class="row-fluid">
	<!-- Analytics -->
	<div class="span8">
		<div class="well">
			<div class="pull-right" style="margin-top:-5px;">
				<select style="width:100px;margin-top:-10px;" name="Mode" onchange='document.location.href="index.php?Page=<?php echo $Page?>&Mode="+this.value'>
				<option value="Y" <?php echo $Mode=='Y'?"selected":""?>>Yearly</option>
				<option value="M" <?php echo $Mode=='M'?"selected":""?>>Monthly</option>
				</select>
			</div>
			<h4 class="heading glyphicons cardio"><i></i>Sales</h4>
			<div id="order_sale_graph" style="height: 350px;"></div>
		</div>
	</div>
	<div class="span4" >
		<div class="well">
			<h4 class="heading glyphicons stats"><i></i>Overview</h4>
			<div style="height: 350px;overflow: auto;">
			<table class="table table-striped table-bordered table-responsive block">
				<thead>
					<tr>
					<th align="center" width="5%" class="InsideLeftTd">S.No.</th>
				<?php foreach($Data as $k=>$val):?>	
						<th align="center" width="5%" class="InsideLeftTd"><?php echo $Data[$k]['Title']?></th>
				<?php endforeach;?>	
					</tr>
				<thead>
				<tbody>
				<?php foreach($Xaxis as $index=>$Value):?>	
					<tr>
					<td align="center" width="5%" class="InsideLeftTd"><?php echo $Value?></td>
					<?php foreach($Data as $k=>$val):?>	
							<td align="center" width="5%" class="InsideLeftTd"><?php echo number_format($Data[$k]['Values'][$index],2)?></td>
					<?php endforeach;?>	
					</tr>
				<?php endforeach;?>	
				</tbody>
				</table>
			</div>	
			
		</div>		
	</div>
	<!-- END Analytics -->
	
</div>