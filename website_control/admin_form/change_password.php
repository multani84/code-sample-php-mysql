<!-- Heading  -->
<div class="row-fluid">
	<div class="span12">
		<h1>CHANGE PASSWORD</h1>
		<hr />
	</div>
	<div>
	</div>
<div class="clearfix separator bottom"></div>
<!--  End Heading-->	


<?php
//// Section start
switch($Section)
{
	case "change_password" :
	default:
	/////// 
	if(isset($_SESSION['AdminObj']))
	{
	?>
	<form class="form-horizontal" name="LoginForm" id="LoginForm" action="<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php echo $Page?>&Target=UpdatePassword" method="POST">
		
		<div class="well">
			<div class="form-group">
				<label for="email" class="col-sm-4 col-md-5 col-lg-4 control-label">User Name</label>
				<div class="col-sm-20 col-md-19 col-lg-20 text-left">
					<?php echo $AdminPermissionObj->UserName?>
				</div>
			</div>
			
			<div class="form-group">
				<label for="email" class="col-sm-4 col-md-5 col-lg-4 control-label">Old Password</label>
				<div class="col-sm-20 col-md-19 col-lg-20 text-left">
					<input type="Password" name="OldPassword" id="OldPassword" size="30" required>
				</div>
			</div>
			
			<div class="form-group">
				<label for="email" class="col-sm-4 col-md-5 col-lg-4 control-label">New Password</label>
				<div class="col-sm-20 col-md-19 col-lg-20 text-left">
					<input type="Password" name="Password" id="Password" size="30" required>
				</div>
			</div>
			
			<div class="form-group">
				<label for="email" class="col-sm-4 col-md-5 col-lg-4 control-label">Confirm Password</label>
				<div class="col-sm-20 col-md-19 col-lg-20 text-left">
					<input type="password" name="RPassword" id="RPassword" size="30" required>
				</div>
			</div>
			
			
			
						
			<div class="form-group">
				<div class="col-sm-6 pull-right">
					<button class="btn btn-warning  btn-block" type="submit">Submit</button>
				</div>
			</div>
						
		</div>
		
	</form>
	<?php
	}
	/////// 
	break;
	
}
///// Section end?>