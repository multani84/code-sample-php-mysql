<!-- Heading  -->
<div class="row-fluid">
	<div class="span12">
		<h1>Additional Content</h1>
		<hr />
	</div>
	<div>
	</div>
<div class="clearfix separator bottom"></div>
<!--  End Heading-->	
		
<?php
//// Section start
switch($Section)
{
	case "EditAddOn" :
	/////// Add Page  Start
	?>
	<form class="form-horizontal" method="POST" action="<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php echo $Page?>&Section=<?php echo $Section?>&Target=AddAddOn&AddOnID=<?php echo $AddOnID?>" enctype="multipart/form-data">
				<h3>Add/Edit details</h3>
<div class="well">
			<div class="form-group">
				<label for="email" class="col-sm-4 col-md-5 col-lg-4 control-label">Title</label>
				<div class="col-sm-20 col-md-19 col-lg-20 text-left">
					<?php echo isset($CurrentAddOn->AddOnName)?$CurrentAddOn->AddOnName:""?>
				</div>
			</div>
			<div class="form-group">
				<label for="email" class="col-sm-4 col-md-5 col-lg-4 control-label">Description</label>
				<div class="col-sm-20 col-md-19 col-lg-20 text-left">
					<?php
						$sContent = isset($_POST['Description1'])?MyStripSlashes($_POST['Description1'],true):(isset($CurrentAddOn->Description1)?MyStripSlashes($CurrentAddOn->Description1,true):"");
						$SKEditorObj->CArray = array("Width"=>"750",
													 "Height"=>"400",
													 "DMode"=>"Large2",
													 "Help"=>true,
													 "EditorArray"=>array("css"=>DIR_WS_SITE_CONTROL."style.css",
																		  ),
													 ); 
						$SKEditorObj->CreateEditor("Description1",$sContent);?>	
				</div>
			</div>
			
			<div class="form-group">
				<div class="col-sm-6 pull-right">
					<button class="btn btn-warning  btn-block" type="submit">Submit</button>
				</div>
			</div>
						
		</div>
		
	</form>
		<script>		
			function FunctionDeleteBackup(BackupID)
			{//alert(BackupID);exit;
				if(BackupID==0)
				{
					alert("Please select the backup page to delete.");
					return false;
				}
				else
				{
					result = confirm("Are you sure want to delete the old backup? If yes, You will NOT be able to recover this backup again.")
					if(result == true)
					{
						URL="&BackupID=" + BackupID;
						location.href=URL;
						return true;
					}
				}
				return false;
			}				
		</script>	
	
	<script>
	function PreviewSample(frm)
	{
		
		frm.action="<?php echo DIR_WS_SITE?>index.php?Preview=1&AddOnID=<?php echo $AddOnID?>";
		frm.target="_blank";
		return true;
	}
	function SubmitForm(frm)
	{
		
		frm.action="<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php echo $Page?>&Section=<?php echo $Section?>&Target=AddPage&ParentID=<?php echo $ParentID?>&AddOnID=<?php echo $AddOnID?>";
		frm.target="";
		return true;
	}
	</script>

	<?php
	/////// Add Page End
	break;

	/////////Display Page 
	case "DisplayPage" :
	default:
		$AddOnObj->Where ="1";
		$AddOnObj->TableSelectAll('','AddOnID ASC');
		if($AddOnObj->GetNumRows() > 0)
		{
		?>
		
		
		<table class="table table-striped table-bordered table-responsive block">
			<thead>
			<tr>
				<th align="center" width="5%" class="InsideLeftTd">S.No.</th>
				<th align="left"  class="InsideLeftTd">Title</th>
				<th align="left"  class="InsideLeftTd">Content</th>
				<th align="center"  class="InsideLeftTd">Edit</th>
			</tr>
			</thead>
			<tbody><?php
			$SNo=1;
			$Count=1;
			while($CurrentAddOn = $AddOnObj->GetObjectFromRecord())
			{
				?>
			<tr>
				<td align="center" class="InsideRightTd"><?php echo $SNo?></td>
				<td align="left" class="InsideRightTd">
				<?php echo $CurrentAddOn->AddOnName?>
				</td>
				<td align="left" class="InsideRightTd"><a href="#" onclick="document.getElementById('DivCnt_<?php echo $SNo?>').style.display='block';return false;">Click to see content</a>
				<br>
				<div id="DivCnt_<?php echo $SNo?>" style="display:none;">
					<div align="right"><a href="#" onclick="document.getElementById('DivCnt_<?php echo $SNo?>').style.display='none';return false;">[close]</a></div>
				<?php echo MyStripSlashes($CurrentAddOn->Description1)?>
				</div>
				</td>
				<td align="center" class="InsideRightTd"><a href="<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php echo $Page?>&Section=EditAddOn&AddOnID=<?php echo $CurrentAddOn->AddOnID?>" class="btn btn-success btn-phone-block"><icon class="icon-pencil icon-white"></icon>Edit</a></td>
				</tr>
			<?php
			$SNo++;
			$Count++;
			}
		?>
		</tbody>
		</table>
		<?php
		}
		else 
		{
			?>
			No Result Found.
			<?php
		}
	break;
}
///// Section end?>