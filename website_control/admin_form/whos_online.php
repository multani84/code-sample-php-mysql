<!-- Heading  -->
<div class="row-fluid">
	<div class="span12">
		<h1>WHOS ONLINE</h1>
		<hr />
	</div>
	<div>
	</div>
<div class="clearfix separator bottom"></div>
<!--  End Heading-->		

<?php 
				
switch($Section)
{
	case "DisplayWhosOnline" :
	default:
		$WhosOnlineObj = new DataTable(TABLE_WHOS_ONLINE);
		$WhosOnlineObj->Where = "1 GROUP BY IPAddress ";
		$WhosOnlineObj->TableSelectAll(array("*","DATE_FORMAT(EntryDate,'%b %d, %Y %T') as MyEntryDate,DATE_FORMAT(LastClick,'%b %d, %Y %T') as MyLastClick"),"UserName DESC, EntryDate DESC, IPAddress ASC");
		?>
	
			<table width="100%">
		<tr>
		<td align="center">
				<table class="table table-striped table-bordered table-responsive block">
						<thead>
						<tr>
							<th height="40"  class="InsideLeftTd" align="left"><b>S.No</b></th>
							<th class="InsideLeftTd" align="left"><b>Name</b></th>
							<th class="InsideLeftTd" align="left"><b>IP Address</b></th>
							<th class="InsideLeftTd" align="left"><b>Entry Time</b></th>
							<th class="InsideLeftTd" align="left"><b>Last Click Time</b></th>
							<th class="InsideLeftTd" align="left"><b>Last Url</b></th>
						</tr>
						</thead>
						<tbody>
						<?php
						
							$SNo=1;
							$Count=1;
							$k=1;
							while ($CurrentWhosOnline = $WhosOnlineObj->GetObjectFromRecord())
							{?>
							<tr>
								<td class="InsideRightTd" align="left"><?php echo $SNo?></td>
								<td class="InsideRightTd" align="left">
								<?php echo MyStripSlashes($CurrentWhosOnline->FullName)?>
								<?php if($CurrentWhosOnline->UserName  !="")
								{
									?>
									<br><a href="mailto:<?php echo MyStripSlashes($CurrentWhosOnline->UserName)?>"><?php echo MyStripSlashes($CurrentWhosOnline->UserName)?></a>								<?php
								}
								 ?>
								
								</td>
								<td class="InsideRightTd" align="left"><?php echo MyStripSlashes($CurrentWhosOnline->IPAddress)?></td>
								<td class="InsideRightTd" align="left"><?php echo MyStripSlashes($CurrentWhosOnline->MyEntryDate)?></td>
								<td class="InsideRightTd" align="left"><?php echo MyStripSlashes($CurrentWhosOnline->MyLastClick)?></td>
								<td class="InsideRightTd" align="left"><?php echo MyStripSlashes($CurrentWhosOnline->LastURL)?></td>
								
						</tr>
							<?php
							$SNo++;
							$Count++;
							}
							?>
							</tbody>
					</table>
				
			</td>
		</tr>
	</table>
	<?php 
	break;
} ?>
	
	
	
	
	
