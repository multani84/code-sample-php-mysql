<!-- Heading  -->
<div class="row-fluid">
	<div class="span12">
		<h1>Pages</h1>
		<hr />
	</div>
	<div class="pull-left">
		&nbsp;&nbsp;
		<a href="<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php echo $Page?>" class="adm-link">Pages</a>
		&nbsp;&raquo;&nbsp;
		<?php echo PageChain4Caption($ParentID,"index.php","Page=$Page&ParentID","adm-link",false,"",false,"");?>
	</div>
	<div class="pull-right">
		<?php
		//if($ParentID !=0)
		{
		?>
		<a href="<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php echo $Page?>&Section=AddPage&ParentID=<?php echo $ParentID?>" class="btn btn-primary">Add Page</a>&nbsp;&nbsp;
		<?php
		}?>
	</div>
<div class="clearfix separator bottom"></div>
<!--  End Heading-->	
			
<?php
//// Section start
switch($Section)
{
	case "AddPage" :
	case "EditPage" :
	/////// Add Page  Start
	?>
<?php
$BackupObj =new DataTable(TABLE_BACKUP);

if($BackUpID !="")
{
	$BackupObj->Where = "BackUpID ='$BackUpID'";
	$CurrentBackup = $BackupObj->TableSelectOne();
	$_POST = unserialize(base64_decode($CurrentBackup->Dumps));
}
	
$BackupObj->Where = "Page ='$Page' AND ReferenceID='$PageID'";
$BackupObj->TableSelectAll(array('BackUpID','DATE_FORMAT(CreatedDate,"%M %d, %Y %r")as MyCreatedDate','CreatedDate'),' CreatedDate DESC');
if($BackupObj->GetNumRows() >0)
{
?>
<form action="<?php echo basename($_SERVER['PHP_SELF'])?>" method="GET">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr>
		<td height="20" align="right">
		<?php
		if(count($_GET) >0)
		{
			foreach ($_GET as $Key=>$Value)
			
			{
				if($Key !="BackUpID")
				echo '<input type="hidden" name="'.$Key.'" value="'.$Value.'">';
			}
			
		}	
		echo '<select name="BackUpID" onchange="this.form.submit();">';
		echo '<option value="">---Select Old Backup---</option>';
		while ($CurrentBackup = $BackupObj->GetObjectFromRecord())
		echo '<option value="'.$CurrentBackup->BackUpID.'" '.($BackUpID==$CurrentBackup->BackUpID?"selected":"").'>'.$CurrentBackup->MyCreatedDate.'</option>';
		echo '</select>';
		
			?>
		</td>
	</tr>
</table>					
</form>
<?php
}?>
	
	<form class="form-horizontal" method="POST" action="<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php echo $Page?>&Section=<?php echo $Section?>&Target=AddPage&ParentID=<?php echo $ParentID?>&PageID=<?php echo $PageID?>" enctype="multipart/form-data">
		
		<div class="well">
			<div class="form-group">
				<label for="email" class="col-sm-4 col-md-5 col-lg-4 control-label">Page Name</label>
				<div class="col-sm-20 col-md-19 col-lg-20 text-left">
					<input type="text" name="PageName" value="<?php echo isset($_POST['PageName'])?MyStripSlashes($_POST['PageName']):(isset($CurrentPage->PageName)?MyStripSlashes($CurrentPage->PageName):"")?>" size="70" <?php echo (isset($CurrentPage->PageID) && $CurrentPage->PageID <=1000)?"readonly":"" ?>>
				</div>
			</div>
			<?php if($PageID >1000 OR $PageID ==0):?>
			<div class="form-group">
				<label for="email" class="col-sm-4 col-md-5 col-lg-4 control-label">Navigation Title</label>
				<div class="col-sm-20 col-md-19 col-lg-20 text-left">
					<input type="text" name="LinkTitle" value="<?php echo isset($_POST['LinkTitle'])?MyStripSlashes($_POST['LinkTitle']):(isset($CurrentPage->LinkTitle)?MyStripSlashes($CurrentPage->LinkTitle):"")?>" size="70">
				</div>
			</div>
			<hr>
			<?php endif;?>
			
			<?php if(@constant("DEFINE_SEO_URL_ACTIVE") =="1" && ($PageID >1000 OR $PageID  ==0)):?>
			<div class="form-group">
				<label for="email" class="col-sm-4 col-md-5 col-lg-4 control-label">Page URL</label>
				<div class="col-sm-20 col-md-19 col-lg-20 text-left">
					<input type="text" name="URLName" value="<?php echo isset($_POST['URLName'])?MyStripSlashes($_POST['URLName']):(isset($CurrentPage->URLName)?MyStripSlashes($CurrentPage->URLName):"")?>" size="70" onblur="return CheckSpecialSymbol(this);" title="Please do not use special symbol. Only use alfa numeric characters,(_) underscores or (-) dashes.">
					<br><small>Please do not use special symbol. 
					<br>Only use alfa numeric characters,(_) underscores or (-) dashes.</small>
				</div>
			</div>
			<hr>
			<?php endif;?>
			
			<?php if(@constant("DEFINE_SEO_META_ACTIVE") =="1"):?>
			<div class="form-group">
				<label for="email" class="col-sm-4 col-md-5 col-lg-4 control-label">Meta Title</label>
				<div class="col-sm-20 col-md-19 col-lg-20 text-left">
					<input type="text" name="MetaTitle" value="<?php echo isset($_POST['MetaTitle'])?MyStripSlashes($_POST['MetaTitle']):(isset($CurrentPage->MetaTitle)?MyStripSlashes($CurrentPage->MetaTitle):"")?>" size="100">
				</div>
			</div>
			
			<div class="form-group">
				<label for="email" class="col-sm-4 col-md-5 col-lg-4 control-label">Meta Keywords</label>
				<div class="col-sm-20 col-md-19 col-lg-20 text-left">
					<textarea name="MetaKeyword" cols="60" rows="3"><?php echo isset($_POST['MetaKeyword'])?MyStripSlashes($_POST['MetaKeyword']):(isset($CurrentPage->MetaKeyword)?MyStripSlashes($CurrentPage->MetaKeyword):"")?></textarea>
				</div>
			</div>
			
			<div class="form-group">
				<label for="email" class="col-sm-4 col-md-5 col-lg-4 control-label">Meta Description</label>
				<div class="col-sm-20 col-md-19 col-lg-20 text-left">
					<textarea name="MetaDescription" cols="60" rows="3"><?php echo isset($_POST['MetaDescription'])?MyStripSlashes($_POST['MetaDescription']):(isset($CurrentPage->MetaDescription)?MyStripSlashes($CurrentPage->MetaDescription):"")?></textarea>
				</div>
			</div>
			<hr>
			<?php endif;?>
			
			
			<div class="form-group">
				<label for="email" class="col-sm-4 col-md-5 col-lg-4 control-label">Title</label>
				<div class="col-sm-20 col-md-19 col-lg-20 text-left">
					<input type="text" name="LeftTitle1"  value="<?php echo isset($_POST['LeftTitle1'])?MyStripSlashes($_POST['LeftTitle1']):(isset($CurrentPage->LeftTitle1)?MyStripSlashes($CurrentPage->LeftTitle1):"")?>" size="50">
				</div>
			</div>
			
			<div class="form-group">
				<label for="email" class="col-sm-4 col-md-5 col-lg-4 control-label">Page Content</label>
				<div class="col-sm-20 col-md-19 col-lg-20 text-left">
					<?php
					$sContent = isset($_POST['LeftDescription1'])?MyStripSlashes($_POST['LeftDescription1'],true):(isset($CurrentPage->LeftDescription1)?MyStripSlashes($CurrentPage->LeftDescription1,true):"");
					$SKEditorObj->CArray = array("Width"=>"750",
												 "Height"=>"400",
												 "DMode"=>"Large2",
												 "Help"=>true,
												 "EditorArray"=>array("css"=>DIR_WS_SITE_CONTROL."style.css",
																	  ),
												 ); 
					$SKEditorObj->CreateEditor("LeftDescription1",$sContent);?>
				</div>
			</div>
			<?php //if($PageID >1000 OR $PageID ==0 ):?>
			<?php if(false):?>
			<div class="form-group">
				<label for="email" class="col-sm-4 col-md-5 col-lg-4 control-label">Banner</label>
				<div class="col-sm-20 col-md-19 col-lg-20 text-left">
					<select name="UpperBanner1[]" id="UpperBanner1" size="10" multiple>
					<option value="">--Select--</option>
					<?php
						$BannerObj->Where = "Alignment='H'";
						$BannerObj->TableSelectAll("","Position ASC");
		
					while($CurrentBanner = $BannerObj->GetObjectFromRecord())
					{?>
						<option value="<?php echo $CurrentBanner->BannerID?>" <?php echo (isset($_POST['UpperBanner1']) && in_array($CurrentBanner->BannerID,$_POST['UpperBanner1']))?"selected":((isset($CurrentPage->UpperBanner1) && in_array($CurrentBanner->BannerID,explode(",",$CurrentPage->UpperBanner1)))?"selected":"")?>><?php echo $CurrentBanner->BannerName?></option>
						<?php
					}?>
					</select>
				</div>
			</div>
			
			
			<?php endif;?>
			<div class="form-group">
				<label for="email" class="col-sm-4 col-md-5 col-lg-4 control-label">Activate Page (Main Menu)</label>
				<div class="col-sm-20 col-md-19 col-lg-20 text-left">
					<input type="checkbox" name="Active" value="1" class="chk" <?php echo (isset($_POST['Active']) && $_POST['Active']==1)?"checked":((isset($CurrentPage->Active) && $CurrentPage->Active==1)?"checked":"")?>>
				</div>
			</div>
			
			<div class="form-group">
				<label for="email" class="col-sm-4 col-md-5 col-lg-4 control-label">Redirect URL Enabled?</label>
				<div class="col-sm-20 col-md-19 col-lg-20 text-left">
					<input type="text" name="RedirectURL" id="RedirectURL" size="59" value="<?php echo isset($_POST['RedirectURL'])?MyStripSlashes($_POST['RedirectURL']):(isset($CurrentPage->RedirectURL)?MyStripSlashes($CurrentPage->RedirectURL):"")?>"/>
					&nbsp;&nbsp;&nbsp;&nbsp;
					<input type="checkbox" name="RedirectEnabled" value="1" class="chk" <?php echo (isset($_POST['RedirectEnabled']) && $_POST['RedirectEnabled']==1)?"checked":((isset($CurrentPage->RedirectEnabled) && $CurrentPage->RedirectEnabled==1)?"checked":"")?>> Active
				</div>
			</div>
			<?php if(@constant("DEFINE_CMS_PAGES_TEMPLATE")=="1"):
			 $Template = ((isset($_POST['Template']) && $_POST['Template'] !="")?$_POST['Template']:((isset($CurrentPage->Template) && $CurrentPage->Template !="")?$CurrentPage->Template:DEFINE_CMS_PAGES_TEMPLATE_DEFAULT));
			?>
			<div class="form-group">
				<label for="email" class="col-sm-4 col-md-5 col-lg-4 control-label">Template</label>
				<div class="col-sm-20 col-md-19 col-lg-20 text-left">
					<select name="Template">														
					<?php foreach (SKGetTemplates('cms') as $k=>$v)
					{?>
					<option value="<?php echo $k?>" <?php echo $Template==$k?"selected":""?>><?php echo $v?></option>
					<?php
					}?>
					</select>
				</div>
			</div>
			<?php endif;?>
			
			<div class="form-group">
				<label for="email" class="col-sm-4 col-md-5 col-lg-4 control-label">Save This?</label>
				<div class="col-sm-20 col-md-19 col-lg-20 text-left">
					<input type="checkbox" name="SaveThis" value="1" class="chk">
					<br><small>(For future reference. <?php echo date("F j, Y, g:i a");  ?>)</small>
				</div>
			</div>
			
			
						
			<div class="form-group">
				<div class="col-sm-6 pull-right">
					<button class="btn btn-warning  btn-block" type="submit" onclick="return SubmitForm(this.form);">Submit</button>
				</div>
			</div>
						
		</div>
	</form>
	<br />
		<script>		
			function FunctionDeleteBackup(BackupID)
			{
				if(BackupID==0)
				{
					alert("Please select the backup page to delete.");
					return false;
				}
				else
				{
					result = confirm("Are you sure want to delete the old backup? If yes, You will NOT be able to recover this backup again.")
					if(result == true)
					{
						URL="&BackupID=" + BackupID;
						location.href=URL;
						return true;
					}
				}
				return false;
			}				
		</script>	
		<h3>Your Archives Below</h3>
		<br />
		<b>View All Old Backup</b><br />
	
		<table class="table table-striped table-bordered table-responsive block">
		<?php
		if($BackUpID !="")
		{
			$BackupObj->Where = "BackUpID ='$BackUpID'";
			$CurrentBackup = $BackupObj->TableSelectOne();
		}
		$BackupObj->Where = "Page ='cms' AND ReferenceID='$PageID'";
		$BackupObj->TableSelectAll(array('BackUpID','DATE_FORMAT(CreatedDate,"%M %d, %Y %r")as MyCreatedDate','CreatedDate'),' CreatedDate DESC');
		if($BackupObj->GetNumRows()):
		while ($OldBackUp =$BackupObj->GetObjectFromRecord()):
		?>
				<tr>
					<td align="left"><b><?php echo $OldBackUp->MyCreatedDate?></b></td>
					<td align="right">
					<form action="<?php echo basename($_SERVER['PHP_SELF'])?>" method="GET">
						<input type="submit" name="Restore" value="Restore" onclick="this.form.submit()" class="btn">
						<?php
						if(count($_GET) >0)
						{
							foreach ($_GET as $Key=>$Value)
							
							{
								if($Key !="BackUpID")
								echo '<input type="hidden" name="'.$Key.'" value="'.$Value.'">';
								echo '<input type="hidden" name="BackUpID" value="'.$OldBackUp->BackUpID.'">';
							}
							
						}	
						?>
					</form>
					</td>
					<td>
					
					<a href="<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php echo $Page?>&Target=DeleteBackup&ParentID=<?php echo $ParentID?>&BackUpID=<?php echo $OldBackUp->BackUpID?>" onclick="return confirm('Wooooo - hold on please - the action you want to do is highly hazardous!!\nIf you press ok, You will NOT be able to recover this old back up again.')" class="btn btn-danger btn-phone-block"><icon class="icon-remove icon-white"></icon> Delete</a></td>
					</tr>
			<?php endwhile;?>
		<?php else:?>
			<tr>
				<td>No Old Backup's has been found</td>
			</tr>
		<?php endif;?>
	</table>
	<script>
	function PreviewSample(frm)
	{
		
		frm.action="<?php echo DIR_WS_SITE?>index.php?Preview=1&PageID=<?php echo $PageID?>";
		frm.target="_blank";
		return true;
	}
	function SubmitForm(frm)
	{
		
		frm.action="<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php echo $Page?>&Section=<?php echo $Section?>&Target=AddPage&ParentID=<?php echo $ParentID?>&PageID=<?php echo $PageID?>";
		frm.target="";
		return true;
	}
	</script>

	<?php
	/////// Add Page End
	break;

	/////////Display Page 
	case "DisplayPage" :
	default:
		$PageObj->Where ="ParentID='".$ParentID."'";
		$PageObj->TableSelectAll('','PageID >1000 DESC,Position ASC');
		if($PageObj->GetNumRows() > 0)
		{
		?>
		<script>
				function FunctionMovePage(PageID,Obj)
				{
					ParentID =Obj.value;
					if(PageID==ParentID)
					{
						alert("You cannot shift the page.");
						return false;
					}
					result = confirm("Are you sure want to move the page?")
					if(result == true)
					{
						URL ="<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php echo $Page?>&Section=<?php echo $Section?>&Target=Moved&ParentID=<?php echo $ParentID?>&MovePageID=" + PageID + "&MoveParentID=" + ParentID;
						location.href=URL;
					}
					else
					{
						Obj.value="";
					}
					return false;
				}
				function FunctionDeletePage(PageID,Status)
				{
					if(Status==0)
					{
						alert("First delete all pages inside this section.");
						return false;
					}
					else
					{
						result = confirm("Are you sure want to delete the page?")
						if(result == true)
						{
							URL="<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php echo $Page?>&Target=DeletePage&ParentID=<?php echo $ParentID?>&PageID=" + PageID;
							location.href=URL;
							return true;
						}
					}
					return false;
				}				
			</script>							
		<form method="POST" action="<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php echo $Page?>&Section=<?php echo $Section?>&Target=UpdatePage&ParentID=<?php echo $ParentID?>">
		<table class="table table-striped table-bordered table-responsive block">
			<thead>
			<tr>
				<th align="center" width="5%" class="InsideLeftTd"><b>S.No.</b></th>
				<th align="left"  class="InsideLeftTd"><b>Page Name</b></th>
				<th align="left"  class="InsideLeftTd"><b>Navigation Title</b></th>
				<th align="left"  class="InsideLeftTd"><b>Preview</b></th>
				<?php
				if(@constant("DEFINE_CMS_PAGES_MULITIPLE")=="1")
				{
					if($ParentID ==0)
					{
						?>
						<th align="left"  class="InsideLeftTd"><b>Sub Pages</b></th>
						<?php
					}
					else 
					{
						?>
						<th align="left"  class="InsideLeftTd"><b>Move Page</b></th>
						<?php
						
					}
				}
				?>
				<th align="center"  class="InsideLeftTd"><b>Active(Main Menu)</b></th>
				<th align="center"  class="InsideLeftTd"><b>Position</b></th>
				<th align="center"  class="InsideLeftTd"><b>Edit</b></th>
				<th align="center"  class="InsideLeftTd"><b></b></th>
			</tr>
			</thead>
			<tbody>
			<?php
			$SNo=1;
			$Count=1;
			while($CurrentPage = $PageObj->GetObjectFromRecord())
			{
				$PageObj2 = new DataTable(TABLE_PAGES);
				$PageObj2->Where ="ParentID='".$CurrentPage->PageID."'";
				$PageObj2->TableSelectAll(array("PageID"));
				$SubPages = $PageObj2->GetNumRows();
				if($SubPages==0)
					$Status = 1;
				else 
					$Status = 0;
				?>
			<tr >
				<td align="center" class="InsideRightTd"><b><?php echo $SNo?></b></td>
				<td align="left" class="InsideRightTd">
				<b><?php echo $CurrentPage->PageName?></b>
				<input type="hidden" name="PageID_<?php echo $Count?>" value="<?php echo $CurrentPage->PageID?>"></td>
				<td align="left" class="InsideRightTd"><?php echo GetPageLinkTitle($CurrentPage->PageID);?></td>
				<td align="left" class="InsideRightTd">
				<?php
				if($CurrentPage->PageID >"1000")
				{
				?>
					<a href="<?php echo SKSEOURL($CurrentPage->PageID,"cms")?>" target="_blank" class="btn btn-success"><icon class="icon-eye-open icon-white"></icon> Preview</a>
					<?php
				}?>
				</td>
				<?php
				if(@constant("DEFINE_CMS_PAGES_MULITIPLE")=="1")
				{
					if($ParentID ==0)
					{
						?>
						<td align="left"  class="InsideRightTd"><a href="index.php?Page=<?php echo $Page?>&ParentID=<?php echo $CurrentPage->PageID?>"><b>Sub Pages </b>(<?php echo $SubPages?>)</a></td>
						<?php
					}
					else 
					{
						?>
						<td align="center" class="InsideRightTd">
					<?php
						if(isset($_GET['Type']) && $_GET['Type']=="Move" && isset($_GET['PageID']) && $_GET['PageID']==$CurrentPage->PageID)
						{?>
							<select name="MovePage<?php echo $Count?>" id="MovePage<?php echo $Count?>" onchange="return FunctionMovePage('<?php echo $CurrentPage->PageID?>',this)">
									<option value="">--Move Page--</option>
									<option value="0">MOVE TO TOP</option>
										<?php echo PageChain(0,$CurrentPage->PageID,array(),false,false,0);?>
							</select>
						<?php
						}
						else 
						{?>
							<a href="<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php echo $Page?>&ParentID=<?php echo $ParentID?>&Type=Move&PageID=<?php echo $CurrentPage->PageID?>" class="adm-link">Move Page</a>
						<?php
						}
					?>							
					</td>		
						<?php
					}
				}
				?>
				<td align="center" class="InsideRightTd">
				<?php
				if($CurrentPage->PageID >"1000")
				{
				?>
					<input type="checkbox" name="Active_<?php echo $Count?>" value="1" class="chk" <?php echo $CurrentPage->Active==1?"checked":""?>>
					<?php
				}?>
				</td>
				<td align="center" class="InsideRightTd">
				<?php
				if($CurrentPage->PageID >"1000")
				{
				?>
				<input  type="text" name="Position_<?php echo $Count?>" size="3" value="<?php echo $CurrentPage->Position?>" class="AlignCenter" style="text-align:center;width:60px">
				<?php
				}?>
				</td>
				<td align="center" class="InsideRightTd"><a href="<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php echo $Page?>&Section=EditPage&ParentID=<?php echo $ParentID?>&PageID=<?php echo $CurrentPage->PageID?>" class="btn btn-success btn-phone-block"><icon class="icon-pencil icon-white"></icon> Edit</a></td>
				<td align="center" class="InsideRightTd">
					<?php
					if($CurrentPage->ParentID !="0")
					{?>
					<a href="#" onclick="return FunctionDeletePage('<?php echo $CurrentPage->PageID?>','<?php echo $Status?>');"><b>Delete</b></a>
					<?php
					}?>
				</td>
			</tr>
			<?php
			$SNo++;
			$Count++;
			}
		?>
		<tr>
				<td align="center" width="5%"></td>
				<td align="center"></td>
				<td align="center"></td>
				<td align="center"></td>
				<?php
				if(@constant("DEFINE_CMS_PAGES_MULITIPLE")=="1")
				{
				?>
					<td align="left"></td>
					<td align="center"></td>
				<?php
				}?>
				<td align="center" colspan="2">
					<input type="hidden" name="Count" value="<?php echo $Count?>">
					<input type="submit" name="UpdateActive" value="Update" class="btn btn-primary">
				</td>
				<td align="center"></td>
				<td align="center"></td>
			</tr>
			</tbody>
		</table>
		</form>					
		<?php
		}
		else 
		{
			?>
			<b>No Result Found.</b>
			<?php
		}
	break;
}
///// Section end?>