<table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr>
		<td height="20" colspan="2" align="center">
		<h4>Images</h4>
	</td>
	</tr>
	<tr>
		<td height="20" align="left">
		</td>
		<td height="20" align="right">
		<a href="<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php echo $Page?>&Section=AddMultipleImage" class="adm-link">Add Multiple Image(s)</a>&nbsp;&nbsp;
		&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;
		<a href="<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php echo $Page?>&Section=AddImage" class="adm-link">Add Image</a>&nbsp;&nbsp;
		</td>
	</tr>
</table>				
<?php
//// Section start
switch($Section)
{
	
	case "AddMultipleImage":
		?>
		<form method="POST" action="<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php echo $Page?>&Section=<?php echo $Section?>&Target=AddMultipleImage&ImageID=<?php echo $ImageID?>" enctype="multipart/form-data">
		<table border="0" cellpadding="3" cellspacing="1" width="100%"%" class="InsideTable">
		<tr>
			<td align="left" class="InsideLeftTd"><b>Image Name</b></td>
			<td align="left" class="InsideLeftTd"><b>Image Title</b></td>
			<td align="left" class="InsideLeftTd"><b>Image</b></td>
			<td align="left" class="InsideLeftTd"><b>URL</b></td>
			<td align="center" class="InsideLeftTd"><b>Activate</b></td>
		</tr>
		
		<?php
		for ($i=1;$i<=5;$i++)
		{
			?>
			<tr>
				<td align="left" class="InsideRightTd" valign="top">
					<input type="text" name="ImageName[<?php echo $i?>]" value="" size="25">
				</td>
				<td align="left" class="InsideRightTd" valign="top">
					<input type="text" name="ImageTitle[<?php echo $i?>]" value="" size="25">
				</td>
				<td align="left" class="InsideRightTd" valign="top">
					<input type="file" name="Upload[<?php echo $i?>]" size="25">
				</td>
				<td align="left" class="InsideRightTd" valign="top">
					<input type="text" size="30" name="URL[<?php echo $i?>]" id="URL_[<?php echo $i?>]" value="">
					<br>
					<br>
					<select name="URLType[<?php echo $i?>]" st>
					<option value="_blank">Open in new window</option>
					<option value="_self">Open in same window</option>
					</select>
				</td>
				<td align="center" class="InsideRightTd" valign="top">
				<input type="checkbox" name="Active[<?php echo $i?>]" value="1" class="chk">
				</td>
			</tr>
			<tr>
				<td colspan="5"><hr></td>
			</tr>
			<?php
		}
		?>
		<tr>
			<td align="center" class="InsideLeftTd" colspan="5"><input type="submit" name="AddImage" value="Submit"></td>
		</tr>
		</table>
		</form>
		<?php
		break;
	case "AddImage" :
	case "EditImage" :
	/////// Add Image  Start
	?>
	<form method="POST" action="<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php echo $Page?>&Section=<?php echo $Section?>&Target=AddImage&ImageID=<?php echo $ImageID?>" enctype="multipart/form-data">
		<table border="0" cellpadding="3" cellspacing="1" width="100%"%" class="InsideTable">
			<tr>
				<td align="center" class="InsideLeftTd"><b>Image Name</b></td>
				<td align="left" class="InsideRightTd"><input type="text" name="ImageName" value="<?php echo isset($CurrentImage->ImageName)?MyStripSlashes($CurrentImage->ImageName):""?>" size="60"></td>
			</tr>
			<tr>
				<td align="center" class="InsideLeftTd"><b>Image Title</b></td>
				<td align="left" class="InsideRightTd"><input type="text" name="ImageTitle" value="<?php echo isset($CurrentImage->ImageTitle)?MyStripSlashes($CurrentImage->ImageTitle):""?>" size="60"></td>
			</tr>
			<tr style="display:none;">
				<td align="center" class="InsideLeftTd"><b>Image Type</b></td>
				<td align="left" class="InsideRightTd">
				<input type="radio" class="chk" name="ImageType" id="ImageTypeImage" value="Image" class="radio" checked onclick="return ShowContentType(this.value);">&nbsp;Image
				<input type="radio" class="chk" name="ImageType" id="ImageTypeFlash" value="Flash" class="radio" onclick="return ShowContentType(this.value);">&nbsp;Flash
				<input type="radio" class="chk" name="ImageType" id="ImageTypeHTML" value="HTML" class="radio" onclick="return ShowContentType(this.value);">&nbsp;HTML
				</td>
			</tr>
			<tr>
				<td align="center" width="100%" colspan="2">
				<div id="DivImage" style="display:none;">
				<fieldset class="InsideTable"><legend>Image:</legend>
						<table width="100%" border="0" cellspacing="2" cellpadding="1" class="InsideTable">
							<tr class="CartRecordTr">
								<td width="10%" class="CartRecordTd" align="center"><b>Image</b></td>
								<td class="CartRecordTd" width="30%">
									<input type="file" name="Upload" size="30">
									<?php
									if(isset($CurrentImage->Content) && $CurrentImage->Content!="" && file_exists(DIR_FS_SITE_UPLOADS_OTHER.$CurrentImage->Content))
									{
										?>
										<a href="<?php echo DIR_WS_SITE_UPLOADS_OTHER.$CurrentImage->Content?>" target="_blank" class="adm-link">Available</a>
										<?php
									}
									else 
									{
										echo "Not Available";
									}?>
								</td>
								<td width="10%" align="center"><b>URL</b></td>
								<td valign="top">
									<input type="text" size="40" name="URL" id="URL" value="<?php echo isset($CurrentImage->URL)?MyStripSlashes($CurrentImage->URL):""?>">
									&nbsp;&nbsp;&nbsp;&nbsp;
									<select name="URLType">
									<option value="_blank" <?php echo (isset($CurrentImage->URLType) && $CurrentImage->URLType=="_blank")?"selected":""?>>Open in new window</option>
									<option value="_self" <?php echo (isset($CurrentImage->URLType) && $CurrentImage->URLType=="_self")?"selected":""?>>Open in same window</option>
									</select>
								</td>
							</tr>
						</table>
					</fieldset>
				</div>
				<div id="DivFlash" style="display:none;">
					<fieldset class="InsideTable"><legend>Flash Image:</legend>
						<table width="100%" border="0" cellspacing="2" cellpadding="1" class="InsideTable">
							<tr class="CartRecordTr">
								<td width="10%" class="CartRecordTd" align="center" valign="top"><b>Flash</b></td>
								<td class="CartRecordTd" width="30%" valign="top">
									<input type="file" name="FlashUpload" size="30">
									<?php
									if(isset($CurrentImage->Content) && $CurrentImage->Content!="" && file_exists(DIR_FS_SITE_UPLOADS_OTHER.$CurrentImage->Content))
									{
										?>
										<a href="<?php echo DIR_WS_SITE_UPLOADS_OTHER.$CurrentImage->Content?>" target="_blank" class="adm-link">Available</a>
										<?php
									}
									else 
									{
										echo "Not Available";
									}?>
								</td>
								<td width="10%" align="center" valign="top"><b>Dimension</b></td>
								<td valign="top">
									Width : <input type="text" size="10" name="FlashWidth" id="FlashWidth" value="<?php echo isset($CurrentImage->FlashWidth)?MyStripSlashes($CurrentImage->FlashWidth):""?>">
									&nbsp;&nbsp;&nbsp;&nbsp;
									Height : <input type="text" size="10" name="FlashHeight" id="FlashHeight" value="<?php echo isset($CurrentImage->FlashHeight)?MyStripSlashes($CurrentImage->FlashHeight):""?>">
								</td>
							</tr>
						</table>
					</fieldset>
				</div>
				<div id="DivHTML" style="display:none;">
					<fieldset class="InsideTable"><legend>HTML Image:</legend>
						<table width="100%" border="0" cellspacing="2" cellpadding="1" class="InsideTable">
							<tr class="CartRecordTr">
								<td width="15%" class="CartRecordTd" align="center"><b>HTML Content</b></td>
								<td class="CartRecordTd">
									<textarea name="HTMLContent" cols="50" rows="6"><?php echo isset($CurrentImage->Content)?MyStripSlashes($CurrentImage->Content):""?></textarea>
									
								</td>
							</tr>
						</table>
					</fieldset>
				</div>
				</td>
			</tr>
			<tr>
				<td align="center" class="InsideLeftTd"><b>Activate</b></td>
				<td align="left" class="InsideRightTd">
					<input type="checkbox" name="Active" value="1" class="chk" <?php echo (isset($_POST['Active']) && $_POST['Active']==1)?"checked":((isset($CurrentImage->Active) && $CurrentImage->Active==1)?"checked":"")?>>
				</td>
			</tr>		
			<tr>
				<td align="center" class="InsideLeftTd"></td>
				<td align="left" >
				<input type="submit" name="AddImage" value="Submit">
				</td>
			</tr>
		</table>
		<script type="text/javascript">
		
		function ShowContentType(val)
		{
			if(val=="Image")
			{
				document.getElementById('DivFlash').style.display ="none";
				document.getElementById('DivHTML').style.display ="none";
				document.getElementById('DivImage').style.display ="block";
				document.getElementById('ImageTypeImage').checked=true;
			}
			if(val=="Flash")
			{
				document.getElementById('DivFlash').style.display ="block";
				document.getElementById('DivHTML').style.display ="none";
				document.getElementById('DivImage').style.display ="none";
				document.getElementById('ImageTypeFlash').checked=true;
				
			}
			if(val=="HTML")
			{
				document.getElementById('DivFlash').style.display ="none";
				document.getElementById('DivHTML').style.display ="block";
				document.getElementById('DivImage').style.display ="none";
				document.getElementById('ImageTypeHTML').checked=true;
		
			}
			
			
		}			
			ShowContentType('<?php echo (isset($CurrentImage->ImageType))?$CurrentImage->ImageType:"Image"?>');			
	</script>
		
	</form>
	<?php
	/////// Add Image End
	break;

	/////////Display Image 
	case "DisplayImage" :
	default:
		$OrderBy = isset($_GET['OrderBy'])?$_GET['OrderBy']:"ImageID DESC";
		$ImageObj->Where = "1";
		$ImageObj->AllowPaging =true;
		$ImageObj->PageSize=50;
		$ImageObj->PageTotalDisplay = 10; 
		$ImageObj->PageNo =isset($_GET['PageNo'])?$_GET['PageNo']:1;	
	
		
		$ImageObj->TableSelectAll("",$OrderBy);
		if($ImageObj->GetNumRows() > 0)
		{
		?>
		<script>
				function FunctionDeleteImage(ImageID)
				{
					
						result = confirm("Are you sure want to delete the image?")
						if(result == true)
						{
							URL="<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php echo $Page?>&Target=DeleteImage&ImageID=" + ImageID;
							location.href=URL;
							return true;
						}
					return false;
				}				
			</script>
			<?php
			$TotalRecords = $ImageObj->TotalRecords ;
			$TotalPages =  $ImageObj->TotalPages;
			if($TotalRecords > $ImageObj->PageSize)
			{
			?>
			<br>
			<table cellpadding="1" width="100%" cellspacing="1" border="0" class="InsideTable">
				<tr>
				<td width="100%">
				<?php echo $ImageObj->GetPagingLinks("index.php?Page=".$Page."&OrderBy=".$OrderBy."&PageNo=",PAGING_FORMAT_MULTIPLE,"adm-link","<b>Page No : </b>");?>
				</td>	
				</tr>
			</table>
			<br>
			<?php
		  }
		?>
		<form method="POST" action="<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php echo $Page?>&Section=<?php echo $Section?>&Target=UpdateImage">
		<table border="0" cellpadding="3" cellspacing="1" width="100%" style="BORDER-RIGHT: #a9a9a9 1px solid; BORDER-TOP: #a9a9a9 1px solid; BORDER-LEFT: #a9a9a9 1px solid; BORDER-BOTTOM: #a9a9a9 1px solid">
			<tr>
				<td align="center" width="5%" class="InsideLeftTd"><b>S.No.</b></td>
				<td align="left"  class="InsideLeftTd"><b>Image Name</b></td>
				<td align="left"  class="InsideLeftTd"><b>Image Title</b></td>
				<td align="center"  class="InsideLeftTd"><b>Image</b></td>
				<td align="center"  class="InsideLeftTd"><b>URL</b></td>
				<td align="center"  class="InsideLeftTd"><b>Active</b></td>
				<td align="center"  class="InsideLeftTd"><b>Edit</b></td>
				<td align="center"  class="InsideLeftTd"><b>Delete</b></td>
			</tr>
			<?php
			$SNo=1;
			$Count=1;
			while($CurrentImage = $ImageObj->GetObjectFromRecord())
			{
				?>
			<tr >
				<td align="center" class="InsideRightTd"><b><?php echo $SNo?></b></td>
				<td align="left" class="InsideRightTd">
				<?php echo MyStripSlashes($CurrentImage->ImageName);?>				
				<input type="hidden" name="ImageID_<?php echo $Count?>" value="<?php echo $CurrentImage->ImageID?>"></td>
				<td align="center" class="InsideRightTd">
				<?php echo MyStripSlashes($CurrentImage->ImageTitle);?>				
				</td>
				<td align="center" class="InsideRightTd">
					<?php
					switch ($CurrentImage->ImageType)
					{
						case "Flash":
						 echo "<a href='".DIR_WS_SITE_UPLOADS_OTHER.$CurrentImage->Content."' target='_blank'>Flash</a>";
						break;
						case "HTML":
						 echo MyStripSlashes($CurrentImage->Content);
						break;
						case "Image":
						 echo "<a href='".DIR_WS_SITE_UPLOADS_OTHER.$CurrentImage->Content."' target='_blank'>";
						 SKImgDisplay(DIR_FS_SITE_UPLOADS_OTHER.$CurrentImage->Content,70,'&nbsp;');
						 echo "</a>";
						break;
					}
					?>
				</td>
				<td align="center" class="InsideRightTd">
					<a href="<?php echo MyStripSlashes($CurrentImage->URL)?>" target="_blank"><?php echo MyStripSlashes($CurrentImage->URL)?></a>
				</td>
				<td align="center" class="InsideRightTd">
					<input type="checkbox" name="Active_<?php echo $Count?>" value="1" class="chk" <?php echo $CurrentImage->Active==1?"checked":""?>>
				</td>
				<td align="center" class="InsideRightTd"><a href="<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php echo $Page?>&Section=EditImage&ImageID=<?php echo $CurrentImage->ImageID?>"><b>Edit</b></a></td>
				<td align="center" class="InsideRightTd"><a href="#" onclick="return FunctionDeleteImage('<?php echo $CurrentImage->ImageID?>');"><b>Delete</b></a></td>
			</tr>
			<?php
			$SNo++;
			$Count++;
			}
		?>
			<tr>
				<td align="center" width="5%"></td>
				<td align="center"></td>
				<td align="center"></td>
				<td align="center"></td>
				<td align="center"><input type="hidden" name="Count" value="<?php echo $Count?>"></td>
				<td align="center"><input type="submit" name="UpdateActive" value="Update"></td>
				<td align="center"></td>
				<td align="center"></td>
			</tr>
		</table>
		</form>					
		<?php
		}
		else 
		{
			?>
			<b>No Result Found.</b>
			<?php
		}
	break;
}
///// Section end?>