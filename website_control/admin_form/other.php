<?php
if(isset($_GET['Other']) && $_GET['Other'] !="")
{
	if(file_exists(DIR_FS_SITE_MODULE_OTHER.$_GET['Other']."/admin_form/".$_GET['Other'].".php"))
		require_once(DIR_FS_SITE_MODULE_OTHER.$_GET['Other']."/admin_form/".$_GET['Other'].".php");
}
else 
{
?>
	<table border="0" cellpadding="0" cellspacing="0" width="100%">
		<tr>
			<td height="20" align="center">
			<h4>Others</h4>
		</td>
		</tr>	
	</table><?php
	$OtherModuleObj = new DataTable(TABLE_MODULES);
	$OtherModuleObj->Where ="ModuleType='Other' AND Active='1'";
	$OtherModuleObj->TableSelectAll("","ModuleID ASC");
	if($OtherModuleObj->GetNumRows() >0)
	{?>
	<table border="0" cellpadding="3" cellspacing="1" width="100%"%">
	<tr>
		<td colspan="3" align="left">
		<?php
		if($OtherModuleObj->GetNumRows() =="1")
		{
			$CurrentModule = $OtherModuleObj->GetObjectFromRecord();
			MyRedirect(DIR_WS_SITE_CONTROL."index.php?Page=".$Page."&Other=".$CurrentModule->ModuleCode);
			exit;
			?>
			<table border="0" cellpadding="3" cellspacing="1" width="100%"%" class="InsideTable">
			<tr>
					<td class="InsideLeftTd"><b>&nbsp;Other Method</b></td>
				</tr>
				<tr>
					<td align="left" class="InsideRightTd">
						<a href="<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php echo $Page?>&Other=<?php echo $CurrentModule->ModuleCode?>" class="adm-link">&nbsp;&nbsp;<?php echo $CurrentModule->Description?></a>
					</td>
				</tr>
			</table>
			<?php
		}
		else 
		{
			?>
			<table border="0" cellpadding="3" cellspacing="1" width="100%"%" class="InsideTable">
				<tr>
					<td class="InsideLeftTd"><b>&nbsp;Other Sections</b></td>
				</tr>
				<?php
				$SNo=1;
				while($CurrentModule = $OtherModuleObj->GetObjectFromRecord())
				{?>
				<tr>
					<td align="left" class="InsideRightTd">
						<a href="<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php echo $Page?>&Other=<?php echo $CurrentModule->ModuleCode?>" class="adm-link">&nbsp;&nbsp;<?php echo $CurrentModule->Description?></a>
					</td>
				</tr>
				<?php
				$SNo++;
				}?>				
			</table>
			<?php
		}?>
		</td>
	</tr>
	</table>
	<?php
	}
}?>
				