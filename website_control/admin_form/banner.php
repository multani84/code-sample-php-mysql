<!-- Heading  -->
<div class="row-fluid">
	<div class="span12">
		<h1><?php echo $Label?> Banners</h1>
		<hr />
	</div>
	<!--<div class="pull-left">
		&nbsp;&nbsp;<a href="<?php  echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php  echo $Page?>&Alignment=H" class="adm-link">HomePage Slider</a>&nbsp;&nbsp;|	
        &nbsp;&nbsp;<a href="<?php  echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php  echo $Page?>&Alignment=S" class="adm-link">HomePage Static</a>&nbsp;&nbsp;|		
        &nbsp;&nbsp;<a href="<?php  echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php  echo $Page?>&Alignment=BR" class="adm-link">Brand Slider</a>&nbsp;&nbsp;|		
	</div>-->
	<div class="pull-right">
		<a href="<?php  echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php  echo $Page?>&Section=AddBanner&Alignment=<?php  echo $Alignment?>" class="btn btn-primary">Add Banner<icon class="icon-plus-sign icon-white-t"></icon></a>
	</div>
<div class="clearfix separator bottom"></div>
<!--  End Heading-->	
				
<?php
//// Section start
switch($Section)
{
	case "AddBanner" :
	case "EditBanner" :
	/////// Add Banner  Start
	?>
	
	<form class="form-horizontal" method="POST" action="<?php  echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php  echo $Page?>&Section=<?php  echo $Section?>&Target=AddBanner&Alignment=<?php  echo $Alignment?>&BannerID=<?php  echo $BannerID?>" enctype="multipart/form-data">
		<div class="well">
			<div class="form-group">
				<label for="field" class="col-sm-4 col-md-5 col-lg-4 control-label">Banner Name</label>
				<div class="col-sm-20 col-md-19 col-lg-20 text-left">
					<input type="text" name="BannerName" value="<?php  echo isset($CurrentBanner->BannerName)?MyStripSlashes($CurrentBanner->BannerName):""?>" size="60">
				</div>
			</div>
			
			<div class="form-group">
				<label for="field" class="col-sm-4 col-md-5 col-lg-4 control-label">Banner Title</label>
				<div class="col-sm-20 col-md-19 col-lg-20 text-left">
					<input type="text" name="Parameters" value="<?php  echo isset($CurrentBanner->Parameters)?MyStripSlashes($CurrentBanner->Parameters):""?>" size="60">
				</div>
			</div>
			
			<div class="form-group">
				<label for="field" class="col-sm-4 col-md-5 col-lg-4 control-label">Banner Type</label>
				<div class="col-sm-20 col-md-19 col-lg-20 text-left">
					<input type="radio" class="chk" name="BannerType" id="BannerTypeImage" value="Image" class="radio" checked onclick="return ShowContentType(this.value);">&nbsp;Image
					<input type="radio" class="chk" name="BannerType" id="BannerTypeHTML" value="HTML" class="radio" onclick="return ShowContentType(this.value);">&nbsp;HTML				
				</div>
			</div>
			
			<div class="form-group">
				<label for="field" class="col-sm-4 col-md-5 col-lg-4 control-label"></label>
				<div class="col-sm-20 col-md-19 col-lg-20 text-left">
					<div id="DivImage" style="display:none;">
					<fieldset class="InsideTable"><legend>Image Banner:</legend>
							<table class="table table-striped table-bordered table-responsive block">
								<tr class="CartRecordTr">
									<td class="CartRecordTd" width="40%">
										<input type="file" name="Upload" size="30">
										<?php
										if(isset($CurrentBanner->Content) && $CurrentBanner->Content!="" && file_exists(DIR_FS_SITE_UPLOADS_BANNER.$CurrentBanner->Content))
										{
											?>
											<a href="<?php  echo DIR_WS_SITE_UPLOADS_BANNER.$CurrentBanner->Content?>" target="_blank" class="adm-link">Available</a>
											<?php
										}
										else 
										{
											//echo "Not Available";
										}?>
									</td>
									<td width="10%" align="center"><b>URL</b></td>
									<td valign="top">
										<input type="text" size="40" name="URL" id="URL" value="<?php  echo isset($CurrentBanner->URL)?MyStripSlashes($CurrentBanner->URL):""?>">
										&nbsp;&nbsp;&nbsp;&nbsp;
										<select name="URLType">
										<option value="_blank" <?php  echo (isset($CurrentBanner->URLType) && $CurrentBanner->URLType=="_blank")?"selected":""?>>Open in new window</option>
										<option value="_self" <?php  echo (isset($CurrentBanner->URLType) && $CurrentBanner->URLType=="_self")?"selected":""?>>Open in same window</option>
										</select>
									</td>
								</tr>
							</table>
						</fieldset>
					</div>
					<div id="DivHTML" style="display:none;">
						<fieldset class="InsideTable"><legend>HTML Banner:</legend>
							<table width="100%" border="0" cellspacing="2" cellpadding="1" class="InsideTable">
								<tr class="CartRecordTr">
									<td width="15%" class="CartRecordTd" align="center"><b>HTML Content</b></td>
									<td class="CartRecordTd">
										<textarea name="HTMLContent" cols="50" rows="6"><?php  echo isset($CurrentBanner->Content)?MyStripSlashes($CurrentBanner->Content):""?></textarea>
										
									</td>
								</tr>
							</table>
						</fieldset>
					</div>	
				</div>
			</div>
			
			<div class="form-group">
				<label for="field" class="col-sm-4 col-md-5 col-lg-4 control-label">Activate</label>
				<div class="col-sm-20 col-md-19 col-lg-20 text-left">
					<input type="checkbox" name="Active" value="1" class="chk" <?php  echo (isset($_POST['Active']) && $_POST['Active']==1)?"checked":((isset($CurrentBanner->Active) && $CurrentBanner->Active==1)?"checked":"")?>>
				</div>
			</div>
			
			<div class="form-group">
				<div class="col-sm-6 pull-right">
					<button class="btn btn-warning  btn-block" type="submit" name="AddBanner">Submit</button>
				</div>
			</div>
			
		</div>
		
		<script type="text/javascript">
		
		function ShowContentType(val)
		{
			if(val=="Image")
			{
				document.getElementById('DivHTML').style.display ="none";
				document.getElementById('DivImage').style.display ="block";
				document.getElementById('BannerTypeImage').checked=true;
			}
			
			if(val=="HTML")
			{
				document.getElementById('DivHTML').style.display ="block";
				document.getElementById('DivImage').style.display ="none";
				document.getElementById('BannerTypeHTML').checked=true;
		
			}
			
			
		}			
			ShowContentType('<?php  echo (isset($CurrentBanner->BannerType))?$CurrentBanner->BannerType:"Image"?>');			
	</script>
		
	</form>
	<?php
	/////// Add Banner End
	break;

	/////////Display Banner 
	case "DisplayBanner" :
	default:
		if($Alignment !="")
			$BannerObj->Where = "Alignment='".$BannerObj->MysqlEscapeString($Alignment)."'";
		
		$BannerObj->TableSelectAll("","BannerID ASC");
		if($BannerObj->GetNumRows() > 0)
		{
		?>
		<script>
				function FunctionDeleteBanner(BannerID)
				{
					
						result = confirm("Are you sure want to delete the banner?")
						if(result == true)
						{
							URL="<?php  echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php  echo $Page?>&Target=DeleteBanner&Alignment=<?php  echo $Alignment?>&BannerID=" + BannerID;
							location.href=URL;
							return true;
						}
					return false;
				}				
			</script>							
		<form method="POST" action="<?php  echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php  echo $Page?>&Section=<?php  echo $Section?>&Target=UpdateBanner&Alignment=<?php  echo $Alignment?>">
		<table class="table table-striped table-bordered table-responsive block">
			<thead>
				<tr>
					<th align="center" width="5%" class="InsideLeftTd">S.No.</th>
					<th align="left"  class="InsideLeftTd">Banner Name</th>
					<th align="left"  class="InsideLeftTd">Banner Title</th>
					<th align="center"  class="InsideLeftTd">Banner Type</th>
					<th align="center"  class="InsideLeftTd">URL</th>
					<th align="center"  class="InsideLeftTd">Active</th>
					<th align="center"  class="InsideLeftTd">Position</th>
					<th align="center"  class="InsideLeftTd">Edit</th>
					<th align="center"  class="InsideLeftTd"></th>
				</tr>
			</thead>
			<?php
			$SNo=1;
			$Count=1;
			while($CurrentBanner = $BannerObj->GetObjectFromRecord())
			{
				?>
			<tr >
				<td align="center" class="InsideRightTd"><b><?php  echo $SNo?></b></td>
				<td align="left" class="InsideRightTd">
				<?php echo MyStripSlashes($CurrentBanner->BannerName);?>				
				<input type="hidden" name="BannerID_<?php  echo $Count?>" value="<?php  echo $CurrentBanner->BannerID?>"></td>
				<td align="center" class="InsideRightTd">
				<?php  echo  MyStripSlashes($CurrentBanner->Parameters);?>				
				</td>
				<td align="center" class="InsideRightTd">
					<?php
					switch ($CurrentBanner->BannerType)
					{
						case "Flash":
						 echo "<a href='".DIR_WS_SITE_UPLOADS_BANNER.$CurrentBanner->Content."' target='_blank'>Flash</a>";
						break;
						case "HTML":
						 echo MyStripSlashes($CurrentBanner->Content);
						break;
						case "Image":
						 echo "<a href='".DIR_WS_SITE_UPLOADS_BANNER.$CurrentBanner->Content."' target='_blank'><img src='".DIR_WS_SITE_UPLOADS_BANNER.$CurrentBanner->Content."' border='0' width='150'></a>";
						break;
					}
					?>
				</td>
				<td align="center" class="InsideRightTd">
					<a href="<?php  echo MyStripSlashes($CurrentBanner->URL)?>" target="_blank"><?php  echo MyStripSlashes($CurrentBanner->URL)?></a>
				</td>
				<td align="center" class="InsideRightTd">
					<input type="checkbox" name="Active_<?php  echo $Count?>" value="1" class="chk" <?php  echo $CurrentBanner->Active==1?"checked":""?>>
				</td>
				<td align="center" class="InsideRightTd"><input type="text" name="Position_<?php  echo $Count?>" size="3" value="<?php  echo $CurrentBanner->Position?>" size="3" style="text-align:center;width:60px"></td>
				<td align="center" class="InsideRightTd"><a href="<?php  echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php  echo $Page?>&Section=EditBanner&Alignment=<?php  echo $Alignment?>&BannerID=<?php  echo $CurrentBanner->BannerID?>" class="btn btn-success btn-phone-block"><icon class="icon-pencil icon-white"></icon> Edit</a></td>
				<td align="center" class="InsideRightTd">
					<?php
					if($CurrentBanner->Alignment !="0")
					{?>
					<a href="#" onclick="return FunctionDeleteBanner('<?php  echo $CurrentBanner->BannerID?>');" class="btn btn-danger btn-phone-block"><icon class="icon-remove icon-white"></icon> Delete</a>
					<?php
					}?>
				</td>
			</tr>
			<?php
			$SNo++;
			$Count++;
			}
		?>
			<tr>
				<td align="center" width="5%"></td>
				<td align="center"></td>
				<td align="center"></td>
				<td align="center"></td>
				<td align="center"><input type="hidden" name="Count" value="<?php  echo $Count?>"></td>
				<td align="center" colspan="2" style="text-align:center;"><input type="submit" name="UpdateActive" value="Update" class="btn btn-primary"></td>
				<td align="center"></td>
				<td align="center"></td>
			</tr>
		</table>
		</form>					
		<?php
		}
		else 
		{
			?>
			<b>No Result Found.</b>
			<?php
		}
	break;
}
///// Section end?>