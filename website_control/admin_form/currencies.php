<!-- Heading  -->
<div class="row-fluid">
	<div class="span12">
		<h1>CURRENCIES</h1>
		<hr />
	</div>
	<div>
		<a href="<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php echo $Page?>&Section=Addcurrencies" class="btn btn-primary pull-right">Add Currencies <icon class="icon-plus-sign icon-white-t"></icon></a>
	</div>
<div class="clearfix separator bottom"></div>
<!--  End Heading-->		



<?php 
switch($Section)
{
	case "Addcurrencies":
	case "Editcurrencies":
	/////// Add Product  Start
	?>
	<div class="row-fluid">
		
		<?php /*?>
	<!--  Tabs -->
	<div class="span3">
		<ul class="tabs-arrow">
			<li class="active"><a class="glyphicons pencil" href="#lA" data-toggle="tab"><i></i> 1. Add details</a></li>
			<li class=""><a class="glyphicons camera" href="#lB" data-toggle="tab"><i></i> 2. Add images</a></li>
			<li class=""><a class="glyphicons list" href="#lC" data-toggle="tab"><i></i> 3. Set properties</a></li>
		</ul>
		<button type="submit" class="btn btn-primary btn-block">Save product <icon class="icon-ok icon-white-t"></icon></button>
		<div class="clearfix"></div><br/>
	</div>
	<!-- End Tabs -->
	<?php */?>	
	<div class="span9">
		<div class="tab-content">
			<form method="POST" action="<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php echo $Page?>&Section=<?php echo $Section?>&Target=AddCurrency&CurrencyID=<?php echo $CurrencyID?>" enctype="multipart/form-data">
			<!--  Tab Content Block -->
			<div class="tab-pane active" id="lA">
				<h3>Add/Edit details</h3>
				<div class="form-horizontal well">

					<legend>Currency details</legend>
					<div class="control-group">
						<label class="control-label" for="inputEmail">Currency Name</label>
						<div class="controls">
							<input type="text" name="CurrencyName" value="<?php echo isset($CurrentCurrency->CurrencyName)?$CurrentCurrency->CurrencyName:""?>" size="30">
						</div>
					</div>
					<div class="control-group">
						<label class="control-label" for="inputEmail">Currency Code</label>
						<div class="controls">
							<input type="text" name="Code" value="<?php echo isset($CurrentCurrency->Code)?$CurrentCurrency->Code:""?>" size="30">
						</div>
					</div>
					<div class="control-group">
						<label class="control-label" for="inputEmail">Currency Symbol</label>
						<div class="controls">
							<input type="text" name="Symbol" value="<?php echo isset($CurrentCurrency->Symbol)?$CurrentCurrency->Symbol:""?>" size="30">
						</div>
					</div>
					<div class="control-group">
						<label class="control-label" for="inputEmail">Currency Value</label>
						<div class="controls">
							<input type="text" name="Value" value="<?php echo isset($CurrentCurrency->Value)?$CurrentCurrency->Value:""?>" size="30">
						</div>
					</div>
					<div class="control-group">
						<label class="control-label" for="inputEmail">Prefix</label>
						<div class="controls">
							<input type="radio" value="1" name="Prefix" class="radio" checked>&nbsp;YES 
							<br /> 
							<input type="radio" value="0" name="Prefix" class="radio" <?php echo isset($CurrentCurrency->Prefix) && $CurrentCurrency->Prefix==0?"checked":""?>>&nbsp;No
									
						</div>
					</div>

					<div class="control-group">
						<label class="control-label" for="inputEmail">Active</label>
						<div class="controls">
							<input type="checkbox" name="Active" value="1" class="chk" <?php echo ((isset($_POST['Active']) and $_POST['Active']=="1")?"checked":((isset($CurrentCurrency->Active) and $CurrentCurrency->Active=="1")?"checked":""))?>>
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;		
						</div>
					</div>

					<div class="clearfix"></div>
				
					

				</div>
					
					<div class="pull-right">
						<button type="submit" class="btn btn-success pull-right">Submit <icon class="icon-share-alt icon-white-t"></icon></button>
					</div>
					<div class="clearfix"></div><br/>
				
			</div>
			<!--  End Tab Content Block -->
			</form>
			
		</div>
	</div>	
	
	<?php
	
	break;
	case "DisplayCurrency" :
	default:
		$CurrencyObj = new DataTable(TABLE_CURRENCIES);
		$CurrencyObj->Where = "1";
		$CurrencyObj->TableSelectAll("","CurrencyID ASC");
		?>
	<table width="100%">
		<tr>
		<td align="center">
			<form method="POST" action="<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php echo $Page?>&Section=<?php echo $Section?>&Target=UpdateCurrency">
				<table class="table table-striped table-bordered table-responsive block">
						<thead>
						<tr>
							<th height="40"  class="InsideLeftTd">S.No</th>
							<th class="InsideLeftTd">Currency Name</th>
							<th class="InsideLeftTd" align="center">Currency Value</th>
							<th class="InsideLeftTd" align="center">Active</th>
							<th class="InsideLeftTd" align="center">Default</th>
							<th class="InsideLeftTd">EDIT</th>
							<th class="InsideLeftTd">DELETE</th>
						</tr>
						</thead>
						<tbody>
						<script type="text/javascript">
						function FunctionDeleteCurrency()
						{
							result = confirm("Are you sure want to delete the currency?")
							if(result == true)
							{
								return true;
							}
							return false;
						}				
						</script>
				
						<?php
						
							$SNo=1;
							$Count=1;
							$k=1;
							while ($CurrentCurrency = $CurrencyObj->GetObjectFromRecord())
							{?>
							<tr>
								<td class="InsideRightTd"><?php echo $SNo?>
								<input type="hidden" name="CurrencyID_<?php echo $Count?>" value="<?php echo $CurrentCurrency->CurrencyID?>"></td>
								<td class="InsideRightTd"><?php echo $CurrentCurrency->CurrencyName?> (<?php echo $CurrentCurrency->Code?>)</td>
								<td class="InsideRightTd" align="center"><?php echo $CurrentCurrency->Value?></td>
								<td align="center"><input type="checkbox" name="Active_<?php echo $Count?>" value="1" class="chk" <?php echo @$CurrentCurrency->Active==1?"checked":""?>></td>
								<td align="center"><input type="radio" name="DefaultValue" value="<?php echo $CurrentCurrency->CurrencyID?>" class="chk" <?php echo @$CurrentCurrency->DefaultValue==1?"checked":""?>></td>	
								<td class="InsideRightTd">
									<a href="<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php echo $Page?>&Section=Editcurrencies&CurrencyID=<?php echo $CurrentCurrency->CurrencyID?>" class="btn btn-success btn-phone-block"><icon class="icon-pencil icon-white"></icon> Edit</a>
								</td>
								<td class="InsideRightTd">
									<?php
										if($CurrentCurrency->CurrencyID != 1)
										{
									?>
										<a href="<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php echo $Page?>&Target=DeleteCurrency&CurrencyID=<?php echo $CurrentCurrency->CurrencyID?>" class="btn btn-danger btn-phone-block" onclick="return FunctionDeleteCurrency();"><icon class="icon-remove icon-white"></icon> Delete</a>
									<?php	}
									?>
								</td>
						</tr>
							<?php
							$SNo++;
							$Count++;
							}
							?>
						<tr>
							<td height="35" class="InsideRightTd"></td>
							<td class="InsideRightTd"><input type="hidden" name="Count" value="<?php echo $Count?>"></td>
							<td class="InsideRightTd"></td>
							<td align="center" class="InsideRightTd" colspan="2">
							<input type="submit"  name="UpdateCurrency<?php echo $Count?>" value="Update" class="btn btn-warning"></td>
							<td class="InsideRightTd"></td>
							<td class="InsideRightTd"></td>
							
						</tr>
					</tbody>						
					</table>
				</form>				
				<br>
				<form method="POST" action="<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=<?php echo $Page?>&Section=<?php echo $Section?>&Target=UpdateCurrency4Web">
				<table width="100%" cellpadding="0" cellspacing="0" class="InsideTable">
					<tr>
						<td class="InsideLeftTd" height="45" align="center"><input type="submit" value="Update Currencies" style="height:30px;"></td>
					</tr>
				</table>
				</form>
			</td>
		</tr>
	</table>
	<?php 
	break;
} ?>
	
	
	
	
	
