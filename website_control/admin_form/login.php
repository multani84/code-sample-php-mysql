<!-- Heading  -->
<div class="row-fluid">
	<div class="span12">
		<h1>Dashboard</h1>
		<hr />
	</div>
</div>
<!--  End Heading -->
<?php
//// Section start
switch($Section)
{
	case "login" :
	default:
	/////// 
	if(!isset($_SESSION['AdminObj']))
	{
	@ob_clean();	
	?>
	
<?php require(DIR_FS_SITE_CONTROL_INCLUDES."control_meta.php");?>	
<body class="login">
	<!--  Header -->
	
	<!-- Top Gray Line -->
	<div class="navbar navbar-fixed-top top-line">
		<div class="container-fluid">
			<!-- Logo -->
			<div class="pull-left">
				<a href="<?php echo DIR_WS_SITE_CONTROL."index.php"?>" class="brand"><?php echo d('ADMIN_TITLE')?> - <?php echo DEFINE_SITE_NAME?></a>
			</div>
			<!-- End Logo -->
			
		</div>
	</div>
	<!-- End Top Gray Line -->

		
	<!-- End Header -->
<?php if(isset($ErrorMessage) && $ErrorMessage != ""):?>
		<div class="alert alert-danger"><?php echo @$ErrorMessage?></div>
	<?php endif;?>
	
	<!-- Start Content -->
				<div class="container-fluid mainContainerFluid">
		
		<!-- Login Box -->
<form action="<?php echo DIR_WS_SITE_CONTROL?>index.php?Target=LoginCheck&Return=<?php echo @$_GET['Return']?>" class="login-form" id="form" method="post">
<div class="well" style="width:290px;margin-top:30px;">
		<legend>
		<icon class="icon-circles"></icon>Restricted Area</icon>
	</legend>
	<div class="control-group">
		<label class="control-label" for="inputPassword">Username</label>
		<div class="controls">
			<div class="input-prepend">
				<input class="input" type="text" name="UserName" id="UserName" placeholder="Username" required />
			</div>
		</div>
	</div>
	<div class="control-group">
		<label class="control-label" for="inputPassword">Password</label>
		<div class="controls">
			<div class="input-prepend">
				</span> <input class="input" type="password" name="Password" id="Password" value="" placeholder="Password" required/>
			</div>
		</div>
	</div>
	<div class="control-group signin">
		<div class="controls ">
			<button type="submit" class="btn btn-block" id="">Login</button>
			
		</div>
	</div>
</div>
</form>
<!--  End Login Box-->
	
		
	</div>
		<!-- End Content  -->

	
	
</body>
</html>
	<?php
	exit;
	}
	else 
	{
		/* Login Dashboard SaleGraph start */
		?>
		<!--  Flot (Charts) JS -->
	<!--[if lte IE 8]><script language="javascript" type="text/javascript" src="<?php echo DIR_WS_SITE_CONTROL_THEMES.THEME_STYLE?>//scripts/flot/excanvas.min.js"></script><![endif]-->
	<script src="<?php echo DIR_WS_SITE_CONTROL_THEMES.THEME_STYLE?>/scripts/flot/jquery.flot.js" type="text/javascript"></script>
	<script src="<?php echo DIR_WS_SITE_CONTROL_THEMES.THEME_STYLE?>/scripts/flot/jquery.flot.pie.js" type="text/javascript"></script>
	<script src="<?php echo DIR_WS_SITE_CONTROL_THEMES.THEME_STYLE?>/scripts/flot/jquery.flot.tooltip.js" type="text/javascript"></script>
	<script src="<?php echo DIR_WS_SITE_CONTROL_THEMES.THEME_STYLE?>/scripts/flot/jquery.flot.resize.js" type="text/javascript"></script>
	<script src="<?php echo DIR_WS_SITE_CONTROL_THEMES.THEME_STYLE?>/scripts/flot/jquery.flot.orderBars.js" type="text/javascript"></script>

<?php 
$OrderObj1 = new DataTable(TABLE_ORDERS ." o, ".TABLE_ORDER_CURRENCIES." oc ");	
$Mode = isset($_GET['Mode'])?$_GET['Mode']:"M";
$YearArray = isset($_GET['Y'])?$_GET['Y']:array(date('Y'));
$YearArray = array_unique($YearArray);

$MonthArray = isset($_GET['M'])?$_GET['M']:array(date('Y-m'));
$MonthArray = array_unique($MonthArray);

$MaxValue = 10;
?>	
<?php switch($Mode)
{
	case "M":
	?>
	<!-- Search -->
		<form class="form-horizontal form-search well" id="FrmFilter" method="GET" action="index.php">
			<div class="pull-right" style="margin-top:-5px;">
				<select style="width:100px;margin-top:-10px;" name="Mode" onchange='document.location.href="index.php?Page=<?php echo $Page?>&Mode="+this.value'>
				<option value="Y" <?php echo $Mode=='Y'?"selected":""?>>Yearly</option>
				<option value="M" <?php echo $Mode=='M'?"selected":""?>>Monthly</option>
				</select>
			</div>
			<legend>Dashboard</legend>
			<input type="hidden" name="Page" value="<?php echo $Page?>" >
			<input type="hidden" name="Mode" value="M">
			Month(s)
			<?php foreach($MonthArray as $k=>$MonthIndex):?>
			<select name="M[]" class="span3" style="width:130px;">
						<?php
						for($y=date('Y')-1;$y<=date('Y')+1;$y++)
						{
							for($m=1;$m<=12;$m++)
							{	
							?>
							<option value="<?php echo date("Y-m", mktime(0, 0, 0, $m, 1, $y))?>" <?php echo date("Y-m", mktime(0, 0, 0, $m, 1, $y))==$MonthIndex?"selected":""?>><?php echo date("Y-M", mktime(0, 0, 0, $m, 1, $y))?></option>
							<?php
							}
						}?>					
					</select>
					&nbsp;&nbsp;&nbsp;&nbsp;
			<?php endforeach;?>
				<span id="CompareMonth"><a href="javascript:" onclick="GetCompareDump('CompareMonth','MonthDump');">Compare</a></span>
				&nbsp;&nbsp;&nbsp;&nbsp;
				<button type="submit" class="btn"><icon class="icon-search icon-brown"></icon> Go</button>
				<textarea id="MonthDump" style="display:none;"><select name="M[]" class="span3" style="width:130px;">
						<?php
						for($y=date('Y')-1;$y<=date('Y')+1;$y++)
						{
							for($m=1;$m<=12;$m++)
							{	
							?>
							<option value="<?php echo date("Y-m", mktime(0, 0, 0, $m, 1, $y))?>"><?php echo date("Y-M", mktime(0, 0, 0, $m, 1, $y))?></option>
							<?php
							}
						}?>					
					</select>
					&nbsp;&nbsp;&nbsp;&nbsp;</textarea>
		</form>
	<!-- End Search -->	
	<?php
	break;
	case "Y":
	default:
?>
<!-- Search -->
		<form class="form-horizontal form-search well" id="FrmFilter" method="GET" action="index.php">
			<div class="pull-right" style="margin-top:-5px;">
				<select style="width:100px;margin-top:-10px;" name="Mode" onchange='document.location.href="index.php?Page=<?php echo $Page?>&Mode="+this.value'>
				<option value="Y" <?php echo $Mode=='Y'?"selected":""?>>Yearly</option>
				<option value="M" <?php echo $Mode=='M'?"selected":""?>>Monthly</option>
				</select>
			</div>
			<legend>Dashboard</legend>
			<input type="hidden" name="Page" value="<?php echo $Page?>" >
			<input type="hidden" name="Mode" value="Y">
			Year(s)
			<?php foreach($YearArray as $k=>$Year):?>
			<select name="Y[]" class="span3" style="width:130px;">
						<?php
						for($i=date('Y')-5;$i<=date('Y')+5;$i++)
						{
							?>
							<option value="<?php echo $i?>" <?php echo $i==$Year?"selected":""?>><?php echo $i?></option>
							<?php
						}?>					
					</select>
					&nbsp;&nbsp;&nbsp;&nbsp;
			<?php endforeach;?>
				<span id="CompareYear"><a href="javascript:" onclick="GetCompareDump('CompareYear','YearDump');">Compare</a></span>
				&nbsp;&nbsp;&nbsp;&nbsp;
				<button type="submit" class="btn"><icon class="icon-search icon-brown"></icon> Go</button>
				<textarea id="YearDump" style="display:none;"><select name="Y[]" class="span3" style="width:130px;"><?php
					for($i=date('Y')-5;$i<=date('Y')+5;$i++)
					{
						?>
						<option value="<?php echo $i?>"><?php echo $i?></option>
						<?php
					}?>					
				</select>&nbsp;&nbsp;&nbsp;&nbsp;</textarea>
		</form>
	<!-- End Search -->
<?php 
break;
}?>	
<?php 

$Data = $XaxisGraph = array();
switch($Mode)
{
	
	case "M":
	
	foreach($MonthArray as $k=>$Year_Month)
	{
		if($Year_Month !="")
		{
			$arr = explode("-",$Year_Month);
			$Year = $arr[0];
			$Month = $arr[1];
			$Data[$Year_Month]['Title']  =  date('Y-M',strtotime($Year.'-'.$Month.'-01'));
			$Data[$Year_Month]['Values']  =  array();
			$XaxisGraph = array();
			$Xaxis = array();
			$NosOfDays = 31;//cal_days_in_month(CAL_GREGORIAN, $Month, $Year);
			for($i=1;$i<=$NosOfDays;$i++)
			{
				$CurrentDay = $Year."-".$Month."-".$i;
				$OrderObj1->Where = "o.OrderID = oc.OrderID AND o.OrderStatus='Paid' AND DATE_FORMAT(o.CreatedDate,'%Y-%m-%e') = '".$CurrentDay."'";
				$RecordAmountObj = $OrderObj1->TableSelectOne(array("SUM(o.GrandTotal/oc.ExchangeRate) as Total","Count(*) as TotalOrder"));								
				$val = isset($RecordAmountObj->Total)?number_format($RecordAmountObj->Total,2):0;		
				$val = str_replace(",","",$val);
				$MaxValue = max($val,$MaxValue);
				
				$Data[$Year_Month]['Values'][$i] = $val;
				$Data[$Year_Month]['Graph'][] = "[".$i.",".$val."]";
				$XaxisGraph[] = "[".$i.",'".$i."']";
				$Xaxis[$i] = $i;
			}
			
		}
	}
	
	break;
	case "Y":
	default:
	foreach($YearArray as $k=>$Year)
	{
		if($Year !="")
		{
			$Data[$Year]['Title']  =  $Year;
			$Data[$Year]['Values']  =  array();
			$XaxisGraph = array();
			$Xaxis = array();
			for($i=1;$i<=12;$i++)
			{
				$CurrentMonth = $Year."-".$i;
				$OrderObj1->Where = "o.OrderID = oc.OrderID AND o.OrderStatus='Paid' AND DATE_FORMAT(o.CreatedDate,'%Y-%c') = '".$CurrentMonth."'";
				$RecordAmountObj = $OrderObj1->TableSelectOne(array("SUM(o.GrandTotal/oc.ExchangeRate) as Total","Count(*) as TotalOrder"));								
				$val = isset($RecordAmountObj->Total)?number_format($RecordAmountObj->Total,2):0;		
				$val = str_replace(",","",$val);
				
				$MaxValue = max($val,$MaxValue);
				
				//$val = rand(0,100);
				$Data[$Year]['Values'][$i] = $val;
				$Data[$Year]['Graph'][] = "[".$i.",".$val."]";
				$XaxisGraph[] = "[".$i.",'".date('M',strtotime('2015-'.$i.'-01'))."']";
				$Xaxis[$i] = date('M',strtotime('2015-'.$i.'-01'));
			}
		}
	}
	
	break;
}
?>
<script>
function GetCompareDump(El1,El2)
{
	$("#"+El1).html($("#"+El2).val());
	return false;
}
jQuery(document).ready(function($){

		var plot = $.plot("#order_sale_graph", [ 
			<?php $Sno = 0;
			foreach($Data as $k=>$val):?>
			{ data: [<?php echo implode (",",$Data[$k]['Graph'])?>], label: "<?php echo $Data[$k]['Title']?>"}<?php echo $Sno++<count($Data)?",":""?>
			<?php endforeach;?>
		], {
			series: {
				lines: {
					show: true
				},
				points: {
					show: true
				}
			},
			grid: {
				hoverable: true,
				borderColor : "#f4e9c9",
				color : "#763d00",
				backgroundColor : {
						colors : [ "#fff", "#fff" ]
					},
					
				clickable: true
			},
			xaxis : {
				ticks : [<?php echo implode (",",$XaxisGraph)?>]
			},
			yaxis : {
				ticks : 10,
				min : 0,
				max : <?php echo ceil($MaxValue)?>
			},
			tooltip : true
			
			
		});
		
		$("#order_sale_graph").bind("plothover", function (event, pos, item) {
			if (item) {
					var x = plot.getOptions().xaxis.ticks[item.dataIndex][1],
						y = item.datapoint[1].toFixed(2);
					$("#flotTip").html(item.series.label + " of " + x + " - sale: " + y);
				} else {
					$("#flotTip").hide();
				}
		});
});
</script>

<div class="row-fluid">
	<!-- Analytics -->
	<div class="span8">
		<div class="well">
			<h4 class="heading glyphicons cardio"><i></i>Sales</h4>
			<div id="order_sale_graph" style="height: 350px;"></div>
		</div>
	</div>
	<div class="span4" >
		<div class="well">
			<h4 class="heading glyphicons stats"><i></i>Overview</h4>
			<div style="height: 350px;overflow: auto;">
			<table class="table table-striped table-bordered table-responsive block">
				<thead>
					<tr>
					<th align="center" width="5%" class="InsideLeftTd">S.No.</th>
				<?php foreach($Data as $k=>$val):?>	
						<th align="center" width="5%" class="InsideLeftTd"><?php echo $Data[$k]['Title']?></th>
				<?php endforeach;?>	
					</tr>
				<thead>
				<tbody>
				<?php foreach($Xaxis as $index=>$Value):?>	
					<tr>
					<td align="center" width="5%" class="InsideLeftTd"><?php echo $Value?></td>
					<?php foreach($Data as $k=>$val):?>	
							<td align="center" width="5%" class="InsideLeftTd"><?php echo number_format($Data[$k]['Values'][$index],2)?></td>
					<?php endforeach;?>	
					</tr>
				<?php endforeach;?>	
				</tbody>
				</table>
			</div>	
			
		</div>		
	</div>
	<!-- END Analytics -->
	
</div>
		<?php
		/* Login Dashboard SaleGraph end*/
		
		/* Login Dashboard Account start */
		?>
		<hr>
<div class="row-fluid">
	<div class="span4">
		<div class="well ">
		<?php 
				$OrderObj1 = new DataTable(TABLE_ORDERS ." o, ".TABLE_ORDER_CURRENCIES." oc ");
				$OrderObj1->Where = "o.OrderID = oc.OrderID AND o.OrderStatus='Paid' AND DATE_FORMAT(o.CreatedDate,'%Y-%m-%d') = '".date('Y-m-d')."'";
				$RecordAmountObj = $OrderObj1->TableSelectOne(array("SUM(o.GrandTotal/oc.ExchangeRate) as Total","Count(*) as TotalOrder"));								
			?>
			<h4 class="heading glyphicons book"><i></i>Stats</h4>
			<ul class="nav nav-dotted">
				<li>Today's orders <span class="info"><?php echo isset($RecordAmountObj->TotalOrder)?number_format($RecordAmountObj->TotalOrder,0):0?></span></li>
				<li>Today's Total <span class="info"><?php echo isset($RecordAmountObj->Total)?Change2CurrentCurrency($RecordAmountObj->Total):0?></span></li>
		<?php 
				$OrderObj1 = new DataTable(TABLE_ORDERS ." o, ".TABLE_ORDER_CURRENCIES." oc ");
				$OrderObj1->Where = "o.OrderID = oc.OrderID AND o.OrderStatus='Paid' AND DATE_FORMAT(o.CreatedDate,'%Y-%m') = '".date('Y-m')."'";
				$RecordAmountObj = $OrderObj1->TableSelectOne(array("SUM(o.GrandTotal/oc.ExchangeRate) as Total","Count(*) as TotalOrder"));								
			?>
			<li><?php echo date('Y,M')?>'s orders <span class="info"><?php echo isset($RecordAmountObj->TotalOrder)?number_format($RecordAmountObj->TotalOrder,0):0?></span></li>
			<li><?php echo date('Y,M')?>'s Total <span class="info"><?php echo isset($RecordAmountObj->Total)?Change2CurrentCurrency($RecordAmountObj->Total):0?></span></li>
			</ul>
		</div>
	</div>
	<!-- End Block  -->

	<!-- Block  -->
	<div class="span4 ">
		<div class="well ">
			<h4 class="heading glyphicons user"><i></i>Latest users</h4>
			<?php 
				
				$UserObj = new DataTable(TABLE_USERS);
				$UserObj->Where="1";
				$UserObj->TableSelectAll(array("*, DATE_FORMAT(CreatedDate,'%b %d, %Y')as MyCreatedDate, DATE_FORMAT(CreatedDate,'%d %M, %Y')as MyCreatedMonth"),"CreatedDate DESC",4);
			
			?>
			<ul class="nav nav-dotted list nav-orders">
				<?php while ($CurrentUser = $UserObj->GetObjectFromRecord()):?>
					<li>
					<div class="pull-left">
						<a href ="<?php echo DIR_WS_SITE_CONTROL?>index.php?Page=account/user&Section=EditUser&UserID=<?php echo $CurrentUser->UserID?>" class="adm-link"><?php echo MyStripSlashes($CurrentUser->UserName)?></a>
						<div class="clearfix"></div>
					</div>
					<div class="clearfix"></div>
				</li>
				<?php endwhile;?>
			</ul>
		</div>
	</div>
	<!-- End Block  -->

	<!-- Block  -->
	<div class="span4">
		<div class="well compact">
			<h4 class="heading glyphicons shopping_cart"><i></i>Latest orders</h4>
			<?php 
				
				$OrderObj = new DataTable(TABLE_ORDERS);
				$OrderObj->Where="OrderStatus='Paid'";
				$OrderObj->TableSelectAll(array("*, DATE_FORMAT(CreatedDate,'%b %d, %Y')as MyCreatedDate, DATE_FORMAT(CreatedDate,'%d %M, %Y')as MyCreatedMonth"),"CreatedDate DESC",4);
			
			?>
			<ul class="nav nav-dotted list nav-orders">
			<?php while ($CurrentOrder = $OrderObj->GetObjectFromRecord()):
					$CurrencyOrderID = $CurrentOrder->OrderID;
			?>
					<li>
					<div class="pull-left">
						<label class="label label-warning pull-left">New Order <a href ="index.php?Page=shop/display_order&Section=FullDetail&OrderID=<?php echo $CurrentOrder->OrderID?>" class="text-danger">#<?php echo $CurrentOrder->OrderNo?></a></label>
						<div class="clearfix"></div>
						<span class="info">Total: <?php echo Change2CurrentCurrency($CurrentOrder->GrandTotal)?> 
					</div>
					<div class="pull-right">
						<a href ="index.php?Page=shop/display_order&Section=FullDetail&OrderID=<?php echo $CurrentOrder->OrderID?>" class="btn"><icon class="icon-arrow-right icon-brow-fluidn"></icon></a>
					</div>
					<div class="clearfix"></div>
				</li>
				<?php endwhile;?>
			</ul>
		</div>
	</div>
	<!-- End Block  -->
</div>	
		<?php
		/* Login Dashboard Account end */
		
	}
	
	/////// 
	break;
	
}
///// Section end?>