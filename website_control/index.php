<?php require_once("../includes/configure.php");
 require_once(DIR_FS_SITE_CONTROL."includes/admin_configure.php");
 

 if(strstr($Page,"/"))
{
	$DArray = explode("/",$Page,2);
	define("DEFINE_DIR_NAME_FRONT",DIR_FS_SITE."source/".substr(@$DArray[0],0,20)."/admin_design/",true);	
	define("DEFINE_DIR_NAME_SCRIPT",DIR_FS_SITE."source/".substr(@$DArray[0],0,20)."/admin_code/",true);	
	define("DEFINE_FILE_NAME",substr(@$DArray[1],0,50),true);
}
else
{
	define("DEFINE_DIR_NAME_FRONT",DIR_FS_SITE_CONTROL_FORMS,true);	
	define("DEFINE_DIR_NAME_SCRIPT",DIR_FS_SITE_CONTROL_ADMINSCRIPT,true);	
	define("DEFINE_FILE_NAME",basename($Page),true);	
}

	if(DEFINE_FILE_NAME !="")
	{
		if(file_exists(DEFINE_DIR_NAME_SCRIPT.DEFINE_FILE_NAME.".php"))
		{
			if(in_array($Page,$PagePermissionArray) || in_array("*",$PagePermissionArray))
				require_once(DEFINE_DIR_NAME_SCRIPT.DEFINE_FILE_NAME.".php");						
		}
	}
	if(@$_REQUEST['Meta'] !="false")
		require_once(DIR_FS_SITE_CONTROL_INCLUDES."control_meta.php");
	?>
<body class="main left-menu sticky_footer">	
<?php 
if(@$_REQUEST['Popup'] !="true")
	require_once(DIR_FS_SITE_CONTROL_INCLUDES."control_header.php");
	?>
<table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr>
		<td bgcolor="" align="center" valign="top">
			<table border="0" cellpadding="0" cellspacing="0" width="98%">
				<tr>
					<td valign="top" align="center" style="padding-top:6px;">
					<?php require_once(DIR_FS_SITE_INCLUDES."message.php");?>
					<!-------Start Here --------->
					<?php if(DEFINE_FILE_NAME !="") 
						{
							if(file_exists(DEFINE_DIR_NAME_FRONT.DEFINE_FILE_NAME.".php"))
							{
								if(in_array($Page,$PagePermissionArray) || in_array("*",$PagePermissionArray))
									require_once(DEFINE_DIR_NAME_FRONT.DEFINE_FILE_NAME.".php");
								else 	
									echo"<b>You do not have permission to access the page.</b>";
							}
							else 
							{
								echo"<b>Page is under construction.</b>";
							}
						}
						else 
						{
							echo "&nbsp;";
						} ?>
						<!-------End Here--------->
					</td>
				 </tr>
			</table>
		</td>
	</tr>
</table>
<?php 
if(@$_REQUEST['Popup'] !="true")
	require_once(DIR_FS_SITE_CONTROL_INCLUDES."control_footer.php");?>
	<script type="text/javascript">
	function InitCall()
	{
		$.ajax({
			   type: "GET",
			   url: "index.php",
			   data: "",
			   success: function(msg){
			   }
			 });
		setTimeout('InitCall()', 600000);
	}
	InitCall();
	</script>
	<?php
$sk_timeend = explode(' ', microtime() );
$sk_timeend = $sk_timeend[1] + $sk_timeend[0];
echo "<!--".number_format($sk_timeend-$sk_timestart,3)."-->";	
?>
<script type="text/javascript" src="<?php echo DIR_WS_SITE_INCLUDES?>footer.js.php"></script>
</body>
</html>
