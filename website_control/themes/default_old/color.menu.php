<?php
	
	############ Color Defination 
	define("COLOR_MENU_BGCOLOR","#1A559C");
	define("COLOR_MENU_BGCOLOR_HOVER","#679FE4");
	define("COLOR_MENU_BORDER","#ffffff");

	define("COLOR_MENU_DIVIDER_BGCOLOR","#ffffff");
	define("COLOR_MENU_DIVIDER_BORDER","#ffffff");
	
	define("COLOR_MENU_TEXT","#ffffff");
	define("COLOR_MENU_TEXT_HOVER","#000000");
	define("COLOR_MENU_TEXT_BORDER","#ffffff");
	############ Color Defination 
?>